<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Experiencia extends Model
{

    protected $table = 'experiencias';

    public $timestamps = true;

    public $fillable = [

        'plataforma_id',
        'user_id',
        'empresa',
        'cargo',
        'data_inicio',
        'data_fim',
        'sobre',
        'atividades',
        'estrelas',

    ];

    public static  function boot()
    {
        parent::boot(); 
        static::creating(function ($model)
        {
            $model->plataforma_id  = \Auth()->user()->plataforma_id;
        });
    }

    public function scopePlataforma($query)
    {
        return $query->where('plataforma_id', session('plataforma_id') ?? Plataforma::dominio()->pluck('id')[0] ?? null);
    }

   
}
