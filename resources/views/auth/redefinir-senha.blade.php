@extends('layouts.auth')
@section('content')
<div class="h-100vh">
    <div class="container h-100">
        <div class="row align-items-center h-100">
            <div class="col-md-5 mx-auto">
                <div class="boxLogin bg-white px-4">

                    <div class="card-body">
                        <form method="POST">
                            @csrf

                            <div class="text-center mb-4-5 mt-2">
                                <a href="/">
                                    <img src="{{ $plataforma->logotipo }}" class="img-fluid" width="80">
                                </a>
                            </div>

                            <h1 class="text-center color-555 color-555 fs-19">
                                Redefinir Senha
                            </h1>
                            <hr/>

                            <div class="input-group">
                                    
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <i class="fa fa-key color-555"></i>
                                    </div>
                                </div>

                                <input id="password" type="password" class="form-control bg-f9 weight-600 color-555 ls-5 p-4" name="new_password" required placeholder="Defina sua Nova Senha">

                            </div>

                            <div class="text-center">
                                <button type="submit" name="redefinir" value="true" class="mt-4 btn btn-primary weight-600 w-100 py-2">
                                    Confirmar nova senha
                                </button>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection