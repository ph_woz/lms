@php

  $cursosGraduacao = \App\Models\Curso::select('nome','slug','foto_miniatura')
      ->plataforma()
      ->ativo()
      ->publico()
      ->graduacao()
      ->orderBy('nome')
      ->get();

  $cursosPosGraduacao = \App\Models\Curso::select('nome','slug','foto_miniatura')
      ->plataforma()
      ->ativo()
      ->publico()
      ->posGraduacao()
      ->orderBy('nome')
      ->get();

  $cursosExtensao = \App\Models\Curso::select('nome','slug')
      ->plataforma()
      ->ativo()
      ->publico()
      ->cursosExtensao()
      ->orderBy('nome')
      ->get();

@endphp

{!! $plataforma->scripts_start_body !!}

<header>
   
   <div class="sub-header py-lg-2-5 pt-2">
      <div class="container">
         <div class="row align-items-center">
            <div class="col-lg-10">

               <div>

                  <li class="d-xs-none p-0 m-0 d-inline-block">
                     <a class="btn-submenu px-2 d-xs-none" href="#">
                       Acadêmico
                       <i class="fas fa-caret-down"></i>
                     </a>
                   <div uk-dropdown class="box-shadow rounded z-9 m-0 m-0 bg-faixa">
                      <ul class="uk-nav uk-dropdown-nav">
                        <a href="https://jbh.faroroseira.edu.br/index.php/jbh" target="_blank" class="d-block btn-submenu py-1 px-2">Revista Científica</a>
                        <a href="/editais" class="d-block btn-submenu py-1 px-2">Editais</a>
                        <a class="d-block btn-submenu py-1 px-2" href="https://modular.mentorweb.ws/modularSecurityG5/?pcaes=1aeeca054c75260d78afb1450ccb248e2d7758e322540603e0f1addb26be6f0ed544599cf791fbfa1f1de5eb830b0d09f3eb85b4a65fb48bd0a0fc03ae5f4da43b99ce3ea6f8e53e8b14541dd742e486ce7b57858fe68e31c3e18b4005c436e5ef000e3da32d24ca832e8d7564eb0dc29540d32f0065bf322e078856b6a6b5606205ee8a76cc31447754a6bd32de4299ee40b492bfd4fdf36f85edc85321c9806d36bc64eda65a01cf37334ad50266b94b383c9f32d1e145fffbc9df2faac600406d40ef02e86af9df4a21dfa5098164e9553a8da6a2f08e21e2f41d361f9f68bf2e04adcb418ae9747e5509fd4589dc" target="_blank">Mentor Web</a>
                        <a href="/dou" class="d-block btn-submenu py-1 px-2">DOU</a>
                      </ul>
                   </div>
                  </li>

                  <a href="/polos" class="btn-submenu px-2 d-xs-none">Polos</a>
                  <a href="/biblioteca" class="btn-submenu px-2 d-xs-none">Biblioteca</a>
                  <a href="/sobre-nos" class="btn-submenu px-2 d-xs-none">Conheça-nos</a>
                  <a href="/ouvidoria" class="btn-submenu px-2 d-xs-none">Ouvidoria</a>
                  <a href="https://zeno.fm/radio/faro-roseira/" target="_blank" class="btn-submenu px-2 d-xs-none">Rádio</a>
                  <a href="/transferencia" class="btn-submenu px-2 d-xs-none">Transferência</a>
                  <a href="/fies" class="btn-submenu px-2 d-xs-none">FIES</a>
                  <a href="/enem" class="btn-submenu px-2 d-xs-none">ENEM</a>
                  <a href="/consultar-diploma" class="btn-submenu px-2 d-xs-none">Consultar Diploma</a>
                  
               </div>

               <div class="w-100 d-lg-none">
                  <div class="w-100 d-flex justify-content-between">
                     <a href="/f/formulario-de-matricula/45/t/631" target="_blank" class="btn-submenu btnPortalAluno mx-0">
                        <i class="fas fa-graduation-cap me-1"></i>
                        Matricule-se
                     </a>
                     <a href="https://faroroseira.grupoa.education/plataforma/" target="_blank" class="btnPortalAluno">
                        Área do Aluno
                     </a>
                  </div>
               </div>

            </div>

            <div class="col-lg-2 d-xs-none d-md-none d-lg-block" align="right">

               <a href="https://faroroseira.grupoa.education/plataforma/" target="_blank" class="btnPortalAluno">
                  Área do Aluno
               </a>

            </div>

         </div>
      </div>
   </div>
 
   <nav class="navbar navbar-expand-lg bg-white box-shadow z-6" id="menu">
      <div class="container-fluid px-xl-7">
          <a class="navbar-brand nav-link-hide-cat" href="/#inicio">
            <img src="{{ $plataforma->logotipo }}" width="200" alt="Logotipo {{ $plataforma->nome }}">
          </a>
          <button class="navbar-toggler outline-0 border-0 text-dark" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="fa fa-bars"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarSupportedContent">
               <ul class="navbar-nav align-items-center ms-auto mb-2 mb-lg-0">

                  <li class="nav-item">
                     <a class="nav-link" href="#">
                       Como Ingressar
                       <i class="fas fa-caret-down"></i>
                     </a>
                   <div uk-dropdown class="box-shadow rounded">
                      <ul class="uk-nav uk-dropdown-nav">
                           <li><a href="/vestibular" class="nav-link fs-15">Vestibular</a></li>
                           <li><a href="/transferencia" class="nav-link fs-15">Transferência</a></li>
                           <li><a href="/enem" class="nav-link fs-15">ENEM</a></li>
                           <li><a href="/fies" class="nav-link fs-15">FIES</a></li>
                           <li><a href="/prouni" class="nav-link fs-15">ProUni</a></li>
                      </ul>
                   </div>
                  </li>
                  <li class="nav-item">
                     <a class="nav-link" href="/graduacao">
                        Graduação
                        <i class="fas fa-caret-down"></i>
                     </a>
                     <div uk-dropdown class="box-shadow rounded">
                        <ul class="uk-nav uk-dropdown-nav">
                           @foreach($cursosGraduacao as $cursoGraduacao)
                              <li><a href="/curso/{{$cursoGraduacao->slug}}" class="nav-link fs-15">{{ $cursoGraduacao->nome }}</a></li>
                           @endforeach
                        </ul>
                     </div>
                  </li>
                  <li class="nav-item">
                     <a class="nav-link" href="/pos-graduacao">
                        Pós graduação
                        <i class="fas fa-caret-down"></i>
                     </a>
                     <div uk-dropdown class="box-shadow rounded">
                        <ul class="uk-nav uk-dropdown-nav">
                           @foreach($cursosPosGraduacao as $cursoPosGraduacao)
                              <li><a href="/curso/{{$cursoPosGraduacao->slug}}" class="nav-link fs-15">{{ $cursoPosGraduacao->nome }}</a></li>
                           @endforeach
                        </ul>
                     </div>
                  </li>
                  <li class="nav-item">
                     <a class="nav-link" href="/cursos-de-extensao">
                        Cursos de Extensão
                        <i class="fas fa-caret-down"></i>
                     </a>
                     <div uk-dropdown class="box-shadow rounded">
                        <ul class="uk-nav uk-dropdown-nav">
                           @foreach($cursosExtensao as $cursoExtensao)
                              <li><a href="/curso/{{$cursoExtensao->slug}}" class="nav-link fs-15">{{ $cursoExtensao->nome }}</a></li>
                           @endforeach
                        </ul>
                     </div>
                  </li>
                  <li class="nav-item ms-lg-5">
                    <a class="nav-link btnInscrever animation-grow d-inline-block" href="/f/formulario-de-matricula/45/t/631" target="_blank">
                       <i class="fas fa-graduation-cap me-1"></i>
                       Matricule-se
                    </a>
                  </li>

              </ul>
          </div>
      </div>
   </nav>

</header>