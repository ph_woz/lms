<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProdutosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('produtos', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('plataforma_id');
            $table->unsignedBigInteger('cadastrante_id')->nullable();
            $table->unsignedBigInteger('unidade_id')->nullable();
            $table->string('produtos_iguais_cores_diferentes')->nullable();
            $table->string('produtos_iguais_tamanhos_diferentes')->nullable();
            $table->string('nome');
            $table->string('marca')->nullable();
            $table->string('modelo')->nullable();
            $table->string('tamanho')->nullable();
            $table->string('cor')->nullable();
            $table->string('foto')->nullable();
            $table->string('video')->nullable();
            $table->text('descricao')->nullable();
            $table->integer('status')->default(0);
            $table->integer('visibilidade')->default(0); // Listar ou Não Listado para Venda
            $table->string('disponivel', 1)->default('S');
            $table->string('publicar', 1)->default('S');
            $table->string('exibir_preco', 1)->default('S');
            $table->string('valor_parcelado')->nullable();
            $table->string('valor')->nullable();
            $table->string('valor_promocional')->nullable();
            $table->string('frete_gratis', 1)->default('N');
            $table->string('estoque')->nullable();
            $table->string('peso')->nullable();
            $table->string('comprimento')->nullable();
            $table->string('largura')->nullable();
            $table->string('altura')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('produtos');
    }
}
