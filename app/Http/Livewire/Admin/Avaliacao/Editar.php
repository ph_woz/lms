<?php

namespace App\Http\Livewire\Admin\Avaliacao;

use Livewire\Component;
use App\Models\Avaliacao;
use App\Models\Curso;
use App\Models\Turma;
use App\Models\Categoria;
use App\Models\AvaliacaoCategoria;
use App\Models\AvaliacaoConfigNota;
use Illuminate\Support\Facades\Http;

class Editar extends Component
{
    public $avaliacao;
    public $cursos;
    public $curso_id;
    public $turmas = [];
    public $turma_id;
    public $referencia;
    public $condicao_fazer_avaliacao;
    public $n_tentativas;
    public $liberar_gabarito = 'S';
    public $instrucoes;
    public $status = 0;

    public $i = 0;
    public $questoesQuantidadesMinimas = [];
    public $questoesNotas = [];

    public $categoriasSelected = [];

    protected $rules = [
        'referencia' => 'required',
        'curso_id'   => 'required',
        'turma_id'   => 'required',
    ];

    protected $messages = [
        'referencia.required' => 'O Nome não pode estar vazio.',
        'curso_id.required' => 'O Curso não pode estar vazio.',
        'turma_id.required' => 'A Turma não pode estar vazio.',
    ];

    public function updated()
    {
        if($this->curso_id)
        {
            $this->turmas = Turma::plataforma()->ativo()->where('curso_id', $this->curso_id)->select('id','nome')->orderBy('nome')->get();
        }
    }

    public function mount($avaliacao_id)
    {
        $this->avaliacao = Avaliacao::plataforma()->find($avaliacao_id);
        
        $this->referencia = $this->avaliacao->referencia;
        $this->curso_id   = $this->avaliacao->curso_id;

        $this->turmas = Turma::plataforma()->ativo()->where('curso_id', $this->curso_id)->select('id','nome')->orderBy('nome')->get();
        $this->turma_id   = $this->avaliacao->turma_id;

        $this->cursos = Curso::plataforma()->ativo()->orderBy('referencia')->get();

        $this->status = $this->avaliacao->status;
        $this->n_tentativas = $this->avaliacao->n_tentativas;
        $this->condicao_fazer_avaliacao = $this->avaliacao->condicao_fazer_avaliacao;
        $this->liberar_gabarito = $this->avaliacao->liberar_gabarito;
        $this->instrucoes = $this->avaliacao->instrucoes;

        $notas = AvaliacaoConfigNota::plataforma()->where('avaliacao_id', $this->avaliacao->id)->get()->pluck('n_questoes','valor_nota')->toArray();

        $this->categoriasSelected = AvaliacaoCategoria::plataforma()->where('avaliacao_id', $this->avaliacao->id)->get()->pluck('categoria_id')->toArray();

        foreach ($notas as $valor_nota => $n_questoes)
        {
            $this->i = $this->i+1;
            $this->questoesQuantidadesMinimas[$this->i] = $n_questoes;
            $this->questoesNotas[$this->i] = $valor_nota;
        }
    }

    public function editar()
    {
        $this->validate();

        $this->avaliacao->update([
            'curso_id'                 => $this->curso_id,
            'turma_id'                 => $this->turma_id,
            'referencia'               => $this->referencia,
            'condicao_fazer_avaliacao' => $this->condicao_fazer_avaliacao,
            'n_tentativas'             => $this->n_tentativas,
            'liberar_gabarito'         => $this->liberar_gabarito,
            'instrucoes'               => $this->instrucoes,
            'status'                   => $this->status,
        ]);

        AvaliacaoConfigNota::plataforma()->where('avaliacao_id', $this->avaliacao->id)->delete();

        $notas = array_combine($this->questoesQuantidadesMinimas, $this->questoesNotas);

        foreach ($notas as $n_questoes => $valor_nota)
        {
            if($n_questoes !== null && $valor_nota !== null)
            {
                $countDuplicate = AvaliacaoConfigNota::plataforma()
                    ->where('avaliacao_id', $this->avaliacao->id)
                    ->where('valor_nota', $valor_nota)
                    ->count();

                if($countDuplicate == 0)
                {
                    AvaliacaoConfigNota::create([
                        'avaliacao_id' => $this->avaliacao->id,
                        'n_questoes'   => $n_questoes,
                        'valor_nota'   => $valor_nota,
                    ]);
                }
            }
        }

        $this->syncCategorias($this->avaliacao->id);

        session()->flash('success-livewire', 'Avaliação atualizada com êxito.');
        
        $this->dispatchBrowserEvent('toast');
    }

    public function syncCategorias($avaliacao_id)
    {
        AvaliacaoCategoria::plataforma()->where('avaliacao_id', $avaliacao_id)->delete();

        if($this->categoriasSelected == null)
        {
            return;
        }

        foreach($this->categoriasSelected as $categoria_id)
        {            
            AvaliacaoCategoria::create([
                'plataforma_id' => session('plataforma_id'),
                'avaliacao_id'  => $avaliacao_id, 
                'categoria_id'  => $categoria_id,
            ]);
        }
    }

    public function addCampoNota($i)
    {
        $this->i = $i+1;
        $this->questoesQuantidadesMinimas[$this->i] = null;
    }

    public function removeCampoNota($i)
    {
        unset($this->questoesQuantidadesMinimas[$i]);
        unset($this->questoesNotas[$i]);
    }

    public function render()
    {
        $listaNotas = Avaliacao::listaNotas();
        $categorias = Categoria::plataforma()->ativo()->tipo('avaliacao')->orderBy('nome')->get();

        return view('livewire.admin.avaliacao.editar', ['listaNotas' => $listaNotas, 'categorias' => $categorias] );
    }

}
