<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $table = 'users';

    protected $fillable = [
        'plataforma_id',
        'nome',
        'email',
        'password',
        'tipo',
        'status',
        'unidade_id',
        'restringir_unidade',
        'data_ultimo_acesso',
        'data_docs_recebidas',

        'cadastrante_id',
        'cpf',
        'rg',
        'naturalidade',
        'data_nascimento',
        'celular',
        'telefone',
        'genero',
        'profissao',
        'foto_perfil',
        'obs',
        'descricao',
        'aceita_termos',
        'token',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    public function scopePlataforma($query)
    {
        return $query->where('plataforma_id', session('plataforma_id') ?? Auth()->user()->plataforma_id ?? Plataforma::dominio()->pluck('id')[0] ?? null);
    }

    public static  function boot()
    {
        parent::boot(); 
        static::creating(function ($model) {
            $model->plataforma_id  = session('plataforma_id') ?? Auth()->user()->plataforma_id ?? Plataforma::dominio()->pluck('id')[0] ?? null;
            $model->cadastrante_id = Auth()->user()->id ?? null;
        });
    }

    public function scopeAtivo($query)
    {
        return $query->where('status', 0);
    }

    public function scopeAluno($query)
    {
        return $query->where('tipo', 'aluno');
    }

    public function scopeColaborador($query)
    {
        return $query->where('tipo', 'colaborador');
    }

}
