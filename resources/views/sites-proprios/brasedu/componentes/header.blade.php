   <header class="fixed-top">

      @php
         $expiresAt = \Carbon\Carbon::now()->addHour();

         $plataforma_id = session('plataforma_id') ?? \Cache::remember('sitesproprios-brasedu-plataforma_id', $expiresAt, function() {
            return \App\Models\Plataforma::dominio()->pluck('id')[0] ?? null;
         });

         $cursosExtensao = \Cache::remember('sitesproprios-brasedu-cursosExtensao', $expiresAt, function() use ($plataforma_id) {
            return \App\Models\Trilha::select('id','nome','slug','foto_capa')
               ->where('plataforma_id', $plataforma_id)
               ->where('publicar', 'S')
               ->where('status', 0)
               ->where('nivel','Cursos de Extensão')
               ->orderBy('nome')
               ->get();
         });

         $cursos2Graduacao = \Cache::remember('sitesproprios-brasedu-cursos2Graduacao', $expiresAt, function() use ($plataforma_id) {
            return \App\Models\Trilha::select('id','nome','slug','foto_capa')
               ->where('plataforma_id', $plataforma_id)
               ->where('publicar', 'S')
               ->where('status', 0)
               ->where('nivel','Segunda Graduação')
               ->orderBy('nome')
               ->get();
         });

         $cursosGraduacao = \Cache::remember('sitesproprios-brasedu-cursosGraduacao', $expiresAt, function() use ($plataforma_id) {
            return \App\Models\Trilha::select('id','nome','slug','foto_capa')
               ->where('plataforma_id', $plataforma_id)
               ->where('publicar', 'S')
               ->where('status', 0)
               ->where('nivel','Graduação')
               ->orderBy('nome')
               ->get();
         });

         $cursosPosGraduacao = \Cache::remember('sitesproprios-brasedu-cursosPosGraduacao', $expiresAt, function() use ($plataforma_id) {
            return \App\Models\Trilha::select('id','nome','slug','foto_capa')
               ->where('plataforma_id', $plataforma_id)
               ->where('publicar', 'S')
               ->where('status', 0)
               ->where('nivel','Pós-Graduação')
               ->orderBy('nome')
               ->get();
         });

         $cursos2Licenciatura = \Cache::remember('sitesproprios-brasedu-cursos2Licenciatura', $expiresAt, function() use ($plataforma_id) {
            return \App\Models\Trilha::select('id','nome','slug')
               ->where('plataforma_id', $plataforma_id)
               ->where('publicar', 'S')
               ->where('status', 0)
               ->where('nivel','Segunda Licenciatura')
               ->orderBy('nome')
               ->get();
         });

         $cursosFormacaoPedagogicaR2 = \Cache::remember('sitesproprios-brasedu-cursosFormacaoPedagogicaR2', $expiresAt, function() use ($plataforma_id) {
            return \App\Models\Trilha::select('id','nome','slug')
               ->where('plataforma_id', $plataforma_id)
               ->where('publicar', 'S')
               ->where('status', 0)
               ->where('nivel','Formação Pedagógica R2')
               ->orderBy('nome')
               ->get();
         });

         $cursosAperfeicoamento = \Cache::remember('sitesproprios-brasedu-cursosAperfeicoamento', $expiresAt, function() use ($plataforma_id) {
            return \App\Models\Trilha::select('id','nome','slug')
               ->where('plataforma_id', $plataforma_id)
               ->where('publicar', 'S')
               ->where('status', 0)
               ->where('nivel','Aperfeiçoamento')
               ->orderBy('nome')
               ->get();
         });
      @endphp

      <div class="sub-header container-fluid px-xl-7 py-2">
         <div class="d-lg-flex align-items-center">
            <div class="w-lg-25">

               <a class="navbar-brand d-xs-none" href="/#inicio">
                  <img src="{{ $plataforma->logotipo ?? 'https://wozcodelms2.s3.amazonaws.com/plataformas/4/logotipo.png' }}" width="100" alt="Logotipo {{ $plataforma->nome }}">
               </a>

            </div>

            <div class="w-100 d-flex align-items-center justify-content-end" align="right">

               <a href="https://api.whatsapp.com/send?phone=55{{preg_replace('/\D/', '', $plataforma->whatsapp)}}&text=Olá, vim através do Site {{ $plataforma->nome }}." target="_blank" class="btn-submenu d-xs-none">
                  <i class="fab fa-whatsapp fs-17"></i>
                  <span class="d-xs-none"> 
                     Matricule-se pelo WhatsApp
                  </span>
                  <span class="d-lg-none ms-1">
                     {{ $plataforma->whatsapp }}
                  </span>
               </a>

               <a href="/#contato" class="btn-submenu d-xs-none">
                  Fale Conosco
               </a>

               <a href="/sobre-nos" class="btn-submenu d-xs-none">
                  Quem somos
               </a>

               <a href="https://blog.braseducursos.com.br/" target="_blank" class="btn-submenu me-4 d-xs-none">
                  Blog
               </a>

               <div class="w-xs-100 d-flex justify-content-between">

                  <a href="/login" class="btnAreaAluno me-lg-3 text-center animation-grow d-block weight-700 font-poppins">
                     Portal do Aluno
                  </a>

                  <a href="https://braseducursos.com.br/f/ficha-de-matricula/37/t/627" target="_blank" class="btnAreaAluno text-center animation-grow d-block weight-700 font-poppins">
                     <i class="fas fa-graduation-cap me-1"></i>
                     Matricule-se
                  </a>

               </div>

            </div>

         </div>
      </div>

      <nav class="navbar navbar-expand-lg bg-white box-shadow z-6" id="menu">
         <div class="container-fluid px-xl-7 py-lg-2 py-2">
               <a class="navbar-brand d-lg-none" href="/#inicio">
                  <img src="{{ $plataforma->logotipo ?? 'https://wozcodelms2.s3.amazonaws.com/plataformas/4/logotipo.png' }}" width="100" alt="Logotipo {{ $plataforma->nome }}">
               </a>
               <button class="navbar-toggler outline-0 border-0 text-dark" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
               <span class="fa fa-bars"></span>
               </button>
               <div class="collapse navbar-collapse" id="navbarSupportedContent">
                  <ul class="navbar-nav mx-auto mb-2 mb-lg-0 align-items-center text-center">
                     <li class="nav-item">
                        <a class="nav-link" href="/graduacao">
                           Graduação
                        </a>
                        <div uk-dropdown class="box-shadow rounded">
                           <ul class="uk-nav uk-dropdown-nav">
                              @foreach($cursosGraduacao as $cursoGraduacao)
                                 <li><a href="/curso/{{$cursoGraduacao->slug}}" class="nav-link-drop">{{ $cursoGraduacao->nome }}</a></li>
                              @endforeach
                           </ul>
                        </div>
                     </li>
                     <li class="nav-item">
                        <a class="nav-link" href="/pos-graduacao">
                           Pós-graduação
                        </a>
                        <div uk-dropdown class="box-shadow rounded">
                           <ul class="uk-nav uk-dropdown-nav">
                              @foreach($cursosPosGraduacao as $cursoPosGraduacao)
                                 <li><a href="/curso/{{$cursoPosGraduacao->slug}}" class="nav-link-drop">{{ $cursoPosGraduacao->nome }}</a></li>
                              @endforeach
                           </ul>
                        </div>
                     </li>
                     <li class="nav-item">
                        <a class="nav-link" href="/segunda-graduacao">
                           2ª Graduação
                        </a>
                        <div uk-dropdown class="box-shadow rounded">
                           <ul class="uk-nav uk-dropdown-nav">
                              @foreach($cursos2Graduacao as $curso2Graduacao)
                                 <li><a href="/curso/{{$curso2Graduacao->slug}}" class="nav-link-drop">{{ $curso2Graduacao->nome }}</a></li>
                              @endforeach
                           </ul>
                        </div>
                     </li>
                     <li class="nav-item">
                        <a class="nav-link" href="/segunda-licenciatura">
                           2ª Licenciatura
                        </a>
                        <div uk-dropdown class="box-shadow rounded">
                           <ul class="uk-nav uk-dropdown-nav">
                              @foreach($cursos2Licenciatura as $curso2Licenciatura)
                                 <li><a href="/curso/{{$curso2Licenciatura->slug}}" class="nav-link-drop">{{ $curso2Licenciatura->nome }}</a></li>
                              @endforeach
                           </ul>
                        </div>
                     </li>
                     <li class="nav-item">
                        <a class="nav-link" href="/formacao-pedagogica-r2">
                           Formação Pedagógica R2
                        </a>
                        <div uk-dropdown class="box-shadow rounded">
                           <ul class="uk-nav uk-dropdown-nav">
                              @foreach($cursosFormacaoPedagogicaR2 as $cursoFormacaoPedagogicaR2)
                                 <li><a href="/curso/{{$cursoFormacaoPedagogicaR2->slug}}" class="nav-link-drop">{{ $cursoFormacaoPedagogicaR2->nome }}</a></li>
                              @endforeach
                           </ul>
                        </div>
                     </li>
                     <li class="nav-item">
                        <a class="nav-link" href="/aperfeicoamento">
                           Aperfeiçoamento
                        </a>
                        <div uk-dropdown class="box-shadow rounded">
                           <ul class="uk-nav uk-dropdown-nav">
                              @foreach($cursosAperfeicoamento as $cursoAperfeicoamento)
                                 <li><a href="/curso/{{$cursoAperfeicoamento->slug}}" class="nav-link-drop">{{ $cursoAperfeicoamento->nome }}</a></li>
                              @endforeach
                           </ul>
                        </div>
                     </li>
                     <li class="nav-item">
                        <a class="nav-link" href="/cursos-de-extensao">
                           Cursos de Extensão
                        </a>
                        <div uk-dropdown class="box-shadow rounded">
                           <ul class="uk-nav uk-dropdown-nav">
                              @foreach($cursosExtensao as $cursoExtensao)
                                 <li><a href="/curso/{{$cursoExtensao->slug}}" class="nav-link-drop">{{ $cursoExtensao->nome }}</a></li>
                              @endforeach
                           </ul>
                        </div>
                     </li>

                  </ul>
               </div>
         </div>
      </nav>

   </header>