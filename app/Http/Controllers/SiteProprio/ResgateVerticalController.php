<?php

namespace App\Http\Controllers\SiteProprio;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Contato;
use App\Models\Plataforma;
use App\Models\PaginaConteudo;
use App\Models\Banner;
use App\Models\Parceiro;
use App\Models\Curso;
use Mail;
use App\Mail\EmailFormulario;

class ResgateVerticalController extends Controller
{

    public function index()
    {
        $plataforma = Plataforma::plataforma()->first();

        $seo = PaginaConteudo::plataforma()->where('url', '/')->first();

        $cursos = Curso::select('nome','slug','nivel','foto_miniatura')->plataforma()->ativo()->publico()->orderBy('nome')->get();

        $banners  = Banner::plataforma()->ativo()->orderBy('ordem')->get();
        $clientes = Parceiro::plataforma()->publico()->orderBy('ordem')->get();

        if(request('btnSend'))
        {
            if(request('check'))
            {
                return back()->with('danger', 'SPAM Detected!');
            }

            $contato = \App\Models\Contato::create(request()->all() + [
                'plataforma_id' => $seo->plataforma_id,
                'tipo' => 'L',
                'assunto' => 'Solicitar Contato',
            ]);

            $email = Plataforma::select('email')->where('id', $seo->plataforma_id)->value('email');

            if($email != null)
            {
                Mail::to($email)->send(new EmailFormulario($contato));
            }

            return redirect('/#form-contato')->with('success','Inscrição enviada com êxito.');
        }

        return view('sites-proprios.resgate-vertical.home', compact('seo','cursos','banners','clientes') );
    }

    public function curso($slug)
    {
        $curso = \App\Models\Curso::plataforma()->publico()->where('slug', $slug)->firstOrFail();

        return view('sites-proprios.resgate-vertical.curso', compact('curso') );
    }

    public function consultarDiploma()
    {
        $seo = PaginaConteudo::plataforma()->where('url','/consultar-diploma')->select('plataforma_id','seo_title','seo_keywords','seo_description')->first();


        $trilhas = [];
        $turmas  = [];
        $aluno   = [];
        $tipo    = $_GET['tipo']  ?? null;
        $value   = $_GET['value'] ?? null;


        if(request('sent'))
        {
            if(request('tipo') == 'CPF')
            {
                $cpf = $this->formatCnpjCpf($value);

                $aluno = \App\Models\User::select('id','nome')->plataforma()->aluno()->where('cpf', $cpf)->first();

                if(isset($aluno))
                {
                    $trilhas = \App\Models\TrilhaAluno::select('trilhas.nome','trilhas_alunos.data_conclusao')
                        ->join('trilhas','trilhas_alunos.trilha_id','trilhas.id')
                        ->where('trilhas_alunos.plataforma_id', $seo->plataforma_id)
                        ->where('trilhas.plataforma_id', $seo->plataforma_id)
                        ->where('trilhas_alunos.aluno_id', $aluno->id)
                        ->orderBy('data_conclusao')
                        ->get();

                    $turmas = \App\Models\TurmaAluno::select('turmas.nome','turmas_alunos.data_conclusao_curso')
                        ->join('turmas','turmas_alunos.turma_id','turmas.id')
                        ->where('turmas_alunos.plataforma_id', $seo->plataforma_id)
                        ->where('turmas.plataforma_id', $seo->plataforma_id)
                        ->where('turmas_alunos.aluno_id', $aluno->id)
                        ->orderBy('data_conclusao_curso')
                        ->get();
                }

            } else {

                $trilhas = \App\Models\TrilhaAluno::select('trilhas.nome','trilhas_alunos.data_conclusao','trilhas_alunos.aluno_id')
                    ->join('trilhas','trilhas_alunos.trilha_id','trilhas.id')
                    ->where('trilhas_alunos.plataforma_id', $seo->plataforma_id)
                    ->where('trilhas.plataforma_id', $seo->plataforma_id)
                    ->where('codigo_certificado', $value)
                    ->whereNotNull('data_conclusao')
                    ->orderBy('data_conclusao')
                    ->get();

                $turmas = \App\Models\TurmaAluno::select('turmas.nome','turmas_alunos.data_conclusao_curso')
                    ->join('turmas','turmas_alunos.turma_id','turmas.id')
                    ->where('turmas_alunos.plataforma_id', $seo->plataforma_id)
                    ->where('turmas.plataforma_id', $seo->plataforma_id)
                    ->where('codigo_certificado', $value)
                    ->whereNotNull('data_conclusao_curso')
                    ->orderBy('data_conclusao_curso')
                    ->get();

                $aluno_id = $trilhas[0]['aluno_id'] ?? $turmas[0]['aluno_id'] ?? null;

                $aluno = \App\Models\User::select('id','nome')->plataforma()->aluno()->where('id', $aluno_id)->first();
            }
        }

        return view('sites-proprios.resgate-vertical.consultar-diploma', compact('seo','aluno','trilhas','turmas','tipo','value') );
    }

    public function sobreNos()
    {
        $seo = PaginaConteudo::plataforma()->ativo()->where('url','/sobre-nos')->first();

        return view('sites-proprios.resgate-vertical.sobre-nos', compact('seo') );
    }

    public function getPlataformaId()
    {
        return session('plataforma_id') ?? \App\Models\Plataforma::dominio()->pluck('id')[0] ?? null;
    }


    public function formatCnpjCpf($value)
    {
      $CPF_LENGTH = 11;
      $cnpj_cpf = preg_replace("/\D/", '', $value);
      
      if (strlen($cnpj_cpf) === $CPF_LENGTH) {
        return preg_replace("/(\d{3})(\d{3})(\d{3})(\d{2})/", "\$1.\$2.\$3-\$4", $cnpj_cpf);
      } 
      
      return preg_replace("/(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/", "\$1.\$2.\$3/\$4-\$5", $cnpj_cpf);
    }

}