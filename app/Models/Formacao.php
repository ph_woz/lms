<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Formacao extends Model
{

    public $timestamps = true;

	protected $table = 'formacoes';

	protected $fillable = [
		'plataforma_id',
		'user_id',
		'tipo_formacao',
		'curso',
		'instituicao',
		'data_inicio',
		'data_fim',
		'status_curso',
		'descricao',
		'estrelas',
	];

    public static  function boot()
    {
        parent::boot(); 
        static::creating(function ($model)
        {
            $model->plataforma_id  = \Auth()->user()->plataforma_id;
        });
    }

    public function scopePlataforma($query)
    {
        return $query->where('plataforma_id', session('plataforma_id') ?? Plataforma::dominio()->pluck('id')[0] ?? null);
    }

}
