<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    use HasFactory;
    
    protected $table = 'banners';

    public $timestamps = false;

    public $fillable = [
    	'plataforma_id',
        'titulo',
        'subtitulo',
        'link',
        'botao_texto',
        'botao_link',
        'overlay',
        'overlay_color',
        'overlay_opacity',
        'ordem',
        'status',
    ];
    
    public function scopePlataforma($query)
    {
        return $query->where('plataforma_id', session('plataforma_id') ?? Plataforma::dominio()->pluck('id')[0] ?? null);
    }

    public function scopeAtivo($query)
    {
        return $query->where('status', 0);
    }
}
