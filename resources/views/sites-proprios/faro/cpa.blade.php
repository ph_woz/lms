<!doctype html>
<html lang="pt-br">
<head>

   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1">

   <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/uikit@3.5.9/dist/css/uikit.min.css" />
   <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
   <link rel="stylesheet" href="{{ asset('css/util.css') }}?v=3">
   <link rel="stylesheet" href="{{ asset('sites-proprios/faro/css/home.css') }}?v=3">

   <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">

   <title>{{ $pagina->seo_title ?? $plataforma->nome }}</title>
   <meta name="robots" content="index, follow">
   <meta name="description" content="{{ $pagina->seo_description ?? null }}"/>
   <meta name="keywords" content="{{ $pagina->seo_keywords ?? null }}"/>
   <link rel="canonical" href="{{ Request::url() }}" />
   <link rel="shortcut icon" href="{{ $plataforma->favicon }}">
   <meta property="og:locale" content="pt_BR" />
   <meta property="og:type" content="website" />
   <meta property="og:title" content="{{ $pagina->seo_title ?? $plataforma->nome }}" />
   <meta property="og:description" content="{{ $pagina->seo_description ?? null }}" />
   <meta property="og:url" content="{{ Request::url() }}" />
   <meta property="og:site_name" content="{{ $plataforma->nome }}" />
   <meta property="og:image" content="{{ $plataforma->logotipo }}" />
   <meta name='author' content='WOZCODE | Pedro Henrique' />

   <style>
      .border-th { border-color: #e3e3e3!important; }
   </style>

</head>
<body class="bg-f4" id="inicio">

   @include('sites-proprios/faro/componentes/header')

   <main>

      <section class="w-100 h-lg-250 background-size-cover background-position-center" style="background-image: url('{{ $plataforma->jumbotron ?? asset('sites-proprios/faro/images/jumbotron.jpeg') }}');">
              <div class="position-relative h-100">
              <div class="background-overlay opacity-08 h-100" style="background: rgba(0,0,0,1);"></div>
               <div class="container h-100">
                 <div class="row container align-items-center h-100 position-relative">
                    <div>
                       <h1 class="text-light font-poppins weight-800 fs-xs-35 fs-md-50 text-shadow-3 col-lg-6" id="tituloCPA">
                          CPA Sociedade
                       </h1>
                    </div>
                 </div>
              </div>
            </div>
      </section>

      <section class="py-5">
         <div class="container">
            
            <div class="bg-white p-5 box-shadow rounded-20">

              <div class="row align-items-center">

                <div class="col-lg-4 mb-4 mb-lg-0">

                  <img src="{{ asset('sites-proprios/faro/images/cpa.png') }}" alt="CPA - FARO" class="img-fluid w-xs-100">

                </div>

                <div class="col-lg-8">

                   <h1 class="font-poppins text-dark weight-700 fs-30">
                      Questionário da Comissão Própria de Avaliação (CPA) para a Sociedade - Faculdade de Roseira
                   </h1>

                   <p class="mb-5 mt-3 font-poppins weight-600 text-dark">
                      A avaliação é um processo fundamental para a qualidade do trabalho desenvolvido nas Instituições de Ensino Brasileiras. Nesse sentido, a CPA (Comissão Própria de Avaliação) da Faculdade de Roseira propõe um questionário com o intuito de coletar opiniões sobre a interação/atuação da Instituição com a sociedade. Os resultados desta avaliação servirão para implementar ações de melhoria continua. Por isso, a sua opinião é de fundamental importância.
                   </p>

                   <a href="/f/formulario-de-cpa/57/t/681" target="_blank" class="bg-faixa bg-hover-gradient fs-17 ls-05 text-center px-3-5 rounded box-shadow text-light weight-700 py-2-5 font-poppins hover-d-none d-xs-block">
                      <span class="me-2">Prosseguir</span>
                      <i class="fas fa-arrow-alt-circle-right"></i>
                   </a>

                  </div>

              </div>

            </div>

         </div>
      </section>

   </main>

   @include('sites-proprios/faro/componentes/footer')

   <a href="https://api.whatsapp.com/send?phone=55{{preg_replace('/\D/', '', $plataforma->whatsapp)}}&text=Olá, vim através do Site {{ $plataforma->nome }}" target="_blank" class="text-white fs-30">
       <div class="fixed-bottom right-0 me-3 mb-3 z-3">
           <div class="square-60 rounded-circle bg-wpp flex-center box-shadow-wpp">
               <i class="fab fa-whatsapp"></i>
           </div>
       </div>
   </a>

   <script src="https://cdn.jsdelivr.net/npm/uikit@3.5.9/dist/js/uikit.min.js"></script>
   <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-U1DAWAznBHeqEIlVSCgzq+c9gqGAJn5c/t99JyeKa9xxaYpSvHU5awsuZVVFIhvj" crossorigin="anonymous"></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>
   <script>

      window.onscroll = function() {myFunction()};

      var header = document.getElementById("menu");
      var sticky = header.offsetTop;

      function myFunction() {
        if (window.pageYOffset > sticky) {
          header.classList.add("fixed-top");
        } else {
          header.classList.remove("fixed-top");
        }
      }
   </script>

</body>
</html>