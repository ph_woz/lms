<?php

namespace App\Exports;

use DB;
use App\Models\User;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class RelatorioExport implements FromView
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function view(): View
    {
        if(request('tipo-perfil-aba') == 'C')
        {        
           $allCampos = $this->getCamposContatos();

        } else {

           $allCampos = $this->getCampos();
        }

    	$query = DB::select( DB::raw(request('queryString')) );

        return view('admin.relatorio.export', [
            'query' => $query,
            'camposSelected' => explode(', ', request('campos')),
            'allCampos' => $allCampos,
        ]);
    }

    public function getCampos()
    {
        $campos = [
            ['select' => 'users.nome',                      'nome' => 'Nome'],
            ['select' => 'users.email',                     'nome' => 'Email'],
            ['select' => 'users.cpf',                       'nome' => 'CPF'],
            ['select' => 'users.id',                        'nome' => 'Matrícula'],
            ['select' => 'users.rg',                        'nome' => 'RG'],
            ['select' => 'users.genero',                    'nome' => 'Gênero'],
            ['select' => 'users.profissao',                 'nome' => 'Profissão'],
            ['select' => 'users.obs',                       'nome' => 'Observações'],
            ['select' => 'users_status',                    'nome' => 'Status de Matrícula'],
            ['select' => 'users.celular',                   'nome' => 'Celular'],
            ['select' => 'users.telefone',                  'nome' => 'Telefone'],
            ['select' => 'endereco.cep',                    'nome' => 'CEP'],
            ['select' => 'endereco.rua',                    'nome' => 'Rua'],
            ['select' => 'endereco.numero',                 'nome' => 'Número'],
            ['select' => 'endereco.bairro',                 'nome' => 'Bairro'],
            ['select' => 'endereco.cidade',                 'nome' => 'Cidade'],
            ['select' => 'endereco.estado',                 'nome' => 'Estado'],
            ['select' => 'endereco.complemento',            'nome' => 'Complemento'],
            ['select' => 'endereco.ponto_referencia',       'nome' => 'Ponto de Referência'],
            ['select' => 'trilha_referencia',               'nome' => 'Trilha'],
            ['select' => 'curso_referencia',                'nome' => 'Curso'],
            ['select' => 'turma_referencia',                'nome' => 'Turma'],
            ['select' => 'avaliacao_referencia',            'nome' => 'Avaliação'],
            ['select' => 'avaliacoes_alunos.nota',          'nome' => 'Nota de Avaliação'],
            ['select' => 'avaliacoes_alunos.n_tentativa',   'nome' => 'N° de Tentativas feitas na Avaliação'],
            ['select' => 'users_data_nascimento',           'nome' => 'Data de Nascimento'],
            ['select' => 'users_data_cadastro',             'nome' => 'Data de Cadastro'],
            ['select' => 'users_data_ultimo_acesso',        'nome' => 'Data de Último Acesso'],
            ['select' => 'data_matricula_trilha',           'nome' => 'Data de Matrícula de Trilha'],
            ['select' => 'data_conclusao_trilha',           'nome' => 'Data de Conclusão de Trilha'],
            ['select' => 'data_matricula_turma',            'nome' => 'Data de Matrícula de Turma'],
            ['select' => 'users_data_conclusao_curso',      'nome' => 'Data de Conclusão de Curso'],
            ['select' => 'data_avaliacao_finalizada',       'nome' => 'Data de Avaliação Finalizada'],
            ['select' => 'data_ultimo_acesso_curso',        'nome' => 'Data de Último Acesso do Conteúdo do Curso'],
            ['select' => 'users_data_docs_recebidas',       'nome' => 'Data de Conclusão de envio de Documentação'],
            ['select' => 'cadastrante_aluno',               'nome' => 'Cadastrante do Aluno'],
            ['select' => 'users_idade',                     'nome' => 'Idade'],
            ['select' => 'unidade_referencia',              'nome' => 'Unidade'],
            ['select' => 'responsaveis_nome',               'nome' => 'Nome do Responsável'],
            ['select' => 'responsaveis_cpf',                'nome' => 'CPF do Responsável'],
            ['select' => 'responsaveis_rg',                 'nome' => 'RG do Responsável'],
            ['select' => 'responsaveis_celular',            'nome' => 'Celular do Responsável'],
            ['select' => 'responsaveis_email',              'nome' => 'Email do Responsável'],
            ['select' => 'responsaveis_data_nascimento',    'nome' => 'Data de Nascimento do Responsável'],
            ['select' => 'responsaveis_profissao',          'nome' => 'Nome do Responsável'],
            ['select' => 'responsaveis_cep',                'nome' => 'CEP do Responsável'],
            ['select' => 'responsaveis_rua',                'nome' => 'Rua do Responsável'],
            ['select' => 'responsaveis_numero',             'nome' => 'Número do Responsável'],
            ['select' => 'responsaveis_bairro',             'nome' => 'Bairro do Responsável'],
            ['select' => 'responsaveis_cidade',             'nome' => 'Cidade do Responsável'],
            ['select' => 'responsaveis_estado',             'nome' => 'Estado do Responsável'],
            ['select' => 'responsaveis_complemento',        'nome' => 'Complemento do Responsável'],
            ['select' => 'responsaveis_ponto_referencia',   'nome' => 'Ponto de Referência do Responsável'],

            ['select' => 'fluxo_caixa.valor_parcela',       'nome' => 'Valor de Parcela'],
            ['select' => 'fluxo_caixa.valor_total',         'nome' => 'Valor Total'],
            ['select' => 'fluxo_caixa.produto_nome',        'nome' => 'Produto'],
            ['select' => 'fluxo_caixa.cadastrante_nome',    'nome' => 'Cadastrante do Financeiro'],
            ['select' => 'fluxo_caixa.vendedor_nome',       'nome' => 'Vendedor'],
            ['select' => 'fluxo_caixa_data_vencimento',     'nome' => 'Data de Vencimento'],
            ['select' => 'fluxo_caixa_data_recebimento',    'nome' => 'Data de Recebimento'],
            ['select' => 'fluxo_caixa_data_cadastro',       'nome' => 'Data de Cadastro'],
            ['select' => 'fluxo_caixa_data_estorno',        'nome' => 'Data de Estorno'],
            ['select' => 'fluxo_caixa.forma_pagamento',     'nome' => 'Forma de Pagamento'],
            ['select' => 'fluxo_caixa.link',                'nome' => 'Link de Pagamento'],
            ['select' => 'fluxo_caixa_n_parcela',           'nome' => 'Parcelas'],
            ['select' => 'fluxo_caixa_status',              'nome' => 'Status de Pagamento'],
            ['select' => 'fluxo_caixa.obs',                 'nome' => 'Observações'],
            ['select' => 'categoria_aluno',                 'nome' => 'Categorias do Aluno'],
        ];

        return $campos;
    }

    public function getCamposContatos()
    {
        $campos = [
            ['select' => 'contatos.assunto',                   'nome' => 'Assunto'],
            ['select' => 'contatos.nome',                      'nome' => 'Nome'],
            ['select' => 'contatos.mensagem',                  'nome' => 'Mensagem'],
            ['select' => 'contatos.email',                     'nome' => 'Email'],
            ['select' => 'contatos.cpf',                       'nome' => 'CPF'],
            ['select' => 'contatos.id',                        'nome' => 'Matrícula'],
            ['select' => 'contatos.rg',                        'nome' => 'RG'],
            ['select' => 'contatos.genero',                    'nome' => 'Gênero'],
            ['select' => 'contatos.profissao',                 'nome' => 'Profissão'],
            ['select' => 'contatos.obs',                       'nome' => 'Observações'],
            ['select' => 'contatos_status',                    'nome' => 'Status de Atendimento'],
            ['select' => 'contatos.celular',                   'nome' => 'Celular'],
            ['select' => 'contatos.telefone',                  'nome' => 'Telefone'],
            ['select' => 'contatos.cep',                       'nome' => 'CEP'],
            ['select' => 'contatos.rua',                       'nome' => 'Rua'],
            ['select' => 'contatos.numero',                    'nome' => 'Número'],
            ['select' => 'contatos.bairro',                    'nome' => 'Bairro'],
            ['select' => 'contatos.cidade',                    'nome' => 'Cidade'],
            ['select' => 'contatos.estado',                    'nome' => 'Estado'],
            ['select' => 'contatos.complemento',               'nome' => 'Complemento'],
            ['select' => 'contatos.ponto_referencia',          'nome' => 'Ponto de Referência'],
            ['select' => 'trilha_referencia',                  'nome' => 'Trilha'],
            ['select' => 'curso_referencia',                   'nome' => 'Curso'],
            ['select' => 'turma_referencia',                   'nome' => 'Turma'],
            ['select' => 'formulario_referencia',              'nome' => 'Formulário'],
            ['select' => 'unidade_referencia',                 'nome' => 'Unidade'],
            ['select' => 'contatos_data_nascimento',           'nome' => 'Data de Nascimento'],
            ['select' => 'contatos_data_cadastro',             'nome' => 'Data de Cadastro'],
            ['select' => 'contatos_idade',                     'nome' => 'Idade'],
            ['select' => 'contatos.anotacao',                  'nome' => 'Anotações'],
            ['select' => 'categoria_contato',                  'nome' => 'Categorias do Contato'],
        ];

        return $campos;
    }
}
