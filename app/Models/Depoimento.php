<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Depoimento extends Model
{
    protected $table = 'depoimentos';

    public $timestamps = false;

    public $fillable = [
    	'plataforma_id',
        'ordem',
    	'nome',
    	'foto',
        'descricao',
        'status',
    ];

    public static  function boot()
    {
        parent::boot(); 
        static::creating(function ($model)
        {
            $model->plataforma_id  = session('plataforma_id') ?? \Auth::user()->plataforma_id ?? 0;
        });
    }

    public function scopePlataforma($query)
    {
        return $query->where('plataforma_id', session('plataforma_id') ?? Plataforma::dominio()->pluck('id')[0] ?? null);
    }

    public function scopeAtivo($query)
    {
        return $query->where('status', 0);
    }

}
