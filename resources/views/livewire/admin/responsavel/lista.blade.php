<div>

    @if(isset($aluno))
        <style>
            .default-show { display: none; }
        </style>
        <h1 class="fs-18 bg-primary p-3 text-light rounded-2 shadow text-shadow-1">
            + adicione Responsáveis ao Aluno<br>
            <a href="{{ route('admin.aluno.info', ['id' => $aluno->id]) }}" class="ms-3 text-light">
                <span class="weight-600">
                    {{ $aluno->nome }}
                </span>
            </a>
        </h1>
        <hr>
    @else
        <style>
            .add_responsaveis_para_aluno_id { display: none; }
        </style>
    @endif

    <form>

        <div class="d-lg-flex mb-3">

            <input wire:model="search" type="search" class="form-control p-form" placeholder="Buscar">

            <select wire:model="status" class="mx-lg-3 my-3 my-lg-0 select-form w-lg-40">
                <option value="0">Ativos</option>
                <option value="S">Desativados</option>
            </select>

            <select wire:model="ordem" class="select-form w-lg-40">
                <option value="assunto">Alfabética</option>
                <option value="desc">Mais novos</option>
                <option value="asc">Mais antigos</option>
            </select>

        </div>

    </form>

    <div class="mt-5 d-flex justify-content-between">

        <div class="align-self-end mb-2">
            <span class="resultados-encontrados">
                {{ count($responsaveis) }} resultados de {{ $countResponsaveis }}.
            </span>
        </div>

        <div class="mb-3">
            <a href="{{ route('admin.responsavel.add') }}" class="btn-add aclNivel2">
                + Adicionar
            </a>
        </div>

    </div>

    <div class="row">
        <div class="col-12">

            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col">Nome</th>
                                    <th scope="col">
                                        <span class="float-end">
                                            Ações
                                        </span>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($responsaveis as $responsavel)
                                    <tr>
                                        <td>{{ $responsavel->nome }}</td>
                                        <td>
                                            <span class="float-end">

                                                <a href="#ModalAddResponsavelParaAluno" data-bs-toggle="modal" wire:click="setResponsavelId({{$responsavel->id}})" class="btn btn-primary add_responsaveis_para_aluno_id">
                                                    + Add
                                                </a>
                                                <a href="{{ route('admin.responsavel.info', ['id' => $responsavel->id]) }}" class="btn bg-base text-white">
                                                    <i class="fa fa-eye"></i>
                                                </a>

                                            </span>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            {{ $responsaveis->links() }}

        </div>
    </div>

    <form wire:submit.prevent="addResponsavelParaAluno">
        <div wire:ignore class="modal fade" id="ModalAddResponsavelParaAluno" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3 class="modal-title" id="exampleModalLabel">Vincular Responsável ao Aluno</h3>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-footer">
            
                        <div class="w-100 d-flex">

                            <select wire:model="add_responsavel_grau_parentesco" class="w-100 me-3 select-form" required>
                                <option value="">Selecione o Grau Parentesco</option>
                                <option value="Pai">Pai</option>
                                <option value="Mãe">Mãe</option>
                                <option value="Irmão">Irmão</option>
                                <option value="Irmã">Irmã</option>
                                <option value="Tia">Tia</option>
                                <option value="Tio">Tio</option>
                                <option value="Avô">Avô</option>
                                <option value="Avó">Avó</option>
                                <option value="Primo">Primo</option>
                                <option value="Prima">Prima</option>
                                <option value="Outro">Outro</option>
                            </select>
                            
                            <button type="submit" class="btn btn-primary">
                                Confirmar
                            </button>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </form>

    @include('livewire.util.toast')
    
</div>