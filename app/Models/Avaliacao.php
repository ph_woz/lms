<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Avaliacao extends Model
{
    use HasFactory;

    protected $table = 'avaliacoes';

	public $timestamps = true;

    public $fillable = [
        'plataforma_id',
        'cadastrante_id',
        'curso_id',
        'turma_id',
        'referencia',
        'condicao_fazer_avaliacao', // concluir-conteudo-do-curso
        'n_tentativas',
        'liberar_gabarito',
        'instrucoes',
        'status',
    ];

    public static  function boot()
    {
        parent::boot(); 
        static::creating(function ($model)
        {
            $model->plataforma_id = session('plataforma_id') ?? \Auth()->user()->plataforma_id ?? Plataforma::dominio()->pluck('id')[0] ?? null;
            $model->cadastrante_id = \Auth()->user()->id ?? null;
        });
    }
    
    public function scopePlataforma($query)
    {
        return $query->where('plataforma_id', session('plataforma_id') ?? Plataforma::dominio()->pluck('id')[0] ?? null);
    }

    public function scopeAtivo($query)
    {
        return $query->where('status', 0);
    }
    
    public static function listaNotas()
    {
        $notas[] = '0,00';
        $notas[] = '0,25';
        $notas[] = '0,50';
        $notas[] = '0,75';
        $notas[] = '1,00';
        $notas[] = '1,25';
        $notas[] = '1,50';
        $notas[] = '1,75';
        $notas[] = '2,00';
        $notas[] = '2,25';
        $notas[] = '2,50';
        $notas[] = '2,75';
        $notas[] = '3,00';
        $notas[] = '3,25';
        $notas[] = '3,50';
        $notas[] = '3,75';
        $notas[] = '4,00';
        $notas[] = '4,25';
        $notas[] = '4,50';
        $notas[] = '4,75';
        $notas[] = '5,00';
        $notas[] = '5,25';
        $notas[] = '5,50';
        $notas[] = '5,75';
        $notas[] = '6,00';
        $notas[] = '6,25';
        $notas[] = '6,50';
        $notas[] = '6,75';
        $notas[] = '7,00';
        $notas[] = '7,25';
        $notas[] = '7,50';
        $notas[] = '7,75';
        $notas[] = '8,00';
        $notas[] = '8,25';
        $notas[] = '8,50';
        $notas[] = '8,75';
        $notas[] = '9,00';
        $notas[] = '9,25';
        $notas[] = '9,50';
        $notas[] = '9,75';
        $notas[] = '10,00';

        return $notas;
    }

    public function getValorNotaMaxima($avaliacao_id)
    {
        $notas = AvaliacaoConfigNota::plataforma()->where('avaliacao_id', $avaliacao_id)->get();
        
        $valoresParaSoma = [];

        foreach($notas as $nota) {

            $valor_nota = floatval(str_replace(',','.', $nota->valor_nota));
            $n_questoes = intval($nota->n_questoes);

            $valoresParaSoma[] = $n_questoes * $valor_nota;
        }
        
        $valor_nota_maxima = array_sum($valoresParaSoma);

        return $valor_nota_maxima;
    }

    public function qntQuestoes($avaliacao_id)
    {
        $quantidadeQuestoes = AvaliacaoConfigNota::plataforma()->where('avaliacao_id', $avaliacao_id)->sum('n_questoes');

        return $quantidadeQuestoes;
    }

}
