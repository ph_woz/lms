<?php

namespace App\Http\Controllers\Aluno;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\FluxoCaixa;

class FinanceiroController extends Controller
{
    public function financeiro()
    {
        $getNomePage = 'Financeiro';
        $listar = request('listar') ?? 'abertas';


        $entradas = FluxoCaixa::select('produto_nome','n_parcelas','n_parcela','registro_id','status','data_vencimento','valor_parcela','link','data_recebimento','data_estorno')
        	->plataforma()
            ->entrada()
        	->where('cliente_id', \Auth::id());

        if($listar == 'abertas')
        {
        	$entradas = $entradas->where('status', 0);
        }

        $entradas = $entradas
        	->orderBy('data_vencimento')
        	->get();

        return view('aluno.financeiro', compact('getNomePage','entradas','listar') );
    }
}