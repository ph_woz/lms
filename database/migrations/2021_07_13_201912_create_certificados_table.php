<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCertificadosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('certificados', function (Blueprint $table) {
            $table->id();
            $table->integer('plataforma_id');
            $table->integer('cadastrante_id')->nullable();

            $table->string('referencia');

            $table->text('modelo')->nullable();
            $table->string('posicao_imagem')->nullable();

            $table->string('nome_posicao_x')->nullable();
            $table->string('nome_posicao_y')->nullable();
            $table->string('nome_fontSize')->nullable();

            $table->text('texto1')->nullable();
            $table->string('texto1_posicao_x')->nullable();
            $table->string('texto1_posicao_y')->nullable();
            $table->string('texto1_fontSize')->nullable();

            $table->text('texto2')->nullable();
            $table->string('texto2_posicao_x')->nullable();
            $table->string('texto2_posicao_y')->nullable();
            $table->string('texto2_fontSize')->nullable();

            $table->string('codigo_posicao_x')->nullable();
            $table->string('codigo_posicao_y')->nullable();
            $table->string('codigo_fontSize')->nullable();
            
            $table->integer('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('certificados');
    }
}
