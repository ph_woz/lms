<?php

namespace App\Http\Livewire\Admin\Avaliacao;

use Livewire\Component;
use App\Models\Avaliacao;
use App\Models\Turma;
use App\Models\User;
use App\Models\Questao;
use App\Models\AvaliacaoQuestao;
use App\Models\AvaliacaoConfigNota;
use DB;
use Illuminate\Support\Facades\Http;

class Info extends Component
{
    public $avaliacao;
    public $turma;
    public $cadastrante;
    public $password_to_delete;
    public $questoes;

    public $remove_questao_id;
    public $remove_questao_enunciado;

    public function mount($avaliacao_id)
    {
        $this->avaliacao = Avaliacao::plataforma()->find($avaliacao_id);

        $this->cadastrante = User::plataforma()->where('id', $this->avaliacao->cadastrante_id)->first();

        $this->turma = Turma::plataforma()->where('id', $this->avaliacao->turma_id)->first();
    }

    public function render()
    {
        $this->questoes = DB::table('avaliacoes')
            ->join('avaliacoes_questoes','avaliacoes.id','avaliacoes_questoes.avaliacao_id')
            ->join('questoes','avaliacoes_questoes.questao_id','questoes.id')
            ->where('avaliacoes.id', $this->avaliacao->id)
            ->where('avaliacoes_questoes.avaliacao_id', $this->avaliacao->id)
            ->where('avaliacoes.plataforma_id', session('plataforma_id'))
            ->where('avaliacoes_questoes.plataforma_id', session('plataforma_id'))
            ->select(
                'questoes.id',
                'questoes.enunciado',
                'avaliacoes_questoes.valor_nota',
            )->get();

        $valor_nota_maxima = $this->avaliacao->getValorNotaMaxima($this->avaliacao->id);

        $configsNotas = AvaliacaoConfigNota::plataforma()->where('avaliacao_id', $this->avaliacao->id)->get();

        return view('livewire.admin.avaliacao.info', 
            [
                'questoes'          => $this->questoes,
                'valor_nota_maxima' => $valor_nota_maxima,
                'configsNotas'      => $configsNotas,
            ] 
        );
    }

    public function setRemoverQuestaoDaAvaliacao($questao_id)
    {
        $this->remove_questao_id = $questao_id;
        $this->remove_questao_enunciado = Questao::plataforma()->where('id', $questao_id)->pluck('enunciado')[0] ?? null;
    }

    public function removerQuestao()
    {
        if(\Auth::attempt(['id' => \Auth::id(), 'password' => $this->password_to_delete, 'status' => 0]))
        {
            AvaliacaoQuestao::plataforma()->where('questao_id', $this->remove_questao_id)->delete();

            \App\Models\Log::create([
                'tipo' => 'avaliacao',
                'ref'  => 'Avaliação: ' . $this->avaliacao->referencia . ' teve uma questão removida.',
            ]);

            session()->flash('success-livewire', 'Questão removida da avaliação com êxito.');
            
            $this->dispatchBrowserEvent('toast');
            $this->dispatchBrowserEvent('dissmiss-modal');

        } else {

            session()->flash('danger-livewire', 'Senha incorreta.');

            $this->dispatchBrowserEvent('toast');
        } 
    }

    public function removerTodasQuestoes()
    {
        if(\Auth::attempt(['id' => \Auth::id(), 'password' => $this->password_to_delete, 'status' => 0]))
        {
            $deleteAndReturnCountDeleteds = AvaliacaoQuestao::plataforma()->where('avaliacao_id', $this->avaliacao->id)->delete();

            \App\Models\Log::create([
                'tipo' => 'avaliacao',
                'ref'  => 'Avaliação: ' . $this->avaliacao->referencia . ' teve todas as questões removidas. ('.$deleteAndReturnCountDeleteds.')',
            ]);

            session()->flash('success-livewire', 'Todas as Questões da avaliação foram removidas com êxito.');
            
            $this->dispatchBrowserEvent('toast');
            $this->dispatchBrowserEvent('dissmiss-modal');

        } else {

            session()->flash('danger-livewire', 'Senha incorreta.');

            $this->dispatchBrowserEvent('toast');
        } 
    }

    public function delete()
    {
        if(\Auth::attempt(['id' => \Auth::id(), 'password' => $this->password_to_delete, 'status' => 0]))
        {
            AvaliacaoQuestao::plataforma()->where('avaliacao_id', $this->avaliacao->id)->delete();
            AvaliacaoConfigNota::plataforma()->where('avaliacao_id', $this->avaliacao->id)->delete();
            
            $deleteAndReturnCountDeleteds = \App\Models\AvaliacaoAluno::plataforma()->where('avaliacao_id', $this->avaliacao->id)->delete();
            
            \App\Models\AvaliacaoAlunoResposta::plataforma()->where('avaliacao_id', $this->avaliacao->id)->delete();

            $avaliacao = Avaliacao::plataforma()->where('id', $this->avaliacao->id)->first();

            \App\Models\Log::create([
                'tipo' => 'avaliacao',
                'ref'  => 'Avaliação: ' . $avaliacao->referencia . ' foi deletada. Quantidade de Provas feitas deletadas: ' . $deleteAndReturnCountDeleteds,
            ]);

            $avaliacao->delete();

            session()->flash('success', 'Avaliacão deletada com êxito.');
            
            return redirect(route('admin.avaliacao.lista'));

        } else {

            session()->flash('danger-livewire', 'Senha incorreta.');

            $this->dispatchBrowserEvent('toast');
        }
    }
}
