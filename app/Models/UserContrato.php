<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserContrato extends Model
{
    use HasFactory;

    protected $table = 'users_contratos';

    public $timestamps = false;

    protected $fillable = [
        'plataforma_id',
        'cadastrante_id',
     	'contrato_id',
        'user_id',
        'referencia',
     	'pdf',
        'pdf_assinado',
     	'data_assinatura',
        'foto_assinatura',
    ];

    public static  function boot()
    {
        parent::boot(); 
        static::creating(function ($model)
        {
            $model->plataforma_id  = session('plataforma_id') ?? \Auth()->user()->plataforma_id ?? Plataforma::dominio()->pluck('id')[0] ?? null;
            $model->cadastrante_id = \Auth()->user()->id;
        });
    }
    
    public function scopePlataforma($query)
    {
        return $query->where('plataforma_id', session('plataforma_id') ?? Plataforma::dominio()->pluck('id')[0] ?? null);
    }
}
