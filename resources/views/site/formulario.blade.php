@extends('layouts.site')

@section('seo_title', $formulario->referencia . ' - ' . $nomePlataforma)

@section('css')
<style>
    body { background-image: url({{ asset('images/bg-login.png') }}) }
</style>
@endsection

@section('content')
<form method="POST" enctype="multipart/form-data" class="container-fluid px-xl-5 py-5 mt-5" id="formContainer">
@csrf

	<section class="bg-white p-lg-5 p-4 my-5 rounded-2 shadow">

		<div id="formDescricao">
			{!! nl2br($formulario->texto) !!}
		</div>

		<hr/>

        <div class="row mt-4">

            @foreach($perguntas as $pergunta)
                
                @if($pergunta->tipo_input == 'select')
                    <div class="mb-4 col-lg-3">
                        <label>{{ $pergunta->pergunta }}</label>
                        <select name="{{ $pergunta->validation ?? 'respostas['.$pergunta->id.']' }}" class="{{ $pergunta->validation ?? null }} select-form" @if($pergunta->obrigatorio == 'S') required @endif>
                            <option value="" selected disabled>Selecione</option>
                            @foreach(unserialize($pergunta->options) as $option)
                                <option value="{{ $option }}" @if(old($pergunta->validation) == $option) selected @endif>{{ $option }}</option>
                            @endforeach
                        </select> 
                    </div>
                @endif
                
                @if($pergunta->tipo_input == 'text')
                <div class="mb-4 col-lg-3">
                    <label>{{ $pergunta->pergunta }}</label>
                    <input type="text" name="{{ $pergunta->validation ?? 'respostas['.$pergunta->id.']' }}" class="{{ $pergunta->validation ?? null }} form-control p-form" @if($pergunta->obrigatorio == 'S') required @endif>
                </div>
                @endif
                
                @if($pergunta->tipo_input == 'radio')
                <div class="mb-4 col-12">
                    <label class="d-block mb-1">{{ $pergunta->pergunta }}</label>
                    @foreach(unserialize($pergunta->options) as $option)
                        <label for="opt{{$loop->index}}p{{$pergunta->id}}" class="mb-1 d-flex align-items-center">
                            <input type="radio" class="square-15" id="opt{{$loop->index}}p{{$pergunta->id}}" name="{{ $pergunta->validation ?? 'respostas['.$pergunta->id.']' }}" value="{{ $option }}" @if($pergunta->obrigatorio == 'S') required @endif>
                            <span class="ms-2">{!! $option !!}</span>
                        </label>
                    @endforeach
                </div>
                @endif

                @if($pergunta->tipo_input == 'checkbox')
                <div class="mb-4 col-12">
                    <label class="d-block mb-1">{{ $pergunta->pergunta }}</label>
                    @foreach(unserialize($pergunta->options) as $option)
                        <label for="opt{{$loop->index}}p{{$pergunta->id}}" class="mb-1 d-flex align-items-center">
                            <input type="checkbox" class="square-15" id="opt{{$loop->index}}p{{$pergunta->id}}" name="{{ $pergunta->validation ?? 'respostas['.$pergunta->id.']' }}" value="{{ $option }}">
                            <span class="ms-2">{!! $option !!}</span>
                        </label>
                    @endforeach
                </div>
                @endif

                @if($pergunta->tipo_input == 'file')
                <div class="mb-4 colIndex{{$loop->index}} col-lg-3">
                    <label class="d-block mb-0">{{ $pergunta->pergunta }}</label>
                    <input type="file" @if($pergunta->validation) name="{{ $pergunta->validation }}" @else name="files[{{$pergunta->pergunta}}]" @endif class="file-form" @if($pergunta->obrigatorio == 'S') required @endif>
                </div>
                @endif
                                        
            @endforeach

        </div>

        <input type="text" name="check" class="h-0 border-0 outline-0 float-end">

        <button type="submit" name="sent" value="ok" id="formBtnEnviar" class="mt-4 btn btn-primary weight-600 shadow px-4 py-2">
        	Enviar
        </button>

	</section>

</form>

<div class="modal fade" id="modalCallback" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalCenterTitle">Inscrição enviada com sucesso!</h5>
      </div>
      <div class="modal-body">
        
            {!! $formulario->texto_retorno !!}

      </div>
      <div class="modal-footer">
        <a href="{{ $formulario->link_retorno }}" class="btn btn-primary weight-600 shadow px-4 py-2">
            {{ $formulario->texto_botao_retorno ?? 'Retornar' }}
        </a>
      </div>
    </div>
  </div>
</div>

@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.js"></script>

@if(session('success') && $formulario->texto_retorno != null)
<script>
    $(document).ready(function() {
        $("#modalCallback").modal('show');
    });
</script>
@endif

<script>

    $(document).ready(function() {

        function limpa_formulário_cep() {

            $(".logradouro").val("");
            $(".bairro").val("");
            $(".cidade").val("");
            $(".estado").val("");
        }
        

        $(".cep").keyup(function() {

            var cep = $(this).val().replace(/\D/g, '');

            if (cep != "" && cep.length >= 8) {

                var validacep = /^[0-9]{8}$/;

                if(validacep.test(cep)) {

                    $(".logradouro").val("...");
                    $(".bairro").val("...");
                    $(".cidade").val("...");
                    $(".estado").val("...");

                    $.getJSON("https://viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {

                        if (!("erro" in dados)) {
                            $(".logradouro").val(dados.logradouro);
                            $(".bairro").val(dados.bairro);
                            $(".cidade").val(dados.localidade);
                            $(".estado").val(dados.uf);
                        } 
                        else {
                           
                            limpa_formulário_cep();
                        }
                    });
                } 
                else {
                    
                    limpa_formulário_cep();
                    
                }
            }
            else {
              
                limpa_formulário_cep();
            }
        });
    });

</script>

<script>
	$(".email").attr('type','email');
	$(".password").attr('type','password');
    $(".cpf").mask('000.000.000-00').attr('minlength','14');
    $(".data_nascimento").mask('00/00/0000');
    $(".cep").mask('00000-000');
    $(".estado, .uf").attr('minlength','2').attr('maxlength','2');
    $(".numero").mask('000000000');
    $(".password").val('');

    $(document).on("focus", ".celular, .telefone", function() { 
      jQuery(this)
          .mask("(99) 9999-99999")
          .change(function (event) {  
              target = (event.currentTarget) ? event.currentTarget : event.srcElement;  
              phone = target.value.replace(/\D/g, '');
              element = $(target);  
              element.unmask();  
              if(phone.length > 10) {  
                  element.mask("(99) 99999-9999");  
              } else {  
                  element.mask("(99) 9999-9999?9");  
              }  
      });
    });

    $(document).ready(function() {

        function limpa_formulário_cep() {

            $(".logradouro").val("");
            $(".bairro").val("");
            $(".cidade").val("");
            $(".estado").val("");
        }
        

        $(".cep").keyup(function() {

            var cep = $(this).val().replace(/\D/g, '');

            if (cep != "" && cep.length >= 8) {

                var validacep = /^[0-9]{8}$/;

                if(validacep.test(cep)) {

                    $(".logradouro").val("...");
                    $(".bairro").val("...");
                    $(".cidade").val("...");
                    $(".estado").val("...");

                    $.getJSON("https://viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {

                        if (!("erro" in dados)) {
                            $(".logradouro").val(dados.logradouro);
                            $(".bairro").val(dados.bairro);
                            $(".cidade").val(dados.localidade);
                            $(".estado").val(dados.uf);
                        } 
                        else {
                           
                            limpa_formulário_cep();
                        }
                    });
                } 
                else {
                    
                    limpa_formulário_cep();
                    
                }
            }
            else {
              
                limpa_formulário_cep();
            }
        });
    });

    $('.estado, .uf').keyup(function(){
        $(this).val($(this).val().toUpperCase());
    });

</script>
@endpush

@endsection
