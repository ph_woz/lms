<!doctype html>
<html lang="pt-br">
<head>

  <!-- Google tag (gtag.js) -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=G-MREB450HCT"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'G-MREB450HCT');
  </script>
  
   {!! $plataforma->scripts_start_head !!}

   <meta charset="utf-8">
   <meta name="viewport" content="width=device-width, initial-scale=1">

   <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/uikit@3.5.5/dist/css/uikit.min.css" />
   <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
   <link rel="stylesheet" href="{{ asset('sites-proprios/cefaep/css/home.css') }}?v=4">
   <link rel="stylesheet" href="{{ asset('css/util.css') }}?v=4">

   <title>{{ $seo->title ?? $plataforma->nome }}</title>
   <meta name="robots" content="index, follow">
   <meta name="description" content="{{ $seo->description ?? null }}"/>
   <meta name="keywords" content="{{ $seo->keywords ?? null }}"/>
   <link rel="canonical" href="{{ Request::url() }}" />
   <link rel="shortcut icon" href="{{ $plataforma->favicon }}">
   <meta property="og:locale" content="pt_BR" />
   <meta property="og:type" content="website" />
   <meta property="og:title" content="{{ $seo->title ?? $plataforma->nome }}" />
   <meta property="og:description" content="{{ $seo->description ?? null }}" />
   <meta property="og:url" content="{{ Request::url() }}" />
   <meta property="og:site_name" content="{{ $plataforma->nome }}" />
   <meta property="og:image" content="{{ $plataforma->logotipo }}" />
   <meta name='author' content='WOZCODE | Pedro Henrique' />

   {!! $plataforma->scripts_final_head !!}

</head>
<body id="inicio">

   {!! $plataforma->scripts_start_body !!}

   <header>
      <nav class="navbar navbar-expand-lg navbar-light bg-light header">
         <div class="container-fluid px-xl-7">
            <a class="navbar-brand" href="#">
               <img src="{{ $plataforma->logotipo }}" class="img-fluid" width="70">
            </a>
            <button class="navbar-toggler outline-0 border-0" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
               <span class="fa fa-bars fs-25"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
               <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
                  <li class="nav-item">
                     <a class="nav-link" href="#vantagens">Vantagens</a>
                  </li>
                  <li class="nav-item">
                     <a class="nav-link" href="#como-funciona">Como funciona</a>
                  </li>
                  <li class="nav-item">
                     <a class="nav-link" href="#faq">FAQ</a>
                  </li>
                  <li class="nav-item">
                     <a class="nav-link" href="#planos">Planos</a>
                  </li>
                  <li class="nav-item">
                     <a class="nav-link" href="#footer">Contato</a>
                  </li>
                  <li class="nav-item">
                     <a class="nav-link link-areadoaluno" href="/login">Área do Aluno</a>
                  </li>
                  <li class="nav-item">
                     <a class="nav-link link-matriculese" href="#colForm">Matricule-se</a>
                  </li>
               </ul>
            </div>
         </div>
      </nav>
   </header>

   <main>

      <section class="h-lg-100vh pb-5 pb-lg-0 background-size-cover background-position-center" style="background-image: url('https://www.cnead.com.br/site/wp-content/uploads/2020/09/banner-01.jpg');">
         <div class="container-fluid px-xl-7 h-100">
            <div class="row align-items-center h-100 pt-5">

               <div class="col-lg-7 pt-lg-4">

                  <span class="c-banner__title-lead">Supletivo EJA</span>
                  <h1 class="c-banner__title me-5">Supletivo Online <b>Reconhecido</b></h1>

                  <div class="mt-4">
                     <a href="tel:08000420075" class="btnCall box-shadow-0">
                        <div class="d-flex justify-content-between">
                           <span class="text-shadow-2">Ligue agora <span class="d-xs-none">gratuitamente</span></span>
                           <span class="text-shadow-2">0800 042 0075</span>
                           <div>
                              <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-telephone-inbound" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                 <path fill-rule="evenodd" d="M3.925 1.745a.636.636 0 0 0-.951-.059l-.97.97c-.453.453-.62 1.095-.421 1.658A16.47 16.47 0 0 0 5.49 10.51a16.47 16.47 0 0 0 6.196 3.907c.563.198 1.205.032 1.658-.421l.97-.97a.636.636 0 0 0-.06-.951l-2.162-1.682a.636.636 0 0 0-.544-.115l-2.052.513a1.636 1.636 0 0 1-1.554-.43L5.64 8.058a1.636 1.636 0 0 1-.43-1.554l.513-2.052a.636.636 0 0 0-.115-.544L3.925 1.745zM2.267.98a1.636 1.636 0 0 1 2.448.153l1.681 2.162c.309.396.418.913.296 1.4l-.513 2.053a.636.636 0 0 0 .167.604L8.65 9.654a.636.636 0 0 0 .604.167l2.052-.513a1.636 1.636 0 0 1 1.401.296l2.162 1.681c.777.604.849 1.753.153 2.448l-.97.97c-.693.693-1.73.998-2.697.658a17.471 17.471 0 0 1-6.571-4.144A17.47 17.47 0 0 1 .639 4.646c-.34-.967-.035-2.004.658-2.698l.97-.969zM15.854.146a.5.5 0 0 1 0 .708L11.707 5H14.5a.5.5 0 0 1 0 1h-4a.5.5 0 0 1-.5-.5v-4a.5.5 0 0 1 1 0v2.793L15.146.146a.5.5 0 0 1 .708 0z"></path>
                              </svg>
                           </div>
                        </div>
                    </a>
                  </div>

                </div>

               <div class="col-lg-5 pt-5" id="colForm">

                  <div class="card border-0 shadow">
                     <div class="card-header">
                        <p class="mb-0 font-nunito weight-800 text-center fs-30 text-light text-shadow-3">
                           <span class="d-none d-md-block">
                              Dúvidas? Solicite em contato!
                           </span>
                           <span class="d-md-none d-block fs-25">
                              Solicite nosso contato
                           </span>
                        </p>
                     </div>
                     <div class="card-body box-shadow">
                        <form method="POST" id="formSolicitarContato">
                        @csrf

                           <input type="hidden" name="assunto" value="Solicitar Contato">

                           @if(session('success_prematricula'))
                              <div class='alert alert-success'><b>Sucesso!</b> {{ session('success_prematricula') }}</div>
                           @endif

                           <input type="hidden" name="assunto" value="Solicitar Contato">
                           <input type="text" name="nome" class="form-contact mb-2" placeholder="Nome" required>
                           <input type="tel" name="celular" class="form-contact mb-2" placeholder="WhatsApp" required>
                           <input type="text" name="cidade" class="form-contact mb-2" placeholder="Cidade" required>
                           <input type="email" name="email" class="form-contact mb-2" placeholder="Email" required>
               
                           <div class="my-3">
                              <label class="cursor-pointer font-poppins fs-14 text-white">
                                 <input type="checkbox" class="" name="consentimento" value="1" required> 
                                 Concordo que os dados pessoais fornecidos acima serão utilizados para envio de conteúdo informativo, analítico e publicitário sobre produtos, serviços e assuntos gerais, nos termos da Lei Geral de Proteção de Dados.
                              </label>
                           </div>

                           <input type="text" name="check" class="invisible h-0px d-block" placeholder="Não preencha este campo.">
                           <input type="submit" value="Enviar" name="sentContato" class="btnEntrarEmContato text-shadow-3">
                  
                        </form>
                     </div>
                  </div>

               </div> 

            </div>
         </div>
      </section>

      <section class="quem-somos pt-5 pt-lg-0">
         <div class="container-fluid px-xl-7 h-100">
            <div class="row align-items-end h-100">

               <div class="col-lg-6 pb-5">
                  
                  <div>
                     <span class="title-lead title-lead--detail">Ensino à Distância</span>
                     <img src="{{ asset('sites-proprios/cefaep/images/dots-blue.png') }}">
                  </div>

                  <h1 class="title m-quem-somos__title font-saira-condensed text-uppercase">
                     Como funciona nosso <span>Supletivo Reconhecido</span>
                  </h1>

                  <div class="m-quem-somos__content">
                     <p>
                        A CEFAEP tem o ideal de promover a possibilidade de pessoas concluírem o Fundamental e Ensino Médio que acabou ficando pelo caminho. Além disso, o diploma que é reconhecido pelo MEC no final dos estudos terá um papel fundamental em abrir novas portas.
                     </p>
                  </div>

                  <div>
                     <a href="tel:08000420075" class="btn btn-primary hover-d-none" title="Conheça o que podemos fazer por você">
                        <span>Ligue agora </span> <span class="ms-1 d-xs-none">gratuitamente</span>
                        <span class="ms-2">0800 042 0075</span>
                     </a>
                  </div>

               </div>

               <div class="col-lg-6 d-none d-md-block">
                  <img alt="Uma mulher e um homem confiantes para os estudos" class="img-fluid" src="{{ asset('sites-proprios/cefaep/images/2-pessoas.png') }}">
               </div>

            </div>
         </div>
      </section>  

      <section id="vantagens" class="m-vantagens text-center py-5"> 
         <div class="container-fluid px-xl-7 mt-4 pb-lg-5">

            <span class="title-lead">Vantagens especiais</span>

            <h2 class="title title--small font-saira-condensed mb-4 px-4">Sabe quando você pode começar? <span>Agora!</span></h2>
            <img src="{{ asset('sites-proprios/cefaep/images/dots-blue.png') }}">

            <div class="row mt-4-5">

               <div class="col-lg-4">
                  <div class="m-vantagens-item bg-white p-4 mb-4 box-shadow">
                     <div class="row align-items-center h-100">
                        <div class="col-md-12">
                           <img alt="Reconhecida pelo MEC"  class="img-fluid" src="{{ asset('sites-proprios/cefaep/images/icon1.png') }}" alt="Reconhecida pelo MEC" width="175">
                           <span class="vantangens-span d-block mt-3">Reconhecida pelo MEC</span>
                        </div>
                     </div>
                  </div>
               </div>

               <div class="col-lg-4">
                  <div class="m-vantagens-item bg-white p-4 mb-4 box-shadow">
                     <div class="row align-items-center h-100">
                        <div class="col-md-12">
                           <img alt="Diploma válido em todo território Nacional" class="img-fluid" src="{{ asset('sites-proprios/cefaep/images/icon2.png') }}" alt="Diploma válido em todo território Nacional">
                           <span class="vantangens-span d-block mt-3">Diploma válido em todo território Nacional</span>
                        </div>
                     </div>
                  </div>
               </div>

               <div class="col-lg-4">
                  <div class="m-vantagens-item bg-white p-4 mb-4 box-shadow">
                     <div class="row align-items-center h-100">
                        <div class="col-md-12">
                           <img alt="Faça as provas quando estiver preparado" class="img-fluid" src="https://www.cnead.com.br/site/wp-content/uploads/2020/09/icon-vantagens-03.jpg" alt="Faça as provas quando estiver preparado">
                           <span class="vantangens-span d-block mt-3">Faça as provas quando estiver preparado</span>
                        </div>
                     </div>
                  </div>
               </div>

               <div class="col-lg-4">
                  <div class="m-vantagens-item bg-white p-4 mb-4 box-shadow">
                     <div class="row align-items-center h-100">
                        <div class="col-md-12">
                           <img alt="Estude em seu próprio horário" class="img-fluid" src="https://www.cnead.com.br/site/wp-content/uploads/2020/09/icon-vantagens-04.jpg" alt="Estude em seu próprio horário">
                           <span class="vantangens-span d-block mt-3">Estude em seu próprio horário</span>
                        </div>
                     </div>
                  </div>
               </div>

               <div class="col-lg-4">
                  <div class="m-vantagens-item bg-white p-4 mb-4 box-shadow">
                     <div class="row align-items-center h-100">
                        <div class="col-md-12">
                           <img alt="Certificado válido pelo Conselho Estadual de Educação." class="img-fluid" src="https://www.cnead.com.br/site/wp-content/uploads/2020/09/icon-vantagens-05.jpg" alt="Certificado válido pelo Conselho Estadual de Educação.">
                           <span class="vantangens-span d-block mt-3">Certificado válido pelo Conselho Estadual de Educação.</span>
                        </div>
                     </div>
                  </div>
               </div>

               <div class="col-lg-4">
                  <div class="m-vantagens-item bg-white p-4 mb-4 box-shadow">
                     <div class="row align-items-center h-100">
                        <div class="col-md-12">
                           <img alt="Escola 100% OnLine" class="img-fluid" src="https://www.cnead.com.br/site/wp-content/uploads/2020/09/icon-vantagens-06.jpg" alt="Escola 100% OnLine">
                           <span class="vantangens-span d-block mt-3">Escola 100% OnLine</span>
                        </div>
                     </div>
                  </div>
               </div>

            </div>

         </div>
      </section>

      <section class="background-size-cover my-lg-5" style="background-image: url('{{ asset('sites-proprios/cefaep/images/bg-orange.jpg') }}');">
         <div class="container-fluid px-xl-7">
            <div class="row">

               <div class="col-lg-6 py-5 z-3">

                  <div>
                     <span class="title-lead title-lead--detail text-light me-4">Conclua seus estudos</span>
                     <img src="{{ asset('sites-proprios/cefaep/images/dots-white.png') }}">
                  </div>

                  <h3 class="title title--big c-chamada__title text-white weight-700 text-uppercase font-saira-condensed">
                     Comece hoje a <br>
                     estudar sem <br>
                     burocracia!
                  </h3>

                  <div class="c-chamada__btns mt-4">
                     <a href="#colForm" class="w-xs-100 me-lg-4 me-2 btn btn-white btn-white--big hover-d-none" title="Cadastre-se agora e aproveite todas as vantagens">Quero me matricular</a>
                     <a href="#faq" class="w-xs-100 btn btn-outline-white hover-d-none" title="Tire todas as suas dúvidas em nossas perguntas frequentes">Ainda tenho dúvidas</a>
                  </div>

               </div>

               <div class="col-lg-6 d-none d-lg-block text-left">

                  <div class="d-flex justify-content-center" style="margin-top: -100px;">
                     <img src="{{ asset('sites-proprios/cefaep/images/2-pessoas-no-bg-laranja.png') }}" style="max-width: none;">
                  </div>

               </div>

            </div>
         </div>
      </section>

      <section id="como-funciona" class="py-5"> 
         <div class="container-fluid px-xl-7">

            <div class="text-center">
               <span class="title-lead">Conheça</span>

               <h2 class="title title--small font-saira-condensed mb-4 px-4">COMO FUNCIONA</h2>
               <img src="{{ asset('sites-proprios/cefaep/images/dots-blue.png') }}">
            </div>

            <div class="row align-items-center pt-5">

               <div class="col-lg-6 mb-4">

                  <div>
                     <h2 class="p-0 m-0 font-saira-condensed weight-600 color-title">
                        O QUE É SUPLETIVO EJA EAD?
                     </h2>
                     <p>
                        Metodologia que visa auxiliar as pessoas que não conseguiram terminar seus estudos básicos, e o ensino ocorre de forma didática acelerada.
                     </p>
                     <hr class="d-md-none"/>
                  </div>

               </div>

               <div class="col-lg-6 d-xs-none mb-4">

                  <div>
                     <img src="{{ asset('sites-proprios/cefaep/images/1.png') }}" alt="Imagem Como Funciona" class="img-fluid rounded box-shadow">
                  </div>

               </div>

               <div class="col-lg-6 d-xs-none mb-4">

                  <div>
                     <img src="{{ asset('sites-proprios/cefaep/images/2.png') }}" alt="Imagem Como Funciona" class="img-fluid rounded box-shadow">
                  </div>

               </div>

               <div class="col-lg-6 mb-4">

                  <div>
                     <h2 class="p-0 m-0 font-saira-condensed weight-600 color-title">
                        DETALHES SOBRE SUPLETIVO EJA EAD
                     </h2>
                     <p>
                        É uma metodologia de ensino que foi criada para auxiliar as pessoas que não conseguiram completar, abandonaram ou não tiveram acesso à educação formal no período da idade apropriada.
                        <br><br>
                        Seu principal objetivo é realizar a inclusão social, promovendo nossos jovens e adultos ao ensino-aprendizagem faltante, ao final de cada etapa de estudos, o participante se submete a uma prova e, alcançando a média mínima exigida, obtém a certificação de conclusão daquela etapa educacional. O Supletivo EJA EAD é classificado em 2 etapas:
                     </p>
                     <hr class="d-md-none"/>
                  </div>

               </div>

               <div class="col-lg-6 mb-4">

                  <div>
                     <h2 class="p-0 m-0 font-saira-condensed weight-600 color-title">
                        ETAPA DO ENSINO FUNDAMENTAL
                     </h2>
                     <p>
                        Esta etapa consiste em inserir o aluno no ensino-aprendizagem do 1º ao 9º ano do ensino fundamental. Esta primeira etapa é destinada aos jovens a partir dos 15 anos de idade.
                        <br>
                        Nesta etapa do Ensino Fundamental, os alunos aprendem Língua Portuguesa, Matemática, História, Geografia, Ciências, Inglês, Artes e Educação Física.
                     </p>
                     <hr class="d-md-none"/>
                  </div>

               </div>

               <div class="col-lg-6 d-xs-none mb-4">

                  <div>
                     <img src="{{ asset('sites-proprios/cefaep/images/3.png') }}" alt="Imagem Como Funciona" class="img-fluid rounded box-shadow">
                  </div>

               </div>

               <div class="col-lg-6 d-xs-none mb-4">

                  <div>
                     <img src="{{ asset('sites-proprios/cefaep/images/4.png') }}" alt="Imagem Como Funciona" class="img-fluid rounded box-shadow">
                  </div>

               </div>

               <div class="col-lg-6 mb-4">

                  <div>
                     <h2 class="p-0 m-0 font-saira-condensed weight-600 color-title">
                        ETAPA DO ENSINO MÉDIO
                     </h2>
                     <p>
                        Esta etapa consiste em inserir o aluno no ensino-aprendizagem do 1º ao 3º ano do ensino médio. Esta segunda etapa é destinada aos jovens a partir dos 18 anos e seis meses de idade.
                        <br><br>
                        Nesta segunda etapa, a grade curricular é composta por Língua Portuguesa, Matemática, História, Geografia, Ciências, Inglês, Artes, Educação Física. Química, Física, Sociologia e Filosofia.
                     </p>
                     <hr class="d-md-none"/>
                  </div>

               </div>

            </div>

         </div>
      </section>

      <section id="alunos-formados" class="py-lg-5 mb-5 mb-lg-0"> 
         <div style="background: #080f20;" class="py-5 py-lg-2">
            <div class="container-fluid px-xl-7 mt-lg-4 mb-lg-5">

               <div class="mb-5 text-center">

                  <span class="title-lead title-lead--detail me-lg-4 me-2">SUCESSO</span>
                  <img src="{{ asset('sites-proprios/cefaep/images/dots-blue.png') }}">

                  <h2 class="title text-light m-como-funciona__title text-uppercase font-saira-condensed">ALUNOS <span>FORMADOS</span></h2>

               </div>

               <div uk-slider="autoplay: true; autoplay-interval: 3000;">

                  <div class="uk-position-relative z-1 uk-visible-toggle uk-light" tabindex="-1">
                       
                     <ul class="uk-slider-items uk-child-width-1-4@s uk-grid">
                        @foreach($fotosAlunosFormados as $foto)
                        <li>
                           <div class="uk-card uk-card-default rounded-1 shadow">
                              <div class="uk-card-media-top text-center">
                                 <img src="{{ $foto }}" alt="Aluno Formado CEFAEP" class="px-2 h-250 object-fit-cover">
                              </div>
                           </div>
                        </li>
                        @endforeach
                     </ul>

                     <a class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous uk-slider-item="previous"></a>
                     <a class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-next uk-slider-item="next"></a>

                  </div>

               </div>

            </div>
         </div>
      </section>

      <section id="faq" class="py-lg-5"> 
         <div class="container-fluid px-xl-7 mt-lg-2 mb-lg-4">
            <div class="row align-items-center">

               <div class="col-lg-8 col-12">

                  <span class="title-lead title-lead--detail me-lg-4 me-2">AULAS 100% ONLINE </span>
                  <img src="{{ asset('sites-proprios/cefaep/images/dots-blue.png') }}">

                  <h2 class="title m-como-funciona__title text-uppercase font-saira-condensed">DÚVIDAS <span>FREQUENTES</span></h2>

                  <div class="m-como-funciona__content">
                     <p>Entre em contato conosco, faça sua matrícula, acesse o sistema para estudar e faça a prova. Pronto, agora é só receber seu certificado!</p>
                  </div>

                  <div class="pt-4-5">

                     <div>
                        <a href="#collapseFaq0" data-bs-toggle="collapse" href="#collapseFaq0" class="clickFaq text-d-none hover-d-none color-333 font-poppins weight-600">
                           <i class="fas fa-caret-right"></i>
                           Quando poderei utilizar a Área do Aluno?
                        </a>
                        <p class="collapse mb-0 mt-1 font-poppins text-dark" id="collapseFaq0">
                          Após a confirmação de pagamento, o seu acesso já estará totalmente liberado.
                        </p>  
                        <hr/>
                     </div>
                     <div>
                        <a href="#collapseFaq1" data-bs-toggle="collapse" href="#collapseFaq1" class="clickFaq text-d-none hover-d-none color-333 font-poppins weight-600">
                           <i class="fas fa-caret-right"></i>
                           Qual a idade mínima para fazer o supletivo?
                        </a>
                        <p class="collapse mb-0 mt-1 font-poppins text-dark" id="collapseFaq1">
                           A idade mínima para se fazer o supletivo é de 18 anos e 6 meses.
                        </p>  
                        <hr/>
                     </div>
                     <div>
                        <a href="#collapseFaq2" data-bs-toggle="collapse" href="#collapseFaq2" class="clickFaq text-d-none hover-d-none color-333 font-poppins weight-600">
                           <i class="fas fa-caret-right"></i>
                           O curso é totalmente à distância?
                        </a>
                        <p class="collapse mb-0 mt-1 font-poppins text-dark" id="collapseFaq2">
                           Sim, é possível fazer o curso de qualquer lugar do Brasil.
                        </p>  
                        <hr/>
                     </div>
                     <div>
                        <a href="#collapseFaq3" data-bs-toggle="collapse" href="#collapseFaq3" class="clickFaq text-d-none hover-d-none color-333 font-poppins weight-600">
                           <i class="fas fa-caret-right"></i>
                           O curso é legalizado pelo Centro de Aplicações e Avaliações?
                        </a>
                        <p class="collapse mb-0 mt-1 font-poppins text-dark" id="collapseFaq3">
                           Sim, nossos parceiros certificadores estão devidamente legalizados na aba supletivo, você terá acesso a todos os atos de legalidade dos cursos.
                        </p>  
                        <hr/>
                     </div>
                     <div>
                        <a href="#collapseFaq4" data-bs-toggle="collapse" href="#collapseFaq4" class="clickFaq text-d-none hover-d-none color-333 font-poppins weight-600">
                           <i class="fas fa-caret-right"></i>
                           E se eu acabar reprovando em alguma matéria?
                        </a>
                        <p class="collapse mb-0 mt-1 font-poppins text-dark" id="collapseFaq4">
                           Neste caso, após a correção da prova você receberá outra para poder fazer novamente, sem nenhum custo adicional.
                        </p>  
                        <hr/>
                     </div>
                     <div>
                        <a href="#collapseFaq5" data-bs-toggle="collapse" href="#collapseFaq5" class="clickFaq text-d-none hover-d-none color-333 font-poppins weight-600">
                           <i class="fas fa-caret-right"></i>
                           Qual é o valor do curso?
                        </a>
                        <p class="collapse mb-0 mt-1 font-poppins text-dark" id="collapseFaq5">
                           Neste caso, você deverá entrar em contato com nossa equipe através de um de nossos canais de comunicação (Telefone/E-mail/Contato Via Site) e assim um de nossos consultores entrará em contato com você.
                        </p>  
                        <hr/>
                     </div>
                     <div>
                        <a href="#collapseFaq7" data-bs-toggle="collapse" href="#collapseFaq7" class="clickFaq text-d-none hover-d-none color-333 font-poppins weight-600">
                           <i class="fas fa-caret-right"></i>
                           Qual o prazo para recebimento do certificado e demais documentos?
                        </a>
                        <p class="collapse mb-0 mt-1 font-poppins text-dark" id="collapseFaq7">
                           Após a conclusão das provas e a quitação do curso, em até 120 dias úteis letivos você receberá o certificado, e será publicado no diário oficial dos estudantes.
                        </p>  
                        <hr/>
                     </div>
                     <div>
                        <a href="#collapseFaq8" data-bs-toggle="collapse" href="#collapseFaq8" class="clickFaq text-d-none hover-d-none color-333 font-poppins weight-600">
                           <i class="fas fa-caret-right"></i>
                           Qual a duração do curso?
                        </a>
                        <p class="collapse mb-0 mt-1 font-poppins text-dark" id="collapseFaq8">
                           O tempo de conclusão depende do próprio aluno, você irá receber o material de apoio e as provas, quanto mais rápido responder e nos devolver, mais rápido receberá seu diploma.
                        </p>  
                        <hr/>
                     </div>
                     <div>
                        <a href="#collapseFaq10" data-bs-toggle="collapse" href="#collapseFaq10" class="clickFaq text-d-none hover-d-none color-333 font-poppins weight-600">
                           <i class="fas fa-caret-right"></i>
                           O certificado é válido em meu estado?
                        </a>
                        <p class="collapse mb-0 mt-1 font-poppins text-dark" id="collapseFaq10">
                           Todo curso autorizado pelo Conselho Estadual de Educação é válido em todo o território nacional.
                        </p>  
                        <hr/>
                     </div>

                     <a href="#colForm" class="btn btn-primary hover-d-none" title="Conheça o que podemos fazer por você">Saiba mais</a>

                  </div>

               </div>

               <div class="col-4 d-none d-lg-block">
                  <img src="{{ asset('sites-proprios/cefaep/images/ilustracao.png') }}" alt="imagem de ilustração">
               </div>

            </div>
         </div>
      </section>

      <section id="ligue-agora" class="pb-5 pb-lg-5 pt-5 pt-lg-0">
         <a href="tel:08000420075">
            <img src="{{ asset('sites-proprios/cefaep/images/banner-0800.jpeg') }}" class="img-fluid w-100 object-fit-cover">
         </a>
      </section>

      <section id="planos" class="m-vantagens text-center pb-5 pt-3"> 
         <div class="container-fluid px-xl-7 pb-5">

            <div class="text-center">
               <p class="m-0 text-center title-lead">Do Ensino Fundamental ao Pré-vestibular com o melhor e mais completo conteúdo da internet.</p>

               <h2 class="title title--small font-saira-condensed px-4">Escolha o Plano Ideal para você</h2>
               <img src="{{ asset('sites-proprios/cefaep/images/dots-blue.png') }}">
            </div>

            <div class="row gx-lg-5 mt-4">

               <div class="col-lg-4 mb-xs-4-5">
                  <div class="card">
                     <div class="card-header">

                        <div class="position-absolute bg-warning rounded-circle square-60 flex-center box-shadow right-0 mt--35px mr-lg--25">
                           <div class="flex-center position-absolute font-poppins flex-center line-height-5-5">
                              <span class="weight-700 text-dark fs-md-17">
                                 25%<br>OFF
                              </span>
                           </div>
                        </div>

                        <h1 class="text-light font-montserrat weight-800 fs-30 px-3-5 px-lg-0">Plano CEFAEP Médio</h1>

                     </div>
                     <div class="card-body px-2 text-light">

                        <div class="my-3">
                           <div>
                              <p class="color-c9 text-d-inline mb-0 weight-600 fs-16 ls-05 font-montserrat weight-700">De 12x R$ 199,99</p>
                              <p class="text-light mb-0 weight-600 fs-27 ls-05 font-montserrat weight-700">Por 10x de R$ 149,90</p>
                           </div>
                        </div>

                        <div class="h-lg-300">

                           <p class="mb-0 weight-600 color-white">
                              <i class="far fa-check-circle"></i>
                              Aulas 100% Online e Digital
                           </p>
                           <hr>
                           <p class="mb-0 weight-600 color-white">
                              <i class="far fa-check-circle"></i>
                              Reconhecido pelo MEC
                           </p>
                           <hr>
                           <p class="mb-0 weight-600 color-white">
                              <i class="far fa-check-circle"></i>
                              Certificado Entregue em 120 dias
                           </p>
                           <hr>
                           <p class="mb-0 weight-600 color-white">
                              <i class="far fa-check-circle"></i>
                              Plataforma 24hrs por 7 dias
                           </p>
                           <hr>
                           <p class="mb-0 weight-600 color-white">
                              <i class="far fa-check-circle"></i>
                              Grupo de Alunos Whatsapp
                           </p>

                        </div>

                        <div class="px-3 pb-1 pt-4 pt-lg-0">
                           <a href="#colForm" class="btn btn-orange text-shadow-3 hover-d-none w-100" title="Conheça o que podemos fazer por você">Faça sua Matrícula</a>
                        </div>

                     </div>
                  </div>
               </div>
               <div class="col-lg-4 mb-xs-4-5">
                  <div class="card">
                     <div class="card-header">

                        <div class="position-absolute bg-warning rounded-circle square-60 flex-center box-shadow right-0 mt--35px mr-lg--25">
                           <div class="flex-center position-absolute font-poppins flex-center line-height-5-5">
                              <span class="weight-700 text-dark fs-md-17">
                                 60%<br>OFF
                              </span>
                           </div>
                        </div>

                        <h1 class="text-light font-montserrat weight-800 fs-30 px-3-5 px-lg-0">Plano CEFAEP Oferta do Dia</h1>

                     </div>
                     <div class="card-body px-2 text-light">

                        <div class="my-3">
                           <div>
                              <p class="color-c9 text-d-inline mb-0 weight-600 fs-16 ls-05 font-montserrat weight-700">De 12x R$ 199,99</p>
                              <p class="text-light mb-0 weight-600 fs-27 ls-05 font-montserrat weight-700">Por 10x de R$ 99,99</p>
                           </div>
                        </div>

                        <div class="h-lg-400">

                           <p class="mb-0 weight-600 color-white">
                              <i class="far fa-check-circle"></i>
                              Aulas 100% Online e Digital
                           </p>
                           <hr>
                           <p class="mb-0 weight-600 color-white">
                              <i class="far fa-check-circle"></i>
                              Reconhecido pelo MEC
                           </p>
                           <hr>
                           <p class="mb-0 weight-600 color-white">
                              <i class="far fa-check-circle"></i>
                              Certificado Entregue em 120 dias
                           </p>
                           <hr>
                           <p class="mb-0 weight-600 color-white">
                              <i class="far fa-check-circle"></i>
                              Plataforma 24hrs por 7 dias
                           </p>
                           <hr>
                           <p class="mb-0 weight-600 color-white">
                              <i class="far fa-check-circle"></i>
                              Grupo de Alunos Whatsapp
                           </p>
                           <hr>
                           <p class="mb-0 weight-600 color-white">
                              <i class="far fa-check-circle"></i>
                              Suporte de tutores
                           </p>
                           <hr>
                           <p class="mb-0 weight-600 color-white">
                              <i class="far fa-check-circle"></i>
                              Oferta especial do dia
                           </p>

                        </div>

                        <div class="px-3 pb-1 pt-4 pt-lg-0">
                           <a href="#colForm" class="btn btn-orange text-shadow-3 hover-d-none w-100" title="Conheça o que podemos fazer por você">Faça sua Matrícula</a>
                        </div>

                     </div>
                  </div>
               </div>
               <div class="col-lg-4 mb-xs-4-5">
                  <div class="card">
                     <div class="card-header">

                        <div class="position-absolute bg-warning rounded-circle square-60 flex-center box-shadow right-0 mt--35px mr-lg--25">
                           <div class="flex-center position-absolute font-poppins flex-center line-height-5-5">
                              <span class="weight-700 text-dark fs-md-17">
                                 25%<br>OFF
                              </span>
                           </div>
                        </div>

                        <h1 class="text-light font-montserrat weight-800 fs-30 px-3-5 px-lg-0">Plano CEFAEP Completo</h1>

                     </div>
                     <div class="card-body px-2 text-light">

                        <div class="my-3">
                           <div>
                              <p class="color-c9 text-d-inline mb-0 weight-600 fs-16 ls-05 font-montserrat weight-700">De 12x R$ 199,99</p>
                              <p class="text-light mb-0 weight-600 fs-27 ls-05 font-montserrat weight-700">Por 10x de R$ 149,99</p>
                           </div>
                        </div>

                        <div class="h-lg-300">

                           <p class="mb-0 weight-600 color-white">
                              <i class="far fa-check-circle"></i>
                              Aulas 100% Online e Digital
                           </p>
                           <hr>
                           <p class="mb-0 weight-600 color-white">
                              <i class="far fa-check-circle"></i>
                              Reconhecido pelo MEC
                           </p>
                           <hr>
                           <p class="mb-0 weight-600 color-white">
                              <i class="far fa-check-circle"></i>
                              Certificado Entregue em 120 dias
                           </p>
                           <hr>
                           <p class="mb-0 weight-600 color-white">
                              <i class="far fa-check-circle"></i>
                              Plataforma 24hrs por 7 dias
                           </p>
                           <hr>
                           <p class="mb-0 weight-600 color-white">
                              <i class="far fa-check-circle"></i>
                              Grupo de Alunos Whatsapp
                           </p>

                        </div>

                        <div class="px-3 pb-1 pt-4 pt-lg-0">
                           <a href="#colForm" class="btn btn-orange text-shadow-3 hover-d-none w-100" title="Conheça o que podemos fazer por você">Faça sua Matrícula</a>
                        </div>

                     </div>
                  </div>
               </div>

            </div>

         </div>
      </section>
      
   </main>

   <footer id="footer" class="footer container-fluid content-info" style="background-image: url('{{ asset('sites-proprios/images/cefaep/bg-footer.jpg') }}');" role="contentinfo">
      
      <div class="container-fluid px-xl-7">
         <div class="row">
            <div class="col-lg-6 col-xl-5 footer__item">
               <h2 class="footer__title text-uppercase">CONTATO</h2>
               <div itemscope="" itemtype="http://schema.org/Organization">
                  <meta itemprop="name" content="{{ $plataforma->nome }}">
                  <address class="row" itemprop="address" itemscope="" itemtype="http://schema.org/PostalAddress" style="margin-bottom: 0;">
                     <div class="col-12 col-lg address">

                        <div class="address__icon">
                           <i class="fa fa--address fa-phone" aria-hidden="true"></i>
                        </div>
                        <a href="tel:08000420075" class="address__phone" itemprop="telephone">
                           0800 042 0075
                        </a>

                        <div class="address__icon">
                           <i class="fa fa--address fa-phone" aria-hidden="true"></i>
                        </div>
                        <a href="tel:+551735130341" class="address__phone" itemprop="telephone">
                           (17) 3513-0341
                        </a>

                        <div class="address__icon">
                           <i class="fa fa--address fa-phone" aria-hidden="true"></i>
                        </div>
                        <a href="tel:+551735135874" class="address__phone" itemprop="telephone">
                           (17) 3513-5874
                        </a>

                        <div class="address__icon">
                           <i class="fa fa--address fa-phone" aria-hidden="true"></i>
                        </div>
                        <a href="tel:+551735133947" class="address__phone" itemprop="telephone">
                           (17) 3513-3947
                        </a>

                        <div class="address__icon">
                           <i class="fa fa--address fa-phone" aria-hidden="true"></i>
                        </div>
                        <a href="tel:+551735139668" class="address__phone" itemprop="telephone">
                           (17) 3513-9668
                        </a>

                        @if($plataforma->telefone)
                        <div class="address__icon">
                           <i class="fa fa--address fa-phone" aria-hidden="true"></i>
                        </div>
                        <a href="tel:+55{{preg_replace('/\D/', '', $plataforma->telefone)}}" class="address__phone" itemprop="telephone">
                           {{ $plataforma->telefone ?? '(00) 0000-0000' }}
                        </a>
                        @endif

                        @if($plataforma->whatsapp)
                        <div class="address__icon">
                           <i class="fa fa-whatsapp" aria-hidden="true"></i>
                        </div>
                        <a href="https://api.whatsapp.com/send?phone=55{{preg_replace('/\D/', '', $plataforma->whatsapp)}}&text=Olá, vim através do Site {{ $plataforma->nome }}" target="_blank" class="address__phone" itemprop="telephone">
                           {{ $plataforma->whatsapp ?? '(00) 0000-0000' }}
                        </a>
                        @endif
                        
                        <div class="address__icon">
                           <i class="fa fa--address fa-envelope-o" aria-hidden="true"></i>
                        </div>
                        <div>
                           <a href="mailto:{{ $plataforma->email }}" class="link--white" title="mande-nos um e-mail" itemprop="email">{{ $plataforma->email ?? 'example@domain.com' }}</a>
                        </div>
                     </div>
                     <div class="col-12 col-lg address">

                        <div class="address__icon"><i class="fa fa-map-marker" aria-hidden="true"></i></div>
                        <div>
                           <span itemprop="streetAddress"> </span> <br> <span itemprop="addressLocality">São José do Rio Preto</span> - <span itemprop="addressRegion">SP</span>
                        </div>

                     </div>
                  </address>
               </div>
            </div>
            <div class="col-lg-4 col-xl-4 offset-xl-1 footer__item">

               <ul class="nav-neutro nav-neutro--inline nav-social pull-right-sm">
                  <li class="nav-neutro__item nav-social__item">
                     <h2 class="footer__title text-uppercase">Redes Sociais</h2>
                  </li>
                  <li class="nav-neutro__item nav-social__item">
                     <a href="{{ $plataforma->facebook }}" class="nav-social__link" title="Curta nossa página no Facebook" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                  </li>
                  <li class="nav-neutro__item nav-social__item">
                     <a href="{{ $plataforma->instagram }}" class="nav-social__link" title="Siga nosso perfil no Instagram" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                  </li>
               </ul>

            </div>
            <div class="col-lg-2 col-xl-2 footer__item d-none d-lg-flex align-items-center justify-content-end">
               <img alt="Logotipo {{ $plataforma->nome }}" class="img-fluid lazyloaded" src="{{ $plataforma->logotipo }}">
            </div>
         </div>
      </div>

      <div class="container-fluid px-xl-7 footer__copyright">
         <div class="row">
            <div class="col-sm-6 text-xs-center">
               <span class="weight-600">{{ $plataforma->nome }}</span> © 2021 - Todos os direitos reservados.
            </div>
            <div class="col-sm-6 text-xs-center footer__copyright--by">
               Desenvolvido por <a href="https://wozcode.com" class="link--white" title="Confira outros trabalhos desenvolvidos por WOZCODE" target="_blank"><strong class="footer__copyright--by">WOZCODE.</strong></a>
            </div> 
         </div>
      </div>

   </footer>

   <a href="https://api.whatsapp.com/send?phone=55{{preg_replace('/\D/', '', $plataforma->whatsapp)}}&text=Olá, vim através do Site {{ $plataforma->nome }}" target="_blank" class="text-white fs-30">
      <div class="position-fixed bottom-0 right-0 me-3 me-lg-4 mb-3 mb-lg-4 z-4 wpp-pulse-button z-8">
         <i class="fab fa-whatsapp"></i>
      </div>
   </a>

   <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
   <script src="https://kit.fontawesome.com/2f91eaaef4.js" crossorigin="anonymous"></script>  
   <script src="https://cdn.jsdelivr.net/npm/uikit@3.5.5/dist/js/uikit.min.js"></script>

   {!! $plataforma->scripts_final_body !!}

</body>
</html>