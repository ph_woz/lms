<?php

namespace App\Http\Livewire\Admin\Servico;

use Livewire\Component;
use App\Models\Servico;
use App\Models\User;
use App\Models\Unidade;
use App\Models\Categoria;
use App\Models\ServicoCategoria;
use Illuminate\Support\Facades\Http;

class Info extends Component
{
    public $servico;
    public $unidade;
    public $cadastrante;
    public $categorias;
    public $password_to_delete;

    public function mount($servico_id)
    {
        $this->servico = Servico::plataforma()->find($servico_id);

        $this->unidade = Unidade::select('id','nome')->plataforma()->where('id', $this->servico->unidade_id)->first();

        $this->cadastrante = User::select('id','nome')->plataforma()->where('id', $this->servico->cadastrante_id)->first();

        $categoriasIds = ServicoCategoria::plataforma()->where('servico_id', $this->servico->id)->get()->pluck('categoria_id')->toArray();
        $this->categorias = Categoria::select('id','nome')->plataforma()->ativo()->whereIn('id', $categoriasIds)->orderBy('nome')->get();

    }

    public function render()
    {
        return view('livewire.admin.servico.info');
    }

    public function delete()
    {
        if(\Auth::attempt(['id' => \Auth::id(), 'password' => $this->password_to_delete, 'status' => 0]))
        {
            Servico::plataforma()->where('id', $this->servico->id)->delete();

            session()->flash('success', 'Serviço deletado com êxito.');
            
            return redirect(route('admin.servico.lista'));

        } else {

            session()->flash('danger-livewire', 'Senha incorreta.');

            $this->dispatchBrowserEvent('toast');
        }
    }
}
