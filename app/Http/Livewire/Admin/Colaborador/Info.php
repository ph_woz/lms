<?php

namespace App\Http\Livewire\Admin\Colaborador;

use Livewire\Component;
use App\Models\User;
use App\Models\UserEndereco;
use App\Models\UserCategoria;
use App\Models\Categoria;
use App\Models\Unidade;
use App\Models\AclPlataformaModuloUser;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Http;

class Info extends Component
{
    public $user;
    public $cadastrante;
    public $endereco;
    public $unidade;
    public $categorias;

    public $password_to_delete;

    public function mount($user_id)
    {
        $this->user = User::plataforma()->find($user_id);
    
        $this->cadastrante = User::plataforma()->where('id', $this->user->cadastrante_id ?? null)->first();
        
        $this->unidade = Unidade::plataforma()->where('id', $this->user->unidade_id ?? null)->first();

        $categoriasIds = UserCategoria::plataforma()->where('user_id', $this->user->id ?? null)->get()->pluck('categoria_id')->toArray();
        $this->categorias = Categoria::plataforma()->ativo()->whereIn('id', $categoriasIds)->orderBy('nome')->get();

        $this->endereco = UserEndereco::plataforma()->where('user_id', $this->user->id ?? null)->select('cep','rua','numero','bairro','cidade','estado','complemento','ponto_referencia')->first() ?? array();

        if($this->endereco)
        {
            $this->endereco = $this->endereco->toArray();
        }
    }

    public function render()
    {
        $modulos = \DB::table('plataformas_modulos')
            ->join('acl_plataformas_modulos_users', 'plataformas_modulos.id', 'acl_plataformas_modulos_users.modulo_id')
            ->where('plataformas_modulos.status', 0)
            ->where('plataformas_modulos.plataforma_id', session('plataforma_id'))
            ->where('acl_plataformas_modulos_users.user_id', $this->user->id ?? null)
            ->select('acl_plataformas_modulos_users.nivel','plataformas_modulos.url','plataformas_modulos.texto')
            ->get();

        
        if($this->user == null)
        {
            $erro = 'Colaborador não encontrado.';
            return view('admin.page-whoops', ['erro' => $erro]);
        }

        return view('livewire.admin.colaborador.info', ['modulos' => $modulos]);
    }

    public function delete()
    {
        if(\Auth::attempt(['id' => \Auth::id(), 'password' => $this->password_to_delete, 'status' => 0]))
        {
            UserEndereco::plataforma()->where('user_id', $this->user->id)->delete();
            UserCategoria::plataforma()->where('user_id', $this->user->id)->delete();
            AclPlataformaModuloUser::plataforma()->where('user_id', $this->user->id)->delete();
            User::plataforma()->where('id', $this->user->id)->delete();

            session()->flash('success', 'Colaborador deletado com êxito.');
            
            return redirect(route('admin.colaborador.lista'));

        } else {

            session()->flash('danger-livewire', 'Senha incorreta.');

            $this->dispatchBrowserEvent('toast');
        }
    }
}
