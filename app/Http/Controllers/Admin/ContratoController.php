<?php
  
namespace App\Http\Controllers\Admin;
  
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use setasign\Fpdi\Fpdi;
use Mail;
use App\Mail\EmailContratoAssinado;
use App\Models\Unidade;
use App\Models\Contrato;
use App\Models\Plataforma;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use App\Models\User;

  
class ContratoController extends Controller
{

    public function lista()
    {
        return view('admin.contrato.lista');
    }
    
    public function info($contrato_id)
    {
        return view('admin.contrato.info', compact('contrato_id') );
    }

    public function add()
    {
        $unidades = Unidade::select('id','nome')->plataforma()->ativo()->orderBy('nome')->get();

        if(request('sent'))
        {
            $contrato = Contrato::create(request()->all());

            $this->syncPDF($contrato, 'pdf', 'arquivo_pdf');

            return redirect()->route('admin.contrato.info', ['id' => $contrato->id])->with('success','Contrato cadastrado com êxito.');
        }

        return view('admin.contrato.add', compact('unidades') );
    }

    public function editar($contrato_id)
    {
        $contrato = Contrato::plataforma()->where('id', $contrato_id)->first();

        $unidades = Unidade::select('id','nome')->plataforma()->ativo()->orderBy('nome')->get();

        if(request('sent'))
        {
            $contrato->update(request()->all());

            $this->syncPDF($contrato, 'pdf', 'arquivo_pdf');

            return back()->with('success','Contrato atualizado com êxito.');
        }

        return view('admin.contrato.editar', compact('contrato','unidades') );
    }

    public function solicitarAssinaturaContratoDigital($token, $email, $id, $user_contrato_id)
    {
        $user = User::select('nome','email')->plataforma()->where('id', $id)->where('email', $email)->where('token', $token)->first();

        if(is_null($user))
        {
            return 'Não é válido';
        }

        $userContrato = \App\Models\UserContrato::plataforma()->where('id', $user_contrato_id)->first();

        if(request('aceitar_termos'))
        {

            $path = 'plataformas/'.$userContrato->plataforma_id.'/contratos/users/'.$userContrato->user_id.'/assinatura-do-cliente.png';

            if (!is_file($path))
            {
                file_put_contents($path, '');
            }


            $split = explode(',', request('base64_sign'));

            $status = file_put_contents($path,base64_decode($split[1]));

            if($status)
            {
                $userContrato->update([
                    'data_assinatura' => date('Y-m-d H:i'),
                    'foto_assinatura' => $path,
                ]);

                $this->setDesenhoAssinatura($userContrato);

                $plataforma = Plataforma::select('nome','email')->where('id', $userContrato->plataforma_id)->first();

                $emailsToSend = [$user->email, $plataforma->email];

                \Mail::cc($emailsToSend)->send(new \App\Mail\EmailContratoAssinado($user, $userContrato, $plataforma));

                return back()->with('success', 'Contrato assinado com êxito.');

            } else {

                echo "Upload failed"; die();
            }
        }

        return view('aluno.solicitar-assinatura-contrato-digital', compact('userContrato','user') );
    }

    public function syncPDF($contrato, $tipo, $inputFileName)
    {
        $file = request()->file($inputFileName);

        if($file)
        {
            $caminhoPDF = str_replace(\Request::root().'/', '', $contrato->pdf);

            if(file_exists($caminhoPDF))
            {
                unlink($caminhoPDF);
            }

            $filename  = 'modelo-'.$contrato->id.'.'.$file->getClientOriginalExtension();
            $diretorio = 'plataformas/'.\Auth::user()->plataforma_id.'/contratos/';
            $caminho = \Request::root().'/'.$diretorio.$filename;

            if (!file_exists($diretorio))
            {
                mkdir($diretorio, 0777, true);
            }

            $file->move($diretorio, $filename);

            $contrato->update(['pdf' => $caminho]);
        }
    }

    public function gerar($userContrato)
    {
        $filePathModelo = Contrato::select('pdf')->plataforma()->where('id', $userContrato->contrato_id)->value('pdf');

        $filePath = str_replace(\Request::root().'/', '', $filePathModelo);
        $caminhoDeSaida = 'plataformas/'.\Auth::user()->plataforma_id.'/contratos/users/'.$userContrato->user_id;
        $outputFilePath = public_path($caminhoDeSaida);

        if (!file_exists($outputFilePath))
        {
            mkdir($outputFilePath, 0777, true);
        }

        $outputFilePath = $outputFilePath .'/'.$userContrato->contrato_id.'.pdf';
        $caminhoDeSaida = $caminhoDeSaida . '/'.$userContrato->contrato_id.'.pdf';

        $this->fillPDFFile($filePath, $outputFilePath, $userContrato, $caminhoDeSaida);
    }
    
    public function gerarSimulacao($contrato_id)
    {   
        $filePathModelo = Contrato::select('pdf')->plataforma()->where('id', $contrato_id)->value('pdf');
        $file = str_replace(\Request::root().'/', '', $filePathModelo);

        $fpdi = new FPDI;

        $count = $fpdi->setSourceFile($file);
  
        for ($i=1; $i<=$count; $i++)
        {
            $template = $fpdi->importPage($i);
            $size = $fpdi->getTemplateSize($template);
            $fpdi->AddPage($size['orientation'], array($size['width'], $size['height']));
            $fpdi->useTemplate($template);
            

            $campos = array_combine(request('camposIds') ?? array(), request('values') ?? array());

            foreach($campos as $campo_id => $value)
            {
                $campo = \App\Models\ContratoCampo::select('campo','px','py','fontSize','n_pagina')->plataforma()->where('contrato_id', $contrato_id)->where('id', $campo_id)->first();
                $n_pagina = $campo->n_pagina ?? 1;
                $n_pagina = intval($n_pagina);

                if($n_pagina == $i)
                {        
                    $fpdi->SetFont("helvetica", "", $campo->fontSize ?? 14);
                    $fpdi->SetTextColor(0,0,0);
                    $fpdi->Text($campo->px ?? 20, $campo->py ?? 100, utf8_decode($value ?? ' '));
                }
            }
        }
  
        $fpdi->Output('I');
    }

    public function setDesenhoAssinatura($userContrato)
    {
        $fpdi = new FPDI;

        $count = $fpdi->setSourceFile($userContrato->pdf);

        $campo = \App\Models\ContratoCampo::select('campo','px','py','fontSize','n_pagina')->plataforma()->where('contrato_id', $userContrato->contrato_id)->where('campo','Assinatura Contratante')->first();

        for ($i=1; $i<=$count; $i++)
        {
            $template = $fpdi->importPage($i);
            $size = $fpdi->getTemplateSize($template);
            $fpdi->AddPage($size['orientation'], array($size['width'], $size['height']));
            $fpdi->SetTitle(utf8_decode($userContrato->referencia));
            $fpdi->useTemplate($template);

            $n_pagina = $campo->n_pagina ?? 1;
            $n_pagina = intval($n_pagina);

            $n_pagina = $campo->n_pagina ?? 1;
            $n_pagina = intval($n_pagina);

            if($n_pagina == $i)
            {
                $fpdi->Image($userContrato->foto_assinatura, $campo->px ?? 20, $campo->py ?? 100, $campo->fontSize ?? 0);
            }

        }

        $fullPath = 'plataformas/'.$userContrato->plataforma_id.'/contratos/users/'.$userContrato->user_id.'/'.$userContrato->contrato_id.'-contrato-assinado.pdf';

        $fpdi->Output($fullPath, 'F');

        $userContrato->update(['pdf_assinado' => $fullPath]);
    }

    public function fillPDFFile($file, $outputFilePath, $userContrato, $caminhoDeSaida)
    {
        $fpdi = new FPDI;

        $plataforma_id = $userContrato->plataforma_id;
        $user_id = $userContrato->user_id;
        $user_contrato_id = $userContrato->id;
        $contrato_id = $userContrato->contrato_id;

        $campos = DB::select( DB::raw("
            SELECT
                contratos_campos.campo,
                contratos_campos.px,
                contratos_campos.py,
                contratos_campos.fontSize,
                contratos_campos.color,
                contratos_campos.n_pagina,
                users_contratos_campos.value
            FROM users_contratos
            INNER JOIN users_contratos_campos ON users_contratos.id = users_contratos_campos.user_contrato_id
            INNER JOIN contratos_campos ON users_contratos_campos.campo_id = contratos_campos.id
            WHERE contratos_campos.plataforma_id  = ".$plataforma_id."
                AND users_contratos_campos.user_id = ".$user_id."
                AND contratos_campos.contrato_id = ".$contrato_id."
                AND users_contratos_campos.contrato_id = ".$contrato_id."
                AND users_contratos.id = ".$user_contrato_id."
                AND users_contratos_campos.user_contrato_id = ".$user_contrato_id."
                AND users_contratos.contrato_id = ".$contrato_id.";
        "));

        $count = $fpdi->setSourceFile($file);
  
        for ($i=1; $i<=$count; $i++)
        {
            $template = $fpdi->importPage($i);
            $size = $fpdi->getTemplateSize($template);
            $fpdi->AddPage($size['orientation'], array($size['width'], $size['height']));
            $fpdi->SetTitle(utf8_decode($userContrato->referencia));
            $fpdi->useTemplate($template);
            
            foreach($campos as $campo)
            {
                $n_pagina = $campo->n_pagina ?? 1;
                $n_pagina = intval($n_pagina);
                
                if($n_pagina == $i)
                {  
                    $text = $campo->value ?? ' ';
                    $fpdi->SetFont("helvetica", "", $campo->fontSize ?? 14);
                    $fpdi->SetTextColor(0,0,0);
                    $fpdi->Text($campo->px ?? 20, $campo->py ?? 100, utf8_decode($text));
                }
            }
        }
  
        $fpdi->Output($outputFilePath, 'F');

        $userContrato->update(['pdf' => $caminhoDeSaida]);
    }
}