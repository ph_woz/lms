<!DOCTYPE html>
<html lang="pt-br">
<head>

  <!-- Google tag (gtag.js) -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=G-MREB450HCT"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'G-MREB450HCT');
  </script>

   <meta charset="utf-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1">
   
   <title>{{ $seo->seo_title ?? $plataforma->nome }}</title>
   <meta name="robots" content="index, follow">
   <meta name="description" content="{{ $seo->seo_description ?? null }}"/>
   <meta name="keywords" content="{{ $seo->seo_keywords ?? null }}"/>
   <link href="{{ Request::url() }}" rel="canonical">
   <link rel="shortcut icon" href="{{ $plataforma->favicon ?? asset('sites-proprios/resgate-vertical/images/favicon.ico') }}">
   <meta property="og:locale" content="pt_BR" />
   <meta property="og:type" content="website" />
   <meta property="og:title" content="{{ $seo->seo_title ?? $plataforma->nome }}" />
   <meta property="og:description" content="{{ $seo->seo_description ?? null }}" />
   <meta property="og:url" content="{{ Request::url() }}" />
   <meta property="og:site_name" content="{{ $plataforma->nome }}" />
   <meta property="og:image" content="{{ $plataforma->logotipo ?? asset('sites-proprios/resgate-vertical/images/logotipo.png') }}" />
   <meta name='author' content='WOZCODE | Pedro Henrique' />
       
   <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/uikit@3.5.4/dist/css/uikit.min.css" />

   <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
   <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">

   <link rel="stylesheet" href="{{ asset('css/lmc-old.css') }}">
   <link rel="stylesheet" href="{{ asset('css/lmc-old-app.css') }}">
    
   <style>
    
.menu-link, .btn-entrar {
  font-family: 'Nunito', sans-serif;
  font-weight: 600;
  font-size: 15px;
  letter-spacing: .4px;
  display: block;
    padding: .5rem .3rem;
    display: inline-block;
}

.line-vertical { 
  width: 1.5px;
  height: 20px;
  border:.8px solid rgba(0, 0, 0, .05);
  margin-top: 10px; 
  margin-left: 10px;
  margin-right: 10px;
}

@media (max-width: 991px) { 
  .line-vertical {
    display: none;
  }
}


    #menuPlataforma nav { background: #dc3545; }
    .menu-link, .menu-link-color { color: #F9F9F9;; }
    .menu-link:hover { text-decoration: none; background: #bd3131; }
    .navbar-brand span, .activeNavLink { text-decoration: none; background: #bd3131; }

    .color-base { color: #555; }
    .color-base-secundaria { color: #555; }
    
    .bg-base, 
    .bg-selected, 
    .btnDown a div:hover,
    ::-webkit-scrollbar-thumb { background: rgba(181, 36, 36, .95); }

    .bg-base-secundaria { background: rgba(178, 49, 49, .9); }

    .btn-cadastrar, .btn-link, .btn-submit, .btnTop, .btn-append { background: rgba(181, 36, 36, .95); color: #FFF; }
    .btn-hover:hover, .header-icons a div:hover, .btn-cadastrar:hover, .btn-link:hover, .btn-submit:hover, .btnTop:hover, .btn-append:hover { background: #dc3545; }
    
    .section-jumbontron { padding-top: 125px!important; }

#menu {
    width: 100%;
    box-sizing: border-box;
    padding: 9px 10px!important;
    position: fixed;
    z-index: 200;
    box-shadow: 0px -1px 8px 0px rgba(0, 0, 0, 0.60);
    background: transparent;
    transition: background-color 0.3s ease-in-out;
}
@media (min-width: 769px) 
{
    #menu { height: 75px; }

    .menu-link { 
      margin: 0 10px;
      padding-top: 27.2px!important;
      padding-bottom: 27.2px!important;
      padding-left: 10px!important; 
      padding-right: 10px!important; 
    }
}
.menu-link {
    font-family: 'Open Sans',sans-serif;
    font-size: 13.5px;
    text-transform: uppercase;
    text-decoration: none;
    font-weight: 700;
    color: #F5F5F5!important;
    cursor: pointer;
    transition: all .2s;
    text-shadow: 1px 1px rgba(0, 0, 0, .1);
}
.line-vertical { display:none }

.btn-entrar { 
  background: #ffc107!important; 
  color: #021a36!important; 
  font-family: 'Poppins', sans-serif; 
  font-weight: 600!important; 
  border-radius: 30px!important;
  padding: 12px 24px!important;
}

.btn-sell {
 background-color:#c92027!important;
 display:inline-block!important;
 cursor:pointer!important;
 color:#ffffff!important;
 font-family:'Raleway', sans-serif!important;
 font-size:18px!important;
 padding:14px 35px!important;
 text-decoration:none!important;
 text-shadow:0px -1px 0px #7a2a1d!important;
 text-transform: uppercase!important;
 font-weight: 800!important;
 border-radius: 3px!important;
 border-bottom: 3.5px solid #5c0f19!important;
 text-shadow: 1.5px 1.5px rgba(0, 0, 0, .20)!important;
 letter-spacing: .2px!important;
}
.btn-sell:hover { background-color:#b51d23!important; }
#mainBlog { padding-top: 100px!important; }
    
    #footer { position: absolute!important; }
    
</style>
</head>
<body class="bg-body">

   <header id="menuPlataforma">
    <nav class="navbar-header navbar navbar-expand-lg fixed-top shadow" id="menu">
        <div class="container-fluid px-xl-7">

          <a class="navbar-brand" href="/#inicio">
                          <img src="https://lmcv2.s3-sa-east-1.amazonaws.com/plataformas/19/logotipoMenu.jpg" class="img-fluid" width="175">
                      </a>

          <button class="bg-transparent d-lg-none outline-0 border-0 menu-link-color" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="fa fa-bars fs-20"></span>
          </button>

          <div class="collapse navbar-collapse py-4 py-lg-0" id="navbarSupportedContent">

              <ul class="navbar-nav ml-auto ulNavLinks">
                                          <div class="line-vertical"></div>
                                                          <li class="nav-item">
                        <a class="menu-link " 
                           href="/#cursos" >
                            Nossos cursos
                        </a>
                    </li>

                                          <div class="line-vertical"></div>
                                                          <li class="nav-item">
                        <a class="menu-link " 
                           href="/#servicos" >
                            Nossos serviços
                        </a>
                    </li>

                                          <div class="line-vertical"></div>
                                                          <li class="nav-item">
                        <a class="menu-link activeNavLink " 
                           href="/sobre-nos" >
                            Quem Somos
                        </a>
                    </li>


                                          <div class="line-vertical"></div>
                                                          <li class="nav-item">
                        <a class="menu-link nav-link" 
                           href="/consultar-certificado" >
                            Consultar Certificado
                        </a>
                    </li>

                  <li class="nav-item">
                     <a class="menu-link nav-link activeNavLink" href="/login">
                        <i class="fa fa-user-graduate mr-1"></i>
                        Área do Aluno
                     </a>
                  </li>

             </ul>

          </div>

        </div>
    </nav>
</header>


    <main>

        <section>
            <div class="section-jumbontron bg-2">
                <div class="container-fluid px-xl-7">

                    <h1 class="titulo-jumbotron display-5 text-white">
                        Quem somos
                    </h1>
                    
                    <p class="slogan-jumbotron">
                        Sobre nós
                    </p>

                </div>
            </div>
            <div id="border-jumb" class="bg-base py-1-5 shadow"></div>
        </section>

        <section class="bg-white px-2 pt-5 pb-lg-5 pb-0" id="sobre-nos">
            <div class="container-fluid px-xl-7 mb-5 pb-5">
            
                <div class="row" id="rowSobreNos">
                    
                    <div class="col-lg-8 mt-2">
                        
                        <div class="pr-lg-5">
                            
                                                        
                            <p class="mb-0 font-opensans">
                              A <span class="weight-600 color-red-dark">RESGATE VERTICAL</span> é uma empresa de Assessoria e Consultoria em Saúde, Segurança do Trabalho e Meio Ambiente, uma empresa que busca sempre o apoio total às empresas, baseada nas exigências da legislação vigente em nosso país e na força de um trabalho dinâmico e responsável de seus profissionais com larga experiência profissional.<br />
<br />
Elaborando documentos que dão respaldo legal e com objetivo de atender as exigências legais da Portaria 3214/78 do Ministério do Trabalho e Emprego (MTE) e ainda o Instituto Nacional de Seguro Social (INSS) em conformidade com as Instruções Normativas, a RESGATE VERTICAL se destaca por atender empresas de todo segmento de mercado, sejam elas: micro, pequena, média e empresas de grande porte.<br />
<br />
<span class="title-blog color-base">Diretrizes</span><br />
Desde sua fundação, a RESGATE VERTICAL vem desenvolvendo e aperfeiçoando condutas que garantem a excelência de seus serviços, tendo construído uma experiência que permite planejar um futuro de crescimento sustentável.<br />
<br />
<span class="title-blog color-base">Missão</span><br />
Proporcionar soluções customizadas, criativas e econômicas em Saúde, Segurança e Meio Ambiente (SSMA) às empresas-clientes, assessorando-as para maximizar seus resultados, desempenho e confiabilidade.<br />
<br />
<span class="title-blog color-base">Visão</span><br />
Alcançar juntamente com nosso cliente um ambiente de trabalho salubre, proporcionando soluções que previnem o trabalhador de acidentes, garantindo assim, maior produtividade.<br />
Buscando reconhecimento no mercado brasileiro como a melhor empresa na prestação de serviços em Saúde, Segurança e Meio Ambiente (SSMA).<br />
<br />
<span class="title-blog color-base">Valores</span><br />
Atender o cliente como se fosse único;<br />
Superar a expectativa de atendimento dos clientes;<br />
Atuar com Responsabilidade Social e respeito ao Meio Ambiente;<br />
Gerar resultado econômico para garantir a continuidade do negócio;<br />
Selecionar e manter profissionais altamente qualificados e motivados;<br />
Atuar com ética e confidencialidade .
                            </p>

                        </div>

                    </div>
                    
                    <div class="col-lg-4 text-center mt-5 mt-lg-0 text-center">
                        
                        
                                                            <div class="mb-3">
                                    <img src="https://lmcv2.s3-sa-east-1.amazonaws.com/plataformas/19/plataforma-texto/544.png" class="d-block w-100 img-fluid fotoQuemSomos" width="285">
                                </div>
                            
                            
                        
                    </div>

                </div>

            </div>
        </section>

    </main>

   <footer class="bg-2 py-2" id="footer">
		<div class="container">
			<p class="mb-0 text-white font-nunito fs-8 ls-05">&copy; Resgate Vertical - Todos os direitos reservados</p>
		</div>	
	</footer>

	

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>



<script>

	$("#alertDanger").toast('show');
	$("#alertSuccess").toast('show');

	setTimeout(function() {

		$(".boxToast").addClass('d-none');

	}, 4500);
	
</script>

    
   <script src="https://cdn.jsdelivr.net/npm/uikit@3.5.4/dist/js/uikit.min.js"></script>

       
</body>
</html>