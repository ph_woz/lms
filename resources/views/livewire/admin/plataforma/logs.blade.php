<div>

    <div class="alert alert-secondary mb-4 px-4 py-3 shadow d-flex align-items-center" role="alert">
        <i class="fas fa-exclamation-triangle"></i>
        <span class="weight-600 ms-3">
            Os registros após o tempo de 60 dias serão excluídos automaticamente.
        </span>
    </div>

    <form>

        <div class="d-lg-flex mb-3">

            <input wire:model="search" type="search" class="me-lg-3 mb-3 mb-lg-0 form-control p-form" placeholder="Buscar">

            <div wire:ignore class="w-100 me-lg-3 mb-3 mb-lg-0">
                <select wire:model="user_id" class="selectpicker" data-width="100%" data-live-search="true" title="Colaboradores">
                    <option value="">Selecione</option>
                    @foreach($users as $user)
                        <option value="{{ $user->id }}">{{ $user->nome }}</option>
                    @endforeach
                </select>
            </div>

            <div wire:ignore class="w-100">
                <select wire:model="tipo" class="selectpicker" data-width="100%" data-live-search="true" title="Tag referente">
                    <option value="">Selecione</option>
                    @foreach($tipos as $tipo)
                        <option value="{{ $tipo }}">{{ $tipo }}</option>
                    @endforeach
                </select>
            </div>

        </div>

    </form>

    <div class="mt-5 mb-2">
        <span class="resultados-encontrados">
            {{ count($log) }} resultados de {{ $countLog }}.
        </span>
    </div>

    <div class="row">
        <div class="col-12">

            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col">Colaborador</th>
                                    <th scope="col">Referência</th>
                                    <th scope="col">Data da Ação</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($log as $l)
                                    <tr>

                                        <td>
                                            @if($l->userId)
                                                <a href="{{ route('admin.colaborador.info', ['id' => $l->userId]) }}" class="color-blue-info-link"> 
                                                    {{ strtok($l->userNome, " ") }}
                                                </a>
                                            @else
                                                {{ $l->nome }} - {{ $l->email }}
                                            @endif
                                        </td>
                                        
                                        <td>{{ $l->ref }}</td>

                                        <td>
                                            {{ \App\Models\Util::replaceDateTimePt($l->created_at) }}
                                        </td>

                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            {{ $log->links() }}

        </div>
    </div>

</div>