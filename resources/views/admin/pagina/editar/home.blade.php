@extends('layouts.admin.master')

@section('css')
<style>
.container-banner { position: relative; }
.image-banner {
  display: block;
  width: 100%;
  height: auto;
}
.overlay-banner {
  position: absolute;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  height: 100%;
  width: 100%;
  opacity: 0;
  transition: .5s ease;
  background-color: rgba(0,0,0,.6);
}
.container-banner:hover .overlay-banner { opacity: 1; }
.text-banner {
  color: white;
  font-size: 75px;
  position: absolute;
  top: 45%;
  left: 50%;
  -webkit-transform: translate(-50%, -50%);
  -ms-transform: translate(-50%, -50%);
  transform: translate(-50%, -50%);
  text-align: center;
}

.placeholder-edit, .placeholder-edit:focus { background: rgba(255,255,255,.1); color: #FFF; }
.placeholder-edit::-webkit-input-placeholder { color:#FFF; }
.placeholder-edit::-moz-placeholder { color:#FFF; }
.placeholder-edit::-ms-input-placeholder { color:#FFF; }
</style>
@endsection

@section('content')

<nav aria-label="breadcrumb" class="mb-4-5 p-3 bg-db rounded ashadow border">
	<ol class="breadcrumb p-0 m-0">
    	<li class="breadcrumb-item"><a href="{{ route('admin.pagina.lista') }}" class="color-blue-info-link">Páginas</a></li>
    	<li class="breadcrumb-item text-dark" aria-current="page">Home</li>
  	</ol>
</nav>

<section class="mb-4-5">

	<div class="section-title rounded-bottom-0 d-lg-flex justify-content-between align-items-center">
		<div>
			Seus Banners ({{ count($banners) }})
		</div>
		<div>
			<form method="POST" enctype="multipart/form-data">
			@csrf
		    	<label for="input-file" class="btn btn-outline-primary border-light text-light">
		    		+ Carregar novo Banner
		    		<span id='file-name'></span>
		    	</label>
		    	<input type="file" name="upload_banner" id="input-file" class="d-none" onchange="this.form.submit()">
		    </form>
	    </div>
	</div>

	<div class="collapse show border p-3 bg-white" id="boxUploadBanners">

		<div class="row">

			@foreach($banners as $banner)
				<div class="col-lg-4 mb-4">
					<a href="?editar_banner={{$banner->id}}">
						<div class="container-banner">
							<img src="{{ $banner->link }}" class="image-banner">
							@if($banner->overlay == 'S')
								<div class="background-overlay" style="background: {{ $banner->overlay_color ?? 'unset' }};opacity: {{ $banner->overlay_opacity ?? '0' }};"></div>
						  	@endif
						  	<div class="overlay-banner">
						    	<div class="text-banner">
						    		<i class="fa fa-edit fs-40 pb-5"></i>
						    	</div>
						  	</div>
						</div>
					</a>
				</div>
			@endforeach

		</div>

	</div>

</section>

<section class="mb-4-5">

	<div class="section-title rounded-bottom-0 d-lg-flex justify-content-between align-items-center">
		<div>
			Depoimentos / Alunos Formados ({{ count($depoimentos) }})
		</div>
		<div>
	    	<a href="#ModalDepoimentoAdd" data-bs-toggle="modal" class="btn btn-outline-primary border-light text-light">
	    		+ Adicionar novo
	    	</a>
	    </div>
	</div>

	<div class="collapse show border p-3 bg-white">

		@if(count($depoimentos) == 0)
			<p class="mb-0 color-777 weight-500">
				Nenhum depoimento cadastrado.
			</p>
		@else
        <div class="table-responsive">
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">Nome</th>
                        <th scope="col">Foto</th>
                        <th scope="col">Status</th>
                        <th scope="col">
                            <span class="float-end">
                                Ações
                            </span>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($depoimentos as $depoimento)
                        <tr>
                            <td>{{ $depoimento->nome }}</td>
                            <td>
                            	<img src="{{ $depoimento->foto }}" class="square-50 rounded-circle object-fit-cover">
                            </td>
                            <td>
                            	@if($depoimento->status == 0)
                            		Ativo
                            	@else
                            		Desativado
                            	@endif
                            </td>
                            <td>
                                <span class="float-end">
                                    <a href="?editar_depoimento={{$depoimento->id}}" class="aclNivel2 btn btn-warning text-dark">
                                        <i class="fa fa-edit"></i>
                                    </a>
                                    <a href="#ModalDepoimentoDeletar" data-bs-toggle="modal" class="aclNivel3 btn btn-danger" onclick="
                                    	document.getElementById('depoimento_delete_id').value = '{{ $depoimento->id }}';
                                    	document.getElementById('deletar_depoimento_nome').innerHTML = '{{ $depoimento->nome }}';
                                    ">
                                        <i class="fa fa-trash"></i>
                                    </a>
                                </span>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
		@endif

	</div>

</section>


<section class="mb-4-5" id="boxCursoSEO">

	<a href="#boxSeo" data-bs-toggle="collapse" aria-expanded="false" class="hover-d-none">
		<div class="section-title">
			Informações de SEO para resultados de buscas em Navegadores
		</div>
	</a>
	
	<form class="collapse show bg-white border p-4" id="boxSeo">
	@csrf

		<div class="mb-3">
			<label>Status da Página</label>
			<select name="status" class="select-form">
				<option value="0">Ativo</option>
				<option value="1" @if(isset($conteudo) && $conteudo->status == 1) selected @endif>Desativado</option>
			</select>
		</div>

		<div class="d-lg-flex mb-3">

			<div class="w-100 me-lg-3 mb-3 mb-lg-0">
				<label>Título</label>
				<input type="text" name="seo_title" class="form-control p-form" placeholder="Title" value="{{ $conteudo->seo_title ?? null }}">
			</div>

			<div class="w-100">
				<label>Palavras-chaves</label>
				<input type="text" name="seo_keywords" class="form-control p-form" placeholder="Keywords" value="{{ $conteudo->seo_keywords ?? null }}">
			</div>

		</div>

		<div class="w-100 mb-4">
			<label>Descrição</label>
			<input type="text" name="seo_description" class="form-control p-form" placeholder="Description" value="{{ $conteudo->seo_description ?? null }}">
		</div>

		<button type="submit" name="sent" value="S" class="btn-add">
			Salvar
		</button>

	</form>

</section>

@php
	$editar_banner = \App\Models\Banner::plataforma()->where('id', $_GET['editar_banner'] ?? null)->first();
@endphp
@if($editar_banner)
<form method="POST">
@csrf
<input type="hidden" name="banner_id" value="{{ $_GET['editar_banner'] ?? 0 }}">
<input type="hidden" name="link" value="{{ $editar_banner->link }}">
<div class="modal fade" id="ModalEditarBanner" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  	<div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    	<div class="modal-content">
      		<div class="modal-header">
      			<div class="w-100 d-flex justify-content-between align-items-center">
	        		<h3 class="modal-title w-lg-20 w-30" id="exampleModalCenterTitle">Editar Banner</h3>
	        		<a href="/admin/pagina/editar/home?delete_banner={{$editar_banner->id}}" class="aclNivel3 btn btn-danger weight-600 fs-12">
	        			<i class="fa fa-trash"></i>
	        			Deletar
	        		</a>
	        		<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
	        	</div>
      		</div>
      		<div class="modal-body p-0">
    			
    			<div class="d-flex align-items-end" style="background-image: url('{{ $editar_banner->link }}');height: 250px;background-size: cover;">
    			<div id="bg-overlay-editar-banner" class="background-overlay" @if($editar_banner->overlay == 'N') style="display: none;" @endif style="background: {{ $editar_banner->overlay_color ?? '#000' }};opacity: {{ $editar_banner->overlay_opacity ?? '0.7' }};"></div>

    				<div class="w-100 p-4 position-relative">
	    				<input type="text" name="titulo" class="form-control p-form placeholder-edit" placeholder="Título" value="{{ $editar_banner->titulo }}">
	    				<input type="text" name="subtitulo" class="my-2 form-control p-form placeholder-edit" placeholder="Slogan" value="{{ $editar_banner->subtitulo }}">
	    				<div class="d-flex">
	    					<input type="text" name="botao_texto" class="w-lg-25 form-control p-form placeholder-edit rounded-right-0" placeholder="Texto do Botão" value="{{ $editar_banner->botao_texto }}">
	    					<input type="text" name="botao_link" class="form-control p-form placeholder-edit rounded-left-0" placeholder="Link do Botão" value="{{ $editar_banner->botao_link }}">
	    				</div>
	    			</div>

    			</div>

      		</div>
      		<div class="d-flex p-4 justify-content-between">
      			<select name="status" class="w-lg-45 select-form">
      				<option value="0">Ativo</option>
      				<option value="1" @if($editar_banner->status == 1) selected @endif>Desativado</option>
      			</select>
      			<select name="overlay" onchange="controlOverlayEditarBanner(this.value)" class="ms-lg-3 w-lg-50 select-form">
      				<option value="N">Sem película</option>
      				<option value="S" @if($editar_banner->overlay == 'S') selected @endif>Com película</option>
      			</select>
      			<div @if($editar_banner->overlay == 'N') style="display: none;" @endif id="boxOverlayEditarBanner" class="ms-lg-3  w-lg-80">
	      			<div class="d-flex align-items-center">
	      				<input type="color" name="overlay_color" id="inputColorEditar" style="height: 43px;width: 80px;" class="rounded-right-0" value="{{ $editar_banner->overlay_color ?? '#000' }}">
	      				<input type="text" list="listOpacidades" name="overlay_opacity" onkeyup="setOpacityEditarBanner(this.value)" class="form-control p-form rounded-left-0" placeholder="N° de Opacidade" value="{{ $editar_banner->overlay_opacity ?? '0.7' }}">
	      				<datalist id="listOpacidades">
	      					<option value="0.1">0.1</option>
	      					<option value="0.2">0.2</option>
	      					<option value="0.3">0.3</option>
	      					<option value="0.4">0.4</option>
	      					<option value="0.5">0.5</option>
	      					<option value="0.6">0.6</option>
	      					<option value="0.7">0.7</option>
	      					<option value="0.8">0.8</option>
	      					<option value="0.9">0.9</option>
	      					<option value="1">1</option>
	      				</datalist>
	      			</div>
	      		</div>
		        <input type="text" name="ordem" class="ms-lg-3 w-lg-40 number form-control p-form" placeholder="N° de Ordem" value="{{ $editar_banner->ordem }}">
      			<button type="submit" name="editarBanner" value="S" class="ms-lg-3 btn btn-primary weight-600">
      				Salvar
      			</button>
      		</div>
    	</div>
  	</div>
</div>
</form>
@endif

@php
	$delete = \App\Models\Banner::plataforma()->where('id', $_GET['delete_banner'] ?? null)->first();
@endphp
@if($delete)
<form method="POST">
@csrf
    <input type="hidden" name="delete_id" id="delete_id" value="{{ $delete->id }}">
    <div class="modal fade" id="ModalDeletarBanner" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h3 class="modal-title color-444 font-nunito weight-600">Deletar Banner</h3>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body p-0">
            
    		<div class="d-flex align-items-end" style="background-image: url('{{ $delete->link }}');height: 250px;background-size: cover;">
            <div class="background-overlay" @if($delete->overlay == 'N') style="display: none;" @endif style="background: {{ $delete->overlay_color ?? '#000' }};opacity: {{ $delete->overlay_opacity ?? '0.7' }};"></div>

				<div class="w-100 px-4 py-5 position-relative">
						
					@if($delete->titulo)
						<h1 id="bannerTitle" class="text-white font-poppins weight-800 text-shadow-1">
							{{ $delete->titulo }}
						</h1>
					@endif
					@if($delete->subtitulo)
						<p id="bannerSubtitle" class="text-white font-poppins fs-17">
							{{ $delete->subtitulo }}
						</p>
					@endif

					@if($delete->botao_texto)
						<div class="mt-4">
							<a href="{{ $delete->botao_link }}" class="btn btn-primary weight-600 py-2">
								{{ $delete->botao_texto }}
							</a>
						</div>
					@endif

    			</div>

        	</div>

          </div>
          <div class="modal-footer justify-content-between">
            <p class="mb-0 weight-600 text-dark fs-17">
            	Você tem certeza que deseja deletar este banner permanentemente?
            </p>
            <button type="submit" name="deletarBanner" value="S" class="btn btn-danger weight-600">
                Confirmar
            </button>
          </div>
        </div>
      </div>
    </div>
</form>
@endif


<form method="POST" enctype="multipart/form-data">
@csrf
    <div class="modal fade" id="ModalDepoimentoAdd" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      	<div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        	<div class="modal-content">
	          	<div class="modal-header">
	            	<h3 class="modal-title color-444 font-nunito weight-600">Adicionar Depoimento</h3>
	            	<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
	          	</div>
	          	<div class="modal-body">
					
					<div class="d-lg-flex mb-3">

		       			<div class="w-100 me-lg-3 mb-3 mb-lg-0">
		       				<label>Nome</label>
		       				<input type="text" name="nome" class="form-control p-form" placeholder="Nome" required>
		       			</div>
		       			<div class="w-lg-50">
		       				<label>Foto</label>
		       				<input type="file" name="foto_resp" class="file-form" required>
		       			</div>
		       			<div class="w-lg-35 my-3 my-lg-0 mx-lg-3">
		       				<label>Ordem</label>
		       				<input type="number" name="ordem" class="form-control p-form" placeholder="N° de Ordem">
		       			</div>
				    	<div class="w-lg-35">
				    		<label>Status</label>
				    		<select name="status" class="select-form">
				    			<option value="0">Ativo</option>
				    			<option value="1">Desativado</option>
				    		</select>
				    	</div>

				    </div>

				    <div class="w-100">
				    	<label>Descrição</label>
				    	<textarea name="descricao" class="textarea-form" placeholder="Descreva o depoimento"></textarea>
				    </div>

	          	</div>
          		<div class="modal-footer">
            		<button type="submit" name="addDepoimento" value="S" class="btn btn-primary py-2 weight-600">
                		Confirmar
            		</button>
          		</div>
	        </div>
      	</div>
    </div>
</form>

@php
	$editar_depoimento = \App\Models\Depoimento::plataforma()->where('id', $_GET['editar_depoimento'] ?? null)->first();
@endphp
@if($editar_depoimento)
<form method="POST" enctype="multipart/form-data">
@csrf
	<input type="hidden" name="depoimento_id" value="{{ $editar_depoimento->id }}">
    <div class="modal fade" id="ModalDepoimentoEditar" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      	<div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        	<div class="modal-content">
	          	<div class="modal-header">
	            	<h3 class="modal-title color-444 font-nunito weight-600">Editar Depoimento</h3>
	            	<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
	          	</div>
	          	<div class="modal-body">
					
					<div class="d-lg-flex mb-3">

		       			<div class="w-100 me-lg-3 mb-3 mb-lg-0">
		       				<label>Nome</label>
		       				<input type="text" name="nome" class="form-control p-form" placeholder="Nome" required value="{{ $editar_depoimento->nome }}">
		       			</div>
		       			<div class="w-lg-50">
		       				<label>
		       					Foto
								@if($editar_depoimento->foto)
									<a href="{{ $editar_depoimento->foto }}" class="weight-600 fs-11" target="_blank">
										visualizar anexo
									</a>
								@endif
		       				</label>
		       				<input type="file" name="foto_resp" class="file-form">
		       			</div>
		       			<div class="w-lg-35 my-3 my-lg-0 mx-lg-3">
		       				<label>Ordem</label>
		       				<input type="number" name="ordem" class="form-control p-form" placeholder="N° de Ordem" value="{{ $editar_depoimento->ordem }}">
		       			</div>
				    	<div class="w-lg-35">
				    		<label>Status</label>
				    		<select name="status" class="select-form">
				    			<option value="0">Ativo</option>
				    			<option value="1" @if($editar_depoimento->status == 1) selected @endif>Desativado</option>
				    		</select>
				    	</div>

				    </div>

				    <div class="w-100">
				    	<label>Descrição</label>
				    	<textarea name="descricao" class="textarea-form" placeholder="Descreva o depoimento">{{ $editar_depoimento->descricao }}</textarea>
				    </div>

	          	</div>
          		<div class="modal-footer">
            		<button type="submit" name="editarDepoimento" value="S" class="btn btn-primary py-2 weight-600">
                		Salvar Alterações
            		</button>
          		</div>
	        </div>
      	</div>
    </div>
</form>
@endif

<form method="POST">
	@csrf
	<input type="hidden" name="depoimento_delete_id" id="depoimento_delete_id">
	<div class="modal fade" id="ModalDepoimentoDeletar" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  		<div class="modal-dialog modal-dialog-centered">
    		<div class="modal-content">
      			<div class="modal-header">
        			<h3 class="modal-title" id="exampleModalLabel">Deletar Depoimento</h3>
        			<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      			</div>
      			<div class="modal-body text-center py-5">
      				<p class="mb-0 weight-600" id="deletar_depoimento_nome"></p>
      				<hr/>
        			<p class="m-0 fs-16">
        				Você tem certeza que deseja deletar este depoimento?
        				<br>
        				Todas as informações serão perdidas definitivamente.
        			</p>
      			</div>
      			<div class="modal-footer">
	        		<button type="submit" name="deletarDepoimento" value="S" class="btn btn-danger weight-600">Confirmar</button>
      			</div>
    		</div>
  		</div>
	</div>
</form>

<script>

	function controlOverlayEditarBanner(value)
	{
		if(value == 'S')
		{
			document.getElementById('bg-overlay-editar-banner').style.display = 'block';
			document.getElementById('boxOverlayEditarBanner').style.display = 'block';
			document.getElementById('bg-overlay-editar-banner').style.backgroundColor = document.getElementById('inputColorEditar').value;
			
		} else {

			document.getElementById('bg-overlay-editar-banner').style.display = 'none';
			document.getElementById('boxOverlayEditarBanner').style.display = 'none';
		}
	}

	function setOpacityEditarBanner(value)
	{
		document.getElementById('bg-overlay-editar-banner').style.opacity = value;
	}

	@if($editar_banner)

	    var theInputEditar = document.getElementById("inputColorEditar");
	    theInputEditar.addEventListener("input", function() {
	    document.getElementById('bg-overlay-editar-banner').style.backgroundColor = theInputEditar.value;
	    }, false);

	@endif

</script>

@push('scripts')
<script>
	@if(isset($_GET['editar_banner']))
		var myModal = new bootstrap.Modal(document.getElementById("ModalEditarBanner"), {});
		document.onreadystatechange = function () {
		  myModal.show();
		};
	@endif
	@if(isset($_GET['delete_banner']))
		var myModal = new bootstrap.Modal(document.getElementById("ModalDeletarBanner"), {});
		document.onreadystatechange = function () {
		  myModal.show();
		};
	@endif
	@if(isset($_GET['editar_depoimento']))
		var myModal = new bootstrap.Modal(document.getElementById("ModalDepoimentoEditar"), {});
		document.onreadystatechange = function () {
		  myModal.show();
		};
	@endif
</script>
@endpush

@endsection