<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use App\Models\Plataforma;


class APIPlataformaController extends Controller
{
    public function auth($api_key)
    {
        $plataforma_idIFOK = Plataforma::select('id','api_key')->where('api_key', $api_key)->pluck('id')[0] ?? null;
 
        if($plataforma_idIFOK == null)
        {
            return 0;

        } else {

            return $plataforma_idIFOK;
        }
    }

    public function info(Request $request)
    {
        $plataforma_id = $this->auth($request->api_key);

        if($plataforma_id === 0)
        {
            return response()->json(['mensagem' => 'API KEY não autorizado.'], 403);
        }


        $plataforma = Plataforma::where('id', $plataforma_id)->first();

        if(is_null($plataforma))
        {
            return response()->json(['mensagem' => 'Aluno não encontrado.'], 404);
        }

        return response()->json(['data' => $plataforma]);
    }
}