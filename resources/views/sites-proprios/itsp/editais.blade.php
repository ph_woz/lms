<!doctype html>
<html lang="pt-br">
<head>

  <!-- Google tag (gtag.js) -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=G-MREB450HCT"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'G-MREB450HCT');
  </script>
  
   <meta charset="utf-8">
   <meta name="viewport" content="width=device-width, initial-scale=1">
   
   <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/uikit@3.5.9/dist/css/uikit.min.css" />
   <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
   <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
   <link rel="stylesheet" href="{{ asset('css/util.css') }}?v=3">
   <link rel="stylesheet" href="{{ asset('sites-proprios/cedetep/css/home.css') }}">

   <title>{{ $seo->seo_title ?? 'Editais ' . $plataforma->nome }}</title>
   <meta name="robots" content="index, follow">
   <meta name="description" content="{{ $seo->seo_description ?? null }}"/>
   <meta name="keywords" content="{{ $seo->seo_keywords ?? null }}"/>
   <link rel="canonical" href="{{ Request::url() }}" />
   <link rel="shortcut icon" href="{{ $plataforma->favicon }}">
   <meta property="og:locale" content="pt_BR" />
   <meta property="og:type" content="website" />
   <meta property="og:title" content="{{ $seo->seo_title ?? $plataforma->nome }}" />
   <meta property="og:description" content="{{ $seo->seo_description ?? null }}" />
   <meta property="og:url" content="{{ Request::url() }}" />
   <meta property="og:site_name" content="{{ $plataforma->nome }}" />
   <meta property="og:image" content="{{ $plataforma->logotipo }}" />
   <meta name="author" content='WOZCODE | Pedro Henrique' />

</head>
<body class="bg-f4" id="inicio">

   @include('sites-proprios/itsp/componentes/header')

   <main>

        <section>
            <div class="w-100 h-200 background-size-cover background-position-center" style="background-image: url('{{ $plataforma->jumbotron ?? asset('images/utils/bg-page-header.webp') }}');">
                <div class="position-relative h-100 px-xl-7 px-3">
                    <div class="background-overlay opacity-08 h-100" style="background: rgba(0,0,0,1);"></div>
                    <div class="row container align-items-center h-100 position-relative">
                        <div class="mt-2">
                            <h1 class="text-light font-poppins weight-800 fs-xs-35 fs-md-50 text-shadow-3">
                                Editais
                            </h1>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="py-5">
         <div class="container pb-5">
         
            <div>

                  <div class="bg-white p-3 box-shadow rounded">

                     <form>

                        <div class="d-flex mb-3">

                           <div class="w-100 me-3">
                              <input name="search" type="search" class="filtro-search-curso rounded" placeholder="Buscar" value="{{ $search }}">
                           </div>

                           <button type="submit" class="bg-verde-gradient text-white d-block text-center px-4 rounded text-dark weight-700 font-poppins hover-d-none border-0">
                              <i class="fa fa-search"></i>
                           </button>

                        </div>

                     </form>

                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col" class="border-th font-poppins weight-600 color-titulo fs-15">Referência</th>
                                    <th scope="col" class="border-th font-poppins weight-600 color-titulo fs-15">Publicação</th>
                                    <th scope="col" class="border-th font-poppins weight-600 color-titulo fs-15">Abrir</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($documentos as $documento)
                                    <tr>
                                        <td class="w-lg-75 font-poppins fs-14">{{ $documento->nome }}</td>
                                        <td class="w-lg-20 font-poppins fs-14">{{ \App\Models\Util::replaceDatePt($documento->data_publicacao) }}</td>
                                        <td>
                                          <a href="{{ $documento->link }}" target="_blank" class="btn-azul px-3 py-2 fs-11 weight-700 font-poppins ls-05 hover-d-none">
                                              DOWNLOAD
                                          </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                  </div>

            </div>

         </div>
        </section>

   </main>

   @include('sites-proprios/itsp/componentes/footer')

   <a href="https://api.whatsapp.com/send?phone=55{{preg_replace('/\D/', '', $plataforma->whatsapp)}}&text=Olá, vim através do Site {{ $plataforma->nome }}" target="_blank" class="text-white fs-30">
       <div class="fixed-bottom right-0 me-3 mb-3 z-4">
           <div class="square-60 rounded-circle bg-wpp flex-center box-shadow-wpp">
               <i class="fab fa-whatsapp"></i>
           </div>
       </div>
   </a>


   <script src="https://cdn.jsdelivr.net/npm/uikit@3.5.9/dist/js/uikit.min.js"></script>
   <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa" crossorigin="anonymous"></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.1/jquery.min.js" integrity="sha512-aVKKRRi/Q/YV+4mjoKBsE4x3H+BkegoM/em46NNlCqNTmUYADjBbeNefNxYV7giUp0VxICtqdrbqU7iVaeZNXA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

</body>
</html>