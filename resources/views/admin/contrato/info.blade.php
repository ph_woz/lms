@extends('layouts.admin.master')
@section('content')
    
    @livewire('admin.contrato.info', ['contrato_id' => $contrato_id])

@endsection

@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.js"></script>
<script>
var myModal = new bootstrap.Modal(document.getElementById("ModalClienteEscolhidoGerarContrato"), {});
document.onreadystatechange = function () {
  myModal.show();
};
</script>
<script>
	$('.number').mask('0000000');
</script>
<script>
	window.addEventListener('dissmiss-modal', event => {
		$(".modal").modal('hide');
	});
</script>
@endpush