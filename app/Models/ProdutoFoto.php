<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProdutoFoto extends Model
{
    protected $table = 'produtos_fotos';

    public $timestamps = false;

    public $fillable = [
    	'plataforma_id',
    	'produto_id', 
    	'foto',
        'ordem',
    ];

    public static  function boot()
    {
        parent::boot(); 
        static::creating(function ($model) {
            $model->plataforma_id = session('plataforma_id') ?? \Auth()->user()->plataforma_id;
        });
    }

    public function scopePlataforma($query)
    {
        return $query->where('plataforma_id', session('plataforma_id') ?? \Auth::user()->plataforma_id);
    }
}
