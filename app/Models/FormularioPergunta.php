<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FormularioPergunta extends Model
{
    use HasFactory;

    protected $table = 'formularios_perguntas';

    public $timestamps = false;

    /*
		
		tipo_input  = opções {
			
			Múltipla Escolha  -  radio
			Caixas de Seleção -  checkbox
			Dissertativa      -  text
			Lista             -  select
		}

    */

    public $fillable = [
    	'plataforma_id',
    	'formulario_id',
    	'pergunta',
        'tipo_input',
        'options',
        'validation',
        'obrigatorio',
        'ordem',
    ];

    public static  function boot()
    {
        parent::boot(); 
        static::creating(function ($model)
        {
            if(\Auth::check())
                $model->plataforma_id = session('plataforma_id') ?? \Auth()->user()->plataforma_id ?? Plataforma::dominio()->pluck('id')[0] ?? null;
        });
    }

    public function getPlataforma()
    {
        return $this->belongsTo(Plataforma::class, 'plataforma_id');
    }
    
    public function scopePlataforma($query)
    {
        return $query->where('plataforma_id', session('plataforma_id') ?? Plataforma::dominio()->pluck('id')[0] ?? null);
    }

    public function tipoInput($tipo)
    {
        switch ($tipo) {
            case 'radio':
                return 'Múltipla Escolha';

            case 'checkbox':
                return 'Caixas de Seleção';

            case 'select':
                return 'Lista';

            case 'file':
                return 'Arquivo';

            default:
                return 'Dissertativa';
        }
    }
}
