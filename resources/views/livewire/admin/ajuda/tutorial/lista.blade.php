<div class="row">
    
    @foreach($tutoriais as $tutorial) 

        <article class="col-md-6 col-lg-4 col-xl-3">
            <a href="#" data-bs-toggle="modal" data-bs-target="#ModalVideo" class="hover-d-none" onclick="document.getElementById('tutorial-iframe').src = '{{ $tutorial->link }}';">
                <div class="card box-shadow cursor-pointer pb-0">

                    <img src="{{ $tutorial->capa ?? asset('images/no-image.jpeg') }}" class="card-img-top object-fit-cover object-position-top h-150">
                    
                    <div class="p-3 mb-1 text-center flex-center h-100px">

                        <h1 class="color-333 mb-1 weight-600 text-center fs-16">
                            {{ $tutorial->titulo }}
                        </h1>

                    </div>

                </div>
            </a>
        </article>

    @endforeach 

    <div class="modal fade" id="ModalVideo" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-xl modal-dialog-centered" role="document">
            <div class="modal-content">
                
                <iframe width="100%" class="h-lg-90vh h-xs-400 border border-light" id="tutorial-iframe" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
           
            </div>
        </div>
    </div>

</div>