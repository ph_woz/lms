<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AvaliacaoAlunoResposta extends Model
{
    use HasFactory;

    protected $table = 'avaliacoes_alunos_respostas';

    public $timestamps = false;

    public $fillable = [
        'plataforma_id',
        'avaliacao_id',
        'turma_id',
        'aluno_id',
        'questao_id',
        'alternativa_id',
        'acertou', // S, N / Em correção
        'resposta_dissertativa',
        'resposta_dissertativa_correcao',
    ];

    public static  function boot()
    {
        parent::boot(); 
        static::creating(function ($model)
        {
            $model->plataforma_id = session('plataforma_id') ?? \Auth()->user()->plataforma_id ?? Plataforma::dominio()->pluck('id')[0] ?? null;
        });
    }

    public function scopePlataforma($query)
    {
        return $query->where('plataforma_id', session('plataforma_id') ?? Plataforma::dominio()->pluck('id')[0] ?? null);
    }

}
