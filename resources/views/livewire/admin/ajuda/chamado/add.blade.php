<div>

    <div class="col-12">
        @if (session()->has('success-livewire'))
            <div class="alert alert-success alert-dismissible" role="alert">
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                <div class="alert-message">
                	Chamado criado:
                    <span>
	                    <a href="{{ route('admin.ajuda.chamado.info', ['id' => $this->chamado->id ?? null]) }}" class="weight-600">
	                    	{{ $chamado->assunto }} 
	                	</a>
	                </span>
                </div>
            </div>
        @endif
    </div>

    <form wire:submit.prevent="add" method="POST" class="mb-4">

		<section class="mb-4-5">
			
			<a href="#boxInfoPrincipal" data-bs-toggle="collapse" aria-expanded="false" class="hover-d-none">
				<div class="section-title">
					Informações Principais
				</div>
			</a>
			<hr/>
			
			<div class="collapse show" id="boxInfoPrincipal">

		    	<div class="w-100 mb-3">
		    		<label>Assunto</label>
		    		<input type="text" wire:model="assunto" class="form-control p-form" placeholder="Do que se refere o chamado" required>
		    	</div>

			    <div class="mb-3">
			    	<label>Descrição</label>
			    	<textarea wire:model="descricao" class="textarea-form" placeholder="Descreva" required></textarea>
			    </div>

			</div>

		</section>

		<button type="submit" class="btn-add mt-3">Cadastrar</button>	

    </form>

    @include('livewire.util.toast')

</div>
