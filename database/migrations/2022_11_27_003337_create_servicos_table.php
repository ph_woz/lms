<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServicosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('servicos', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('plataforma_id');
            $table->unsignedBigInteger('cadastrante_id')->nullable();
            $table->unsignedBigInteger('unidade_id')->nullable();
            $table->string('nome');
            $table->text('descricao')->nullable();
            $table->integer('status')->default(0);
            $table->string('publicar', 1)->default('S');
            $table->string('duracao')->nullable();
            $table->string('valor')->nullable();
            $table->string('valor_promocional')->nullable();
            $table->string('tipo_valor', 1)->default('F');
            $table->integer('ordem')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('servicos');
    }
}
