<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AlunoAulaConcluida extends Model
{
    use HasFactory;

    protected $table = 'alunos_aulas_concluidas';

    public $timestamps = true;

    public $fillable = [
    	'plataforma_id',
    	'aluno_id',
    	'curso_id',
    	'modulo_id',
    	'aula_id',
    ];

    public function scopePlataforma($query)
    {
        return $query->where('plataforma_id', session('plataforma_id') ?? Plataforma::dominio()->pluck('id')[0] ?? null);
    }
    
    public static  function boot()
    {
        parent::boot(); 
        static::creating(function ($model) {
            $model->plataforma_id = session('plataforma_id') ?? \Auth()->user()->plataforma_id ?? Plataforma::dominio()->pluck('id')[0] ?? null;
        });
    }
}
