<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHorariosFuncionamentosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('horarios_funcionamentos', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('plataforma_id'); 
            $table->string('dia');
            $table->string('de', 5)->nullable();
            $table->string('ate', 5)->nullable();
            $table->string('fechado', 1)->default('N');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('horarios_funcionamentos');
    }
}
