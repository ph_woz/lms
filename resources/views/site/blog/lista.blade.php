@extends('layouts.site')

@section('css')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/uikit@3.5.9/dist/css/uikit.min.css" />
<style>

@media(min-width: 990px) {
    .w-lg-700px { width: 700px; }
}

</style>
@endsection

@section('seo_title', $pagina->seo_title ?? $nomePlataforma)
@section('seo_description', $pagina->seo_description ?? null)
@section('seo_keywords', $pagina->seo_keywords ?? null)

@section('content')
<section id="blog" class="py-5">
   <div class="container py-5">

      <div class="d-lg-none mb-5 mb-lg-0">

        <div class="uk-position-relative uk-visible-toggle" tabindex="-1" uk-slideshow="animation: push">

            <ul class="uk-slideshow-items">
                <li>
                    <img src="{{ $postDestaque1->foto_capa ?? asset('images/no-image.jpeg') }}" alt="{{ $postDestaque1->titulo ?? null }} | Blog" uk-cover>
                    <div class="overlay-dark uk-position-cover rounded"></div>
                    <div class="uk-position-center uk-position-small uk-text-center uk-light">
                        <h2 class="uk-margin-remove">{{ $postDestaque1->titulo ?? null }}</h2>
                    </div>
                </li>
                <li>
                    <img src="https://yata-apix-528e58cd-14b8-445a-beb7-a7f32b12d386.s3-object.locaweb.com.br/7a1c89c31688445b9d497e284460ecc3.jpg" alt="" uk-cover>
                    <div class="overlay-dark uk-position-cover rounded"></div>
                    <div class="uk-position-center uk-position-small uk-text-center uk-light">
                        <h2 class="uk-margin-remove">Center</h2>
                    </div>
                </li>
                <li>
                    <img src="https://yata-apix-528e58cd-14b8-445a-beb7-a7f32b12d386.s3-object.locaweb.com.br/7a1c89c31688445b9d497e284460ecc3.jpg" alt="" uk-cover>
                    <div class="overlay-dark uk-position-cover rounded"></div>
                    <div class="uk-position-center uk-position-small uk-text-center uk-light">
                        <h2 class="uk-margin-remove">Center</h2>
                    </div>
                </li>
            </ul>

            <div class="uk-light">
                <a class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous uk-slideshow-item="previous"></a>
                <a class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-next uk-slideshow-item="next"></a>
            </div>

        </div>

      </div>

      <div class="d-md-none d-xs-none d-lg-block pt-5">

        <div class="w-100 d-flex">

           <div>
              <a href="">
                <div class="uk-inline-clip uk-transition-toggle" tabindex="0">
                    <img class="w-lg-700px h-lg-425 uk-transition-scale-up uk-transition-opaque object-fit-cover rounded" src="{{ $postDestaque1->foto_capa ?? asset('images/no-image.jpeg') }}" alt="{{ $postDestaque1->titulo ?? null }} | Blog">
                    <div class="overlay-dark uk-position-cover rounded"></div>
                    <div class="uk-overlay uk-position-top uk-light text-start">
                       <h1 class="p-lg-4 font-montserrat weight-800 text-shadow-3 text-white fs-md-25 fs-xs-20">
                          {{ $postDestaque1->titulo ?? null }}
                       </h1>
                    </div>
                </div>
              </a>
           </div>

           <div class="w-100 d-flex flex-column">

              <div class="pb-2 ps-2">
                 <a href="">
                   <div class="uk-inline-clip uk-transition-toggle w-100" tabindex="0">
                       <img class="uk-transition-scale-up uk-transition-opaque object-fit-cover rounded w-100" src="https://yata-apix-528e58cd-14b8-445a-beb7-a7f32b12d386.s3-object.locaweb.com.br/7a1c89c31688445b9d497e284460ecc3.jpg" alt="" style="height: 208px;">
                       <div class="overlay-dark uk-position-cover rounded"></div>
                       <div class="uk-overlay uk-position-top uk-light text-start">
                          <h1 class="p-4 font-montserrat weight-700 text-shadow-3 text-white fs-35">
                             Projeto de Reflorestamento  FARO
                          </h1>
                       </div>
                   </div>
                 </a>
              </div>

              <div class="w-100 d-flex w-100">

                 <div class="w-100 px-2">
                    <a href="">
                      <div class="uk-inline-clip uk-transition-toggle w-100" tabindex="0">
                          <img class="uk-transition-scale-up uk-transition-opaque object-fit-cover rounded w-100" src="https://yata-apix-528e58cd-14b8-445a-beb7-a7f32b12d386.s3-object.locaweb.com.br/7a1c89c31688445b9d497e284460ecc3.jpg" alt="" style="height: 208px;">
                          <div class="overlay-dark uk-position-cover rounded"></div>
                          <div class="uk-overlay uk-position-top uk-light text-start">
                             <h1 class="p-4 font-montserrat weight-700 text-shadow-3 text-white fs-25">
                                Projeto de Reflorestamento  FARO
                             </h1>
                          </div>
                      </div>
                    </a>
                 </div>
                 <div class="w-100">
                    <a href="">
                      <div class="uk-inline-clip uk-transition-toggle w-100" tabindex="0">
                          <img class="uk-transition-scale-up uk-transition-opaque object-fit-cover rounded w-100" src="https://yata-apix-528e58cd-14b8-445a-beb7-a7f32b12d386.s3-object.locaweb.com.br/7a1c89c31688445b9d497e284460ecc3.jpg" alt="" style="height: 208px;">
                          <div class="overlay-dark uk-position-cover rounded"></div>
                          <div class="uk-overlay uk-position-top uk-light text-start">
                             <h1 class="p-4 font-montserrat weight-700 text-shadow-3 text-white fs-25">
                                Projeto de Reflorestamento  FARO
                             </h1>
                          </div>
                      </div>
                    </a>
                 </div>

              </div>

           </div>

        </div>

      </div>

   </div>
</section>
@endsection

@push('scripts')
<script src="https://cdn.jsdelivr.net/npm/uikit@3.5.9/dist/js/uikit.min.js"></script>
@endpush