<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ServicoCategoria extends Model
{
    protected $table = 'servicos_categorias';

    public $timestamps = false;

    public $fillable = [
    	'plataforma_id',
    	'categoria_id',
    	'servico_id', 
    ];

    public static  function boot()
    {
        parent::boot(); 
        static::creating(function ($model) {
            $model->plataforma_id = session('plataforma_id') ?? \Auth()->user()->plataforma_id;
        });
    }

    public function scopePlataforma($query)
    {
        return $query->where('plataforma_id', session('plataforma_id') ?? \Auth::user()->plataforma_id);
    }
}
