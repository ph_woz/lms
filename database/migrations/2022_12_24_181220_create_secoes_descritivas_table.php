<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSecoesDescritivasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('secoes_descritivas', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('plataforma_id');
            $table->unsignedBigInteger('registro_id')->nullable();
            $table->string('tipo')->default('T'); // T(rilha) or (C)lass/Turma
            $table->string('titulo')->nullable();
            $table->text('descricao')->nullable();
            $table->string('ordem')->nullable();
            $table->string('expandido', 1)->default('N');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('secoes_descritivas');
    }
}
