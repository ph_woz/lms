<?php

namespace App\Http\Livewire\Admin\Contato;

use Livewire\Component;
use App\Models\Contato;
use App\Models\Categoria;
use App\Models\ContatoCategoria;
use App\Models\Curso;
use App\Models\Turma;
use App\Models\TurmaAluno;
use App\Models\TrilhaAluno;
use App\Models\Trilha;
use App\Models\Formulario;
use App\Models\Plataforma;
use App\Models\User;
use App\Models\Unidade;
use App\Models\ContatoDocumento;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;

class Info extends Component
{
    public $contato;
    public $formulario;
    public $trilha;
    public $turma;
    public $cadastrante;
    public $unidade;
    public $categorias;
    public $password_to_delete;
    
    public $user_id;

    // Editar
    public $anotacao;
    public $status;
    public $arquivado;
    public $categoriasSelected = [];

    public $resposta_assunto;
    public $resposta_descricao;

    public function mount($contato_id)
    {
        $this->contato = Contato::plataforma()->find($contato_id);

        $this->cadastrante = User::plataforma()->where('id', $this->contato->cadastrante_id)->first();
        $this->formulario  = Formulario::plataforma()->where('id', $this->contato->formulario_id)->first();
        $this->unidade     = Unidade::plataforma()->where('id', $this->contato->unidade_id)->first();

        $this->anotacao  = $this->contato->anotacao;
        $this->status    = $this->contato->status;
        $this->arquivado = $this->contato->arquivado;

        $this->resposta_assunto = 'RE: ' . $this->contato->mensagem;

        $this->categorias = Categoria::plataforma()->ativo()->tipo('formulario')->orderBy('nome')->get();
    }

    public function render()
    {
        $categoriasIds = ContatoCategoria::plataforma()->where('contato_id', $this->contato->id)->get()->pluck('categoria_id')->toArray();
        $categoriasSelecionadas = Categoria::plataforma()->ativo()->tipo('formulario')->whereIn('id', $categoriasIds)->select('nome')->orderBy('nome')->get()->pluck('nome')->toArray();
        $curso = Curso::plataforma()->where('id', $this->contato->curso_id)->first();
        $documentos = ContatoDocumento::plataforma()->where('contato_id', $this->contato->id)->get();

        $this->user_id = User::plataforma()->where('email', $this->contato->email)->pluck('id')[0] ?? null;

        if($this->contato->tipo == 'L')
        {
            if($this->contato->turma_id)
            {
                $this->turma = Turma::plataforma()->where('id', $this->contato->turma_id)->first();

                $countCheck = TurmaAluno::plataforma()->where('aluno_id', $this->user_id)->where('turma_id', $this->contato->turma_id ?? null)->count();
                
            } else {

                $this->trilha = Trilha::plataforma()->where('id', $this->contato->trilha_id)->first();

                $countCheck = TrilhaAluno::plataforma()->where('aluno_id', $this->user_id)->where('trilha_id', $this->contato->trilha_id ?? null)->count();                
            }
        }

        return view('livewire.admin.contato.info', 
            [
                'categoriasSelecionadas' => $categoriasSelecionadas, 
                'curso'                  => $curso,
                'countCheck'             => $countCheck ?? 0,
                'documentos'             => $documentos,
            ]);
    }

    public function editar()
    {
        $this->contato->update([
            'anotacao'  => $this->anotacao,
            'status'    => $this->status,
            'arquivado' => $this->arquivado,
        ]);

        $this->syncCategorias($this->contato->id);

        session()->flash('success-livewire', 'Contato atualizado com êxito.');

        $this->dispatchBrowserEvent('dissmiss-modal');
        $this->dispatchBrowserEvent('toast');
    }

    public function syncCategorias($contato_id)
    {
        ContatoCategoria::plataforma()->where('contato_id', $contato_id)->delete();

        if($this->categoriasSelected == null)
            return;

        foreach($this->categoriasSelected as $categoria_id)
        {
            $countDuplicate = ContatoCategoria::plataforma()->where('contato_id', $contato_id)->where('categoria_id', $categoria_id)->count();
            
            if($countDuplicate <= 0)
                ContatoCategoria::create([
                    'plataforma_id' => session('plataforma_id'), 
                    'contato_id'    => $contato_id, 
                    'categoria_id'  => $categoria_id,
                ]);
        }
    }

    public function addRespostaAoAluno()
    {
        $comunicado = \App\Models\Comunicado::create([
            'assunto'   => $this->resposta_assunto,
            'descricao' => $this->resposta_descricao,
        ]);

        \App\Models\ComunicadoAluno::create([
            'comunicado_id' => $comunicado->id,
            'aluno_id'      => $this->user_id,
        ]);

        $this->resposta_assunto   = null;
        $this->resposta_descricao = null;

        session()->flash('success-livewire', 'Resposta enviada para o aluno com êxito.');

        $this->dispatchBrowserEvent('dissmiss-modal');
        $this->dispatchBrowserEvent('toast');
    }

    public function delete()
    {
        if(\Auth::attempt(['id' => \Auth::id(), 'password' => $this->password_to_delete, 'status' => 0]))
        {
            ContatoCategoria::plataforma()->where('contato_id', $this->contato->id)->delete();
            Contato::plataforma()->where('id', $this->contato->id)->delete();

            $documentos = ContatoDocumento::plataforma()->where('contato_id', $this->contato->id)->get();
            foreach($documentos as $documento)
            {
                $path = str_replace(\App\Models\Util::getLinkStorage(), '', $documento->link);
                Storage::delete($path);
                $documento->delete();
            }

            session()->flash('success', 'Contato deletado com êxito.');
            
            return redirect(route('admin.contato.lista'));

        } else {

            session()->flash('danger-livewire', 'Senha incorreta.');

            $this->dispatchBrowserEvent('toast');
        }
    }
}
