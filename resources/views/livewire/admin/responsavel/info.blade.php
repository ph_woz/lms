<div>

	<div class="d-flex justify-content-end align-items-center mb-3">
    
		<a href="{{ route('admin.responsavel.editar', ['id' => $responsavel->id]) }}" class="aclNivel2 btn btn-warning text-dark">
			<i class="fa fa-edit"></i>
		</a>

		<a href="#" data-bs-toggle="modal" data-bs-target="#ModalDeletarResponsavel" class="aclNivel3 btn btn-danger ms-3">
			<i class="fa fa-trash"></i>
		</a>

	</div>

	<section class="mb-4-5 bg-white shadow p-4 border">
	
		<div class="row">

			<div class="col-lg-6 text-center">

				<div class="d-flex mb-2 mr-2 text-white">
					@if($responsavel->status == 0)
						<span class="bg-success px-3 py-1 rounded">Ativo</span>
					@else
						<span class="bg-danger px-3 py-1 rounded">Desativado</span>
					@endif
				</div>

				<i class="fa fa-user-circle fs-md-75 fs-xs-50"></i>

				<h1 class="fs-19 mt-3 color-333">{{ $responsavel->nome }}</h1>

				@if($responsavel->profissao)
					<h2 class="fs-15 mt-0 color-666 font-roboto" title="Profissão">{{ $responsavel->profissao }}</h2>
				@endif

				<hr class="hr-1 mb-4"/>

				<p class="fs-17 color-555">{{ $responsavel->email }}</p>

				@if($responsavel->cpf)
					<p class="fs-17 color-555">{{ $responsavel->cpf }}</p>
				@endif

				@if($responsavel->obs)
					<p class="mb-0 text-start color-333 mt-4">Observações</p>
					<p class="mb-0 text-start color-555">{!! nl2br($responsavel->obs) !!}</p>
				@endif

			</div>

			<div class="col-lg-6">

				<div class="ps-lg-5">

					@if(isset($unidade))
					<p class="mb-1 fs-14">
						<span>Unidade:</span> 
						<a href="{{ route('admin.unidade.info', ['id' => $unidade->id]) }}" class="color-blue-info-link">{{ $unidade->nome }}</a>
					</p>
					@endif

					@if($responsavel->rg)
					<p class="mb-1 fs-14">
						<span>RG:</span> 
						<span class="color-555">{{ $responsavel->rg ?? 'Não definido' }}</span>
					</p>
					@endif
					
					<p class="mb-1 fs-14">
						<span>Data de Nascimento:</span> 
						<span class="color-555">
							@if($responsavel->data_nascimento)
								{{ \App\Models\Util::replaceDatePt($responsavel->data_nascimento) }} ({{ \App\Models\Util::getIdade($responsavel->data_nascimento) }} anos)
							@else
								Não definido
							@endif
						</span>
					</p>

					@if($responsavel->genero)
					<p class="mb-1 fs-14">
						<span>Gênero:</span> 
						<span class="color-555">
							@if($responsavel->genero == 'M')
								Masculino
							@else
								Feminino
							@endif
						</span>
					</p>
					@endif

					<p class="mb-1 fs-14">
						<span>Celular:</span> 
						<a href="tel:+55{{preg_replace('/\D/', '', $responsavel->celular)}}" class="color-555">{{ $responsavel->celular ?? 'Não definido' }}</a>
					</p>

					@if($responsavel->telefone)
					<p class="mb-1 fs-14">
						<span>Telefone:</span> 
						<a href="tel:+55{{preg_replace('/\D/', '', $responsavel->telefone)}}" class="color-555">{{ $responsavel->telefone ?? 'Não definido' }}</a>
					</p>
					@endif

					<hr>

					@if(isset($endereco->cidade))

						@if($responsavel->cep)
						<p class="mb-1 fs-14">
							<span>CEP:</span>
							<span class="color-555">{{ $responsavel->cep }}</span>
						</p>
						@endif
						@if($responsavel->rua)
						<p class="mb-1 fs-14">
							<span>Rua:</span>
							<span class="color-555">
								{{ $responsavel->rua }}@if($responsavel->numero), {{ $responsavel->numero }}@endif
							</span>
						</p>
						@endif
						@if($responsavel->bairro)
						<p class="mb-1 fs-14">
							<span>Bairro:</span>
							<span class="color-555">{{ $responsavel->bairro }}</span>
						</p>
						@endif
						@if($responsavel->cidade)
						<p class="mb-1 fs-14">
							<span>Cidade:</span>
							<span class="color-555">
								{{ $responsavel->cidade }}
							</span>
						</p>
						@endif
						@if($responsavel->estado)
						<p class="mb-1 fs-14">
							<span>Estado:</span>
							<span class="color-555">{{ $responsavel->estado }}</span>
						</p>
						@endif
						@if($responsavel->complemento)
						<p class="mb-1 fs-14"> 
							<span>Complemento:</span>
							<span class="color-555">
								{{$responsavel->complemento}}
							</span>
						</p>
						@endif
						@if($responsavel->ponto_referencia)
						<p class="mb-1 fs-14"> 
							<span>Ponto de Referência:</span>
							<span class="color-555">
								{{$responsavel->ponto_referencia}}
							</span>
						</p>
						@endif

						<hr>

					@endif

					<p class="mb-1 fs-14 ls-05">
						<span>Data de Cadastro:</span> 
						<span class="color-555">{{ $responsavel->created_at->format('d/m/Y H:i') }}</span>
					</p>

					@if($cadastrante)
					<p class="mb-1 fs-14 ls-05">
						<span>Cadastrante:</span> 
						<a href="/admin/user/info/{{$cadastrante->id}}" class="color-555">
							{{ $cadastrante->nome }}
						</a>
					</p>
					@endif
					
				</div>

			</div>

		</div>

	</section>

	<section class="mb-4-5">
		
	
		<div class="section-title rounded-bottom-0">
			<a href="#boxAlunos" data-bs-toggle="collapse" aria-expanded="false" class="hover-d-none text-white">
				Alunos responsáveis
			</a>
		</div>

		<div class="collapse show bg-white p-4 border" id="boxAlunos">

            <div class="table-responsive">
                <table class="table">
				  	<thead>
				    	<tr>
					      	<th scope="col">Nome</th>
							<th scope="col">Grau Parentesco</th>
							<th scope="col"><span class="float-end mr-2">Ações</span></th>
				    	</tr>
				  	</thead>
                    <tbody>
					  	@foreach($alunos as $aluno)
						    <tr>
								<td>
									<a href="{{ route('admin.aluno.info', ['id' => $aluno->id]) }}" class="color-blue-info-link"> 
										{{ $aluno->nome }}
									</a>
								</td>
								<td>{{ $aluno->grau_parentesco }}</td>

								<td>
									<div class="float-end mr-2">
										<a href="#" wire:click="desvincularAlunoId({{$aluno->id}})" data-bs-toggle="modal" data-bs-target="#ModalDesvincularAluno" data-bs-dismiss="modal" class="mb-2 btn btn-danger" title="Desvincular Responsável">
											<i class="fa fa-times-circle"></i>
										</a>
									</div>
								</td>
						    </tr>
					    @endforeach
                    </tbody>
                </table>
            </div>

		</div>

	</section>

	<form wire:submit.prevent="delete" method="POST">
		<div wire:ignore class="modal fade" id="ModalDeletarResponsavel" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  		<div class="modal-dialog modal-dialog-centered">
	    		<div class="modal-content">
	      			<div class="modal-header">
	        			<h3 class="modal-title" id="exampleModalLabel">Deletar Responsável</h3>
	        			<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
	      			</div>
	      			<div class="modal-body text-center py-5">
	        			<p class="m-0 fs-16">
	        				Você tem certeza que deseja deletar este responsável?
	        				<br>
	        				Todas as informações serão perdidas definitivamente.
	        			</p>
	      			</div>
	      			<div class="modal-footer">
	      				<div class="d-flex w-100">
		      				<input type="password" wire:model="password_to_delete" class="w-100 form-control p-form me-3" placeholder="Digite sua senha" required>
		        			<button type="submit" class="btn btn-danger weight-600">Confirmar</button>
	    				</div>
	      			</div>
	    		</div>
	  		</div>
		</div>
	</form>

	<form wire:submit.prevent="desvincularAluno" method="POST">
	    <input type="hidden" wire:model="desvincular_aluno_id">
		<div wire:ignore class="modal fade" id="ModalDesvincularAluno" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  		<div class="modal-dialog modal-dialog-centered">
	    		<div class="modal-content">
	      			<div class="modal-header">
	        			<h3 class="modal-title" id="exampleModalLabel">Desvincular Aluno</h3>
	        			<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
	      			</div>
	      			<div class="modal-body text-center py-5">
	        			<p class="m-0 fs-15">
	        				Você tem certeza que deseja desvincular a responsabilidade <br> de  {{ $responsavel->nome }} para este aluno?
	        			</p>
	      			</div>
	      			<div class="modal-footer">
	      				<div class="d-flex w-100">
		      				<input type="password" wire:model="password_to_delete" class="w-100 form-control p-form me-3" placeholder="Digite sua senha" required>
		        			<button type="submit" class="btn btn-danger weight-600">Confirmar</button>
	    				</div>
	      			</div>
	    		</div>
	  		</div>
		</div>
	</form>

	@include('livewire.util.toast')
	
</div>

