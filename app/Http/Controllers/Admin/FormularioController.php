<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Formulario;
use App\Models\FormularioCategoria;
use App\Models\Categoria;
use App\Models\Unidade;
use App\Models\FormularioIntegracao;

class FormularioController extends Controller
{
    public function lista()
    {
        return view('admin.formulario.lista');
    }
    
    public function info($formulario_id)
    {
        return view('admin.formulario.info', compact('formulario_id') );
    }

    public function add()
    {
        $categorias = Categoria::plataforma()->ativo()->tipo('formulario')->orderBy('nome')->get();
        $unidades   = Unidade::plataforma()->ativo()->orderBy('nome')->get();
        
        if(request('sent'))
        {
            $formulario = Formulario::create(request()->all());

            $this->syncCategorias($formulario->id);

            foreach(collect(request('integracao'))->whereNotNull('value')->all() as $integracao)
            {
                FormularioIntegracao::updateOrCreate(
                    ['formulario_id' => $formulario->id, 'ref' => $integracao['ref']], 
                    $integracao
                );
            }

            return redirect()->route('admin.formulario.info', ['id' => $formulario->id])->with('success', 'Formulário criado com êxito.');
        }

    	return view('admin.formulario.add', compact('categorias','unidades') );
    }

    public function editar($formulario_id)
    {
        $formulario = Formulario::plataforma()->where('id', $formulario_id)->first();

        $categorias = Categoria::plataforma()->ativo()->tipo('formulario')->orderBy('nome')->get();
        $categoriasSelected = FormularioCategoria::plataforma()->where('formulario_id', $formulario_id)->get()->pluck('categoria_id')->toArray();

        $unidades = Unidade::plataforma()->ativo()->orderBy('nome')->get();

        $integracao_email   = FormularioIntegracao::plataforma()->where('formulario_id', $formulario_id)->where('ref','email')->first();
        $integracao_hubspot = FormularioIntegracao::plataforma()->where('formulario_id', $formulario_id)->where('ref','hubspot')->first();

        if(request('sent'))
        {
            $formulario->update(request()->all());

            $this->syncCategorias($formulario->id);

            foreach(request('integracao') as $integracao)
            {
                FormularioIntegracao::updateOrCreate(
                    ['formulario_id' => $formulario->id, 'ref' => $integracao['ref']], 
                    $integracao
                );
            }

            return back()->with('success', 'Formulário atualizado com êxito.');
        }

        return view('admin.formulario.editar', compact('formulario','categorias','categoriasSelected','unidades','integracao_email','integracao_hubspot') );
    }

    public function syncCategorias($formulario_id)
    {
        FormularioCategoria::where('formulario_id', $formulario_id)->whereNotIn('categoria_id', request('categorias') ?? array())->delete();

        if(request('categorias') == null)
            return;

        foreach(request('categorias') as $categoria_id)
        {
            $countDuplicate = FormularioCategoria::plataforma()->where('formulario_id', $formulario_id)->where('categoria_id', $categoria_id)->count();
            
            if($countDuplicate <= 0)
                FormularioCategoria::create([
                    'plataforma_id' => session('plataforma_id'),
                    'formulario_id' => $formulario_id, 
                    'categoria_id'  => $categoria_id
                ]);
        }
    }

}