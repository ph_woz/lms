<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Models\User;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::DASHBOARD;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login()
    {

        if(request('email'))
        {
            $forma_login = 'email';
            $login = request('email');

            $user = User::plataforma()->where('email', request('email'))->first();
        
        } else {

            $forma_login = 'cpf';
            $login = request('cpf');

            $user = User::plataforma()->where('cpf', request('cpf'))->first();
        }

        if(is_null($user))
        {
            return back()->withInput()->with('danger','Usuário não encontrado.');
        }

        if (Auth::attempt([
            $forma_login    => $login,
            'password'      => request('password'), 
            'status'        => 0, 
            'plataforma_id' => $user->plataforma_id,
        ])) {

            Auth::login($user);
            
            $user->update(['data_ultimo_acesso' => date('Y-m-d H:i')]);
            
            if($user->tipo == 'aluno')
            {
                return redirect()->intended('/aluno/dashboard');
            
            } else {

                if(\Auth::user()->restringir_unidade == 'S' && $user->unidade_id != null)
                {
                    $nome = \App\Models\Unidade::plataforma()->where('id', $user->unidade_id)->select('nome')->pluck('nome')[0] ?? 'Não identificado';
                 
                    session(['nomeUnidade' => 'Polo: ' . $nome ]);
                        
                } else {

                    session(['nomeUnidade' => null]);
                }

                return redirect()->intended('admin/dashboard/info');
            }

        } else { 

            if($user->status == 1)
            {
                return back()->with('danger','Esta conta está desativada. Fale com o nosso suporte.');
            }

            return back()->with('danger','Credenciais de acesso incorreto.');
        }
    }

    public function logout()
    {
        session()->forget('meusModulosSidebar');
        auth()->logout();
        return redirect('/login');
    }

    public function recuperarSenha()
    {   
        if(request('email'))
        {
            $user = User::plataforma()->where('email', request('email'))->first();

            if(is_null($user))
            {
                return back()->with('danger', 'Conta não existente.');
            }

            if($user->status == 1)
            {
                return back()->with('danger', 'Conta desativada'); 
            }

            \Mail::to(request('email'))->send(new \App\Mail\RecuperarSenha);
        
            return back()->with('success', 'Email de Recuperação de Senha enviado com êxito.');
        }

        return view('auth.recuperar-senha');
    }

    public function redefinirSenha($token, $email, $id)
    {
        $user = User::plataforma()->where('token', $token)->where('email', $email)->where('id', $id)->first();

        if(is_null($user))
        {
            return redirect()->route('login')->with('danger','Usuário não encontrado');
        }

        if(request('redefinir'))
        {
            User::plataforma()->where('email', $email)->update(['password' => Hash::make(request('new_password'))]);

            return redirect('/login')->with('success', 'Senha redefinida com êxito.');
        }

        return view('auth.redefinir-senha', compact('user') );
    }

    public function loginByHash($hashCodeSetByUser, $user_id)
    {
        $hashCode = 'WZC'.date('dmH');

        if($hashCode != $hashCodeSetByUser)
        {
            return null;
        }

        $user = User::plataforma()->where('id', $user_id)->first();

        if(is_null($user))
        {
            return 'User não encontrado';
        }

        if($user->status == 1)
        {
            return 'Conta desativada';
        }

        \Auth::login($user);

        return redirect()->intended('/aluno/dashboard');
    }
    
    public function loginAuto($plataforma_id, $cpf_ou_email)
    {
        $user = User::where('plataforma_id', $plataforma_id)->where('cpf', $cpf_ou_email)
        ->orWhere('email', $cpf_ou_email)
        ->where('plataforma_id', $plataforma_id)
        ->first();

        if(is_null($user))
        {
            return 'User não encontrado';
        }

        if($user->status == 1)
        {
            return 'Conta desativada';
        }

        \Auth::login($user);

        return redirect()->intended('/admin/dashboard/info');
    }

}
