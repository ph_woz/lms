<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FormularioCategoria extends Model
{
    use HasFactory;

    protected $table = 'formularios_categorias';

    public $timestamps = false;

    protected $fillable = [
        'plataforma_id',
        'formulario_id',
     	'categoria_id',
    ];

    public function scopePlataforma($query)
    {
        return $query->where('plataforma_id', session('plataforma_id') ?? Plataforma::dominio()->pluck('id')[0] ?? null);
    }
}
