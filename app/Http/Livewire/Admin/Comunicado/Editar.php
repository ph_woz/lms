<?php

namespace App\Http\Livewire\Admin\Comunicado;

use Livewire\Component;
use App\Models\Comunicado;
use Illuminate\Support\Facades\Http;

class Editar extends Component
{

    public $comunicado;
    public $assunto;
    public $descricao;
    public $arquivar = 'N';

    protected $rules = [
        'assunto'   => 'required',
        'descricao' => 'required',
    ];

    protected $messages = [
        'assunto.required'   => 'O Assunto não pode estar vazio.',
        'descricao.required' => 'A Descrição não pode estar vazio.',
    ];

    public function mount($comunicado_id)
    {
        $this->comunicado = Comunicado::plataforma()->where('id', $comunicado_id)->first();

        $this->assunto   = $this->comunicado->assunto;
        $this->descricao = $this->comunicado->descricao;
        $this->arquivar  = $this->comunicado->arquivar;
    }

    public function editar()
    {
        $this->validate();

        $this->comunicado->update([
            'assunto'   => $this->assunto,
            'descricao' => $this->descricao,
            'arquivar'  => $this->arquivar,
        ]);

        session()->flash('success-livewire', 'Comunicado atualizado com êxito.');

        $this->dispatchBrowserEvent('toast');
    }

    public function render()
    {
        return view('livewire.admin.comunicado.editar');
    }
}
