@extends('layouts.admin.master')
@section('content')
<div>

    <div class="bg-white px-4 pt-3">

        <h1 class="h4 fs-20">Planos</h1>
        <p>
            Precisa cadastrar mais alunos?
        </p>
        <hr>

        <div class="row align-items-center pt-4">

            @if(isset($planoAtual))
                <div class="col-lg-3 mb-4">
                    <div class="bg-dark p-3 text-center">
                        <p class="text-light text-center weight-600 fs-17 m-0">{{ $planoAtual->plano }}</p>
                    </div>
                    <div class="bg-secondary p-4 rounded-bottom box-shadow" id="plano{{$planoAtual->id}}"> 

                        <p class="text-white weight-600 text-center m-0">
                            até {{ $planoAtual->max_qnt_alunos }} alunos
                        </p>

                        <hr class="border border-light">
                        
                        <p class="text-white weight-600 text-center m-0">
                            R$ {{ $planoAtual->valor }}/mês
                        </p>

                        <hr class="border border-light">

                        <button type="button" disabled class="btn btn-dark weight-600 w-100 py-2 fs-16 text-shadow-1">
                            Seu Plano em uso atual
                        </button>

                    </div>
                </div>
            @endif

            @foreach($planos as $plano)
                <div class="col-lg-3 mb-4">
                    <div class="bg-dark p-3 text-center">
                        <p class="text-light text-center weight-600 fs-17 m-0">{{ $plano->plano }}</p>
                    </div>
                    <div class="bg-primary p-4 rounded-bottom box-shadow" id="plano{{$plano->id}}"> 
                        <p class="text-white weight-600 text-center m-0">
                            até {{ $plano->max_qnt_alunos }} alunos
                        </p>

                        <hr class="border border-light">
                        
                        <p class="text-white weight-600 text-center m-0">
                            R$ {{ $plano->valor }}/mês
                        </p>

                        <hr class="border border-light">

                        <button type="button" data-bs-toggle="modal" data-bs-target="#ModalContratarPlano" class="btn btn-info weight-600 w-100 py-2 box-shadow fs-16 text-shadow-1"
                            onclick="
                                document.getElementById('contratar_novo_plano_id').value = '{{ $plano->id }}';
                                document.getElementById('contratar_max_qnt_alunos_plano').innerHTML = '{{ $plano->max_qnt_alunos }}';
                                document.getElementById('contratar_valor_plano').innerHTML = 'R$ {{ $plano->valor }}';
                                document.getElementById('contratar_nome_plano').innerHTML = '{{ $plano->plano }}';"
                            >
                            Contratar
                        </button>
                    </div>
                </div>
            @endforeach
        </div>

    </div>

    <hr class="mb-4-5" />

    <div class="row mt-4">
        <div class="col-12">

            <div class="card">
                <div class="card-body">
                    @if($countTotal > 0)
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col">Data de Vencimento</th>
                                    <th scope="col">Data de Pagamento</th>
                                    <th scope="col">Valor</th>
                                    <th scope="col">Status</th>
                                    <th scope="col">
                                        <span class="float-end me-2">
                                            Ações
                                        </span>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($faturasInadimplentes as $fatura)
                                    <tr>
                                        <td>
                                            {{ date('d/m/Y', strtotime($fatura->data_vencimento)) }}
                                        </td>
                                        <td>
                                            @if($fatura->data_recebimento != null)
                                                {{ date('d/m/Y', strtotime($fatura->data_recebimento)) }}
                                            @else
                                                Ainda não foi pago
                                            @endif
                                        </td>
                                        <td>
                                            R$ {{ $fatura->valor_parcela }}
                                        </td>
                                        <td>
                                            <span class="py-2 px-3 text-light rounded bg-danger">
                                                Inadimplente
                                            </span>
                                        </td>
                                        <td>
                                            <span class="float-end">
                                                <a href="#ModalPagar" data-bs-toggle="modal" onclick="document.getElementById('valor').innerHTML = 'R$ {{ $fatura->valor_parcela }}';" class="py-2 px-3 text-light rounded hover-d-none box-shadow bg-success">
                                                    Pagar
                                                </a>
                                            </span>
                                        </td>
                                    </tr>
                                @endforeach
                                @foreach ($faturasDoMesAtual as $fatura)
                                    <tr>
                                        <td>
                                            {{ date('d/m/Y', strtotime($fatura->data_vencimento)) }}
                                        </td>
                                        <td>
                                            @if($fatura->data_recebimento != null)
                                                {{ date('d/m/Y', strtotime($fatura->data_recebimento)) }}
                                            @else
                                                Ainda não foi pago
                                            @endif
                                        </td>
                                        <td>
                                            R$ {{ $fatura->valor_parcela }}
                                        </td>
                                        <td>
                                            @switch($fatura->status)
                                                @case(0)
                                                    @if(date('Y-m-d') > $fatura->data_vencimento)
                                                        <span class="py-2 px-3 text-light rounded bg-danger">
                                                            Inadimplente
                                                        </span>
                                                    @endif
                                                    @if(date('Y-m-d') <= $fatura->data_vencimento)
                                                        <span class="py-2 px-3 text-light rounded bg-info">
                                                            A vencer
                                                        </span>
                                                    @endif
                                                @break
                                                @case(1)
                                                    <span class="py-2 px-3 text-light rounded bg-success">
                                                        Pago
                                                    </span>
                                                @break
                                                @case(2)
                                                    <span class="py-2 px-3 text-light rounded bg-inadimplente">
                                                        Estornado
                                                    </span>
                                                @break
                                            @endswitch
                                        </td>
                                        <td>
                                            <span class="float-end">
                                                <a href="#ModalPagar" data-bs-toggle="modal" onclick="document.getElementById('valor').innerHTML = 'R$ {{ $fatura->valor_parcela }}';" class="py-2 px-3 text-light rounded hover-d-none box-shadow bg-success">
                                                    Pagar
                                                </a>
                                            </span>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    @else
                        <p class="mb-0 weight-600">
                            Tudo em dia! Não há nenhuma fatura em aberto :-)
                        </p>
                    @endif
                </div>
            </div>

        </div>
    </div>

    <div class="modal fade" id="ModalPagar" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title text-primary" id="exampleModalLabel">
                        Pague com PIX<br>
                        <span id="valor" class="weight-600"></span>                           
                    </h3>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body d-lg-flex">

                    <div class="w-100 border p-3 rounded-1 mb-4">
                        <p class="m-0 weight-600 fs-17">Opção Chave PIX</p>
                        <hr class="my-2">
                        <p class="m-0 fs-15">
                            <span class="weight-600">Chave Celular:</span> (21) 96839-0250<br>
                            <span class="weight-600">Banco:</span> Nubank<br>
                            <span class="weight-600">Nome:</span> Pedro Henrique Caetano da Silva<br>
                        </p>
                    </div>

                    <div class="mx-lg-1"></div>

                    <div class="w-100 border p-3 rounded-1 mb-4">
                        <p class="m-0 weight-600 fs-17">Opção QR Code PIX</p>
                        <hr class="my-2">

                        <div align="center">
                            <img src="{{ asset('images/qr-code-pix.jpeg') }}" class="img-fluid" width="200" alt="QR CODE PIX">
                        </div>

                    </div>

                </div>
                <div class="modal-footer">

                    <p class="fs-16 weight-600 text-primary">
                        Após finalizar o pagamento, nos envie o comprovante pelo WhatsApp.
                    </p>

                    <a href="https://api.whatsapp.com/send?phone=5521968390250&text=Olá, irei enviar abaixo o comprovante de pagamento da Plataforma LMS" target="_blank" class="btn btn-success py-2 weight-600 fs-16 px-3 text-shadow-1 box-shadow">
                        Já paguei. Quero enviar o comprovante!
                    </a>

                </div>
            </div>
        </div>
    </div>

    <form method="POST">
    @csrf

        <div class="modal fade" id="ModalContratarPlano" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3 class="modal-title" id="exampleModalLabel">
                            Contratar Plano<br>
                            <span id="contratar_nome_plano" class="fs-18 weight-600 text-dark"></span>
                        </h3>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body py-5 px-4">

                        <p class="fs-16 weight-600 text-primary text-center">
                            Após confirmar, será possível cadastrar até <span id="contratar_max_qnt_alunos_plano">0</span> alunos<br>
                            e o valor da mensalidade do próximo mês será <br>reajustado para <span id="contratar_valor_plano">R$ 297,99</span>.
                        </p>

                    </div>
                    <div class="modal-footer">

                        <button type="submit" name="contratar_novo_plano_id" value="" id="contratar_novo_plano_id" class="btn btn-success py-2 weight-600 fs-16 px-3 text-shadow-1 box-shadow">
                            Confirmar meu novo Plano!
                        </button>

                    </div>
                </div>
            </div>
        </div>
    </form>

</div>    
@endsection