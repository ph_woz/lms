<div>

	<section class="mb-4-5">

		<div class="section-title rounded-bottom-0 d-flex justify-content-between align-items-center">
			{{ $avaliacao->referencia }}
			<div>
				<a href="{{ route('admin.avaliacao.info.finalizaram', ['id' => $avaliacao->id]) }}" class="btn btn-primary fs-13 py-1-5 me-2">
					Alunos que finalizaram
				</a>
				<a href="{{ route('admin.avaliacao.info.nao-finalizaram', ['id' => $avaliacao->id]) }}" class="btn btn-primary fs-13 py-1-5 me-2">
					Alunos que não finalizaram
				</a>
				<a href="#" data-bs-toggle="modal" data-bs-target="#ModalMetaDados" class="btn btn-info me-2 fs-14 mb-lg-0 mb-1 py-1 px-2-3">
					<i class="fa fa-align-left"></i>
				</a>
				<a href="/admin/avaliacao/editar/{{$avaliacao->id}}" class="aclNivel2 btn btn-warning text-dark fs-14 mb-lg-0 mb-1 py-1 px-2">
					<i class="fa fa-edit"></i>
				</a>
				<a href="#" data-bs-toggle="modal" data-bs-target="#ModalDeletarAvaliacao" class="aclNivel3 btn btn-danger mb-lg-0 mb-1 fs-15 py-1 px-2 ms-2">
					<i class="fa fa-trash"></i>
				</a>
			</div>
		</div>

		<div class="bg-white border p-4 d-lg-flex justify-content-between">
			
			<div class="mb-2 mb-lg-0">
				<p class="mb-0 weight-600 color-444">Turma</p>
				@if($turma)
					<a href="{{ route('admin.turma.info', ['id' => $turma->id]) }}" class="color-blue-info-link">
						{{ $turma->nome }}
					</a>
				@else
					<p class="mb-0">
						Não definido
					</p>
				@endif
			</div>

			<div class="mb-2 mb-lg-0">
				<p class="mb-0 weight-600 color-444">Liberar Gabarito</p>
				<p class="mb-0">
					@if($avaliacao->liberar_gabarito == 'S')
						Sim
					@else
						Não
					@endif
				</p>
			</div>

			<div class="mb-2 mb-lg-0">
				<p class="mb-0 weight-600 color-444">N° de Tentaticas</p>
				<p class="mb-0">
					{{ $avaliacao->n_tentativas ?? 'Sem tentativas extras.' }}
				</p>
			</div>

		</div>

		@if($avaliacao->condicao_fazer_avaliacao == 'concluir-conteudo-do-curso')
		<div class="bg-white border p-4 d-lg-flex justify-content-between">

			<div class="mb-2 mb-lg-0">
				<p class="mb-0 weight-600 color-444">Condição para fazer a avaliação</p>
				<p class="mb-0">
					É necessário ter concluído todo o conteúdo do curso.
				</p>
			</div>	

		</div>
		@endif

	</section>

	<section class="mb-4-5">

		<div class="section-title rounded-bottom-0 d-flex justify-content-between align-items-center">
			Questões
			<div>
				<a href="{{ route('admin.questao.lista', ['add_questoes_para_avaliacao_id' => $avaliacao->id]) }}" class="me-lg-2 btn btn-primary fs-13 py-2">
					+ Adicionar Questão
				</a>
				<a href="#ModalRemoverTodasQuestoes" data-bs-toggle="modal" class="aclNivel3 btn btn-danger fs-13 py-2">
					- Remover todas as Questões
				</a>
			</div>
		</div>

		<div class="bg-white border p-4">

			<div class="mb-0 border p-3">
				<p class="mb-0 weight-600 color-444">Questões selecionadas: {{ count($questoes) }}</p>
			</div>

			<div class="mb-0 border p-3">
				<p class="mb-0 color-444"><span class="weight-600">Valor de Nota máxima</span> configurada: <span class="weight-600">{{ $valor_nota_maxima }}</span></p>
			</div>

			<div class="mb-2">
				@foreach($configsNotas as $configsNotas)
				<p class="mb-0 border p-3">
					<span>
						<span class="weight-600">
							{{ $configsNotas->n_questoes }}
							@if($configsNotas->n_questoes > 1)
								questões
							@else
								questão
							@endif
						</span>
						com valor de <span class="weight-600">nota: {{ $configsNotas->valor_nota }}</span>

						@php
							$questoesIds = \App\Models\AvaliacaoQuestao::plataforma()->where('avaliacao_id', $avaliacao->id)->where('valor_nota', $configsNotas->valor_nota)->select('questao_id')->get()->pluck('questao_id')->toArray();
							$countQntJaCadastrada = \App\Models\Questao::plataforma()->whereIn('id', $questoesIds)->count();
						@endphp
						@if($countQntJaCadastrada < intval($configsNotas->n_questoes))
							@php
								$qntFaltante = intval($configsNotas->n_questoes) - $countQntJaCadastrada;
							@endphp
							<span class="ms-1 bg-danger fs-12 rounded-1 weight-600 text-white px-2 py-1">
								Atenção!&nbsp;
								Falta adicionar no mínimo {{ $qntFaltante }}
								@if($qntFaltante > 1)
									questões
								@else
									questão
								@endif
								desta configuração de nota. Adicione agora mesmo!
							</span>
						@endif
					</span>
				</p>
				@endforeach
			</div>

		</div>

		<div class="bg-white border p-4">

			@foreach($questoes as $questao)
			<div class="row align-items-center">
				<div class="col-lg-11">
					<a href="{{ route('admin.questao.info', ['id' => $questao->id]) }}" class="text-dark mb-0">
						{{ $questao->enunciado }}
					</a>
				</div>
				<div class="col-lg-1">
					<div align="right" data-bs-toggle="tooltip" data-bs-placement="top" title="Remover esta questão da avaliação">
						<a href="#ModalRemoverQuestao" data-bs-toggle="modal" wire:click="setRemoverQuestaoDaAvaliacao({{$questao->id}})" class="aclNivel3 btn btn-danger">
							<span class="weight-800">x</span>
						</a>
					</div>
				</div>
			</div>
			<hr/>
			@endforeach

		</div>

	</section>

	<div class="modal fade" id="ModalMetaDados" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	  	<div class="modal-dialog modal-dialog-centered" role="document">
	    	<div class="modal-content">
	      		<div class="modal-header">
	        		<h3 class="modal-title">Metadados</h3>
	    			<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
	      		</div>
	      		<div class="modal-body">

					<ul class="list-group list-group-flush">
	  					<li class="list-group-item">
							<span class="mb-0 weight-600 color-444">Cadastrante:</span>
							@if($cadastrante)
								<a href="/admin/colaborador/info/{{$cadastrante->id}}" class="color-blue-info-link">
									{{ $cadastrante->nome }}
								</a>
							@else
								Não definido
							@endif
	  					</li>   
	  					<li class="list-group-item">
							<span class="mb-0 weight-600 color-444">Status:</span>
							@if($avaliacao->status == 0)
								<span class="text-success weight-700">Ativo</span>
							@else
								<span class="text-danger weight-700">Desativado</span>
							@endif
	  					</li>
	  					<li class="list-group-item">
							<span class="mb-0 weight-600 color-444">Data de Cadastro:</span>
							@if($avaliacao->created_at)
								<span class="mb-0">{{ $avaliacao->created_at->format('d/m/Y H:i') ?? null }} ({{$avaliacao->created_at->diffForHumans()}}) </span>
	  						@else
	  							Não definido
	  						@endif
	  					</li>   
	  				</ul>

	      		</div>
	    	</div>
	  	</div>
	</div>

	<form wire:submit.prevent="removerTodasQuestoes">
		<div wire:ignore class="modal fade" id="ModalRemoverTodasQuestoes" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  		<div class="modal-dialog modal-dialog-centered">
	    		<div class="modal-content">
	      			<div class="modal-header">
	        			<h3 class="modal-title" id="exampleModalLabel">Remover todas as Questões da Avaliação</h3>
	        			<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
	      			</div>
	      			<div class="modal-body text-center py-5">
	        			<p class="m-0 fs-16">
	        				Você tem certeza que deseja remover todas as questões desta avaliação?
	        			</p>
	      			</div>
	      			<div class="modal-footer">
	      				<div class="d-flex w-100">
		      				<input type="password" wire:model="password_to_delete" class="w-100 form-control p-form me-3" placeholder="Digite sua senha" required>
		        			<button type="submit" class="btn btn-danger weight-600">Confirmar</button>
	    				</div>
	      			</div>
	    		</div>
	  		</div>
		</div>
	</form>
	<form wire:submit.prevent="removerQuestao">
		<div wire:ignore class="modal fade" id="ModalRemoverQuestao" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  		<div class="modal-dialog modal-dialog-centered">
	    		<div class="modal-content">
	      			<div class="modal-header">
	        			<h3 class="modal-title" id="exampleModalLabel">Remover Questão da Avaliação</h3>
	        			<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
	      			</div>
	      			<div class="modal-body text-center py-5">
	      				<input type="hidden" wire:model="remove_questao_id">
	      				<input type="text" wire:model="remove_questao_enunciado" class="weight-600 m-0 fs-16 w-100 border-0 outline-0" disabled readonly>
	      				<hr>
	        			<p class="m-0 fs-16">
	        				Você tem certeza que deseja remover esta questão desta avaliação?
	        			</p>
	      			</div>
	      			<div class="modal-footer">
	      				<div class="d-flex w-100">
		      				<input type="password" wire:model="password_to_delete" class="w-100 form-control p-form me-3" placeholder="Digite sua senha" required>
		        			<button type="submit" class="btn btn-danger weight-600">Confirmar</button>
	    				</div>
	      			</div>
	    		</div>
	  		</div>
		</div>
	</form>

	<form wire:submit.prevent="delete" method="POST">
		<div wire:ignore class="modal fade" id="ModalDeletarAvaliacao" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  		<div class="modal-dialog modal-dialog-centered">
	    		<div class="modal-content">
	      			<div class="modal-header">
	        			<h3 class="modal-title" id="exampleModalLabel">Deletar Avaliação</h3>
	        			<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
	      			</div>
	      			<div class="modal-body text-center py-5">
	        			<p class="m-0 fs-16">
	        				Você tem certeza que deseja deletar esta avaliação?
	        				<br>
	        				Todas as informações serão perdidas definitivamente.
	        			</p>
	      			</div>
	      			<div class="modal-footer">
	      				<div class="d-flex w-100">
		      				<input type="password" wire:model="password_to_delete" class="w-100 form-control p-form me-3" placeholder="Digite sua senha" required>
		        			<button type="submit" class="btn btn-danger weight-600">Confirmar</button>
	    				</div>
	      			</div>
	    		</div>
	  		</div>
		</div>
	</form>

	@include('livewire.util.toast')

</div>

