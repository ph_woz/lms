<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Plano;
use App\Models\PlanoBeneficio;
use App\Models\Unidade;
use App\Models\Categoria;

class PlanoController extends Controller
{
    public function lista()
    {
        return view('admin.plano.lista');
    }

    public function info($plano_id)
    {
        return view('admin.plano.info', compact('plano_id') );
    }

    public function add()
    {
        if(request()->has('sent'))
        {
            $plano = Plano::create(request()->all());

            if(count(request('beneficios')) > 0)
            {
                foreach (array_filter(request('beneficios')) as $beneficio)
                {
                    PlanoBeneficio::create([
                        'plano_id'  => $plano->id,
                        'descricao' => $beneficio,
                    ]);
                }
            }

            return redirect()->route('admin.plano.info', ['id' => $plano->id])->with('success', 'Plano cadastrado com êxito.');
        }

        $unidades   = Unidade::select('id','nome')->plataforma()->ativo()->orderBy('nome')->get();
        $categorias = Categoria::select('id','nome')->plataforma()->ativo()->tipo('Planos')->orderBy('nome')->get();

        return view('admin.plano.add', compact('unidades','categorias') );
    }

    public function editar($id)
    {

        $plano = Plano::plataforma()->find($id);

        if(is_null($plano))
        {
            return view('admin.page-whoops', ['erro' => 'Plano não encontrado.']);
        }

        if(request()->has('sent'))
        {
            $plano->update(request()->all());

            PlanoBeneficio::plataforma()->where('plano_id', $plano->id)->delete();

            if(count(array_unique(array_filter(request('beneficios')))) > 0)
            {
                foreach(array_unique(request('beneficios')) as $descricao)
                {
                    PlanoBeneficio::create([
                        'plano_id'  => $plano->id,
                        'descricao' => $descricao,
                    ]);
                }
            }

            return back()->with('success', 'Plano atualizado com êxito.');
        }


        $unidades   = Unidade::select('id','nome')->plataforma()->ativo()->orderBy('nome')->get();
        $categorias = Categoria::select('id','nome')->plataforma()->ativo()->tipo('Planos')->orderBy('nome')->get();
        $beneficios = PlanoBeneficio::select('descricao')->plataforma()->where('plano_id', $plano->id)->get()->pluck('descricao')->toArray();

        return view('admin.plano.editar', compact('plano','beneficios','unidades','categorias') );
    }
}