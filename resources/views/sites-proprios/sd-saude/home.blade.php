<!doctype html>
<html lang="pt-br">
<head>

  <!-- Google tag (gtag.js) -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=G-MREB450HCT"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'G-MREB450HCT');
  </script>
  
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1">

   <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/uikit@3.5.9/dist/css/uikit.min.css" />
   <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
   <link rel="stylesheet" href="{{ asset('css/util.css') }}">
   <link rel="stylesheet" href="{{ asset('sites-proprios/sd-saude/css/home.css') }}">

   <title>{{ $seo->seo_title ?? $plataforma->nome }}</title>
   <meta name="robots" content="index, follow">
   <meta name="description" content="{{ $seo->seo_description ?? null }}"/>
   <meta name="keywords" content="{{ $seo->seo_keywords ?? null }}"/>
   <link rel="canonical" href="{{ Request::url() }}" />
   <link rel="shortcut icon" href="{{ $plataforma->favicon }}">
   <meta property="og:locale" content="pt_BR" />
   <meta property="og:type" content="website" />
   <meta property="og:title" content="{{ $seo->seo_title ?? $plataforma->nome }}" />
   <meta property="og:description" content="{{ $seo->seo_description ?? null }}" />
   <meta property="og:url" content="{{ Request::url() }}" />
   <meta property="og:site_name" content="{{ $plataforma->nome }}" />
   <meta property="og:image" content="{{ $plataforma->logotipo }}" />
   <meta name='author' content='WOZCODE | Pedro Henrique' />

</head>
<body class="bg-f4" id="inicio">

   <header>
      
      <div class="sub-header container-fluid px-xl-7 py-2-3">
         <div class="d-flex justify-content-between align-items-center">

            <div>

               <span class="d-xs-none font-poppins text-light text-shadow-2 fs-13 weight-600">
                  <i class="fa fa-envelope"></i>
                  {{ $plataforma->email ?? 'email@exemplo.com' }}
               </span>

               <a href="https://api.whatsapp.com/send?phone=55{{preg_replace('/\D/', '', $plataforma->whatsapp)}}&text=Olá, vim através do Site SD SAÚDE." target="_blank" class="mx-3 text-shadow-2 text-d-none font-poppins text-light hover-d-none fs-13 weight-600">
                  <i class="fab fa-whatsapp"></i>
                  {{ $plataforma->whatsapp ?? '(00) 00000-0000' }}
               </a>

               <a href="tel:+55{{preg_replace('/\D/', '', $plataforma->telefone)}}" class="d-xs-none text-shadow-2 text-d-none font-poppins text-light hover-d-none fs-13 weight-600">
                  <i class="fa fa-phone"></i>
                  {{ $plataforma->telefone ?? '(00) 0000-0000' }}
               </a>

            </div>

            <div class="d-flex justify-content-between align-items-end">

               <div>
                  <a href="{{ $plataforma->facebook }}" target="_blank" class="fs-17 text-d-none text-light text-shadow-2">
                       <i class="fab fa-facebook-f flex-center"></i>
                  </a>
                  <a href="{{ $plataforma->instagram }}" target="_blank" class="ms-2 fs-17 text-d-none text-light text-shadow-2">
                     <i class="fab fa-instagram flex-center"></i>
                  </a>
               </div>

            </div>

         </div>
      </div>

      <nav class="navbar navbar-expand-lg bg-white box-shadow py-0 z-3" id="menu">
         <div class="container-fluid px-xl-7">
             <a class="navbar-brand" href="#inicio" uk-scroll>
               <img src="{{ $plataforma->logotipo }}" class="d-xs-none" width="100" alt="Logotipo SD Saúde">
               <img src="{{ $plataforma->logotipo }}" class="d-md-none" id="logoMenuMobile" width="75" alt="Logotipo SD Saúde">
             </a>
             <button class="navbar-toggler outline-0 border-0 text-dark" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
               <span class="fa fa-bars"></span>
             </button>
             <div class="collapse navbar-collapse" id="navbarSupportedContent">
                  <ul id="ulLinks" class="navbar-nav ms-auto mb-2 mb-lg-0 align-items-lg-center p-3 p-lg-auto">
                     <li class="nav-item">
                        <a class="nav-link" href="#cursos" uk-scroll="offset: 140;">
                           <span class="d-block weight-500">
                              Nossos
                           </span>
                           <span class="weight-700">
                              CURSOS
                           </span>
                        </a>
                     </li>
                     <li class="nav-item">
                        <a class="nav-link" href="#quem-somos" uk-scroll="offset: 140;">
                           <span class="d-block weight-500">
                              Sobre a
                           </span>
                           <span class="weight-700">
                              SD SAÚDE
                           </span>
                        </a>
                     </li>
                     <li class="nav-item">
                        <a class="nav-link" href="#pre-matricula" uk-scroll="offset: 140;">
                           <span class="d-block weight-500">
                              Como
                           </span>
                           <span class="weight-700">
                              INGRESSAR
                           </span>
                        </a>
                     </li>
                     <li class="nav-item">
                        <a class="nav-link" href="#unidades">
                           <span class="d-block weight-500">
                              Nossas
                           </span>
                           <span class="weight-700">
                              UNIDADES
                           </span>
                        </a>
                     </li>
                     <li class="nav-item">
                        <a class="nav-link" href="#contato" uk-scroll="offset: 100;">
                           <span class="d-block weight-500">
                              Central de 
                           </span>
                           <span class="weight-700">
                              ATENDIMENTO
                           </span>
                        </a>
                     </li>
                     <li class="nav-item ms-lg-5 mt-lg-1 mt-4">
                       <a class="nav-link btnInscrever animation-grow d-inline-block" href="#pre-matricula" uk-scroll="offset: 140;">
                          Inscreva-se
                       </a>
                     </li>
                 </ul>
             </div>
         </div>
      </nav>

   </header>

   <main>

      <section id="slides">
        <div class="uk-position-relative uk-visible-toggle" tabindex="-1" uk-slideshow="animation: push; autoplay: true; autoplay-interval: 5000; pause-on-hover: false; min-height: 550; max-height: 550">

            <ul class="uk-slideshow-items">
              @foreach($banners as $banner)
                <li>
                     <img src="{{ $banner->link }}" alt="Banner do site {{ $plataforma->nome }}" uk-cover>
                     @if($banner->overlay == 'S')
                        <div class="background-overlay" style="background: {{ $banner->overlay_color ?? 'unset' }};opacity: {{ $banner->overlay_opacity ?? '0' }};"></div>
                     @endif
                    <div class="container-fluid px-xl-7 mt--50 uk-position-center-left uk-position-small uk-light">
                      
                      <h2 class="uk-margin-remove font-montserrat weight-800 display-3 ls--03" uk-slideshow-parallax="x: 300,0,-100">
                        {{ $banner->titulo }}
                      </h2>

                      @if($banner->subtitulo)
                      <p uk-slideshow-parallax="x: 300,0,-75" class="uk-margin-remove text-light fs-20">
                        {{ $banner->subtitulo }}
                      </p>
                      @endif

                      @if($banner->botao_texto)
                      <div class="mt-4-5">
                        <a href="{{ $banner->botao_link }}" class="btnInscricao d-xs-block text-center text-shadow-2" uk-slideshow-parallax="x: 300,0,-50">
                          {{ $banner->botao_texto }}
                        </a>
                      </div>
                      @endif

                    </div>
                </li>
              @endforeach
            </ul>

            <div class="uk-light">
                <a class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous uk-slideshow-item="previous"></a>
                <a class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-next uk-slideshow-item="next"></a>
            </div>

        </div>
      </section>

      <section id="pre-matricula">
         <div style="background-image: url('https://eaducativa.com/wp-content/uploads/2019/05/bg-gray.gif');">
            <div class="container-fluid py-5 px-xl-7">

               <div class="bg-white p-lg-5 p-4 box-shadow rounded">

                  <h1 class="fs-40 text-center font-montserrat weight-800 text-dark">FAÇA SUA <span class="color-faixa-verde">PRÉ-MATRÍCULA</span></h1>

                  <p class="font-poppins text-center">
                     Preenchendo o formulário abaixo
                  </p>

                  <div class="flex-center mt-4 mb-4-5"><div class="w-80px h-2px bg-base-amarelo rounded"></div></div>

                  <form method="POST">
                  @csrf

                    @if(session('success_prematricula'))
                       <div class='alert alert-success'><b>Sucesso!</b> {{ session('success_prematricula') }}</div>
                    @endif

                    <input type="hidden" name="assunto" value="Pré-Matrícula">
                     
                     <div class="d-lg-flex">
                        <input type="text" name="nome" class="form" placeholder="Seu nome" required>
                        <input type="text" name="email" class="mx-lg-3 my-3 my-lg-0 form" placeholder="Seu email" required>
                        <input type="text" name="telefone" class="form" placeholder="Telefone ou Celular" required>
                     </div>
                     <div class="w-100 mt-3">
                        <select name="curso_id" class="form weight-700 color-777" required>
                           <option value="">Selecione o Curso de Interesse</option>
                            @foreach($cursos as $curso)
                              <option value="{{ $curso->id }}" class="text-dark">{{ $curso->nome }}</option>
                            @endforeach
                        </select>
                     </div>
                     <div class="mt-3 mb-2">
                       <label class="cursor-pointer font-poppins fs-14 color-base-blue">
                          <input type="checkbox" class="" name="consentimento" value="1" required> Concordo que os dados pessoais fornecidos acima serão utilizados para envio de conteúdo informativo, analítico e publicitário sobre produtos, serviços e assuntos gerais, nos termos da Lei Geral de Proteção de Dados.
                       </label>
                     </div>
                     
                     <input type="text" name="check" class="invisible h-0px d-block" placeholder="Não preencha este campo.">

                     <button type="submit" name="sentContato" value="S" class="btnInscrever weight-700 box-shadow mt--20px">ENVIAR</button>

                  </form>

               </div>

            </div>
         </div>
      </section>

      <section class="py-5">
         <div class="container-fluid px-xl-3" id="cursos">

            <h1 class="fs-37 text-center font-montserrat text-dark weight-800">NOSSOS <span class="color-faixa-verde">CURSOS</span></h1>

            <div class="mb-4-5 container">
               <p class="font-poppins text-center">
                  A UNITERP conta com diversas modalidades de cursos e serviços, adequando-se às necessidades específicas de cada cliente.
               </p>
            </div>

            <div class="flex-center mt-4-5 mb-4-5"><div class="w-80px h-2px bg-base-amarelo rounded"></div></div>

            <div class="d-lg-flex">

              @foreach($cursos as $curso)
               <a href="/curso/{{$curso->slug}}" class="w-100 px-lg-2">
                  <div class="w-100 uk-inline-clip uk-transition-toggle rounded box-shadow mb-4 mb-lg-0" tabindex="0">
                     <img src="{{ $curso->foto_capa }}" class="uk-transition-scale-up uk-transition-opaque object-fit-cover w-100 h-lg-350 h-xs-350">
                     <div class="uk-position-bottom fig-caption flex-center px-3">
                        <h5 class="text-light text-shadow-2 fs-17 font-montserrat weight-700 ls-05 text-uppercase text-center">
                           {{ $curso->nome }}
                        </h5>
                     </div>
                  </div>
               </a>
              @endforeach

            </div>

         </div>
      </section>

      <section id="quem-somos" class="py-5">
         <div class="bg-faixa p-lg-5 p-4 h-lg-325">
            <div class="container-fluid px-xl-4">
               <div class="row">

                  <div class="col-lg-6 mb-4 mb-lg-0">

                     <div class="pe-lg-5">

                        <h1 class="fs-35 font-montserrat color-faixa weight-800">QUEM SOMOS</h1>
                        <div class="my-4 w-80px h-2px bg-base-amarelo rounded"></div>

                        <p class="font-poppins color-faixa fs-14">
                           Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
                        </p>

                     </div>

                  </div>

                  <div class="col-lg-6">

                     <div class="uk-slider-container-offset" uk-slider>

                         <div class="uk-position-relative uk-visible-toggle uk-light" tabindex="-1">

                             <ul class="uk-slider-items uk-child-width-1-1@s uk-grid">
                                 <li>
                                     <div class="uk-card box-shadow bg-white rounded-10 h-lg-325 cursor-drag">
                                         <div class="uk-card-body text-dark">
                                             <i class="fas fa-quote-left fs-50 color-faixa-verde opacity-04"></i>
                                             <div class="mt-2 d-lg-flex align-items-center">
                                                <div class="w-100">
                                                   <img src="{{ asset('sites-proprios/sd-saude/images/dep1.jpg') }}" class="object-fit-cover rounded-circle" width="80" height="80">
                                                   <p class="mb-0 mt-3 color-faixa-verde text-lg-center ms--lg-5 weight-600">
                                                      Maria Joana
                                                   </p>
                                                </div>
                                                <p class="mt-lg-3 mt-0 ms-lg-4 font-poppins fs-14 color-555">
                                                   Tinha meu próprio site rodando uma plataforma EAD, mas gerenciar tudo, além dos problemas que sempre aconteciam, estava ficando impossível! Foi então que encontrei a EAD Plataforma e finalmente achei a estabilidade que precisava. O sistema está constantemente melhorando e o suporte é excelente. Recomendo!
                                                </p>
                                             </div>
                                         </div>
                                     </div>
                                 </li>
                                 <li>
                                     <div class="uk-card box-shadow bg-white rounded-10 h-lg-325 cursor-drag">
                                         <div class="uk-card-body text-dark">
                                             <i class="fas fa-quote-left fs-50 color-faixa-verde opacity-04"></i>
                                             <div class="mt-2 d-lg-flex align-items-center">
                                                <div class="w-100">
                                                   <img src="{{ asset('sites-proprios/sd-saude/images/dep1.jpg') }}" class="object-fit-cover rounded-circle" width="80" height="80">
                                                   <p class="mb-0 mt-3 color-faixa-verde text-lg-center ms--lg-5 weight-600">
                                                      Maria Joana
                                                   </p>
                                                </div>
                                                <p class="mt-lg-3 mt-0 ms-lg-4 font-poppins fs-14 color-555">
                                                   Tinha meu próprio site rodando uma plataforma EAD, mas gerenciar tudo, além dos problemas que sempre aconteciam, estava ficando impossível! Foi então que encontrei a EAD Plataforma e finalmente achei a estabilidade que precisava. O sistema está constantemente melhorando e o suporte é excelente. Recomendo!
                                                </p>
                                             </div>
                                         </div>
                                     </div>
                                 </li>
                                 <li>
                                     <div class="uk-card box-shadow bg-white rounded-10 h-lg-325 cursor-drag">
                                         <div class="uk-card-body text-dark">
                                             <i class="fas fa-quote-left fs-50 color-faixa-verde opacity-04"></i>
                                             <div class="mt-2 d-lg-flex align-items-center">
                                                <div class="w-100">
                                                   <img src="{{ asset('sites-proprios/sd-saude/images/dep1.jpg') }}" class="object-fit-cover rounded-circle" width="80" height="80">
                                                   <p class="mb-0 mt-3 color-faixa-verde text-lg-center ms--lg-5 weight-600">
                                                      Maria Joana
                                                   </p>
                                                </div>
                                                <p class="mt-lg-3 mt-0 ms-lg-4 font-poppins fs-14 color-555">
                                                   Tinha meu próprio site rodando uma plataforma EAD, mas gerenciar tudo, além dos problemas que sempre aconteciam, estava ficando impossível! Foi então que encontrei a EAD Plataforma e finalmente achei a estabilidade que precisava. O sistema está constantemente melhorando e o suporte é excelente. Recomendo!
                                                </p>
                                             </div>
                                         </div>
                                     </div>
                                 </li>
                             </ul>

                             <a class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous uk-slider-item="previous"></a>
                             <a class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-next uk-slider-item="next"></a>

                         </div>

                         <ul class="uk-slider-nav uk-dotnav uk-flex-center uk-margin"></ul>

                     </div>

                  </div>

               </div>
            </div>
         </div>
      </section>

      <section>
         <div style="background-image: url('https://eaducativa.com/wp-content/uploads/2019/05/bg-gray.gif');">
            <div class="container-fluid px-xl-7 py-5 mb-5 mt-lg-5">

               <h1 class="mb-4 fs-35 text-center font-montserrat text-dark weight-800">
                  MOTIVOS PARA ESCOLHER A <span class="color-faixa-verde">SD SAÚDE</span>
               </h1>

               <div class="flex-center mt-4-5 mb-4-5"><div class="w-80px h-2px bg-base-amarelo rounded"></div></div>

               <div class="row text-center">

                  <div class="col-lg-3 mb-4 mb-lg-0">
                     <div class="bg-white p-4 box-shadow rounded">
                        <i class="fas fa-desktop d-block mb-3 color-faixa-verde fs-50"></i>
                        <p class="mb-0 font-montserrat weight-700 color-444 fs-17">
                           Canal Conecta
                        </p>
                        <p class="mb-0 font-poppins fs-14">
                           Plataforma exclusiva de vagas de emprego e estágio com diversas oportunidades para aplicar.
                        </p>
                     </div>
                  </div>
                  <div class="col-lg-3 mb-4 mb-lg-0">
                     <div class="bg-white p-4 box-shadow rounded">
                        <i class="fas fa-chalkboard-teacher d-block mb-3 color-faixa-verde fs-50"></i>
                        <p class="mb-0 font-montserrat weight-700 color-444 fs-17">
                           Aula Destaque
                        </p>
                        <p class="mb-0 font-poppins fs-14">
                           Plataforma exclusiva de vagas de emprego e estágio com mais de 400 mil oportunidades abertas.
                        </p>
                     </div>
                  </div>
                  <div class="col-lg-3 mb-4 mb-lg-0">
                     <div class="bg-white p-4 box-shadow rounded">
                        <i class="fas fa-chart-line d-block mb-3 color-faixa-verde fs-50"></i>
                        <p class="mb-0 font-montserrat weight-700 color-444 fs-17">
                           Trilhas de Carreira
                        </p>
                        <p class="mb-0 font-poppins fs-14">
                           Plataforma exclusiva de vagas de emprego e estágio com mais de 400 mil oportunidades abertas.
                        </p>
                     </div>
                  </div>
                  <div class="col-lg-3 mb-4 mb-lg-0">
                     <div class="bg-white p-4 box-shadow rounded">
                        <i class="fa fa-edit d-block mb-3 color-faixa-verde fs-50"></i>
                        <p class="mb-0 font-montserrat weight-700 color-444 fs-17">
                           Avaliação Continuada
                        </p>
                        <p class="mb-0 font-poppins fs-14">
                           Plataforma exclusiva de vagas de emprego e estágio com mais de 400 mil oportunidades abertas.
                        </p>
                     </div>
                  </div>

               </div>

               <div class="text-center pt-3 mt-5">
                 <a class="btnInscreverMotivos weight-700 text-decoration-none animation-grow text-shadow-2">
                    INSCREVA-SE AGORA!
                 </a>  
               </div>

            </div>
         </div>
      </section>

      <section class="py-3 bg-faixa" id="contato">
         <div class="container-fluid px-xl-7">
            <div class="d-flex justify-content-between align-items-center">
               <h1 class="font-poppins weight-600 color-faixa fs-15 pe-4 m-0">
                  FALE COM A GENTE: {{ $plataforma->whatsapp ?? '(00) 00000-0000' }} E TIRE TODAS AS SUAS DÚVIDAS NO NOSSO WHATSAPP
               </h1>
               <a href="https://api.whatsapp.com/send?phone=55{{preg_replace('/\D/', '', $plataforma->whatsapp)}}&text=Olá, vim através do Site SD SAÚDE." target="_blank" class="text-center text-decoration-none color-faixa border-faixa font-popins weight-600 px-3 py-2 rounded hover-white">
                  <i class="fab fa-whatsapp"></i>
                  FALAR AGORA
               </a>
            </div>
         </div>   
      </section>

   </main>

   <footer style="background-image: url('{{ asset('sites-proprios/sd-saude/images/bg-rodape.jpg') }}'); background-repeat: no-repeat; background-size: cover; background-position: center;">
      <div class="container-fluid px-xl-7 pt-5">

         <h1 class="fs-40 text-center font-montserrat text-light weight-800">FALE CONOSCO</h1>
         <div class="flex-center mt-4-5 mb-4-5"><div class="w-80px h-2px bg-base-amarelo rounded"></div></div>

         <div class="row align-items-center py-lg-4 px-3 px-md-0">

            <div class="col-lg-6 mt-4 mb-5 mb-lg-0">      

               <div class="mb-4-5">
                 <p class="mb-0 font-opensans weight-600 text-white d-flex align-items-center">
                   <i class="fa fa-clock fs-35 color-base-amarelo"></i>
                   <span class="ms-3">HORÁRIO DE ATENDIMENTO</span>
                 </p>
                 <p class="mt-1 mt-lg-0 mb-0 font-opensans weight-600 fs-15 color-faixa ms-lg-5">
                   &nbsp;Segunda à Sexta-Feira: dàs 07:00h às 18:00h
                 </p>
               </div>

               <div class="mb-4-5">
                 <p class="mb-0 font-opensans weight-600 text-white d-flex align-items-center">
                   <i class="fa fa-envelope fs-35 color-base-amarelo"></i>
                   <span class="ms-3">EMAIL </span>
                 </p>
                 <p class="mt-1 mt-lg-0 mb-0 font-opensans weight-600 fs-15 color-faixa ms-lg-5">
                   &nbsp;{{ $plataforma->email ?? 'email@exemplo.com' }}
                 </p>
               </div>

               <div class="mb-4-5">
                 <p class="mb-0 font-opensans weight-600 text-white d-flex align-items-center">
                   <i class="fab fa-whatsapp fs-40 color-base-amarelo"></i>
                   <span class="ms-3">CONTATO</span>
                 </p>
                 <div class="mt-1 mt-lg-0 mb-0 font-opensans weight-600 fs-15 color-faixa ms-lg-5">
                   <p class="mb-0">
                     Unidade Pinda:
                     <a href="https://api.whatsapp.com/send?phone=5512992609279" target="_blank" class="text-d-none color-faixa hover-white">(12) 99260-9279</a>
                   </p>
                 </div>
               </div>

               <div class="d-flex mt-4">

                  <a href="{{ $plataforma->facebook }}" target="_blank" class="me-4 fs-40 color-base-amarelo color-hover-amarelo">
                       <i class="fab fa-facebook-square"></i>
                  </a>
                  <a href="{{ $plataforma->instagram }}" target="_blank" class="fs-40 color-base-amarelo color-hover-amarelo">
                     <i class="fab fa-instagram flex-center"></i>
                  </a>

               </div>

            </div>

            <div class="col-lg-6">
             <form method="POST">
             @csrf

               @if(session('success'))
                 <div class='alert alert-success'><b>Sucesso!</b> {{ session('success') }}</div>
               @endif
                
               <input type="hidden" name="assunto" value="Fale Conosco">
               <input type="text" name="nome" class="form-contact mb-2" placeholder="Nome" required>
               <input type="tel" name="celular" class="form-contact mb-2" placeholder="Telefone" required>
               <input type="email" name="email" class="form-contact mb-2" placeholder="Email" required>
               <textarea name="mensagem" placeholder="Mensagem" class="form-contact h-min-100 mb-2 p-3 h-min-100"></textarea>
               
              <div class="my-3">
                <label class="cursor-pointer font-poppins fs-14 text-white">
                   <input type="checkbox" class="" name="consentimento" value="1" required> 
                   Concordo que os dados pessoais fornecidos acima serão utilizados para envio de conteúdo informativo, analítico e publicitário sobre produtos, serviços e assuntos gerais, nos termos da Lei Geral de Proteção de Dados.
                </label>
               </div>

               <input type="text" name="check" class="invisible h-0px d-block" placeholder="Não preencha este campo.">
               <input type="submit" value="Enviar" name="sentContato" class="btnEntrarEmContato">

             </form>
            </div>
            
         </div>

         <div id="unidades">

            <hr class="my-5 border border-light" />

            <h1 class="fs-35 font-montserrat text-light weight-800">UNIDADES</h1>
            <div class="mb-4-5 mt-4 w-80px h-2px bg-base-amarelo rounded"></div>

            <div class="row">

               <div class="mb-4 col-lg-3">
                  <div class="p-4 rounded-2 border-unidade h-lg-325">
                     <p class="mb-0 font-opensans weight-600 color-faixa d-flex align-items-center">
                        PINDA - SP
                     </p>
                     <hr class="border border-faixa" />
                     <p class="font-opensans weight-600 fs-15 color-faixa">
                        <i class="fa fa-envelope color-base-amarelo"></i> 
                        &nbsp;sdpindamonhangaba@gmail.com
                     </p>
                     <p>
                        <a href="https://api.whatsapp.com/send?phone=5512992609279" target="_blank" class="text-d-none hover-white font-opensans weight-600 fs-15 color-faixa">
                           <i class="fab fa-whatsapp color-base-amarelo"></i>
                           &nbsp;(12) 99260-9279
                        </a>
                     </p>
                     <address class="font-opensans weight-600 fs-15 color-faixa">
                        <i class="fa fa-map-marker-alt color-base-amarelo"></i>
                        Pindamonhangaba. CEP 12400131 n 611end. Rua Bicudo Leme bairro Jardim irmãos Braga. Cidade Pindamonhangaba.
                     </address>
                  </div>
               </div>

            </div>

         </div>

         <hr class="mt-5 border border-light" />

         <div class="pb-3 d-lg-flex justify-content-between align-items-center">

            <p class="text-xs-center mb-1 mb-lg-0 p-0 color-faixa font-poppins fs-11 ls-05">
               SD SAÚDE © 2021 - Todos os direitos reservados.
            </p>

            <p class="text-xs-center m-0 p-0 color-faixa font-poppins fs-11 ls-05">
               Desenvolvido por <a href="https://wozcode.com" target="_blank" class="text-danger text-d-none fs-11 weight-600 text-shadow-1">WOZCODE</a>
            </p>

         </div>

      </div>
   </footer>

   <a href="https://api.whatsapp.com/send?phone=55{{preg_replace('/\D/', '', $plataforma->whatsapp)}}&text=Olá, vim através do Site SD SAÚDE." target="_blank" class="text-white fs-30">
       <div class="fixed-bottom right-0 me-3 mb-3 z-3">
           <div class="square-60 rounded-circle bg-wpp flex-center box-shadow-wpp">
               <i class="fab fa-whatsapp"></i>
           </div>
       </div>
   </a>

   <script src="https://kit.fontawesome.com/2f91eaaef4.js" crossorigin="anonymous"></script>  
   <script src="https://cdn.jsdelivr.net/npm/uikit@3.5.9/dist/js/uikit.min.js"></script>
   <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-U1DAWAznBHeqEIlVSCgzq+c9gqGAJn5c/t99JyeKa9xxaYpSvHU5awsuZVVFIhvj" crossorigin="anonymous"></script>
   <script>

      var isMobile = {
          Android: function() {
              return navigator.userAgent.match(/Android/i);
          },
          BlackBerry: function() {
              return navigator.userAgent.match(/BlackBerry/i);
          },
          iOS: function() {
              return navigator.userAgent.match(/iPhone|iPad|iPod/i);
          },
          Opera: function() {
              return navigator.userAgent.match(/Opera Mini/i);
          },
          Windows: function() {
              return navigator.userAgent.match(/IEMobile/i) || navigator.userAgent.match(/WPDesktop/i);
          },
          any: function() {
              return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
          }
      };

      if(isMobile.any() != null)
      {
        const navLinks = document.querySelectorAll('.nav-item');
        const menuToggle = document.getElementById('navbarSupportedContent');
        navLinks.forEach((l) => {
            l.addEventListener('click', () => { new bootstrap.Collapse(menuToggle) })
        })
      }

      window.onscroll = function() {myFunction()};

      var header = document.getElementById("menu");
      var sticky = header.offsetTop;

      function myFunction() {
        if (window.pageYOffset > sticky) {
          header.classList.add("fixed-top");
        } else {
          header.classList.remove("fixed-top");
        }
      }
   </script>

  <div class="box-cookies d-none fixed-bottom bg-white py-3 px-4 box-shadow">
    <div class="d-flex align-items-center justify-content-between px-xl-3">
       <p class="msg-cookies font-poppins mb-0 p-0 color-faixa-verde weight-600">
          Este site usa cookies para garantir que você obtenha a melhor experiência. 
          <a href="#ModalPoliticaPrivacidade" data-bs-toggle="modal" class="color-faixa-verde text-decoration-underline">
            Política de Privacidade
          </a>
       </p>
       <button class="btn-cookies btnInscrever weight-700 box-shadow">Ok!</button>
    </div>
  </div>
  <div class="modal fade" id="ModalPoliticaPrivacidade" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-dialog-scrollable modal-lg modal-dialog-centered border-0">
        <div class="modal-content border-0">
            <div class="modal-header bg-faixa">
              <h3 class="modal-title fs-20 text-white" id="exampleModalLabel">Política de Privacidade</h3>
              <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body px-lg-4">

              <p>
                What is Lorem Ipsum?
                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
              </p>
              <p>
                What is Lorem Ipsum?
                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
              </p>

            </div>
        </div>
      </div>
  </div>
  <script>
  (() => {
    if (!localStorage.getItem('accept')) {
      document.querySelector(".box-cookies").classList.remove('d-none');
    }
    
    const acceptCookies = () => {
      document.querySelector(".box-cookies").classList.add('d-none');
      localStorage.setItem('accept', 'S');
    };
    
    const btnCookies = document.querySelector(".btn-cookies");

    btnCookies.addEventListener('click', acceptCookies);
  })();
  </script>

</body>
</html>