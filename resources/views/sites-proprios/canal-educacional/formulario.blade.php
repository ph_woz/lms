<!doctype html>
<html lang="pt-br">
<head>

  <!-- Google tag (gtag.js) -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=G-MREB450HCT"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'G-MREB450HCT');
  </script>
  
   <meta charset="utf-8">
   <meta name="viewport" content="width=device-width, initial-scale=1">

   <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
   <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
   <link rel="stylesheet" href="{{ asset('css/util.css') }}?v=14">
   <link rel="stylesheet" href="{{ asset('sites-proprios/canal-educacional/css/home.css') }}?v=23">

   <title>{{ $seo->seo_title ?? $plataforma->nome }}</title>
   <meta name="robots" content="index, follow">
   <meta name="description" content="{{ $seo->seo_description ?? null }}"/>
   <meta name="keywords" content="{{ $seo->seo_keywords ?? null }}"/>
   <link rel="canonical" href="{{ Request::url() }}" />
   <link rel="shortcut icon" href="{{ $plataforma->favicon }}">
   <meta property="og:locale" content="pt_BR" />
   <meta property="og:type" content="website" />
   <meta property="og:title" content="{{ $seo->seo_title ?? $plataforma->nome }}" />
   <meta property="og:description" content="{{ $seo->seo_description ?? null }}" />
   <meta property="og:url" content="{{ Request::url() }}" />
   <meta property="og:site_name" content="{{ $plataforma->nome }}" />
   <meta property="og:image" content="{{ $plataforma->logotipo }}" />
   <meta name='author' content='WOZCODE | Pedro Henrique' />

</head>
<body style="background-image: url('https://supletivocanalead.com.br/wp-content/uploads/2023/05/Canal-Educacional-BG-1.png'); background-size: cover;" id="inicio">

   <main class="pt-9rem">

      <form method="POST" class="container-fluid px-xl-5 pb-5 pt-3 w-lg-60" id="formContainer">
      @csrf

        <section class="bg-white p-lg-5 p-4 my-5 rounded-2 shadow">

          <div id="formDescricao">
            {!! nl2br($formulario->texto) !!}
          </div>

          <hr/>

              <div class="row mt-4">

                  @foreach($perguntas as $pergunta)
                      
                      @if($pergunta->tipo_input == 'select')
                          <div class="mb-4 col-lg-6">
                              <label>{{ $pergunta->pergunta }}</label>
                              <select name="{{ $pergunta->validation ?? 'respostas['.$pergunta->id.']' }}" class="{{ $pergunta->validation ?? null }} select-form" @if($pergunta->obrigatorio == 'S') required @endif>
                                  <option value="" selected disabled>Selecione</option>
                                  @foreach(unserialize($pergunta->options) as $option)
                                      <option value="{{ $option }}" @if(old($pergunta->validation) == $option) selected @endif>{{ $option }}</option>
                                  @endforeach
                              </select> 
                          </div>
                      @endif
                      
                      @if($pergunta->tipo_input == 'text')
                      <div class="mb-4 col-lg-6">
                          <label>{{ $pergunta->pergunta }}</label>
                          <input type="text" name="{{ $pergunta->validation ?? 'respostas['.$pergunta->id.']' }}" class="{{ $pergunta->validation ?? null }} form-control p-form" @if($pergunta->obrigatorio == 'S') required @endif>
                      </div>
                      @endif
                      
                      @if($pergunta->tipo_input == 'radio')
                      <div class="mb-4 col-12">
                          <label class="d-block mb-1">{{ $pergunta->pergunta }}</label>
                          @foreach(unserialize($pergunta->options) as $option)
                              <label for="opt{{$loop->index}}p{{$pergunta->id}}" class="mb-1 d-flex align-items-center">
                                  <input type="radio" class="square-15" id="opt{{$loop->index}}p{{$pergunta->id}}" name="{{ $pergunta->validation ?? 'respostas['.$pergunta->id.']' }}" value="{{ $option }}" @if($pergunta->obrigatorio == 'S') required @endif>
                                  <span class="ms-2">{!! $option !!}</span>
                              </label>
                          @endforeach
                      </div>
                      @endif

                      @if($pergunta->tipo_input == 'checkbox')
                      <div class="mb-4 col-12">
                          <label class="d-block mb-1">{{ $pergunta->pergunta }}</label>
                          @foreach(unserialize($pergunta->options) as $option)
                              <label for="opt{{$loop->index}}p{{$pergunta->id}}" class="mb-1 d-flex align-items-center">
                                  <input type="checkbox" class="square-15" id="opt{{$loop->index}}p{{$pergunta->id}}" name="{{ $pergunta->validation ?? 'respostas['.$pergunta->id.']' }}" value="{{ $option }}">
                                  <span class="ms-2">{!! $option !!}</span>
                              </label>
                          @endforeach
                      </div>
                      @endif

                  @endforeach

              </div>

              <input type="text" name="check" class="h-0 border-0 outline-0 float-end" readonly>

              <button type="submit" name="sent" value="ok" id="formBtnEnviar" class="mt-3 btnAreaAluno hover-d-none weight-600 fs-17 ls-05 d-inline-block rounded-3 box-shadow">
                Enviar
              </button>

        </section>

      </form>

      <div class="modal fade" id="modalCallback" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalCenterTitle">Inscrição enviada com sucesso!</h5>
            </div>
            <div class="modal-body">
              
                  {!! $formulario->texto_retorno !!}

            </div>
            <div class="modal-footer">
              <a href="{{ $formulario->link_retorno }}" class="btnAreaAluno hover-d-none weight-600 fs-17 ls-05 d-inline-block rounded-3 box-shadow">
                  {{ $formulario->texto_botao_retorno ?? 'Retornar' }}
              </a>
            </div>
          </div>
        </div>
      </div>


   </main>

   <script src="https://kit.fontawesome.com/2f91eaaef4.js" crossorigin="anonymous"></script>  
   <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-U1DAWAznBHeqEIlVSCgzq+c9gqGAJn5c/t99JyeKa9xxaYpSvHU5awsuZVVFIhvj" crossorigin="anonymous"></script>


    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.js"></script>

    @if(session('success') && $formulario->texto_retorno != null)
    <script>
        $(document).ready(function() {
            $("#modalCallback").modal('show');
        });
    </script>
    @endif

    <script>

        $(document).ready(function() {

            function limpa_formulário_cep() {

                $(".logradouro").val("");
                $(".bairro").val("");
                $(".cidade").val("");
                $(".estado").val("");
            }
            

            $(".cep").keyup(function() {

                var cep = $(this).val().replace(/\D/g, '');

                if (cep != "" && cep.length >= 8) {

                    var validacep = /^[0-9]{8}$/;

                    if(validacep.test(cep)) {

                        $(".logradouro").val("...");
                        $(".bairro").val("...");
                        $(".cidade").val("...");
                        $(".estado").val("...");

                        $.getJSON("https://viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {

                            if (!("erro" in dados)) {
                                $(".logradouro").val(dados.logradouro);
                                $(".bairro").val(dados.bairro);
                                $(".cidade").val(dados.localidade);
                                $(".estado").val(dados.uf);
                            } 
                            else {
                               
                                limpa_formulário_cep();
                            }
                        });
                    } 
                    else {
                        
                        limpa_formulário_cep();
                        
                    }
                }
                else {
                  
                    limpa_formulário_cep();
                }
            });
        });

    </script>

    <script>
      $(".email").attr('type','email');
      $(".password").attr('type','password');
        $(".cpf").mask('000.000.000-00').attr('minlength','14');
        $(".data_nascimento").mask('00/00/0000');
        $(".cep").mask('00000-000');
        $(".estado, .uf").attr('minlength','2').attr('maxlength','2');
        $(".numero").mask('000000000');
        $(".password").val('');

        $(document).on("focus", ".celular, .telefone", function() { 
          jQuery(this)
              .mask("(99) 9999-99999")
              .change(function (event) {  
                  target = (event.currentTarget) ? event.currentTarget : event.srcElement;  
                  phone = target.value.replace(/\D/g, '');
                  element = $(target);  
                  element.unmask();  
                  if(phone.length > 10) {  
                      element.mask("(99) 99999-9999");  
                  } else {  
                      element.mask("(99) 9999-9999?9");  
                  }  
          });
        });

        $(document).ready(function() {

            function limpa_formulário_cep() {

                $(".logradouro").val("");
                $(".bairro").val("");
                $(".cidade").val("");
                $(".estado").val("");
            }
            

            $(".cep").keyup(function() {

                var cep = $(this).val().replace(/\D/g, '');

                if (cep != "" && cep.length >= 8) {

                    var validacep = /^[0-9]{8}$/;

                    if(validacep.test(cep)) {

                        $(".logradouro").val("...");
                        $(".bairro").val("...");
                        $(".cidade").val("...");
                        $(".estado").val("...");

                        $.getJSON("https://viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {

                            if (!("erro" in dados)) {
                                $(".logradouro").val(dados.logradouro);
                                $(".bairro").val(dados.bairro);
                                $(".cidade").val(dados.localidade);
                                $(".estado").val(dados.uf);
                            } 
                            else {
                               
                                limpa_formulário_cep();
                            }
                        });
                    } 
                    else {
                        
                        limpa_formulário_cep();
                        
                    }
                }
                else {
                  
                    limpa_formulário_cep();
                }
            });
        });

        $('.estado, .uf').keyup(function(){
            $(this).val($(this).val().toUpperCase());
        });

    </script>

   <script>

      $(".btn-cat").click(function(e) {

         $(".btn-cat").removeClass('bg-active-cat');
         $(this).addClass('bg-active-cat');

      });
      
    </script>

    @if(session()->has('success'))
        <div id="box-toast-success" class="boxToast">
            <div id="alertSuccess" class="toast-success">
                <p class="mt-2 text-white font-inter fs-14">
                    <span class="weight-800">Sucesso!&nbsp;</span>{{ session('success') }}
                </p>
            </div>
        </div>
        <script>
            setTimeout(function() {

                document.getElementById('box-toast-success').style.display = 'none';

            }, 2500);
        </script>
    @endif

    @if(session()->has('danger'))
        <div id="box-toast-danger" class="boxToast">
            <div id="alertDanger" class="toast-danger">
                <p class="mt-2 text-white font-inter fs-14">
                    <span class="weight-800">Ops!&nbsp;</span>{{ session('danger') }}
                </p>
            </div>
        </div>
        <script>
            setTimeout(function() {

                document.getElementById('box-toast-danger').style.display  = 'none';

            }, 2500);
        </script>
    @endif

</body>
</html>