<!doctype html>
<html lang="pt-br">
<head>

    <!-- Global site tag (gtag.js) - Google Ads: 10893363242 --> <script async src="https://www.googletagmanager.com/gtag/js?id=AW-10893363242"></script> <script> window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'AW-10893363242'); </script>

   <meta charset="utf-8">
   <meta name="viewport" content="width=device-width, initial-scale=1">

   <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <link rel="stylesheet" href="https://edtech.wozcode.com/css/app.css">
    <link rel="stylesheet" href="https://lms.wozcode.com/css/util.css?v=3">

    <link rel="shortcut icon" href="images/favicon.ico">
    <link href="https://edtech.wozcode.com/" rel="canonical">

    <meta name="robots" content="index, follow">
    <meta name='author' content="WOZCODE | Pedro Henrique" />
    <meta name="description" content="Desenvolvemos Sites, Sistemas, Apps, CRM para a sua instituição de ensino.">
    <meta name="keywords" content="wozcode, plataforma ead, plataforma lms, sistema de gestão escolar, software escolar, sistema escolar, software para escolas, sistema para instituições de ensino">
    <title>Fábrica de Software para Instituições de Ensino - WOZCODE EDTECH</title>

    <meta property="og:locale" content="pt_BR">
    <meta property="og:type" content="website">
    <meta property="og:image" content="images/logo.png">
    <meta property="og:url" content="https://edtech.wozcode.com/">
    <meta property="og:description" content="Desenvolvemos Sites, Sistemas, Apps, CRM para a sua instituição de ensino.">
    <meta property="og:title" content="Fábrica de Software para Instituições de Ensino - WOZCODE EDTECH">
    <meta property="fb:app_id" content="1976461969328043">

    <style>


        .bg-app { background: #EFEFEF;  }

        .color-base { color: #2d4b70!important; }
        .bg-base { background: #2d4b70; }

        .fig-caption { 
           margin-top: -47px;
           z-index: 333;
           position: relative;
          background-color: rgba(15, 31, 48, .9); 
          padding:15px;
        }

        .box-convenios {
          box-shadow: 0px 5px 10px -5px #022B73;
          border-radius: 50px!important;
        }


        .bg-default { background: #152d45; }

        .bg-roxo-fraco { background: #645caa; }

        .titulo {
            font-family: 'Poppins', sans-serif;
            font-weight: 700;
            font-size: 54px;
            line-height: 1.07;
            color: #152d45;
            position: relative;
        }


      .btn-default {
        font-family: Poppins,sans-serif;
        box-sizing: border-box!important;
        text-decoration: none!important;
        display: inline-block!important;
        font-weight: 400!important;
        text-align: center!important;
        vertical-align: middle!important;
        user-select: none!important;
        border: 1px solid transparent!important;
        font-size: 1rem!important;
        line-height: 1.5!important;
        transition: color .1s ease-in-out,background-color .1s ease-in-out,border-color .1s ease-in-out,box-shadow .1s ease-in-out;
        color: #fff!important;
        outline: none!important;
        background: #152d45!important;
        border-color: #152d45!important;
        border-radius: 23px!important;
        padding: 11px 33px!important;
      }

      .btn-default:hover, .bg-default-hover:hover {
          transition: all 1s ease!important;
          background: #e46026!important;
          border-color: #e46026!important;
      }

    </style>

</head>
<body id="inicio">

  <div id="fb-root"></div>
  <script>
    window.fbAsyncInit = function() {
      FB.init({
        xfbml            : true,
        version          : 'v3.2'
      });
    };

    (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = 'https://connect.facebook.net/pt_BR/sdk/xfbml.customerchat.js';
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));</script>

   <header>
      <nav class="navbar navbar-expand-lg navbar-light bg-light box-shadow fixed-top">
         <div class="container">
            <a class="navbar-brand titulo fs-20" href="#inicio">
               Melhor Academia
            </a>
            <button class="navbar-toggler outline-0 border-0" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
               <span class="fa fa-bars text-white"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
               <ul class="navbar-nav mb-2 mb-lg-0 ms-5">
                  <li class="nav-item">
                     <a class="nav-link btn-default" href="#ModalOrcamento" data-bs-toggle="modal">Para Academias</a>
                  </li>
                  <li class="nav-item">
                     <a class="nav-link btn-default" href="#ModalOrcamento" data-bs-toggle="modal">Para empresas</a>
                  </li>
                  <li class="nav-item">
                     <a class="nav-link btn-default" href="#ModalOrcamento" data-bs-toggle="modal">Para alunos</a>
                  </li>
               </ul>
               <ul class="navbar-nav mb-2 mb-lg-0 ms-auto">
                  <li class="nav-item ms-auto">
                     <a class="nav-link btn-default" href="#ModalOrcamento" data-bs-toggle="modal">Entrar</a>
                  </li>
               </ul>
            </div>
        </div>
      </nav>
   </header>

   <main>

      <section id="banner" class="bg-roxo-fraco py-5 box-shadow">
         <div class="container pt-5">

            <h1 class="fs-20 text-white text-shadow-3 font-nunito weight-800 mt-5">
               Busque, avalie e compartilhe academias e clubes perto de você
            </h1>

            <hr class="border border-light" />

            <div class="d-flex align-items-end mt-5 mt-lg-4">
               
               <div class="w-100">
                  <label class="text-light weight-600 text-shadow-1">Serviço</label>
                  <input type="text" name="servico" list="servicos" class="form-control border-0 py-3 px-4 outline-0 box-shadow" placeholder="Ex: Academia, Crossfit ou Pilates" required>
                  <datalist id="servicos">
                     <option value="Academias">Academias</option>
                     <option value="Aeróbica">Aeróbica</option>
                     <option value="Artes Marciais">Artes Marciais</option>
                     <option value="Artigos Esportivos">Artigos Esportivos</option>
                     <option value="Clubes Esportivos">Clubes Esportivos</option>
                     <option value="Crossfit">Crossfit</option>
                     <option value="Escola de Natação">Escola de Natação</option>
                     <option value="Estúdios de Dança">Estúdios de Dança</option>
                     <option value="Ioga & Meditação">Ioga & Meditação</option>
                     <option value="Personal Trainer">Personal Trainer</option>
                     <option value="Pilates">Pilates</option>
                     <option value="Quadras Esportivas">Quadras Esportivas</option>
                     <option value="Suplemento Alimentar">Suplemento Alimentar</option>
                  </datalist>
               </div>

               <div class="mx-3 w-100">
                  <label class="text-light weight-600 text-shadow-1">Local</label>
                  <input type="text" name="local" class="form-control border-0 py-3 px-4 outline-0 box-shadow" placeholder="Bairro ou Cidade" required>
               </div>

               <button type="submit" class="btn btn-dark bg-default bg-default-hover d-flex align-items-center weight-600 px-4 py-3 box-shadow">
                  <i class="fa fa-search"></i>
                  <span class="ms-2">BUSCAR</span>
               </button>

            </div>

         </div>
      </section>

      <section class="py-5" id="o-que-prezamos">
         <div class="container pb-4">

            <div class="row">

               <div class="col-lg-4">
                  
                  <a href="/blog/post/3/12-de-janeiro-dia-nacional-do-bombeiro-civil-2020">
                     <div class="w-100 box-convenios">
                        <img style="height: 375px;" src="https://helpbairros.com.br/storage/uploads/img/FIfh126WKaX4oQJgS23AiVvgCRtOsboXtxJG5k3d.jpeg" class="object-fit-cover w-100 rounded" alt="12 de Janeiro, Dia nacional do Bombeiro Civil (2020) - ABCER">
                        <div class="fig-caption rounded-bottom">
                           <h5 class="text-light text-shadow-2 fs-16 font-opensans mb-0 p-0">
                              Academias
                           </h5>
                        </div>
                     </div>
                  </a>

               </div>

               <div class="col-lg-8">

                  <div class="row mb-4">

                     <div class="col-lg-3">
                        <a href="/blog/post/3/12-de-janeiro-dia-nacional-do-bombeiro-civil-2020">
                           <div class="box-convenios">
                              <img style="height: 175px;" src="https://helpbairros.com.br/storage/uploads/img/aW4TNmxXmdP9FBRrTfWonPP4QK9lN3i2KQKASXKh.jpeg" class="object-fit-cover w-100 rounded" alt="12 de Janeiro, Dia nacional do Bombeiro Civil (2020) - ABCER">
                              <div class="fig-caption rounded-bottom">
                                 <h5 class="text-light text-shadow-2 fs-16 font-opensans mb-0 p-0">
                                    Crossfit
                                 </h5>
                              </div>
                           </div>
                        </a>
                     </div>
                     <div class="col-lg-3">
                        <a href="/blog/post/3/12-de-janeiro-dia-nacional-do-bombeiro-civil-2020">
                           <div class="box-convenios">
                              <img style="height: 175px;" src="https://helpbairros.com.br/storage/uploads/img/cr4E5D7TuK4LyMr2W8zXkyRTGR6hRyhXzhbS1Vjr.jpeg" class="object-fit-cover w-100 rounded" alt="12 de Janeiro, Dia nacional do Bombeiro Civil (2020) - ABCER">
                              <div class="fig-caption rounded-bottom">
                                 <h5 class="text-light text-shadow-2 fs-16 font-opensans mb-0 p-0">
                                    Pilates
                                 </h5>
                              </div>
                           </div>
                        </a>
                     </div>
                     <div class="col-lg-3">
                        <a href="/blog/post/3/12-de-janeiro-dia-nacional-do-bombeiro-civil-2020">
                           <div class="box-convenios">
                              <img style="height: 175px;" src="https://helpbairros.com.br/storage/uploads/img/gdJaUp9giKftMmWNGOYERsUMZBecilIV87lbFzm0.jpeg" class="object-fit-cover w-100 rounded" alt="12 de Janeiro, Dia nacional do Bombeiro Civil (2020) - ABCER">
                              <div class="fig-caption rounded-bottom">
                                 <h5 class="text-light text-shadow-2 fs-16 font-opensans mb-0 p-0">
                                    Aeróbica
                                 </h5>
                              </div>
                           </div>
                        </a>
                     </div>
                     <div class="col-lg-3">
                        <a href="/blog/post/3/12-de-janeiro-dia-nacional-do-bombeiro-civil-2020">
                           <div class="box-convenios">
                              <img style="height: 175px;" src="https://helpbairros.com.br/storage/uploads/img/B5votvzVJDbdlGEho4E1dGopljpcss7TUxdkMOxL.jpeg" class="object-fit-cover w-100 rounded" alt="12 de Janeiro, Dia nacional do Bombeiro Civil (2020) - ABCER">
                              <div class="fig-caption rounded-bottom">
                                 <h5 class="text-light text-shadow-2 fs-16 font-opensans mb-0 p-0">
                                    Natação
                                 </h5>
                              </div>
                           </div>
                        </a>
                     </div>

                  </div>

                  <div class="row">

                     <div class="col-lg-3">
                        <a href="/blog/post/3/12-de-janeiro-dia-nacional-do-bombeiro-civil-2020">
                           <div class="box-convenios">
                              <img style="height: 175px;" src="https://www.itl.cat/pngfile/big/317-3170599_student-wallpaper.jpg" class="object-fit-cover w-100 rounded" alt="12 de Janeiro, Dia nacional do Bombeiro Civil (2020) - ABCER">
                              <div class="fig-caption rounded-bottom">
                                 <h5 class="text-light text-shadow-2 fs-16 font-opensans mb-0 p-0">
                                    Artes Marciais
                                 </h5>
                              </div>
                           </div>
                        </a>
                     </div>
                     <div class="col-lg-3">
                        <a href="/blog/post/3/12-de-janeiro-dia-nacional-do-bombeiro-civil-2020">
                           <div class="box-convenios">
                              <img style="height: 175px;" src="https://ae01.alicdn.com/kf/HTB1tpwPNVXXXXcMXpXXq6xXFXXXN/Pap-is-de-parede-coloridos-arco-ris-arte-moderna-decora-o-de-interior-pintura-de-parede.jpg_Q90.jpg_.webp" class="object-fit-cover w-100 rounded" alt="12 de Janeiro, Dia nacional do Bombeiro Civil (2020) - ABCER">
                              <div class="fig-caption rounded-bottom">
                                 <h5 class="text-light text-shadow-2 fs-16 font-opensans mb-0 p-0">
                                    Imóveis
                                 </h5>
                              </div>
                           </div>
                        </a>
                     </div>
                     <div class="col-lg-3">
                        <a href="/blog/post/3/12-de-janeiro-dia-nacional-do-bombeiro-civil-2020">
                           <div class="box-convenios">
                              <img style="height: 175px;" src="http://www.blogto.com/listings/fashion/upload/2008/05/20080524-fraiche2.jpg" class="object-fit-cover w-100 rounded" alt="12 de Janeiro, Dia nacional do Bombeiro Civil (2020) - ABCER">
                              <div class="fig-caption rounded-bottom">
                                 <h5 class="text-light text-shadow-2 fs-16 font-opensans mb-0 p-0">
                                    Lojas
                                 </h5>
                              </div>
                           </div>
                        </a>
                     </div>
                     <div class="col-lg-3">
                        <a href="/blog/post/3/12-de-janeiro-dia-nacional-do-bombeiro-civil-2020">
                           <div class="box-convenios">
                              <img style="height: 175px;" src="https://cdn.folhape.com.br/upload/dn_arquivo/2020/01/medico1.jpg" class="object-fit-cover w-100 rounded" alt="12 de Janeiro, Dia nacional do Bombeiro Civil (2020) - ABCER">
                              <div class="fig-caption rounded-bottom">
                                 <h5 class="text-light text-shadow-2 fs-16 font-opensans mb-0 p-0">
                                    Saúde
                                 </h5>
                              </div>
                           </div>
                        </a>
                     </div>
                     
                  </div>
                  
               </div>

               <div class="pt-5 d-flex justify-content-center">
                  <a href="" class="btn-default">
                     Ver todas as Categorias
                  </a>
               </div>

            </div>

         </div>   
      </section>

      <section class="py-5" id="o-que-prezamos">
         <div class="container pb-4">

            <h1 class="titulo fs-20">Cupons de Desconto e Promoções</h1>

            <hr/>

            <div class="row">


            </div>

         </div>
      </section>

      <section class="py-5" id="o-que-prezamos">
         <div class="container pb-4">

            <h1 class="titulo fs-20">Últimos adicionados</h1>

            <hr/>

            <div class="row">


            </div>

         </div>
      </section>

      <section class="py-5" id="o-que-prezamos">
         <div class="container pb-4">

            <h1 class="titulo fs-20">Mais bem avaliados</h1>

            <hr/>

            <div class="row">


            </div>

         </div>
      </section>

      <section class="bg-app" id="o-que-prezamos">
         <div class="container">

            <div class="row align-items-center">

               <div class="col-lg-8">

                  <h1 class="titulo fs-20">Baixe grátis o App do Melhor Academia</h1>

                  <p class="text-dark weight-600">
                     O guia com os melhores serviços e estabelecimentos da sua região. <br>
                     Veja os lugares que estão próximos a você e não fique perdido para encontrar o que procura.
                  </p>

                  <div class="d-flex">

                     <a href="" class="btn btn-dark bg-roxo-fraco border-0 flex-center box-shadow me-2" style="width: 60px;height: 50px;">
                        <i class="fs-20 fab fa-android" aria-hidden="true"></i>
                     </a>
                     <a href="" class="btn btn-dark bg-roxo-fraco border-0 flex-center box-shadow" style="width: 60px;height: 50px;">
                        <i class="fs-20 fab fa-apple" aria-hidden="true"></i>
                     </a>

                  </div>

               </div>

               <div class="col-lg-4">

                  <img src="https://helpbairros.com.br/img/phones-bottom-app.png" class="img-fluid" alt="">

               </div>

            </div>

         </div>
      </section>

   </main>

   <footer class="bg-roxo-fraco py-4">
      <div class="container-fluid">



      </div>
   </footer>

   <form method="POST" action="sendContato.php">
      <div class="modal fade" id="ModalOrcamento" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title font-raleway weight-800 color-444" id="exampleModalLabel">
                 ESTAMOS QUASE LÁ!
              </h5>
              <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">

               <input type="text" name="nome" class="mb-3 placeholder-edit form-control p-form" placeholder="Seu nome" required>

               <select name="forma_deseja_contato" onchange="setFieldFormaContato(this.value)" class="mb-3 select-edit select-form" required> 
                  <option value="">
                     Selecione a forma que deseja ser contactado
                  </option> 
                  <optgroup label="Formas">
                     <option value="WhatsApp">WhatsApp</option> 
                     <option value="Ligação">Ligação</option>
                     <option value="Email">Email</option>
                  </optgroup> 
               </select>

               <div id="boxFormasContato">
                  <input type="email" style="display:none;" name="email" id="forma_email" class="placeholder-edit form-control p-form" placeholder="Digite seu email">
                  <input type="phone" style="display:none;" name="whatsapp" id="forma_whatsapp" class="placeholder-edit form-control p-form" placeholder="(00) 00000-0000">
                  <input type="phone" style="display:none;" name="ligacao" id="forma_ligacao" class="placeholder-edit form-control p-form" placeholder="(00) 00000-0000">
               </div>

               <input type="text" name="check" class="h-0px border-0">

            </div>
            <div class="modal-footer">
              <button type="submit" name="btnSend" value="ok" class="w-100 btn btn-primary weight-600 box-shadow fs-17 px-4 py-2-3">
                  Solicitar Orçamento!
              </button>
            </div>
          </div>
        </div>
      </div>
   </form>

   <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
   <script src="https://kit.fontawesome.com/2f91eaaef4.js" crossorigin="anonymous"></script>
   <script>

      var isMobile = false;

      if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) 
          || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4))) { 
          isMobile = true;
      }

      if(isMobile == true)
      {
        const navLinks = document.querySelectorAll('.nav-item')
        const menuToggle = document.getElementById('navbarSupportedContent')
        const bsCollapse = new bootstrap.Collapse(menuToggle, {
          toggle: false
        })
        navLinks.forEach((l) => {
            l.addEventListener('click', () => { bsCollapse.toggle() })
        });
      }
      

      function setFieldFormaContato(value)
      {
         if(value == 'WhatsApp')
         {
            document.getElementById('forma_whatsapp').style.display = 'block';
            document.getElementById('forma_email').style.display = 'none';
            document.getElementById('forma_ligacao').style.display = 'none';

            document.getElementById('forma_whatsapp').setAttribute('required','');
            document.getElementById('forma_email').removeAttribute('required');
            document.getElementById('forma_ligacao').removeAttribute('required');

         }

         if(value == 'Ligação')
         {
            document.getElementById('forma_whatsapp').style.display = 'none';
            document.getElementById('forma_email').style.display = 'none';
            document.getElementById('forma_ligacao').style.display = 'block';

            document.getElementById('forma_whatsapp').removeAttribute('required');
            document.getElementById('forma_email').removeAttribute('required');
            document.getElementById('forma_ligacao').setAttribute('required','');
         }

         if(value == 'Email')
         {
            document.getElementById('forma_whatsapp').style.display = 'none';
            document.getElementById('forma_email').style.display = 'block';
            document.getElementById('forma_ligacao').style.display = 'none';

            document.getElementById('forma_whatsapp').removeAttribute('required');
            document.getElementById('forma_email').setAttribute('required','');
            document.getElementById('forma_ligacao').removeAttribute('required');
         }
      }

   </script>

</body>
</html>