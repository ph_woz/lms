@extends('layouts.aluno')
@section('content')

@section('css')
<style>

    #url-{{\Request::segment(3)}}-{{\Request::segment(4)}}  { background: #222e3c!important; }
    #href-url-{{\Request::segment(3)}} { background: #222e3c!important; }

</style>
@endsection

@section('seo_title', 'Acadêmico')
<div class="container-fluid px-xl-5 mt-5">

    <div class="row">

        <div class="col-lg-4">
        
            <div class="mb-4 p-5 rounded-10 box-shadow bg-14">

                @include('aluno.minha-conta.componentes.navbar')

            </div>

        </div>

        <div class="col-lg-8">
              
            <form method="POST">
            @csrf

                <div class="mb-4 p-5 rounded-10 box-shadow bg-14">

                    <div class="table-responsive">
                        <table class="table">
                          <thead>
                            <tr>
                              <th scope="col" class="color-light-soft">Empresa</th>
                              <th scope="col" class="color-light-soft">Cargo</th>
                              <th scope="col" class="color-light-soft">Período</th>
                              <th scope="col" class="color-light-soft">
                                <span class="float-end me-2">
                                    Ações
                                </span>
                              </th>
                            </tr>
                          </thead>
                          <tbody>
                            @foreach($experiencias as $experiencia)
                            <tr>
                                <td class="color-light-soft">
                                    {{ $experiencia->empresa }}
                                </td>
                                <td class="color-light-soft">
                                    {{ $experiencia->cargo }}
                                </td>
                                <td class="color-light-soft">
                                    {{ $experiencia->data_inicio }} -
                                    @if($experiencia->data_fim == null)
                                        Presente
                                    @else
                                        {{ $experiencia->data_fim }}
                                    @endif
                                </td>
                                <td>
                                    <span class="float-end">
                                        <a href="{{ route('aluno.minha-conta.experiencia.editar', ['id' => $experiencia->id]) }}" class="weight-600 btn btn-warning text-dark">
                                            Editar
                                        </a>
                                        <a href="#ModalDeletar" data-bs-toggle="modal" onclick="document.getElementById('deletar_registro_id').value = '{{ $experiencia->id }}'; document.getElementById('empresa_nome').innerHTML = '{{ $experiencia->empresa }}';" class="weight-600 ms-1 btn btn-danger">
                                            Deletar
                                        </a>
                                    </span>
                                </td>
                            </tr>
                            @endforeach
                          </tbody>
                        </table>
                    </div>

                </div>

            </form>

        </div>

    </div>

</div>

<form method="POST">
    @csrf
    <div class="modal fade" id="ModalDeletar" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title" id="exampleModalLabel">Deletar Experiência Profissional</h3>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body text-center py-5">
                    <input type="hidden" name="registro_id" id="deletar_registro_id">

                    <p id="empresa_nome" class="weight-600 fs-17 mb-0"></p>
                    <hr class="px-4">
                    <p class="m-0 fs-16">
                        Você tem certeza que deseja deletar esta experiência?
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="submit" name="deletar" value="S" class="btn btn-danger weight-600">Confirmar</button>
                </div>
            </div>
        </div>
    </div>
</form>

@endsection

@push('scripts')
    
    <script>

        document.getElementById('url-{{\Request::segment(3)}}').classList.add('show');

    </script>

@endpush