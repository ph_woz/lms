<?php

namespace App\Http\Livewire\Admin\Turma;

use Livewire\Component;
use App\Models\Turma;
use App\Models\Curso;
use App\Models\User;
use App\Models\Formulario;
use App\Models\Unidade;
use App\Models\Avaliacao;
use DB;
use Illuminate\Support\Facades\Http;

class Info extends Component
{
    public $turma;
    public $curso;
    public $cadastrante;
    public $unidade;
    public $formulario;
    public $password_to_delete;

    public function mount($turma_id)
    {
        $this->turma = Turma::plataforma()->find($turma_id);

        $this->unidade     = Unidade::plataforma()->where('id', $this->turma->unidade_id)->first();
        $this->cadastrante = User::plataforma()->where('id', $this->turma->cadastrante_id)->first();
        $this->curso       = Curso::plataforma()->find($this->turma->curso_id);
        $this->formulario  = Formulario::plataforma()->find($this->turma->formulario_id);
    }

    public function render()
    {
        $countAlunos = DB::table('turmas_alunos')
            ->join('users','turmas_alunos.aluno_id','users.id')
            ->where('users.plataforma_id', session('plataforma_id'))
            ->where('turmas_alunos.plataforma_id', session('plataforma_id'))
            ->where('turmas_alunos.turma_id', $this->turma->id)
            ->count();

        $avaliacoes = Avaliacao::plataforma()->select('id','referencia')->where('turma_id', $this->turma->id)->orderBy('referencia')->get();

        return view('livewire.admin.turma.info', compact('countAlunos','avaliacoes') );
    }

    public function delete()
    {
        if(\Auth::attempt(['id' => \Auth::id(), 'password' => $this->password_to_delete, 'status' => 0]))
        {
            \App\Models\TurmaAluno::plataforma()->where('turma_id', $this->turma->id)->delete();
            \App\Models\TrilhaTurma::plataforma()->where('turma_id', $this->turma->id)->delete();
            \App\Models\AvaliacaoAluno::plataforma()->where('turma_id', $this->turma->id)->delete();
            \App\Models\AvaliacaoAlunoResposta::plataforma()->where('turma_id', $this->turma->id)->delete();

            \App\Models\Log::create([
                'tipo' => 'turma',
                'ref'  => 'Turma: ' . $this->turma->nome . ' deletada por ' . \Auth::user()->nome.'.'
            ]);

            Turma::plataforma()->where('id', $this->turma->id)->delete();

            session()->flash('success', 'Turma deletada com êxito.');
            
            return redirect(route('admin.turma.lista'));

        } else {

            session()->flash('danger-livewire', 'Senha incorreta.');

            $this->dispatchBrowserEvent('toast');
        }
    }
}
