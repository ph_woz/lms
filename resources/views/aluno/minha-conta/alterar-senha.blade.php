@extends('layouts.aluno')

@section('css')
   <style>

      #url-alterar-senha { background: #222e3c!important; }

   </style>
@endsection

@section('content')

@section('seo_title', 'Acadêmico')
<div class="container-fluid px-xl-5 mt-5">

	<div class="row">

        <div class="col-lg-4">
        
            <div class="mb-4 p-5 rounded-10 box-shadow bg-14">

                @include('aluno.minha-conta.componentes.navbar')

            </div>

        </div>

		<div class="col-lg-8">
		
			<div class="mb-4 p-5 rounded-10 box-shadow bg-14">

          <form method="POST">
          @csrf

              <div class="mb-3">
                  <label class="text-light">Senha Atual</label>
                  <input type="password" name="atual_password" class="mb-2 form-control p-form bg-444 border-0 text-white py-3 color-placeholder-light" placeholder="Digite sua Senha Atual" required>
              </div>

              <div class="mb-3">
                  <label class="text-light">Nova Senha</label>
                  <input type="password" name="new_password" class="mb-2 form-control p-form bg-444 border-0 text-white py-3 color-placeholder-light" placeholder="Digite sua Nova Senha" required>
              </div>

              <button type="submit" name="sent" value="S" class="btn-suporte-enviar mt-4 px-4-5">
                  Salvar
              </button>

          </form>

			</div>

		</div>

	</div>

</div>
@endsection

@push('scripts')
<script>
    
    function setFotoPerfil()
    {
        document.getElementById('input-file-foto').click();
    }

    var loadFile = function(event) {
    var reader = new FileReader();
    reader.onload = function(){
      var output = document.getElementById('fotoPreview');
      output.src = reader.result;
      output.classList.remove('p-3');
    };
    reader.readAsDataURL(event.target.files[0]);
    };

</script>
@endpush