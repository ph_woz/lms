<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TurmaAluno extends Model
{
    use HasFactory;

    protected $table = 'turmas_alunos';

    public $timestamps = true;

    public $fillable = [
        'plataforma_id',
        'turma_id',
        'curso_id',
        'aluno_id',
        'data_primeiro_acesso',
        'data_ultimo_acesso',
        'data_conclusao_curso',
        'data_colacao_grau',
        'data_conclusao_conteudo',
        'data_expiracao',
        'status',
        'codigo_certificado',
    ];

    public function scopePlataforma($query)
    {
        return $query->where('plataforma_id', session('plataforma_id') ?? Plataforma::dominio()->pluck('id')[0] ?? null);
    }

    public function scopeAtivo($query)
    {
        return $query->where('status', 0);
    }

    public static function geraCodigoCertificado($turmaAlunoId)
    {
        $codigo = session('plataforma_id') . '-' . $turmaAlunoId . '-' . substr(str_shuffle("ABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 1);

        return $codigo;
    }

}
