@extends('layouts.admin.master')

@section('content')
    
    @livewire('admin.curso.info', ['curso_id' => $curso_id])

@endsection