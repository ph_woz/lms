<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Http\Request;
use App\Models\Plataforma;
use App\Models\Util;
use App\Models\User;
use App\Models\TurmaAluno;
use App\Models\Curso;
use App\Models\Turma;

class EmailContratacaoTurma extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $turmaAluno;

    public function __construct(User $user, TurmaAluno $turmaAluno)
    {
        $this->user = $user;
        $this->turmaAluno = $turmaAluno;
    }

    public function generateToken()
    {
        return md5(rand(1, 10) . microtime());
    }

    public function replaceVariaveis($descricao)
    {
        $plataforma = Plataforma::plataforma()->first();

        $curso = Curso::plataforma()->where('id', $this->turmaAluno->curso_id)->pluck('nome')[0] ?? null;
        $turma = Turma::plataforma()->where('id', $this->turmaAluno->turma_id)->pluck('nome')[0] ?? null;

        $descricao = str_replace('[NOME]',      $this->user->nome,      $descricao);
        $descricao = str_replace('[EMAIL]',     $this->user->email,     $descricao);
        $descricao = str_replace('[CPF]',       $this->user->cpf,       $descricao);
        $descricao = str_replace('[CURSO]',     $curso,                 $descricao);
        $descricao = str_replace('[TURMA]',     $turma,                 $descricao);

        $descricao = str_replace('[PLATAFORMA_NOME]', $plataforma->nome, $descricao);
        $descricao = str_replace('[PLATAFORMA_LINK]', \Request::root().'/login', $descricao);

        return $descricao;
    }

    public function build()
    {
        $plataforma = Plataforma::plataforma()->first();

        $descricao  = $this->replaceVariaveis(request('email_contratacao_turma') ?? $plataforma->email_contratacao_turma);

        $statusBtnRedefinirSenha = false;
        if(strpos($descricao, '[BOTAO_REDEFINIR_SENHA]') !== false)
        {
            $descricao = str_replace('[BOTAO_REDEFINIR_SENHA]', '', $descricao);
            $token = $this->generateToken();
            $this->user->update(['token' => $token]);
            $statusBtnRedefinirSenha = true;
        }

        $assunto = 'Matriculado! | ' . $plataforma->nome;

        return $this->view('mail.contratacao')
                ->subject($assunto)
                ->from($plataforma->email, $plataforma->nome)
                ->replyTo($plataforma->email, $plataforma->nome)
                ->with([
                    'descricao' => $descricao,
                    'token'     => $this->user->token, 
                    'email'     => $this->user->email, 
                    'id'        => $this->user->id,
                    'botaoRedefinirSenha' => $statusBtnRedefinirSenha,
                ]);
    }
}
