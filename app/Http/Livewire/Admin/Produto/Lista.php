<?php

namespace App\Http\Livewire\Admin\Produto;

use Livewire\WithPagination;
use Livewire\Component;
use App\Models\Produto;

class Lista extends Component
{
    use WithPagination;

    protected $paginationTheme = 'bootstrap';

    public $search;
    public $status = 0;
    public $ordem;

    protected $queryString = ['search','status','ordem'];

    public function render()
    {
        $produtos = Produto::plataforma();

        if($this->search)
        {
            $produtos = $produtos->where('nome', 'like', '%'.$this->search.'%');
        }

        $produtos = $produtos->where('status', $this->status);

        $countProdutos = $produtos->count();


        if($this->ordem == 'desc' || $this->ordem == 'asc')
        {
            $produtos = $produtos->orderBy('id', $this->ordem);
        
        } else {

            $produtos = $produtos->orderBy('nome');
        }
        

        $produtos = $produtos->paginate(10);


        return view('livewire.admin.produto.lista', [
            'produtos'      => $produtos,
            'countProdutos' => $countProdutos,
        ]);
    }
}
