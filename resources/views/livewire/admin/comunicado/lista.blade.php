<div>

    <form>

        <div class="d-lg-flex mb-3">

            <input wire:model="search" type="search" class="form-control p-form" placeholder="Buscar">

            <select wire:model="arquivar" class="mx-lg-3 my-3 my-lg-0 select-form w-lg-40">
                <option value="N">Não Arquivados</option>
                <option value="S">Arquivados</option>
            </select>

            <select wire:model="ordem" class="select-form w-lg-40">
                <option value="assunto">Alfabética</option>
                <option value="desc">Mais novos</option>
                <option value="asc">Mais antigos</option>
            </select>

        </div>

    </form>

    <div class="mt-5 d-flex justify-content-between">

        <div class="align-self-end mb-2">
            <span class="resultados-encontrados">
                {{ count($comunicados) }} resultados de {{ $countComunicados }}.
            </span>
        </div>

        <div class="mb-3">
            <a href="{{ route('admin.comunicado.add') }}" class="btn-add aclNivel2">
                + Adicionar
            </a>
        </div>

    </div>

    <div class="row">
        <div class="col-12">

            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col">Assunto</th>
                                    <th scope="col">
                                        <span class="float-end">
                                            Ações
                                        </span>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($comunicados as $comunicado)
                                    <tr>
                                        <td>{{ $comunicado->assunto }}</td>
                                        <td>
                                            <span class="float-end">

                                                <a href="{{ route('admin.comunicado.info', ['id' => $comunicado->id]) }}" class="btn bg-base text-white">
                                                    <i class="fa fa-eye"></i>
                                                </a>

                                            </span>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            {{ $comunicados->links() }}

        </div>
    </div>

</div>