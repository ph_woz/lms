<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCursosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cursos', function (Blueprint $table) {
            $table->id();

            $table->integer('plataforma_id');
            $table->integer('cadastrante_id')->nullable();
            $table->integer('certificado_id')->nullable();
            $table->integer('coordenador_id')->nullable();

            $table->string('nome');
            $table->string('referencia');
            $table->string('slug')->nullable();

            $table->string('slogan')->nullable();
            $table->string('carga_horaria')->nullable();
            $table->string('duracao')->nullable();
            $table->string('tipo_formacao')->nullable();
            $table->string('nivel')->nullable();

            $table->longText('descricao')->nullable();
            $table->text('foto_capa')->nullable();
            $table->text('foto_miniatura')->nullable();
            $table->text('matriz_curricular')->nullable();
            $table->text('video')->nullable();
            $table->string('exibir_cc', 1)->nullable()->default('N');

            $table->integer('status')->default(0);
            $table->string('publicar', 1)->default('N');

            $table->text('seo_title')->nullable();
            $table->text('seo_keywords')->nullable();
            $table->text('seo_description')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cursos');
    }
}
