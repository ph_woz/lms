<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersContratosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_contratos', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('plataforma_id');
            $table->unsignedBigInteger('cadastrante_id');
            $table->unsignedBigInteger('contrato_id');
            $table->unsignedBigInteger('user_id');
            $table->string('referencia');
            $table->text('pdf');
            $table->text('pdf_assinado')->nullable();
            $table->dateTime('data_assinatura')->nullable();
            $table->longText('foto_assinatura')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_contratos');
    }
}
