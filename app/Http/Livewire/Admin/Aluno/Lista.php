<?php

namespace App\Http\Livewire\Admin\Aluno;

use Livewire\WithPagination;
use Livewire\Component;
use App\Models\User;
use App\Models\Categoria;
use App\Models\Unidade;
use App\Models\Comunicado;
use App\Models\ComunicadoAluno;

class Lista extends Component
{
    use WithPagination;

    protected $paginationTheme = 'bootstrap';

    public $search;
    public $status = 0;
    public $ano;
    public $mes;
    public $trilha_tipo;
    public $data_field;
    public $ordem;
    public $unidade_id;
    public $categoriasSelected;

    public $add;
    public $add_alunos_para_comunicado_id;
    public $add_alunos_para_trilha_id;
    public $add_alunos_para_turma_id;
    public $add_alunos_para_curso_id;
    public $add_aluno_para_fluxo_caixa;

    public $alunosIds = [];

    protected $queryString = ['search','ano','mes','status','ordem','data_field','trilha_tipo','unidade_id','add','add_aluno_para_fluxo_caixa','add_alunos_para_comunicado_id','add_alunos_para_trilha_id','add_alunos_para_turma_id','add_alunos_para_curso_id','categoriasSelected'];


    public function mount()
    {
        $this->add_alunos_para_comunicado_id = $_GET['add_alunos_para_comunicado_id'] ?? null;
        $this->add_alunos_para_trilha_id     = $_GET['add_alunos_para_trilha_id'] ?? null;
        $this->add_alunos_para_turma_id      = $_GET['add_alunos_para_turma_id'] ?? null;
        $this->add_alunos_para_curso_id      = $_GET['add_alunos_para_curso_id'] ?? null;
        $this->trilha_tipo                   = $_GET['trilha_tipo'] ?? null;
    }

    public function render()
    {
        $alunos = User::select('users.id','users.nome','users.email');


        if($this->trilha_tipo == 'concluiram')
        {
            $alunos = $alunos
                ->join('trilhas_alunos','users.id','trilhas_alunos.aluno_id')
                ->whereNotNull('trilhas_alunos.data_conclusao')
                ->whereYear('trilhas_alunos.data_conclusao', $this->ano)
                ->whereMonth('trilhas_alunos.data_conclusao', $this->mes);
        }

        $alunos = $alunos
            ->where('users.plataforma_id', session('plataforma_id') ?? Auth()->user()->plataforma_id)
            ->where('users.tipo','aluno')
            ->where('users.status', $this->status);
            

        if($this->data_field != null)
        {
            $alunos = $alunos
                ->whereYear($this->data_field, $this->ano)
                ->whereMonth($this->data_field, $this->mes); 
        }

        if($this->categoriasSelected != null)
        {
            $alunos = $alunos->join('users_categorias','users.id','users_categorias.user_id')
                ->whereIn('users_categorias.categoria_id', $this->categoriasSelected ?? array());
        }

        if($this->add_alunos_para_comunicado_id)
        {
            $alunosIdsJaAdicionados = ComunicadoAluno::plataforma()->where('comunicado_id', $this->add_alunos_para_comunicado_id)->select('aluno_id')->get()->pluck('aluno_id')->toArray();
            $alunos = $alunos->whereNotIn('users.id', $alunosIdsJaAdicionados);
        }

        if($this->add_alunos_para_trilha_id)
        {
            $alunos = $alunos
                ->leftJoin('trilhas_alunos', function ($join) {
                    $join->on('users.id', '=', 'trilhas_alunos.aluno_id')
                        ->where('trilhas_alunos.trilha_id', '=', $this->add_alunos_para_trilha_id);
                })
                ->whereNull('trilhas_alunos.aluno_id');
        }

        if($this->add_alunos_para_turma_id)
        {
            $alunos = $alunos
                ->leftJoin('turmas_alunos', function ($join) {
                    $join->on('users.id', '=', 'turmas_alunos.aluno_id')
                        ->where('turmas_alunos.turma_id', '=', $this->add_alunos_para_turma_id);
                })
                ->whereNull('turmas_alunos.aluno_id');
        }

        if($this->search)
        {
            $alunos->where(function ($query)
            {
                $query
                    ->orWhere('users.nome', 'like', '%'.$this->search.'%')
                    ->orWhere('users.email', 'like', '%'.$this->search.'%')
                    ->orWhere('users.cpf', 'like', '%'.$this->search.'%');
            });
        }


        if(\Auth::user()->restringir_unidade == 'S' && \Auth::user()->unidade_id != null)
        {
            $alunos = $alunos->where('users.unidade_id', \Auth::user()->unidade_id);
        }

        if($this->unidade_id)
        {
            $alunos = $alunos->where('users.unidade_id', $this->unidade_id);
        }

        $countAlunos = $alunos->count();


        if($this->ordem == 'desc' || $this->ordem == 'asc')
        {
            $alunos = $alunos->orderBy('id', $this->ordem);

        } else {

            if($this->ordem == 'data_ultimo_acesso')
            {
                $alunos = $alunos->orderBy('users.data_ultimo_acesso', 'desc');

            } else {

                $alunos = $alunos->orderBy('users.nome');
            }
        }

        $alunos = $alunos->orderBy('users.nome')->paginate(10);

        $unidades   = Unidade::select('id','nome')->plataforma()->unidade()->ativo()->orderBy('nome')->get();
        $categorias = Categoria::select('id','nome')->plataforma()->ativo()->tipo('aluno')->orderBy('nome')->get();

        if($this->add_alunos_para_comunicado_id)
        {
            $this->alunosIds = [];
            foreach($alunos as $aluno)
            {
                $this->alunosIds[] = $aluno->id;
            }
            
            $comunicado = Comunicado::select('id','assunto')->plataforma()->naoArquivado()->where('id', $this->add_alunos_para_comunicado_id)->first();
        }

        if($this->add_alunos_para_trilha_id)
        {
            $this->alunosIds = [];
            foreach($alunos as $aluno)
            {
                $this->alunosIds[] = $aluno->id;
            }
            
            $trilha_nome = \App\Models\Trilha::select('referencia')->plataforma()->where('id', $this->add_alunos_para_trilha_id)->value('referencia');
        }

        if($this->add_alunos_para_turma_id)
        {
            $this->alunosIds = [];
            foreach($alunos as $aluno)
            {
                $this->alunosIds[] = $aluno->id;
            }
            
            $turma_nome = \App\Models\Turma::select('nome')->plataforma()->where('id', $this->add_alunos_para_turma_id)->value('nome');
        }

        return view('livewire.admin.aluno.lista', [
            'alunos'      => $alunos,
            'countAlunos' => $countAlunos,
            'unidades'    => $unidades,
            'categorias'  => $categorias,
            'comunicado'  => $comunicado ?? null,
            'trilha_nome' => $trilha_nome ?? null,
            'turma_nome'  => $turma_nome ?? null,
        ]);
    }

    public function addTodosOsAlunosAoComunicado()
    {
        foreach($this->alunosIds as $aluno_id)
        {
            ComunicadoAluno::create([
                'comunicado_id' => $this->add_alunos_para_comunicado_id,
                'aluno_id'      => $aluno_id,
            ]);
        }

        session()->flash('success-livewire', 'Todos os Alunos foram adicionadas ao Comunicado com êxito.');
        
        $this->dispatchBrowserEvent('toast');
        $this->dispatchBrowserEvent('dissmiss-modal');
    }

    public function addAlunoParaComunicado($aluno_id)
    {
        ComunicadoAluno::create(['comunicado_id' => $this->add_alunos_para_comunicado_id, 'aluno_id' => $aluno_id]);
        
        session()->flash('success-livewire', 'Aluno adicionado ao Comunicado com êxito.');

        $this->dispatchBrowserEvent('toast');
    }
    
    public function addAlunoParaTrilha($aluno_id)
    {
        $this->matricularAlunoNaTrilha($aluno_id);

        session()->flash('success-livewire', 'Aluno adicionado a Trilha com êxito.');

        $this->dispatchBrowserEvent('toast');
    }

    public function addAlunoParaTurma($aluno_id)
    {
        $this->matricularAlunoNaTurma($aluno_id, $this->add_alunos_para_turma_id, $this->add_alunos_para_curso_id);

        session()->flash('success-livewire', 'Aluno adicionado a Turma com êxito.');

        $this->dispatchBrowserEvent('toast');
    }

    public function addTodosOsAlunosATurma()
    {
        foreach($this->alunosIds as $aluno_id)
        {
            $this->matricularAlunoNaTurma($aluno_id, $this->add_alunos_para_turma_id, $this->add_alunos_para_curso_id);
        }

        session()->flash('success-livewire', 'Todos os Alunos foram adicionadas a Turma com êxito.');
        
        $this->dispatchBrowserEvent('toast');
        $this->dispatchBrowserEvent('dissmiss-modal');
    }

    public function addTodosOsAlunosATrilha()
    {
        foreach($this->alunosIds as $aluno_id)
        {
            $this->matricularAlunoNaTrilha($aluno_id);
        }

        session()->flash('success-livewire', 'Todos os Alunos foram adicionadas a Trilha com êxito.');
        
        $this->dispatchBrowserEvent('toast');
        $this->dispatchBrowserEvent('dissmiss-modal');
    }

    public function matricularAlunoNaTrilha($aluno_id)
    {
        \App\Models\TrilhaAluno::create([
            'plataforma_id' => session('plataforma_id') ?? \Auth::user()->plataforma_id,
            'trilha_id' => $this->add_alunos_para_trilha_id,
            'aluno_id'  => $aluno_id,
        ]);

        $turmas  = \App\Models\TrilhaTurma::select('turmas.id','turmas.curso_id')
            ->join('turmas','trilhas_turmas.turma_id','turmas.id')
            ->where('trilhas_turmas.plataforma_id', session('plataforma_id') ?? \Auth::user()->plataforma_id)
            ->where('trilhas_turmas.trilha_id', $this->add_alunos_para_trilha_id)
            ->get();
        

        foreach($turmas as $turma)
        {
            $checkJaExists = \App\Models\TurmaAluno::plataforma()->where('aluno_id', $aluno_id)->where('turma_id', $turma->id)->exists();

            if($checkJaExists == false)
            {
                $this->matricularAlunoNaTurma($aluno_id, $turma->id, $turma->curso_id);
            }
        }
    }

    public function matricularAlunoNaTurma($aluno_id, $turma_id, $curso_id)
    {
        \App\Models\TurmaAluno::create([
            'plataforma_id' => session('plataforma_id') ?? \Auth::user()->plataforma_id,
            'turma_id' => $turma_id,
            'curso_id' => $curso_id,
            'aluno_id' => $aluno_id,
        ]);
    }

}
