<div>

    <nav aria-label="breadcrumb" class="mb-4-5 p-3 bg-db rounded ashadow border">
        <ol class="breadcrumb p-0 m-0">
            <li class="breadcrumb-item"><a href="{{ route('admin.pagina.lista') }}" class="color-blue-info-link">Páginas</a></li>
            <li class="breadcrumb-item text-dark" aria-current="page">Blog</li>
        </ol>
    </nav>

    <form>

        <div class="d-lg-flex mb-3">

            <input wire:model="search" type="search" class="me-lg-3 mb-3 mb-lg-0 form-control p-form" placeholder="Buscar">

            <div wire:ignore class="w-100">
                <select wire:model="categoria_id" class="selectpicker" data-width="100%" data-live-search="true">
                    <option value="">Categoria</option>
                    @foreach($categorias as $categoria)
                        <option value="{{ $categoria->id }}">{{ $categoria->nome }}</option>
                    @endforeach
                </select>
            </div>

            <select wire:model="publicar" class="mx-lg-3 my-3 my-lg-0 select-form w-lg-40">
                <option value="S">Publicados</option>
                <option value="N">Não publicados</option>
            </select>

            <select wire:model="ordem" class="select-form w-lg-40">
                <option value="titulo">Alfabética</option>
                <option value="desc">Mais novos</option>
                <option value="asc">Mais antigos</option>
            </select>

        </div>

    </form>

    <div class="mt-5 d-flex justify-content-between">

        <div class="align-self-end mb-2">
            <span class="resultados-encontrados">
                {{ count($posts) }} resultados de {{ $countPosts }}.
            </span>
        </div>

        <div class="mb-3">
            <a href="#ModalPostAdd" data-bs-toggle="modal" class="btn-add aclNivel2">
                + Adicionar
            </a>
        </div>

    </div>

    <div class="row">
        <div class="col-12">

            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col">Título</th>
                                    <th scope="col">
                                        <span class="float-end">
                                            Ações
                                        </span>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($posts as $post)
                                    <tr>
                                        <td>
                                            <a href="/blog/post/{{$post->slug}}/{{$post->id}}" target="_blank" class="color-blue-info-link"> 
                                                {{ $post->titulo }}
                                            </a>
                                        </td>
                                        <td>
                                            <span class="float-end">

                                                <button data-bs-target="#ModalInfo" data-bs-toggle="modal" wire:click="modalMetaDados('{{$post->id}}')" class="btn bg-base text-white">
                                                    <i class="fa fa-eye"></i>
                                                </button>

                                            </span>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            {{ $posts->links() }}

        </div>
    </div>

    <div wire:ignore.self class="modal fade" id="ModalInfo" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title" id="exampleModalLabel">Post</h3>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>

                @if(isset($postMetaDado))
                <div class="modal-body">

                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">
                            <span class="mb-0 weight-600 color-444">Título:</span>
                            {{ $postMetaDado->titulo }}
                        </li>

                        <li class="list-group-item">
                            <span class="mb-0 weight-600 color-444">Cadastrante:</span>
                            @if($postMetaDado->cadastrante)
                                <a href="/admin/colaborador/info/{{$postMetaDado->cadastrante_id}}" class="color-blue-info-link">
                                    {{ $postMetaDado->cadastrante }}
                                </a>
                            @else
                                Não definido
                            @endif
                        </li>
                        <li class="list-group-item">
                            <span class="mb-0 weight-600 color-444">Autor:</span>
                            @if($postMetaDado->autor)
                                <a href="/admin/colaborador/info/{{$postMetaDado->autor_id}}" class="color-blue-info-link">
                                    {{ $postMetaDado->autor }}
                                </a>
                            @else
                                Não definido
                            @endif
                        </li>

                        <li class="list-group-item">
                            <span class="mb-0 weight-600 color-444">Status:</span>
                            @if($postMetaDado->publicar == 'S')
                                <span class="text-success weight-700">Publicado</span>
                            @else
                                <span class="text-danger weight-700">Não publicado</span>
                            @endif
                        </li>
                        <li class="list-group-item">
                            <span class="mb-0 weight-600 color-444">Data de Cadastro:</span>
                            @if($postMetaDado->created_at)
                                <span class="mb-0">{{ $postMetaDado->created_at->format('d/m/Y H:i') ?? null }} ({{$postMetaDado->created_at->diffForHumans()}}) </span>
                            @else
                                Não definido
                            @endif
                        </li>   
                    </ul>

                </div>

                <div class="modal-footer">
                    <button type="button" data-bs-dismiss="modal" onclick="document.getElementById('deletar_post_titulo').innerHTML = '{{ $postMetaDado->titulo }}';" data-bs-toggle="modal" data-bs-target="#ModalDeletarPost" class="aclNivel3 btn btn-danger weight-600 py-2">Deletar</button>
                    <a href="?editar_post={{$postMetaDado->id}}" class="aclNivel2 btn btn-warning weight-600 py-2 text-dark">Editar</a>
                </div>
                @endif

            </div>
        </div>
    </div>

    <form wire:submit.prevent="delete" method="POST">
        <div wire:ignore class="modal fade" id="ModalDeletarPost" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3 class="modal-title" id="exampleModalLabel">Deletar Post</h3>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body text-center py-5">

                        <p id="deletar_post_titulo" class="m-0 fs-15 weight-600">

                        </p>

                        <hr/>

                        <p class="m-0 fs-15">
                            Você tem certeza que deseja deletar este Post?
                        </p>

                    </div>
                    <div class="modal-footer">
                        <div class="d-flex w-100">
                            <input type="password" wire:model="password_to_delete" class="w-100 form-control p-form me-3" placeholder="Digite sua senha" required>
                            <button type="submit" class="btn btn-danger weight-600">Confirmar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <form method="POST" action="{{ route('admin.pagina.editar.blog.post.add') }}" enctype="multipart/form-data">
    @csrf
        <div class="modal fade" id="ModalPostAdd" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3 class="modal-title" id="exampleModalLabel">Adicionar Post</h3>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body px-lg-4">

                        <div class="mb-3 d-lg-flex">
                            <div class="w-100 me-lg-3 mb-3 mb-lg-0">
                                <label>Título</label>
                                <input type="text" name="titulo" class="form-control p-form" maxlength="100" required>
                            </div>
                            <div class="w-lg-75 me-lg-3 mb-3 mb-lg-0">
                                <label>Autor</label>
                                <select name="autor_id" class="me-lg-3 mb-3 mb-lg-0 selectpicker" data-width="100%" data-live-search="true">
                                    <option value="">Selecione</option>
                                    @foreach($autores as $autor)
                                        <option value="{{ $autor->id }}">{{ $autor->nome }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="w-lg-75">
                                <label>Foto de Capa</label>
                                <input type="file" name="foto" class="file-form">
                            </div>
                        </div>

                        <div class="mb-3 d-lg-flex">

                            <div class="w-100 me-lg-3 mb-3 mb-lg-0">
                                <label>Publicar</label>
                                <div class="d-flex">
                                    <select name="publicar_e_destaque" id="add_publicar_e_destaque" onchange="addPublicarDestaque(this.value)" class="w-100 select-form rounded-right-0">
                                        <option value="S-S">Sim, e enviar para Destaque</option>
                                        <option value="S-N">Sim, mas não enviar para Destaque</option>
                                        <option value="N-N">Não</option>
                                    </select>
                                    <input type="text" list="ordens-options" id="add_destaque_ordem" name="destaque_ordem" class="w-lg-50 form-control p-form rounded-left-0" placeholder="N° de Ordem">
                                    <datalist id="ordens-options">
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                    </datalist>
                                </div>
                            </div>
                            <div class="w-100">
                                <label>Categorias</label>
                                <select name="categorias[]" class="selectpicker" multiple data-width="100%" data-live-search="true" title="Selecione">
                                    @foreach($categorias as $categoria)
                                        <option value="{{ $categoria->id }}">{{ $categoria->nome }}</option>
                                    @endforeach
                                </select>
                            </div>

                        </div>

                        <div class="w-100">
                            <label>Descrição</label>
                            <textarea name="descricao" class="descricao textarea-form"></textarea>
                        </div>

                        <section class="mt-4-5" id="boxCursoSEO">

                            <p class="mb-0">
                                Informações de SEO para resultados de buscas em Navegadores
                            </p>
                            <hr class="mt-2" />
                            
                            <div class="collapse show" id="boxSeo">

                                <div class="d-lg-flex mb-3">

                                    <div class="w-100 me-lg-3 mb-3 mb-lg-0">
                                        <label>Título</label>
                                        <input type="text" name="seo_title" class="form-control p-form" placeholder="Title">
                                    </div>

                                    <div class="w-100">
                                        <label>Palavras-chaves</label>
                                        <input type="text" name="seo_keywords" class="form-control p-form" placeholder="Keywords">
                                    </div>

                                </div>

                                <div class="w-100 mb-3">
                                    <label>Descrição</label>
                                    <input type="text" name="seo_description" class="form-control p-form" placeholder="Description">
                                </div>
                                
                            </div>

                        </section>

                    </div>

                    <div class="modal-footer">
                        <button type="submit" name="add" value="S" class="btn btn-primary weight-600 py-2">Confirmar</button>
                    </div>
                </div>
            </div>
        </div>
    </form>

    @if(isset($editar_post))
    <form method="POST" action="{{ route('admin.pagina.editar.blog.post.editar') }}" enctype="multipart/form-data">
    @csrf
        <input type="hidden" name="editar_post_id" value="{{ $editar_post->id }}">
        <div class="modal fade" id="ModalPostEditar" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3 class="modal-title" id="exampleModalLabel">Editar Post</h3>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body px-lg-4">

                        <div class="mb-3 d-lg-flex">
                            <div class="w-100 me-lg-3 mb-3 mb-lg-0">
                                <label>Título</label>
                                <input type="text" name="titulo" class="form-control p-form" maxlength="100" required value="{{ $editar_post->titulo }}">
                            </div>
                            <div class="w-lg-75 me-lg-3 mb-3 mb-lg-0">
                                <label>Autor</label>
                                <select name="autor_id" class="me-lg-3 mb-3 mb-lg-0 selectpicker" data-width="100%" data-live-search="true">
                                    <option value="">Selecione</option>
                                    @foreach($autores as $autor)
                                        <option value="{{ $autor->id }}" @if($autor->id == $editar_post->id) selected @endif>{{ $autor->nome }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="w-lg-75">
                                <label>Foto de Capa</label>
                                @if($editar_post->foto_capa)
                                    <a href="{{ $editar_post->foto_capa }}" class="weight-600 fs-11" target="_blank">
                                        visualizar anexo
                                    </a>
                                @endif
                                <input type="file" name="editar_foto" class="file-form">
                            </div>
                        </div>

                        <div class="mb-3 d-lg-flex">

                            <div class="w-100 me-lg-3 mb-3 mb-lg-0">
                                <label>Publicar</label>
                                <div class="d-flex">
                                    <select name="publicar_e_destaque" id="add_publicar_e_destaque" onchange="addPublicarDestaque(this.value)" class="w-100 select-form rounded-right-0">
                                        <option value="S-S" @if($editar_post->publicar == 'S' && $editar_post->destaque_ordem != null) selected @endif>Sim, e enviar para Destaque</option>
                                        <option value="S-N" @if($editar_post->publicar == 'S' && $editar_post->destaque_ordem == null) selected @endif>Sim, mas não enviar para Destaque</option>
                                        <option value="N-N" @if($editar_post->publicar == 'N') selected @endif>Não</option>
                                    </select>
                                    <input type="text" list="ordens-options" id="add_destaque_ordem" name="destaque_ordem" class="w-lg-50 form-control p-form rounded-left-0" placeholder="N° de Ordem" value="{{ $editar_post->destaque_ordem }}">
                                    <datalist id="ordens-options">
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                    </datalist>
                                </div>
                            </div>
                            <div class="w-100">
                                <label>Categorias</label>
                                <select name="categorias[]" class="selectpicker" multiple data-width="100%" data-live-search="true" title="Selecione">
                                    @foreach($categorias as $categoria)
                                        <option value="{{ $categoria->id }}" @if(in_array($categoria->id, $categoriasSelected)) selected @endif>{{ $categoria->nome }}</option>
                                    @endforeach
                                </select>
                            </div>

                        </div>

                        <div class="w-100">
                            <label>Descrição</label>
                            <textarea name="descricao" class="descricao textarea-form">{{ $editar_post->descricao }}</textarea>
                        </div>

                        <section class="mt-4-5" id="boxCursoSEO">

                            <p class="mb-0">
                                Informações de SEO para resultados de buscas em Navegadores
                            </p>
                            <hr class="mt-2" />
                            
                            <div class="collapse show" id="boxSeo">

                                <div class="d-lg-flex mb-3">

                                    <div class="w-100 me-lg-3 mb-3 mb-lg-0">
                                        <label>Título</label>
                                        <input type="text" name="seo_title" class="form-control p-form" placeholder="Title" value="{{ $editar_post->seo_title }}">
                                    </div>

                                    <div class="w-100">
                                        <label>Palavras-chaves</label>
                                        <input type="text" name="seo_keywords" class="form-control p-form" placeholder="Keywords" value="{{ $editar_post->seo_keywords }}">
                                    </div>

                                </div>

                                <div class="w-100 mb-3">
                                    <label>Descrição</label>
                                    <input type="text" name="seo_description" class="form-control p-form" placeholder="Description" value="{{ $editar_post->seo_description }}">
                                </div>
                                
                            </div>

                        </section>

                    </div>

                    <div class="modal-footer">
                        <button type="submit" name="editar" value="S" class="btn btn-primary weight-600 py-2">Confirmar</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
    @endif

    @include('livewire.util.toast')

</div>

<script>

    function addPublicarDestaque(value)
    {
        if(value != 'S-S')
        {
            document.getElementById('add_destaque_ordem').value = '';
            document.getElementById('add_destaque_ordem').style.display = 'none';
            document.getElementById('add_publicar_e_destaque').classList.remove('rounded-right-0');
        
        } else {

            document.getElementById('add_destaque_ordem').style.display = 'block';
            document.getElementById('add_publicar_e_destaque').classList.add('rounded-right-0');
        }
    }

</script>