<?php

namespace App\Http\Livewire\Admin\Plataforma;

use Livewire\WithPagination;
use Livewire\Component;
use App\Models\Log;
use App\Models\User;

class Logs extends Component
{
    use WithPagination;

    protected $paginationTheme = 'bootstrap';

    public $search;
    public $user_id;
    public $tipo;

    protected $queryString = ['search','user_id','tipo'];

    public function render()
    {
        $log = Log::leftJoin('users','log.user_id','users.id');

        $log = $log->where('log.plataforma_id', session('plataforma_id'));

        if($this->search)
            $log = $log->where('log.ref', 'like', '%'.$this->search.'%');

        if($this->user_id)
            $log = $log->where('log.user_id', $this->user_id);

        if($this->tipo)
            $log = $log->where('log.tipo', $this->tipo);

        $countLog = $log->count();

        $log = $log->select('users.id as userId', 'users.nome as userNome', 'log.*')->orderBy('id','desc')->paginate(10);

        $users = User::plataforma()->ativo()->colaborador()->orderBy('nome')->get();

        $tipos = Log::plataforma()->whereNotNull('tipo')->distinct('tipo')->select('tipo')->orderBy('tipo')->get()->pluck('tipo')->toArray();

        return view('livewire.admin.plataforma.logs', [
            'log'      => $log,
            'countLog' => $countLog,
            'users'    => $users,
            'tipos'    => $tipos,
        ]);
    }
}
