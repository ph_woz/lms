<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Contato extends Model
{
    use HasFactory;
    
    public $timestamps = true;

    protected $table = 'contatos';

    public $fillable = [
    	'plataforma_id',
        'formulario_id',
        'curso_id',
        'turma_id',
        'trilha_id',
        'unidade_id',
        'tipo', // (L)ead ou (S)uporte
        'assunto',
        'mensagem',
        'nome',
        'email',
        'password',
        'cpf',
        'rg',
        'naturalidade',
        'valor_contratado',
        'celular',
        'telefone',
        'genero',
        'data_nascimento',

        'cep',
        'rua',
        'numero',
        'bairro',
        'cidade',
        'estado',
        'complemento',
        'ponto_referencia',

        'anotacao',
        'status',
        'arquivado',
    ];

    public function scopePlataforma($query)
    {
        return $query->where('plataforma_id', session('plataforma_id') ?? Plataforma::dominio()->pluck('id')[0] ?? null);
    }

    public function scopeSuporte($query)
    {
        return $query->where('tipo','S');
    }
    public function scopeLead($query)
    {
        return $query->where('tipo','L');
    }

    public function scopeNaoArquivados($query)
    {
        return $query->where('arquivado', 'N');
    }

    public function scopePendente($query)
    {
        return $query->where('status', 0);
    }

    public function getStatusNome($status)
    {
        switch ($status)
        {
            case 0:
                return 'Pendente';

            case 1:
                return 'Em análise';

            case 2:
                return 'Atendido';

            case 3:
                return 'Desativado';
        }
    }

    public function getStatusColor($status)
    {
        switch ($status)
        {
            case 0:
                return 'text-danger';

            case 1:
                return 'text-primary';

            case 2:
                return 'text-success';

            case 3:
                return 'text-secondary';
        }
    }
}
