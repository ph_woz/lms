<div>

    <div class="col-12">
        @if (session()->has('success-livewire'))
            <div class="alert alert-success alert-dismissible" role="alert">
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                <div class="alert-message">
                	Responsável cadastrado:
                    <span>
	                    <a href="{{ route('admin.responsavel.info', ['id' => $this->responsavel->id ?? null]) }}" class="weight-600">
	                    	{{ $responsavel->nome }} 
	                	</a>
	                </span>
                </div>
            </div>
        @endif
    </div>

    <form wire:submit.prevent="add" method="POST" class="mb-4">

		<section class="mb-4-5">
			
			<a class="hover-d-none">
				<div class="section-title">
					Responsável pelo Aluno(a)
				</div>
			</a>
			<hr/>
			
			<div class="d-lg-flex align-items-center">

				<a href="{{ route('admin.aluno.info', ['id' => $this->aluno->id]) }}" class="w-100 hover-d-none color-blue-info-dark fs-16 weight-600">
					<div class="w-100 border py-2 px-3 bg-white">
						{{ $aluno->nome }}
					</div>
				</a>

				<select wire:model="grau_parentesco" class="ms-4 select-form" required>
					<option value="">Selecione o Grau Parentesco</option>
					<option value="Pai">Pai</option>
					<option value="Mãe">Mãe</option>
					<option value="Irmão">Irmão</option>
					<option value="Irmã">Irmã</option>
					<option value="Tia">Tia</option>
					<option value="Tio">Tio</option>
					<option value="Avô">Avô</option>
					<option value="Avó">Avó</option>
					<option value="Primo">Primo</option>
					<option value="Prima">Prima</option>
					<option value="Outro">Outro</option>
				</select>

			</div>

		</section>

		<section class="mb-4-5">
			
			<a href="#boxInfoPrincipal" data-bs-toggle="collapse" aria-expanded="false" class="hover-d-none">
				<div class="section-title">
					Informações Principais
				</div>
			</a>
			<hr/>
			
			<div class="collapse show" id="boxInfoPrincipal">

		    	<div class="d-lg-flex mb-3">

			    	<div class="w-100">
			    		<label>Nome</label>
			    		<input type="text" wire:model="nome" class="form-control p-form" placeholder="Nome" required>
			    	</div>
			    	<div class="w-100 mx-lg-3 my-3 my-lg-0">
			    		<label>Email</label>
			    		<input type="text" wire:model="email" class="form-control p-form" placeholder="Email">
			    		@error('email')
			    			<span class="text-danger weight-600">
			    				{{ $message }}
			    			</span>
			    		@enderror
			    	</div>

					<div class="w-lg-55">
						<label>CPF</label>
						<input type="text" wire:model="cpf" class="cpf form-control p-form" placeholder="000.000.000-00" minlength="14" maxlength="14">
					</div>

			    	<div class="w-lg-40 ms-lg-3 mt-3 mt-lg-0">
			    		<label>Status</label>
			    		<select wire:model="status" class="select-form">
			    			<option value="0">Ativo</option>
			    			<option value="1">Desativado</option>
			    		</select>
			    	</div>

			    </div>

			    <div class="d-lg-flex mb-3">

					<div class="w-100">
						<label>Data de Nascimento</label>
						<input type="date" wire:model="data_nascimento" class="form-control p-form">
					</div>
					<div class="w-100 ms-lg-3 mt-3 mt-lg-0">
						<label>Celular</label>
						<input type="text" wire:model="celular" class="phone form-control p-form" placeholder="(00) 00000-0000" minlength="15">
					</div>
					<div class="w-100 ms-lg-3 mt-3 mt-lg-0">
						<label>Telefone</label>
						<input type="text" wire:model="telefone" class="phone form-control p-form" placeholder="(00) 0000-0000">
					</div>

			    </div>

			    <div class="d-lg-flex mb-3">

					<div class="w-100">
						<label>RG</label>
						<input type="text" wire:model="rg" class="form-control p-form" placeholder="Registro Geral">
					</div>

					<div class="w-100 ms-lg-3 mt-3 mt-lg-0">
						<label>Naturalidade</label>
						<input type="text" wire:model="naturalidade" class="form-control p-form" placeholder="Cidade - Estado">
					</div>

					<div class="w-100 ms-lg-3 mt-3 mt-lg-0">
						<label>Gênero</label>
						<select wire:model="genero" class="select-form">
							<option value="">Selecione</option>
							<option value="M">Masculino</option>
							<option value="F">Feminino</option>
						</select>
					</div>

			    	<div class="w-100 ms-lg-3 mt-lg-0">
			    		<label>Profissão</label>
			    		<input type="text" wire:model="profissao" class="form-control p-form" placeholder="Profissão">
			    	</div>

			    </div>

				<div class="mb-3">
					<label>Observações</label>
					<textarea wire:model="obs" class="textarea-form" placeholder="Observações"></textarea>
				</div>

			</div>

		</section>

		<section class="mb-4-5">
			
			<a href="#boxEndereco" data-bs-toggle="collapse" aria-expanded="false" class="hover-d-none">
				<div class="section-title">
					Endereço
				</div>
			</a>
			<hr/>

			<div class="collapse show" id="boxEndereco">

				<div class="d-lg-flex mb-3">
					<div class="w-lg-50">
						<label>CEP</label>
						<input type="text" wire:model="cep" wire:change="setEndereco()" class="cep form-control p-form" placeholder="CEP" minlength="8" maxlength="9">
					</div>
					<div class="w-100 mx-lg-3 my-3 my-lg-0">
						<label>Rua</label>
						<input type="text" wire:model="rua" class="form-control p-form" placeholder="Rua">
					</div>
					<div class="w-lg-30">
						<label>Estado</label>
						<input type="text" wire:model="estado" class="form-control p-form" placeholder="UF" minlength="2" maxlength="2">
					</div>
					<div class="w-100 mx-lg-3 my-3 my-lg-0">
						<label>Bairro</label>
						<input type="text" wire:model="bairro" class="form-control p-form" placeholder="Bairro">
					</div>
					<div class="w-100">
						<label>Cidade</label>
						<input type="text" wire:model="cidade" class="form-control p-form" placeholder="Cidade">
					</div>
				</div>
	
				<div class="d-lg-flex">
					<div class="w-lg-30">
						<label>Nº</label>
						<input type="text" wire:model="numero" class="form-control p-form" placeholder="Nº">
					</div>
					<div class="w-lg-50 mx-lg-3 my-3 my-lg-0">
						<label>Complemento</label>
						<input type="text" wire:model="complemento" class="form-control p-form" placeholder="Complemento">
					</div>
					<div class="w-lg-80">
						<label>Ponto de Referência</label>
						<input type="text" wire:model="ponto_referencia" class="form-control p-form" placeholder="Ponto de Referência">
					</div>
				</div>

			</div>

		</section>

		<button type="submit" class="btn-add mt-3">Cadastrar</button>	

    </form>

    @include('livewire.util.toast')

</div>
