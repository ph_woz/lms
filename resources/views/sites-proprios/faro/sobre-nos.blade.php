<!doctype html>
<html lang="pt-br">
<head>

   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1">

   <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/uikit@3.5.9/dist/css/uikit.min.css" />
   <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
   <link rel="stylesheet" href="{{ asset('css/util.css') }}?v=3">
   <link rel="stylesheet" href="{{ asset('sites-proprios/faro/css/home.css') }}?v=3">

   <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
   
   <title>{{ $seo->seo_title ?? $plataforma->nome }}</title>
   <meta name="robots" content="index, follow">
   <meta name="description" content="{{ $seo->seo_description ?? null }}"/>
   <meta name="keywords" content="{{ $seo->seo_keywords ?? null }}"/>
   <link rel="canonical" href="{{ Request::url() }}" />
   <link rel="shortcut icon" href="{{ $plataforma->favicon }}">
   <meta property="og:locale" content="pt_BR" />
   <meta property="og:type" content="website" />
   <meta property="og:title" content="{{ $seo->seo_title ?? $plataforma->nome }}" />
   <meta property="og:description" content="{{ $seo->seo_description ?? null }}" />
   <meta property="og:url" content="{{ Request::url() }}" />
   <meta property="og:site_name" content="{{ $plataforma->nome }}" />
   <meta property="og:image" content="{{ $plataforma->logotipo }}" />
   <meta name='author' content='WOZCODE | Pedro Henrique' />

</head>
<body class="bg-f4" id="inicio">

   @include('sites-proprios/faro/componentes/header')

   <main>

      <section class="w-100 h-lg-450 background-size-cover background-position-center" style="background-image: url('{{ $plataforma->jumbotron ?? asset('sites-proprios/faro/images/jumbotron.jpeg') }}');">
         <div class="position-relative h-100">
            <div class="background-overlay opacity-08 h-100" style="background: rgba(0,0,0,1);"></div>
            <div class="container h-100">
               <div class="row container align-items-center h-100 position-relative">

                  <div class="mt--lg-75px py-5 py-lg-0">
                     <p class="text-light font-poppins weight-300 fs-xs-35 fs-md-50 text-shadow-3 col-lg-6 mb-0">
                        Sobre a
                     </p>
                     <h1 class="text-light font-poppins weight-800 fs-xs-35 fs-md-50 text-shadow-3 col-lg-6" id="tituloSobreNos">
                        Faculdade Faro
                     </h1>
                     <p class="mb-0 weight-600 text-light fs-20">
                        Há +16 anos transformando vidas por meio da educação.
                     </p>
                  </div>

               </div>
            </div>
         </div>
      </section>

      <section class="pt-5 mt--lg-125px position-relative z-3">
         <div class="container">

            <div class="bg-white p-lg-5 p-4 box-shadow rounded-20">
               <div class="row align-items-center">

                  <div class="order-0 col-lg-8">

                     <div class="pe-lg-5">
                        
                        <h1 class="mb-0 color-navlink font-poppins weight-800 fs-35">Faculdade de Roseira</h1>

                        <p class="mb-0 font-opensans fs-25 color-555">
                           Onde mentes abertas se conectam.
                        </p>
                        <p class="mb-4 mt-3 weight-600 color-444 fs-18">
                           Inovação nos define. Em mais de 16 anos de história sempre nos comprometemos a oferecer a mais alta qualidade de ensino com a visão para antecipar as tendências do mercado e formar profissionais globais, completos. Com infraestrutura moderna, professores experientes e uma forte conexão com o mercado de trabalho.
                        </p>

                        <div class="rounded-20 w-100 h-lg-250 background-size-cover background-position-center" style="background-image: url('{{ $plataforma->jumbotron ?? asset('sites-proprios/faro/images/jumbotron.jpeg') }}');">
                           <div class="rounded-20 position-relative h-100">
                           <div class="rounded-20 background-overlay h-100" style="background: rgba(0,0,0,1);"></div>
                              <div class="container h-100 py-5 py-lg-0">
                                 <div class="row container align-items-center h-100 position-relative">
                                    <h1 class="text-light font-poppins weight-800 fs-xs-35 fs-md-50 text-shadow-3" id="tituloProUni">
                                       A FARO
                                    </h1>
                                    <div class="text-center mt--lg-150px">
                                       <a href="https://youtube.com/shorts/CRUM5SRKGTA?feature=share" target="_blank">
                                          <i class="fas fa-play-circle text-light font-poppins weight-800 fs-55 text-shadow-3"></i>                                 
                                       </a>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>

                     </div>

                  </div>

                  <div class="order-1 col-lg-4 mt-5 mt-lg-0">

                     <div class="d-flex">

                        <div class="d-xs-none w-3px h-lg-200 bg-gradient"></div>

                        <div class="px-5 text-center mb-5 mb-lg-0">
                           <div class="text-center">
                              <i class="fa fa-graduation-cap color-azul fs-50"></i>
                           </div>
                           <h1 class="color-icon-footer weight-900 font-montserrat text-shadow-1">
                              80+
                           </h1>
                           <p class="mb-0 color-azul weight-600 font-poppins">
                              CURSOS
                           </p>
                           <p class="mb-0 color-azul weight-600 font-poppins">
                              no Guia da Faculdade
                           </p>
                        </div>

                     </div>
                     <div class="d-flex">

                        <div class="d-xs-none w-3px h-lg-200 bg-gradient"></div>

                        <div class="px-5 text-center mb-5 mb-lg-0">
                           <div class="text-center">
                              <i class="fas fa-chalkboard-teacher color-azul fs-50"></i>
                           </div>
                           <h1 class="color-icon-footer weight-900 font-montserrat text-shadow-1">
                              90%
                           </h1>
                           <p class="mb-0 color-azul weight-600 font-poppins">
                              MESTRES E DOUTORES
                           </p>
                           <p class="mb-0 color-azul weight-600 font-poppins">
                              dos nossos professores
                           </p>
                        </div>

                     </div>
                     <div class="d-flex">

                        <div class="d-xs-none w-3px h-lg-200 bg-gradient"></div>

                        <div class="px-5 text-center mb-5 mb-lg-0">
                           <div class="text-center">
                              <i class="fas fa-swatchbook color-azul fs-50"></i>
                           </div>
                           <h1 class="color-icon-footer weight-900 font-montserrat text-shadow-1">
                              150+
                           </h1>
                           <p class="mb-0 color-azul weight-600 font-poppins">
                              ÍTENS DE ACERVOS
                           </p>
                           <p class="mb-0 color-azul weight-600 font-poppins">
                              na Biblioteca da Faro
                           </p>
                        </div>

                     </div>
                     
                  </div>

               </div>
            </div>

         </div>
      </section>

      <section class="pt-5 position-relative z-3">
         <div class="container">

            <div class="bg-white p-lg-5 p-4 box-shadow rounded-20">

               <h1 class="color-navlink font-poppins fs-35">
                  Por que escolher a <span class="weight-800">FARO?</span>
               </h1>

               <p class="mb-4 weight-600 color-444 fs-18">
                  Só aqui você encontra uma metodologia de ensino moderna e inovadora em conjunto com cursos com nota máxima do MEC e infraestrutura de padrão internacional.
               </p>

               <a href="/f/formulario-de-matricula/45/t/631" target="_blank" class="bg-faixa d-xs-block bg-hover-gradient fs-14 text-center px-4 rounded box-shadow text-light weight-700 py-2-5 font-poppins hover-d-none d-xs-block">
                  INSCREVA-SE AGORA
               </a>
               <a href="/#cursos" target="_blank" class="ms-lg-3 mt-3 mt-lg-0 d-xs-block bg-gradient bg-hover-gradient fs-14 text-center px-4 rounded box-shadow text-dark weight-700 py-2-5 font-poppins hover-d-none">
                  <span class="me-2">CONHEÇA NOSSOS CURSOS</span>
               </a>

            </div>

         </div>
      </section>

      <section class="pt-5 position-relative z-3">
         <div class="container">

            <div class="bg-white p-lg-5 p-4 box-shadow rounded-20">

               <h1 class="mb-3 color-navlink font-poppins weight-800 fs-35">
                  Os valores que nos
                  movem <span class="weight-600">rumo ao futuro</span>
               </h1>

               <div class="uk-slider-container-offset" uk-slider="autoplay: true; autoplay-interval: 1200; pause-on-hover: true">

                   <div class="uk-position-relative uk-visible-toggle uk-light" tabindex="-1">

                       <ul class="uk-slider-items uk-child-width-1-6@s">
                           <li class="px-2 cursor-drag">
                              <div class="d-flex justify-content-center">
                                 <div class="bg-gradient text-dark rounded-circle square-75 flex-center fs-25 z-3 position-relative box-shadow" style="border:2px solid #fff; margin-bottom: -35px;">
                                    <i class="fas fa-globe-africa"></i>
                                 </div>
                              </div>
                              <div class="bg-white border h-150 box-shadow px-3 pt-5 rounded-10">
                                 <p class="mb-0 text-dark text-center weight-600 fs-17">
                                    Respeito a Diversidade
                                 </p>
                              </div>
                           </li>
                           <li class="px-2 cursor-drag">
                              <div class="d-flex justify-content-center">
                                 <div class="bg-gradient text-dark rounded-circle square-75 flex-center fs-25 z-3 position-relative box-shadow" style="border:2px solid #fff; margin-bottom: -35px;">
                                    <i class="fa fa-users"></i>
                                 </div>
                              </div>
                              <div class="bg-white border h-150 box-shadow px-3 pt-5 rounded-10">
                                 <p class="mb-0 text-dark text-center weight-600 fs-17">
                                    Trabalho em Equipe
                                 </p>
                              </div>
                           </li>
                           <li class="px-2 cursor-drag">
                              <div class="d-flex justify-content-center">
                                 <div class="bg-gradient text-dark rounded-circle square-75 flex-center fs-25 z-3 position-relative box-shadow" style="border:2px solid #fff; margin-bottom: -35px;">
                                    <i class="fas fa-hand-holding-heart"></i>
                                 </div>
                              </div>
                              <div class="bg-white border h-150 box-shadow px-3 pt-5 rounded-10">
                                 <p class="mb-0 text-dark text-center weight-600 fs-17">
                                    Paixão pela Educação
                                 </p>
                              </div>
                           </li>
                           <li class="px-2 cursor-drag">
                              <div class="d-flex justify-content-center">
                                 <div class="bg-gradient text-dark rounded-circle square-75 flex-center fs-25 z-3 position-relative box-shadow" style="border:2px solid #fff; margin-bottom: -35px;">
                                    <i class="fas fa-sitemap"></i>
                                 </div>
                              </div>
                              <div class="bg-white border h-150 box-shadow px-3 pt-5 rounded-10">
                                 <p class="mb-0 text-dark text-center weight-600 fs-17">
                                    Inovação
                                 </p>
                              </div>
                           </li>
                           <li class="px-2 cursor-drag">
                              <div class="d-flex justify-content-center">
                                 <div class="bg-gradient text-dark rounded-circle square-75 flex-center fs-25 z-3 position-relative box-shadow" style="border:2px solid #fff; margin-bottom: -35px;">
                                    <i class="fas fa-chart-line"></i>
                                 </div>
                              </div>
                              <div class="bg-white border h-150 box-shadow px-3 pt-5 rounded-10">
                                 <p class="mb-0 text-dark text-center weight-600 fs-17">
                                    Foco em Resultado
                                 </p>
                              </div>
                           </li>
                           <li class="px-2 cursor-drag">
                              <div class="d-flex justify-content-center">
                                 <div class="bg-gradient text-dark rounded-circle square-75 flex-center fs-25 z-3 position-relative box-shadow" style="border:2px solid #fff; margin-bottom: -35px;">
                                    <i class="fas fa-tree"></i>
                                 </div>
                              </div>
                              <div class="bg-white border h-150 box-shadow px-3 pt-5 rounded-10">
                                 <p class="mb-0 text-dark text-center weight-600 fs-17">
                                    Responsabilidade de Social e Ambiental
                                 </p>
                              </div>
                           </li>
                           <li class="px-2 cursor-drag">
                              <div class="d-flex justify-content-center">
                                 <div class="bg-gradient text-dark rounded-circle square-75 flex-center fs-25 z-3 position-relative box-shadow" style="border:2px solid #fff; margin-bottom: -35px;">
                                    <i class="fas fa-book-open"></i>
                                 </div>
                              </div>
                              <div class="bg-white border h-150 box-shadow px-3 pt-5 rounded-10">
                                 <p class="mb-0 text-dark text-center weight-600 fs-17">
                                    Ética e Transparência
                                 </p>
                              </div>
                           </li>
                       </ul>

                       <a class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous uk-slider-item="previous"></a>
                       <a class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-next uk-slider-item="next"></a>

                   </div>

                   <ul class="uk-slider-nav uk-dotnav uk-flex-center uk-margin"></ul>

               </div>

            </div>

         </div>
      </section>

      <section class="py-5 position-relative z-3">
         <div class="container">

            <div class="bg-white p-lg-5 p-4 box-shadow rounded-20">

               <div class="row align-items-center">

                  <div class="col-lg-6">

                     <h1 class="mb-3 color-navlink font-poppins weight-800 fs-35">
                        Depoimentos
                        dos <br> <span class="weight-600">nossos alunos</span>
                     </h1>

                     <p class="mb-0 color-navlink font-poppins weight-600 fs-18 mt-3 pe-lg-5">
                        Ao longo dos anos, com base do que é visivel sobre nossos ensinos, envaidece-nos deixando assim a nosso cargo a obrigação de proporcionar ao alunado ensinamentos com tecnologias avançadas, pautadas nos melhores referênciais pedagógicas.
                     </p>

                  </div>

                  <div class="col-lg-6">

                     <div class="uk-slider-container-offset" uk-slider>

                         <div class="uk-position-relative uk-visible-toggle uk-light" tabindex="-1">

                             <ul class="uk-slider-items uk-child-width-1-1@s uk-grid">
                                 @foreach($depoimentos as $depoimento)
                                 <li>
                                     <div class="uk-card box-shadow bg-faixa rounded-10 h-lg-325 cursor-drag">
                                         <div class="uk-card-body text-dark">
                                             <i class="fas fa-quote-left fs-50 color-faixa-verde opacity-04"></i>
                                             <div class="mt-1 d-lg-flex align-items-center">
                                                <div class="my-2 mb-lg-0 d-xs-flex align-items-center w-200px">
                                                   <div class="w-100px text-center">
                                                      <img src="{{ $depoimento->foto }}" class="object-fit-cover rounded-circle square-75">
                                                   </div>
                                                   <p class="mb-0 text-light weight-600 text-center">
                                                      {{ $depoimento->nome }}
                                                   </p>
                                                </div>
                                                <div>
                                                   <p class="mt--lg-15px ms-lg-4 font-poppins fs-14 text-light mb-2">
                                                     {{ $depoimento->descricao }}
                                                   </p>
                                                   <div class="ms-lg-4">
                                                      <i class="fa fa-star color-star"></i>
                                                      <i class="fa fa-star color-star"></i>
                                                      <i class="fa fa-star color-star"></i>
                                                      <i class="fa fa-star color-star"></i>
                                                      <i class="fa fa-star color-star"></i>
                                                   </div>
                                                </div>
                                             </div>
                                         </div>
                                     </div>
                                 </li>
                                 @endforeach
                             </ul>

                             <a class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous uk-slider-item="previous"></a>
                             <a class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-next uk-slider-item="next"></a>

                         </div>

                         <ul class="uk-slider-nav uk-dotnav uk-flex-center uk-margin"></ul>

                     </div>

                  </div>

            </div>

         </div>
      </section>

      <section class="px-lg-5 py-lg-5 py-4 px-3 bg-gradient">
         <div class="container">

            <h2 class="text-center text-dark font-poppins weight-500 fs-20">Conheça os Pilares da </h2>
            <h1 class="mb-4 text-center text-dark font-poppins weight-700">
               Faculdade de Roseira
            </h1>

            <div id="myGroup">

               <button class="btn-info m-2 outline-0" type="button" data-bs-toggle="collapse" data-bs-target="#collapseSecao1" aria-expanded="false" aria-controls="collapseWidthExample">
                  Inovação e Criatividade
               </button>
               <button class="btn-info m-2 outline-0" type="button" data-bs-toggle="collapse" data-bs-target="#collapseSecao2" aria-expanded="false" aria-controls="collapseWidthExample">
                  Internacionalidade
               </button>
               <button class="btn-info m-2 outline-0" type="button" data-bs-toggle="collapse" data-bs-target="#collapseSecao3" aria-expanded="false" aria-controls="collapseWidthExample">
                  Espaços inspiradores
               </button>

               <div class="collapse" id="collapseSecao1">
                  <div class="bg-white p-lg-5 p-4 rounded-20 box-shadow mt-5">

                     <h1 class="color-base font-poppins weight-800 fs-md-35 fs-xs-30">Inovação e Criatividade</h1>
                     <p class="font-opensans fs-21 color-555">
                        Pensar fora da caixa nos inspira. A Faculdade Faro cria novos padrões de mercado, oferecendo cursos inovadores, com teoria e prática realizadas em ambientes disruptivos, que desenvolvem sua capacidade de pensar de forma diferente, e te preparam para o futuro.
                     </p>

                  </div>
               </div>
            
               <div class="collapse" id="collapseSecao2">
                  <div class="bg-white p-lg-5 p-4 rounded-20 box-shadow mt-5">

                     <h1 class="color-base font-poppins weight-800 fs-md-35 fs-xs-30">Internacionalidade</h1>
                     <p class="font-opensans fs-21 color-555">
                        Conectada com o futuro e com as novas tendências, a Faculdade Faro prepara você para protagonizar os desafios do mercado, em diversas formas de intercâmbio entre diferentes culturas. Nossos alunos e alunas são inspirados a pensar de forma global, pois promovemos a conexão com pessoas de todas as partes do mundo.
                     </p>

                  </div>
               </div>

               <div class="collapse" id="collapseSecao3">
                  <div class="bg-white p-lg-5 p-4 rounded-20 box-shadow mt-5">

                     <h1 class="color-base font-poppins weight-800 fs-md-35 fs-xs-30">Espaços inspiradores</h1>
                     <p class="font-opensans fs-21 color-555">
                        A Faculdade Faro se destaca entre as faculdades com a melhor infraestrutura do Brasil, com metodologias inovadoras e equipamentos de última geração, que estimulam a inovação e criatividade dos nossos alunos. Nossos espaços inspiram novos experimentos e o compartilhamento de ideias.
                     </p>

                  </div>
               </div>

            </div>

         </div>
      </section>

      <section class="py-5 position-relative z-3">
         <div class="container">

            <div class="bg-white p-lg-5 p-4 box-shadow rounded-20">

               <div class="row">

                  <div class="col-lg-6">

                     <h1 class="mb-lg-5 mb-3 color-navlink font-poppins weight-600 fs-35">
                        Conheça nosso<br><span class="weight-800">Corpo Administrativo</span>
                     </h1>

                     <div class="mb-4">
                        <p class="mb-0 weight-700 font-poppins color-navlink fs-19">Diretor Geral</p>
                        <p class="mb-0 weight-600 fs-17">
                           <!-- Prof. Dr. Ueverton Rodrigues de Sousa  -->
                           Prof. Dr. Fernando Horita
                        </p>
                     </div>

                     <div class="mb-4">
                        <p class="mb-0 weight-700 font-poppins color-navlink fs-19">Diretora Acadêmica</p>
                        <p class="mb-0 weight-600 fs-17">
                           Profª Drª Alcinéa Guimarães de castro
                        </p>
                     </div>

                     <div class="mb-4">
                        <p class="mb-0 weight-700 font-poppins color-navlink fs-19">Secretário Acadêmico</p>
                        <p class="mb-0 weight-600 fs-17">
                           Marcos Vinicius Botelho Albernaz
                        </p>
                     </div>

                     <div class="mb-4">
                        <p class="mb-0 weight-700 font-poppins color-navlink fs-19">Vice Secretário Acadêmico</p>
                        <p class="mb-0 weight-600 fs-17">
                           Prof. Everson Gustavo de Oliveira
                        </p>
                     </div>

                     <!--
                     <div class="mb-4">
                        <p class="mb-0 weight-700 font-poppins color-navlink fs-19">Diretor Financeiro</p>
                        <p class="mb-0 weight-600 fs-17">
                           Prof. Bruno Ricardo de Sousa Chanes
                        </p>
                     </div>
                     -->
                     
<!--                      <div class="mb-4">
                        <p class="mb-0 weight-700 font-poppins color-navlink fs-19">Diretor de Pesquisa</p>
                        <p class="mb-0 weight-600 fs-17">
                           Prof. Dr. Rogério Rodrigo Ramos
                        </p>
                     </div> -->

                  </div>

                  <div class="col-lg-6 mt-4 mt-lg-0">

                     <h1 class="mb-lg-5 mb-3 color-navlink font-poppins weight-800 fs-35">
                        Atos Autorizativos<br>Institucionais
                     </h1>

                     <div class="mb-4">
                        <p class="mb-0 weight-700 font-poppins color-navlink fs-19">Modalidade Presencial</p>
                        <p class="mb-0 weight-600 fs-17">
                           Recredenciada pela portaria MEC nº 799 de 07 de agosto de 2015, publicada no D.O.U de 10 de agosto de 2015, seção 1, pág 18.
                        </p>
                     </div>

                  </div>

            </div>

         </div>
      </section>

   </main>

   @include('sites-proprios/faro/componentes/footer')

   <a href="https://api.whatsapp.com/send?phone=55{{preg_replace('/\D/', '', $plataforma->whatsapp)}}&text=Olá, vim através do Site {{ $plataforma->nome }}" target="_blank" class="text-white fs-30">
       <div class="fixed-bottom right-0 me-3 mb-3 z-3">
           <div class="square-60 rounded-circle bg-wpp flex-center box-shadow-wpp">
               <i class="fab fa-whatsapp"></i>
           </div>
       </div>
   </a>

   <script src="https://cdn.jsdelivr.net/npm/uikit@3.5.9/dist/js/uikit.min.js"></script>
   <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-U1DAWAznBHeqEIlVSCgzq+c9gqGAJn5c/t99JyeKa9xxaYpSvHU5awsuZVVFIhvj" crossorigin="anonymous"></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>
   <script>

      var $myGroup = $('#myGroup');
      $myGroup.on('show.bs.collapse','.collapse', function() {
         $myGroup.find('.collapse.show').collapse('hide');
      });

      window.onload = function()
      {
         $(".btn-info").first().click().focus();
         window.scrollTo(0, 0);
      };

      window.onscroll = function() {myFunction()};

      var header = document.getElementById("menu");
      var sticky = header.offsetTop;

      function myFunction() {
        if (window.pageYOffset > sticky) {
          header.classList.add("fixed-top");
        } else {
          header.classList.remove("fixed-top");
        }
      }
   </script>

</body>
</html>