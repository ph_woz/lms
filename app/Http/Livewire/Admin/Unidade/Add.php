<?php

namespace App\Http\Livewire\Admin\Unidade;

use Livewire\Component;
use App\Models\Unidade;
use Illuminate\Support\Facades\Http;

class Add extends Component
{
    public $unidade;
    public $nome;
    public $telefone;
    public $celular;
    public $email;
    public $endereco;
    public $status = 0;
    public $publicar = 'N';

    public function add()
    {
        $this->unidade = Unidade::create([
            'nome'     => $this->nome,
            'telefone' => $this->telefone,
            'celular'  => $this->celular,
            'email'    => $this->email,
            'endereco' => $this->endereco,
            'status'   => $this->status,
            'publicar' => $this->publicar,
        ]);

        session()->flash('success-livewire', 'Unidade criada com êxito.');
        
        $this->resetFields();

        $this->dispatchBrowserEvent('toast');
    }

    public function resetFields()
    {
        $this->nome     = null;
        $this->telefone = null;
        $this->celular  = null;
        $this->email    = null;
        $this->endereco = null;
        $this->status   = 0;
        $this->publicar = 'N';
    }

    public function render()
    {
        return view('livewire.admin.unidade.add');
    }
}
