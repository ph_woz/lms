<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FluxoCaixa extends Model
{
    use HasFactory;

    public $timestamps = true;

    protected $table = 'fluxo_caixa';

    protected $fillable = [
        'plataforma_id',
        'referencia',
        'cadastrante_id',
        'unidade_id',
        'vendedor_id',
        'registro_id',
        'cliente_id',
        'produto_id',
        'cadastrante_nome',
        'vendedor_nome',
        'unidade_nome',
        'cliente_nome',
        'produto_nome',
        'tipo_transacao', // E(ntrada) x S(aída)
        'tipo_produto', // T(trilha) x C(lass)/Turma
        'valor_parcela',
        'valor_total',
        'forma_pagamento',
        'n_parcela',
        'n_parcelas',
        'link',
        'data_recebimento',
        'data_vencimento',
        'data_estorno',
        'motivo_estorno',
        'status',
        'obs',
    ];

    public function scopePlataforma($query)
    {
        return $query->where('plataforma_id', session('plataforma_id') ?? \Auth()->user()->plataforma_id);
    }

    public function scopeAtivo($query)
    {
        return $query->where('status', 0);
    }

    public function scopeBilling($query)
    {
        return $query->where('tipo_transacao', 'B');
    }

    public function scopeEntrada($query)
    {
        return $query->where('tipo_transacao', 'E');
    }
    public function scopeSaida($query)
    {
        return $query->where('tipo_transacao', 'S');
    }

    public function scopeTrilha($query)
    {
        return $query->where('tipo_produto', 'T');
    }

    public function scopeTurma($query)
    {
        return $query->where('tipo_produto', 'C');
    }

    public function scopeRecebido($query)
    {
        return $query->where('status', 1);
    }

    public function scopePendente($query)
    {
        return $query->where('status', 0);
    }

    public function scopePago($query)
    {
        return $query->where('status', 1);
    }

    public static  function boot()
    {
        parent::boot(); 
        static::creating(function ($model)
        {
            $model->plataforma_id  = session('plataforma_id') ?? \Auth()->user()->plataforma_id ?? Plataforma::dominio()->pluck('id')[0] ?? null;
            $model->cadastrante_id = \Auth()->user()->id ?? null;
            $model->cadastrante_nome = \Auth()->user()->nome ?? null;
        });
    }
    
    public function getValorReal($valor)
    {
        $valor = str_replace('.', '', $valor);
        $valor = str_replace(',', '.', $valor);
        $valor = floatval($valor);

        $valor = number_format($valor, 2,",",".");

        return $valor;
    }

    public static function getValorRealInArray($valoresFaturamento)
    {
        $convertValoresToFloat = [];

        foreach($valoresFaturamento as $valor)
        {
            $valor = str_replace('.', '', $valor);
            $valor = str_replace(',', '.', $valor);

            $convertValoresToFloat[] = floatval($valor);
        }

        $faturamento = number_format(array_sum($convertValoresToFloat) , 2,",",".");

        return $faturamento;
    }

    public static function getValorRealDisponivel($faturamento, $despesas)
    {
        $faturamento = str_replace('.', '', $faturamento);
        $faturamento = str_replace(',', '.', $faturamento);
        $faturamento = floatval($faturamento);

        $despesas = str_replace('.', '', $despesas);
        $despesas = str_replace(',', '.', $despesas);
        $despesas = floatval($despesas);

        $disponivel = $faturamento - $despesas;
        $disponivel = number_format($disponivel, 2,",",".");

        return $disponivel;
    }

    public function scopeWhereCreatedMonthIn($query, $mesesSelected)
    {  
        return $query->where(function($q) use($mesesSelected) {
                    foreach ($mesesSelected as $mes) {
                        $q->whereMonth('data', '=', $mes, 'or');
                    }
                });
    }

    public static function checkActive()
    {
        $check = PlataformaModulo::plataforma()->ativo()->where('url','controle-financeiro')->exists();
        
        return $check;
    }
    
}