@php
    $isMobile = \App\Models\Util::isMobile();
@endphp

<!doctype html>
<html lang="pt-br">
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Solicitar Assinatura Digital - {{ $plataforma->nome }}</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
    <link href="{{ asset('css/util.css') }}" rel="stylesheet">

	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">

    <style>
        
        @media (min-width: 769px) {
            #boxW { width: 610px; }
            .xs-460w { width: 560px; }
        }

        @media (max-width: 768px) {

            #contract-sign-area { width: 280px!important; }
            #boxW { width: 310px; }
            .xs-460w { width: 460px; }
        }

    </style>

</head>
<body>

	<main>

		<section class="py-5">
			<div class="container">

				<h1 class="fs-22">
					Olá, {{ \App\Models\Util::replacePrimeiroNome($user->nome) }}. Leia e assine o contrato da {{ $plataforma->nome }}.
				</h1>

				<hr>

				@if(session('success'))
					<div class="alert alert-success" role="alert">
					 	<p class="mb-0"><b>Sucesso!</b> {{ session('success') }}</p>
					</div>
				@endif

				<iframe src="/{{ $userContrato->pdf }}" class="w-100 h-60vh"></iframe>

				<div class="text-center">

                    @if($userContrato->data_assinatura)
                        <div class="alert alert-success mt-3" role="alert">
                            <p class="mb-0 weight-600">Este contrato já foi assinado.</p>
                        </div>
                    @endif

				</div>

                @if($userContrato->data_assinatura == null)
                <div class="flex-center mt-4">

                    <div class="text-center bg-white box-shadow p-lg-4 p-3 rounded-3" id="boxW">

                        <div class="d-flex justify-content-center">
                            <div class="xs-460w">
                                <div class="d-flex justify-content-between align-items-center">

                                    <h1 class="font-inter fs-xs-17 fs-md-25 p-0 m-0">Faça sua assinatura</h1>

                                    <button type="button" id="contract-sign-clear" class="btn btn-success fs-14 rounded weight-600">
                                        <i class="fa fa-undo-alt"></i>
                                        Refazer
                                    </button>

                                </div>
                            </div>
                        </div>

                        <form method="POST">
                        @csrf

                            <div id="canvas-wrapper" class="mt-2">

                                <canvas id="contract-sign-area" class="mt-2 rounded-3" @if($isMobile) width="460" @else width="560" @endif height="250" style="border: solid 2px #555555;"></canvas>
                                   
                                <input id="contract-sign-text" name="base64_sign" type="hidden">

                                <div class="text-center">
                                    <small>Assine dentro da área demarcada.</small>
                                </div>
                                
                            </div>

                            <hr>

                            <div class="d-lg-flex justify-content-center align-items-center">

                                <div class="mb-4 mb-lg-0">
                                    <label for="checkTermosDeUso" class="color-444 me-5">
                                        <input type="checkbox" required id="checkTermosDeUso" class="square-15">
                                        <span class="ms-2">Li e aceito os Termos do Contrato</span>
                                    </label>
                                </div>

                                <button type="submit" name="aceitar_termos" value="S" class="btn btn-primary weight-600 py-2">
                                    <i class="fas fa-check-circle"></i>
                                    &nbsp;Confirmar
                                </button>

                            </div>

                        </form>

                    </div>

                </div>
                @endif

			</div>
		</section>

	</main>

 
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js" integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN" crossorigin="anonymous"></script>

<script>


    document.addEventListener("DOMContentLoaded", function()
    {
        (function() {

            window.requestAnimFrame = (function(callback)
            {
                return window.requestAnimationFrame ||
                    window.webkitRequestAnimationFrame ||
                    window.mozRequestAnimationFrame ||
                    window.oRequestAnimationFrame ||
                    window.msRequestAnimaitonFrame ||

                    function(callback) {
                        window.setTimeout(callback, 1000/50);
                    };
            })();


            var canvas = document.getElementById("contract-sign-area");
            var modal = document.getElementById("modal-contract");

            var ctx = canvas.getContext("2d");
                ctx.strokeStyle = "#222222";
                ctx.lineWidth = 3;

            var drawing = false;
            var mousePos = { x: 0, y: 0 };
            var lastPos = mousePos;


            canvas.addEventListener("mousedown", function(e) {
                drawing = true;
                lastPos = getMousePos(canvas, e);
            }, false);

            canvas.addEventListener("mousemove", function(e) {
                mousePos = getMousePos(canvas, e);
            }, false);

            canvas.addEventListener("mouseup", function(e) {
                drawing = false;

                /**
                 ** Ao terminar o evento, salva a imagem
                 ** capturada no CANVAS para o formato 
                 ** Base64.
                 **
                 */ saveSign(canvas.toDataURL()); /*
                 */
                
            }, false);


            canvas.addEventListener("touchstart", function(e) {
                mousePos = getTouchPos(canvas, e);

                var touch = e.touches[0];
                var me = new MouseEvent("mousedown", {
                    clientX: touch.clientX,
                    clientY: touch.clientY
                });

                canvas.dispatchEvent(me);
            }, false);

            canvas.addEventListener("touchmove", function(e) {
                var touch = e.touches[0];
                var me = new MouseEvent("mousemove", {
                    clientX: touch.clientX,
                    clientY: touch.clientY
                });

                canvas.dispatchEvent(me);
            }, false);

            canvas.addEventListener("touchend", function(e) {
                var me = new MouseEvent("mouseup", {});

                canvas.dispatchEvent(me);

                /**
                 ** Ao terminar o evento, salva a imagem
                 ** capturada no CANVAS para o formato 
                 ** Base64.
                 **
                 */ saveSign(canvas.toDataURL()); /*
                 */
            }, false);


            function getMousePos(canvasDom, mouseEvent) {
                var rect = canvasDom.getBoundingClientRect();

                return {
                    x: mouseEvent.clientX - rect.left,
                    y: mouseEvent.clientY - rect.top
                }
            }

            function getTouchPos(canvasDom, touchEvent) {
                var rect = canvasDom.getBoundingClientRect();

                return {
                    x: touchEvent.touches[0].clientX - rect.left,
                    y: touchEvent.touches[0].clientY - rect.top
                }
            }

            function renderCanvas() {
                if (drawing) {
                    ctx.moveTo(lastPos.x, lastPos.y);
                    ctx.lineTo(mousePos.x, mousePos.y);
                    ctx.stroke();

                    lastPos = mousePos;
                }
            }

            // Prevent scrolling when touching the canvas
            document.addEventListener("touchstart", function(e) {
                if (e.target == canvas) {
                    modal.style.overflow = "hidden";
                    e.stopPropagation();
                }
            });

            document.addEventListener("touchend", function(e) {
                if (e.target == canvas) {
                    modal.style.overflow = "auto";
                    e.stopPropagation();
                }
            });

            document.addEventListener("touchmove", function(e) {
                if (e.target == canvas) {
                    modal.style.overflow = "hidden";
                    e.stopPropagation();
                }
            });


            (function drawLoop() {
                requestAnimFrame(drawLoop);
                renderCanvas();
            })();


            function clearCanvas() {
                canvas.width = canvas.width;
            }

            clearCanvas();


            /**
             * Customização para o correto funcionamento da UI
             */
            // var okbutton = document.getElementById('contract-ok-button');
                // okbutton.disabled = true;

            /**
             * Captura a imagem da assinatura no formato Base64
             * definindo um limite de tamanho válido, atribuindo
             * para um campo de texto (hidden) para posterior envio.
             * Atualiza a UI mostrando a validade da assinutura
             * de acordo com o limite proposto, permitindo o
             * fechamento do Modal.
             */
            function saveSign(_data)
            {
                document.getElementById('contract-sign-text').value = _data;
            }

            /**
             * Limpa o CANVAS para reiniciar a assinatura
             */
            var signclear = document.getElementById("contract-sign-clear");
                signclear.addEventListener("click", function(e) {
                    // okbutton.disabled = true;                    
                    clearCanvas();
                    saveSign('');
                });

        })();
    });

</script>


</body>
</html>