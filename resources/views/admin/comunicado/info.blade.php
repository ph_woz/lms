@extends('layouts.admin.master')

@section('content')
    
    @livewire('admin.comunicado.info', ['comunicado_id' => $comunicado_id])

@endsection

@section('js')
<script>
	window.addEventListener('dissmiss-modal', event => {
		$(".modal").modal('hide');
	});
</script>
@endsection