@if(session()->has('success-livewire'))
    <div id="box-toast-success" class="boxToast">
        <div id="alertSuccess" class="toast-success">
            <p class="mt-2 text-white">
                <span class="weight-800">Sucesso!&nbsp;</span>{{ session('success-livewire') }}
            </p>
        </div>
    </div>
    <script>
    window.addEventListener('toast', event => {
        setTimeout(function() {

            document.getElementById('box-toast-success').style.display = 'none';

        }, 2500);
    })
    </script>
@endif

@if(session()->has('danger-livewire'))
    <div id="box-toast-danger" class="boxToast">
        <div id="alertDanger" class="toast-danger">
            <p class="mt-2 text-white">
                <span class="weight-800">Ops!&nbsp;</span>{{ session('danger-livewire') }}
            </p>
        </div>
    </div>
    <script>
    window.addEventListener('toast', event => {
        setTimeout(function() {

            document.getElementById('box-toast-danger').style.display  = 'none';

        }, 2500);
    })
    </script>
@endif