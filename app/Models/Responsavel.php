<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Responsavel extends Model
{
    use HasFactory;

    protected $table = 'responsaveis';

    public $timestamps = true;
    
    protected $fillable = [
        'plataforma_id',
        'cadastrante_id',
        'nome',
        'email',
        'status',
        'cpf',
        'rg',
        'naturalidade',
        'data_nascimento',
        'celular',
        'telefone',
        'genero',
        'profissao',
        'obs',

        // endereco
    	'cep',
    	'rua',
    	'numero',
    	'bairro',
    	'cidade',
    	'estado',
    	'complemento',
    	'ponto_referencia',

    ];

    public function scopePlataforma($query)
    {
        return $query->where('plataforma_id', session('plataforma_id') ?? Plataforma::dominio()->pluck('id')[0] ?? null);
    }

    public static  function boot()
    {
        parent::boot(); 
        static::creating(function ($model) {
            $model->plataforma_id  = session('plataforma_id') ?? Auth()->user()->plataforma_id ?? Plataforma::dominio()->pluck('id')[0] ?? null;
            $model->cadastrante_id = Auth()->user()->id ?? null;
        });
    }

    public function scopeAtivo($query)
    {
        return $query->where('status', 0);
    }

}
