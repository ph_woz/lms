<!doctype html>
<html lang="pt-br">
<head>

  <!-- Google tag (gtag.js) -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=G-MREB450HCT"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'G-MREB450HCT');
  </script>
  
   {!! $plataforma->scripts_start_head !!}

   <meta charset="utf-8">
   <meta name="viewport" content="width=device-width, initial-scale=1">

   <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KK94CHFLLe+nY2dmCWGMq91rCGa5gtU4mk92HdvYe+M/SXH301p5ILy+dN9+nJOZ" crossorigin="anonymous">
   <link rel="stylesheet" href="{{ asset('sites-proprios/cefaep2/css/home.css') }}">
   <link rel="stylesheet" href="{{ asset('css/util.css') }}">

   <title>{{ $pagina->title ?? $plataforma->nome }}</title>
   <meta name="robots" content="index, follow">
   <meta name="description" content="{{ $pagina->description ?? null }}"/>
   <meta name="keywords" content="{{ $pagina->keywords ?? null }}"/>
   <link rel="canonical" href="{{ Request::url() }}" />
   <link rel="shortcut icon" href="{{ $plataforma->favicon }}">
   <meta property="og:locale" content="pt_BR" />
   <meta property="og:type" content="website" />
   <meta property="og:title" content="{{ $pagina->title ?? $plataforma->nome }}" />
   <meta property="og:description" content="{{ $pagina->description ?? null }}" />
   <meta property="og:url" content="{{ Request::url() }}" />
   <meta property="og:site_name" content="{{ $plataforma->nome }}" />
   <meta property="og:image" content="{{ $plataforma->logotipo }}" />
   <meta name='author' content='WOZCODE | Pedro Henrique' />

   {!! $plataforma->scripts_final_head !!}

</head>
<body id="inicio">

	@include('sites-proprios/cefaep2/componentes/header')

	<main>

      <section>
       <div class="w-100 h-200 background-size-cover background-position-center" style="background-image: url('{{ asset('sites-proprios/cefaep2/images/header-contato.webp') }}">
          <div class="position-relative h-100 px-xl-7 px-3">

            <!--
             <div class="row align-items-center h-100 position-relative">
                <div class="mt-2">
                   <h1 class="text-light font-saira-condensed weight-800 fs-xs-35 fs-md-50 text-shadow-3">
                      CONTATO
                   </h1>
                </div>
             </div>
            -->
            
          </div>
       </div>
      </section>

<section class="pt-5 pb-2 bg-light" id="contato">
      <div class="container-fluid px-xl-8">

         <h1 class="font-inter fs-18 color-444">
            Dúvidas? Fale conosco sem compromisso.<br>
            Nossa equipe está pronta para atender você e tirar todas as suas dúvidas.
         </h1>

         <hr>

         <div class="mb-5 row py-lg-4 px-3 px-md-0">
            
            <div class="col-lg-6 mt-4 mb-5 mb-lg-0 contact-methods-container">      
              
               <div class="mb-3 d-xs-flex justify-content-center">
                  <a href="https://api.whatsapp.com/send?phone=55{{preg_replace('/\D/', '', $plataforma->whatsapp)}}&text=Olá, vim através do Site {{ $plataforma->nome }}" target="_blank" class="hover-d-none text-d-none text-light w-xs-100">
                     <div class="bg-btn-contato bg-btn-contato-hover rounded px-4 py-3-5 d-flex align-items-center">
                        <i class="text-shadow-2 bi bi-whatsapp fs-30 me-3"></i>
                        <span class="me-2 weight-600 text-shadow-2">WhatsApp</span>
                        <span class="ms-auto weight-600 font-opensans text-shadow-2">{{ $plataforma->whatsapp ?? '(00) 00000-0000' }}</span>
                     </div>
                  </a>
               </div>
               <div class="mb-3 d-xs-flex justify-content-center">
                  <a href="tel:+55{{preg_replace('/\D/', '', $plataforma->telefone)}}" target="_blank" class="hover-d-none text-d-none text-light w-xs-100">
                     <div class="bg-btn-contato bg-btn-contato-hover rounded px-4 py-3-5 d-flex align-items-center">
                        <i class="text-shadow-2 bi bi-telephone fs-30 me-3"></i>
                        <span class="me-2 weight-600 text-shadow-2">Telefone</span>
                        <span class="ms-auto weight-600 font-opensans text-shadow-2">{{ $plataforma->telefone ?? '(00) 00000-0000' }}</span>
                     </div>
                  </a>
               </div>
               <div class="mb-3 d-xs-flex justify-content-center">
                  <a href="mailto:{{ $plataforma->email ?? 'meuemail@exemplo.com' }}" class="hover-d-none text-d-none text-light">
                     <div class="bg-btn-contato bg-btn-contato-hover rounded px-4 py-3-5 d-flex align-items-center">
                        <i class="text-shadow-2 bi bi-envelope fs-25 me-3"></i>
                        <span class="me-2 weight-600 text-shadow-2">Email</span>
                        <span class="ms-auto weight-600 font-opensans fs-15 text-shadow-2">{{ $plataforma->email ?? 'meuemail@exemplo.com' }}</span>
                     </div>
                  </a>
               </div>
               
            </div>

            <div class="col-lg-6">
             <form method="POST">
            @csrf

               @if(session('success'))
                  <div class='alert alert-success'><b>Sucesso!</b> {{ session('success') }}</div>
               @endif

               <div>
                 <div class="d-flex align-items-center">
                   <i class="fa fa-user color-icon-form"></i>
                   <input type="text" class="lead-form border-0 bg-transparent mb-0 form-control fs-17 outline-0" name="nome" placeholder="Nome" required="">
                 </div>
                 <hr class="mb-3 border border-dark">
               </div>

               <div class="my-4">
                 <div class="d-flex align-items-center">
                   <i class="fa fa-envelope color-icon-form"></i>
                   <input type="email" class="lead-form border-0 bg-transparent mb-0 form-control fs-17 outline-0" name="email" placeholder="Email" required="">
                 </div>
                 <hr class="mb-3 border border-dark">
               </div>

               <div class="my-4">
                 <div class="d-flex align-items-center">
                   <i class="fa fa-phone color-icon-form"></i>
                   <input type="tel" class="lead-form border-0 bg-transparent mb-0 form-control fs-17 outline-0" name="celular" placeholder="Celular" required="">
                 </div>
                 <hr class="mb-3 border border-dark">
               </div>
               
               <div>
                 <div class="d-flex">
                   <i class="fa fa-comment-alt color-icon-form pt-2-5"></i>
                   <textarea name="mensagem" class="lead-form border-0 bg-transparent mb-0 form-control fs-17 outline-0 h-min-100" placeholder="Mensagem" required=""></textarea>
                 </div>
                 <hr class="mb-3 border border-dark">
               </div>

               <input type="text" name="check" class="invisible h-0px" placeholder="Email">

               <button type="submit" name="btnSend" value="ok" class="w-100 px-4 py-3 fs-20 btn btn-quero-me-matricular">
                  Enviar
               </button>

             </form>
            </div>
            
         </div>

      </div>
   </section>

	</main>

	@include('sites-proprios/cefaep2/componentes/footer')

</body>
</html>