<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BlogPost extends Model
{
    protected $table = 'blog_posts';

    public $timestamps = true;

    protected $fillable = [ 
    	'plataforma_id', 
    	'cadastrante_id',
    	'autor_id',
    	'titulo',
        'slug',
    	'publicar',
    	'foto_capa',
    	'descricao',
        'destaque_ordem',
        
        // SEO
        'seo_title',
        'seo_keywords',
        'seo_description',

    ];

    public static  function boot()
    {
        parent::boot(); 
        static::creating(function ($model)
        {
            $model->plataforma_id  = session('plataforma_id') ?? \Auth()->user()->plataforma_id ?? Plataforma::dominio()->pluck('id')[0] ?? null;
            $model->cadastrante_id = \Auth()->user()->id ?? null;
        });
    }
    
    public function scopePlataforma($query)
    {
        return $query->where('plataforma_id', session('plataforma_id') ?? Plataforma::dominio()->pluck('id')[0] ?? null);
    }

    public function scopePublico($query)
    {
        return $query->where('publicar', 'S');
    }

}
