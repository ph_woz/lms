    <header id="menuPlataforma">
    <nav class="navbar-header navbar navbar-expand-lg fixed-top shadow" id="menu">
        <div class="container-fluid px-xl-7">

          <a class="navbar-brand" href="/#inicio">
                          <img src="{{ $plataforma->logotipoMenu ?? asset('sites-proprios/geracaodevencedores-com-br/images/logotipo.png') }}" class="img-fluid" width="180">
                      </a>

          <button class="bg-transparent d-lg-none outline-0 border-0 menu-link-color" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="fa fa-bars fs-20"></span>
          </button>

          <div class="collapse navbar-collapse py-4 py-lg-0" id="navbarSupportedContent">

              <ul class="navbar-nav ml-auto ulNavLinks">
                                      <li class="nav-item">
                        <a class="menu-link" 
                           href="/#inicio" >
                            Início
                        </a>
                    </li>
                                                          <li class="nav-item">
                        <a class="menu-link " 
                           href="/#sobre-nos" >
                            Quem somos
                        </a>
                    </li>
                                                          <li class="nav-item">
                        <a class="menu-link " 
                           href="/#cursos" >
                            Cursos
                        </a>
                    </li>
                                                          <li class="nav-item">
                        <a class="menu-link " 
                           href="/blog" >
                            Blog
                        </a>
                    </li>
                                                          <li class="nav-item">
                        <a class="menu-link " 
                           href="/#contato" >
                            Fale Conosco
                        </a>
                    </li>
                    <li class="nav-item ml-lg-5">
                      <a href="/login" class="cursor-pointer btn-entrar border-0 text-white rounded-3 hover-d-none bg-yellow shadow weight-800 px-4">
                        <i class="fa fa-user-graduate"></i>
                          Entrar
                      </a>
                    </li>
              </ul>

          </div>

        </div>
    </nav>
</header>