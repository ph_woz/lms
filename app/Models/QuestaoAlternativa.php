<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class QuestaoAlternativa extends Model
{
    use HasFactory;

    protected $table = 'questoes_alternativas';

    public $timestamps = false;

    public $fillable = [
        'plataforma_id',
        'questao_id',
    	'alternativa',
    	'correta',
    ];

    public static  function boot()
    {
        parent::boot(); 
        static::creating(function ($model)
        {
            $model->plataforma_id = session('plataforma_id') ?? \Auth()->user()->plataforma_id ?? Plataforma::dominio()->pluck('id')[0] ?? null;
        });
    }
    
    public function scopePlataforma($query)
    {
        return $query->where('plataforma_id', session('plataforma_id') ?? Plataforma::dominio()->pluck('id')[0] ?? null);
    }

    public function alphabetic($index)
    {
        switch ($index)
        {
            case 0:
                return 'A';    
            break;
            case 1:
                return 'B';    
            break;
            case 2:
                return 'C';    
            break;    
            case 3:
                return 'D';    
            break;
            case 4:
                return 'E';    
            break;
            case 5:
                return 'F';    
            break;    
            case 6:
                return 'G';    
            break;
            case 7:
                return 'H';    
            break;
            case 8:
                return 'I';    
            break; 
            case 9:
                return 'J';    
            break;    
            case 10:
                return 'K';    
            break;
            case 11:
                return 'L';    
            break;
            case 12:
                return 'M';    
            break;   
            case 13:
                return 'N';    
            break;    
        }
    }
}
