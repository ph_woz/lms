<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ComunicadoController extends Controller
{
    public function lista()
    {
        return view('admin.comunicado.lista');
    }
    
    public function info($comunicado_id)
    {
        return view('admin.comunicado.info', compact('comunicado_id') );
    }

    public function add()
    {
    	return view('admin.comunicado.add');
    }

    public function editar($comunicado_id)
    {
        return view('admin.comunicado.editar', compact('comunicado_id') );
    }

}