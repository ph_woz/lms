<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class QuestaoController extends Controller
{
    public function lista()
    {
        return view('admin.questao.lista');
    }
    
    public function info($questao_id)
    {
        return view('admin.questao.info', compact('questao_id') );
    }

    public function add()
    {
        $categorias = \App\Models\Categoria::plataforma()->tipo('questao')->orderBy('nome')->get();

        if(request()->has('assunto')) {

            $questao = \App\Models\Questao::create(request()->all());

            $status = true;
            if($questao->modelo == 'O')
                $status = $this->syncAlternativas($questao->id);
            
            $this->syncCategorias($questao->id);
            $this->syncArquivo($questao, 'arquivo', 'arq');

            if($status == true)
                return redirect('/admin/questao/editar/'.$questao->id)->with('success', 'Questão cadastrada com êxito.');
            else
                return redirect('/admin/questao/editar/'.$questao->id)->with('danger', 'Alternativa Correta não selecionada.');
        }

        return view('admin.questao.add', compact('categorias') );
    }

    public function editar($questao_id)
    {
        $questao = \App\Models\Questao::plataforma()->findOrFail($questao_id);

        $alternativas = \App\Models\QuestaoAlternativa::where('questao_id', $questao_id)->get();

        $categorias = \App\Models\Categoria::plataforma()->tipo('questao')->orderBy('nome')->get();
        $categoriasSelected = \App\Models\QuestaoCategoria::plataforma()->where('questao_id', $questao_id)->get()->pluck('categoria_id')->toArray();

        if(request()->has('assunto'))
        {
            $questao->update(request()->all());

            $status = true;
            if($questao->modelo == 'O')
                $status = $this->syncAlternativas($questao_id);
            
            $this->syncCategorias($questao_id);
            $this->syncArquivo($questao, 'arquivo', 'arq');

            if($status == true)
                return back()->with('success', 'Questão atualizada com êxito.');
            else
                return back()->with('danger', 'Alternativa Correta não selecionada.');
        }

        return view('admin.questao.editar', compact('questao','alternativas','categorias','categoriasSelected') );
    }

    public function syncArquivo($questao, $tipo, $inputFileName)
    {
        if(request('remover_arquivo'))
        {
            $path = str_replace(\App\Models\Util::getLinkStorage(), '', $questao->arquivo);

            Storage::delete($path);

            $questao->update(['arquivo' => null]);
        }

        $file = request()->file($inputFileName);
     
        if($file)
        {
            $path = str_replace(\App\Models\Util::getLinkStorage(), '', $questao->$tipo);
            Storage::delete($path);
            
            $diretorio = 'plataformas/'.\Auth::user()->plataforma_id.'/questoes/'.$questao->id;
            $filename = $tipo.'.'.$file->getClientOriginalExtension();

            $path = Storage::putFileAs($diretorio, $file, $filename, 'public');
            $linkStorage = \App\Models\Util::getLinkStorage();
            $caminho = $linkStorage . $path;

            $questao->update([$tipo => $caminho]);
        }
    }

    public function syncAlternativas($id)
    {            
        \App\Models\QuestaoAlternativa::where('questao_id', $id)->delete();

        $alternativas = array_combine(request('alternativas') ?? array(), request('resolucoes') ?? array());

        if(count($alternativas) <= 0)
            return;

        foreach($alternativas as $alternativa => $correta)
        {   
            $request = ['questao_id' => $id, 'alternativa' => $alternativa, 'correta' => $correta];

            \App\Models\QuestaoAlternativa::create($request);
        }

        if(in_array('S', request('resolucoes')))
            return true;
        else
            return false;
    }

    public function syncCategorias($id)
    {
        \App\Models\QuestaoCategoria::where('questao_id', $id)->whereNotIn('id', request('categorias') ?? array())->delete();

        if(request('categorias') == null)
            return;

        foreach(request('categorias') as $categoria_id)
        {
            $countDuplicate = \App\Models\QuestaoCategoria::plataforma()->where('categoria_id', $categoria_id)->where('questao_id', $id)->count();
            
            if($countDuplicate <= 0)
                \App\Models\QuestaoCategoria::create(['categoria_id' => $categoria_id, 'questao_id' => $id]);
        }
    }

}