@extends('layouts.admin.master')

@section('content')
    
    @livewire('admin.avaliacao.nao-finalizaram', ['avaliacao_id' => $avaliacao_id])

@endsection

@push('scripts')

@if(isset($_GET['alterar_nota_aluno_id']))
	<script>
		var myModal = new bootstrap.Modal(document.getElementById("ModalAlterarNota"), {});
		document.onreadystatechange = function () {
		  myModal.show();
		};
	</script>
@endif

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>
<script>
	window.addEventListener('dissmiss-modal', event => {
		$(".modal").modal('hide');
	});
</script>

@endpush