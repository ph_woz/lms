<?php

namespace App\Http\Livewire\Admin\ControleFinanceiro\FluxoCaixa;

use Livewire\Component;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;
use App\Models\Unidade;
use App\Models\Util;
use App\Models\User;
use App\Models\FluxoCaixa;
use App\Models\Trilha;
use App\Models\Turma;
use App\Models\Log;

class Lista extends Component
{
    public $buscar;
    public $filter_cliente_id;
    public $vendedor_id;
    public $produto_id;
	public $ano;
	public $mes;
    public $unidade_id;
    public $status;


    // ADD ENTRADA
    public $cliente_id;
    public $cliente;
    public $add_entrada_cliente_selected_nome;
    public $add_entrada_vendedor_id;
    public $add_entrada_vendedor_nome;
    public $add_entrada_trilha_id;
    public $add_entrada_turma_id;
    public $add_entrada_produto_id;
    public $add_entrada_produto_nome;
    public $add_entrada_unidade_id;
    public $add_entrada_unidade_nome;
    public $add_entrada_valor_total;
    public $add_entrada_valor;
    public $add_entrada_n_parcelas;
    public $add_entrada_link;
    public $add_entrada_forma_pagamento;
    public $add_entrada_valueDisplayParcelas = 'none';
    public $add_entrada_data_recebimento;
    public $add_entrada_data_vencimento;
    public $add_entrada_data_estorno;
    public $add_entrada_status = 0;
    public $add_entrada_obs;

    public $add_entrada_n_parcela        = [];
    public $add_entrada_parcelas         = [];
    public $add_entrada_datas_vencimento = [];
    public $add_entrada_valores          = [];
    public $add_entrada_formas_pagamento = [];
    public $add_entrada_links            = [];

    // EDITAR ENTRADA
    public $editar_entrada_cliente_selected_nome;
    public $editar_entrada_vendedor_id;
    public $editar_entrada_vendedor_nome;
    public $editar_entrada_trilha_id;
    public $editar_entrada_turma_id;
    public $editar_entrada_produto_id;
    public $editar_entrada_produto_nome;
    public $editar_entrada_unidade_id;
    public $editar_entrada_unidade_nome;
    public $editar_entrada_valor_total;
    public $editar_entrada_valor;
    public $editar_entrada_n_parcelas;
    public $editar_entrada_link;
    public $editar_entrada_forma_pagamento;
    public $editar_entrada_valueDisplayParcelas = 'none';
    public $editar_entrada_data_recebimento;
    public $editar_entrada_data_vencimento;
    public $editar_entrada_id_para_acao;
    public $editar_entrada_data_estorno;
    public $editar_entrada_status = 0;
    public $editar_entrada_obs;
    public $editar_entrada;
    public $editar_entrada_id;

    public $editar_entrada_n_parcela        = [];
    public $editar_entrada_parcelas         = [];
    public $editar_entrada_datas_vencimento = [];
    public $editar_entrada_valores          = [];
    public $editar_entrada_formas_pagamento = [];
    public $editar_entrada_links            = [];
    public $editar_entrada_ids              = [];
    public $editar_entrada_registro_ids     = [];


    // ADD SAÍDA
    public $add_saida_ref_saida;
    public $add_saida_unidade_id;
    public $add_saida_unidade_nome;
    public $add_saida_valor_total;
    public $add_saida_valor;
    public $add_saida_n_parcelas;
    public $add_saida_link;
    public $add_saida_forma_pagamento;
    public $add_saida_valueDisplayParcelas = 'none';
    public $add_saida_data_recebimento;
    public $add_saida_data_vencimento;
    public $add_saida_status = 0;
    public $add_saida_obs;

    public $add_saida_n_parcela        = [];
    public $add_saida_parcelas         = [];
    public $add_saida_datas_vencimento = [];
    public $add_saida_valores          = [];
    public $add_saida_formas_pagamento = [];
    public $add_saida_links            = [];

    // EDITAR SAÍDA
    public $editar_saida;
    public $editar_saida_ref_saida;
    public $editar_saida_unidade_id;
    public $editar_saida_unidade_nome;
    public $editar_saida_valor_total;
    public $editar_saida_valor;
    public $editar_saida_n_parcelas;
    public $editar_saida_link;
    public $editar_saida_forma_pagamento;
    public $editar_saida_valueDisplayParcelas = 'none';
    public $editar_saida_data_recebimento;
    public $editar_saida_data_vencimento;
    public $editar_saida_status = 0;
    public $editar_saida_obs;
    public $editar_saida_id_para_acao;

    public $editar_saida_ids              = [];
    public $editar_saida_registro_ids     = [];
    public $editar_saida_n_parcela        = [];
    public $editar_saida_parcelas         = [];
    public $editar_saida_datas_vencimento = [];
    public $editar_saida_valores          = [];
    public $editar_saida_formas_pagamento = [];
    public $editar_saida_links            = [];

    // INFO ENTRADA
    public $view_entrada = true;
    public $entrada_id;

    // INFO SAIDA
    public $view_saida = true;
    public $saida_id;


    // DELETE
    public $password_to_delete;


    protected $queryString = ['buscar','vendedor_id','produto_id','unidade_id','mes','ano','filter_cliente_id','cliente_id','entrada_id','status'];


    public function mount()
    {
        $this->ano = $_GET['ano'] ?? date('Y');
        $this->mes = $_GET['mes'] ?? date('m');

        $this->cliente_id = $_GET['cliente_id'] ?? null;

        $this->add_entrada_vendedor_id = \Auth::id(); 
        $this->add_entrada_vendedor_nome = \Auth::user()->nome;
        $this->add_entrada_n_parcelas = 1;
        $this->add_entrada_data_vencimento = date('Y-m-d');

        $this->add_saida_n_parcelas = 1;
        $this->add_saida_data_vencimento = date('Y-m-d');

        if(isset($_GET['entrada_id']) && $_GET['entrada_id'] != null)
        {
            $this->view_entrada = FluxoCaixa::plataforma()->where('id', $_GET['entrada_id'])->first() ?? false;
        }

        if(isset($_GET['saida_id']) && $_GET['saida_id'] != null)
        {
            $this->view_saida = FluxoCaixa::plataforma()->where('id', $_GET['saida_id'])->first() ?? false;
        }

        if(isset($_GET['editar_entrada_id']) && $_GET['editar_entrada_id'] != null)
        {
            $this->editar_entrada = FluxoCaixa::plataforma()->where('id', $_GET['editar_entrada_id'])->first() ?? false;

            if(isset($this->editar_entrada) && $this->editar_entrada !== false)
            {
                $this->editar_entrada_cliente_selected_nome = $this->editar_entrada->cliente_nome;
                $this->editar_entrada_vendedor_id = $this->editar_entrada->vendedor_id;
                $this->editar_entrada_vendedor_nome = $this->editar_entrada->vendedor_nome;
                $this->editar_entrada_produto_id = $this->editar_entrada->produto_id;
                $this->editar_entrada_produto_nome = $this->editar_entrada->produto_nome;
                $this->editar_entrada_unidade_id = $this->editar_entrada->unidade_id;
                $this->editar_entrada_unidade_nome = $this->editar_entrada->unidade_nome;
                $this->editar_entrada_valor = $this->editar_entrada->valor;
                $this->editar_entrada_valor_total = $this->editar_entrada->valor_total;
                $this->editar_entrada_n_parcelas = $this->editar_entrada->n_parcelas;
                $this->editar_entrada_link = $this->editar_entrada->link;
                $this->editar_entrada_forma_pagamento = $this->editar_entrada->forma_pagamento;
                $this->editar_entrada_data_recebimento = $this->editar_entrada->data_recebimento;
                $this->editar_entrada_data_vencimento = $this->editar_entrada->data_vencimento;
                $this->editar_entrada_data_estorno = $this->editar_entrada->data_estorno;
                $this->editar_entrada_status = $this->editar_entrada->status;
                $this->editar_entrada_obs = $this->editar_entrada->obs;

                $this->editar_entrada_trilha_id = $this->editar_entrada->trilha_id;
                $this->editar_entrada_turma_id = $this->editar_entrada->turma_id;
                
                if($this->editar_entrada->trilha_id)
                {
                    $this->editar_entrada_produto_id = 'trilha_id'.$this->editar_entrada->trilha_id;
                
                } else {
                
                    $this->editar_entrada_produto_id = 'turma_id'.$this->editar_entrada->turma_id;
                }

                if($this->editar_entrada->n_parcelas >= 2)
                {            
                    $value = intval($this->editar_entrada->n_parcelas);

                    if($value >= 2)
                    {
                        $this->editar_entrada_valueDisplayParcelas = 'block';

                        if($this->editar_entrada->registro_id != null)
                        {
                            $this->editar_entrada_parcelas = FluxoCaixa::plataforma()->where('id', $this->editar_entrada->registro_id)->orWhere('registro_id', $this->editar_entrada->registro_id)->orderBy('data_vencimento')->get();
                        
                        } else {

                            $this->editar_entrada_parcelas = FluxoCaixa::plataforma()->where('registro_id', $this->editar_entrada->id)->orderBy('data_vencimento')->get();                            
                        }
                                                
                        $i = -1;
                        foreach($this->editar_entrada_parcelas as $parcela)
                        {
                            $i++;

                            $this->editar_entrada_n_parcela[$i]        = $parcela->n_parcela;
                            $this->editar_entrada_datas_vencimento[$i] = $parcela->data_vencimento;
                            $this->editar_entrada_valores[$i]          = $parcela->valor;
                            $this->editar_entrada_formas_pagamento[$i] = $parcela->forma_pagamento;
                            $this->editar_entrada_links[$i]            = $parcela->link;
                            $this->editar_entrada_registro_ids[$i]     = $parcela->registro_id;
                            $this->editar_entrada_ids[$i]              = $parcela->id;
                        }

                    } else {

                        $this->editar_entrada_valueDisplayParcelas = 'none';
                    }
                }

            }
        }

        if(isset($_GET['editar_saida_id']) && $_GET['editar_saida_id'] != null)
        {
            $this->editar_saida = FluxoCaixa::plataforma()->where('id', $_GET['editar_saida_id'])->first() ?? false;

            if(isset($this->editar_saida) && $this->editar_saida !== false)
            {
                $this->editar_saida_ref_saida = $this->editar_saida->ref_saida;
                $this->editar_saida_unidade_id = $this->editar_saida->unidade_id;
                $this->editar_saida_unidade_nome = $this->editar_saida->unidade_nome;
                $this->editar_saida_valor = $this->editar_saida->valor;
                $this->editar_saida_valor_total = $this->editar_saida->valor_total;
                $this->editar_saida_n_parcelas = $this->editar_saida->n_parcelas;
                $this->editar_saida_link = $this->editar_saida->link;
                $this->editar_saida_forma_pagamento = $this->editar_saida->forma_pagamento;
                $this->editar_saida_data_recebimento = $this->editar_saida->data_recebimento;
                $this->editar_saida_data_vencimento = $this->editar_saida->data_vencimento;
                $this->editar_saida_status = $this->editar_saida->status;
                $this->editar_saida_obs = $this->editar_saida->obs;

                if($this->editar_saida->n_parcelas >= 2)
                {            
                    $value = intval($this->editar_saida->n_parcelas);

                    if($value >= 2)
                    {
                        $this->editar_saida_valueDisplayParcelas = 'block';

                        if($this->editar_saida->registro_id != null)
                        {
                            $this->editar_saida_parcelas = FluxoCaixa::plataforma()->where('id', $this->editar_saida->registro_id)->orWhere('registro_id', $this->editar_saida->registro_id)->orderBy('data_vencimento')->get();
                        
                        } else {

                            $this->editar_saida_parcelas = FluxoCaixa::plataforma()->where('registro_id', $this->editar_saida->id)->orderBy('data_vencimento')->get();                            
                        }
                                                
                        $i = -1;
                        foreach($this->editar_saida_parcelas as $parcela)
                        {
                            $i++;

                            $this->editar_saida_n_parcela[$i]        = $parcela->n_parcela;
                            $this->editar_saida_datas_vencimento[$i] = $parcela->data_vencimento;
                            $this->editar_saida_valores[$i]          = $parcela->valor;
                            $this->editar_saida_links[$i]            = $parcela->link;
                            $this->editar_saida_formas_pagamento[$i] = $parcela->forma_pagamento;
                            $this->editar_saida_registro_ids[$i]     = $parcela->registro_id;
                            $this->editar_saida_ids[$i]              = $parcela->id;
                        }

                    } else {

                        $this->editar_saida_valueDisplayParcelas = 'none';
                    }
                }

            }
        }
        if($this->cliente_id != null)
        {
            $this->cliente = User::plataforma()->aluno()->where('id', $this->cliente_id)->select('nome','email','cpf','celular')->first();

            if(isset($this->cliente))
            {
                $this->add_entrada_cliente_selected_nome = $this->cliente->nome . ' / ' . $this->cliente->email;

                if(isset($this->cliente->cpf))
                {
                    $this->add_entrada_cliente_selected_nome = $this->add_entrada_cliente_selected_nome . ' / ' . $this->cliente->cpf;
                }
                if(isset($this->cliente->celular))
                {
                    $this->add_entrada_cliente_selected_nome = $this->add_entrada_cliente_selected_nome . ' / ' . $this->cliente->celular;
                }
            }
        }

        if(\Auth::user()->restringir_unidade == 'S' && \Auth::user()->unidade_id != null)
        {
            $this->unidade_id = \Auth::user()->unidade_id;
        }
    }

    public function render()
    {
        if($this->cliente_id != null && is_null($this->cliente))
        {
            $erro = 'Aluno(a) não encontrado.';
            return view('admin.page-whoops', ['erro' => $erro]);
        }

    	$meses = Util::getMeses();

        $entradas = FluxoCaixa::plataforma()->where('tipo','entrada');
            
        $saidas = FluxoCaixa::select('fluxo_caixa.*')
            ->where('fluxo_caixa.plataforma_id', session('plataforma_id') ?? \Auth()->user()->plataforma_id)
            ->where('fluxo_caixa.tipo','saida');


        if($this->buscar)
        {
            $entradas->where(function ($query)
            {
                $query
                    ->orWhere('users.nome', 'like', '%'.$this->buscar.'%')
                    ->orWhere('users.email', 'like', '%'.$this->buscar.'%')
                    ->orWhere('users.cpf', 'like', '%'.$this->buscar.'%')
                    ->orWhere('fluxo_caixa.cliente_nome', 'like', '%'.$this->buscar.'%')
                    ->orWhere('fluxo_caixa.valor', 'like', '%'.$this->buscar.'%')
                    ->orWhere('fluxo_caixa.link', 'like', '%'.$this->buscar.'%')
                    ->orWhere('fluxo_caixa.forma_pagamento', 'like', '%'.$this->buscar.'%');
            });

            $saidas->where(function ($query)
            {
                $query
                    ->orWhere('fluxo_caixa.ref_saida', 'like', '%'.$this->buscar.'%')
                    ->orWhere('fluxo_caixa.valor', 'like', '%'.$this->buscar.'%')
                    ->orWhere('fluxo_caixa.link', 'like', '%'.$this->buscar.'%')
                    ->orWhere('fluxo_caixa.forma_pagamento', 'like', '%'.$this->buscar.'%');
            });
        }

        if($this->ano)
        {
            $entradas = $entradas->whereYear('fluxo_caixa.data_vencimento', $this->ano);
            $saidas   = $saidas->whereYear('fluxo_caixa.data_vencimento', $this->ano);
        }
        
        if($this->mes)
        {
            $entradas = $entradas->whereMonth('fluxo_caixa.data_vencimento', $this->mes);
            $saidas   = $saidas->whereMonth('fluxo_caixa.data_vencimento', $this->mes);
        }

        if($this->filter_cliente_id)
        {
            $entradas = $entradas->where('fluxo_caixa.cliente_id', $this->filter_cliente_id);
        }

        if($this->vendedor_id)
        {
            $entradas = $entradas->where('fluxo_caixa.vendedor_id', $this->vendedor_id);
        }

        if($this->produto_id)
        {
            $checkTrilha = Str::contains($this->produto_id, 'trilha_id');

            if($checkTrilha == true)
            {
                $value = str_replace('trilha_id', '', $this->produto_id);

                $entradas = $entradas->where('fluxo_caixa.trilha_id', $value);

            } else {

                $value = str_replace('turma_id', '', $this->produto_id);

                $entradas = $entradas->where('fluxo_caixa.turma_id', $value);
            }
        }

        if($this->status)
        {
            if($this->status == 'a-receber')
            {
                $entradas = $entradas->where('fluxo_caixa.status', 0)->whereDate('fluxo_caixa.data_vencimento','>=', date('Y-m-d'));
            }

            if($this->status == 'recebidos-e-pagos')
            {
                $entradas = $entradas->where('fluxo_caixa.status', 1);
                $saidas   = $saidas->where('fluxo_caixa.status', 1);
            }

            if($this->status == 'inadimplentes')
            {
                $entradas = $entradas->where('fluxo_caixa.status', 0)->whereDate('fluxo_caixa.data_vencimento','<', date('Y-m-d'));
                $saidas   = $saidas->where('fluxo_caixa.status', 0)->whereDate('fluxo_caixa.data_vencimento','<', date('Y-m-d'));
            }

            if($this->status == 'estornados')
            {
                $entradas = $entradas->where('fluxo_caixa.status', 2);
                $saidas   = $saidas->where('fluxo_caixa.status', 2);
            }
        }

        $inadimplentes = clone $entradas;
        $inadimplentes = $inadimplentes->where('fluxo_caixa.status', 0)->whereDate('fluxo_caixa.data_vencimento','<', date('Y-m-d'))->select('valor')->get()->pluck('valor')->toArray();

        $despesasEmAtraso = clone $saidas;
        $countDespesasEmAtraso = $despesasEmAtraso->where('fluxo_caixa.status', 0)->whereDate('fluxo_caixa.data_vencimento','<', date('Y-m-d'))->count();

        $entradas = $entradas->orderBy('fluxo_caixa.id','desc')->get();
        $saidas   = $saidas->orderBy('fluxo_caixa.id','desc')->get();

        $unidades = Unidade::plataforma()->ativo()->orderBy('nome')->get();

        if(\Auth::user()->restringir_unidade == 'S' && \Auth::user()->unidade_id != null)
        {
            $vendedores = User::plataforma()->colaborador()->where('unidade_id', $this->unidade_id)->ativo()->orderBy('nome')->get();
            $trilhas    = Trilha::plataforma()->ativo()->where('unidade_id', $this->unidade_id)->orderBy('referencia')->get();
            $turmas     = Turma::plataforma()->ativo()->where('unidade_id', $this->unidade_id)->orderBy('nome')->get();
        
        } else {

            $vendedores = User::plataforma()->colaborador()->ativo()->orderBy('nome')->get();
            $trilhas    = Trilha::plataforma()->ativo()->select('id','referencia')->orderBy('referencia')->get()->pluck('id','referencia')->toArray();
            $turmas     = Turma::plataforma()->ativo()->select('id','nome')->orderBy('nome')->get()->pluck('id','nome')->toArray();
        }


        return view('livewire.admin.controle-financeiro.fluxo-caixa.lista', 
        	[
        		'meses'         => $meses,
                'unidades'      => $unidades,
                'trilhas'       => $trilhas,
                'turmas'        => $turmas,
                'vendedores'    => $vendedores,
                'entradas'      => $entradas,
                'saidas'        => $saidas,
                'inadimplentes' => $inadimplentes,
                'countDespesasEmAtraso' => $countDespesasEmAtraso,
        	]
        );
    }

    public function gerarParcelasAddEntrada($value)
    {
        $this->add_entrada_parcelas = [];

        $value = intval($value);

        if($value >= 2)
        {
            $this->add_entrada_valueDisplayParcelas = 'block';

            $this->add_entrada_valor_total = $this->add_entrada_valor;

            $valor = $this->add_entrada_valor;
            $valor = str_replace('.', '', $valor);
            $valor = str_replace(',', '.', $valor);
            $valor = floatval($valor);
            $valor = round($valor, 2);
            $valor = $valor / $value;
            $valor = number_format($valor , 2,",",".");


            for ($i=1; $i < $value; $i++)
            { 
                $this->add_entrada_parcelas[$i] = null;
                $this->add_entrada_valores[$i]  = $valor;
                $this->add_entrada_valor = $valor;

                $this->add_entrada_formas_pagamento[$i] = $this->add_entrada_forma_pagamento;
                $this->add_entrada_links[$i] = $this->add_entrada_link;
                    
                $this->add_entrada_n_parcela[$i] = $i+1;

                $interval = '+'.$i.' month';
                $this->add_entrada_datas_vencimento[$i] = date('Y-m-d', strtotime ($interval , strtotime ($this->add_entrada_data_vencimento) )) ;
            }

        } else {

            $this->add_entrada_valueDisplayParcelas = 'none';
        }
    }

    public function gerarParcelasEditarEntrada($value)
    {
        $this->editar_entrada_parcelas = [];

        $value = intval($value);

        if($value >= 2)
        {
            $this->editar_entrada_valueDisplayParcelas = 'block';

            $valor = $this->editar_entrada_valor;
            $valor = str_replace('.', '', $valor);
            $valor = str_replace(',', '.', $valor);
            $valor = floatval($valor);
            $valor = round($valor, 2);
            $valor = $valor / $value;
            $valor = number_format($valor , 2,",",".");


            for ($i=1; $i < $value; $i++)
            { 
                $this->editar_entrada_parcelas[$i] = null;
                $this->editar_entrada_valores[$i]  = $valor;
                $this->editar_entrada_valor = $valor;

                $this->editar_entrada_formas_pagamento[$i] = $this->editar_entrada_forma_pagamento;
                $this->editar_entrada_links[$i] = $this->editar_entrada_link;
                    
                $this->editar_entrada_n_parcela[$i] = $i+1;

                $interval = '+'.$i.' month';
                $this->editar_entrada_datas_vencimento[$i] = date('Y-m-d', strtotime ($interval , strtotime ($this->editar_entrada_data_vencimento) )) ;
            }

        } else {

            $this->editar_entrada_valueDisplayParcelas = 'none';
        }
    }

    public function gerarParcelasAddSaida($value)
    {
        $this->add_saida_parcelas = [];

        $value = intval($value);

        if($value >= 2)
        {
            $this->add_saida_valueDisplayParcelas = 'block';

            $this->add_saida_valor_total = $this->add_saida_valor;

            $valor = $this->add_saida_valor;
            $valor = str_replace('.', '', $valor);
            $valor = str_replace(',', '.', $valor);
            $valor = floatval($valor);
            $valor = round($valor, 2);
            $valor = $valor / $value;
            $valor = number_format($valor , 2,",",".");


            for ($i=1; $i < $value; $i++)
            { 
                $this->add_saida_parcelas[$i] = null;
                $this->add_saida_valores[$i]  = $valor;
                $this->add_saida_valor = $valor;

                $this->add_saida_formas_pagamento[$i] = $this->add_saida_forma_pagamento;
                $this->add_saida_links[$i] = $this->add_saida_link;
                    
                $this->add_saida_n_parcela[$i] = $i+1;

                $interval = '+'.$i.' month';
                $this->add_saida_datas_vencimento[$i] = date('Y-m-d', strtotime ($interval , strtotime ($this->add_saida_data_vencimento) )) ;
            }

        } else {

            $this->add_saida_valueDisplayParcelas = 'none';
        }
    }

    public function gerarParcelasEditarSaida($value)
    {
        $this->editar_saida_parcelas = [];

        $value = intval($value);

        if($value >= 2)
        {
            $this->editar_saida_valueDisplayParcelas = 'block';

            $valor = $this->editar_saida_valor;
            $valor = str_replace('.', '', $valor);
            $valor = str_replace(',', '.', $valor);
            $valor = floatval($valor);
            $valor = round($valor, 2);
            $valor = $valor / $value;
            $valor = number_format($valor , 2,",",".");


            for ($i=1; $i < $value; $i++)
            { 
                $this->editar_saida_parcelas[$i] = null;
                $this->editar_saida_valores[$i]  = $valor;
                $this->editar_saida_valor = $valor;

                $this->editar_saida_formas_pagamento[$i] = $this->editar_saida_forma_pagamento;
                $this->editar_saida_links[$i] = $this->editar_saida_link;
                    
                $this->editar_saida_n_parcela[$i] = $i+1;

                $interval = '+'.$i.' month';
                $this->editar_saida_datas_vencimento[$i] = date('Y-m-d', strtotime ($interval , strtotime ($this->editar_saida_data_vencimento) )) ;
            }

        } else {

            $this->editar_saida_valueDisplayParcelas = 'none';
        }
    }

    public function addEntrada()
    {
        $entrada = FluxoCaixa::create([
            'cadastrante_id'   => \Auth::id(),
            'cadastrante_nome' => \Auth::user()->nome,
            'vendedor_id'      => $this->add_entrada_vendedor_id,
            'cliente_id'       => $this->cliente_id,
            'cliente_nome'     => $this->add_entrada_cliente_selected_nome,
            'trilha_id'        => $this->add_entrada_trilha_id,
            'turma_id'         => $this->add_entrada_turma_id,
            'produto_nome'     => $this->add_entrada_produto_nome,
            'unidade_nome'     => $this->add_entrada_unidade_nome,
            'vendedor_nome'    => $this->add_entrada_vendedor_nome,
            'unidade_id'       => $this->add_entrada_unidade_id,
            'tipo'             => 'entrada',
            'valor'            => $this->add_entrada_valor,
            'forma_pagamento'  => $this->add_entrada_forma_pagamento,
            'n_parcela'        => 1,
            'n_parcelas'       => $this->add_entrada_n_parcelas,
            'link'             => $this->add_entrada_link,
            'data_recebimento' => $this->add_entrada_data_recebimento,
            'data_vencimento'  => $this->add_entrada_data_vencimento,
            'data_estorno'     => $this->add_entrada_data_estorno,
            'status'           => $this->add_entrada_status,
            'obs'              => $this->add_entrada_obs,
        ]);
    

        $lineParcelas = array_map(function ($add_entrada_n_parcela, $add_entrada_data_vencimento, $add_entrada_valor, $add_entrada_forma_pagamento, $add_entrada_link)
        {
          return array_combine(
            ['add_entrada_n_parcela','add_entrada_data_vencimento','add_entrada_valor', 'add_entrada_forma_pagamento', 'add_entrada_link'],
            [$add_entrada_n_parcela, $add_entrada_data_vencimento, $add_entrada_valor, $add_entrada_forma_pagamento, $add_entrada_link]
          );
        }, $this->add_entrada_n_parcela, $this->add_entrada_datas_vencimento, $this->add_entrada_valores, $this->add_entrada_formas_pagamento, $this->add_entrada_links);

        $valores = [];
        $valores[] = $this->add_entrada_valor;

        foreach($lineParcelas as $parcela)
        {
            $valores[] = $parcela['add_entrada_valor'];

            FluxoCaixa::create([
                'cadastrante_id'   => \Auth::id(),
                'cadastrante_nome' => \Auth::user()->nome,
                'vendedor_id'      => $this->add_entrada_vendedor_id,
                'cliente_id'       => $this->cliente_id,
                'cliente_nome'     => $this->add_entrada_cliente_selected_nome,
                'trilha_id'        => $this->add_entrada_trilha_id,
                'turma_id'         => $this->add_entrada_turma_id,
                'produto_nome'     => $this->add_entrada_produto_nome,
                'unidade_nome'     => $this->add_entrada_unidade_nome,
                'vendedor_nome'    => $this->add_entrada_vendedor_nome,
                'unidade_id'       => $this->add_entrada_unidade_id,
                'registro_id'      => $entrada->id,
                'tipo'             => 'entrada',
                'n_parcelas'       => $this->add_entrada_n_parcelas,
                'link'             => $this->add_entrada_link,
                'n_parcela'        => $parcela['add_entrada_n_parcela'],
                'valor'            => $parcela['add_entrada_valor'],
                'forma_pagamento'  => $parcela['add_entrada_forma_pagamento'],
                'link'             => $parcela['add_entrada_link'],
                'data_vencimento'  => $parcela['add_entrada_data_vencimento'],
            ]);
        }

        $valor_total = FluxoCaixa::getValorRealInArray($valores);
        
        $entrada->update(['valor_total' => $valor_total]);

        FluxoCaixa::plataforma()->where('registro_id', $entrada->id)->update(['valor_total' => $valor_total]);

        $this->resetFieldsAddEntrada();

        session()->flash('success-livewire', 'Entrada cadastrada com êxito.');
        $this->dispatchBrowserEvent('dissmiss-modal');
        $this->dispatchBrowserEvent('toast');

    }


    public function editarEntrada()
    {
        $this->editar_entrada->update([
            'editor_id'        => \Auth::id(),
            'editor_nome'      => \Auth::user()->nome,
            'vendedor_id'      => $this->editar_entrada_vendedor_id,
            'cliente_nome'     => $this->editar_entrada_cliente_selected_nome,
            'trilha_id'        => $this->editar_entrada_trilha_id,
            'turma_id'         => $this->editar_entrada_turma_id,
            'produto_nome'     => $this->editar_entrada_produto_nome,
            'unidade_nome'     => $this->editar_entrada_unidade_nome,
            'vendedor_nome'    => $this->editar_entrada_vendedor_nome,
            'unidade_id'       => $this->editar_entrada_unidade_id,
            'tipo'             => 'entrada',
            'valor'            => $this->editar_entrada_valor,
            'forma_pagamento'  => $this->editar_entrada_forma_pagamento,
            'link'             => $this->editar_entrada_link,
            'n_parcela'        => 1,
            'n_parcelas'       => $this->editar_entrada_n_parcelas,
            'data_recebimento' => $this->editar_entrada_data_recebimento,
            'data_vencimento'  => $this->editar_entrada_data_vencimento,
            'data_estorno'     => $this->editar_entrada_data_estorno,
            'status'           => $this->editar_entrada_status,
            'obs'              => $this->editar_entrada_obs,
        ]);
    
        
        $lineParcelas = array_map(function ($editar_entrada_n_parcela, $editar_entrada_data_vencimento, $editar_entrada_valor, $editar_entrada_forma_pagamento, $editar_entrada_link, $editar_entrada_registro_id, $editar_entrada_id)
        {
          return array_combine(
            ['editar_entrada_n_parcela','editar_entrada_data_vencimento','editar_entrada_valor', 'editar_entrada_forma_pagamento', 'editar_entrada_link', 'editar_entrada_registro_id','editar_entrada_id'],
            [$editar_entrada_n_parcela, $editar_entrada_data_vencimento, $editar_entrada_valor, $editar_entrada_forma_pagamento, $editar_entrada_link, $editar_entrada_registro_id, $editar_entrada_id]
          );
        }, $this->editar_entrada_n_parcela, $this->editar_entrada_datas_vencimento, $this->editar_entrada_valores, $this->editar_entrada_formas_pagamento, $this->editar_entrada_links, $this->editar_entrada_registro_ids, $this->editar_entrada_ids);


        $parcelasIds = [];
        $parcelasIds[] = $this->editar_entrada->id;

        foreach($lineParcelas as $parcela)
        {
            $checkEntrada = FluxoCaixa::plataforma()->where('id', $parcela['editar_entrada_id'] ?? 0)->first();

            if($checkEntrada)
            {
                $checkEntrada->update([
                    'editor_id'        => \Auth::id(),
                    'editor_nome'      => \Auth::user()->nome,
                    'vendedor_id'      => $this->editar_entrada_vendedor_id,
                    'cliente_nome'     => $this->editar_entrada_cliente_selected_nome,
                    'trilha_id'        => $this->editar_entrada_trilha_id,
                    'turma_id'         => $this->editar_entrada_turma_id,
                    'produto_nome'     => $this->editar_entrada_produto_nome,
                    'unidade_nome'     => $this->editar_entrada_unidade_nome,
                    'vendedor_nome'    => $this->editar_entrada_vendedor_nome,
                    'unidade_id'       => $this->editar_entrada_unidade_id,
                    'n_parcelas'       => $this->editar_entrada_n_parcelas,
                    'n_parcela'        => $parcela['editar_entrada_n_parcela'],
                    'valor'            => $parcela['editar_entrada_valor'],
                    'forma_pagamento'  => $parcela['editar_entrada_forma_pagamento'],
                    'link'             => $parcela['editar_entrada_link'],
                    'data_vencimento'  => $parcela['editar_entrada_data_vencimento'],
                ]);

            } else {

                $checkEntrada = FluxoCaixa::create([
                    'cadastrante_id'   => \Auth::id(),
                    'cadastrante_nome' => \Auth::user()->nome,
                    'vendedor_id'      => $this->editar_entrada_vendedor_id,
                    'cliente_id'       => $this->editar_entrada->cliente_id,
                    'cliente_nome'     => $this->editar_entrada_cliente_selected_nome,
                    'trilha_id'        => $this->editar_entrada_trilha_id,
                    'turma_id'         => $this->editar_entrada_turma_id,
                    'produto_nome'     => $this->editar_entrada_produto_nome,
                    'unidade_nome'     => $this->editar_entrada_unidade_nome,
                    'vendedor_nome'    => $this->editar_entrada_vendedor_nome,
                    'unidade_id'       => $this->editar_entrada_unidade_id,
                    'tipo'             => 'entrada',
                    'n_parcelas'       => $this->editar_entrada_n_parcelas,
                    'registro_id'      => $this->editar_entrada->registro_id ?? $this->editar_entrada->id,
                    'n_parcela'        => $parcela['editar_entrada_n_parcela'],
                    'valor'            => $parcela['editar_entrada_valor'],
                    'forma_pagamento'  => $parcela['editar_entrada_forma_pagamento'],
                    'link'             => $parcela['editar_entrada_link'],
                    'data_vencimento'  => $parcela['editar_entrada_data_vencimento'],  
                ]);

            }

            FluxoCaixa::plataforma()->where('cliente_id', $this->editar_entrada->cliente_id)->where('n_parcela', $checkEntrada->n_parcela)->where('registro_id', $checkEntrada->registro_id)->where('id','<>', $checkEntrada->id)->delete();

            $parcelasIds[] = $checkEntrada->id;
        }

        $valores = FluxoCaixa::plataforma()->where('cliente_id', $this->editar_entrada->cliente_id)->whereIn('id', $parcelasIds)->select('valor')->get()->pluck('valor')->toArray();
        $valor_total = FluxoCaixa::getValorRealInArray($valores);
        FluxoCaixa::plataforma()->where('cliente_id', $this->editar_entrada->cliente_id)->whereIn('id', $parcelasIds)->update(['valor_total' => $valor_total]);

        session()->flash('success-livewire', 'Entrada atualizada com êxito.');
        $this->dispatchBrowserEvent('dissmiss-modal');
        $this->dispatchBrowserEvent('toast');

    }


    public function addSaida()
    {
        $saida = FluxoCaixa::create([
            'cadastrante_id'   => \Auth::id(),
            'cadastrante_nome' => \Auth::user()->nome,
            'ref_saida'        => $this->add_saida_ref_saida,
            'unidade_id'       => $this->add_saida_unidade_id,
            'tipo'             => 'saida',
            'valor'            => $this->add_saida_valor,
            'forma_pagamento'  => $this->add_saida_forma_pagamento,
            'link'             => $this->add_saida_link,
            'n_parcela'        => 1,
            'n_parcelas'       => $this->add_saida_n_parcelas,
            'data_recebimento' => $this->add_saida_data_recebimento,
            'data_vencimento'  => $this->add_saida_data_vencimento,
            'status'           => $this->add_saida_status,
            'obs'              => $this->add_saida_obs,
        ]);
    

        $lineParcelas = array_map(function ($add_saida_n_parcela, $add_saida_data_vencimento, $add_saida_valor, $add_saida_forma_pagamento, $add_saida_link)
        {
          return array_combine(
            ['add_saida_n_parcela','add_saida_data_vencimento','add_saida_valor', 'add_saida_forma_pagamento', 'add_saida_link'],
            [$add_saida_n_parcela, $add_saida_data_vencimento, $add_saida_valor, $add_saida_forma_pagamento, $add_saida_link]
          );
        }, $this->add_saida_n_parcela, $this->add_saida_datas_vencimento, $this->add_saida_valores, $this->add_saida_formas_pagamento, $this->add_saida_links);

        $valores = [];
        $valores[] = $this->add_saida_valor;

        foreach($lineParcelas as $parcela)
        {
            $valores[] = $parcela['add_saida_valor'];

            FluxoCaixa::create([
                'cadastrante_id'   => \Auth::id(),
                'cadastrante_nome' => \Auth::user()->nome,
                'ref_saida'        => $this->add_saida_ref_saida,
                'unidade_id'       => $this->add_saida_unidade_id,
                'registro_id'      => $saida->id,
                'tipo'             => 'saida',
                'n_parcelas'       => $this->add_saida_n_parcelas,
                'n_parcela'        => $parcela['add_saida_n_parcela'],
                'valor'            => $parcela['add_saida_valor'],
                'forma_pagamento'  => $parcela['add_saida_forma_pagamento'],
                'link'             => $parcela['add_saida_links'],
                'data_vencimento'  => $parcela['add_saida_data_vencimento'],
            ]);
        }

        $valor_total = FluxoCaixa::getValorRealInArray($valores);

        $saida->update(['valor_total' => $valor_total]);

        FluxoCaixa::plataforma()->where('registro_id', $saida->id)->update(['valor_total' => $valor_total]);

        $this->resetFieldsAddSaida();

        session()->flash('success-livewire', 'Saída cadastrada com êxito.');
        $this->dispatchBrowserEvent('dissmiss-modal');
        $this->dispatchBrowserEvent('toast');
    }

    public function resetFieldsAddSaida()
    {
        $this->add_saida_ref_saida = null;
        $this->add_saida_unidade_id = null;
        $this->add_saida_valor = null;
        $this->add_saida_forma_pagamento = null;
        $this->add_saida_link = null;
        $this->add_saida_n_parcelas = 1;
        $this->add_saida_data_recebimento = null;
        $this->add_saida_data_vencimento = date('Y-m-d');
        $this->add_saida_status = 0;
        $this->add_saida_obs = null;
    }

    public function resetFieldsAddEntrada()
    {
        $this->add_entrada_vendedor_id = null;
        $this->cliente_id = null;
        $this->add_entrada_cliente_selected_nome = null;
        $this->add_entrada_trilha_id = null;
        $this->add_entrada_turma_id = null;
        $this->add_entrada_produto_nome = null;
        $this->add_entrada_unidade_nome = null;
        $this->add_entrada_vendedor_nome = null;
        $this->add_entrada_unidade_id = null;
        $this->add_entrada_valor = null;
        $this->add_entrada_forma_pagamento = null;
        $this->add_entrada_link = null;
        $this->add_entrada_n_parcelas = 1;
        $this->add_entrada_link = null;
        $this->add_entrada_data_recebimento = null;
        $this->add_entrada_data_vencimento = date('Y-m-d');
        $this->add_entrada_data_estorno = null;
        $this->add_entrada_status = 0;
        $this->add_entrada_obs = null;
    }

    public function editarSaida()
    {
        $this->editar_saida->update([
            'editor_id'        => \Auth::id(),
            'editor_nome'      => \Auth::user()->nome,
            'ref_saida'        => $this->editar_saida_ref_saida,
            'unidade_nome'     => $this->editar_saida_unidade_nome,
            'unidade_id'       => $this->editar_saida_unidade_id,
            'tipo'             => 'saida',
            'valor'            => $this->editar_saida_valor,
            'forma_pagamento'  => $this->editar_saida_forma_pagamento,
            'link'             => $this->editar_saida_link,
            'n_parcela'        => 1,
            'n_parcelas'       => $this->editar_saida_n_parcelas,
            'data_recebimento' => $this->editar_saida_data_recebimento,
            'data_vencimento'  => $this->editar_saida_data_vencimento,
            'status'           => $this->editar_saida_status,
            'obs'              => $this->editar_saida_obs,
        ]);
    
        
        $lineParcelas = array_map(function ($editar_saida_n_parcela, $editar_saida_data_vencimento, $editar_saida_valor, $editar_saida_forma_pagamento, $editar_saida_link, $editar_saida_registro_id, $editar_saida_id)
        {
          return array_combine(
            ['editar_saida_n_parcela','editar_saida_data_vencimento','editar_saida_valor', 'editar_saida_forma_pagamento', 'editar_saida_link', 'editar_saida_registro_id','editar_saida_id'],
            [$editar_saida_n_parcela, $editar_saida_data_vencimento, $editar_saida_valor, $editar_saida_forma_pagamento, $editar_saida_link, $editar_saida_registro_id, $editar_saida_id]
          );
        }, $this->editar_saida_n_parcela, $this->editar_saida_datas_vencimento, $this->editar_saida_valores, $this->editar_saida_formas_pagamento, $this->editar_saida_link, $this->editar_saida_registro_ids, $this->editar_saida_ids);


        $parcelasIds = [];
        $parcelasIds[] = $this->editar_saida->id;

        foreach($lineParcelas as $parcela)
        {
            $checkSaida = FluxoCaixa::plataforma()->where('id', $parcela['editar_saida_id'] ?? 0)->first();

            if($checkSaida)
            {
                $checkSaida->update([
                    'editor_id'        => \Auth::id(),
                    'editor_nome'      => \Auth::user()->nome,
                    'ref_saida'        => $this->editar_saida_ref_saida,
                    'unidade_nome'     => $this->editar_saida_unidade_nome,
                    'unidade_id'       => $this->editar_saida_unidade_id,
                    'n_parcelas'       => $this->editar_saida_n_parcelas,
                    'n_parcela'        => $parcela['editar_saida_n_parcela'],
                    'valor'            => $parcela['editar_saida_valor'],
                    'forma_pagamento'  => $parcela['editar_saida_forma_pagamento'],
                    'link'             => $parcela['editar_saida_link'],
                    'data_vencimento'  => $parcela['editar_saida_data_vencimento'],
                ]);

            } else {

                $checkSaida = FluxoCaixa::create([
                    'cadastrante_id'   => \Auth::id(),
                    'cadastrante_nome' => \Auth::user()->nome,
                    'ref_saida'        => $this->editar_saida_ref_saida,
                    'unidade_id'       => $this->editar_saida_unidade_id,
                    'tipo'             => 'saida',
                    'n_parcelas'       => $this->editar_saida_n_parcelas,
                    'registro_id'      => $this->editar_saida->registro_id ?? $this->editar_saida->id,
                    'n_parcela'        => $parcela['editar_saida_n_parcela'],
                    'valor'            => $parcela['editar_saida_valor'],
                    'forma_pagamento'  => $parcela['editar_saida_forma_pagamento'],
                    'link'             => $parcela['editar_saida_link'],
                    'data_vencimento'  => $parcela['editar_saida_data_vencimento'],  
                ]);

            }

            FluxoCaixa::plataforma()->where('n_parcela', $checkSaida->n_parcela)->where('registro_id', $checkSaida->registro_id)->where('id','<>', $checkSaida->id)->delete();

            $parcelasIds[] = $checkSaida->id;
        }

        $valores = FluxoCaixa::plataforma()->whereIn('id', $parcelasIds)->select('valor')->get()->pluck('valor')->toArray();
        $valor_total = FluxoCaixa::getValorRealInArray($valores);
        FluxoCaixa::plataforma()->whereIn('id', $parcelasIds)->update(['valor_total' => $valor_total]);

        session()->flash('success-livewire', 'Saída atualizada com êxito.');
        $this->dispatchBrowserEvent('dissmiss-modal');
        $this->dispatchBrowserEvent('toast');

    }


    public function setAddEntradaProdutoNome($value)
    {
        $checkTrilha = Str::contains($value, 'trilha_id');

        if($checkTrilha == true)
        {
            $value = str_replace('trilha_id', '', $value);
            $this->add_entrada_trilha_id = $value;
        
            $trilha = Trilha::plataforma()->where('id', $value)->select('referencia','valor_a_vista')->first();
            $this->add_entrada_produto_nome = $trilha->referencia;

            if($trilha->valor_a_vista)
            {
                $this->add_entrada_valor = $trilha->valor_a_vista;
            }

        } else {

            $value = str_replace('turma_id', '', $value);
            $this->add_entrada_turma_id = $value;
        
            $turma = Turma::plataforma()->where('id', $value)->select('nome','valor_a_vista')->first();
            $this->add_entrada_produto_nome = $turma->nome;

            if($turma->valor_a_vista)
            {
                $this->add_entrada_valor = $turma->valor_a_vista;
            }
        }
    }

    public function setAddEntradaUnidadeNome($value)
    {
        $nome = Unidade::select('nome')->plataforma()->where('id', $value)->pluck('nome')[0] ?? null;

        $this->add_entrada_unidade_nome = $nome;
    }

    public function setAddEntradaVendedorNome($value)
    {
        $nome = User::select('nome')->plataforma()->where('id', $value)->pluck('nome')[0] ?? null;

        $this->add_entrada_vendedor_nome = $nome;
    }

    public function setEditarEntradaProdutoNome($value)
    {
        $checkTrilha = Str::contains($value, 'trilha_id');

        if($checkTrilha == true)
        {
            $value = str_replace('trilha_id', '', $value);
            $this->editar_entrada_trilha_id = $value;
            $this->editar_entrada_turma_id  = null;

            $trilha = Trilha::plataforma()->where('id', $value)->select('referencia','valor_a_vista')->first();
            $this->editar_entrada_produto_nome = $trilha->referencia;

            if($trilha->valor_a_vista)
            {
                $this->editar_entrada_valor = $trilha->valor_a_vista;
            }

        } else {

            $value = str_replace('turma_id', '', $value);
            $this->editar_entrada_turma_id  = $value;
            $this->editar_entrada_trilha_id = null;

            $turma = Turma::plataforma()->where('id', $value)->select('nome','valor_a_vista')->first();
            $this->editar_entrada_produto_nome = $turma->nome;

            if($turma->valor_a_vista)
            {
                $this->editar_entrada_valor = $turma->valor_a_vista;
            }
        }
    }

    public function setEditarEntradaUnidadeNome($value)
    {
        $nome = Unidade::select('nome')->plataforma()->where('id', $value)->pluck('nome')[0] ?? null;

        $this->editar_entrada_unidade_nome = $nome;
    }

    public function setEditarEntradaVendedorNome($value)
    {
        $nome = User::select('nome')->plataforma()->where('id', $value)->pluck('nome')[0] ?? null;

        $this->editar_entrada_vendedor_nome = $nome;
    }

    public function setAddSaidaUnidadeNome($value)
    {
        $nome = Unidade::select('nome')->plataforma()->where('id', $value)->pluck('nome')[0] ?? null;

        $this->add_saida_unidade_nome = $nome;
    }

    public function setEditarSaidaUnidadeNome($value)
    {
        $nome = Unidade::select('nome')->plataforma()->where('id', $value)->pluck('nome')[0] ?? null;

        $this->editar_saida_unidade_nome = $nome;
    }

    public function infoEntrada($entrada_id, $mes, $ano)
    {   
        if(strlen($mes) == 1)
        {
            $mes = '0'.$mes;
        }

        $_GET['mes'] = $mes;
        $_GET['ano'] = $ano;
        
        $this->view_entrada = true;
        $this->view_entrada = FluxoCaixa::plataforma()->where('id', $entrada_id)->first() ?? false;
        $this->entrada_id   = $entrada_id;
    }

    public function infoSaida($saida_id, $mes, $ano)
    {           
        if(strlen($mes) == 1)
        {
            $mes = '0'.$mes;
        }

        $_GET['mes'] = $mes;
        $_GET['ano'] = $ano;
        
        $this->view_saida = true;
        $this->view_saida = FluxoCaixa::plataforma()->where('id', $saida_id)->first() ?? false;
        $this->saida_id   = $saida_id;
    }

    public function setIdParaMarcarDespesaComoPaga($id)
    {
        $this->editar_saida_id_para_acao = $id;
        $this->editar_saida_data_recebimento = date('Y-m-d');
    }

    public function marcarDespesaComoPaga()
    {
        FluxoCaixa::plataforma()->where('id', $this->editar_saida_id_para_acao)->update(['status' => 1, 'data_recebimento' => $this->editar_saida_data_recebimento]);

        session()->flash('success-livewire', 'Despesa marcada como paga com êxito.');
        $this->dispatchBrowserEvent('dissmiss-modal');
        $this->dispatchBrowserEvent('toast');
    }

    public function setIdParaMarcarVendaComoRecebida($id)
    {
        $this->editar_entrada_id_para_acao = $id;
        $this->editar_entrada_data_recebimento = date('Y-m-d');
    }

    public function marcarVendaComoRecebida()
    {
        FluxoCaixa::plataforma()->where('id', $this->editar_entrada_id_para_acao)->update(['status' => 1, 'data_recebimento' => $this->editar_entrada_data_recebimento]);

        session()->flash('success-livewire', 'Venda marcada como recebida com êxito.');
        $this->dispatchBrowserEvent('dissmiss-modal');
        $this->dispatchBrowserEvent('toast');
    }

    public function setIdParaMarcarVendaComoEstornada($id)
    {
        $this->editar_entrada_id_para_acao = $id;
        $this->editar_entrada_data_estorno = date('Y-m-d');
    }

    public function marcarVendaComoEstornada()
    {
        FluxoCaixa::plataforma()->where('id', $this->editar_entrada_id_para_acao)->update(['status' => 2, 'data_estorno' => $this->editar_entrada_data_recebimento]);

        session()->flash('success-livewire', 'Venda marcada como estornada com êxito.');
        $this->dispatchBrowserEvent('dissmiss-modal');
        $this->dispatchBrowserEvent('toast');
    }

    public function setIdParaDeletarEntrada($id)
    {
        $this->editar_entrada_id_para_acao = $id;
    }

    public function deletarEntrada()
    {
        if(\Auth::attempt(['id' => \Auth::id(), 'password' => $this->password_to_delete, 'status' => 0]))
        {
            $entrada = FluxoCaixa::plataforma()->where('id', $this->editar_entrada_id_para_acao)->first();

            $deleteAndReturnCountDeleteds = 0;

            if($entrada->registro_id == null)
            {
                $deleteAndReturnCountDeleteds = FluxoCaixa::plataforma()->where('registro_id', $entrada->id)->delete();
            }

            Log::create([
                'tipo' => 'fluxo-caixa',
                'ref'  => 'Entrada do Fluxo de Caixa deletada. | ' . $entrada->cliente_nome . ' | ' . $entrada->produto_nome . ' | ' . $entrada->valor . ' | ' . $deleteAndReturnCountDeleteds . ' parcelas futuras afetadas.',
            ]);

            $entrada->delete();

            session()->flash('success', 'Entrada deletada com êxito.');
            
            return redirect(route('admin.controle-financeiro.lista.fluxo-caixa', ['mes' => $this->mes, 'ano' => $this->ano]));

        } else {

            session()->flash('danger-livewire', 'Senha incorreta.');

            $this->dispatchBrowserEvent('toast');
        }
    }

    public function deletarSaida()
    {
        if(\Auth::attempt(['id' => \Auth::id(), 'password' => $this->password_to_delete, 'status' => 0]))
        {
            $saida = FluxoCaixa::plataforma()->where('id', $this->editar_saida_id_para_acao)->first();

            $deleteAndReturnCountDeleteds = 0;

            if($saida->registro_id == null)
            {
                $deleteAndReturnCountDeleteds = FluxoCaixa::plataforma()->where('registro_id', $saida->id)->delete();
            }

            Log::create([
                'tipo' => 'fluxo-caixa',
                'ref'  => 'Saída do Fluxo de Caixa deletada. | ' . $saida->ref_saida . ' | ' . $saida->valor . ' | ' . $deleteAndReturnCountDeleteds . ' parcelas futuras afetadas.',
            ]);

            $saida->delete();

            session()->flash('success', 'Saída deletada com êxito.');
            
            return redirect(route('admin.controle-financeiro.lista.fluxo-caixa', ['mes' => $this->mes, 'ano' => $this->ano]));

        } else {

            session()->flash('danger-livewire', 'Senha incorreta.');

            $this->dispatchBrowserEvent('toast');
        }
    }

    public function setIdParaDeletarSaida($id)
    {
        $this->editar_saida_id_para_acao = $id;
    }

}
