<li class="nav-item dropdown">

    <a class="nav-link dropdown-toggle border-0 text-dark me-2" href="#" data-bs-toggle="dropdown" title="Ajuda">
        <i class="fas fa-question-circle fs-16 text-dark"></i>
    </a>
    <div class="dropdown-menu dropdown-menu-end">
        <a class="dropdown-item" href="{{ route('admin.ajuda.tutorial.lista') }}"><i class="fa fa-info-circle"></i>&nbsp;Tutoriais</a>
        <a class="dropdown-item" href="{{ route('admin.ajuda.chamado.lista') }}"><i class="fa fa-headset"></i>&nbsp;Chamados</a>
        <a class="dropdown-item" href="{{ route('admin.billing') }}"><i class="fas fa-dollar-sign"></i>&nbsp; Meu Plano</a>
    </div>
</li>

<li class="nav-item dropdown">

    <a class="nav-link dropdown-toggle border-0 text-dark" href="#" data-bs-toggle="dropdown">
    	<i class="fas fa-user-circle fs-17"></i>
        <span>
            {{ strtok(Auth::user()->nome, " ") }}
        </span>
    </a>
    <div class="dropdown-menu dropdown-menu-end">
        <a class="dropdown-item" href="{{ route('aluno.dashboard') }}"><i class="fas fa-chalkboard-teacher"></i> Área do Aluno</a>
        <a class="dropdown-item" href="{{ route('logout') }}"><i class="align-middle" data-feather="log-out"></i> Sair</a>
    </div>
</li>
