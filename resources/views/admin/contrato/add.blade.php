@extends('layouts.admin.master')
@section('content')
    
<div>

    <form method="POST" enctype="multipart/form-data" class="mb-4">
    @csrf

		<section class="mb-4-5">
			
			<a href="#boxInfoPrincipal" data-bs-toggle="collapse" aria-expanded="false" class="hover-d-none">
				<div class="section-title">
					Informações Principais
				</div>
			</a>
			<hr/>
			
			<div class="collapse show" id="boxInfoPrincipal">

		    	<div class="d-lg-flex mb-3">

			    	<div class="w-100 me-lg-3 mb-3 mb-lg-0">
			    		<label>Referência</label>
			    		<input type="text" name="referencia" class="form-control p-form" placeholder="Nome" required>
			    	</div>

					<div class="w-100 me-lg-3 mb-3 mb-lg-0">
						<label>PDF</label>
						<input type="file" name="arquivo_pdf" class="file-form" required>
					</div>

					@if(\App\Models\Unidade::checkActive() === true)
					<div class="w-lg-60 me-lg-3 mb-3 mb-lg-0 boxUnidadeRemove">
						<label>Unidade</label>
						<select name="unidade_id" id="unidade_id" class="select-form">
							<option value="">Selecione</option>
							@foreach($unidades as $unidade)
								<option value="{{ $unidade->id }}" @if(old('unidade_id') == $unidade->id) selected @endif>
									{{ $unidade->nome }}
								</option>
							@endforeach
						</select>
					</div>
					@endif

			    	<div class="w-lg-35">
			    		<label>Status</label>
			    		<select name="status" class="select-form">
			    			<option value="0">Ativo</option>
			    			<option value="1">Desativado</option>
			    		</select>
			    	</div>

			    </div>

			</div>

		</section>

		<button type="submit" name="sent" value="ok" class="btn-add mt-3">Cadastrar</button>	

    </form>

</div>

@endsection