<ul class="list-group list-group-sidebar">

    <!--
    <a href="" class="list-group-item text-light bg-14 bg-hover-222e3c border">
        Meu Perfil
    </a>                                            
    -->
    
    <br>

    <a href="{{ route('aluno.minha-conta') }}" id="url-informacoes-gerais" class="list-group-item text-light bg-14 bg-hover-222e3c border">
        Informações Gerais
    </a>                                            

    <br>

    <a href="#url-experiencia" id="href-url-experiencia" data-bs-toggle="collapse" class="d-flex align-items-center justify-content-between list-group-item text-light bg-14 bg-hover-222e3c border">
        <span>Experiências Profissionais</span>
        <i class="fa fa-sort-down"></i>
    </a>
    <div id="url-experiencia" class="collapse" align="center">
       <a href="/aluno/minha-conta/experiencia/lista" id="url-experiencia-lista" class="list-group-item text-light bg-14 bg-hover-222e3c border">
            Visualizar todas
        </a>
       <a href="/aluno/minha-conta/experiencia/add" id="url-experiencia-add" class="list-group-item text-light bg-14 bg-hover-222e3c border">
            Adicionar
        </a>
    </div>

    <br>

    <a href="#url-formacao" id="href-url-formacao" data-bs-toggle="collapse" class="d-flex align-items-center justify-content-between list-group-item text-light bg-14 bg-hover-222e3c border">
        <span>Formação Acadêmica</span>
        <i class="fa fa-sort-down"></i>
    </a>
    <div id="url-formacao" class="collapse" align="center">
       <a href="/aluno/minha-conta/formacao/lista" id="url-formacao-lista" class="list-group-item text-light bg-14 bg-hover-222e3c border">
            Visualizar todas
        </a>
       <a href="/aluno/minha-conta/formacao/add" id="url-formacao-add" class="list-group-item text-light bg-14 bg-hover-222e3c border">
            Adicionar
        </a>
    </div>

    <br>
    <br>

    <a href="{{ route('aluno.minha-conta.alterar-senha') }}" id="url-alterar-senha" class="list-group-item text-light bg-14 bg-hover-222e3c border">
        Alterar Senha
    </a> 

</ul>