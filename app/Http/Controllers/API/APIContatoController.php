<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Contato;
use Illuminate\Http\Request;
use HubSpot\Factory;
use HubSpot\Client\Crm\Contacts\ApiException;
use HubSpot\Client\Crm\Contacts\Model\SimplePublicObjectInput;

class APIContatoController extends Controller
{
    public function addContatoGreatpagesFadec(Request $request)
    {
        $plataforma_id = 26;
        $assunto = 'Website';
        $chaveApiHubspot = 'pat-na1-c388c743-5bb3-4f0b-a964-6d7cd2dc8d2c';

        \App\Models\Contato::create([
            'plataforma_id' => $plataforma_id,
            'tipo'          => 'L',
            'assunto'       => $assunto,
            'nome'          => $request->nome,
            'email'         => $request->email,
            'celular'       => $request->telefone,
        ]);

        $this->sendContatoHubspot($chaveApiHubspot, $request);
    }

    public function sendContatoHubspot($chaveApiHubspot, $request)
    {
        $nome      = $request->nome;
        $firstname = $this->getPrimeiroSegundoNome($nome);
        $lastname  = $this->getLastName($firstname, $nome);

        $client = Factory::createWithAccessToken($chaveApiHubspot);

        $properties = [
            'email'     => $request->email,
            'firstname' => $firstname,
            'lastname'  => $lastname,
            'phone'     => $request->telefone,
            'website'   => \Request::getHost()
        ];

        $simplePublicObjectInputForCreate = new SimplePublicObjectInput([
            'properties'   => $properties,
            'associations' => null,
        ]);

        try {

            $apiResponse = $client->crm()->contacts()->basicApi()->create($simplePublicObjectInputForCreate);

        } catch (ApiException $e) {

            echo "Exception when calling basic_api->create: ", $e->getMessage();
        }
    }

    public function getPrimeiroSegundoNome($nome)
    {
        $names = explode(' ', $nome);
        $nome = (isset($names[1])) ? $names[0]. ' ' .$names[1] : $names[0];

        if (array_key_exists(2, $names))
            $names2 = $names[2];
        else
            $names2 = null;

        if( $this->endsWith($nome, 'de') || $this->endsWith($nome, 'dos') || $this->endsWith($nome, 'da') )
            $nome = $nome . ' ' . $names2 ?? null;

        return $nome;
    }

    public function endsWith( $haystack, $needle ) {
        return $needle === "" || (substr($haystack, -strlen($needle)) === $needle);
    }

    public function getLastName($firstname, $nome)
    {
        $lastname  = explode($firstname, $nome);
        $lastname  = $lastname[1];
        
        if($lastname == '')
        {
            $fullname  = explode(' ', $firstname);
            $firstname = $fullname[0];

            if (array_key_exists(1, $fullname))
                $lastname  = $fullname[1];
            else
                $lastname = ' ';
        }

        return $lastname;
    }

}
