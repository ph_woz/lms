<?php

namespace App\Http\Controllers\SiteProprio;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\PaginaConteudo;
use App\Models\Plataforma;
use App\Models\PlataformaIntegracao;
use Mail;
use App\Mail\EmailFormulario;
use HubSpot\Factory;
use HubSpot\Client\Crm\Contacts\ApiException;
use HubSpot\Client\Crm\Contacts\Model\SimplePublicObjectInput;

class PoloController extends Controller
{
    public function index()
    {
        $seo = PaginaConteudo::plataforma()->where('url','/')->select('plataforma_id','seo_title','seo_keywords','seo_description')->first();

        $textoQuemSomos = PaginaConteudo::select('conteudo')->plataforma()->where('url','/sobre-nos')->value('conteudo');

        if(request('email'))
        {            
            if(request('check'))
            {
                return back()->with('danger', 'SPAM Detected!');
            }

            if(strpos(request('nome'), 'Cryto') !== false)
            {
                return back()->with('danger', 'SPAM Detected!');
            }

            $contato = \App\Models\Contato::create(request()->all() + [
                'plataforma_id' => $seo->plataforma_id,
                'tipo' => 'L',
                'assunto' => 'NOVO LEAD POLO',
            ]);

            $chaveApiHubspot = PlataformaIntegracao::plataforma()->ativo()->where('ref','chave-api-hubspot')->pluck('value')[0] ?? null;
            
            if(isset($chaveApiHubspot))
            {
                $this->sendContatoHubspot($chaveApiHubspot);
            }

            $email = Plataforma::select('email')->where('id', $seo->plataforma_id)->value('email');

            if($email != null)
            {
                Mail::to($email)->send(new EmailFormulario($contato));
            }

            return redirect('/agradecimentos')->send();
        }

    	return view('sites-proprios.polo.home', compact('seo','textoQuemSomos') );
    }

    public function agradecimentos()
    {
        return view('sites-proprios.polo.agradecimentos');
    }

    public function getLastName($firstname, $nome)
    {
        $lastname  = explode($firstname, $nome);
        $lastname  = $lastname[1];
        
        if($lastname == '')
        {
            $fullname  = explode(' ', $firstname);
            $firstname = $fullname[0];

            if (array_key_exists(1, $fullname))
                $lastname  = $fullname[1];
            else
                $lastname = ' ';
        }

        return $lastname;
    }

    public function sendContatoHubspot($chaveApiHubspot)
    {
        $nome      = request('nome');
        $firstname = $this->getPrimeiroSegundoNome($nome);
        $lastname  = $this->getLastName($firstname, $nome);

        $client = Factory::createWithAccessToken($chaveApiHubspot);

        $properties = [
            'email'     => request('email'),
            'firstname' => $firstname,
            'lastname'  => $lastname,
            'phone'     => request('celular'),
            'website'   => \Request::getHost()
        ];

        $simplePublicObjectInputForCreate = new SimplePublicObjectInput([
            'properties'   => $properties,
            'associations' => null,
        ]);

        try {

            $apiResponse = $client->crm()->contacts()->basicApi()->create($simplePublicObjectInputForCreate);

        } catch (ApiException $e) {

            echo "Exception when calling basic_api->create: ", $e->getMessage();
        }
    }

    public function getPrimeiroSegundoNome($nome)
    {
        $names = explode(' ', $nome);
        $nome = (isset($names[1])) ? $names[0]. ' ' .$names[1] : $names[0];

        if (array_key_exists(2, $names))
            $names2 = $names[2];
        else
            $names2 = null;

        if( $this->endsWith($nome, 'de') || $this->endsWith($nome, 'dos') || $this->endsWith($nome, 'da') )
            $nome = $nome . ' ' . $names2 ?? null;

        return $nome;
    }

    public function endsWith( $haystack, $needle ) {
        return $needle === "" || (substr($haystack, -strlen($needle)) === $needle);
    }

}