<!doctype html>
<html lang="pt-br">
<head>

   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1">

   <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/uikit@3.5.9/dist/css/uikit.min.css" />
   <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
   <link rel="stylesheet" href="{{ asset('css/util.css') }}?v=3">
   <link rel="stylesheet" href="{{ asset('sites-proprios/faro/css/home.css') }}?v=3">

   <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">

   <title>{{ $seo->seo_title ?? $plataforma->nome }}</title>
   <meta name="robots" content="index, follow">
   <meta name="description" content="{{ $seo->seo_description ?? null }}"/>
   <meta name="keywords" content="{{ $seo->seo_keywords ?? null }}"/>
   <link rel="canonical" href="{{ Request::url() }}" />
   <link rel="shortcut icon" href="{{ $plataforma->favicon }}">
   <meta property="og:locale" content="pt_BR" />
   <meta property="og:type" content="website" />
   <meta property="og:title" content="{{ $seo->seo_title ?? $plataforma->nome }}" />
   <meta property="og:description" content="{{ $seo->seo_description ?? null }}" />
   <meta property="og:url" content="{{ Request::url() }}" />
   <meta property="og:site_name" content="{{ $plataforma->nome }}" />
   <meta property="og:image" content="{{ $plataforma->logotipo }}" />
   <meta name='author' content='WOZCODE | Pedro Henrique' />

</head>
<body class="bg-f4" id="inicio">

   @include('sites-proprios/faro/componentes/header')

   <main class="pb-5">

      <section class="w-100 h-lg-300 background-size-cover background-position-center" style="background-image: url('{{ $plataforma->jumbotron ?? asset('sites-proprios/faro/images/jumbotron.jpeg') }}');">
              <div class="position-relative h-100">
              <div class="background-overlay opacity-08 h-100" style="background: rgba(0,0,0,1);"></div>
               <div class="container h-100 py-5 py-lg-0">
                 <div class="row container align-items-center h-100 position-relative">
                    <div>
                        <h1 class="text-light font-poppins weight-800 fs-xs-35 fs-md-50 text-shadow-3" id="tituloTransferencia">
                           TRANSFERÊNCIA
                        </h1>
                        <h2 class="text-light font-poppins weight-600 fs-19 text-shadow-3 mb-4">
                           Um recomeço que respeita sua história.<br>
                           Escolha mudar e aproveite até 100% da carga horária cursada.
                        </h2>
                    </div>
                 </div>
              </div>
            </div>
      </section>

      <section class="py-5 z-3 position-relative">
         <div class="container">
            <div class="p-lg-5 p-4 bg-white box-shadow border rounded-20">
               <div class="row">
                  <div class="col-lg-7">
                     <h1 class="color-base font-poppins weight-800 fs-35">Como Funciona</h1>
                     <p class="font-opensans fs-22 color-555">
                        Se você fez o ENEM, não precisa fazer o vestibular da FARO. A sua nota do ENEM vale como forma de ingresso por até dez anos.
                     </p>
                     <div class="mt-4 mb-3 mb-lg-0">
                        <a href="/f/formulario-de-matricula/45/t/631" target="_blank" class="bg-faixa bg-hover-gradient fs-14 text-center px-3-5 rounded box-shadow text-light weight-700 py-2-5 font-poppins hover-d-none d-xs-block">
                           QUERO ME INSCREVER
                        </a>
                        <a href="https://api.whatsapp.com/send?phone=55{{preg_replace('/\D/', '', $plataforma->whatsapp)}}&text=Olá, vim através do Site {{ $plataforma->nome }}" target="_blank" class="ms-lg-3 bg-wpp text-shadow-1 fs-14 text-center px-3-5 rounded box-shadow text-light weight-700 py-2-5 font-poppins hover-d-none d-xs-block mt-2 mt-lg-0">
                           <span class="me-2">QUERO RECEBER UMA MENSAGEM</span>
                           <i class="fab fa-whatsapp d-xs-none"></i>
                        </a>
                     </div>
                  </div>
                  <div class="col-lg-5 text-xs-center">
                     <div class="mt--lg-175px">

                        <div class="mb-4 mt-3 mt-lg-0">
                           <img src="{{ asset('sites-proprios/faro/images/mulher.jpg') }}" alt="Faro - Mulher" class="img-fluid rounded-20">
                        </div>

                        <div class="d-lg-flex align-items-center justify-content-end">

                           <div class="mb-3 mb-lg-0 me-lg-4">
                              <p class="mb-0 color-azul weight-700 font-poppins">
                                 Compartilhe com um amigo
                              </p>
                           </div>
                           <div class="d-flex justify-content-center">
                              <a href="{{ $plataforma->facebook }}" target="_blank" class="hover-d-none">
                                 <div class="bg-faixa square-40 flex-center rounded-circle">
                                    <i class="fab fa-facebook-f color-icon-footer fs-20"></i>
                                 </div>
                              </a>
                              <a href="" target="_blank" class="mx-2 hover-d-none">
                                 <div class="bg-faixa square-40 flex-center rounded-circle">
                                    <i class="fab fa-linkedin-in color-icon-footer fs-20"></i>
                                 </div>
                              </a>
                              <a href="" target="_blank" class="hover-d-none">
                                 <div class="bg-faixa square-40 flex-center rounded-circle">
                                    <i class="fab fa-whatsapp color-icon-footer fs-20"></i>
                                 </div>
                              </a>
                           </div>

                        </div>

                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>

      <section class="p-lg-5 p-3 mb-lg-5 bg-f5 mt-3 mt-lg-0">
         <div class="container">

            <h2 class="text-center text-dark font-poppins weight-500 fs-20">Confira o passo a passo</h2>
            <h1 class="text-center text-dark font-poppins weight-700">
               Como fazer a Transferência
            </h1>

            <div class="row align-items-center mt-75px">

               <div class="col-lg-3 mb-5 mb-lg-0">

                  <div class="bg-white p-4 box-shadow rounded-10 h-lg-350">
                     <div class="text-center">

                        <div class="d-flex justify-content-center">
                           <div class="square-75 rounded-circle bg-gradient flex-center mt--60px">
                              <span class="fs-50">1</span>
                           </div>
                        </div>

                        <h1 class="mb-0 fs-25 weight-700 mt-3 font-opensans">
                           Escolha seu curso
                        </h1>

                        <div class="flex-center mt-3"><div class="bg-gradient" style="height: 2px;width: 50px;"></div></div>

                        <p class="mt-3 weight-600 font-opensans">
                           Escolha qual curso você deseja fazer a sua transferência.
                        </p>
                     
                     </div>
                  </div>

               </div>

               <div class="col-lg-3 mb-5 mb-lg-0">

                  <div class="bg-white p-4 box-shadow rounded-10 h-lg-350">
                     <div class="text-center">

                        <div class="d-flex justify-content-center">
                           <div class="square-75 rounded-circle bg-gradient flex-center mt--60px">
                              <span class="fs-50">2</span>
                           </div>
                        </div>

                        <h1 class="mb-0 fs-25 weight-700 mt-3 font-opensans">
                           Envio de documentação
                        </h1>

                        <div class="flex-center mt-3"><div class="bg-gradient" style="height: 2px;width: 50px;"></div></div>

                        <p class="mt-3 weight-600 font-opensans">
                           Inscreva-se e envie sua documentação para análise.
                        </p>
                     
                     </div>
                  </div>

               </div>

               <div class="col-lg-3 mb-5 mb-lg-0">

                  <div class="bg-white p-4 box-shadow rounded-10 h-lg-350">
                     <div class="text-center">

                        <div class="d-flex justify-content-center">
                           <div class="square-75 rounded-circle bg-gradient flex-center mt--60px">
                              <span class="fs-50">3</span>
                           </div>
                        </div>

                        <h1 class="mb-0 fs-25 weight-700 mt-3 font-opensans">
                           Acompanhe seu processo
                        </h1>

                        <div class="flex-center mt-3"><div class="bg-gradient" style="height: 2px;width: 50px;"></div></div>

                        <p class="mt-3 weight-600 font-opensans">
                           Após a validação é importante que você acompanhe e aceite a análise em relação as unidades curriculares.
                        </p>
                     
                     </div>
                  </div>

               </div>

               <div class="col-lg-3 mb-5 mb-lg-0">

                  <div class="bg-white p-4 box-shadow rounded-10 h-lg-350">
                     <div class="text-center">

                        <div class="d-flex justify-content-center">
                           <div class="square-75 rounded-circle bg-gradient flex-center mt--60px">
                              <span class="fs-50">4</span>
                           </div>
                        </div>

                        <h1 class="mb-0 fs-25 weight-700 mt-3 font-opensans">
                           Faça sua Matrícula
                        </h1>

                        <div class="flex-center mt-3"><div class="bg-gradient" style="height: 2px;width: 50px;"></div></div>

                        <p class="mt-3 weight-600 font-opensans">
                           Agora é só realizar a sua matrícula, pagar o boleto e começar sua jornada na Faro!
                        </p>
                     
                     </div>
                  </div>

               </div>
            </div>

         </div>
      </section>

      <section class="p-lg-5 p-3 bg-gradient">
         <div class="container">

            <h1 class="text-center text-dark font-poppins weight-700">
               Perguntas Frequentes
            </h1>

            <p class="mb-0 text-center font-poppins text-dark fs-17 col-md-10 offset-md-1">
               Aqui estão todas as informações necessárias para você tirar suas dúvidas sobre nosso processo de ensino.
            </p>

            <div class="pt-4-5 px-xl-8 ml-lg-2">

               <a href="#collapseFaq1" data-bs-toggle="collapse" href="#collapseFaq1" class="text-d-none hover-d-none text-dark font-poppins weight-600 fs-16">
                  <div class="mb-2 bg-white p-3 rounded box-shadow">
                     <i class="fas fa-caret-right"></i>
                     <span class="fs-17">O que é ProUni?</span>
                     <p class="collapse mb-0 mt-1 font-poppins text-dark weight-500" id="collapseFaq1">
                        O Prouni – Programa Universidade para Todos – é um sistema que faz concede bolsas de estudo integrais e parciais de 50% em instituições privadas de educação superior, em cursos de graduação e sequenciais de formação específica a estudantes brasileiros, desde que ainda não tenham diploma de nível superior.
                     </p>
                  </div>
               </a>
               <a href="#collapseFaq2" data-bs-toggle="collapse" href="#collapseFaq2" class="text-d-none hover-d-none text-dark font-poppins weight-600 fs-16">
                  <div class="mb-2 bg-white p-3 rounded box-shadow">
                     <i class="fas fa-caret-right"></i>
                     <span class="fs-17">Fiz o ENEM há 2 anos, posso tentar o ProUni?</span>
                     <p class="collapse mb-0 mt-1 font-poppins text-dark weight-500" id="collapseFaq2">
                        Não. O Prouni só aceita a nota do Enem do ano anterior ao processo de concessão da bolsa.
                     </p>
                  </div>
               </a>
               <a href="#collapseFaq3" data-bs-toggle="collapse" href="#collapseFaq3" class="text-d-none hover-d-none text-dark font-poppins weight-600 fs-16">
                  <div class="mb-2 bg-white p-3 rounded box-shadow">
                     <i class="fas fa-caret-right"></i>
                     <span class="fs-17">Qual a menor nota aceita para solicitar o ProUni?</span>
                     <p class="collapse mb-0 mt-1 font-poppins text-dark weight-500" id="collapseFaq3">
                        Sua nota tem que ser de no mínimo de 450 pontos e você não pode ter zerado a redação.
                     </p>
                  </div>
               </a>
               <a href="#collapseFaq4" data-bs-toggle="collapse" href="#collapseFaq4" class="text-d-none hover-d-none text-dark font-poppins weight-600 fs-16">
                  <div class="mb-2 bg-white p-3 rounded box-shadow">
                     <i class="fas fa-caret-right"></i>
                     <span class="fs-17">Posso solicitar o ProUni antes de concluir o ensino médio?</span>
                     <p class="collapse mb-0 mt-1 font-poppins text-dark weight-500" id="collapseFaq4">
                        Não. É necessário ter cursado as 3 séries do ensino médio.
                     </p>
                  </div>
               </a>
               <a href="#collapseFaq5" data-bs-toggle="collapse" href="#collapseFaq5" class="text-d-none hover-d-none text-dark font-poppins weight-600 fs-16">
                  <div class="mb-2 bg-white p-3 rounded box-shadow">
                     <i class="fas fa-caret-right"></i>
                     <span class="fs-17">O ProUni é concedido em caso de segunda graduação?</span>
                     <p class="collapse mb-0 mt-1 font-poppins text-dark weight-500" id="collapseFaq5">
                        Não. O Prouni é exclusivo para quem não possui nível superior completo.
                     </p>
                  </div>
               </a>

            </div>

         </div>
      </section>

      <section class="pt-5">
         <div class="container">

            <div class="bg-white p-lg-5 p-4 box-shadow rounded-20">
               <div class="row">

                  <div class="order-0 col-lg-8">

                     <div class="pe-lg-5">
                        <h1 class="color-navlink font-poppins">
                           <span class="weight-700">Há +16 anos</span> transformando vidas por meio da <span class="weight-700">educação</span>.
                        </h1>

                        <p class="font-opensans fs-22 color-555">
                           Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
                        </p>

                        <div class="rounded-20 w-100 h-lg-250 background-size-cover background-position-center" style="background-image: url('{{ $plataforma->jumbotron ?? asset('sites-proprios/faro/images/jumbotron.jpeg') }}');">
                           <div class="rounded-20 position-relative h-100">
                           <div class="rounded-20 background-overlay h-100" style="background: rgba(0,0,0,1);"></div>
                              <div class="container h-100 py-5 py-lg-0">
                                 <div class="row container align-items-center h-100 position-relative">
                                    <h1 class="text-light font-poppins weight-800 fs-xs-35 fs-md-50 text-shadow-3" id="tituloProUni">
                                       A FARO
                                    </h1>
                                    <div class="text-center mt--lg-150px">
                                       <a href="https://youtube.com/shorts/CRUM5SRKGTA?feature=share" target="_blank">
                                          <i class="fas fa-play-circle text-light font-poppins weight-800 fs-55 text-shadow-3"></i>                                 
                                       </a>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>

                  </div>

                  <div class="order-1 col-lg-4 mt-5 mt-lg-0">

                     <div class="d-flex">

                        <div class="d-xs-none w-3px h-lg-200 bg-gradient"></div>

                        <div class="px-5 text-center mb-5 mb-lg-0">
                           <div class="text-center">
                              <i class="fa fa-graduation-cap color-azul fs-50"></i>
                           </div>
                           <h1 class="color-icon-footer weight-900 font-montserrat text-shadow-1">
                              80+
                           </h1>
                           <p class="mb-0 color-azul weight-600 font-poppins">
                              CURSOS
                           </p>
                           <p class="mb-0 color-azul weight-600 font-poppins">
                              no Guia da Faculdade
                           </p>
                        </div>

                     </div>
                     <div class="d-flex">

                        <div class="d-xs-none w-3px h-lg-200 bg-gradient"></div>

                        <div class="px-5 text-center mb-5 mb-lg-0">
                           <div class="text-center">
                              <i class="fas fa-chalkboard-teacher color-azul fs-50"></i>
                           </div>
                           <h1 class="color-icon-footer weight-900 font-montserrat text-shadow-1">
                              90%
                           </h1>
                           <p class="mb-0 color-azul weight-600 font-poppins">
                              MESTRES E DOUTORES
                           </p>
                           <p class="mb-0 color-azul weight-600 font-poppins">
                              dos nossos professores
                           </p>
                        </div>

                     </div>
                     <div class="d-flex">

                        <div class="d-xs-none w-3px h-lg-200 bg-gradient"></div>

                        <div class="px-5 text-center mb-5 mb-lg-0">
                           <div class="text-center">
                              <i class="fas fa-swatchbook color-azul fs-50"></i>
                           </div>
                           <h1 class="color-icon-footer weight-900 font-montserrat text-shadow-1">
                              150+
                           </h1>
                           <p class="mb-0 color-azul weight-600 font-poppins">
                              ÍTENS DE ACERVOS
                           </p>
                           <p class="mb-0 color-azul weight-600 font-poppins">
                              na Biblioteca da Faro
                           </p>
                        </div>

                     </div>
                     
                  </div>

               </div>
            </div>

         </div>
      </section>

      <section class="py-5">
         <div class="container">
            <div class="bg-gradient rounded-top-20 box-shadow p-5">
               <h1 class="text-dark font-poppins weight-800 fs-35">Transfira-se</h1>
                <h2 class="text-dark font-poppins weight-500 fs-25">também com ProUni</h2>
            </div>
            <div class="bg-white rounded-bottom-20 p-lg-4 px-1 pb-1 box-shadow">
               <p class="px-4 pb-4 font-opensans fs-21 color-555">
                  {!! nl2br('

                     <b>Saiba mais sobre o processo de transferência externa</b>

                     A Portaria Normativa N° 19, de 20 de novembro de 2008 do MEC, estabelece que o processo de transferências para bolsistas ProUni e de critério de cada instituição de ensino. A Faculdade de Roseira já encontra dentro do limite exigido pelo MEC, dessa forma não aceitaremos transferência externas de bolsas para o primeiro semestre de 2021.

                     Manual do Bolsista (acesse aqui) Transferência.

                     O bolsista do Prouni poderá transferir a utilização da sua bolsa de estudo para outro curso afim e, ainda, que para turno, campus ou mesmo outra instituição de ensino.

                     Para que a transferência seja efetivada é necessário que:

                     As instituições de origem e de destino estejam de acordo com a transferência;
                     A instituição e o respectivo curso para o qual o estudante deseja se transferir estejam regularmente credenciados no Programa;
                     Exista vaga no curso para o qual o estudante deseja se transferir.
                     O processo de transferência somente é considerado concluído após a formalização da aceitação do estudante pela instituição de ensino de destino, por meio da emissão do Termo de Transferência do Usufruto de Bolsa. Uma vez concluída a transferência, o prazo de utilização da bolsa passará a ser o prazo do curso de destino, subtraído o período utilizado e suspenso no curso de origem.

                     O procedimento de transferência de bolsa é de caráter interno das instituições de ensino envolvidas, efetivando-se no âmbito do Sisprouni, não cabendo intermediação do MEC.
                  ') !!}
               </p>
            </div>
         </div>
      </section>

   </main>

   @include('sites-proprios/faro/componentes/footer')

   <a href="https://api.whatsapp.com/send?phone=55{{preg_replace('/\D/', '', $plataforma->whatsapp)}}&text=Olá, vim através do Site {{ $plataforma->nome }}" target="_blank" class="text-white fs-30">
       <div class="fixed-bottom right-0 me-3 mb-3 z-3">
           <div class="square-60 rounded-circle bg-wpp flex-center box-shadow-wpp">
               <i class="fab fa-whatsapp"></i>
           </div>
       </div>
   </a>

   <script src="https://cdn.jsdelivr.net/npm/uikit@3.5.9/dist/js/uikit.min.js"></script>
   <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-U1DAWAznBHeqEIlVSCgzq+c9gqGAJn5c/t99JyeKa9xxaYpSvHU5awsuZVVFIhvj" crossorigin="anonymous"></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>
   <script>

      var $myGroup = $('#myGroup');
      $myGroup.on('show.bs.collapse','.collapse', function() {
         $myGroup.find('.collapse.show').collapse('hide');
      });

      window.onload = function()
      {
         $(".btn-info").first().click().focus();
      };

      window.onscroll = function() {myFunction()};

      var header = document.getElementById("menu");
      var sticky = header.offsetTop;

      function myFunction() {
        if (window.pageYOffset > sticky) {
          header.classList.add("fixed-top");
        } else {
          header.classList.remove("fixed-top");
        }
      }
   </script>

</body>
</html>