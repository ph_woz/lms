<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserEndereco extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $table = 'users_enderecos';

    protected $fillable = [
        'plataforma_id',
    	'user_id',
    	'cep',
    	'rua',
    	'numero',
    	'bairro',
    	'cidade',
    	'estado',
    	'complemento',
    	'ponto_referencia',
    ];

    public function scopePlataforma($query)
    {
        return $query->where('plataforma_id', session('plataforma_id') ?? Plataforma::dominio()->pluck('id')[0] ?? null);
    }
    
}
