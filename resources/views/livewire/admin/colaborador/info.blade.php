<div>

	<div class="d-flex justify-content-end align-items-center mb-3">
    
		<a href="{{ route('admin.colaborador.editar', ['id' => $user->id]) }}" class="aclNivel2 btn btn-warning text-dark">
			<i class="fa fa-edit"></i>
		</a>

		<a href="#" data-bs-toggle="modal" data-bs-target="#ModalDeletarColaborador" class="aclNivel3 btn btn-danger ms-3">
			<i class="fa fa-trash"></i>
		</a>

	</div>

	<section class="mb-4-5 bg-white shadow p-4 border">
	
		<div class="row">

			<div class="col-lg-6 text-center">


				<div class="d-flex mb-2 mr-2 text-white">
					@if($user->status == 0)
						<span class="bg-success px-3 py-1 rounded">Ativo</span>
					@else
						<span class="bg-danger px-3 py-1 rounded">Desativado</span>
					@endif
				</div>

				@if($user->foto_perfil)
					<img src="{{ $user->foto_perfil }}" class="square-125 rounded-circle object-fit-contain border-profile cursor-pointer" onclick="setFotoPerfil()">
				@else
					<i class="fa fa-user-circle fs-md-75 fs-xs-50 cursor-pointer" onclick="setFotoPerfil()"></i>
				@endif

				<h1 class="fs-19 mt-3 color-333">{{ $user->nome }}</h1>

				@if($user->profissao)
					<h2 class="fs-15 mt-0 color-666 font-roboto" title="Profissão">{{ $user->profissao }}</h2>
				@endif

				<hr class="hr-1 mb-4"/>

				<p class="fs-17 color-555">{{ $user->email }}</p>

				@if($user->cpf)
					<p class="fs-17 color-555">{{ $user->cpf }}</p>
				@endif

				@if($user->matricula)
					<p class="fs-17 color-555" title="Matrícula">{{ $user->matricula }}</p>
				@endif

				@if($user->obs)
					<p class="mb-0 text-start color-333 mt-4">Observações</p>
					<p class="mb-0 text-start color-555">{!! nl2br($user->obs) !!}</p>
				@endif

			</div>

			<div class="col-lg-6">

				<div class="ps-lg-5">

					<p class="mb-1 fs-14">
						<span>Matrícula:</span> 
						<span class="color-555">{{ $user->id }}</span>
					</p>
					
					@if(isset($unidade))
					<p class="mb-1 fs-14">
						<span>Unidade:</span> 
						<a href="{{ route('admin.unidade.info', ['id' => $unidade->id]) }}" class="color-blue-info-link">{{ $unidade->nome }}</a>
					</p>
					@endif

					@if($user->rg)
					<p class="mb-1 fs-14">
						<span>RG:</span> 
						<span class="color-555">{{ $user->rg ?? 'Não definido' }}</span>
					</p>
					@endif
					
					@if($user->naturalidade)
					<p class="mb-1 fs-14">
						<span>Naturalidade:</span> 
						<span class="color-555">{{ $user->naturalidade ?? 'Não definido' }}</span>
					</p>
					@endif

					<p class="mb-1 fs-14">
						<span>Data de Nascimento:</span> 
						<span class="color-555">
							@if($user->data_nascimento)
								{{ \App\Models\Util::replaceDatePt($user->data_nascimento) }} ({{ \App\Models\Util::getIdade($user->data_nascimento) }} anos)
							@else
								Não definido
							@endif
						</span>
					</p>

					@if($user->genero)
					<p class="mb-1 fs-14">
						<span>Gênero:</span> 
						<span class="color-555">
							@if($user->genero == 'M')
								Masculino
							@else
								Feminino
							@endif
						</span>
					</p>
					@endif

					<p class="mb-1 fs-14">
						<span>Celular:</span> 
						<a href="tel:+55{{preg_replace('/\D/', '', $user->celular)}}" class="color-555">{{ $user->celular ?? 'Não definido' }}</a>
					</p>

					@if($user->telefone)
					<p class="mb-1 fs-14">
						<span>Telefone:</span> 
						<a href="tel:+55{{preg_replace('/\D/', '', $user->telefone)}}" class="color-555">{{ $user->telefone ?? 'Não definido' }}</a>
					</p>
					@endif

					<hr>

					@if(isset($endereco['cidade']))

						@if($endereco['cep'])
						<p class="mb-1 fs-14">
							<span>CEP:</span>
							<span class="color-555">{{ $endereco['cep'] }}</span>
						</p>
						@endif
						@if($endereco['rua'])
						<p class="mb-1 fs-14">
							<span>Rua:</span>
							<span class="color-555">
								{{ $endereco['rua'] }}@if($endereco['numero']), {{ $endereco['numero'] }}@endif
							</span>
						</p>
						@endif
						@if($endereco['bairro'])
						<p class="mb-1 fs-14">
							<span>Bairro:</span>
							<span class="color-555">{{ $endereco['bairro'] }}</span>
						</p>
						@endif
						@if($endereco['cidade'])
						<p class="mb-1 fs-14">
							<span>Cidade:</span>
							<span class="color-555">
								{{ $endereco['cidade'] }}
							</span>
						</p>
						@endif
						@if($endereco['estado'])
						<p class="mb-1 fs-14">
							<span>Estado:</span>
							<span class="color-555">{{ $endereco['estado'] }}</span>
						</p>
						@endif
						@if($endereco['complemento'])
						<p class="mb-1 fs-14"> 
							<span>Complemento:</span>
							<span class="color-555">
								{{$endereco['complemento']}}
							</span>
						</p>
						@endif
						@if($endereco['ponto_referencia'])
						<p class="mb-1 fs-14"> 
							<span>Ponto de Referência:</span>
							<span class="color-555">
								{{$endereco['ponto_referencia']}}
							</span>
						</p>
						@endif

						<hr>

					@endif

					@if(count($categorias) > 0)
						@foreach($categorias as $categoria)
							<p class="mb-2 fs-13">
								<i class="fas fa-plus-circle color-777 fs-13" title="Categoria"></i>
								{{ $categoria->nome }}
							</p>
						@endforeach
						<hr/>
					@endif


					<p class="mb-1 fs-14 ls-05">
						<span>Último acesso:</span> 
						<span class="color-555">
					      	@if($user->data_ultimo_acesso)
					      		{{ \App\Models\Util::replaceDateTimePt($user->data_ultimo_acesso) }}
					      		({{ \Carbon\Carbon::createFromTimeStamp(strtotime($user->data_ultimo_acesso))->diffForHumans() }})
					      	@else
								Ainda não acessou
					      	@endif
						</span>
					</p>

					<p class="mb-1 fs-14 ls-05">
						<span>Data de Cadastro:</span> 
						<span class="color-555">{{ $user->created_at->format('d/m/Y H:i') }}</span>
					</p>

					@if($cadastrante)
					<p class="mb-1 fs-14 ls-05">
						<span>Cadastrante:</span> 
						<a href="/admin/colaborador/info/{{$cadastrante->id}}" class="color-555">
							{{ $cadastrante->nome }}
						</a>
					</p>
					@endif
					
				</div>

			</div>

		</div>

	</section>

	<section class="mb-4-5">
		
	
		<div class="section-title rounded-bottom-0">
			<a href="#boxPermissoes" data-bs-toggle="collapse" aria-expanded="false" class="hover-d-none text-white">
				Permissões de Acesso
			</a>
		</div>

		<div class="collapse show bg-white p-4 border" id="boxPermissoes">

            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">Módulo</th>
                            <th scope="col">Nível</th>
                        </tr>
                    </thead>
                    <tbody>
					  	@foreach($modulos as $modulo)
					    <tr>
					      	<td>{{ $modulo->texto }}</td>
					      	<td> 
								@switch($modulo->nivel)

								    @case(0)
								        Sem acesso
								    @break

								    @case(1)
								        Apenas visualizar
								    @break

								    @case(2)
								        Visualizar, cadastrar e editar
								    @break

									@case(3)
								        Visualizar, cadastrar, editar e excluir
								    @break

								@endswitch
					      	</td>
					    </tr>
					    @endforeach
                    </tbody>
                </table>
            </div>

		</div>

	</section>

	<form wire:submit.prevent="delete" method="POST">
		<div wire:ignore class="modal fade" id="ModalDeletarColaborador" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  		<div class="modal-dialog modal-dialog-centered">
	    		<div class="modal-content">
	      			<div class="modal-header">
	        			<h3 class="modal-title" id="exampleModalLabel">Deletar Colaborador</h3>
	        			<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
	      			</div>
	      			<div class="modal-body text-center py-5">
	        			<p class="m-0 fs-16">
	        				Você tem certeza que deseja deletar este colaborador?
	        				<br>
	        				Todas as informações serão perdidas definitivamente.
	        			</p>
	      			</div>
	      			<div class="modal-footer">
	      				<div class="d-flex w-100">
		      				<input type="password" wire:model="password_to_delete" class="w-100 form-control p-form me-3" placeholder="Digite sua senha" required>
		        			<button type="submit" class="btn btn-danger weight-600">Confirmar</button>
	    				</div>
	      			</div>
	    		</div>
	  		</div>
		</div>
	</form>

	@include('livewire.util.toast')
	
	<form method="POST" enctype="multipart/form-data" action="{{ route('admin.colaborador.info.setFotoPerfil', ['id' => $this->user->id]) }}">
	@csrf
		<input type="file" name="foto" style="display:none" id="input-file-foto" onchange="this.form.submit()" />
	</form>

	<script>
		
		function setFotoPerfil()
		{
			document.getElementById('input-file-foto').click();
		}

	</script>

</div>