<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AjudaChamadoController extends Controller
{
    public function lista()
    {
        $getNomePaginaInterno = 'Chamados';

        return view('admin.ajuda.chamado.lista', compact('getNomePaginaInterno') );
    }
    
    public function info($chamado_id)
    {
        $getNomePaginaInterno = 'Chamado';

        return view('admin.ajuda.chamado.info', compact('chamado_id','getNomePaginaInterno') );
    }

    public function add()
    {
        $getNomePaginaInterno = 'Adicionar Chamado';

    	return view('admin.ajuda.chamado.add', compact('getNomePaginaInterno') );
    }

}