<!doctype html>
<html lang="pt-br">
<head>

   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1">

   <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/uikit@3.5.9/dist/css/uikit.min.css" />
   <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
   <link rel="stylesheet" href="{{ asset('css/util.css') }}?v=4">
   <link rel="stylesheet" href="{{ asset('sites-proprios/cefaep2/css/home.css') }}?v=4">

   <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
   
   <title>{{ $curso->seo_title ?? $curso->nome }}</title>
   <meta name="robots" content="index, follow">
   <meta name="description" content="{{ $curso->seo_description ?? null }}"/>
   <meta name="keywords" content="{{ $curso->seo_keywords ?? null }}"/>
   <link rel="canonical" href="{{ Request::url() }}" />
   <link rel="shortcut icon" href="{{ $plataforma->favicon }}">
   <meta property="og:locale" content="pt_BR" />
   <meta property="og:type" content="website" />
   <meta property="og:title" content="{{ $curso->seo_title ?? $curso->nome }}" />
   <meta property="og:description" content="{{ $curso->seo_description ?? null }}" />
   <meta property="og:url" content="{{ Request::url() }}" />
   <meta property="og:site_name" content="{{ $plataforma->nome }}" />
   <meta property="og:image" content="{{ $plataforma->logotipo }}" />
   <meta name='author' content='WOZCODE | Pedro Henrique' />

</head>
<body class="bg-f4" id="inicio">

   @include('sites-proprios/cefaep2/componentes/header')

   <main>

      <section>
       <div class="w-100 h-200 background-size-cover background-position-center" style="background-image: url('{{ $curso->foto_capa ?? $plataforma->jumbotron }}">
          <div class="position-relative h-100 px-xl-7 px-3">
          <div class="background-overlay opacity-08 h-100" style="background: rgba(0,0,0,1);"></div>

             <div class="row align-items-center h-100 position-relative">
                <div class="mt-2">
                   <h1 class="text-light font-poppins weight-800 fs-xs-35 fs-md-50 text-shadow-3" id="cursoTiulo">
                      {!! $curso->nome !!}
                   </h1>
                </div>
             </div>
          </div>
       </div>
      </section>

      <section class="pb-lg-3">
         <div class="row container-fluid px-3 gx-0 gx-lg-4 px-xl-6 mt--15px position-relative pb-5">

            <div class="col-lg-4 mb-4 mb-lg-0">
               
               <div class="bg-fundo-faca-sua-inscricao px-4-5 pt-4-5 pb-4-5 box-shadow rounded-top-20 box-shadow">
                  
                  <div class="text-center">              
                     <a href="/f/formulario-de-matricula/63/t/{{$curso->id}}" target="_blank" class="btnCursoInscreverSe w-100 animation-grow box-shadow-warning hover-d-none">
                        FAÇA SUA<br>PRÉ-INSCRIÇÃO
                     </a>
                  </div>

               </div>

               <div class="bg-white px-4-5 pt-4-5 pb-4-5 rounded-top-0 rounded-bottom-20 box-shadow">

                  <div class="bg-investimento px-4-5 pt-4 pb-5 rounded box-shadow">

                     <h1 class="p-0 m-0 text-light font-inter text-center fs-25 text-shadow-1 weight-700">Investimento</h1>

                     <hr class="border">

                     <p class="mb-3 weight-700 font-montserrat text-dark fs-17 text-shadow-1" style="text-decoration: line-through;">
                        De R$ {{ $curso->valor_a_vista ?? '0.000,00' }}
                     </p>
                     <span class="text-light weight-600 font-montserrat d-block" style="margin-bottom: -10px;">Por</span>
                     <div class="d-flex">
                      <p class="m-0 pe-2 fs-40 font-montserrat weight-700 text-light ls-05 text-shadow-1">
                          R$
                      </p>
                       <p class="mb-0 fs-40 font-montserrat weight-800 text-light ls-05 text-shadow-1">
                          {{ $curso->valor_promocional ?? 'N/D' }}
                       </p>
                     </div>

                  </div>

                  <div class="text-center mt--20px">
                    <span class="color-promo bg-promo px-3-5 py-2 rounded box-shadow weight-600 font-poppins ls-05 fs-18">
                      PROMOCIONAL
                    </span>
                  </div>

                  <div class="pt-5">
                    
                    <h1 class="weight-600 font-roboto weight-800 fs-22 text-center color-titulo">Formas de Pagamento</h1>

                    <div class="w-100 box-shadow mb-4-5 d-flex align-items-center">
                      <button type="button" class="btn-forma-pg-cc bg-forma-pg border-forma-pg w-100 weight-700 font-poppins text-shadow-1 fs-18 ls-05 px-3 rounded-0 height-categorias text-light" style="height: 60px;">
                         Cartão de Crédito
                      </button>
                      <button type="button" class="btn-forma-pg-bo border-forma-pg color-forma-pg w-100 weight-700 font-poppins bg-light fs-18 ls-05 px-3 rounded-0 height-categorias text-light" style="height: 60px;">
                         Boleto
                      </button>
                    </div>

                    <div>

                      @php
                        $formasPagamento = \App\Models\FormaPagamento::plataforma()->trilha()->where('registro_id', $curso->id ?? 0)->get();
                      @endphp

                      <div id="boxFormaPagamentoCC"> 
                        @foreach($formasPagamento->where('tipo','CC') as $formaPagamento)
                          <div class="bg-f4 mb-1 px-3-5 py-2 rounded-2">
                            <p class="m-0 font-montserrat ls-05 weight-800 fs-20 text-center">
                              {{ $formaPagamento->parcela }}x de R$ {{ $formaPagamento->valor }}
                            </p>
                          </div>
                        @endforeach
                      </div>

                      <div id="boxFormaPagamentoBoleto" style="display: none;"> 
                        @foreach($formasPagamento->where('tipo','BO') as $formaPagamento)
                          <div class="bg-f4 mb-1 px-3-5 py-2 rounded-2">
                            <p class="m-0 font-montserrat ls-05 weight-800 fs-20 text-center">
                              {{ $formaPagamento->parcela }}x de R$ {{ $formaPagamento->valor }}
                            </p>
                          </div>
                        @endforeach
                      </div>

                    </div>

                  </div>

               </div>

               @if($curso->video)
                <div class="mt-4 bg-white px-4-5 pt-4-5 pb-4-5 rounded-top-20 rounded-bottom-20 box-shadow">

                  <h1 class="mb-3 weight-600 font-roboto weight-800 fs-20 color-titulo">Apresentação em Vídeo</h1>

                  <iframe width="100%" height="200" class="rounded" src="{{ \App\Models\CursoAula::getVideoYoutubeEmbed($curso->video) }}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

               </div>
               @endif

               @if(isset($coordenador))
              <div class="mt-4 bg-white px-4-5 pt-4-5 pb-4-5 rounded-top-20 rounded-bottom-20 box-shadow">

                <h1 class="weight-600 font-roboto weight-800 fs-20 color-titulo">Coordenador</h1>

                <div class="mt-3">
                   <div class="d-flex align-items-center justify-content-between">

                      <div class="d-flex align-items-center">
                        <img src="{{ $coordenador->foto_perfil ?? asset('images/no-image.jpeg') }}" alt="{{ $coordenador->nome ?? 'Não definido' }} | Corpo Docente" class="me-2 img-fluid square-60 rounded-circle border object-fit-cover">
                        
                        <div>
                           <h1 class="font-poppins m-0 p-0 weight-600 fs-15 color-grafite">
                              {{ \App\Models\Util::getPrimeiroSegundoNome($coordenador->nome ?? 'Não definido') }}
                           </h1>
                           <p class="mb-0 font-inter weight-500 fs-14 text-secondary">
                              {{ $coordenador->naturalidade ?? 'Titulação N/D' }}
                           </p>
                        </div>
                      </div>

                      <div>

                        <a href="https://api.whatsapp.com/send?phone=55{{preg_replace('/\D/', '', $coordenador->celular ?? '(00) 00000-0000' )}}&text=Olá, vim através do Site da CEFAEP" target="_blank" class="btn btn-success bg-wpp border-0 box-shadow py-2 text-light px-2-5 weight-600 hover-d-none">
                          <div class="d-flex align-items-center justify-content-between">
                            <span class="me-2 fs-13 d-xs-none">WhatsApp</span>
                            <i class="fab fa-whatsapp fs-13"></i>
                          </div>
                        </a>

                      </div>

                   </div>

                   <div class="ps-1 mt-1">
                      <p class="font-poppins m-0 weight-600 color-grafite fs-14">
                         Formação
                      </p>
                      <p class="mb-0 font-inter weight-500 fs-14 text-secondary">
                        {{ $coordenador->descricao ?? 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.' }}
                      </p>
                   </div>
                </div>

              </div>
              @endif

               @if(count($corpoDocente) > 0)
                <div class="mt-4 bg-white px-4-5 pt-4-5 pb-4-5 rounded-top-20 rounded-bottom-20 box-shadow">

                  <h1 class="weight-600 font-roboto weight-800 fs-20 color-titulo mb-4">Corpo Docente</h1>

                  <div>
                    
                    @foreach($corpoDocente as $user)
                     <div class="d-flex align-items-center">

                        <img src="{{ $user->foto_perfil ?? asset('images/no-image.jpeg') }}" alt="{{ $user->nome }} | Corpo Docente" class="me-2 img-fluid square-60 rounded-circle border object-fit-cover">
                        
                        <div>
                           <h1 class="font-poppins m-0 p-0 weight-600 fs-15 color-grafite">
                              {{ $user->nome }}
                           </h1>
                           <p class="mb-0 font-inter weight-500 fs-14 text-secondary">
                              {{ $user->titulacao ?? 'Titulação N/D' }}
                           </p>
                        </div>

                     </div>

                     <div class="ps-1 mt-1">
                        <p class="font-poppins m-0 weight-600 color-grafite fs-14">
                           Formação
                        </p>
                        <p class="mb-0 font-inter weight-500 fs-14 text-secondary">
                          {!! nl2br($user->formacao) ?? 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.' !!}
                        </p>
                     </div>

                     <hr class="mt-3">

                    @endforeach

                  </div>

                </div>
                @endif

            </div>

            <div class="col-lg-8">

               <section class="mb-4 bg-white px-lg-5 px-4 pt-4-5 pb-4-5 box-shadow border-top rounded-20">

                  <p class="mb-0 pb-0 font-poppins weight-600 fs-14 color-666">
                     Informações do Curso
                  </p>

                  <hr class="border border-secondary">

                   <div class="d-lg-flex justify-content-between">
                      <div class="mb-3 mb-lg-0">
                          <p class="fs-15 mb-0 weight-600 font-poppins color-555">NÍVEL:</p>
                          <p class="fs-14 mb-0 weight-600 font-poppins color-444">{{ $curso->tipo_formacao ?? 'Não definido' }} ({{ $curso->nivel ?? 'Não definido' }})</p>
                      </div>

                      <div class="mb-3 mb-lg-0">
                          <p class="fs-15 mb-0 weight-600 font-poppins color-555">DURAÇÃO:</p>
                          <p class="fs-14 mb-0 weight-600 font-poppins color-444">{{ $curso->duracao ?? 'Não definido' }}</p>
                      </div>

                      <div class="mb-3 mb-lg-0">
                          <p class="fs-15 mb-0 weight-600 font-poppins color-555">CARGA HORÁRIA:</p>
                          <p class="fs-14 mb-0 weight-600 font-poppins color-444">{{ $curso->carga_horaria ?? '0.000' }} horas</p>
                      </div>

                      <div class="mb-3 mb-lg-0">
                          <p class="fs-15 mb-0 weight-600 font-poppins color-555">MATRIZ CURRICULAR:</p>
                           <a href="{{ $curso->matriz_curricular }}" target="_blank" class="fs-14 mb-0 weight-600 font-poppins color-blue-info-link">
                              @if($curso->matriz_curricular)
                                 Visualizar
                              @else
                                 Não definido
                              @endif
                           </a>
                      </div>

                    </div>

               </section>

               @foreach($secoesDescritivas as $secao)
               <section class="mb-3">
                 <a href="#" class="hover-d-none color-444" data-bs-toggle="collapse" data-bs-target="#secaoDesc{{$secao->id}}" aria-expanded="false" aria-controls="collapseExample">
                   <div class="bg-white px-lg-5 p-3 box-shadow border-top rounded-20">

                      <h1 class="py-0 my-0 font-poppins weight-600 fs-17 color-555">
                         {{ $secao->titulo }}
                      </h1>

                     <div class="collapse @if($secao->expandido == 'S') show @endif " id="secaoDesc{{$secao->id}}">
                         <hr class="border border-secondary">
                         <p class="m-0 fs-15 color-444 weight-500 font-poppins">
                           {!! nl2br($secao->descricao) !!}
                         </p>
                     </div>

                   </div>
                 </a>
               </section>
               @endforeach

            </div>

         </div>
      </section>

   </main>

   @include('sites-proprios/cefaep2/componentes/footer')

   <a href="https://api.whatsapp.com/send?phone=55{{preg_replace('/\D/', '', $plataforma->whatsapp)}}&text=Olá, vim através do Site {{ $plataforma->nome }}" target="_blank" class="text-white fs-30">
       <div class="fixed-bottom right-0 me-3 mb-3 z-3">
           <div class="square-60 rounded-circle bg-wpp flex-center box-shadow-wpp">
               <i class="fab fa-whatsapp"></i>
           </div>
       </div>
   </a>

   <script src="https://cdn.jsdelivr.net/npm/uikit@3.5.9/dist/js/uikit.min.js"></script>
   <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-U1DAWAznBHeqEIlVSCgzq+c9gqGAJn5c/t99JyeKa9xxaYpSvHU5awsuZVVFIhvj" crossorigin="anonymous"></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.1/jquery.min.js" integrity="sha512-aVKKRRi/Q/YV+4mjoKBsE4x3H+BkegoM/em46NNlCqNTmUYADjBbeNefNxYV7giUp0VxICtqdrbqU7iVaeZNXA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
   <script>

      $(".btn-forma-pg-bo").click(function() {

        $(".btn-forma-pg-cc").removeClass('bg-forma-pg').removeClass('text-light').addClass('bg-light').addClass('color-forma-pg');
        $(this).removeClass('bg-light').removeClass('color-forma-pg').addClass('bg-forma-pg').addClass('text-light');

        $("#boxFormaPagamentoCC").hide();
        $("#boxFormaPagamentoBoleto").show();
      });

      $(".btn-forma-pg-cc").click(function() {

        $(".btn-forma-pg-bo").removeClass('bg-forma-pg').removeClass('text-light').addClass('bg-light').addClass('color-forma-pg');
        $(this).removeClass('bg-light').removeClass('color-forma-pg').addClass('bg-forma-pg').addClass('text-light');

        $("#boxFormaPagamentoCC").show();
        $("#boxFormaPagamentoBoleto").hide();
      });

      window.onscroll = function() {myFunction()};

      var header = document.getElementById("menu");
      var sticky = header.offsetTop;

      function myFunction() {
        if (window.pageYOffset > sticky) {
          header.classList.add("fixed-top");
        } else {
          header.classList.remove("fixed-top");
        }
      }
   </script>

</body>
</html>