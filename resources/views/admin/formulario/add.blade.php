@extends('layouts.admin.master')

@section('css')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.14.0-beta2/dist/css/bootstrap-select.min.css">
@endsection

@section('content')
    
<div>

    <form method="POST" class="mb-4">
    @csrf

		<section class="mb-4-5">
			
			<a href="#boxInfoPrincipal" data-bs-toggle="collapse" aria-expanded="false" class="hover-d-none">
				<div class="section-title">
					Informações Principais
				</div>
			</a>
			<hr/>
			
			<div class="collapse show" id="boxInfoPrincipal">

		    	<div class="d-lg-flex mb-3">

			    	<div class="w-100 me-lg-3 my-3 my-lg-0">
			    		<label>Referência</label>
			    		<input type="text" name="referencia" id="referencia" class="form-control p-form" placeholder="Ex: Formulário de Matrícula" required>
			    	</div>
			    	<div class="w-100 me-lg-3 my-3 my-lg-0">
			    		<label>Slug</label>
			    		<input type="text" name="slug" id="slug" onfocus="convertToSlug(document.getElementById('referencia').value)" class="form-control p-form" placeholder="Ex: inscrevase-agora" required>
			    	</div>
			    	<div class="w-lg-35">
			    		<label>Status</label>
			    		<select name="status" class="select-form">
			    			<option value="0">Ativo</option>
			    			<option value="1">Desativado</option>
			    		</select>
			    	</div>

			    </div>

	            @if(\App\Models\Unidade::checkActive() === true)
	            <div class="mb-3">
	            	<label>Unidade</label>
	                <select name="unidade_id" id="unidade_id" class="selectpicker" data-width="100%" data-live-search="true">
	                    <option value="">Selecione</option>
	                    @foreach($unidades as $unidade)
	                        <option value="{{ $unidade->id }}">
	                        	{{ $unidade->nome }}
	                        </option>
	                    @endforeach
	                </select>
	            </div>
	            @endif

			    <div class="mb-3">
			    	<label>Texto</label>
			    	<textarea name="texto" class="descricao textarea-form"></textarea>
			    </div>

		    	<div class="mb-1">
		    		<label>Texto de retorno após envio</label>
		    		<textarea name="texto_retorno" class="descricao textarea-form"></textarea>
		    	</div>

		    	<div class="mb-3">
		    		<div class="d-flex">
		    			<input type="text" name="texto_botao_retorno" class="form-control p-form rounded-right-0" placeholder="Texto do Botão de Retorno após envio">
		    			<input type="text" name="link_retorno" class="form-control p-form rounded-left-0" placeholder="Link de Redirecionamento após envio">
			    	</div>
			    </div>

			</div>

		</section>

		<section class="mb-4-5">
			
			<a href="#boxCategorias" data-bs-toggle="collapse" aria-expanded="false" class="hover-d-none">
				<div class="section-title">
					Categorias 
				</div>
			</a>
			<hr/>

			<div class="collapse" id="boxCategorias">
				<select name="categorias[]" class="selectpicker" multiple data-width="100%" data-live-search="true" title="Selecione">
					@foreach($categorias as $categoria)
						<option value="{{ $categoria->id }}">{{ $categoria->nome }}</option>
					@endforeach
				</select>
			</div>

		</section>

		<section class="mb-4-5">
			
			<a href="#boxIntegracoes" data-bs-toggle="collapse" aria-expanded="false" class="hover-d-none">
				<div class="section-title">
					Integrações 
				</div>
			</a>
			<hr/>

			<div class="collapse" id="boxIntegracoes">
				
				<div class="d-lg-flex mb-3">

					<input type="hidden" name="integracao[email][ref]" value="email">

					<div class="w-100 me-lg-3 mb-3 mb-lg-0">
						<label>Receber Email de Inscrições</label>
						<select name="integracao[email][status]" onchange="if(this.value == 1)document.getElementById('email_value').focus();" class="select-form">
							<option value="0">Não</option>
							<option value="1">Sim</option>
						</select>
					</div>

					<div class="w-100">
						<label>Email de recebimento</label>
						<input type="email" name="integracao[email][value]" id="email_value" class="form-control p-form" value="{{ old('integracao.email.value') ?? $plataforma->email }}" placeholder="Digite o email que será notificado de novos contatos">
					</div>

				</div>

				<div class="d-lg-flex mb-3">

					<input type="hidden" name="integracao[hubspot][ref]" value="hubspot">

					<div class="w-100 me-lg-3 mb-3 mb-lg-0">
						<label>Hubspot</label>
						<select name="integracao[hubspot][status]" onchange="if(this.value == 1)document.getElementById('hubspot_value').focus();" class="select-form">
							<option value="0">Não</option>
							<option value="1">Sim</option>
						</select>
					</div>

					<div class="w-100">
						<label>Chave de API Hubspot</label>
						<input type="text" name="integracao[hubspot][value]" id="hubspot_value" class="form-control p-form" value="{{ old('integracao.hubspot.value') }}" placeholder="Digite a Chave API da conta do Hubspot que deseja integrar">
					</div>

				</div>

			</div>

		</section>

		<button type="submit" name="sent" value="ok" class="btn-add mt-3">Cadastrar</button>	

    </form>

</div>

@endsection

@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.14.0-beta2/dist/js/bootstrap-select.min.js"></script>
<script src="https://cdn.tiny.cloud/1/f50dygt4s4q2k3urd4bh4c2hkjb74m6exr798tjwbayaa0om/tinymce/5/tinymce.min.js"></script>
<script>
	
    function convertToSlug(string)
    { 
      string = string.replace(/^\s+|\s+$/g, ''); 
      string = string.toLowerCase();
      
      let from = "çãáàíéúó";
      let to   = "caaaieuo";

      for (let i=0, l=from.length ; i<l ; i++) {
        string = string.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
      }

      slug = string.replace(/[^a-z0-9 -]/g, '') 
        .replace(/\s+/g, '-') 
        .replace(/-+/g, '-'); 

      document.getElementById('slug').value = slug;
    }

	@if(\Auth::user()->restringir_unidade == 'S' && \Auth::user()->unidade_id != null)
		jQuery("#unidade_id option:contains('Selecione')").remove();
	@endif

</script>
<script>
	tinymce.init({
		selector: '.descricao',
		language: 'pt_BR',
		height: 300,
		plugins: 'image lists link code',
		toolbar: "image | undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link | code"
	});
</script>
@endsection