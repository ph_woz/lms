<!doctype html>
<html lang="pt-br">
<head>

   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1">

   <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/uikit@3.5.9/dist/css/uikit.min.css" />
   <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
   <link rel="stylesheet" href="{{ asset('css/util.css') }}?v=3">
   <link rel="stylesheet" href="{{ asset('sites-proprios/faro/css/home.css') }}?v=3">

   <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">

   <title>{{ $seo->seo_title ?? $plataforma->nome }}</title>
   <meta name="robots" content="index, follow">
   <meta name="description" content="{{ $seo->seo_description ?? null }}"/>
   <meta name="keywords" content="{{ $seo->seo_keywords ?? null }}"/>
   <link rel="canonical" href="{{ Request::url() }}" />
   <link rel="shortcut icon" href="{{ $plataforma->favicon }}">
   <meta property="og:locale" content="pt_BR" />
   <meta property="og:type" content="website" />
   <meta property="og:title" content="{{ $seo->seo_title ?? $plataforma->nome }}" />
   <meta property="og:description" content="{{ $seo->seo_description ?? null }}" />
   <meta property="og:url" content="{{ Request::url() }}" />
   <meta property="og:site_name" content="{{ $plataforma->nome }}" />
   <meta property="og:image" content="{{ $plataforma->logotipo }}" />
   <meta name='author' content='WOZCODE | Pedro Henrique' />

</head>
<body class="bg-f4" id="inicio">

   @include('sites-proprios/faro/componentes/header')

   <main class="pb-5">

      <section class="w-100 h-lg-450 background-size-cover background-position-center" style="background-image: url('{{ $plataforma->jumbotron ?? asset('sites-proprios/faro/images/jumbotron.jpeg') }}');">
              <div class="position-relative h-100">
              <div class="background-overlay opacity-08 h-100" style="background: rgba(0,0,0,1);"></div>
               <div class="container h-100">
                 <div class="row container align-items-center h-100 position-relative">
                    <div class="mt--lg-75px">
                       <h1 class="text-light font-poppins weight-800 fs-xs-35 fs-md-50 text-shadow-3 col-lg-6 py-5 py-lg-0" id="tituloFIES">
                          Fundo de Financiamento ao Estudante do Ensino Superior (FIES)
                       </h1>
                    </div>
                 </div>
              </div>
            </div>
      </section>

      <section class="py-5 z-3 position-relative mt--lg-125px">
         <div class="container">
            <div class="p-lg-5 p-4 bg-white box-shadow border rounded-20">
               <div class="row">
                  <div class="col-lg-7">
                     <h1 class="color-base font-poppins weight-800 fs-35">Como Funciona</h1>
                     <p class="font-opensans fs-22 color-555">
                       O Fundo de Financiamento Estudantil (FIES) é um programa do Ministério da Educação (MEC), que tem como objetivo conceder financiamento a estudantes em cursos superiores não gratuitos. A partir de 2018, o FIES possibilita juros zero a quem mais precisa e uma escala de financiamento que varia conforme a renda familiar do candidato.
                     </p>
                     <div class="mt-4 mb-3 mb-lg-0">
                        <a href="/f/formulario-de-matricula/45/t/631" target="_blank" class="bg-faixa bg-hover-gradient fs-14 text-center px-3-5 rounded box-shadow text-light weight-700 py-2-5 font-poppins hover-d-none d-xs-block">
                           QUERO ME INSCREVER
                        </a>
                        <a href="https://api.whatsapp.com/send?phone=55{{preg_replace('/\D/', '', $plataforma->whatsapp)}}&text=Olá, vim através do Site {{ $plataforma->nome }}" target="_blank" class="ms-lg-3 bg-wpp text-shadow-1 fs-14 text-center px-3-5 rounded box-shadow text-light weight-700 py-2-5 font-poppins hover-d-none d-xs-block mt-2 mt-lg-0">
                           <span class="me-2">QUERO RECEBER UMA MENSAGEM</span>
                           <i class="fab fa-whatsapp d-xs-none"></i>
                        </a>
                     </div>
                  </div>
                  <div class="col-lg-5 text-xs-center">
                     <div class="mt--lg-175px">

                        <div class="mb-4">
                           <img src="{{ asset('sites-proprios/faro/images/mulher.jpg') }}" alt="Faro - Mulher" class="img-fluid rounded-20">
                        </div>

                        <div class="d-lg-flex align-items-center justify-content-end">

                           <div class="mb-3 mb-lg-0 me-lg-4">
                              <p class="mb-0 color-azul weight-700 font-poppins">
                                 Compartilhe com um amigo
                              </p>
                           </div>
                           <div class="d-flex justify-content-center">
                              <a href="{{ $plataforma->facebook }}" target="_blank" class="hover-d-none">
                                 <div class="bg-faixa square-40 flex-center rounded-circle">
                                    <i class="fab fa-facebook-f color-icon-footer fs-20"></i>
                                 </div>
                              </a>
                              <a href="" target="_blank" class="mx-2 hover-d-none">
                                 <div class="bg-faixa square-40 flex-center rounded-circle">
                                    <i class="fab fa-linkedin-in color-icon-footer fs-20"></i>
                                 </div>
                              </a>
                              <a href="" target="_blank" class="hover-d-none">
                                 <div class="bg-faixa square-40 flex-center rounded-circle">
                                    <i class="fab fa-whatsapp color-icon-footer fs-20"></i>
                                 </div>
                              </a>
                           </div>

                        </div>

                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>

      <section class="p-lg-5 p-3 bg-f5">
         <div class="container">

            <h1 class="text-center text-dark font-poppins weight-700">
               Quais são os requisitos necessários?
            </h1>

            <div class="mt-5 row align-items-center">

               <div class="col-lg-4">

                  <div class="mb-4 d-flex align-items-center bg-white box-shadow rounded-20 p-4">
                     <i class="far fa-check-circle fs-30 me-3 color-azul"></i>
                     <p class="mb-0 font-poppins weight-600 fs-17">
                        Ter participado do ENEM, a partir da edição de 2010.
                     </p>
                  </div>
                  <div class="mb-4 d-flex align-items-center bg-white box-shadow rounded-20 p-4">
                     <i class="far fa-check-circle fs-30 me-3 color-azul"></i>
                     <p class="mb-0 font-poppins weight-600 fs-17">
                        Ter obtido média no ENEM igual ou superior a 450 pontos e nota maior que zero na redação.
                     </p>
                  </div>
                  <div class="mb-4 d-flex align-items-center bg-white box-shadow rounded-20 p-4">
                     <i class="far fa-check-circle fs-30 me-3 color-azul"></i>
                     <p class="mb-0 font-poppins weight-600 fs-17">
                        Possuir renda familiar mensal bruta, por pessoa, de até no máximo três salários mínimos.
                     </p>
                  </div>
                  <div class="mb-4 d-flex align-items-center bg-white box-shadow rounded-20 p-4">
                     <i class="far fa-check-circle fs-30 me-3 color-azul"></i>
                     <p class="mb-0 font-poppins weight-600 fs-17">
                        Não possuir financiamento FIES ativo.
                     </p>
                  </div>

               </div>

               <div class="col-lg-8" align="right">

                  <img src="{{ asset('sites-proprios/faro/images/plataforma-elearning.png') }}" aclass="img-fluid">

               </div>

            </div>

         </div>
      </section>

      <section class="p-lg-5 p-3 mb-5 bg-f5">
         <div class="container">

            <h2 class="text-center text-dark font-poppins weight-500 fs-20">Confira o passo a passo</h2>
            <h1 class="text-center text-dark font-poppins weight-700">
               Como participar do FIES
            </h1>

            <div class="row align-items-center" style="margin-top: 75px;">

               <div class="col-lg-3 mb-5 mb-lg-0">

                  <div class="bg-white p-4 box-shadow rounded-10 h-lg-350">
                     <div class="text-center">

                        <div class="d-flex justify-content-center">
                           <div class="square-75 rounded-circle bg-gradient flex-center mt--60px">
                              <span class="fs-50">1</span>
                           </div>
                        </div>

                        <h1 class="mb-0 fs-25 weight-700 mt-3 font-opensans">
                           Inscreva-se
                        </h1>

                        <div class="flex-center mt-3"><div class="bg-gradient" style="height: 2px;width: 50px;"></div></div>

                        <p class="mt-3 weight-600 font-opensans">
                           Clique aqui entre os dias 09 a 12 de agosto, para participar do Processo Seletivo FIES 2º/2022.
                        </p>
                     
                     </div>
                  </div>

               </div>

               <div class="col-lg-3 mb-5 mb-lg-0">

                  <div class="bg-white p-4 box-shadow rounded-10 h-lg-350">
                     <div class="text-center">

                        <div class="d-flex justify-content-center">
                           <div class="square-75 rounded-circle bg-gradient flex-center mt--60px">
                              <span class="fs-50">2</span>
                           </div>
                        </div>

                        <h1 class="mb-0 fs-25 weight-700 mt-3 font-opensans">
                           Fique de olho
                        </h1>

                        <div class="flex-center mt-3"><div class="bg-gradient" style="height: 2px;width: 50px;"></div></div>

                        <p class="mt-3 weight-600 font-opensans">
                           Acompanhe o resultado do processo seletivo de aprovação da chamada única que ficará disponível dia 16 de agosto, tudo pelo Portal Único de Acesso ao Ensino Superior.
                        </p>
                     
                     </div>
                  </div>

               </div>

               <div class="col-lg-3 mb-5 mb-lg-0">

                  <div class="bg-white p-4 box-shadow rounded-10 h-lg-350">
                     <div class="text-center">

                        <div class="d-flex justify-content-center">
                           <div class="square-75 rounded-circle bg-gradient flex-center mt--60px">
                              <span class="fs-50">3</span>
                           </div>
                        </div>

                        <h1 class="mb-0 fs-25 weight-700 mt-3 font-opensans">
                           Complementação de inscrição
                        </h1>

                        <div class="flex-center mt-3"><div class="bg-gradient" style="height: 2px;width: 50px;"></div></div>

                        <p class="mt-3 weight-600 font-opensans">
                           Se tiver sido pré-selecionado na chamada única do FIES, é só complementar sua inscrição no Portal Único entre os dias 17 a 19 de agosto.
                        </p>
                     
                     </div>
                  </div>

               </div>

               <div class="col-lg-3 mb-5 mb-lg-0">

                  <div class="bg-white p-4 box-shadow rounded-10 h-lg-350">
                     <div class="text-center">

                        <div class="d-flex justify-content-center">
                           <div class="square-75 rounded-circle bg-gradient flex-center mt--60px">
                              <span class="fs-50">4</span>
                           </div>
                        </div>

                        <h1 class="mb-0 fs-25 weight-700 mt-3 font-opensans">
                           Convocação Lista de Espera
                        </h1>

                        <div class="flex-center mt-3"><div class="bg-gradient" style="height: 2px;width: 50px;"></div></div>

                        <p class="mt-3 weight-600 font-opensans">
                           Não foi pré-selecionado na chamada única? Então você já está na lista de espera. Acompanhe o site entre os dias 22 de agosto a 22 de setembro.
                        </p>
                     
                     </div>
                  </div>

               </div>

            </div>

         </div>
      </section>

      <section class="p-lg-5 p-3 mb-lg-5 mb-3 bg-gradient">
         <div class="container pb-3 pb-lg-0">

            <h2 class="text-center text-dark font-poppins weight-500 fs-20">Confira informações do</h2>
            <h1 class="text-center text-dark font-poppins weight-700">
               Edital do FIES
            </h1>

            <div id="myGroup">
               @foreach($secoes as $secao)
                  <button class="btn-info m-2 outline-0" type="button" data-bs-toggle="collapse" data-bs-target="#collapseSecao{{$secao->id}}" aria-expanded="false" aria-controls="collapseWidthExample">
                     {{ $secao->titulo }}
                  </button>
               @endforeach

               @foreach($secoes as $secao)            
               <div class="collapse" id="collapseSecao{{$secao->id}}">
                  <div class="bg-white p-lg-5 p-4 rounded-20 box-shadow mt-5">

                     <h1 class="color-base font-poppins weight-800 fs-35">{{ $secao->titulo }}</h1>
                     <p class="font-opensans fs-21 color-555">
                        {!!  nl2br($secao->descricao) !!}
                     </p>

                  </div>
               </div>
               @endforeach
            </div>

         </div>
      </section>

      <section class="py-5">
         <div class="container">
            <div class="bg-gradient rounded-top-20 box-shadow p-5">
               <h1 class="text-dark font-poppins weight-800 fs-35">Transfira-se</h1>
                <h2 class="text-dark font-poppins weight-500 fs-25">também com FIES</h2>
            </div>
            <div class="bg-white p-lg-4 px-1 pb-1 box-shadow">
               <p class="p-4 font-opensans fs-21 color-555">
                  A Transferência é disponibilizada apenas para o estudante que ainda não realizou o Aditamento de Renovação do semestre em questão. Antes de solicitar Transferência é necessário ter ocorrido o devido aditamento de renovação ou suspensão do semestre anterior, caso o semestre em questão não tenha sido o semestre de contratação.
               </p>
            </div>
            <div class="bg-gradient rounded-bottom-20 box-shadow p-lg-5 p-4">
               <div class="d-lg-flex justify-content-between align-items-center">
                  
                  <div class="w-100">
                  
                     <h2 class="text-dark font-poppins weight-500 fs-20">
                        FIES Legado
                     </h2>
                     
                     <p class="text-dark font-poppins weight-500">
                        - Contratações firmadas até 2º.2017; - Juros anuais de 6,5% ao ano; - Juros trimestrais de até R$150,00; - Financiamento até 100%; - Prazo de carência de 18 (dezoito) meses; - Percentual não financiado, pago diretamente para a IES; - Agente Operador - FNDE; - Agentes bancários - Caixa ou Banco do Brasil.
                     </p>

                  </div>

                  <div class="d-xs-none px-lg-5">
                     <div class="h-lg-300 w-2px bg-white"></div>
                  </div>

                  <div class="my-4 d-lg-none px-lg-5">
                     <div class="w-100 h-2px bg-white"></div>
                  </div>

                  <div class="w-100">
                  
                     <h2 class="text-dark font-poppins weight-500 fs-20">
                        Novo FIES
                     </h2>
                     
                     <p class="text-dark font-poppins weight-500">
                        - Contratações firmadas a partir de 1º.2018; - Juros zero; - Escala de financiamento que varia conforme a renda familiar; - Início do pagamento no mês seguinte à conclusão do curso, com prestações respeitando o seu limite de renda; - Percentual não financiado após assinatura do contrato, pago diretamente para a Caixa Econômica (Coparticipação); - Agente Operador - Caixa Econômica; - Agente bancário - Caixa Econômica.
                     </p>

                  </div>

               </div>
            </div>
         </div>
      </section>

   </main>

   @include('sites-proprios/faro/componentes/footer')

   <a href="https://api.whatsapp.com/send?phone=55{{preg_replace('/\D/', '', $plataforma->whatsapp)}}&text=Olá, vim através do Site {{ $plataforma->nome }}" target="_blank" class="text-white fs-30">
       <div class="fixed-bottom right-0 me-3 mb-3 z-3">
           <div class="square-60 rounded-circle bg-wpp flex-center box-shadow-wpp">
               <i class="fab fa-whatsapp"></i>
           </div>
       </div>
   </a>

   <script src="https://cdn.jsdelivr.net/npm/uikit@3.5.9/dist/js/uikit.min.js"></script>
   <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-U1DAWAznBHeqEIlVSCgzq+c9gqGAJn5c/t99JyeKa9xxaYpSvHU5awsuZVVFIhvj" crossorigin="anonymous"></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>
   <script>

      var $myGroup = $('#myGroup');
      $myGroup.on('show.bs.collapse','.collapse', function() {
         $myGroup.find('.collapse.show').collapse('hide');
      });

      window.onload = function()
      {
         $(".btn-info").first().click().focus();
         window.scrollTo(0, 0);
      };

      window.onscroll = function() {myFunction()};

      var header = document.getElementById("menu");
      var sticky = header.offsetTop;

      function myFunction() {
        if (window.pageYOffset > sticky) {
          header.classList.add("fixed-top");
        } else {
          header.classList.remove("fixed-top");
        }
      }
   </script>

</body>
</html>