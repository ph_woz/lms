@extends('layouts.admin.master')

@section('content')
<div>

    <div class="row">
        <div class="col-12">

            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col">Página</th>
                                    <th scope="col">URL</th>
                                    <th scope="col">
                                        <span class="float-end">
                                            Ações
                                        </span>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                    <tr>
                                        <td>Home</td>
                                        <td>
                                            <a href="/" target="_blank" class="color-blue-info-link">
                                                /
                                            </a>
                                        </td>
                                        <td>
                                            <span class="float-end">
                                                <a href="{{ route('admin.pagina.editar.home') }}" class="btn bg-base text-white">
                                                    <i class="fa fa-edit"></i>
                                                </a>
                                            </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Blog</td>
                                        <td>
                                            <a href="/blog" target="_blank" class="color-blue-info-link">
                                                /blog
                                            </a>
                                        </td>
                                        <td>
                                            <span class="float-end">
                                                <a href="{{ route('admin.pagina.editar.blog') }}" class="btn bg-base text-white">
                                                    <i class="fa fa-edit"></i>
                                                </a>
                                            </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Parceiros</td>
                                        <td>
                                            <a href="/parceiros" target="_blank" class="color-blue-info-link">
                                                /parceiros
                                            </a>
                                        </td>
                                        <td>
                                            <span class="float-end">
                                                <a href="{{ route('admin.pagina.editar.parceiros') }}" class="btn bg-base text-white">
                                                    <i class="fa fa-edit"></i>
                                                </a>
                                            </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Biblioteca</td>
                                        <td>
                                            <a href="/biblioteca" target="_blank" class="color-blue-info-link">
                                                /biblioteca
                                            </a>
                                        </td>
                                        <td>
                                            <span class="float-end">
                                                <a href="{{ route('admin.pagina.editar.biblioteca') }}" class="btn bg-base text-white">
                                                    <i class="fa fa-edit"></i>
                                                </a>
                                            </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Editais</td>
                                        <td>
                                            <a href="/editais" target="_blank" class="color-blue-info-link">
                                                /editais
                                            </a>
                                        </td>
                                        <td>
                                            <span class="float-end">
                                                <a href="{{ route('admin.pagina.editar.editais') }}" class="btn bg-base text-white">
                                                    <i class="fa fa-edit"></i>
                                                </a>
                                            </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>DOU</td>
                                        <td>
                                            <a href="/dou" target="_blank" class="color-blue-info-link">
                                                /dou
                                            </a>
                                        </td>
                                        <td>
                                            <span class="float-end">
                                                <a href="{{ route('admin.pagina.editar.dou') }}" class="btn bg-base text-white">
                                                    <i class="fa fa-edit"></i>
                                                </a>
                                            </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Fies</td>
                                        <td>
                                            <a href="/fies" target="_blank" class="color-blue-info-link">
                                                /fies
                                            </a>
                                        </td>
                                        <td>
                                            <span class="float-end">
                                                <a href="{{ route('admin.pagina.editar.fies') }}" class="btn bg-base text-white">
                                                    <i class="fa fa-edit"></i>
                                                </a>
                                            </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>ProUni</td>
                                        <td>
                                            <a href="/prouni" target="_blank" class="color-blue-info-link">
                                                /prouni
                                            </a>
                                        </td>
                                        <td>
                                            <span class="float-end">
                                                <a href="{{ route('admin.pagina.editar.prouni') }}" class="btn bg-base text-white">
                                                    <i class="fa fa-edit"></i>
                                                </a>
                                            </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>ENEM</td>
                                        <td>
                                            <a href="/enem" target="_blank" class="color-blue-info-link">
                                                /enem
                                            </a>
                                        </td>
                                        <td>
                                            <span class="float-end">
                                                <a href="{{ route('admin.pagina.editar.enem') }}" class="btn bg-base text-white">
                                                    <i class="fa fa-edit"></i>
                                                </a>
                                            </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Vestibular</td>
                                        <td>
                                            <a href="/vestibular" target="_blank" class="color-blue-info-link">
                                                /vestibular
                                            </a>
                                        </td>
                                        <td>
                                            <span class="float-end">
                                                <a href="{{ route('admin.pagina.editar.vestibular') }}" class="btn bg-base text-white">
                                                    <i class="fa fa-edit"></i>
                                                </a>
                                            </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Transferência</td>
                                        <td>
                                            <a href="/transferencia" target="_blank" class="color-blue-info-link">
                                                /transferencia
                                            </a>
                                        </td>
                                        <td>
                                            <span class="float-end">
                                                <a href="{{ route('admin.pagina.editar.transferencia') }}" class="btn bg-base text-white">
                                                    <i class="fa fa-edit"></i>
                                                </a>
                                            </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Polos</td>
                                        <td>
                                            <a href="/polos" target="_blank" class="color-blue-info-link">
                                                /polos
                                            </a>
                                        </td>
                                        <td>
                                            <span class="float-end">
                                                <a href="{{ route('admin.pagina.editar.polos') }}" class="btn bg-base text-white">
                                                    <i class="fa fa-edit"></i>
                                                </a>
                                            </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Cursos</td>
                                        <td>
                                            <a href="/cursos" target="_blank" class="color-blue-info-link">
                                                /cursos
                                            </a>
                                        </td>
                                        <td>
                                            <span class="float-end">
                                                <a href="{{ route('admin.pagina.editar.cursos') }}" class="btn bg-base text-white">
                                                    <i class="fa fa-edit"></i>
                                                </a>
                                            </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Sobre nós</td>
                                        <td>
                                            <a href="/sobre-nos" target="_blank" class="color-blue-info-link">
                                                /sobre-nos
                                            </a>
                                        </td>
                                        <td>
                                            <span class="float-end">
                                                <a href="{{ route('admin.pagina.editar.sobrenos') }}" class="btn bg-base text-white">
                                                    <i class="fa fa-edit"></i>
                                                </a>
                                            </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Como funciona</td>
                                        <td>
                                            <a href="/como-funciona" target="_blank" class="color-blue-info-link">
                                                /como-funciona
                                            </a>
                                        </td>
                                        <td>
                                            <span class="float-end">
                                                <a href="{{ route('admin.pagina.editar.como-funciona') }}" class="btn bg-base text-white">
                                                    <i class="fa fa-edit"></i>
                                                </a>
                                            </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>FAQ</td>
                                        <td>
                                            <a href="/faq" target="_blank" class="color-blue-info-link">
                                                /faq
                                            </a>
                                        </td>
                                        <td>
                                            <span class="float-end">
                                                <a href="{{ route('admin.pagina.editar.faq') }}" class="btn bg-base text-white">
                                                    <i class="fa fa-edit"></i>
                                                </a>
                                            </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Ouvidoria</td>
                                        <td>
                                            <a href="/ouvidoria" target="_blank" class="color-blue-info-link">
                                                /ouvidoria
                                            </a>
                                        </td>
                                        <td>
                                            <span class="float-end">
                                                <a href="{{ route('admin.pagina.editar.ouvidoria') }}" class="btn bg-base text-white">
                                                    <i class="fa fa-edit"></i>
                                                </a>
                                            </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>CPA</td>
                                        <td>
                                            <a href="/cpa" target="_blank" class="color-blue-info-link">
                                                /cpa
                                            </a>
                                        </td>
                                        <td>
                                            <span class="float-end">
                                                <a href="{{ route('admin.pagina.editar.cpa') }}" class="btn bg-base text-white">
                                                    <i class="fa fa-edit"></i>
                                                </a>
                                            </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Contato</td>
                                        <td>
                                            <a href="/contato" target="_blank" class="color-blue-info-link">
                                                /contato
                                            </a>
                                        </td>
                                        <td>
                                            <span class="float-end">
                                                <a href="{{ route('admin.pagina.editar.contato') }}" class="btn bg-base text-white">
                                                    <i class="fa fa-edit"></i>
                                                </a>
                                            </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Termos de Uso</td>
                                        <td>
                                            <a href="/termos-de-uso" target="_blank" class="color-blue-info-link">
                                                /termos-de-uso
                                            </a>
                                        </td>
                                        <td>
                                            <span class="float-end">
                                                <a href="{{ route('admin.pagina.editar.termos.de.uso') }}" class="btn bg-base text-white">
                                                    <i class="fa fa-edit"></i>
                                                </a>
                                            </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Política de Privacidade</td>
                                        <td>
                                            <a href="/politica-de-privacidade" target="_blank" class="color-blue-info-link">
                                                /politica-de-privacidade
                                            </a>
                                        </td>
                                        <td>
                                            <span class="float-end">
                                                <a href="{{ route('admin.pagina.editar.politica.de.privacidade') }}" class="btn bg-base text-white">
                                                    <i class="fa fa-edit"></i>
                                                </a>
                                            </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Histórico Escolar</td>
                                        <td>
                                            <a href="/aluno-info-0-historico-escolar" target="_blank" class="color-blue-info-link">
                                                /aluno-info-0-historico-escolar
                                            </a>
                                        </td>
                                        <td>
                                            <span class="float-end">
                                                <a href="{{ route('admin.pagina.editar.aluno-historico-escolar') }}" class="btn bg-base text-white">
                                                    <i class="fa fa-edit"></i>
                                                </a>
                                            </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Acadêmico</td>
                                        <td>
                                            <a href="/aluno/academico" target="_blank" class="color-blue-info-link">
                                                /aluno/academico
                                            </a>
                                        </td>
                                        <td>
                                            <span class="float-end">
                                                <a href="{{ route('admin.pagina.editar.aluno-academico') }}" class="btn bg-base text-white">
                                                    <i class="fa fa-edit"></i>
                                                </a>
                                            </span>
                                        </td>
                                    </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>

</div>
@endsection