<?php

namespace App\Http\Controllers\SiteProprio;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Banner;
use App\Models\Curso;
use App\Models\Depoimento;

class ColegioEducacaoController extends Controller
{
    public function index()
    {
        $banners = Banner::plataforma()->ativo()->orderBy('ordem')->get();
        $cursos = Curso::plataforma()->ativo()->publico()->orderBy('nome')->get();
        $depoimentos = Depoimento::plataforma()->ativo()->orderBy('ordem')->get();

        if(request('sentContato'))
        {
            if(request('check'))
                return back()->with('danger', 'SPAM Detected!');

            $plataforma_id = \App\Models\Plataforma::plataforma()->pluck('id')[0] ?? null;

            \App\Models\Contato::create(request()->all() + [
                'plataforma_id' => $plataforma_id,
                'tipo' => 'L',
            ]);
            
            if(request('assunto') == 'Fale Conosco')
                return redirect('/#contato')->with('success', 'Inscrição enviada com êxito.');
            else
                return redirect('/#pre-matricula')->with('success_prematricula', 'Inscrição enviada com êxito.');
        }

    	return view('sites-proprios.colegio-educacao.home', compact('banners','cursos','depoimentos') );
    }

    public function curso($slug)
    {
        $curso = \App\Models\Curso::plataforma()->publico()->where('slug', $slug)->firstOrFail();

        $turmas = \App\Models\Turma::plataforma()->ativo()->publico()->where('curso_id', $curso->id)->get();

        return view('sites-proprios.colegio-educacao.curso', compact('curso','turmas') );
    }
}