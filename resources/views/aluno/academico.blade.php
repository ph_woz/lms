@extends('layouts.aluno')
@section('content')

@section('seo_title', 'Acadêmico')
<div class="container-fluid px-xl-5 mt-5">

	<div class="mb-4 p-5 rounded-10 box-shadow bg-14">

		<div class="row">

			@foreach($links as $link)
			<div class="col-lg-auto col-12 mb-5 px-1">
				<a href="{{ $link->link }}"  target="_blank" class="btn-suporte-enviar py-2-5">
					{{ $link->titulo }}
				</a>
			</div>
			@endforeach

		</div>
	</div>

</div>
@endsection