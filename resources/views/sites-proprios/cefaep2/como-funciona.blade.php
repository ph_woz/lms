<!doctype html>
<html lang="pt-br">
<head>

  <!-- Google tag (gtag.js) -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=G-MREB450HCT"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'G-MREB450HCT');
  </script>
  
   {!! $plataforma->scripts_start_head !!}

   <meta charset="utf-8">
   <meta name="viewport" content="width=device-width, initial-scale=1">

   <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KK94CHFLLe+nY2dmCWGMq91rCGa5gtU4mk92HdvYe+M/SXH301p5ILy+dN9+nJOZ" crossorigin="anonymous">
   <link rel="stylesheet" href="{{ asset('sites-proprios/cefaep2/css/home.css') }}">
   <link rel="stylesheet" href="{{ asset('css/util.css') }}">

   <title>{{ $pagina->title ?? $plataforma->nome }}</title>
   <meta name="robots" content="index, follow">
   <meta name="description" content="{{ $pagina->description ?? null }}"/>
   <meta name="keywords" content="{{ $pagina->keywords ?? null }}"/>
   <link rel="canonical" href="{{ Request::url() }}" />
   <link rel="shortcut icon" href="{{ $plataforma->favicon }}">
   <meta property="og:locale" content="pt_BR" />
   <meta property="og:type" content="website" />
   <meta property="og:title" content="{{ $pagina->title ?? $plataforma->nome }}" />
   <meta property="og:description" content="{{ $pagina->description ?? null }}" />
   <meta property="og:url" content="{{ Request::url() }}" />
   <meta property="og:site_name" content="{{ $plataforma->nome }}" />
   <meta property="og:image" content="{{ $plataforma->logotipo }}" />
   <meta name='author' content='WOZCODE | Pedro Henrique' />

   {!! $plataforma->scripts_final_head !!}

</head>
<body id="inicio">

	@include('sites-proprios/cefaep2/componentes/header')

	<main>

      <section>
       <div class="w-100 h-200 background-size-cover background-position-center" style="background-image: url('{{ asset('sites-proprios/cefaep2/images/header-como-funciona.webp') }}">
          <div class="position-relative h-100 px-xl-7 px-3">

            <!--
             <div class="row align-items-center h-100 position-relative">
                <div class="mt-2">
                   <h1 class="text-light font-saira-condensed weight-800 fs-xs-35 fs-md-50 text-shadow-3">
                      COMO FUNCIONA
                   </h1>
                </div>
             </div>
            -->
            
          </div>
       </div>
      </section>

      <section class="pt-5 overflow-hidden">
      	<div class="container-fluid px-xl-7">
            <div class="row align-items-center">

               <div class="col-lg-8">

                  <div class="mb-4">
                     <h3 class="m-0 weight-600 fs-25 color-verde-maduro">1º Passo : Entre em contato.</h3>
                     <p class="mb-0 font-inter weight-500 color-444">Fale com a nossa equipe e tire todas as dúvidas possíveis e confira a qualidade de nossa escola.</p>
                  </div>

                  <div class="mb-4">
                     <h3 class="m-0 weight-600 fs-25 color-verde-maduro">2º Passo : Realize sua Matrícula</h3>
                     <p class="mb-0 font-inter weight-500 color-444">Para realizar a matrícula é bem simples e prático, precisamos apenas de RG, CPF e comprovante de endereço. *inicialmente</p>
                  </div>

                  <div class="mb-4">
                     <h3 class="m-0 weight-600 fs-25 color-verde-maduro">3º Passo : Receberá o acesso a Área do Aluno (inicie as aulas 100% Online)</h3>
                     <p class="mb-0 font-inter weight-500 color-444">Após a matricula você receberá o seu acesso a Área do Aluno, podendo iniciar suas aulas quando quiser.</p>
                  </div>

                  <div class="mb-4">
                     <h3 class="m-0 weight-600 fs-25 color-verde-maduro">4º Passo : Realize sua Prova</h3>
                     <p class="mb-0 font-inter weight-500 color-444">Após todos os conteúdos estudados chegou o grande dia de testar seus conhecimentos, realize as provas e preencha os gabaritos.</p>
                  </div>

                  <div class="mb-4">
                     <h3 class="m-0 weight-600 fs-25 color-verde-maduro">5º Passo : Receba seu Certificado</h3>
                     <p class="mb-0 font-inter weight-500 color-444">Pode comemorar! Você conseguiu terminar seus estudos, foi rápido não é mesmo? Agora e só continuar buscando mais conhecimento, lembrando que seu certificado e valido em todo território nacional</p>
                  </div>

               </div>

               <div class="col-lg-4">
                  <img src="{{ asset('images/utils/estudantes-formados.webp') }}" alt="Estudantes de sucesso">
               </div>

            </div>
      	</div>
      </section>

      <section id="vantagens" class="m-vantagens text-center pb-5 pt-3"> 
         <div class="container-fluid px-xl-7 mt-4 pb-lg-5">

            <span class="title-lead color-orange">Vantagens especiais</span>

            <h2 class="title title--small font-saira-condensed mb-4 px-4">Sabe quando você pode começar? <span>Agora!</span></h2>
            <img src="{{ asset('sites-proprios/cefaep/images/dots-blue.png') }}">

            <div class="row mt-4-5">

               <div class="col-lg-4">
                  <div class="m-vantagens-item bg-white p-4 mb-4 box-shadow">
                     <div class="row align-items-center h-100">
                        <div class="col-md-12">
                           <img alt="Reconhecida pelo MEC"  class="img-fluid" src="{{ asset('images/utils/icon-vantagens-01.webp') }}" alt="Reconhecida pelo MEC" width="140">
                           <span class="vantangens-span d-block mt-3">Reconhecida pelo MEC</span>
                        </div>
                     </div>
                  </div>
               </div>

               <div class="col-lg-4">
                  <div class="m-vantagens-item bg-white p-4 mb-4 box-shadow">
                     <div class="row align-items-center h-100">
                        <div class="col-md-12">
                           <img alt="Diploma válido em todo território Nacional" class="img-fluid" src="{{ asset('images/utils/icon-vantagens-02.webp') }}" alt="Diploma válido em todo território Nacional">
                           <span class="vantangens-span d-block mt-3">Diploma válido em todo território Nacional</span>
                        </div>
                     </div>
                  </div>
               </div>

               <div class="col-lg-4">
                  <div class="m-vantagens-item bg-white p-4 mb-4 box-shadow">
                     <div class="row align-items-center h-100">
                        <div class="col-md-12">
                           <img alt="Faça as provas quando estiver preparado" class="img-fluid" src="https://www.cnead.com.br/site/wp-content/uploads/2020/09/icon-vantagens-03.jpg" alt="Faça as provas quando estiver preparado">
                           <span class="vantangens-span d-block mt-3">Faça as provas quando estiver preparado</span>
                        </div>
                     </div>
                  </div>
               </div>

               <div class="col-lg-4">
                  <div class="m-vantagens-item bg-white p-4 mb-4 box-shadow">
                     <div class="row align-items-center h-100">
                        <div class="col-md-12">
                           <img alt="Estude em seu próprio horário" class="img-fluid" src="https://www.cnead.com.br/site/wp-content/uploads/2020/09/icon-vantagens-04.jpg" alt="Estude em seu próprio horário">
                           <span class="vantangens-span d-block mt-3">Estude em seu próprio horário</span>
                        </div>
                     </div>
                  </div>
               </div>

               <div class="col-lg-4">
                  <div class="m-vantagens-item bg-white p-4 mb-4 box-shadow">
                     <div class="row align-items-center h-100">
                        <div class="col-md-12">
                           <img alt="Certificado válido pelo Conselho Estadual de Educação." class="img-fluid" src="https://www.cnead.com.br/site/wp-content/uploads/2020/09/icon-vantagens-05.jpg" alt="Certificado válido pelo Conselho Estadual de Educação.">
                           <span class="vantangens-span d-block mt-3">Certificado válido pelo Conselho Estadual de Educação.</span>
                        </div>
                     </div>
                  </div>
               </div>

               <div class="col-lg-4">
                  <div class="m-vantagens-item bg-white p-4 mb-4 box-shadow">
                     <div class="row align-items-center h-100">
                        <div class="col-md-12">
                           <img alt="Escola 100% OnLine" class="img-fluid" src="https://www.cnead.com.br/site/wp-content/uploads/2020/09/icon-vantagens-06.jpg" alt="Escola 100% OnLine">
                           <span class="vantangens-span d-block mt-3">Escola 100% OnLine</span>
                        </div>
                     </div>
                  </div>
               </div>

            </div>

            <div class="mt-4">
            
               <a href="https://cefaep.com.br/f/formulario-de-matricula/23/t/54" target="_blank" class="me-lg-2 mb-3 mb-lg-0 btn btn-quero-me-matricular px-4-5 py-3">
                  Quero me matricular
               </a>

               <a href="/faq" class="btn btn-duvidas px-4-5 py-3">
                  Ainda tenho dúvidas
               </a>
            
            </div>


         </div>
      </section>

	</main>

	@include('sites-proprios/cefaep2/componentes/footer')

</body>
</html>