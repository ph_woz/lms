<?php

namespace App\Http\Livewire\Admin\Responsavel;

use Livewire\Component;
use App\Models\Responsavel;
use App\Models\ResponsavelAluno;
use App\Models\User;
use DB;
use Illuminate\Support\Facades\Http;

class Info extends Component
{
    public $responsavel;
    public $cadastrante;
    public $password_to_delete;

    public $desvincular_aluno_id;


    public function mount($responsavel_id)
    {
        $this->responsavel = Responsavel::plataforma()->find($responsavel_id);
    
        $this->cadastrante = User::plataforma()->where('id', $this->responsavel->cadastrante_id)->first();
    }

    public function render()
    {
        $alunos = DB::table('users')
            ->join('responsaveis_alunos','users.id','responsaveis_alunos.aluno_id')
            ->where('users.tipo', 'aluno')
            ->where('users.status', 0)
            ->where('users.plataforma_id', session('plataforma_id'))
            ->where('responsaveis_alunos.plataforma_id', session('plataforma_id'))
            ->where('responsaveis_alunos.responsavel_id', $this->responsavel->id)
            ->select(
                'users.id',
                'users.nome',
                'responsaveis_alunos.grau_parentesco'
            )
            ->get();

        return view('livewire.admin.responsavel.info', ['alunos' => $alunos]);
    }


    public function desvincularAlunoId($aluno_id)
    {
        $this->desvincular_aluno_id = $aluno_id;
    }

    public function desvincularAluno()
    {
        if(\Auth::attempt(['id' => \Auth::id(), 'password' => $this->password_to_delete, 'status' => 0]))
        {
            ResponsavelAluno::plataforma()->where('responsavel_id', $this->responsavel->id)->where('aluno_id', $this->desvincular_aluno_id)->delete();

            session()->flash('success-livewire', 'Aluno desvinculado do responsável com êxito.');

            $this->dispatchBrowserEvent('dissmiss-modal');
            $this->dispatchBrowserEvent('toast');

        } else {

            session()->flash('danger-livewire', 'Senha incorreta.');

            $this->dispatchBrowserEvent('toast');
        }
    }

    public function delete()
    {
        if(\Auth::attempt(['id' => \Auth::id(), 'password' => $this->password_to_delete, 'status' => 0]))
        {
            ResponsavelAluno::plataforma()->where('responsavel_id', $this->responsavel->id)->delete();
            Responsavel::plataforma()->where('id', $this->responsavel->id)->delete();

            session()->flash('success', 'Responsável deletado com êxito.');
            
            return redirect(route('admin.responsavel.lista'));

        } else {

            session()->flash('danger-livewire', 'Senha incorreta.');

            $this->dispatchBrowserEvent('toast');
        }
    }
}
