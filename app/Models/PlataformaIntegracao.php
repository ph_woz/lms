<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PlataformaIntegracao extends Model
{
    use HasFactory;

    protected $table = 'plataformas_integracoes';

    public $timestamps = false;

    protected $fillable = [
    	'plataforma_id',
    	'ref',
    	'value',
    	'status',
    ];

    public function scopePlataforma($query)
    {
        return $query->where('plataforma_id', session('plataforma_id') ?? Plataforma::dominio()->pluck('id')[0] ?? null);
    }

    public function scopeAtivo($query)
    {   
        return $query->where('status', 0);
    }
    
}
