@extends('layouts.admin.master')

@section('css')
<style>

.h-100px { height: 100px; }
.h-150 { height: 150px!important; }
@media (min-width: 990px) { .h-lg-90vh { height: 90vh; } }
@media (max-width: 768px) { .h-xs-400 { height: 400px; } }

</style>
@endsection

@section('content')
    
    @livewire('admin.ajuda.tutorial.lista')

@endsection