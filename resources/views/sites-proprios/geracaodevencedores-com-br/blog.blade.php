<!doctype html>
<html lang="pt-br">
<head>

  <!-- Google tag (gtag.js) -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=G-MREB450HCT"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'G-MREB450HCT');
  </script>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/uikit@3.5.5/dist/css/uikit.min.css" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/5.0.0-alpha1/css/bootstrap.min.css" integrity="sha384-r4NyP46KrjDleawBgD5tp8Y7UzmLA05oM1iAEQ17CSuDqnUK2+k9luXQOfXJCJ4I" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('sites-proprios/geracaodevencedores-com-br/css/home.css') }}?v=8">
    <link rel="stylesheet" href="{{ asset('css/util.css') }}?v=2">

    <link rel="shortcut icon" href="{{ $plataforma->favicon ?? asset('sites-proprios/geracaodevencedores-com-br/images/favicon.ico') }}">
    <link href="{{ Request::url() }}" rel="canonical">

    <meta name="robots" content="index, follow">
    <title>{{ $seo->title ?? $plataforma->nome }}</title>
    <meta name="description" content="{{ $seo->description ?? null }}">
    <meta name="keywords" content="{{ $seo->keywords ?? null }}">

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <meta name='author' content='WOZCODE | Pedro Henrique' />

    <meta property="og:locale" content="pt_BR">
    <meta property="og:type" content="website">
    <meta property="og:image" content="{{ $plataforma->logotipoMenu ?? asset('sites-proprios/geracaodevencedores-com-br/images/logotipo.png') }}">
    <meta property="og:url" content="{{ Request::url() }}">
    <meta property="og:description" content="{{ $seo->description ?? null }}">
    <meta property="og:title" content="{{ $seo->title ?? $plataforma->nome }}">

    <style>
    
      #menuPlataforma nav { background: #355a7f; }
      .menu-link, .menu-link-color { color: #F5F5F5;; }
      .menu-link:hover { text-decoration: none; background: #1e3b57; color: #FFF!important; }
      .navbar-brand span, .activeNavLink { text-decoration: none; background: #1e3b57; color: #FFF!important; }

      #menu {
          width: 100%;
          box-sizing: border-box;
          padding: 9px 10px!important;
          position: fixed;
          z-index: 200;
          box-shadow: 0px -1px 8px 0px rgba(0, 0, 0, 0.60);
          background: transparent;
          transition: background-color 0.3s ease-in-out;
      }
      @media (min-width: 769px) 
      {
          #menu { height: 75px; }

          .menu-link { 
            margin: 0 10px;
            padding-top: 27.2px!important;
            padding-bottom: 27.2px!important;
            padding-left: 10px!important; 
            padding-right: 10px!important; 
          }
      }
      .menu-link {
          font-family: 'Open Sans',sans-serif;
          font-size: 13.5px;
          text-transform: uppercase;
          text-decoration: none;
          font-weight: 700;
          color: #F5F5F5!important;
          cursor: pointer;
          transition: all .2s;
          text-shadow: 1px 1px rgba(0, 0, 0, .1);
      }

      .btn-entrar { 
        background: #f5f5f5!important; 
        color: #355a7f!important; 
        font-family: 'Poppins', sans-serif; 
        font-weight: 600!important; 
        border-radius: 3px!important;
        padding: 12px 24px!important;
      }
          
      #footer { position: absolute!important; }
    
</style>

</head>
<body id="inicio">

   @include('sites-proprios/geracaodevencedores-com-br/componentes/header')

   <main class="py-5">

      <section id="blog_destaque" class="py-lg-5">
      <div class="container pt-5">

         <h1 class="fs-md-40 font-montserrat weight-800 text-dark">NOTÍCIAS/<span class="color-faixa-verde">BLOG</span></h1>

         <div class="d-lg-none mb-5 mb-lg-0">

            <div class="uk-position-relative uk-visible-toggle" tabindex="-1" uk-slideshow="animation: push">

               <ul class="uk-slideshow-items">
                     <li>
                     <a href="/blog/post/{{$postDestaque1->slug}}/{{$postDestaque1->id}}">
                        <div class="w-100 rounded-2">
                           <div class="bg-size-cover h-md-500 h-xs-250 rounded-2" style="background-image: linear-gradient(to bottom, rgba(0, 0, 0, .1), rgba(0, 0, 0, 1)), url('{{ $postDestaque1->foto_capa }}');"></div>
                           <div class="uk-position-bottom px-4 pb-4">
                              <h5 class="text-light text-shadow-2 weight-700 fs-md-30 fs-xs-18 font-montserrat mb-0 p-0">
                                 {{ $postDestaque1->titulo }}
                              </h5>
                              <hr class="border" />
                              <div class="d-flex justify-content-between align-items-center">
                                 <p class="mb-0 fs-12">
                                 @if($postDestaque1->autor)
                                    <span class="color-c9">por</span>
                                    <span class="color-c9 weight-600">{{ \App\Models\Util::getPrimeiroSegundoNome($postDestaque1->autor) }}</span>
                                 @endif
                                 </p>
                                 <p class="mb-0 font-nunito fs-12">
                                 <i class="fa fa-clock color-c9"></i>
                                 <span class="color-c9">{{ \Carbon\Carbon::createFromTimeStamp(strtotime($postDestaque1->created_at))->diffForHumans() }}</span>
                                 </p>
                              </div>
                           </div>
                        </div>
                     </a>
                     </li>
                     <li>
                     <a href="/blog/post/{{$postDestaque2->slug}}/{{$postDestaque2->id}}">
                        <div class="w-100 rounded-2">
                           <div class="bg-size-cover h-md-500 h-xs-250 rounded-2" style="background-image: linear-gradient(to bottom, rgba(0, 0, 0, .1), rgba(0, 0, 0, 1)), url('{{ $postDestaque2->foto_capa }}');"></div>
                           <div class="uk-position-bottom px-4 pb-4">
                              <h5 class="text-light text-shadow-2 weight-700 fs-md-25 fs-xs-18 font-montserrat mb-0 p-0">
                                 {{ $postDestaque2->titulo }}
                              </h5>
                              <hr class="border" />
                              <div class="d-flex justify-content-between align-items-center">
                                 <p class="mb-0 fs-12">
                                 @if($postDestaque2->autor)
                                    <span class="color-c9">por</span>
                                    <span class="color-c9 weight-600">{{ \App\Models\Util::getPrimeiroSegundoNome($postDestaque2->autor) }}</span>
                                 @endif
                                 </p>
                                 <p class="mb-0 font-nunito fs-12">
                                 <i class="fa fa-clock color-c9"></i>
                                 <span class="color-c9">{{ \Carbon\Carbon::createFromTimeStamp(strtotime($postDestaque2->created_at))->diffForHumans() }}</span>
                                 </p>
                              </div>
                           </div>
                        </div>
                     </a>
                     </li>
                     <li>
                     <a href="/blog/post/{{$postDestaque3->slug}}/{{$postDestaque3->id}}">
                        <div class="w-100 rounded-2">
                           <div class="bg-size-cover h-md-500 h-xs-250" style="background-image: linear-gradient(to bottom, rgba(0, 0, 0, .1), rgba(0, 0, 0, 1)), url('{{ $postDestaque3->foto_capa }}');"></div>
                           <div class="uk-position-bottom px-4 pb-4">
                              <h5 class="text-light text-shadow-2 weight-700 fs-18 font-montserrat mb-0 p-0">
                                 {{ $postDestaque3->titulo }}
                              </h5>
                              <hr class="border" />
                              <div class="d-flex justify-content-between align-items-center">
                                 <p class="mb-0 fs-12">
                                 @if($postDestaque3->autor)
                                 <span class="color-c9">por</span>
                                 <span class="color-c9 weight-600">{{ \App\Models\Util::getPrimeiroSegundoNome($postDestaque3->autor) }}</span>
                                 @endif
                                 </p>
                              <p class="mb-0 font-nunito fs-12">
                                 <i class="fa fa-clock color-c9"></i>
                                 <span class="color-c9">{{ \Carbon\Carbon::createFromTimeStamp(strtotime($postDestaque3->created_at))->diffForHumans() }}</span>
                              </p>
                              </div>
                           </div>
                        </div>
                     </a>
                     </li>
                     <li>
                        <a href="/blog/post/{{$postDestaque4->slug}}/{{$postDestaque4->id}}">
                           <div class="w-100 rounded-2">
                              <div class="bg-size-cover h-md-500 h-xs-250" style="background-image: linear-gradient(to bottom, rgba(0, 0, 0, .1), rgba(0, 0, 0, 1)), url('{{ $postDestaque4->foto_capa }}');"></div>
                              <div class="uk-position-bottom px-4 pb-4">
                                 <h5 class="text-light text-shadow-2 weight-700 fs-18 font-montserrat mb-0 p-0">
                                    {{ $postDestaque4->titulo }}
                                 </h5>
                                 <hr class="border" />
                                 <div class="d-flex justify-content-between align-items-center">
                                 <p class="mb-0 fs-12">
                                    @if($postDestaque4->autor)
                                    <span class="color-c9">por</span>
                                    <span class="color-c9 weight-600">{{ \App\Models\Util::getPrimeiroSegundoNome($postDestaque4->autor) }}</span>
                                    @endif
                                 </p>
                                 <p class="mb-0 font-nunito fs-12">
                                    <i class="fa fa-clock color-c9"></i>
                                    <span class="color-c9">{{ \Carbon\Carbon::createFromTimeStamp(strtotime($postDestaque4->created_at))->diffForHumans() }}</span>
                                 </p>
                                 </div>
                              </div>
                           </div>
                        </a>
                     </li>
               </ul>

               <div class="uk-light">
                     <a class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous uk-slideshow-item="previous"></a>
                     <a class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-next uk-slideshow-item="next"></a>
               </div>

            </div>

         </div>

         <div class="d-md-none d-xs-none d-lg-block">

            <div class="row">

               <div class="col-lg-6 px-lg-2">
                  
                  <a href="/blog/post/{{$postDestaque1->slug}}/{{$postDestaque1->id}}">
                     <div class="w-100 uk-inline-clip uk-transition-toggle box-convenios rounded-2">
                        <div class="uk-transition-scale-up uk-transition-opaque bg-size-cover h-lg-500 h-xs-250 rounded-2" style="background-image: linear-gradient(to bottom, rgba(0, 0, 0, .1), rgba(0, 0, 0, 1)), url('{{ $postDestaque1->foto_capa }}');"></div>
                        <div class="uk-position-bottom px-4 pb-4">
                           <h5 class="text-light text-shadow-2 weight-700 fs-md-30 fs-xs-18 font-montserrat mb-0 p-0">
                              {{ $postDestaque1->titulo }}
                           </h5>
                           <hr class="border" />
                           <div class="d-flex justify-content-between align-items-center">
                              <p class="mb-0 fs-12">
                              @if($postDestaque1->autor)
                              <span class="color-c9">por</span>
                              <span class="color-c9 weight-600">{{ \App\Models\Util::getPrimeiroSegundoNome($postDestaque1->autor) }}</span>
                              @endif
                              </p>
                           <p class="mb-0 font-nunito fs-12">
                              <i class="fa fa-clock color-c9"></i>
                              <span class="color-c9">{{ \Carbon\Carbon::createFromTimeStamp(strtotime($postDestaque1->created_at))->diffForHumans() }}</span>
                           </p>
                           </div>
                        </div>
                     </div>
                  </a>

               </div>

               <div class="col-lg-3 px-lg-2 py-3 py-lg-0">

                  <a href="/blog/post/{{$postDestaque2->slug}}/{{$postDestaque2->id}}">
                     <div class="w-100 uk-inline-clip uk-transition-toggle box-convenios rounded-2">
                        <div class="uk-transition-scale-up uk-transition-opaque bg-size-cover h-lg-500 h-xs-250 rounded-2" style="background-image: linear-gradient(to bottom, rgba(0, 0, 0, .1), rgba(0, 0, 0, 1)), url('{{ $postDestaque2->foto_capa }}');"></div>
                        <div class="uk-position-bottom px-4 pb-4">
                           <h5 class="text-light text-shadow-2 weight-700 fs-md-25 fs-xs-18 font-montserrat mb-0 p-0">
                              {{ $postDestaque2->titulo }}
                           </h5>
                           <hr class="border" />
                           <div class="d-flex justify-content-between align-items-center">
                              <p class="mb-0 fs-12">
                              @if($postDestaque2->autor)
                              <span class="color-c9">por</span>
                              <span class="color-c9 weight-600">{{ \App\Models\Util::getPrimeiroSegundoNome($postDestaque2->autor) }}</span>
                              @endif
                              </p>
                           <p class="mb-0 font-nunito fs-12">
                              <i class="fa fa-clock color-c9"></i>
                              <span class="color-c9">{{ \Carbon\Carbon::createFromTimeStamp(strtotime($postDestaque2->created_at))->diffForHumans() }}</span>
                           </p>
                           </div>
                        </div>
                     </div>
                  </a>

               </div>

               <div class="col-lg-3 px-lg-2">

                  <div>
                     <a href="/blog/post/{{$postDestaque3->slug}}/{{$postDestaque3->id}}">
                        <div class="w-100 uk-inline-clip uk-transition-toggle box-convenios rounded-2">
                           <div class="uk-transition-scale-up uk-transition-opaque bg-size-cover h-lg-240 h-xs-250" style="background-image: linear-gradient(to bottom, rgba(0, 0, 0, .1), rgba(0, 0, 0, 1)), url('{{ $postDestaque3->foto_capa }}');"></div>
                           <div class="uk-position-bottom px-4 pb-4">
                              <h5 class="text-light text-shadow-2 weight-700 fs-18 font-montserrat mb-0 p-0">
                                 {{ $postDestaque3->titulo }}
                              </h5>
                              <hr class="border" />
                              <div class="d-flex justify-content-between align-items-center">
                                 <p class="mb-0 fs-12">
                                 @if($postDestaque3->autor)
                                 <span class="color-c9">por</span>
                                 <span class="color-c9 weight-600">{{ \App\Models\Util::getPrimeiroSegundoNome($postDestaque3->autor) }}</span>
                                 @endif
                                 </p>
                              <p class="mb-0 font-nunito fs-12">
                                 <i class="fa fa-clock color-c9"></i>
                                 <span class="color-c9">{{ \Carbon\Carbon::createFromTimeStamp(strtotime($postDestaque3->created_at))->diffForHumans() }}</span>
                              </p>
                              </div>
                           </div>
                        </div>
                     </a>
                  </div>
                  <div class="py-2"></div>
                  <div>
                     <a href="/blog/post/{{$postDestaque4->slug}}/{{$postDestaque4->id}}">
                        <div class="w-100 uk-inline-clip uk-transition-toggle box-convenios rounded-2">
                           <div class="uk-transition-scale-up uk-transition-opaque bg-size-cover h-lg-240 h-xs-250" style="background-image: linear-gradient(to bottom, rgba(0, 0, 0, .1), rgba(0, 0, 0, 1)), url('{{ $postDestaque4->foto_capa }}');"></div>
                           <div class="uk-position-bottom px-4 pb-4">
                              <h5 class="text-light text-shadow-2 weight-700 fs-18 font-montserrat mb-0 p-0">
                                 {{ $postDestaque4->titulo }}
                              </h5>
                              <hr class="border" />
                              <div class="d-flex justify-content-between align-items-center">
                                 <p class="mb-0 fs-12">
                                 @if($postDestaque4->autor)
                                 <span class="color-c9">por</span>
                                 <span class="color-c9 weight-600">{{ \App\Models\Util::getPrimeiroSegundoNome($postDestaque4->autor) }}</span>
                                 @endif
                                 </p>
                              <p class="mb-0 font-nunito fs-12">
                                 <i class="fa fa-clock color-c9"></i>
                                 <span class="color-c9">{{ \Carbon\Carbon::createFromTimeStamp(strtotime($postDestaque4->created_at))->diffForHumans() }}</span>
                              </p>
                              </div>
                           </div>
                        </div>
                     </a>
                  </div>

               </div>

            </div>

         </div>

      </div>
      </section>

      @php
         $expiresAt = \Carbon\Carbon::now()->addHour();

         $categorias = \Cache::remember('sitesproprios-geracaodevencedores-com-br-blog-categorias', $expiresAt, function() {
            return \App\Models\Categoria::plataforma()->ativo()->publico()->tipo('blog')->orderBy('nome')->get();
         });
      @endphp
      @foreach($categorias as $categoria)
         @php
            $postsIds = \Cache::remember('sitesproprios-geracaodevencedores-com-br-blog-postsIds', $expiresAt, function() use ($categoria) {
               return \App\Models\BlogPostCategoria::plataforma()->where('categoria_id', $categoria->id)->select('post_id')->get()->pluck('post_id')->toArray();
            });

            $posts    = \Cache::remember('sitesproprios-geracaodevencedores-com-br-blog-posts', $expiresAt, function() use ($postsIds) {
               return \App\Models\BlogPost::plataforma()->whereIn('id', $postsIds)->orderBy('id','desc')->get();
            });
         @endphp

         <section class="pb-5 pt-3">
            <div class="container">

               <div class="d-flex align-items-center mb-3">
                  <h1 class="title-blog my-0 text-shadow-1 fs-15 pe-4">
                     <a href="/blog/categoria/{{\Str::slug($categoria->nome)}}/{{$categoria->id}}" class="color-faixa-verde text-shadow-1 hover-d-none"> 
                        {{ $categoria->nome }}
                     </a>
                  </h1>

                  <hr class="uk-divider border w-100 my-0 py-0">
               </div>
               
               @if(count($posts) > 0)
                  <div uk-slider="autoplay: true; autoplay-interval: 3000;">

                        <div class="uk-position-relative uk-visible-toggle uk-light" tabindex="-1">

                           <ul class="uk-slider-items uk-child-width-1-4@s uk-grid">
                              @foreach($posts as $post)
                              <a href="/blog/post/{{$post->slug}}/{{$post->id}}">
                                    <div class="uk-card uk-card-default box-shadow border-bottom">
                                       <div class="uk-card-media-top">
                                          <img src="{{ $post->foto_capa }}" alt="{{ $post->titulo }}" class="h-lg-175 object-fit-cover w-100">
                                       </div>
                                       <div class="uk-card-body py-3 h-lg-100 h-xs-100 px-4a">
                                          <h3 class="uk-card-title fs-16">
                                             {{ \Str::limit($post->titulo, 55) }}
                                          </h3>
                                       </div>
                                    </div>
                              </a>
                              @endforeach
                           </ul>

                           <a class="uk-position-center-left uk-position-small uk-hidden-hover text-dark" href="#" uk-slidenav-previous uk-slider-item="previous"></a>
                           <a class="uk-position-center-right uk-position-small uk-hidden-hover text-dark" href="#" uk-slidenav-next uk-slider-item="next"></a>

                        </div>

                        <ul class="uk-slider-nav uk-dotnav uk-flex-center uk-margin"></ul>

                  </div>
               @endif
               
            </div>
         </section>

      @endforeach

   </main>

   @include('sites-proprios/geracaodevencedores-com-br/componentes/footer')

   <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
   <script src="https://cdn.jsdelivr.net/npm/uikit@3.5.5/dist/js/uikit.min.js"></script>
   <script src="https://kit.fontawesome.com/2f91eaaef4.js" crossorigin="anonymous"></script>  
   <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>    
   <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
   <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

   <script>

      window.onscroll = function() {myFunction()};

      var header = document.getElementById("menu");
      var sticky = header.offsetTop;

      function myFunction() {
        if (window.pageYOffset > sticky) {
          header.classList.add("fixed-top");
          header.classList.add("mudaCor");
          $(".nav-link").addClass("nav-link-after-hover");
        } else {
          header.classList.remove("fixed-top");
          header.classList.remove("mudaCor");
          $(".nav-link").removeClass("nav-link-after-hover");
        }
      }

   </script>

</body>
</html>