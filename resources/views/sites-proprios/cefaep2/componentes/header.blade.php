{!! $plataforma->scripts_start_body !!}

<header>
	<nav class="navbar navbar-expand-lg bg-white">
	  	<div class="container-fluid px-xl-7">
	    	<a class="navbar-brand" href="/">
	    		<img src="{{ $plataforma->logotipo ?? 'https://wozcodelms2.s3.amazonaws.com/plataformas/28/logotipo.png' }}" class="img-fluid" width="70" alt="Logotipo">
	    	</a>
	    	<button class="navbar-toggler outline-0 border-0" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
	      		<span class="navbar-toggler-icon"></span>
	    	</button>
	    	<div class="collapse navbar-collapse" id="navbarSupportedContent">
	      		<ul class="navbar-nav ms-auto mb-2 mb-lg-0">
					<li class="nav-item">
					<a class="nav-link" href="/">Home</a>
					</li>
					<li class="nav-item">
					<a class="nav-link" href="/quem-somos">Quem somos</a>
					</li>
					<li class="nav-item">
					<a class="nav-link" href="/como-funciona">Como Funciona</a>
					</li>
					<li class="nav-item">
					<a class="nav-link" href="/faq">FAQ</a>
					</li>
					<li class="nav-item">
					<a class="nav-link" href="/contato">Contato</a>
					</li>
					<li class="nav-item">
					<a class="nav-link link-areadoaluno" href="/login">Área do Aluno</a>
					</li>
					<li class="nav-item">
					<a class="nav-link link-matriculese" href="https://cefaep.com.br/f/formulario-de-matricula/23/t/54" target="_blank">Matricule-se</a>
					</li>
	        	</ul>
	    	</div>
	  	</div>
	</nav>
</header>