<div>

    @if(is_null($add))

        <style>
            .add { display: none; }
        </style>

    @else

        <style>
            .default-show { display: none; }
        </style>

        @if(isset($add_alunos_para_comunicado_id))
            <h1 class="fs-18 bg-primary p-3 text-light rounded-2 shadow text-shadow-1">
                + adicione Alunos ao Comunicado<br>
                <a href="{{ route('admin.comunicado.info', ['id' => $comunicado->id]) }}" class="ms-3 text-light">
                    <span class="weight-600">
                        {{ $comunicado->assunto }}
                    </span>
                </a>
            </h1>
            <style>
                .add_aluno_para_fluxo_caixa { display: none; }
                .add_alunos_para_trilha_id { display: none; }
                .add_alunos_para_turma_id { display: none; }
            </style>
        @endif

        @if(isset($add_alunos_para_trilha_id))
            <h1 class="fs-18 bg-primary p-3 text-light rounded-2 shadow text-shadow-1">
                + adicione Alunos a Trilha<br>
                <a href="{{ route('admin.trilha.info', ['id' => $add_alunos_para_trilha_id]) }}" class="ms-3 text-light">
                    <span class="weight-600">
                        {{ $trilha_nome }}
                    </span>
                </a>
            </h1>
            <style>
                .add_aluno_para_fluxo_caixa { display: none; }
                .add_alunos_para_comunicado_id { display: none; }
                .add_alunos_para_turma_id { display: none; }
            </style>
        @endif

        @if(isset($add_alunos_para_turma_id))
            <h1 class="fs-18 bg-primary p-3 text-light rounded-2 shadow text-shadow-1">
                + adicione Alunos a Turma<br>
                <a href="{{ route('admin.turma.info', ['id' => $add_alunos_para_turma_id]) }}" class="ms-3 text-light">
                    <span class="weight-600">
                        {{ $turma_nome }}
                    </span>
                </a>
            </h1>
            <style>
                .add_aluno_para_fluxo_caixa { display: none; }
                .add_alunos_para_comunicado_id { display: none; }
                .add_alunos_para_trilha_id { display: none; }
            </style>
        @endif

        @if(isset($add_aluno_para_fluxo_caixa))
            <h1 class="fs-18 bg-primary p-3 text-light rounded-2 shadow text-shadow-1">
                + escolha e selecione um Aluno(a) para cadastrar uma Entrada no Fluxo de Caixa<br>
            </h1>
            <style>
                .add_alunos_para_comunicado_id { display: none; }
                .add_alunos_para_trilha_id { display: none; }
                .add_alunos_para_turma_id { display: none; }
            </style>
        @endif

        <hr>

    @endif

    <form>

        <div class="d-lg-flex mb-3">

            <input wire:model="search" type="search" class="form-control p-form" placeholder="Buscar">

            <select wire:model="status" class="mx-lg-3 my-3 my-lg-0 select-form w-lg-35">
                <option value="0">Ativos</option>
                <option value="1">Desativados</option>
            </select>

            <select wire:model="ordem" class="select-form w-lg-35">
                <option value="nome">Alfabética</option>
                <option value="desc">Mais novos</option>
                <option value="asc">Mais antigos</option>
                <option value="data_ultimo_acesso">Último acesso</option>
            </select>

        </div>

        <div class="d-lg-flex mb-3">
            @if(\App\Models\Unidade::checkActive() === true && \Auth::user()->restringir_unidade == 'N')
            <div class="w-100 me-lg-3 mb-lg-0" wire:ignore>
                <select wire:model="unidade_id" class="selectpicker" data-width="100%" data-live-search="true">
                    <option value="">Unidades</option>
                    @foreach($unidades as $unidade)
                        <option value="{{ $unidade->id }}">{{ $unidade->nome }}</option>
                    @endforeach
                </select>
            </div>
            @endif
            <div class="w-100" wire:ignore>
                <select wire:model="categoriasSelected" class="selectpicker" multiple data-width="100%" data-live-search="true" title="Categorias">
                    @foreach($categorias as $categoria)
                        <option value="{{ $categoria->id }}">{{ $categoria->nome }}</option>
                    @endforeach
                </select>
            </div>
        </div>

    </form>

    <div class="mt-5 d-flex justify-content-between">

        <div class="align-self-end mb-2">
            <span class="resultados-encontrados">
                {{ count($alunos) }} resultados de {{ $countAlunos }}.
            </span>
        </div>

        <div class="mb-3">
            <a href="{{ route('admin.aluno.add') }}" class="btn-add aclNivel2 default-show">
                + Adicionar
            </a>
            <a href="#ModalAddTodosAlunosAoComunicado" data-bs-toggle="modal" class="btn btn-primary add add_alunos_para_comunicado_id">
                + Adicionar todos
            </a>
            <a href="#ModalAddTodosAlunosATrilha" data-bs-toggle="modal" class="btn btn-primary add add_alunos_para_trilha_id">
                + Adicionar todos
            </a>   
            <a href="#ModalAddTodosAlunosATurma" data-bs-toggle="modal" class="btn btn-primary add add_alunos_para_turma_id">
                + Adicionar todos
            </a>            
        </div>

    </div>

    <div class="row">
        <div class="col-12">

            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col">Nome</th>
                                    <th scope="col">Email</th>
                                    <th scope="col">
                                        <span class="float-end">
                                            Ações
                                        </span>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($alunos as $aluno)
                                    <tr>
                                        <td>{{ $aluno->nome }}</td>
                                        <td>{{ $aluno->email }}</td>
                                        <td>
                                            <span class="float-end">

                                                <a wire:click="addAlunoParaComunicado({{$aluno->id}})" class="btn btn-primary add add_alunos_para_comunicado_id">
                                                    + Add
                                                </a>

                                                <a wire:click="addAlunoParaTrilha({{$aluno->id}})" class="btn btn-primary add add_alunos_para_trilha_id">
                                                    + Add
                                                </a>

                                                <a wire:click="addAlunoParaTurma({{$aluno->id}})" class="btn btn-primary add add_alunos_para_turma_id">
                                                    + Add
                                                </a>

                                                <a href="{{ route('admin.controle-financeiro.lista.fluxo-caixa') }}?add_entrada=S&cliente_id={{$aluno->id}}" class="btn btn-primary add add_aluno_para_fluxo_caixa">
                                                    Selecionar
                                                </a>

                                                <a href="{{ route('admin.aluno.info', ['id' => $aluno->id]) }}" class="btn bg-base text-white">
                                                    <i class="fa fa-eye"></i>
                                                </a>

                                            </span>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            {{ $alunos->links() }}

        </div>
    </div>

    <div wire:ignore class="modal fade" id="ModalAddTodosAlunosAoComunicado" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title" id="exampleModalLabel">Adicionar todos os Alunos ao Comunicado</h3>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body text-center py-5 px-4">

                    <p class="m-0 fs-15">
                        Você tem certeza que deseja adicionar todos estes alunos visíveis na listagem ao comunicado?
                    </p>

                </div>
                <div class="modal-footer">
                    <button type="button" wire:click="addTodosOsAlunosAoComunicado()" class="btn btn-primary weight-600 py-2">Confirmar</button>
                </div>
            </div>
        </div>
    </div>

    <div wire:ignore class="modal fade" id="ModalAddTodosAlunosATrilha" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title" id="exampleModalLabel">Adicionar todos os Alunos a Trilha</h3>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body text-center py-5 px-4">

                    <p class="m-0 fs-15">
                        Você tem certeza que deseja adicionar todos estes alunos visíveis na listagem a Trilha?
                    </p>

                </div>
                <div class="modal-footer">
                    <button type="button" wire:click="addTodosOsAlunosATrilha()" class="btn btn-primary weight-600 py-2">Confirmar</button>
                </div>
            </div>
        </div>
    </div>

    <div wire:ignore class="modal fade" id="ModalAddTodosAlunosATurma" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title" id="exampleModalLabel">Adicionar todos os Alunos a Turma</h3>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body text-center py-5 px-4">

                    <p class="m-0 fs-15">
                        Você tem certeza que deseja adicionar todos estes alunos visíveis na listagem a Turma?
                    </p>

                </div>
                <div class="modal-footer">
                    <button type="button" wire:click="addTodosOsAlunosATurma()" class="btn btn-primary weight-600 py-2">Confirmar</button>
                </div>
            </div>
        </div>
    </div>


    @include('livewire.util.toast')
    
</div>