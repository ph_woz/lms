<?php

namespace App\Http\Livewire\Admin\Servico;

use Livewire\WithPagination;
use Livewire\Component;
use App\Models\Servico;

class Lista extends Component
{
    use WithPagination;

    protected $paginationTheme = 'bootstrap';

    public $search;
    public $status = 0;
    public $ordem;

    protected $queryString = ['search','status','ordem'];

    public function render()
    {
        $servicos = Servico::plataforma();

        if($this->search)
        {
            $servicos = $servicos->where('nome', 'like', '%'.$this->search.'%');
        }

        $servicos = $servicos->where('status', $this->status);

        $countServicos = $servicos->count();


        if($this->ordem == 'desc' || $this->ordem == 'asc')
        {
            $servicos = $servicos->orderBy('id', $this->ordem);
        
        } else {

            $servicos = $servicos->orderBy('nome');
        }
        

        $servicos = $servicos->paginate(10);


        return view('livewire.admin.servico.lista', [
            'servicos'      => $servicos,
            'countServicos' => $countServicos,
        ]);
    }
}
