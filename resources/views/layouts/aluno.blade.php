<!DOCTYPE html>
<html lang="pt-br">
<head>

  <!-- Google tag (gtag.js) -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=G-MREB450HCT"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'G-MREB450HCT');
  </script>
  
    {!! $plataforma->scripts_start_head !!}

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="robots" content="noindex, nofollow">
    <meta name="author" content="Pedro Henrique | WOZCODE">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link rel="shortcut icon" href="{{ $plataforma->favicon }}" />
    <title>@yield('seo_title') - Área do Aluno | {{ $nomePlataforma }}</title>

    @if($plataforma->tema == 'L')
      <link rel="stylesheet" href="{{ asset('css/alunoLight.css') }}?v=4">
    @else
      <link rel="stylesheet" href="{{ asset('css/aluno.css') }}?v=4">
    @endif
    
    <link rel="stylesheet" href="{{ asset('css/util.css') }}?v=3">
    <link rel="stylesheet" href="{{ asset('main/css/app.css') }}?v=3">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
    
    <style>
      #url-{{Request::segment(2)}} .side-color { background: rgba(255,255,255,0.09)!important; }
    </style>

    @livewireStyles
    @yield('css')

    {!! $plataforma->scripts_final_head !!}

</head>

<body>

    {!! $plataforma->scripts_start_body !!}

    <div class="wrapper">
        <nav id="sidebar" class="sidebar js-sidebar">
            <div class="sidebar-content js-simplebar bg-14">

               <div class="flex-center py-4" id="boxLogoSidebarAluno">
                  <img src="{{ $plataforma->logotipo }}" width="80" id="logoSidebarAluno">
               </div>

               <ul class="sidebar-nav">

                   <li class="sidebar-item" id="url-dashboard">
                       <a class="side-color py-2-5 sidebar-link" href="{{ route('aluno.dashboard') }}">
                           <i class="color-icon-sidebar bg-transparent fa fa-home"></i> 
                           <span id="span-inicio">Início</span>
                           <span class="align-middle"></span>
                       </a>
                   </li>
                   <li class="sidebar-item" id="url-avaliacoes">
                       <a class="side-color py-2-5 sidebar-link" href="{{ route('aluno.avaliacoes') }}">
                           <i class="color-icon-sidebar bg-transparent fa fa-edit"></i> 
                           <span id="span-avaliacoes">Avaliações</span>
                           <span class="align-middle"></span>
                       </a>
                   </li>
                   <li class="sidebar-item" id="url-comunicados">
                       <a class="side-color py-2-5 sidebar-link" href="{{ route('aluno.comunicados') }}">
                           <i class="color-icon-sidebar bg-transparent fa fa-bullhorn"></i> 
                           <span id="span-comunicados">Comunicados</span>
                           <span class="align-middle"></span>
                           <span class="bg-primary fs-10 px-2 py-1 rounded weight-600 ms-3 d-none" id="elNovoComunicado">
                              NÃO LIDO
                           </span>
                       </a>
                   </li>

                   <li class="sidebar-item" id="url-financeiro">
                       <a class="side-color py-2-5 sidebar-link" href="{{ route('aluno.financeiro') }}">
                           <i class="color-icon-sidebar bg-transparent fas fa-hand-holding-usd"></i>
                           <span id="span-financeiro">Financeiro</span>
                           <span class="align-middle"></span>
                       </a>
                   </li>                   

                   <li class="sidebar-item" id="url-suporte">
                       <a class="side-color py-2-5 sidebar-link" href="{{ route('aluno.suporte') }}">
                           <i class="color-icon-sidebar bg-transparent fas fa-headset"></i>
                           <span id="span-suporte">Suporte</span>
                           <span class="align-middle"></span>
                       </a>
                   </li>
                   
                   <li class="sidebar-item" id="url-academico">
                       <a class="side-color py-2-5 sidebar-link" href="{{ route('aluno.academico') }}">
                           <i class="color-icon-sidebar bg-transparent fas fa-graduation-cap" aria-hidden="true"></i>
                           <span id="span-academico">Acadêmico</span>
                           <span class="align-middle"></span>
                       </a>
                   </li>

               </ul>

            </div>
        </nav>

        <div class="main bg-0d">
            <nav class="navbar navbar-expand navbar-light navbar-bg box-shadow bg-14">
                <a class="sidebar-toggle js-sidebar-toggle">
                    <i class="hamburger align-self-center"></i>
                </a>

                <h1 class="title fs-16 m-0 p-0" id="getNomePage">
                    {{ $getNomePage ?? null }}
                </h1>

                <div class="navbar-collapse collapse">
                    <ul class="navbar-nav navbar-align">
                        <li class="nav-item dropdown">

                            <a class="nav-link dropdown-toggle border-0 color-drop" href="#" data-bs-toggle="dropdown">
                                <span>
                                    {{ strtok(Auth::user()->nome, " ") }}
                                </span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-end">
                                <a class="dropdown-item" href="{{ route('aluno.minha-conta') }}"><i class="align-middle" data-feather="user"></i> Minha conta</a>
                                <a class="dropdown-item" href="{{ route('logout') }}"><i class="align-middle" data-feather="log-out"></i> Sair</a>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>

            <main class="content p-0 mainContent" id="mainContent-{{Request::segment(2)}}">
               <div class="container-fluid p-0 pb-5">
                  
                  @yield('content')

               </div>
            </main>


            <footer class="footer">
                <div class="container-fluid">
                    <div class="row align-items-center text-muted">
                        <div class="order-1 order-lg-0 col-lg-6 text-lg-start text-xs-center">
                            <p class="mb-0 mt-1 mt-lg-0 text-muted fs-9">
                                Desenvolvido por
                                <a href="https://wozcode.com" class="text-muted" target="_blank">
                                  <strong>
                                    WOZCODE
                                  </strong>
                                  &copy;
                                </a> 
                            </p>
                        </div>
                        <div class="order-0 order-lg-1 col-lg-6 text-xs-center text-lg-end">
                            <p class="mb-0 text-muted">
                              {{ $nomePlataforma }}
                            </p>
                        </div>
                    </div>
                </div>
            </footer>


        </div>
    </div>
    <script src="{{ asset('main/js/app.js') }}"></script>

    @if(session()->has('success'))
        <div id="box-toast-success" class="boxToast">
            <div id="alertSuccess" class="toast-success">
                <p class="mt-2 text-white">
                    <span class="weight-800">Sucesso!&nbsp;</span>{{ session('success') }}
                </p>
            </div>
        </div>
        <script>
            setTimeout(function() {

                document.getElementById('box-toast-success').style.display = 'none';

            }, 2500);
        </script>
    @endif

    @if(session()->has('danger'))
        <div id="box-toast-danger" class="boxToast">
            <div id="alertDanger" class="toast-danger">
                <p class="mt-2 text-white">
                    <span class="weight-800">Ops!&nbsp;</span>{{ session('danger') }}
                </p>
            </div>
        </div>
        <script>
            setTimeout(function() {

                document.getElementById('box-toast-danger').style.display  = 'none';

            }, 2500);
        </script>
    @endif

    <script>
      if(localStorage.getItem('hasComunicado') != null)
        document.getElementById('elNovoComunicado').classList.remove('d-none');
    </script>

    {!! $plataforma->scripts_final_body !!}

    @livewireScripts
    @yield('js')
    @stack('scripts')
</body>

</html>
