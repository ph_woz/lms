<div>

    <div class="col-12">
        @if (session()->has('success-livewire'))
            <div class="alert alert-success alert-dismissible" role="alert">
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                <div class="alert-message">
                	Unidade criada:
                    <span>
	                    <a href="{{ route('admin.unidade.info', ['id' => $this->unidade->id ?? null]) }}" class="weight-600">
	                    	{{ $unidade->nome }} 
	                	</a>
	                </span>
                </div>
            </div>
        @endif
    </div>

    <form wire:submit.prevent="add" method="POST" class="mb-4">

		<section class="mb-4-5">
			
			<a href="#boxInfoPrincipal" data-bs-toggle="collapse" aria-expanded="false" class="hover-d-none">
				<div class="section-title">
					Informações Principais
				</div>
			</a>
			<hr/>
			
			<div class="collapse show" id="boxInfoPrincipal">

		    	<div class="d-lg-flex mb-3">

			    	<div class="w-100 me-lg-3 mb-3 mb-lg-0">
			    		<label>Nome</label>
			    		<input type="text" wire:model="nome" class="form-control p-form" placeholder="Referência" required>
			    	</div>
			    	<div class="w-lg-50 me-lg-3 mb-3 mb-lg-0">
			    		<label>Celular</label>
			    		<input type="phone" wire:model="celular" class="celular form-control p-form" placeholder="(00) 00000-0000">
			    	</div>
			    	<div class="w-lg-50 me-lg-3 mb-3 mb-lg-0">
			    		<label>Telefone</label>
			    		<input type="phone" wire:model="telefone" class="telefone form-control p-form" placeholder="(00) 0000-0000">
			    	</div>
			    	<div class="w-lg-35 me-lg-3 mb-3 mb-lg-0">
			    		<label>Publicar</label>
			    		<select wire:model="publicar" class="select-form">
			    			<option value="N">Não</option>
			    			<option value="S">Sim</option>
			    		</select>
			    	</div>
			    	<div class="w-lg-35">
			    		<label>Status</label>
			    		<select wire:model="status" class="select-form">
			    			<option value="0">Ativo</option>
			    			<option value="1">Desativado</option>
			    		</select>
			    	</div>

			    </div>

		    	<div class="d-lg-flex mb-3">

			    	<div class="w-100 me-lg-3 mb-3 mb-lg-0">
			    		<label>Email</label>
			    		<input type="email" wire:model="email" class="form-control p-form" placeholder="Email">
			    	</div>
			    	<div class="w-100">
			    		<label>Endereço</label>
			    		<input type="text" wire:model="endereco" class="form-control p-form" placeholder="Endereço">
			    	</div>

			    </div>

			</div>

		</section>

		<button type="submit" class="btn-add mt-3">Cadastrar</button>	

    </form>

    @include('livewire.util.toast')

</div>
