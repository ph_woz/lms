<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMensalidadesPorSemestresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mensalidades_por_semestres', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('plataforma_id');
            $table->unsignedBigInteger('registro_id');
            $table->string('tipo', 1);
            $table->integer('semestre');
            $table->string('valor');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mensalidades_por_semestres');
    }
}
