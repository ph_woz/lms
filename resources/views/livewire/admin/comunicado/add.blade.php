<div>

    <div class="col-12">
        @if (session()->has('success-livewire'))
            <div class="alert alert-success alert-dismissible" role="alert">
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                <div class="alert-message">
                	Comunicado criado:
                    <span>
	                    <a href="{{ route('admin.comunicado.info', ['id' => $this->comunicado->id ?? null]) }}" class="weight-600">
	                    	{{ $comunicado->assunto }} 
	                	</a>
	                </span>
                </div>
            </div>
        @endif
    </div>

    <form wire:submit.prevent="add" method="POST" class="mb-4">

		<section class="mb-4-5">
			
			<a href="#boxInfoPrincipal" data-bs-toggle="collapse" aria-expanded="false" class="hover-d-none">
				<div class="section-title">
					Informações Principais
				</div>
			</a>
			<hr/>
			
			<div class="collapse show" id="boxInfoPrincipal">

		    	<div class="d-lg-flex mb-3">

			    	<div class="w-100 me-lg-3 mb-3 mb-lg-0">
			    		<label>Assunto</label>
			    		<input type="text" wire:model="assunto" class="form-control p-form" placeholder="Do que se refere o comunicado" required>
			    	</div>
			    	<div class="w-lg-35">
			    		<label>Arquivar</label>
			    		<select wire:model="arquivar" class="select-form">
			    			<option value="N">Não</option>
			    			<option value="S">Sim</option>
			    		</select>
			    	</div>

			    </div>

			    <div class="mb-3">
			    	<label>Descrição</label>
			    	<textarea wire:model="descricao" class="textarea-form" placeholder="Descreva" required></textarea>
			    </div>

			</div>

		</section>

		<button type="submit" class="btn-add mt-3">Cadastrar</button>	

    </form>

    @include('livewire.util.toast')

</div>
