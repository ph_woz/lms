<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CorpoDocente extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $table = 'corpo_docente';

    protected $fillable = [ 
    	'plataforma_id', 
    	'registro_id',
    	'tipo',
    	'nome',
    	'titulacao',
    	'formacao',
    	'foto_perfil',
    ];

    public static  function boot()
    {
        parent::boot(); 
        static::creating(function ($model) {
            $model->plataforma_id = session('plataforma_id') ?? \Auth()->user()->plataforma_id ?? Plataforma::dominio()->pluck('id')[0] ?? null;
        });
    }

    public function scopePlataforma($query)
    {
        return $query->where('plataforma_id', session('plataforma_id') ?? Plataforma::dominio()->pluck('id')[0] ?? null);
    }

    public function scopeCurso($query)
    {
        return $query->where('tipo', 'C');
    }

    public function scopeTrilha($query)
    {
        return $query->where('tipo', 'T');
    }

}
