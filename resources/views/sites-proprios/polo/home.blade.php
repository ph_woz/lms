<!doctype html>
<html lang="pt-br">
<head>

  <!-- Google tag (gtag.js) -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=G-MREB450HCT"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'G-MREB450HCT');
  </script>
  
   {!! $plataforma->scripts_start_head !!}

   <meta charset="utf-8">
   <meta name="viewport" content="width=device-width, initial-scale=1">

   <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KK94CHFLLe+nY2dmCWGMq91rCGa5gtU4mk92HdvYe+M/SXH301p5ILy+dN9+nJOZ" crossorigin="anonymous">
   <link rel="stylesheet" href="{{ asset('sites-proprios/polo/css/home.css') }}?v=4">
   <link rel="stylesheet" href="{{ asset('css/util.css') }}">

   <title>{{ $seo->seo_title ?? $plataforma->nome }}</title>
   <meta name="robots" content="index, follow">
   <meta name="description" content="{{ $seo->seo_description ?? null }}"/>
   <meta name="keywords" content="{{ $seo->seo_keywords ?? null }}"/>
   <link rel="canonical" href="{{ Request::url() }}" />
   <link rel="shortcut icon" href="{{ $plataforma->favicon }}">
   <meta property="og:locale" content="pt_BR" />
   <meta property="og:type" content="website" />
   <meta property="og:title" content="{{ $seo->seo_title ?? $plataforma->nome }}" />
   <meta property="og:description" content="{{ $seo->seo_description ?? null }}" />
   <meta property="og:url" content="{{ Request::url() }}" />
   <meta property="og:site_name" content="{{ $plataforma->nome }}" />
   <meta property="og:image" content="{{ $plataforma->logotipo }}" />
   <meta name='author' content='WOZCODE | Pedro Henrique' />

   {!! $plataforma->scripts_final_head !!}

</head>
<body id="inicio">

	{!! $plataforma->scripts_start_body !!}

	<header class="py-1">
		<div class="flex-center">
			<img src="{{ $plataforma->logotipo ?? '/images/no-image.jpeg' }}" class="img-fluid" width="200" alt="Logotipo">
		</div>
	</header>

	<main>

		<section class="object-fit-cover w-100" id="banner" style="background-image: url('{{ $plataforma->jumbotron ?? asset('images/utils/header-cone-two-people.png') }}');">
			<div class="container-fluid px-xl-5">
				<div class="row gx-0 pt-5">
					<div class="col-lg-7" id="form">

						<h1 class="font-montserrat text-light text-center weight-600 fs-35">
							<span class="d-lg-block">Modelo de negócio em</span> <span>educação Inovador e Lucrativo!</span>
						</h1>
						
						<div class="pt-3 flex-center">
							<div class="col-lg-8">
								<div class="bg-base-lp p-lg-4 p-3 rounded-6 box-shadow-lp">

									<h1 class="text-center color-titulo-form-lp weight-800 fs-30 font-montserrat">
										SEJA NOSSO PARCEIRO!
									</h1>

									<p class="text-center text-light fs-17 font-montserrat weight-600 text-shadow-2">
										Preencha os dados abaixo, nossa equipe entrará em contato em breve.
									</p>

									<form method="POST">
									@csrf

										<input type="text"  name="nome" class="mb-2 lead-form px-3 form-control fs-17 outline-0 rounded-2 border-0 h-50px" placeholder="Nome" required>
										<input type="email" name="email" class="mb-2 lead-form px-3 form-control fs-17 outline-0 rounded-2 border-0 h-50px" placeholder="Email" required>
										<input type="phone" name="celular" class="mb-2 lead-form px-3 form-control fs-17 outline-0 rounded-2 border-0 h-50px" placeholder="Celular (com DDD)" required>
										
										<div class="d-flex">
											<input type="text" name="estado" onkeyup="this.value = this.value.toUpperCase();" class="me-2 w-lg-30 w-xs-40 mb-2 lead-form px-lg-3 px-xs-2-5 form-control fs-17 outline-0 rounded-2 border-0 h-50px" placeholder="Estado (UF)" maxlength="2" required>
											<input type="text" name="cidade" class="w-70 mb-2 lead-form px-3 form-control fs-17 outline-0 rounded-2 border-0 h-50px" placeholder="Cidade" required>
										</div>

										<select name="mensagem" class="lead-form-select px-3 w-100 fs-17 outline-0 rounded-2 border-0 h-50px" required>
											<option class="lead-form-select" value="">Possui estrutura física?</option>
											<option class="lead-form-select" value="Sim, possuo estrutura física.">Sim, possuo estrutura física.</option>
											<option class="lead-form-select" value="Não, não possuo estrutura física.">Não, não possuo estrutura física.</option>
										</select>

										<input type="text" name="check" class="h-0px invisible">

										<button type="submit" id="btn-01" class="btn rounded-3 bg-base-button-form-lp bg-base-button-form-lp-hover color-base-button-form-lp color-base-button-form-lp-hover weight-700 px-3 h-60px w-100 font-montserrat">
											QUERO FALAR COM UM CONSULTOR!
										</button>

									</form>

								</div>
							</div>
						</div>

					</div>
					<div class="col-lg-4 mt-5 mt-lg-0">
						<div class="row h-100 align-items-end">
							<div class="d-lg-flex">

								<div class="mb-4 mb-lg-0 me-lg-3 w-100 text-center bg-card-baixo-custo-lp animation-grow p-lg-2 p-4 rounded box-shadow-2-lp">
									<i class="text-light fs-30 me-3 bi bi-cash-coin"></i>
									<h1 class="fs-15 text-light weight-400 font-montserrat">
										Com <span class="weight-700">baixo custo</span> 
										de investimento
										(a partir de R$ 197,99/mês)
									</h1>
								</div>
								
								<div class="w-100 text-center bg-card-baixo-custo-lp animation-grow p-lg-2 p-4 rounded box-shadow-2-lp">
									<i class="text-light fs-30 me-3 bi bi-pc-display-horizontal"></i>
									<h1 class="fs-15 text-light weight-400 font-montserrat">
										Educação no modelo <span class="weight-700">Híbrido e 100% Online!</span> 
									</h1>
								</div>

							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

		<section class="mt-lg-200 mt-xs-5">
			<div class="container pb-5">

				<h1 class="pt-5 mt-lg-0 display-1 text-center font-montserrat weight-700 color-titulo-lp">
					VANTAGENS
				</h1>

				<h2 class="display-6 text-center font-montserrat weight-600 color-subtitulo-lp">
					de ser um parceiro {{ $plataforma->nome }}
				</h2>

				<div class="row px-3 px-lg-0 pt-5">

					<div class="col-lg-3 mb-5 mb-lg-0">
						<div class="card bg-card-lp rounded border-0">
							<div class="card-header flex-center border-0 p-0">
								<div class="mt--35px bg-circle-lp box-shadow square-75 rounded-circle flex-center">
									<span class="weight-700 font-montserrat fs-40 text-light">1</span>
								</div>
							</div>
							<div class="card-body h-lg-200 border-0 pb-4 pb-lg-0">

								<h1 class="text-light text-center font-montserrat weight-700 fs-25">
									Baixo Investimento
								</h1>

								<p class="mb-0 text-center font-montserrat text-light weight-500">
									Modelo de Negócio LOW COST (CUSTO BAIXO), a partir de R$ 197,99/mês
								</p>

							</div>
						</div>
					</div>
					<div class="col-lg-3 mb-5 mb-lg-0">
						<div class="card bg-card-lp rounded border-0">
							<div class="card-header flex-center border-0 p-0">
								<div class="mt--35px bg-circle-lp box-shadow square-75 rounded-circle flex-center">
									<span class="weight-700 font-montserrat fs-40 text-light">2</span>
								</div>
							</div>
							<div class="card-body h-lg-200 border-0 pb-4 pb-lg-0">

								<h1 class="text-light text-center font-montserrat weight-700 fs-25">
									Assessoria e treinamento
								</h1>

								<p class="mb-0 text-center font-montserrat text-light weight-500">
									Suporte, Orientação em Marketing, Treinamento e Assesoria contínua para o Gestor Parceiro
								</p>

							</div>
						</div>
					</div>
					<div class="col-lg-3 mb-5 mb-lg-0">
						<div class="card bg-card-lp rounded border-0">
							<div class="card-header flex-center border-0 p-0">
								<div class="mt--35px bg-circle-lp box-shadow square-75 rounded-circle flex-center">
									<span class="weight-700 font-montserrat fs-40 text-light">3</span>
								</div>
							</div>
							<div class="card-body h-lg-200 border-0 pb-4 pb-lg-0">

								<h1 class="text-light text-center font-montserrat weight-700 fs-25">
									Alta Lucratividade
								</h1>

								<p class="mb-0 text-center font-montserrat text-light weight-500">
									Modelo de Negócio LOW COST (CUSTO BAIXO), a partir de R$ 197,99/mês
								</p>

							</div>
						</div>
					</div>
					<div class="col-lg-3 mb-5 mb-lg-0">
						<div class="card bg-card-lp rounded border-0">
							<div class="card-header flex-center border-0 p-0">
								<div class="mt--35px bg-circle-lp box-shadow square-75 rounded-circle flex-center">
									<span class="weight-700 font-montserrat fs-40 text-light">4</span>
								</div>
							</div>
							<div class="card-body h-lg-200 border-0 pb-4 pb-lg-0">

								<h1 class="text-light text-center font-montserrat weight-700 fs-25">
									Reconhecido pelo Mercado
								</h1>

								<p class="mb-0 text-center font-montserrat text-light weight-500">
									Oferte cursos com excelente reconhecimento
								</p>

							</div>
						</div>
					</div>

				</div>

				<div class="text-center mt-lg-5 px-4 px-xl-0">
					<a href="#form" id="btn-02" class="btn rounded-3 bg-base-button-lp bg-base-button-lp-hover color-base-button-lp color-base-button-lp-hover weight-700 px-3-5 py-3 font-montserrat">
						QUERO ME TORNAR UM GESTOR DE POLO!
					</a>
				</div>

			</div>
		</section>

		<section class="py-5">
			<div class="container pb-5">
				<div class="col-lg-10 offset-lg-1 bg-white p-4-5 box-shadow rounded">
					
					<div class="mb-4 row align-items-center">
						
						<div class="col-lg-6">
							<h1 class="font-montserrat weight-700 color-titulo-lp">
								<span class="color-invista-lp d-block fs-55">
									INVISTA NO
								</span>
								<span class="color-lugar-certo-lp mt--10px d-block fs-44">
									LUGAR CERTO
								</span>
							</h1>
						</div>

						<div class="col-lg-6">
							<div>
								<p class="m-0 font-poppins fs-17">
									Por conta do <span class="weight-600">baixo valor de investimento</span> e da <span class="weight-600">grande procura</span> por um Ensino EJA EAD de qualidade, os <span class="weight-600">resultados positivos</span> de se tornar um Gestor EAD são <span class="weight-600">concretos</span>!
								</p>
							</div>
						</div>

					</div>

					<div class="d-lg-flex">

						<div class="w-100 me-lg-4">

							<div class="mb-2 d-flex align-items-center bg-quadrado-invista h-60px rounded">
								<div class="bg-square-invista text-light fs-20 square-60 rounded rounded-right-0 flex-center">
									<i class="bi bi-bar-chart-line"></i>
								</div>
								<span class="ms-3 text-light weight-500 font-montserrat w-100 px-2">
									Demanda <span class="weight-700">constante</span> e <span class="weight-700">crescente</span>
								</span>
							</div>
							<div class="mb-2 d-flex align-items-center bg-quadrado-invista h-60px rounded">
								<div class="bg-square-invista text-light fs-20 square-60 rounded rounded-right-0 flex-center">
									<i class="bi bi-graph-up-arrow"></i>
								</div>
								<span class="ms-3 text-light weight-500 font-montserrat w-100 px-2">
									Capacidade de <span class="weight-700">Expansão</span> e <span class="weight-700">Escala</span>
								</span>
							</div>

						</div>

						<div class="w-100">

							<div class="mb-2 d-flex align-items-center bg-quadrado-invista h-60px rounded">
								<div class="bg-square-invista text-light fs-20 square-60 rounded rounded-right-0 flex-center">
									<i class="bi bi-cash-coin"></i>
								</div>
								<span class="ms-3 text-light weight-500 font-montserrat w-100 px-2">
									Média de <span class="weight-600">1 mês</span> para <span class="weight-600">retorno</span>
								</span>
							</div>
							<div class="mb-2 d-flex align-items-center bg-quadrado-invista h-60px rounded">
								<div class="bg-square-invista text-light fs-20 square-60 rounded rounded-right-0 flex-center">
									<i class="bi bi-piggy-bank"></i>
								</div>
								<span class="ms-3 text-light weight-500 font-montserrat w-100 px-2">
									Menor custo de Investimento do Mercado
								</span>
							</div>

						</div>

					</div>

					<div class="text-center mt-4-5">
						<a href="#form" id="btn-03" class="btn rounded-3 bg-base-button-lp bg-base-button-lp-hover color-base-button-lp color-base-button-lp-hover weight-700 px-3-5 py-3 font-montserrat">
							QUERO ME TORNAR UM GESTOR DE POLO!
						</a>
					</div>

				</div>
			</div>
		</section>

		<section class="py-5" style="background-image: url('{{ asset('images/utils/off-white-map.png') }}'); background-repeat: no-repeat; background-size: cover; background-position: center center;">
			<div class="container">
				<div class="row">

					<div class="col-lg-5 offset-lg-1">

						<h1 class="mb-4 display-2 font-montserrat weight-700 color-titulo-lp">
							<span class="d-block">NOSSA</span>
							<span class="d-block mt--15px color-nossa-estrutura-lp">ESTRUTURA</span>
						</h1>

						<div class="mb-2 d-flex align-items-center h-60px rounded">
							<div class="bg-square-invista text-light fs-20 square-60 px-4 flex-center">
								<i class="bi bi-building"></i>
							</div>
							<span class="ms-3 text-light weight-500 font-montserrat w-100 px-2 text-dark">
								Mais de 35 Polos de Educação a Distância em todos os estados do Brasil.
							</span>
						</div>

						<hr>

						<div class="mb-2 d-flex align-items-center h-60px rounded">
							<div class="bg-square-invista text-light fs-20 square-60 px-4 flex-center">
								<i class="bi bi-globe-americas"></i>
							</div>
							<span class="ms-3 text-light weight-500 font-montserrat w-100 px-2 text-dark">
								Maior Rede educacional do País.
							</span>
						</div>

						<hr>

						<div class="mb-2 d-flex align-items-center h-60px rounded">
							<div class="bg-square-invista text-light fs-20 square-60 px-4 flex-center">
								<i class="bi bi-person-video3"></i>
							</div>
							<span class="ms-3 text-light weight-500 font-montserrat w-100 px-2 text-dark">
								Material acadêmico produzido por um corpo docente especializado.
							</span>
						</div>

						<hr>

						<div class="mb-2 d-flex align-items-center h-60px rounded">
							<div class="bg-square-invista text-light fs-22 square-60 px-4 flex-center">
								<i class="bi bi-hand-thumbs-up"></i>
							</div>
							<span class="ms-3 text-light weight-500 font-montserrat w-100 px-2 text-dark">
								Treinamento e suporte contínuo para os gestores.
							</span>
						</div>

					</div>

					<div class="col-lg-6 d-xs-none">

						<img src="{{ asset('images/utils/many-dot-on-map.png') }}" alt="Unidades">

					</div>

				</div>

				<div class="text-center mt-3 px-4 px-xl-0 pb-5">
					<a href="#form" id="btn-02" class="btn rounded-3 bg-base-button-lp bg-base-button-lp-hover color-base-button-lp color-base-button-lp-hover weight-700 px-3-5 py-3 font-montserrat">
						QUERO ME TORNAR UM GESTOR DE POLO!
					</a>
				</div>

			</div>
		</section>

		<section>
			<div class="container-fluid px-0">
				<div class="row align-items-center gx-0">

					<div class="col-lg-6">

						<div class="bg-white p-3">

							<img src="{{ $plataforma->logotipo ?? '/images/no-image.jpeg' }}" class="img-fluid" alt="Logotipo {{ $plataforma->nome }}">

						</div>

					</div>

					<div class="col-lg-6">

						<div class="bg-base-quem-somos-lp p-lg-5 p-4">

							<h1 class="display-4 font-montserrat weight-700 color-titulo-quem-somos-lp">
								QUEM SOMOS
							</h1>

							<p class="mt-4 display-8 font-montserrat weight-400 color-subtitulo-quem-somos-lp">
								{!! nl2br($textoQuemSomos ?? 'It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. <br> <br> It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.') !!}
							</p>

							<div class="mt-4-5">
								<a href="#form" id="btn-04" class="btn rounded-3 bg-base-button-quem-somos-lp bg-base-button-quem-somos-lp-hover color-base-button-quem-somos-lp color-base-button-quem-somos-lp-hover weight-700 px-3-5 py-3 font-montserrat">
									QUERO ME TORNAR UM PARCEIRO!
								</a>
							</div>

						</div>

					</div>

				</div>
			</div>
		</section>

		<section style="background-size: cover; background-image: url('{{ asset('images/utils/section-white-with-people-to-right.png') }}'); background-position: center center; background-repeat: no-repeat;">
			<div class="container border-top py-5">
				<div class="row g-0">

					<div class="col-lg-5 offset-lg-1 py-5">

						<p class="mb-0 font-montserrat weight-600 fs-23">
							EDUCAÇÃO NO MODELO
						</p>

						<h1 class="mb-4 display-3 font-montserrat weight-700 color-titulo-lp">
							<span class="color-contraste-titulo">HÍBRIDO</span><span>E 100%</span>
							<span class="color-contraste-titulo">ONLINE</span>
						</h1>

						<p class="mb-0 weight-500 font-montserrat fs-18">
							Com uma <span class="weight-600">estrutura simplificada</span> e com um <span class="weight-600">pequeno investimento</span>, você pode se tornar um <span class="weight-600">grande gestor</span> de Polo da {{ $plataforma->nome }} e fazer parte do maior grupo educacional de <span class="weight-600">empresários vencedores</span> com <span class="weight-600">alta lucratividade</span>.
						</p>

						<div class="mt-4-5">
							<a href="#form" id="btn-05" class="btn rounded-3 bg-base-button-lp bg-base-button-lp-hover color-base-button-lp color-base-button-lp-hover weight-700 px-3-5 py-3 font-montserrat">
								QUERO ME TORNAR UM GESTOR DE POLO!
							</a>
						</div>

					</div>

				</div>
			</div>
		</section>

      	<section>
       		<div class="w-100 background-size-cover background-position-center" style="background-image: url('{{ asset('images/utils/banner-page-01.webp') }}">
          		<div class="position-relative h-100 px-xl-7 px-3">
          			<div class="background-overlay opacity-08 h-100" style="background: rgba(23, 83, 135,.8);"></div>
		             <div class="p-lg-5 p-3 row align-items-center h-100 position-relative">
		                <div class="mt-2 col-lg-6 offset-lg-3">

		                	<div class="bg-card-rodape-lp p-4 rounded-2 box-shadow">

			                   	<h1 class="text-shadow-2 text-center text-light font-poppins weight-600 fs-17">
			                   		SEJA UM PARCEIRO {{ $plataforma->nome }}
			                   	</h1>

			                   	<p class="text-shadow-2 line-height-40 mb-0 text-center text-light font-montserrat weight-700 fs-30">
			                   		Torne-se um Grande Gestor de Alta Lucratividade com Pouco Investimento!
			                   	</p>

			                   	<hr class="border">

								<div class="mt-4 text-center">
									<a href="#form" id="btn-06" class="btn rounded-3 bg-base-button-card-lp bg-base-button-card-lp-hover color-base-button-card-lp color-base-button-card-lp-hover weight-700 px-3-5 py-3 font-montserrat">
										QUERO ME TORNAR UM GESTOR DE POLO!
									</a>
								</div>

			                </div>

		                </div>

		             </div>
          		</div>
       		</div>
      	</section>

	</main>

	<footer class="py-2">
		<div class="container">
	      	<div class="row">

	         	<div class="col-sm-6 text-xs-center fs-11">
	            	<span class="weight-600 fs-11">{{ $plataforma->nome }}</span> © 2021 - Todos os direitos reservados <span class="d-xs-none">/</span> <span class="d-xs-block">CNPJ: {{ $plataforma->cnpj ?? 'Não definido' }}</span>
	        	</div>
	         	<div class="col-sm-6 text-xs-center footer__copyright--by fs-11">
	            	Desenvolvido por <a href="https://wozcode.com" class="link--white" target="_blank"><strong class="fs-11 footer__copyright--by">WOZCODE.</strong></a>
	         	</div> 

	      	</div>
	    </div>
	</footer>

{{--    <a href="https://api.whatsapp.com/send?phone=55{{preg_replace('/\D/', '', $plataforma->whatsapp)}}&text=Olá, vim através do Site {{ $plataforma->nome }}" target="_blank" class="text-white fs-30">
      <div class="position-fixed bottom-0 right-0 me-3 me-lg-4 mb-3 mb-lg-4 z-4 wpp-pulse-button z-8">
         <i class="bi bi-whatsapp"></i>
      </div>
   </a> --}}

	{!! $plataforma->scripts_final_body !!}

</body>
</html>