<div>

    <div class="col-12">
        @if (session()->has('success-livewire'))
            <div class="alert alert-success alert-dismissible" role="alert">
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                <div class="alert-message">
                    Turma criada:
                    <span>
	                    <a href="{{ route('admin.turma.info', ['id' => $this->turma->id ?? null]) }}" class="weight-600">
	                    	{{ $turma->nome }} 
	                	</a>
	                </span>
                </div>
            </div>
        @endif
    </div>

    <form wire:submit.prevent="add" method="POST" class="mb-4">

		<section class="mb-4-5">
			
			<a href="#boxInfoPrincipal" data-bs-toggle="collapse" aria-expanded="false" class="hover-d-none">
				<div class="section-title">
					Informações Gerais
				</div>
			</a>
			<hr/>
			
			<div class="collapse show" id="boxInfoPrincipal">

		    	<div class="d-lg-flex mb-3">

			    	<div class="w-100">
			    		<label>Nome</label>
			    		<input type="text" wire:model="nome" class="form-control p-form" placeholder="Nome" required>
			    	</div>
					<div class="w-100 mx-lg-3 my-3 my-lg-0" wire:ignore>
						<label>Curso</label>
						<select wire:model="curso_id" class="selectpicker" title="Selecione" data-width="100%" data-live-search="true" required>
							@foreach($cursos as $curso)
								<option value="{{ $curso->id }}">
									{{ $curso->referencia }}
								</option>
							@endforeach
						</select>
					</div>
			    	<div class="w-lg-35">
			    		<label>Status</label>
			    		<select wire:model="status" class="select-form">
			    			<option value="0">Ativo</option>
			    			<option value="1">Desativado</option>
			    		</select>
			    	</div>

			    </div>

			    <div class="d-lg-flex mb-3">

			    	<div class="w-lg-65">
			    		<label>Nota mínima para aprovação</label>
			    		<input type="text" wire:model="nota_minima_aprovacao" class="form-control p-form" placeholder="Digite o valor da nota">
			    	</div>
					<div class="w-100 mx-lg-3 my-3 my-lg-0" wire:ignore>
						<label>Formulário de Inscrição de Matrícula</label>
						<select wire:model="formulario_id" class="selectpicker" title="Selecione" data-width="100%" data-live-search="true">
							<option value="">Selecione</option>
							@foreach($formularios as $formulario)
								<option value="{{ $formulario->id }}">
									{{ $formulario->referencia }}
								</option>
							@endforeach
						</select>
					</div>
					<div class="w-lg-70">
						<label>Tipo de Inscrição de Matrícula</label>
						<select wire:model="tipo_inscricao" class="select-form">
							<option value="requer-aprovacao">Requer aprovação</option>
							<option value="auto-inscricao">Auto Inscrição</option>
						</select>
					</div>
					<div class="w-lg-30 ms-lg-3 mt-3 mt-lg-0">
						<label>Publicar</label>
						<select wire:model="publicar" wire:change="changeEventPublicar($event.target.value)" class="select-form">
							<option value="N">Não</option>
							<option value="S">Sim</option>
						</select>
					</div>

			    </div>

				<div class="d-lg-flex mb-3">

			    	<div class="w-100 me-lg-3 mb-3 mb-lg-0">
			    		<label>Valor a vista</label>
			    		<input type="text" wire:model="valor_a_vista" id="valor" class="form-control p-form" placeholder="R$">
			    	</div>
			    	<div class="w-100">
			    		<label>Valor parcelado</label>
			    		<input type="text" wire:model="valor_parcelado" class="form-control p-form" placeholder="Ex: 3x de R$ 97,99">
			    	</div>

				</div>
					
				@if(\App\Models\Unidade::checkActive() === true)
				<div class="mb-3 boxUnidadeRemove" wire:ignore>
					<label>Unidade</label>
					<select wire:model="unidade_id" id="unidade_id" class="selectpicker" data-width="100%">
						<option value="">Selecione</option>
						@foreach($unidades as $unidade)
							<option value="{{ $unidade->id }}">
								{{ $unidade->nome }}
							</option>
						@endforeach
					</select>
				</div>
				@endif

			    <div id="boxTurmaApresentacao" style="display: {{$controlBoxApresentacao}};">

				    <div class="d-lg-flex mb-3">

						<div class="w-100">
							<label>Data de Início</label>
							<input type="date" wire:model="data_inicio" class="form-control p-form">
						</div>
						<div class="w-100 mx-lg-3 my-3 my-lg-0">
							<label>Data de Término</label>
							<input type="date" wire:model="data_termino" class="form-control p-form">
						</div>
				    	<div class="w-100">
				    		<label>Duração</label>
				    		<input type="text" wire:model="duracao" class="form-control p-form" placeholder="Ex: 6 meses">
				    	</div>
						<div class="w-100 ms-lg-3 mt-3 mt-lg-0">
							<label>Modalidade</label>
							<select wire:model="modalidade" wire:change="changeEventModalidade($event.target.value)" class="select-form">
								<option value="">Selecione</option>
								<option value="EaD">EaD</option>
								<option value="Presencial">Presencial</option>
								<option value="Semipresencial">Semipresencial</option>
								<option value="Presencial/Online/EaD">Presencial/Online/EaD</option>
							</select>
						</div>

					</div>

					<div id="boxPresencial" style="display:{{$controlBoxPresencial}};">
						<div class="d-lg-flex mb-3" wire:ignore>

							<div class="w-100 me-lg-3 mb-3 mb-lg-0">
								<label>Dias de Aula</label>
								<select wire:model="frequencia" class="selectpicker" data-width="100%" title="Selecione" multiple> 
									<option value="Segunda">Segunda-feira</option>
									<option value="Terça">Terça-feira</option>
									<option value="Quarta">Quarta-feira</option>
									<option value="Quinta">Quinta-feira</option>
									<option value="Sexta">Sexta-feira</option>
									<option value="Sábado">Sábado</option>
									<option value="Domingo">Domingo</option>
								</select>
							</div>
							<div class="w-lg-50">
								<label>Horário</label>
								<input type="text" wire:model="horario" class="horario form-control p-form" placeholder="00:00 - 00:00">
							</div>

					    </div>
					</div>

				</div>

			</div>

		</section>

		<button type="submit" class="btn-add mt-3">Cadastrar</button>	

    </form>

    @include('livewire.util.toast')

</div>