<div>

	<section>

		<div class="section-title rounded-bottom-0 d-flex justify-content-between align-items-center">
			Documento
			<div>
				<a href="#" data-bs-toggle="modal" data-bs-target="#ModalMetaDados" class="btn btn-info fs-14 mb-1 py-1 px-2-3">
					<i class="fa fa-align-left"></i>
				</a>
				<a href="/admin/documento/editar/{{$documento->id}}" class="aclNivel2 btn btn-warning text-dark fs-14 mb-1 py-1 px-2 mx-1">
					<i class="fa fa-edit"></i>
				</a>
				<a href="#" data-bs-toggle="modal" data-bs-target="#ModalDeletarDocumento" class="aclNivel3 btn btn-danger mb-1 fs-15 py-1 px-2">
					<i class="fa fa-trash"></i>
				</a>
			</div>
		</div>

		<div class="bg-white border p-4 d-lg-flex justify-content-between">

			<div class="w-100 mb-2 mb-lg-0">
				<p class="mb-0 weight-600 color-444">Nome</p>
				<p class="mb-0">
					{{ $documento->nome }}
				</p>
			</div>

			<div class="w-100 mb-2 mb-lg-0">
				<p class="mb-0 weight-600 color-444">Referência</p>
				<p class="mb-0">
					{{ $documento->referencia }}
				</p>
			</div>

		</div>
		
		<div class="bg-white border p-4">

			<div class="d-lg-flex justify-content-between">

				<div class="w-100 mb-2 mb-lg-0">
					<p class="mb-0 weight-600 color-444">Publicado</p>
					<div>
						@if($documento->publicar == 'S')
	                     <a href="/" target="_blank" class="hover-d-none">
	                        <div class="bg-primary text-white rounded h-40px d-flex align-items-center justify-content-center w-150px">
	                           <div class="weight-500 fs-13">
	                              <span>
	                                 Sim
	                              </span>
	                           </div>
	                        </div>
	                     </a>
	                     @else
	                        <div class="w-80px bg-secondary text-white px-3 rounded h-40px d-flex align-items-center justify-content-center">
	                           <div class="weight-500 fs-13">
	                              <span>
	                                 Não
	                              </span>
	                           </div>
	                        </div>
	                     @endif
					</div>
				</div>

				<div class="w-100 mb-2 mb-lg-0">
					<p class="mb-0 weight-600 color-444">Data de Publicação</p>
					<p class="mb-0">
						{{ \App\Models\Util::replaceDatePt($documento->data_publicacao) ?? 'Não definido' }}
					</p>
				</div>

				<div class="w-100 mb-2 mb-lg-0">
					<p class="mb-0 weight-600 color-444">Arquivo</p>
					<a href="{{ $documento->link }}" target="_blank" class="color-blue-info-link">
						Visualizar
					</a>
				</div>

			</div>
		</div>

		<div class="bg-white border p-4 d-lg-flex justify-content-between">

			<div class="mb-2 mb-lg-0">
				<p class="mb-0 weight-600 color-444">Categorias</p>
				@if(count($categorias) > 0)
					<ul class="list-group list-group-flush">
				  		@foreach($categorias as $categoria)
				  			<li class="list-group-item">{{ $categoria }}</li>
				  		@endforeach
				  	</ul>
				@else
					<p class="mb-0 font-nunito">Não definido</p>
				@endif
			</div>

		</div>


	</section>

	<div class="modal fade" id="ModalMetaDados" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	  	<div class="modal-dialog modal-dialog-centered" role="document">
	    	<div class="modal-content">
	      		<div class="modal-header">
	        		<h3 class="modal-title">Metadados</h3>
	    			<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
	      		</div>
	      		<div class="modal-body">

					<ul class="list-group list-group-flush">
	  					<li class="list-group-item">
							<span class="mb-0 weight-600 color-444">Cadastrante:</span>
							@if($cadastrante)
								<a href="/admin/colaborador/info/{{$cadastrante->id}}" class="color-blue-info-link">
									{{ $cadastrante->nome }}
								</a>
							@else
								Não definido
							@endif
	  					</li>   
	  					<li class="list-group-item">
							<span class="mb-0 weight-600 color-444">Arquivado:</span>
							@if($documento->arquivado == 'N')
								<span class="text-success weight-700">Não</span>
							@else
								<span class="text-danger weight-700">Sim</span>
							@endif
	  					</li>
	  					<li class="list-group-item">
							<span class="mb-0 weight-600 color-444">Data de Cadastro:</span>
							@if($documento->created_at)
								<span class="mb-0">{{ $documento->created_at->format('d/m/Y H:i') ?? null }} ({{$documento->created_at->diffForHumans()}}) </span>
	  						@else
	  							Não definido
	  						@endif
	  					</li>   
	  				</ul>

	      		</div>
	    	</div>
	  	</div>
	</div>

	<form wire:submit.prevent="delete" method="POST">
		<div wire:ignore class="modal fade" id="ModalDeletarDocumento" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  		<div class="modal-dialog modal-dialog-centered">
	    		<div class="modal-content">
	      			<div class="modal-header">
	        			<h3 class="modal-title" id="exampleModalLabel">Deletar Documento</h3>
	        			<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
	      			</div>
	      			<div class="modal-body text-center py-5">
	        			<p class="m-0 fs-16">
	        				Você tem certeza que deseja deletar este documento?
	        				<br>
	        				Todas as informações serão perdidas definitivamente.
	        			</p>
	      			</div>
	      			<div class="modal-footer">
	      				<div class="d-flex w-100">
		      				<input type="password" wire:model="password_to_delete" class="w-100 form-control p-form me-3" placeholder="Digite sua senha" required>
		        			<button type="submit" class="btn btn-danger weight-600">Confirmar</button>
	    				</div>
	      			</div>
	    		</div>
	  		</div>
		</div>
	</form>

	@include('livewire.util.toast')

</div>

