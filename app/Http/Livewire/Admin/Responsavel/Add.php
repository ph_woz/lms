<?php

namespace App\Http\Livewire\Admin\Responsavel;

use Livewire\Component;
use App\Models\Responsavel;
use App\Models\ResponsavelAluno;
use App\Models\User;
use Illuminate\Support\Facades\Http;

class Add extends Component
{
    public $responsavel;
    public $nome;
    public $email;
    public $password;
    public $status = 0;
    public $cpf;
    public $rg;
    public $naturalidade;
    public $data_nascimento;
    public $celular;
    public $telefone;
    public $genero;
    public $profissao;
    public $obs;

    public $cep;
    public $rua;
    public $numero;
    public $bairro;
    public $cidade;
    public $estado;
    public $complemento;
    public $ponto_referencia;

    public $aluno;
    public $grau_parentesco;


    public function mount($aluno_id)
    {
        $this->aluno = User::plataforma()->aluno()->where('id', $aluno_id)->first();
    }

    public function add()
    {
        if($this->email)
        {
            $checkEmailExists = Responsavel::plataforma()->where('email', $this->email)->exists();

            if($checkEmailExists == true)
            {
                session()->flash('danger-livewire', 'Já existe uma pessoa cadastrada com este Email.');
                $this->dispatchBrowserEvent('toast');
                return;
            }
        }

        if($this->cpf)
        {
            $checkCPFExists = Responsavel::plataforma()->where('cpf', $this->cpf)->exists();

            if($checkCPFExists == true)
            {
                session()->flash('danger-livewire', 'Já existe uma pessoa cadastrada com este CPF.');
                $this->dispatchBrowserEvent('toast');
                return;
            }
        }

        if($this->rg)
        {
            $checkRGExists = Responsavel::plataforma()->where('rg', $this->rg)->exists();

            if($checkRGExists == true)
            {
                session()->flash('danger-livewire', 'Já existe uma pessoa cadastrada com este RG.');
                $this->dispatchBrowserEvent('toast');
                return;
            }
        }

        $this->responsavel = Responsavel::create([
            'nome'               => $this->nome,
            'email'              => $this->email,
            'status'             => $this->status,
            'cpf'                => $this->cpf,
            'rg'                 => $this->rg,
            'naturalidade'       => $this->naturalidade,
            'data_nascimento'    => $this->data_nascimento,
            'celular'            => $this->celular,
            'telefone'           => $this->telefone,
            'genero'             => $this->genero,
            'profissao'          => $this->profissao,
            'obs'                => $this->obs,
            'cep'                => $this->cep,
            'rua'                => $this->rua,
            'numero'             => $this->numero,
            'bairro'             => $this->bairro,
            'cidade'             => $this->cidade,
            'estado'             => $this->estado,
            'complemento'        => $this->complemento,
            'ponto_referencia'   => $this->ponto_referencia,
        ]);

        if($this->aluno)
        {
            ResponsavelAluno::create([
                'responsavel_id'   => $this->responsavel->id,
                'aluno_id'         => $this->aluno->id,
                'grau_parentesco'  => $this->grau_parentesco,
            ]);
        }

        session()->flash('success', 'Responsável cadastrado com êxito.');
        
        return redirect(route('admin.aluno.info', ['id' => $this->aluno->id]));
    }

    public function setEndereco()
    {
        // Retorna endereço do ViaCEP.
        $response = Http::get("http://viacep.com.br/ws/{$this->cep}/json/");
        $viacep = $response->json();
        $this->rua    = $viacep['logradouro'] ?? null;
        $this->bairro = $viacep['bairro'] ?? null;
        $this->cidade = $viacep['localidade'] ?? null;
        $this->estado = $viacep['uf'] ?? null;
    }

    public function render()
    {
        return view('livewire.admin.responsavel.add');
    }
}
