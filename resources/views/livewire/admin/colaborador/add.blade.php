<div>

    <div class="col-12">
        @if (session()->has('success-livewire'))
            <div class="alert alert-success alert-dismissible" role="alert">
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                <div class="alert-message">
                    Colaborador criado:
                    <span>
	                    <a href="{{ route('admin.colaborador.info', ['id' => $this->user->id ?? null]) }}" class="weight-600">
	                    	{{ $user->nome }} 
	                	</a>
	                </span>
                </div>
            </div>
        @endif
    </div>

    <form wire:submit.prevent="add" method="POST" class="mb-4">

		<section class="mb-4-5">
			
			<a href="#boxInfoPrincipal" data-bs-toggle="collapse" aria-expanded="false" class="hover-d-none">
				<div class="section-title">
					Informações Principais
				</div>
			</a>
			<hr/>
			
			<div class="collapse show" id="boxInfoPrincipal">

		    	<div class="d-lg-flex mb-3">

			    	<div class="w-100">
			    		<label>Nome</label>
			    		<input type="text" wire:model="nome" class="form-control p-form" placeholder="Nome" required>
			    	</div>
			    	<div class="w-100 mx-lg-3 my-3 my-lg-0">
			    		<label>Email</label>
			    		<input type="email" wire:model="email" class="form-control p-form" placeholder="Email" required>
			    		@error('email')
			    			<span class="text-danger weight-600">
			    				{{ $message }}
			    			</span>
			    		@enderror
			    	</div>
			    	<div class="w-lg-45">
			    		<label>Senha</label>
			    		<input type="text" wire:model="password" class="form-control p-form" placeholder="Senha" minlength="3" required>
			    	</div>
			    	<div class="w-lg-45 ms-lg-3 mt-3 mt-lg-0">
			    		<label>Status</label>
			    		<select wire:model="status" class="select-form">
			    			<option value="0">Ativo</option>
			    			<option value="1">Desativado</option>
			    		</select>
			    	</div>

			    </div>

			    <div class="d-lg-flex mb-3">

					<div class="w-100">
						<label>CPF</label>
						<input type="text" wire:model="cpf" class="cpf form-control p-form" placeholder="000.000.000-00" minlength="14" maxlength="14">
					</div>
					<div class="w-100 mx-lg-3 my-3 my-lg-0">
						<label>Data de Nascimento</label>
						<input type="date" wire:model="data_nascimento" class="form-control p-form">
					</div>
					<div class="w-100">
						<label>Celular</label>
						<input type="text" wire:model="celular" class="phone form-control p-form" placeholder="(00) 00000-0000">
					</div>
					<div class="w-100 ms-lg-3 mt-3 mt-lg-0">
						<label>Telefone</label>
						<input type="text" wire:model="telefone" class="phone form-control p-form" placeholder="(00) 0000-0000">
					</div>

			    </div>

			    <div class="d-lg-flex mb-3">

					<div class="w-100">
						<label>RG</label>
						<input type="text" wire:model="rg" class="form-control p-form" placeholder="Registro Geral">
					</div>

					<div class="w-100 ms-lg-3 mt-3 mt-lg-0">
						<label>Naturalidade</label>
						<input type="text" wire:model="naturalidade" class="form-control p-form" placeholder="Cidade - Estado">
					</div>

					<div class="w-100 ms-lg-3 mt-3 mt-lg-0">
						<label>Gênero</label>
						<select wire:model="genero" class="select-form">
							<option value="">Selecione</option>
							<option value="M">Masculino</option>
							<option value="F">Feminino</option>
						</select>
					</div>

			    	<div class="w-100 ms-lg-3 mt-lg-0">
			    		<label>Profissão</label>
			    		<input type="text" wire:model="profissao" class="form-control p-form" placeholder="Profissão">
			    	</div>

			    </div>

				<div class="mb-3">
					<label>Descrição</label>
					<textarea wire:model="descricao" class="textarea-form" placeholder="Descrição"></textarea>
				</div>
				
				<div class="mb-3">
					<label>Observações</label>
					<textarea wire:model="obs" class="textarea-form" placeholder="Observações"></textarea>
				</div>

				@if(\App\Models\Unidade::checkActive() === true)
				<div class="mb-3 boxUnidadeRemove">
					<div class="d-lg-flex">
						<div class="w-100 me-lg-3 mb-3 mb-lg-0" wire:ignore>
							<label>Unidade</label>
							<select wire:model="unidade_id" id="unidade_id" class="selectpicker" data-width="100%">
								<option value="">Selecione</option>
								@foreach($unidades as $unidade)
									<option value="{{ $unidade->id }}">
										{{ $unidade->nome }}
									</option>
								@endforeach
							</select>
						</div>
						<div class="w-lg-75">
							<label>Restringir todos os dados pela unidade</label>
							<select wire:model="restringir_unidade" class="select-form">
								@if(\Auth::user()->restringir_unidade == 'S')
									<option value="S">Sim</option>
								@else
									<option value="N">Não</option>
									<option value="S">Sim</option>
								@endif
							</select>
						</div>
					</div>
				</div>
				@endif

			</div>

		</section>

		<section class="mb-4-5">
			
			<a href="#boxEndereco" data-bs-toggle="collapse" aria-expanded="false" class="hover-d-none">
				<div class="section-title">
					Endereço
				</div>
			</a>
			<hr/>

			<div class="collapse show" id="boxEndereco">

				<div class="d-lg-flex mb-3">
					<div class="w-lg-50">
						<label>CEP</label>
						<input type="text" wire:model="endereco_cep" wire:change="setEndereco()" class="cep form-control p-form" placeholder="CEP" minlength="8" maxlength="9">
					</div>
					<div class="w-100 mx-lg-3 my-3 my-lg-0">
						<label>Rua</label>
						<input type="text" wire:model="endereco_rua" class="form-control p-form" placeholder="Rua">
					</div>
					<div class="w-lg-30">
						<label>Estado</label>
						<input type="text" wire:model="endereco_estado" class="form-control p-form" placeholder="UF" minlength="2" maxlength="2">
					</div>
					<div class="w-100 mx-lg-3 my-3 my-lg-0">
						<label>Bairro</label>
						<input type="text" wire:model="endereco_bairro" class="form-control p-form" placeholder="Bairro">
					</div>
					<div class="w-100">
						<label>Cidade</label>
						<input type="text" wire:model="endereco_cidade" class="form-control p-form" placeholder="Cidade">
					</div>
				</div>
	
				<div class="d-lg-flex">
					<div class="w-lg-30">
						<label>Nº</label>
						<input type="text" wire:model="endereco_numero" class="form-control p-form" placeholder="Nº">
					</div>
					<div class="w-lg-50 mx-lg-3 my-3 my-lg-0">
						<label>Complemento</label>
						<input type="text" wire:model="endereco_complemento" class="form-control p-form" placeholder="Complemento">
					</div>
					<div class="w-lg-80">
						<label>Ponto de Referência</label>
						<input type="text" wire:model="endereco_ponto_referencia" class="form-control p-form" placeholder="Ponto de Referência">
					</div>
				</div>

			</div>

		</section>

		<section class="mb-4-5">
			
			<a href="#boxPermissoes" data-bs-toggle="collapse" aria-expanded="false" class="hover-d-none">
				<div class="section-title">
					Permissões de Acesso
				</div>
			</a>
			<hr/>
			
			<div class="collapse show" id="boxPermissoes">

				@foreach($modulos as $modulo)
				
				@php
					$modulo = (array)$modulo;
				@endphp

				<div class="checkbox">

					<label class="mr-3 fs-16 d-block mb-1">
						{{ $modulo['texto'] }}
					</label>
					<label for="{{$modulo['url']}}-0" class="d-block d-md-inline fs-16">
						<input type="radio" id="{{$modulo['url']}}-0" wire:model="acl.{{$modulo['id']}}" class="acessoNivel nivel0" value="0">
						Sem acesso
					</label>
					<label for="{{$modulo['url']}}-1" class="d-block d-md-inline fs-16 mx-lg-4">
						<input type="radio" id="{{$modulo['url']}}-1" wire:model="acl.{{$modulo['id']}}" class="acessoNivel nivel1" value="1">
						Apenas Visualizar
					</label>
					<label for="{{$modulo['url']}}-2" class="d-block d-md-inline fs-16">
						<input type="radio" id="{{$modulo['url']}}-2" wire:model="acl.{{$modulo['id']}}" class="acessoNivel nivel2" value="2" @if(\App\Models\AclPlataformaModuloUser::getAclNivel($modulo['url']) < 2) disabled @endif>
						Visualizar, Cadastrar e Editar
					</label>

					<label for="{{$modulo['url']}}-3" class="d-block d-md-inline fs-16 ms-lg-4">
						<input type="radio" id="{{$modulo['url']}}-3" wire:model="acl.{{$modulo['id']}}" class="acessoNivel nivel3" value="3" @if(\App\Models\AclPlataformaModuloUser::getAclNivel($modulo['url']) < 3) disabled @endif>
						Visualizar, Cadastrar, Editar e Excluir
					</label>

				</div>

				<hr/>

				@endforeach

			</div>

		</section>

		<section class="mb-4-5" wire:ignore>
			
			<a href="#boxCategorias" data-bs-toggle="collapse" aria-expanded="false" class="hover-d-none">
				<div class="section-title">
					Categorias 
				</div>
			</a>
			<hr/>

			<div class="collapse" id="boxCategorias">
				<select wire:model="categoriasSelected" class="selectpicker" multiple data-width="100%" data-live-search="true" title="Selecione">
					@foreach($categorias as $categoria)
						<option value="{{ $categoria->id }}">{{ $categoria->nome }}</option>
					@endforeach
				</select>
			</div>

		</section>

		<button type="submit" class="btn-add mt-3">Cadastrar</button>	

    </form>

    @include('livewire.util.toast')
    
</div>