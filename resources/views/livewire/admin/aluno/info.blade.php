<div>

	<div class="d-flex justify-content-end align-items-center mb-3">
    
		<a href="{{ route('admin.aluno.editar', ['id' => $user->id]) }}" class="aclNivel2 btn btn-warning text-dark">
			<i class="fa fa-edit"></i>
		</a>

		<a href="#" data-bs-toggle="modal" data-bs-target="#ModalDeletarAluno" class="aclNivel3 btn btn-danger ms-3">
			<i class="fa fa-trash"></i>
		</a>

	</div>

	<section>

		<button type="button" wire:click="getEntradas()" class="color-555 hover-d-none bg-white px-3 border py-2 fs-14 rounded-top rounded-bottom-0" data-bs-toggle="modal" data-bs-target="#ModalFinanceiroEntradas">
			+ Financeiro
		</button>

		<button type="button" wire:click="getDocumentos()" class="color-555 hover-d-none bg-white px-3 border py-2 fs-14 rounded-top rounded-bottom-0" data-bs-toggle="modal" data-bs-target="#ModalDocumentos">
			+ Documentos
		</button>

		<button type="button" wire:click="getContratos()" class="color-555 hover-d-none bg-white px-3 border py-2 fs-14 rounded-top rounded-bottom-0" data-bs-toggle="modal" data-bs-target="#ModalContratos">
			+ Contratos
		</button>

		<button type="button" wire:click="getResponsaveis()" class="color-555 hover-d-none bg-white px-3 border py-2 fs-14 rounded-top rounded-bottom-0" data-bs-toggle="modal" data-bs-target="#ModalResponsaveis">
			+ Reponsáveis
		</button>

		<button type="button" wire:click="getComunicados()" class="color-555 hover-d-none bg-white px-3 border py-2 fs-14 rounded-top rounded-bottom-0" data-bs-toggle="modal" data-bs-target="#ModalComunicados">
			+ Comunicados
		</button>

		<button type="button" onclick="window.open('/admin/aluno/info/{{$user->id}}/historico-escolar')" class="color-555 hover-d-none bg-white px-3 border py-2 fs-14 rounded-top rounded-bottom-0">
			+ Histórico Escolar
		</button>

		<button type="button" class="color-555 hover-d-none bg-white px-3 border py-2 fs-14 rounded-top rounded-bottom-0" data-bs-toggle="modal" data-bs-target="#ModalEntrarNaContaDoAluno">
			+ Entrar na conta do Aluno
		</button>

	</section>

	<section class="mb-4-5 bg-white shadow p-4 border">

		<div class="row">

			<div class="col-lg-6 text-center">


				<div class="d-flex mb-2 mr-2 text-white">
					@if($user->status == 0)
						<span class="bg-success px-3 py-1 rounded">Ativo</span>
					@else
						<span class="bg-danger px-3 py-1 rounded">Desativado</span>
					@endif
				</div>

				@if($user->foto_perfil)
					<img src="{{ $user->foto_perfil }}" class="square-125 rounded-circle object-fit-contain border-profile cursor-pointer" onclick="setFotoPerfil()">
				@else
					<i class="fa fa-user-circle fs-md-75 fs-xs-50 cursor-pointer" onclick="setFotoPerfil()"></i>
				@endif

				<h1 class="fs-19 mt-3 color-333">{{ $user->nome }}</h1>

				@if($user->profissao)
					<h2 class="fs-15 mt-0 color-666 font-roboto" title="Profissão">{{ $user->profissao }}</h2>
				@endif

				<hr class="mb-4"/>

				<p class="fs-17 color-555" title="Email">{{ $user->email }}</p>

				@if($user->cpf)
					<p class="fs-17 color-555" title="CPF">{{ $user->cpf }}</p>
				@endif

				@if($user->obs)
					<p class="mb-0 text-start color-333 mt-4">Observações</p>
					<p class="mb-0 text-start color-555">{!! nl2br($user->obs) !!}</p>
				@endif

			</div>

			<div class="col-lg-6">

				<div class="ps-lg-5">

					<p class="mb-1 fs-14">
						<span>Matrícula:</span> 
						<span class="color-555">{{ $user->id }}</span>
					</p>

					@if(isset($unidade))
					<p class="mb-1 fs-14">
						<span>Unidade:</span> 
						<a href="{{ route('admin.unidade.info', ['id' => $unidade->id]) }}" class="color-blue-info-link">{{ $unidade->nome }}</a>
					</p>
					@endif

					@if($user->rg)
					<p class="mb-1 fs-14">
						<span>RG:</span> 
						<span class="color-555">{{ $user->rg ?? 'Não definido' }}</span>
					</p>
					@endif

					@if($user->naturalidade)
					<p class="mb-1 fs-14">
						<span>Naturalidade:</span> 
						<span class="color-555">{{ $user->naturalidade ?? 'Não definido' }}</span>
					</p>
					@endif

					<p class="mb-1 fs-14">
						<span>Data de Nascimento:</span> 
						<span class="color-555">
							@if($user->data_nascimento)
								{{ \App\Models\Util::replaceDatePt($user->data_nascimento) }} ({{ \App\Models\Util::getIdade($user->data_nascimento) }} anos)
							@else
								Não definido
							@endif
						</span>
					</p>

					@if($user->genero)
					<p class="mb-1 fs-14">
						<span>Gênero:</span> 
						<span class="color-555">
							@if($user->genero == 'M')
								Masculino
							@else
								Feminino
							@endif
						</span>
					</p>
					@endif

					<p class="mb-1 fs-14">
						<span>Celular:</span> 
						<a href="tel:+55{{preg_replace('/\D/', '', $user->celular)}}" class="color-555">{{ $user->celular ?? 'Não definido' }}</a>
					</p>

					@if($user->telefone)
					<p class="mb-1 fs-14">
						<span>Telefone:</span> 
						<a href="tel:+55{{preg_replace('/\D/', '', $user->telefone)}}" class="color-555">{{ $user->telefone ?? 'Não definido' }}</a>
					</p>
					@endif

					<hr class="hr-1">

					@if(isset($endereco['cidade']))

						@if($endereco['cep'])
						<p class="mb-1 fs-14">
							<span>CEP:</span>
							<span class="color-555">{{ $endereco['cep'] }}</span>
						</p>
						@endif
						@if($endereco['rua'])
						<p class="mb-1 fs-14">
							<span>Rua:</span>
							<span class="color-555">
								{{ $endereco['rua'] }}@if($endereco['numero']), {{ $endereco['numero'] }}@endif
							</span>
						</p>
						@endif
						@if($endereco['bairro'])
						<p class="mb-1 fs-14">
							<span>Bairro:</span>
							<span class="color-555">{{ $endereco['bairro'] }}</span>
						</p>
						@endif
						@if($endereco['cidade'])
						<p class="mb-1 fs-14">
							<span>Cidade:</span>
							<span class="color-555">
								{{ $endereco['cidade'] }}
							</span>
						</p>
						@endif
						@if($endereco['estado'])
						<p class="mb-1 fs-14">
							<span>Estado:</span>
							<span class="color-555">{{ $endereco['estado'] }}</span>
						</p>
						@endif
						@if($endereco['complemento'])
						<p class="mb-1 fs-14"> 
							<span>Complemento:</span>
							<span class="color-555">
								{{$endereco['complemento']}}
							</span>
						</p>
						@endif
						@if($endereco['ponto_referencia'])
						<p class="mb-1 fs-14"> 
							<span>Ponto de Referência:</span>
							<span class="color-555">
								{{$endereco['ponto_referencia']}}
							</span>
						</p>
						@endif

						<hr class="hr-2">

					@endif

					@if(count($categorias) > 0)
						@foreach($categorias as $categoria)
							<p class="mb-2 fs-13">
								<i class="fas fa-plus-circle color-777 fs-13" title="Categoria"></i>
								{{ $categoria->nome }}
							</p>
						@endforeach
						<hr class="hr-3" />
					@endif


					<p class="mb-1 fs-14 ls-05">
						<span>Último Acesso:</span>
						<span class="color-555">
					      	@if($user->data_ultimo_acesso)
					      		{{ \App\Models\Util::replaceDateTimePt($user->data_ultimo_acesso) }}
					      		({{ \Carbon\Carbon::createFromTimeStamp(strtotime($user->data_ultimo_acesso))->diffForHumans() }})
					      	@else
								Ainda não acessou
					      	@endif
						</span>
					</p>

					<p class="mb-1 fs-14 ls-05">
						<span>Data de Cadastro:</span> 
						<span class="color-555">{{ $user->created_at->format('d/m/Y H:i') }}</span>
					</p>

					@if($cadastrante)
					<p class="mb-1 fs-14 ls-05">
						<span>Cadastrante:</span> 
						<a href="/admin/colaborador/info/{{$cadastrante->id}}" class="color-555">
							{{ $cadastrante->nome }}
						</a>
					</p>
					@endif
					
				</div>

			</div>

		</div>

	</section>

	<section class="mb-4-5">
	
		<div class="section-title rounded-bottom-0 fs-17 d-flex justify-content-between align-items-center">
			<a href="#boxTrilha" data-bs-toggle="collapse" aria-expanded="false" class="hover-d-none text-white">
				Trilhas ({{ count($trilhasMatriculado) }})
			</a>
			<button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#ModalMatricularAlunoNaTrilha">
				+ Matricular aluno em uma Trilha
			</button>
		</div>

		<div class="collapse show bg-white p-4 border" id="boxTrilha">

			@foreach($trilhasMatriculado as $trilhaMatriculado)

				<article class="mb-4 shadow border p-4 rounded-1">

					<div class="d-flex align-items-end justify-content-between">

						<h1 class="m-0 p-0 fs-15 weight-600">
							<a href="{{ route('admin.trilha.info', ['id' => $trilhaMatriculado->id]) }}" @if($trilhaMatriculado->status == 1) class="text-danger" @endif @if($trilhaMatriculado->data_conclusao != null) class="text-success" @endif> 
								{{ $trilhaMatriculado->referencia }}
							</a>
						</h1>

						<div class="d-flex">

							@if($trilhaMatriculado->data_conclusao == null)
								<div class="me-lg-2" data-bs-toggle="tooltip" data-bs-placement="top" data-html="true" title="Forçar conclusão desta trilha">
									<button type="button" class="btn btn-primary" wire:click="setForcarConclusaoDaTrilhaId({{$trilhaMatriculado->id}}, {{$trilhaMatriculado->id}})" data-bs-toggle="modal" data-bs-target="#ModalForcarConclusaoDaTrilha"
										onclick="document.getElementById('forcar_trilha_nome').innerHTML='{{ $trilhaMatriculado->referencia }}';"
									>
										<i class="fa fa-check"></i>
									</button>
								</div>
							@else
								@if($trilhaMatriculado->certificado_id)
									<div class="me-lg-2" data-bs-toggle="tooltip" data-bs-placement="top" data-html="true" title="Gerar Certificado">
										<a href="/admin/certificado/info/{{$trilhaMatriculado->certificado_id}}?gerar_certificado_aluno_id={{$user->id}}&trilha_id={{$trilhaMatriculado->id}}" target="_blank" class="btn btn-primary">
											<i class="fa fa-certificate"></i>
										</a>
									</div>
								@endif
							@endif

							@if($trilhaMatriculado->status == 1)
								<div class="me-lg-2" data-bs-toggle="tooltip" data-bs-placement="top" data-html="true" title="Ativar acesso desta trilha para o aluno">
									<button type="button" class="btn btn-primary" wire:click="setAtivarAcessoDaTrilhaId({{$trilhaMatriculado->id}})" data-bs-toggle="modal" data-bs-target="#ModalAtivarAcessoDaTrilha"
										onclick="document.getElementById('ativar_acesso_trilha_nome').innerHTML='{{ $trilhaMatriculado->referencia }}';"
									>
										<i class="fas fa-unlock"></i>
									</button>
								</div>
							@endif
							@if($trilhaMatriculado->status == 0)
								<div class="me-lg-2" data-bs-toggle="tooltip" data-bs-placement="top" data-html="true" title="Desativar acesso desta trilha para o aluno">
									<button type="button" class="btn btn-danger" wire:click="setDesativarAcessoDaTrilhaId({{$trilhaMatriculado->id}})" data-bs-toggle="modal" data-bs-target="#ModalDesativarAcessoDaTrilha"
										onclick="document.getElementById('desativar_acesso_trilha_nome').innerHTML='{{ $trilhaMatriculado->referencia }}';"
									>
										<i class="fas fa-lock"></i>
									</button>
								</div>
							@endif

							<div data-bs-toggle="tooltip" data-bs-placement="top" data-html="true" title="Remover aluno desta trilha">
								<button type="button" class="btn btn-danger" wire:click="setRemoverAlunoDaTrilhaId({{$trilhaMatriculado->id}})" data-bs-toggle="modal" data-bs-target="#ModalRemoverAlunoDaTrilha"
									onclick="document.getElementById('remover_trilha_nome').innerHTML='{{ $trilhaMatriculado->referencia }}';"
								>
									<i class="fas fa-times-circle"></i>
								</button>
							</div>

						</div>

					</div>

					<div class="row">

						<div class="col-lg-6 mt-1">
							<p class="mb-1"> 
								Data de Conclusão: {{ \App\Models\Util::replaceDateTimePt($trilhaMatriculado->data_conclusao) ?? 'Ainda não concluiu' }}
							</p>
						</div>

					</div>

				</article>

			@endforeach

		</div>

	</section>

	<section class="mb-4-5">
		
	
		<div class="section-title rounded-bottom-0 fs-17 d-flex justify-content-between align-items-center">
			<a href="#boxTurma" data-bs-toggle="collapse" aria-expanded="false" class="hover-d-none text-white">
				Turmas ({{ count($turmasMatriculado) }})
			</a>
			<button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#ModalMatricularAlunoNaTurma">
				+ Matricular aluno em uma Turma
			</button>
		</div>

		<div class="collapse show bg-white p-4 border" id="boxTurma">

			@foreach($turmasMatriculado as $turmaMatriculado)

				<article class="mb-4 shadow border p-4 rounded-1">

					<div class="d-flex justify-content-between">

						<h1 class="m-0 p-0 fs-15 weight-600">
							<a href="{{ route('admin.turma.info', ['id' => $turmaMatriculado->id]) }}" @if($turmaMatriculado->status == 1) class="text-danger" @endif @if($turmaMatriculado->data_conclusao_curso != null) class="text-success" @endif> 
								{{ $turmaMatriculado->nome }}
							</a>
						</h1>

						<div class="d-flex">

							@if($turmaMatriculado->data_conclusao_curso == null)
								<div class="me-lg-2" data-bs-toggle="tooltip" data-bs-placement="top" data-html="true" title="Forçar conclusão desta turma">
									<button type="button" class="btn btn-primary" wire:click="setForcarConclusaoDaTurmaId({{$turmaMatriculado->id}},{{$turmaMatriculado->turma_aluno_id}})" data-bs-toggle="modal" data-bs-target="#ModalForcarConclusaoDaTurma"
										onclick="document.getElementById('forcar_turma_nome').innerHTML='{{ $turmaMatriculado->nome }}';"
									>
										<i class="fa fa-check"></i>
									</button>
								</div>
							@else
								@php
									$certificado_id = null;
									$certificado_id = \App\Models\Curso::select('certificado_id')->plataforma()->where('id', $turmaMatriculado->curso_id)->value('certificado_id');
								@endphp
								@if($certificado_id)
									<div class="me-lg-2" data-bs-toggle="tooltip" data-bs-placement="top" data-html="true" title="Gerar Certificado">
										<a href="/admin/certificado/info/{{$certificado_id}}?gerar_certificado_aluno_id={{$user->id}}&turma_id={{$turmaMatriculado->id}}" target="_blank" class="btn btn-primary">
											<i class="fa fa-certificate"></i>
										</a>
									</div>
								@endif
							@endif

							@if($turmaMatriculado->status == 1)
								<div class="me-lg-2" data-bs-toggle="tooltip" data-bs-placement="top" data-html="true" title="Ativar acesso desta turma para o aluno">
									<button type="button" class="btn btn-primary" wire:click="setAtivarAcessoDaTurmaId({{$turmaMatriculado->id}})" data-bs-toggle="modal" data-bs-target="#ModalAtivarAcessoDaTurma"
										onclick="document.getElementById('ativar_acesso_turma_nome').innerHTML='{{ $turmaMatriculado->nome }}';"
									>
										<i class="fas fa-unlock"></i>
									</button>
								</div>
							@endif
							@if($turmaMatriculado->status == 0)
								<div class="me-lg-2" data-bs-toggle="tooltip" data-bs-placement="top" data-html="true" title="Desativar acesso desta turma para o aluno">
									<button type="button" class="btn btn-danger" wire:click="setDesativarAcessoDaTurmaId({{$turmaMatriculado->id}})" data-bs-toggle="modal" data-bs-target="#ModalDesativarAcessoDaTurma"
										onclick="document.getElementById('desativar_acesso_turma_nome').innerHTML='{{ $turmaMatriculado->nome }}';"
									>
										<i class="fas fa-lock"></i>
									</button>
								</div>
							@endif

							<div data-bs-toggle="tooltip" data-bs-placement="top" data-html="true" title="Remover aluno desta turma">
								<button type="button" wire:click="setRemoverAlunoDaTurmaId({{$turmaMatriculado->id}})" class="btn btn-danger" data-bs-toggle="modal" data-bs-target="#ModalRemoverAlunoDaTurma"
									onclick="document.getElementById('remover_turma_nome').innerHTML='{{ $turmaMatriculado->nome }}';"
								>
									<i class="fas fa-times-circle"></i>
								</button>
							</div>

						</div>

					</div>

					<div class="row">

						<div class="col-lg-6 mt-1">
							<p class="mb-1">
								Data de Conclusão: {{ \App\Models\Util::replaceDateTimePt($turmaMatriculado->data_conclusao_curso) ?? 'Ainda não concluiu' }}
							</p>
						</div>

						<div class="col-lg-6">
							<p class="mb-1">
								@php
								    $notas = \App\Models\AvaliacaoAluno::select('nota')->plataforma()->finalizada()->where('aluno_id', $user->id)->where('turma_id', $turmaMatriculado->id)->get()->pluck('nota')->toArray();

							        $valoresParaSoma = [];
							        foreach($notas as $nota)
							        {
							            $nota = floatval(str_replace(',','.', $nota));
							            $valoresParaSoma[] = $nota;
							        }
							        
							        $nota = array_sum($valoresParaSoma);

								@endphp

								@if(count($notas) > 0)
									<a href="#ModalAvaliacaoAlunoAcoes" wire:click="getAvaliacoesByTurmaId({{$turmaMatriculado->id}})" data-bs-toggle="modal" class="fs-14 weight-600 hover-d-none">
										<span class="weight-600 @if(isset($turmaMatriculado->nota_minima_aprovacao)) @if($nota >= $turmaMatriculado->nota_minima_aprovacao) text-success @else text-danger @endif @endif">
											Nota: {{ number_format($nota, 2, ',', '.') }}
										</span>
									</a>
								@else
									<a href="#" data-bs-toggle="modal" class="fs-14 weight-600 hover-d-none">
										<span class="weight-600 color-666">
											Não fez
										</span>
									</a>
								@endif

							</p>
						</div>

					</div>

				</article>

			@endforeach

		</div>

	</section>

	<form action="{{ route('admin.aluno.matricular-aluno-na-trilha', ['id' => $this->user->id]) }}" method="POST">
	@csrf
		<div wire:ignore.self class="modal fade" id="ModalMatricularAlunoNaTrilha" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  		<div class="modal-dialog modal-lg modal-dialog-centered">
	    		<div class="modal-content">
	      			<div class="modal-header">
	        			<h3 class="modal-title" id="exampleModalLabel">Matricular Aluno em uma Trilha</h3>
	        			<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
	      			</div>
	      			<div class="modal-body px-lg-4">

				        <div class="d-lg-flex mb-4">

				            <div class="w-100 me-lg-3 mb-3 mb-lg-0" wire:ignore>
				            	<label>Trilha</label>
				                <select wire:model="matricular_aluno_trilha_id" name="matricular_aluno_trilha_id" class="selectpicker" data-width="100%" data-live-search="true" title="Selecione" required>
				                    @foreach($trilhas as $trilha)
				                        <option value="{{ $trilha->id }}">{{ $trilha->referencia }}</option>
				                    @endforeach
				                </select>
				            </div>
				            <div class="w-lg-40">
				            	<label>Status</label>
				            	<select wire:model="matricular_aluno_trilha_status" name="matricular_aluno_trilha_status" class="select-form">
				            		<option value="0">Ativo</option>
				            		<option value="1">Desativado</option>
				            	</select>
				            </div>
				        </div>

		      			<div class="mb-1">
		      				<label for="matricular_aluno_trilha_enviar_email">
		      					<input type="checkbox" id="matricular_aluno_trilha_enviar_email" onclick="controlBoxEmailContratacaoTrilha(this)" wire:model="matricular_aluno_trilha_enviar_email" name="matricular_aluno_trilha_enviar_email" class="square-15">
		      					Enviar Email de Boas Vindas
		      				</label>
		      			</div>

						<div wire:ignore id="boxEmailContratacaoTrilha" style="display:none;">
							<textarea wire:model="email_contratacao_trilha" name="email_contratacao_trilha" class="descricao textarea-form"></textarea>
						</div>

	      			</div>

	      			<div class="modal-footer">
		        		<button type="submit" class="btn btn-primary weight-600 py-2">Confirmar</button>
	      			</div>
	    		</div>
	  		</div>
		</div>
	</form>

	<form action="{{ route('admin.aluno.matricular-aluno-na-turma', ['id' => $this->user->id]) }}" method="POST">
	@csrf
		<div wire:ignore.self class="modal fade" id="ModalMatricularAlunoNaTurma" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  		<div class="modal-dialog modal-lg modal-dialog-centered">
	    		<div class="modal-content">
	      			<div class="modal-header">
	        			<h3 class="modal-title" id="exampleModalLabel">Matricular Aluno em uma Turma</h3>
	        			<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
	      			</div>
	      			<div class="modal-body px-lg-4">

				        <div class="d-lg-flex mb-4">

				            <div class="w-100" wire:ignore>
				            	<label>Curso</label>
				                <select wire:model="matricular_aluno_curso_id" name="matricular_aluno_curso_id" class="selectpicker" data-width="100%" data-live-search="true" title="Selecione">                
				                    @foreach($cursos as $curso)
				                        <option value="{{ $curso->id }}">{{ $curso->referencia }}</option>
				                    @endforeach
				                </select>
				            </div>
				            <div class="w-100 mx-lg-3 my-3 my-lg-0">
				            	<label>Turma</label>
				                <select wire:model="matricular_aluno_turma_id" name="matricular_aluno_turma_id" class="select-form">
				                    <option value="">Selecione</option>
				                    @foreach($turmas as $turma)
				                        <option value="{{ $turma->id }}">{{ $turma->nome }}</option>
				                    @endforeach
				                </select>
				            </div>
				            <div class="w-lg-40">
				            	<label>Status</label>
				            	<select wire:model="matricular_aluno_turma_status" name="matricular_aluno_turma_status" class="select-form">
				            		<option value="0">Ativo</option>
				            		<option value="1">Desativado</option>
				            	</select>
				            </div>
				        </div>

		      			<div class="mb-1">
		      				<label for="matricular_aluno_turma_enviar_email">
		      					<input type="checkbox" id="matricular_aluno_turma_enviar_email" onclick="controlBoxEmailContratacaoTurma(this)" wire:model="matricular_aluno_turma_enviar_email" name="matricular_aluno_turma_enviar_email" class="square-15">
		      					Enviar Email de Boas Vindas
		      				</label>
		      			</div>

						<div wire:ignore id="boxEmailContratacaoTurma" style="display:none;">
							<textarea name="email_contratacao_turma" wire:model="email_contratacao_turma" name="email_contratacao_turma" class="descricao textarea-form"></textarea>
						</div>
						
	      			</div>

	      			<div class="modal-footer">
		        		<button type="submit" class="btn btn-primary weight-600 py-2">Confirmar</button>
	      			</div>
	    		</div>
	  		</div>
		</div>
	</form>

	<form wire:submit.prevent="ativarAcessoDaTrilha" method="POST">
		<input type="hidden" wire:model="ativar_acesso_trilha_id" id="ativar_acesso_trilha_id">
		<div wire:ignore class="modal fade" id="ModalAtivarAcessoDaTrilha" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  		<div class="modal-dialog modal-dialog-centered">
	    		<div class="modal-content">
	      			<div class="modal-header">
	        			<h3 class="modal-title" id="exampleModalLabel">Ativar Acesso da Trilha</h3>
	        			<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
	      			</div>
	      			<div class="modal-body text-center py-5 px-4">

	        			<p id="ativar_acesso_trilha_nome" class="m-0 fs-15 weight-600">
	        				
	        			</p>

	        			<hr/>

	        			<p class="m-0 fs-15">
	        				Você tem certeza que deseja ativar o acesso desta trilha para o aluno?
	        			</p>

	      			</div>
	      			<div class="modal-footer">
	      				<div class="d-flex w-100">
		      				<input type="password" wire:model="password_to_delete" class="w-100 form-control p-form me-3" placeholder="Digite sua senha" required>
		        			<button type="submit" class="btn btn-danger weight-600">Confirmar</button>
	    				</div>
	      			</div>
	    		</div>
	  		</div>
		</div>
	</form>

	<form wire:submit.prevent="desativarAcessoDaTrilha" method="POST">
		<input type="hidden" wire:model="desativar_acesso_trilha_id" id="desativar_acesso_trilha_id">
		<div wire:ignore class="modal fade" id="ModalDesativarAcessoDaTrilha" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  		<div class="modal-dialog modal-dialog-centered">
	    		<div class="modal-content">
	      			<div class="modal-header">
	        			<h3 class="modal-title" id="exampleModalLabel">Desativar Acesso da Trilha</h3>
	        			<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
	      			</div>
	      			<div class="modal-body text-center py-5 px-4">

	        			<p id="desativar_acesso_trilha_nome" class="m-0 fs-15 weight-600">
	        				
	        			</p>

	        			<hr/>

	        			<p class="m-0 fs-15">
	        				Você tem certeza que deseja desativar o acesso desta trilha para o aluno?
	        			</p>

	      			</div>
	      			<div class="modal-footer">
	      				<div class="d-flex w-100">
		      				<input type="password" wire:model="password_to_delete" class="w-100 form-control p-form me-3" placeholder="Digite sua senha" required>
		        			<button type="submit" class="btn btn-danger weight-600">Confirmar</button>
	    				</div>
	      			</div>
	    		</div>
	  		</div>
		</div>
	</form>

	<form wire:submit.prevent="ativarAcessoDaTurma" method="POST">
		<input type="hidden" wire:model="ativar_acesso_turma_id" id="ativar_acesso_turma_id">
		<div wire:ignore class="modal fade" id="ModalAtivarAcessoDaTurma" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  		<div class="modal-dialog modal-dialog-centered">
	    		<div class="modal-content">
	      			<div class="modal-header">
	        			<h3 class="modal-title" id="exampleModalLabel">Ativar Acesso da Turma</h3>
	        			<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
	      			</div>
	      			<div class="modal-body text-center py-5 px-4">

	        			<p id="ativar_acesso_turma_nome" class="m-0 fs-15 weight-600">
	        				
	        			</p>

	        			<hr/>

	        			<p class="m-0 fs-15">
	        				Você tem certeza que deseja ativar o acesso desta turma para o aluno?
	        			</p>

	      			</div>
	      			<div class="modal-footer">
	      				<div class="d-flex w-100">
		      				<input type="password" wire:model="password_to_delete" class="w-100 form-control p-form me-3" placeholder="Digite sua senha" required>
		        			<button type="submit" class="btn btn-danger weight-600">Confirmar</button>
	    				</div>
	      			</div>
	    		</div>
	  		</div>
		</div>
	</form>

	<form wire:submit.prevent="desativarAcessoDaTurma" method="POST">
		<input type="hidden" wire:model="desativar_acesso_turma_id" id="desativar_acesso_turma_id">
		<div wire:ignore class="modal fade" id="ModalDesativarAcessoDaTurma" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  		<div class="modal-dialog modal-dialog-centered">
	    		<div class="modal-content">
	      			<div class="modal-header">
	        			<h3 class="modal-title" id="exampleModalLabel">Desativar Acesso da Turma</h3>
	        			<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
	      			</div>
	      			<div class="modal-body text-center py-5 px-4">

	        			<p id="desativar_acesso_turma_nome" class="m-0 fs-15 weight-600">
	        				
	        			</p>

	        			<hr/>

	        			<p class="m-0 fs-15">
	        				Você tem certeza que deseja desativar o acesso desta turma para o aluno?
	        			</p>

	      			</div>
	      			<div class="modal-footer">
	      				<div class="d-flex w-100">
		      				<input type="password" wire:model="password_to_delete" class="w-100 form-control p-form me-3" placeholder="Digite sua senha" required>
		        			<button type="submit" class="btn btn-danger weight-600">Confirmar</button>
	    				</div>
	      			</div>
	    		</div>
	  		</div>
		</div>
	</form>

	<form wire:submit.prevent="delete" method="POST">
		<div wire:ignore class="modal fade" id="ModalDeletarAluno" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  		<div class="modal-dialog modal-dialog-centered">
	    		<div class="modal-content">
	      			<div class="modal-header">
	        			<h3 class="modal-title" id="exampleModalLabel">Deletar Aluno</h3>
	        			<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
	      			</div>
	      			<div class="modal-body text-center py-5">
	        			<p class="m-0 fs-15">
	        				Você tem certeza que deseja deletar este aluno?
	        				<br>
	        				Todas as informações serão perdidas definitivamente.
	        			</p>
	      			</div>
	      			<div class="modal-footer">
	      				<div class="d-flex w-100">
		      				<input type="password" wire:model="password_to_delete" class="w-100 form-control p-form me-3" placeholder="Digite sua senha" required>
		        			<button type="submit" class="btn btn-danger weight-600">Confirmar</button>
	    				</div>
	      			</div>
	    		</div>
	  		</div>
		</div>
	</form>

	<form wire:submit.prevent="forcarConclusaoDaTrilha" method="POST">
		<div wire:ignore class="modal fade" id="ModalForcarConclusaoDaTrilha" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  		<div class="modal-dialog modal-dialog-centered">
	    		<div class="modal-content">
	      			<div class="modal-header">
	        			<h3 class="modal-title" id="exampleModalLabel">Forçar Conclusão da Trilha</h3>
	        			<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
	      			</div>
	      			<div class="modal-body">

	        			<p id="forcar_trilha_nome" class="m-0 text-center fs-15 weight-600">
	        				
	        			</p>

	        			<hr/>

	        			<div class="mb-3 d-lg-flex">

		        			<div class="w-100 me-lg-3 mb-3 mb-lg-0">
		        				<label>Número de Registro</label>
			        			<input type="text" wire:model="forcar_conclusao_codigo_certificado" placeholder="Código do Certificado" class="form-control p-form">
			        		</div>

		        			<div class="w-100 me-lg-3 mb-3 mb-lg-0">
		        				<label>Data de Conclusão</label>
			        			<input type="date" wire:model="forcar_conclusao_data_conclusao" class="form-control p-form">
			        		</div>

		        			<div class="w-100">
		        				<label>Data de Colação de Grau</label>
			        			<input type="date" wire:model="forcar_conclusao_data_colacao_grau" class="form-control p-form">
			        		</div>

			        	</div>

	      			</div>
	      			<div class="modal-footer">
	      				<div class="d-flex w-100">
		      				<input type="password" wire:model="password_to_delete" class="w-100 form-control p-form me-3" placeholder="Digite sua senha" required>
		        			<button type="submit" class="btn btn-primary weight-600">Confirmar</button>
	    				</div>
	      			</div>
	    		</div>
	  		</div>
		</div>
	</form>

	<form wire:submit.prevent="forcarConclusaoDaTurma" method="POST">
		<div wire:ignore class="modal fade" id="ModalForcarConclusaoDaTurma" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  		<div class="modal-dialog modal-dialog-centered">
	    		<div class="modal-content">
	      			<div class="modal-header">
	        			<h3 class="modal-title" id="exampleModalLabel">Forçar Conclusão da Turma</h3>
	        			<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
	      			</div>
	      			<div class="modal-body">

	        			<p id="forcar_turma_nome" class="m-0 fs-15 weight-600">
	        				
	        			</p>

	        			<hr/>

	        			<div class="mb-3 d-lg-flex">

		        			<div class="w-100 me-lg-3 mb-3 mb-lg-0">
		        				<label>Número de Registro</label>
			        			<input type="text" wire:model="forcar_conclusao_codigo_certificado" placeholder="Código do Certificado" class="form-control p-form">
			        		</div>
			        		
		        			<div class="w-100 me-lg-3 mb-3 mb-lg-0">
		        				<label>Data de Conclusão</label>
			        			<input type="date" wire:model="forcar_conclusao_data_conclusao" class="form-control p-form">
			        		</div>

		        			<div class="w-100">
		        				<label>Data de Colação de Grau</label>
			        			<input type="date" wire:model="forcar_conclusao_data_colacao_grau" class="form-control p-form">
			        		</div>

			        	</div>

	      			</div>
	      			<div class="modal-footer">
	      				<div class="d-flex w-100">
		      				<input type="password" wire:model="password_to_delete" class="w-100 form-control p-form me-3" placeholder="Digite sua senha" required>
		        			<button type="submit" class="btn btn-primary weight-600">Confirmar</button>
	    				</div>
	      			</div>
	    		</div>
	  		</div>
		</div>
	</form>

	<form wire:submit.prevent="removerAlunoDaTrilha" method="POST">
		<input type="hidden" wire:model="remover_aluno_trilha_id" id="remover_aluno_trilha_id">
		<div wire:ignore class="modal fade" id="ModalRemoverAlunoDaTrilha" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  		<div class="modal-dialog modal-dialog-centered">
	    		<div class="modal-content">
	      			<div class="modal-header">
	        			<h3 class="modal-title" id="exampleModalLabel">Remover Aluno da Trilha</h3>
	        			<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
	      			</div>
	      			<div class="modal-body text-center py-5 px-4">

	        			<p id="remover_trilha_nome" class="m-0 fs-15 weight-600">
	        				
	        			</p>

	        			<hr/>

	        			<p class="m-0 fs-15">
	        				Você tem certeza que deseja remover este aluno desta trilha?<br>
	        				Todos os dados, incluindo turmas vinculadas a trilha, também progresso dos cursos e avaliações serão permanentemente deletados. 
	        			</p>

	      			</div>
	      			<div class="modal-footer">
	      				<div class="d-flex w-100">
		      				<input type="password" wire:model="password_to_delete" class="w-100 form-control p-form me-3" placeholder="Digite sua senha" required>
		        			<button type="submit" class="btn btn-danger weight-600">Confirmar</button>
	    				</div>
	      			</div>
	    		</div>
	  		</div>
		</div>
	</form>

	<form wire:submit.prevent="removerAlunoDaTurma" method="POST">
		<input type="hidden" wire:model="remover_aluno_turma_id" id="remover_aluno_turma_id">
		<div wire:ignore class="modal fade" id="ModalRemoverAlunoDaTurma" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  		<div class="modal-dialog modal-dialog-centered">
	    		<div class="modal-content">
	      			<div class="modal-header">
	        			<h3 class="modal-title" id="exampleModalLabel">Remover Aluno da Turma</h3>
	        			<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
	      			</div>
	      			<div class="modal-body text-center py-5 px-4">

	        			<p id="remover_turma_nome" class="m-0 fs-15 weight-600">
	        				
	        			</p>

	        			<hr/>

	        			<p class="m-0 fs-15">
	        				Você tem certeza que deseja remover este aluno desta turma?<br>
	        				Todos os dados referente a turma matriculado, incluindo também progresso do curso e avaliações serão permanentemente deletados. 
	        			</p>

	      			</div>
	      			<div class="modal-footer">
	      				<div class="d-flex w-100">
		      				<input type="password" wire:model="password_to_delete" class="w-100 form-control p-form me-3" placeholder="Digite sua senha" required>
		        			<button type="submit" class="btn btn-danger weight-600">Confirmar</button>
	    				</div>
	      			</div>
	    		</div>
	  		</div>
		</div>
	</form>

	<form method="POST" action="{{ route('admin.aluno.enviar-contrato', ['id' => $this->user->id]) }}">
	@csrf
		<div wire:ignore class="modal fade" id="showModal_SendMailContrato" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		  	<div class="modal-dialog modal-dialog-centered" role="document">
		    	<div class="modal-content">
			      	<div class="modal-header">
			        	<h3 class="modal-title" id="exampleModalCenterTitle">
			        		Contrato sem Assinatura do Aluno.<br>
			        		Envie agora para o aluno Assinar Digitalmente.
			        	</h3>
	        			<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			      	</div>
			      	<div class="modal-body">
						
						<input type="hidden" name="user_contrato_id" id="enviar_contrato_id" value="{{ session('user_contrato_id') }}">

						<div class="text-center bg-warning p-3 rounded-2 box-shadow">

							<span class="fs-17 text-dark">
								<b>Atenção!</b><br> * Antes de disparar, revise o contrato e somente após sua completa revisão, envie para o email do aluno para que então ele(a) possa Assinar digitalmente. *
							</span>

							<hr/>

							<div class="text-center">
								<a href="/{{ session('user_contrato_pdf') }}" target="_blank" class="text-center weight-600 fs-17" style="color: #0554f2;" id="enviar_contrato_referencia">
									{{ session('user_contrato_referencia') }}
								</a>
							</div>

						</div>

			      	</div>
			      	<div class="modal-footer">
						
						<button type="submit" class="btn btn-primary py-2 fs-15 weight-600">
							Já revisei o contrato e quero enviar agora!
						</button>

			      	</div>
		    	</div>
		  	</div>
		</div>
	</form>

	<div wire:ignore.self class="modal fade" id="ModalContratos" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	  	<div class="modal-dialog modal-dialog-centered modal-xl" role="document">
	    	<div class="modal-content">
	      		<div class="modal-header">
	        		<h3 class="modal-title" id="exampleModalCenterTitle">
	        			Contratos ({{ count($contratos) }})
	        		</h3>
	        		<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
	      		</div>
	      		<div class="modal-body">
				
					@if(count($contratos) == 0)
					<p class="text-center mb-0 py-3">Não há nenhum contrato anexado deste aluno.</p>
					@else
					<table class="table table-striped">
					  	<thead>
					    	<tr>
								<th scope="col">Cadastrante</th>
						      	<th scope="col">Referência</th>
								<th scope="col">PDF</th>
								<th scope="col">PDF Assinado</th>
								<th scope="col">Data de Assinatura</th>
								<th scope="col"><span class="float-end mr-2">Ações</span></th>
					    	</tr>
					  	</thead>
					  	<tbody>

						  	@foreach($contratos as $contrato)
						    <tr>

						      	<td>
								  	@php
								  		$cadastrante = \App\Models\User::plataforma()->where('id', $contrato->cadastrante_id)->first();
								  	@endphp

								  	@if($cadastrante)
							      		<a class="text-dark" href="{{ route('admin.colaborador.info', ['id' => $contrato->cadastrante_id]) }}"> 
							      			{{ App\Models\Util::replacePrimeiroNome($cadastrante->nome) ?? 'Não informado' }}
										</a>
									@else
										Não definido
									@endif
						      	</td>

								<td>{{ $contrato->referencia }}</td>

								<td>
									<a href="/{{ $contrato->pdf }}" target="_blank" class="color-blue-info-link">
										Arquivo
									</a>
								</td>

								<td>
									@if($contrato->pdf_assinado)
										<a href="/{{ $contrato->pdf_assinado }}" target="_blank" class="color-blue-info-link">
											Arquivo
										</a>
									@else
										Ainda não assinou
									@endif
								</td>

								<td>{{ \App\Models\Util::replaceDateTimePt($contrato->data_assinatura) ?? 'Ainda não assinou' }}</td>

								<td>
									<div class="float-end mr-2">
										<a href="#" data-bs-toggle="modal" data-bs-target="#showModal_SendMailContrato" data-bs-dismiss="modal" class="aclNivel2 mb-2 btn btn-primary" title="Enviar Email"
										onclick="
											document.getElementById('enviar_contrato_id').value = '{{ $contrato->id }}';
											document.getElementById('enviar_contrato_referencia').href = '/{{ $contrato->pdf }}';
											document.getElementById('enviar_contrato_referencia').innerHTML = '{{ $contrato->referencia }}';
										">
											<i class="fa fa-envelope"></i>
										</a>
										<a href="#" data-bs-toggle="modal" data-bs-target="#ModalDeletarContrato" data-bs-dismiss="modal" class="aclNivel3 mb-2 btn btn-danger" title="Deletar Contrato"
										onclick="
											document.getElementById('deletar_user_contrato_id').value = '{{ $contrato->id }}';
											document.getElementById('deletar_user_contrato_nome').innerHTML = '{{ $contrato->referencia }}';
										">
											<i class="fa fa-trash"></i>
										</a>
									</div>
								</td>

						    </tr>
						    @endforeach
					    
					  	</tbody>
					</table>
					@endif

	      		</div>
	      		<div class="modal-footer">

					<a href="#" wire:click="getAllContratos()" data-bs-toggle="modal" data-bs-target="#ModalCadastrarContrato" data-bs-dismiss="modal" class="btn btn-primary py-2">
						Cadastrar novo
					</a>

	      		</div>
	    	</div>
	   	</div>
	</div>

	<div wire:ignore.self class="modal fade" id="ModalDocumentos" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	  	<div class="modal-dialog modal-dialog-centered modal-xl" role="document">
	    	<div class="modal-content">
	      		<div class="modal-header">
	        		<h3 class="modal-title" id="exampleModalCenterTitle">
	        			Documentos ({{ count($documentos) }})
	        		</h3>
	        		<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
	      		</div>
	      		<div class="modal-body">
				
					@if(count($documentos) == 0)
					<p class="text-center mb-0 py-3">Não há nenhum documento anexado deste aluno.</p>
					@else
					<table class="table table-striped">
					  	<thead>
					    	<tr>
								<th scope="col">Cadastrante</th>
						      	<th scope="col">Nome</th>
								<th scope="col">Link</th>
								<th scope="col">Data</th>
								<th scope="col"><span class="float-end mr-2">Ações</span></th>
					    	</tr>
					  	</thead>
					  	<tbody>

						  	@foreach($documentos as $documento)
						    <tr>

						      	<td>
								  	@php
								  		$cadastrante = \App\Models\User::plataforma()->where('id', $documento->cadastrante_id)->first();
								  	@endphp

								  	@if($cadastrante)
							      		<a class="text-dark" href="{{ route('admin.colaborador.info', ['id' => $documento->cadastrante_id]) }}"> 
							      			{{ App\Models\Util::replacePrimeiroNome($cadastrante->nome) ?? 'Não informado' }}
										</a>
									@else
										Não definido
									@endif
						      	</td>

								<td>{{ $documento->nome }}</td>

								<td>
									@if(\Str::of($documento->link)->contains('http'))
										<a href="{{ $documento->link }}" target="_blank" class="color-blue-info-link">
											Arquivo
										</a>
									@else
										{{ $documento->link }}
									@endif
								</td>

								<td>{{ \App\Models\Util::replaceDateTimePt($documento->created_at) }} ({{ \Carbon\Carbon::createFromTimeStamp(strtotime($documento->created_at))->diffForHumans() }})</td>

								<td>
									<div class="float-end mr-2">
										<a href="#" data-bs-toggle="modal" data-bs-target="#ModalDeletarDocumento" data-bs-dismiss="modal" class="mb-2 btn btn-danger" title="Deletar Documento"
										onclick="
											document.getElementById('deletar_documento_id').value = '{{ $documento->id }}';
											document.getElementById('deletar_documento_nome').innerHTML = '{{ $documento->nome }}';
										">
											<i class="fa fa-trash"></i>
										</a>
									</div>
								</td>

						    </tr>
						    @endforeach
					    
					  	</tbody>
					</table>
					@endif

	      		</div>
	      		<div class="modal-footer">

					<a href="#" data-bs-toggle="modal" data-bs-target="#ModalCadastrarDocumento" data-bs-dismiss="modal" class="btn btn-primary py-2">
						Cadastrar novo
					</a>

	      		</div>
	    	</div>
	   	</div>
	</div>
	<form method="POST" enctype="multipart/form-data" action="{{ route('admin.aluno.cadastrar-documento', ['id' => $this->user->id]) }}">
	@csrf
		<div wire:ignore class="modal fade" id="ModalCadastrarDocumento" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		  	<div class="modal-dialog modal-dialog-centered" role="document">
		    	<div class="modal-content">
			      	<div class="modal-header">
			        	<h3 class="modal-title" id="exampleModalCenterTitle">Cadastrar Documento</h3>
	        			<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			      	</div>
			      	<div class="modal-body">
						
						<div class="mb-3">
							<label>Nome</label>
							<input type="text" name="nome" class="form-control p-form" placeholder="Referência" required>
						</div>
						<div class="mb-3">
							<label>Arquivo</label>
							<input type="file" name="arquivo" class="file-form" required>
						</div>

			      	</div>
			      	<div class="modal-footer">
						
						<button type="submit" name="add_documento" value="true" class="btn btn-primary py-2">
							Cadastrar
						</button>

			      	</div>
		    	</div>
		  	</div>
		</div>
	</form>
	<form method="POST" enctype="multipart/form-data" action="{{ route('admin.aluno.cadastrar-contrato', ['id' => $this->user->id]) }}">
	@csrf
		<div wire:ignore.self class="modal fade" id="ModalCadastrarContrato" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		  	<div class="modal-dialog modal-dialog-centered" role="document">
		    	<div class="modal-content">
			      	<div class="modal-header">
			        	<h3 class="modal-title" id="exampleModalCenterTitle">Adicionar Contrato</h3>
	        			<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			      	</div>
			      	<div class="modal-body">
						
						<div class="d-lg-flex mb-3">

							<div class="w-100 me-lg-3 mb-3 mb-lg-0">
								<label>Contrato</label>
								<select wire:model="contrato_id" name="contrato_id" onchange="setNameContratoSelected(this)" class="select-form" required>
									<option value="" selected>Selecione</option>
									@foreach($allContratos as $contrato)
										<option value="{{ $contrato->id }}">{{ $contrato->referencia }}</option>
									@endforeach
								</select>
							</div>

							<div class="w-100">
								<label>Referência</label>
								<input type="text" name="referencia" id="add_contrato_referencia" class="form-control p-form" required>
							</div>

						</div>

						@if(count($camposContrato) > 0)
						
						@php

							// PAI
							$resp_pai = \App\Models\ResponsavelAluno::select('responsaveis.*','responsaveis_alunos.grau_parentesco')
								->join('responsaveis','responsaveis_alunos.responsavel_id','responsaveis.id')
								->where('responsaveis_alunos.plataforma_id', session('plataforma_id'))
								->where('responsaveis_alunos.aluno_id', $user->id)
								->where('responsaveis_alunos.grau_parentesco','Pai')
								->first();

							if(isset($resp_pai))
							{
				                $resp_pai_enderecoCompleto = $resp_pai->rua . ', ' . $resp_pai->numero  . ' - ' . $resp_pai->bairro . ', ' . $resp_pai->cidade . ' - ' . $resp_pai->estado . ', ' . $resp_pai->cep . '. ' . $resp_pai->complemento;
				                $resp_pai_enderecoRuaENumero = $resp_pai->rua . ', ' . $resp_pai->numero;
							
				                $nome_resp_pai = $resp_pai->nome;
							}


							// MÃE
							$resp_mae = \App\Models\ResponsavelAluno::select('responsaveis.*','responsaveis_alunos.grau_parentesco')
								->join('responsaveis','responsaveis_alunos.responsavel_id','responsaveis.id')
								->where('responsaveis_alunos.plataforma_id', session('plataforma_id'))
								->where('responsaveis_alunos.aluno_id', $user->id)
								->where('responsaveis_alunos.grau_parentesco','Mãe')
								->first();

							if(isset($resp_mae))
							{
				                $resp_mae_enderecoCompleto = $resp_mae->rua . ', ' . $resp_mae->numero  . ' - ' . $resp_mae->bairro . ', ' . $resp_mae->cidade . ' - ' . $resp_mae->estado . ', ' . $resp_mae->cep . '. ' . $resp_mae->complemento;
				                $resp_mae_enderecoRuaENumero = $resp_mae->rua . ', ' . $resp_mae->numero;
							
				                $nome_resp_mae = $resp_mae->nome;
							}


							$unidade_nome = \App\Models\Unidade::select('nome')->where('id', $user->unidade_id)->value('nome');

			                $rua    = $endereco['rua']    ?? null;
			                $numero = $endereco['numero'] ?? null;
			                $enderecoRuaENumero = $rua . ', ' . $numero;

			                $rua         = $endereco['rua']         ?? null;
			                $numero      = $endereco['numero']      ?? null;
			                $bairro      = $endereco['bairro']      ?? null;
			                $cidade      = $endereco['cidade']      ?? null;
			                $estado      = $endereco['estado']      ?? null;
			                $cep         = $endereco['cep']         ?? null;
			                $complemento = $endereco['complemento'] ?? null;

			                $enderecoCompleto = $rua . ', ' . $numero  . ' - ' . $bairro . ', ' . $cidade . ' - ' . $estado . ', ' . $cep . '. ' . $complemento;
			                
					        $camposVariables = [
								'Nome'               => $user->nome,
								'Email'              => $user->email,
								'CPF'                => $user->cpf,
								'Matrícula'          => $user->id,
								'RG'                 => $user->rg,
								'Gênero'             => $user->genero,
								'Profissão'          => $user->profissao,
								'Observações'        => $user->obs,
								'Celular'            => $user->celular,
								'Telefone'           => $user->telefone,
								'Naturalidade'       => $user->naturalidade,
								'Data de Nascimento' => \App\Models\Util::replaceDatePt($user->data_nascimento ?? null) ?? null,
								'Data de Cadastro'   => $user->created_at,
								'Polo/Unidade'       => $unidade_nome ?? null,
								'CEP'                => $endereco['cep'] ?? null,
								'Rua'                => $endereco['rua'] ?? null,
								'Número'             => $endereco['numero'] ?? null,
								'Bairro'             => $endereco['bairro'] ?? null,
								'Cidade'             => $endereco['cidade'] ?? null,
								'Estado'             => $endereco['estado'] ?? null,
								'Complemento'        => $endereco['complemento'] ?? null,
								'Endereço'           => $enderecoCompleto,
								'Rua e Número'       => $enderecoRuaENumero,
								'Dia numeral atual'  => date('d'),
								'Mês numeral atual'  => date('m'),
								'Ano numeral atual'  => date('Y'),
								'Cadastrante'        => \Auth::user()->nome ?? null,
								'Nome do Pai'        => $nome_resp_pai ?? null,
								'Nome da Mãe'        => $nome_resp_mae ?? null,
					        ];

					        if($user->data_nascimento != null)
					        {
								$idade = \App\Models\Util::getIdade($user->data_nascimento);
					        
					        } else {

					        	 $idade = 0;
					        }

							if($idade < 18)
							{
						        $camposResp = [
									'Naturalidade do Responsável'       => $resp_mae->naturalidade ?? $resp_pai->naturalidade ?? null,
									'Cidade do Responsável'             => $resp_mae->cidade ?? $resp_pai->cidade ?? null,
									'CEP do Responsável'                => $resp_mae->cep ?? $resp_pai->cep ?? null,
									'Bairro do Responsável'             => $resp_mae->bairro ?? $resp_pai->bairro ?? null,
									'Endereço do Responsável'           => $resp_mae_enderecoCompleto ?? $resp_pai_enderecoCompleto ?? null,
									'Rua e Número do Responsável'       => $resp_mae_enderecoRuaENumero ?? $resp_pai_enderecoRuaENumero ?? null,
									'Data de Nascimento do Responsável' => \App\Models\Util::replaceDatePt($resp_mae->data_nascimento ?? null) ?? null,
									'RG do Responsável'                 => $resp_mae->rg ?? $resp_pai->rg ?? null,
									'CPF do Responsável'                => $resp_mae->cpf ?? $resp_pai->cpf ?? null,
						        ];


							} else {

						        $camposResp = [
									'Naturalidade do Responsável'       => $user->naturalidade,
									'Cidade do Responsável'             => $endereco['cidade'] ?? null,
									'CEP do Responsável'                => $endereco['cep'] ?? null,
									'Bairro do Responsável'             => $endereco['bairro'] ?? null,
									'Endereço do Responsável'           => $enderecoCompleto ?? null,
									'Rua e Número do Responsável'       => $enderecoRuaENumero,
									'Data de Nascimento do Responsável' => \App\Models\Util::replaceDatePt($user->data_nascimento ?? null) ?? null,
									'RG do Responsável'                 => $user->rg,
									'CPF do Responsável'                => $user->cpf,
						        ];
							}


							$camposVariables = array_merge($camposVariables, $camposResp);

						@endphp

						<div>
							<p class="mb-0">Campos de Auto Preenchimento</p>
							<hr class="mt-1 mb-2">
							@foreach($camposContrato as $campoContrato)
								@php
									$column = $camposVariables[$campoContrato->campo] ?? null;
								@endphp
								<div class="d-flex mb-2">
									<input type="hidden" name="camposIds[]" class="w-50 form-control p-form me-2" placeholder="CAMPO ID" value="{{ $campoContrato->id }}" readonly>
									<input type="text" class="w-50 form-control p-form me-2" placeholder="Campo" value="{{ $campoContrato->campo }}" disabled>
									<input type="text" name="values[]" class="form-control p-form @if(is_null($column)) border-danger @endif " placeholder="Preencha" value="{{ $column }}" required>
								</div>
							@endforeach
						</div>
						@endif

			      	</div>
			      	<div class="modal-footer">
						
						<button type="submit" name="add_contrato" value="true" class="btn btn-primary py-2">
							Cadastrar e Pré-visualizar
						</button>

			      	</div>
		    	</div>
		  	</div>
		</div>
	</form>

	<form method="POST" action="{{ route('admin.aluno.deletar-documento', ['id' => $this->user->id]) }}">
	@csrf
		<div wire:ignore class="modal fade" id="ModalDeletarDocumento" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		  	<div class="modal-dialog modal-dialog-centered" role="document">
		    	<div class="modal-content">
				    <div class="modal-header">
				        <h3 class="modal-title" id="exampleModalCenterTitle">Deletar Documento</h3>
				        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			      	</div>
		      		<div class="modal-body py-3 px-lg-4">
					
						<input type="hidden" name="documento_id" id="deletar_documento_id">			
						
						<p class="text-center" id="deletar_documento_nome">
						
						</p>

						<hr/>

						<p class="text-center">
							Você tem certeza que deseja deletar este documento do aluno
							<br>
							<span class="weight-600">{{ $user->nome }}</span>?
						</p>	
		
		      		</div>
		     	 	<div class="modal-footer">
					  	<button type="submit" class="btn btn-danger" name="deletar_documento" value="true">
					  	 	Confirmar
					  	</button>
		      		</div>
		     	</div>
		   </div>
		</div>
	</form>

	<form method="POST" action="{{ route('admin.aluno.deletar-contrato', ['id' => $this->user->id]) }}">
	@csrf
		<div wire:ignore class="modal fade" id="ModalDeletarContrato" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		  	<div class="modal-dialog modal-dialog-centered" role="document">
		    	<div class="modal-content">
				    <div class="modal-header">
				        <h3 class="modal-title" id="exampleModalCenterTitle">Deletar Contrato</h3>
				        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			      	</div>
		      		<div class="modal-body py-3 px-lg-4">
					
						<input type="hidden" name="user_contrato_id" id="deletar_user_contrato_id">			
						
						<p class="text-center weight-600 fs-15" id="deletar_user_contrato_nome">
						
						</p>

						<hr/>

						<p class="text-center">
							Você tem certeza que deseja deletar este contrato do aluno
							<br>
							<span class="weight-600">{{ $user->nome }}</span>?
						</p>	
		
		      		</div>
		     	 	<div class="modal-footer">
					  	<button type="submit" class="btn btn-danger" name="deletar_contrato" value="true">
					  	 	Confirmar
					  	</button>
		      		</div>
		     	</div>
		   </div>
		</div>
	</form>

	<div wire:ignore.self class="modal fade" id="ModalAvaliacaoAlunoAcoes" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	  	<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
	    	<div class="modal-content">
		      	<div class="modal-header">
		        	<h3 class="modal-title" id="exampleModalCenterTitle">Ações da Avaliação do Aluno</h3>
        			<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
		      	</div>
	      		<div class="modal-body">
				
					@if(count($avaliacoesByTurmaId) == 0)
					<p class="text-center mb-0 py-3"></p>
					@else
					<table class="table table-striped">
					  	<thead>
					    	<tr>
								<th scope="col">Avaliação</th>
						      	<th scope="col">Nota</th>
								<th scope="col"><span class="float-end mr-2">Ações</span></th>
					    	</tr>
					  	</thead>
					  	<tbody>

						  	@foreach($avaliacoesByTurmaId as $getAvaliacaoAluno)
						    <tr>
								<td>{{ $getAvaliacaoAluno->referencia }}</td>
								<td>{{ $getAvaliacaoAluno->nota }}</td>
								<td>
									<div class="float-end mr-2">
										<a href="/aluno/avaliacao/{{$getAvaliacaoAluno->id}}/correcao/{{$user->id}}" target="_blank" id="btnVisualizarAvaliacaoAluno" class="aclNivel2 btn btn-secondary py-2">
											<i class="fa fa-eye"></i>
											Visualizar Avaliação do Aluno
										</a>
										<a href="#" wire:click="setResetarAvaliacaoId({{$getAvaliacaoAluno->id}})" data-bs-toggle="modal" id="btnResetarAvaliacaoAluno" data-bs-target="#ModalResetar" data-bs-dismiss="modal" class="aclNivel2 btn btn-danger py-2">
											<i class="fas fa-undo-alt"></i>
											Resetar Avaliação do Aluno
										</a>
									</div>
								</td>

						    </tr>
						    @endforeach
					    
					  	</tbody>
					</table>
					@endif

	      		</div>
	    	</div>
	  	</div>
	</div>

	<form wire:submit.prevent="resetarAvaliacao" method="POST">
		<input type="hidden" wire:model="resetar_avaliacao_id">
		<div wire:ignore.self class="modal fade" id="ModalResetar" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  		<div class="modal-dialog modal-dialog-centered">
	    		<div class="modal-content">
	      			<div class="modal-header">
	        			<h3 class="modal-title" id="exampleModalLabel">Resetar Avaliação do Aluno</h3>
	        			<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
	      			</div>
	      			<div class="modal-body text-center py-5 px-4">

	        			<p class="m-0 fs-15">
	        				Você tem certeza que deseja resetar a avaliação deste(a) aluno(a)?<br>
	        			</p>

	      			</div>
	      			<div class="modal-footer">
	      				<div class="d-flex w-100">
		      				<input type="password" wire:model="password_to_delete" class="w-100 form-control p-form me-3" placeholder="Digite sua senha" required>
		        			<button type="submit" class="btn btn-danger weight-600">Confirmar</button>
	    				</div>
	      			</div>
	    		</div>
	  		</div>
		</div>
	</form>

	<div wire:ignore class="modal fade" id="ModalEntrarNaContaDoAluno" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  		<div class="modal-dialog modal-dialog-centered">
    		<div class="modal-content">
      			<div class="modal-header">
        			<h3 class="modal-title" id="exampleModalLabel">Link para Entrar na Conta do Aluno</h3>
        			<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      			</div>
      			<div class="modal-body text-center pt-4-5 pb-4-5 px-4">

					<p class="m-0 fs-15">
						Copie o link abaixo e abra em janela anônima do navegador.
					</p>

					<hr/>

					<input type="text" class="form-control p-form" readonly value="{{\Request::root()}}/login-by-hash/WZC{{date('dmH')}}/{{$user->id}}">

      			</div>
    		</div>
  		</div>
	</div>

	<div wire:ignore.self class="modal fade" id="ModalResponsaveis" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	  	<div class="modal-dialog modal-dialog-centered" role="document">
	    	<div class="modal-content">
	      		<div class="modal-header">
	        		<h3 class="modal-title" id="exampleModalCenterTitle">
	        			Responsáveis ({{ count($responsaveis) }})
	        		</h3>
	        		<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
	      		</div>
	      		<div class="modal-body">
				
					@if(count($responsaveis) == 0)
					<p class="text-center mb-0 py-3">Não há nenhum responsável deste aluno.</p>
					@else
					<table class="table table-striped">
					  	<thead>
					    	<tr>
						      	<th scope="col">Nome</th>
								<th scope="col">Grau Parentesco</th>
								<th scope="col"><span class="float-end mr-2">Ações</span></th>
					    	</tr>
					  	</thead>
					  	<tbody>
						  	@foreach($responsaveis as $responsavel)
						    <tr>
								<td>
									<a href="{{ route('admin.responsavel.info', ['id' => $responsavel->id]) }}" class="color-blue-info-link"> 
										{{ $responsavel->nome }}
									</a>
								</td>
								<td>{{ $responsavel->grau_parentesco ?? 'Não definido' }}</td>

								<td>
									<div class="float-end mr-2">
										<a href="#" data-bs-toggle="modal" data-bs-target="#ModalDesvincularResponsavel" data-bs-dismiss="modal" wire:click="setDesvincularResponsavelId({{$responsavel->id}}, '{{ $responsavel->nome }}')" class="mb-2 btn btn-danger" title="Desvincular Responsável">
											<i class="fa fa-times-circle"></i>
										</a>
									</div>
								</td>
						    </tr>
						    @endforeach
					  	</tbody>
					</table>
					@endif

	      		</div>
	      		<div class="w-100 modal-footer">
	      			<div class="w-100 d-lg-flex">
						<a href="{{ route('admin.responsavel.lista', ['add_responsaveis_para_aluno_id' => $this->user->id]) }}" class="me-lg-2 mb-2 mb-lg-0 w-100 py-2 btn btn-primary">
							Vincular um já existente
						</a>
						<a href="{{ route('admin.responsavel.add', ['aluno_id' => $this->user->id]) }}" class="w-100 py-2 btn btn-primary">
							Cadastrar novo
						</a>
					</div>
	      		</div>
	    	</div>
	   	</div>
	</div>

	<form wire:submit.prevent="desvincularResponsavel" method="POST">
		<input type="hidden" wire:model="desvincular_responsavel_id">
		<div wire:ignore.self class="modal fade" id="ModalDesvincularResponsavel" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  		<div class="modal-dialog modal-dialog-centered">
	    		<div class="modal-content">
	      			<div class="modal-header">
	        			<h3 class="modal-title" id="exampleModalLabel">Desvincular Responsável</h3>
	        			<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
	      			</div>
	      			<div class="modal-body text-center py-5 px-4">

	        			<p id="desvincular_responsavel_nome" class="m-0 color-555 fs-15 weight-600">
	        				{{ $desvincular_responsavel_nome }}
	        			</p>

	        			<hr/>

	        			<p class="m-0 fs-15">
	        				Você tem certeza que deseja desvincular este responsável deste(a) aluno(a)?<br>
	        			</p>

	      			</div>
	      			<div class="modal-footer">
	      				<div class="d-flex w-100">
		      				<input type="password" wire:model="password_to_delete" class="w-100 form-control p-form me-3" placeholder="Digite sua senha" required>
		        			<button type="submit" class="btn btn-danger weight-600">Confirmar</button>
	    				</div>
	      			</div>
	    		</div>
	  		</div>
		</div>
	</form>

	<div wire:ignore.self class="modal fade" id="ModalComunicados" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	  	<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
	    	<div class="modal-content">
	      		<div class="modal-header">
	        		<h3 class="modal-title" id="exampleModalCenterTitle">
	        			Comunicados recebidos ({{ count($comunicados) }})
	        		</h3>
	        		<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
	      		</div>
	      		<div class="modal-body">
				
					@if(count($comunicados) == 0)
					<p class="text-center mb-0 py-3">Não há nenhum comunicado enviado para este aluno.</p>
					@else
					<table class="table table-striped">
					  	<thead>
					    	<tr>
						      	<th scope="col">Assunto</th>
								<th scope="col">
									<span class="float-end me-1">
										Confirmação de Leitura
									</span>
								</th>
					    	</tr>
					  	</thead>
					  	<tbody>
						  	@foreach($comunicados as $comunicado)
						    <tr>
								<td>
									<a href="{{ route('admin.comunicado.info', ['id' => $comunicado->id]) }}" class="color-blue-info-link"> 
										{{ $comunicado->assunto }}
									</a>
								</td>
								<td>
									<span class="float-end">
										@if($comunicado->data_visualizacao)
											{{ \App\Models\Util::replaceDateTimePt($comunicado->data_visualizacao) }}
										@else
											Ainda não visualizou
										@endif
									</span>
								</td>
						    </tr>
						    @endforeach
					  	</tbody>
					</table>
					@endif

	      		</div>
	      		<div class="modal-footer">
					<a href="#ModalComunicadoAdd" data-bs-dismiss="modal" data-bs-toggle="modal" class="py-2 btn btn-primary">
						Enviar novo
					</a>
	      		</div>
	    	</div>
	   	</div>
	</div>

	<form wire:submit.prevent="enviarComunicado" method="POST">
		<div wire:ignore class="modal fade" id="ModalComunicadoAdd" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		  	<div class="modal-dialog modal-dialog-centered" role="document">
		    	<div class="modal-content">
		      		<div class="modal-header">
		        		<h3 class="modal-title" id="exampleModalCenterTitle">
		        			Enviar Comunicado
		        		</h3>
		        		<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
		      		</div>
		      		<div class="modal-body">
						
						<div class="mb-3 w-100">
							<label>Assunto</label>
							<input type="text" wire:model="comunicado_assunto" class="form-control p-form" placeholder="Assunto" maxlength="75">
						</div>

						<div class="w-100">
							<label>Mensagem</label>
							<textarea wire:model="comunicado_descricao" class="form-control p-form h-min-100" placeholder="Descreva sua mensagem"></textarea>
						</div>

		      		</div>
		      		<div class="modal-footer">
						<button type="submit" class="py-2 btn btn-primary">
							Enviar
						</button>
		      		</div>
		    	</div>
		   	</div>
		</div>
	</form>

	<div wire:ignore.self class="modal fade" id="ModalFinanceiroEntradas" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	  	<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
	    	<div class="modal-content">
	      		<div class="modal-header">
	        		<h3 class="modal-title" id="exampleModalCenterTitle">
	        			Financeiro ({{ count($entradas) }})
	        		</h3>
	        		<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
	      		</div>
	      		<div class="modal-body">
				
					@if(count($entradas) == 0)
					<p class="text-center mb-0 py-3">Não há nenhuma entrada deste aluno.</p>
					@else
			        <div class="table-responsive collapse show" id="tabEntradas">
			            <table class="table">
			              <thead>
			                <tr>
			                  <th scope="col">Produto</th>
			                  <th scope="col">Valor</th>
			                  <th scope="col">Status</th>
			                  <th scope="col">Data de Vencimento</th>
			                  <th scope="col">
			                    <span class="float-end">
			                        Ações
			                    </span>
			                  </th>
			                </tr>
			              </thead>
			              <tbody>
			                @foreach($entradas as $entrada)
			                <tr>
			                    <td>{{ $entrada->produto_nome }}</td>
			                    <td>
			                        @if($entrada->n_parcelas != $entrada->n_parcela || $entrada->registro_id != null)
			                            {{ $entrada->n_parcela }}/{{$entrada->n_parcelas}} de
			                        @endif
			                        R$ {{ $entrada->valor_parcela }}
			                    </td>
			                    <td>
			                        @switch($entrada->status)
			                            @case(0)
			                                @if(date('Y-m-d') > $entrada->data_vencimento)
			                                    <span class="py-1-5 px-2 text-light rounded bg-danger">
			                                        Em atraso
			                                    </span>
			                                @endif
			                                @if(date('Y-m-d') <= $entrada->data_vencimento)
			                                    <span class="py-1-5 px-2 text-light rounded bg-info">
			                                        A receber
			                                    </span>
			                                @endif
			                            @break
			                            @case(1)
			                                <span class="py-1-5 px-2 text-light rounded bg-success">
			                                    Recebido
			                                </span>
			                            @break
			                            @case(2)
			                                <span class="py-1-5 px-2 text-light rounded bg-inadimplente">
			                                    Estornado
			                                </span>
			                            @break
			                        @endswitch
			                    </td>
			                    <td>
			                        {{ date('d/m/Y', strtotime($entrada->data_vencimento)) }}
			                    </td>
			                    <td>
			                        <span class="float-end">
			                            <a href="{{ route('admin.controle-financeiro.lista.fluxo-caixa') }}?entrada_id={{$entrada->id}}&mes={{date('m', strtotime($entrada->data_vencimento))}}&ano={{date('Y', strtotime($entrada->data_vencimento))}}" title="Detalhes" class="btn-add py-2 px-2-5">
			                                <i class="fa fa-eye"></i>
			                            </a>
			                            <a href="#ModalFinanceiroDeletarEntrada" wire:click="setDeletarEntradaId({{$entrada->id}})" onclick="document.getElementById('deletar_valor').innerHTML = '{{ $entrada->valor_parcela }}';document.getElementById('deletar_produto_nome').innerHTML = '{{ $entrada->produto_nome }}';" data-bs-toggle="modal" data-bs-dismiss="modal" title="Deletar" class="btn btn-danger ms-1 py-1-4 px-2-5 mb-1">
			                                <i class="fa fa-trash"></i>
			                            </a>
			                        </span>
			                    </td>
			                </tr>
			                @endforeach
			              </tbody>
			            </table>
			        </div>
					@endif

	      		</div>
	      		<div class="modal-footer">
					<a href="{{ route('admin.controle-financeiro.lista.fluxo-caixa') }}?add_entrada=S&cliente_id={{$user->id}}" class="btn btn-primary py-2">
						Adicionar nova venda
					</a>
	      		</div>
	    	</div>
	   	</div>
	</div>

	<form wire:submit.prevent="deletarEntradaFinanceira" method="POST">
	    <div wire:ignore class="modal fade" id="ModalFinanceiroDeletarEntrada" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	        <div class="modal-dialog modal-dialog-centered" role="document">
	            <div class="modal-content">
	                <div class="modal-header">
	                    <h3 class="modal-title" id="exampleModalCenterTitle">
	                        Deletar Entrada Financeira
	                    </h3>
	                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
	                </div>
	                <div class="modal-body py-5">

	                    <div class="row align-items-center px-4">

	                        <div class="col-6">
	                            <p class="m-0 fs-15">
	                                <span class="d-block">Valor</span>
	                                <span class="weight-600" id="deletar_valor">

	                                </span>
	                            </p>
	                        </div>
	                        <div class="col-6">
	                            <p class="m-0 fs-15">
	                                <span class="d-block">Produto</span>
	                                <span class="weight-600" id="deletar_produto_nome">

	                                </span>
	                            </p>
	                        </div>

	                    </div>

	                    <hr/>

	                    <p class="text-center m-0 fs-15">
	                        Você tem certeza que deseja deletar esta venda?
	                        <br>
	                        Todas as informações também serão deletadas definitivamente.
	                        <span class="m-0 d-block mt-2 weight-600 text-danger" id="deletar_entrada_others_parcelas">
	                            
	                        </span>
	                    </p>

	                </div>
	                <div class="modal-footer">
	                    <div class="d-flex w-100">
	                        <input type="password" wire:model="password_to_delete" class="w-100 form-control p-form me-3" placeholder="Digite sua senha" required>
	                        <button type="submit" class="btn btn-danger weight-600">Confirmar</button>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </div>
	</form>

	@include('livewire.util.toast')

	<script>

		function controlBoxEmailContratacaoTrilha(el)
		{
			if(el.checked == false)
				document.getElementById('boxEmailContratacaoTrilha').style.display='none';
			else
				document.getElementById('boxEmailContratacaoTrilha').style.display='block';
		}
		function controlBoxEmailContratacaoTurma(el)
		{
			if(el.checked == false)
				document.getElementById('boxEmailContratacaoTurma').style.display='none';
			else
				document.getElementById('boxEmailContratacaoTurma').style.display='block';
		}

		function setNameContratoSelected(el)
		{
			document.getElementById('add_contrato_referencia').value = el.options[el.selectedIndex].text;
		}

	</script>

	<form method="POST" enctype="multipart/form-data" action="{{ route('admin.colaborador.info.setFotoPerfil', ['id' => $this->user->id]) }}">
	@csrf
		<input type="file" name="foto" style="display:none" id="input-file-foto" onchange="this.form.submit()" />
	</form>

	<script>
		
		function setFotoPerfil()
		{
			document.getElementById('input-file-foto').click();
		}

	</script>

</div>