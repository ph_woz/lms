@extends('layouts.admin.master')
@section('content')

<nav aria-label="breadcrumb" class="mb-4-5 p-3 bg-db rounded ashadow border">
	<ol class="breadcrumb p-0 m-0">
    	<li class="breadcrumb-item"><a href="{{ route('admin.curso.lista') }}" class="color-blue-info-link">Cursos</a></li>
    	<li class="breadcrumb-item"><a href="{{ route('admin.curso.info', ['id' => $curso_id]) }}" class="color-blue-info-link">{{ $curso }}</a></li>
    	<li class="breadcrumb-item text-dark" aria-current="page">Corpo Docente</li>
  	</ol>
</nav>


<section class="mb-4-5">

	<div class="section-title rounded-bottom-0 d-lg-flex justify-content-between align-items-center">
		<div>
			Docentes ({{ count($corpoDocente) }})
		</div>
		<div>
	    	<a href="#ModalDocenteAdd" data-bs-toggle="modal" class="btn btn-outline-primary border-light text-light">
	    		+ Adicionar novo
	    	</a>
	    </div>
	</div>

</section>

<div class="row">
    <div class="col-12">

        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">Nome</th>
                                <th scope="col">Titulação</th>
                                <th scope="col">
                                    <span class="float-end">
                                        Ações
                                    </span>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($corpoDocente as $docente)
                                <tr>
                                    <td>{{ $docente->nome }}</td>
                                    <td>{{ $docente->titulacao }}</td>
                                    <td>
                                        <span class="float-end">

                                            <a href="?editar_docente_id={{$docente->id}}" class="aclNivel2 btn btn-warning text-dark">
                                                <i class="fa fa-edit"></i>
                                            </a>
									    	<a href="#ModalDeletar" onclick="document.getElementById('deletar_id').value = '{{$docente->id}}'; document.getElementById('docente_nome').innerHTML = '{{ $docente->nome }}';" data-bs-toggle="modal" class="aclNivel3 btn btn-danger">
									    		<i class="fa fa-trash"></i>
									    	</a>
									    	
                                        </span>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
</div>


<form method="POST" enctype="multipart/form-data">
@csrf
    <div class="modal fade" id="ModalDocenteAdd" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      	<div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        	<div class="modal-content">
	          	<div class="modal-header">
	            	<h3 class="modal-title color-444 font-nunito weight-600">Adicionar Docente</h3>
	            	<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
	          	</div>
	          	<div class="modal-body">
					
					<div class="d-lg-flex mb-3">
		       			
		       			<div class="w-100 me-lg-3 mb-3 mb-lg-0">
		       				<label>Nome</label>
		       				<input type="text" name="nome" class="form-control p-form" placeholder="Nome" required>
		       			</div>

		       			<div class="w-100 me-lg-3 mb-3 mb-lg-0">
		       				<label>Titulação</label>
		       				<input type="text" name="titulacao" class="form-control p-form" placeholder="Ex: Mestre" required>
		       			</div>
						<div class="w-lg-60">
							<label>Foto</label>
							<input type="file" name="foto" class="file-form">
						</div>

				    </div>

				    <div class="w-100">
				    	<label>Formação</label>
				    	<textarea name="formacao" class="textarea-form" placeholder="Descreva"></textarea>
				    </div>

	          	</div>
          		<div class="modal-footer">
            		<button type="submit" name="add" value="S" class="btn btn-primary py-2 weight-600">
                		Confirmar
            		</button>
          		</div>
	        </div>
      	</div>
    </div>
</form>

@if(isset($editarDocente))
<form method="POST" enctype="multipart/form-data">
@csrf
    <div class="modal fade" id="ModalDocenteEditar" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      	<div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        	<div class="modal-content">
	          	<div class="modal-header">
	            	<h3 class="modal-title color-444 font-nunito weight-600">Editar Docente</h3>
	            	<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
	          	</div>
	          	<div class="modal-body">
					
					<div class="d-lg-flex mb-3">
		       			
		       			<div class="w-100 me-lg-3 mb-3 mb-lg-0">
		       				<label>Nome</label>
		       				<input type="text" name="nome" class="form-control p-form" placeholder="Nome" required value="{{ $editarDocente->nome }}">
		       			</div>

		       			<div class="w-100 me-lg-3 mb-3 mb-lg-0">
		       				<label>Titulação</label>
		       				<input type="text" name="titulacao" class="form-control p-form" placeholder="Ex: Mestre" required value="{{ $editarDocente->titulacao }}">
		       			</div>
						<div class="w-lg-60">
							<label>Foto</label>
							@if($editarDocente->foto_perfil)
								<a href="{{ $editarDocente->foto_perfil }}" class="font-nunito weight-600 fs-11 color-blue" target="_blank">
									visualizar anexo
								</a>
							@endif
							<input type="file" name="foto" class="file-form">
						</div>

				    </div>

				    <div class="w-100">
				    	<label>Formação</label>
				    	<textarea name="formacao" class="textarea-form" placeholder="Descreva">{{ $editarDocente->formacao }}</textarea>
				    </div>

	          	</div>
          		<div class="modal-footer">
            		<button type="submit" name="editar" value="S" class="btn btn-primary py-2 weight-600">
                		Confirmar
            		</button>
          		</div>
	        </div>
      	</div>
    </div>
</form>
@endif

<form method="POST">
@csrf
		
	<input type="hidden" name="deletar_id" id="deletar_id">

	<div class="modal fade" id="ModalDeletar" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  		<div class="modal-dialog modal-dialog-centered">
    		<div class="modal-content">
      			<div class="modal-header">
        			<h3 class="modal-title" id="exampleModalLabel">Deletar Docente</h3>
        			<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      			</div>
      			<div class="modal-body text-center py-5">
      				<p class="m-0 weight-600 fs-17" id="docente_nome"></p>
      				<hr>
        			<p class="m-0 fs-16">
        				Você tem certeza que deseja deletar este docente?
        				<br>
        				Todas as informações serão perdidas definitivamente.
        			</p>
      			</div>
      			<div class="modal-footer">
	        		<button type="submit" name="deletar" value="S" class="btn btn-danger weight-600">Confirmar</button>
      			</div>
    		</div>
  		</div>
	</div>
</form>

@push('scripts')
<script src="https://cdn.tiny.cloud/1/f50dygt4s4q2k3urd4bh4c2hkjb74m6exr798tjwbayaa0om/tinymce/5/tinymce.min.js"></script>
<script>
	tinymce.init({
		selector: '.textarea-form',
		language: 'pt_BR',
		height: 300,
		plugins: 'image lists link code',
		toolbar: "image | undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link | code"
	});
</script>
<script>

	@if(isset($editarDocente))

		var myModal = new bootstrap.Modal(document.getElementById("ModalDocenteEditar"), {});
		document.onreadystatechange = function () {
		  myModal.show();
		};
	
	@endif

</script>
@endpush

@endsection