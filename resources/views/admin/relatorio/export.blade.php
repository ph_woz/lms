<table class="table">
    <thead>
        <tr>
            @php
                function getApelido($select, $array)
                {
                   foreach ($array as $key => $val) {
                       if ($val['select'] === $select)
                       {
                            $apelido = explode('.', $val['select'])[1];
                            return $apelido;
                       }
                   }
                   return null;
                }

                function searchForSelect($select, $array)
                {
                   foreach ($array as $key => $val) {
                       if ($val['select'] === $select) {
                           return $val['nome'];
                       }
                   }
                   return null;
                }

            @endphp

            @foreach($camposSelected as $campo)
                <th class="weight-600 font-poppins text-dark fs-13">
                    {{ searchForSelect($campo, $allCampos) }}
                </th>
            @endforeach
        </tr>
    </thead>
    <tbody>
        @foreach($query as $q)
            <tr>
                @foreach($camposSelected as $campo)

                    @php
                        if(\Str::contains($campo, '.'))
                        {
                           $apelido = getApelido($campo, $allCampos);

                        } else {

                            $apelido = $campo;
                        }
                    @endphp
                    
                    <td class="{{ $campo }}">
                        {!! $q->$apelido ?? '<i>Não definido</i>' !!}
                    </td>

                @endforeach
            </tr>
        @endforeach
    </tbody>
</table>