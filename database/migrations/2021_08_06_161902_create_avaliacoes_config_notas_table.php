<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAvaliacoesConfigNotasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('avaliacoes_config_notas', function (Blueprint $table) {
            $table->id();
            $table->integer('plataforma_id');
            $table->integer('avaliacao_id');
            $table->integer('n_questoes');
            $table->string('valor_nota', 5);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('avaliacoes_config_notas');
    }
}
