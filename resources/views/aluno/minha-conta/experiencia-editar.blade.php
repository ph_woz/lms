@extends('layouts.aluno')
@section('content')

@section('css')
<style>

    #url-{{\Request::segment(3)}}-{{\Request::segment(4)}}  { background: #222e3c!important; }
    #href-url-{{\Request::segment(3)}} { background: #222e3c!important; }

</style>
@endsection

@section('seo_title', 'Acadêmico')
<div class="container-fluid px-xl-5 mt-5">

	<div class="row">

        <div class="col-lg-4">
        
            <div class="mb-4 p-5 rounded-10 box-shadow bg-14">

                @include('aluno.minha-conta.componentes.navbar')

            </div>

        </div>

		<div class="col-lg-8">
		      
        <form method="POST">
        @csrf

    			<div class="mb-4 p-5 rounded-10 box-shadow bg-14">

                    <div class="d-lg-flex mb-3">

                        <div class="w-100">
                            <label class="text-light">Nome da Empresa</label>
                            <input type="text" name="empresa" class="form-control p-form bg-444 border-0 text-white py-3 color-placeholder-light" placeholder="Empresa" required value="{{ $experiencia->empresa }}">
                        </div>

                        <div class="w-lg-80 mx-lg-3 my-3 my-lg-0">
                            <label class="text-light">Seu cargo na empresa</label>
                            <input type="text" name="cargo" class="form-control p-form bg-444 border-0 text-white py-3 color-placeholder-light" placeholder="Cargo" required value="{{ $experiencia->cargo }}">
                        </div>

                        <div class="w-lg-40 me-lg-3 mb-3 mb-lg-0">
                            <label class="text-light">Data Início</label>
                            <input type="text" name="data_inicio" class="mask_mes_ano form-control p-form bg-444 border-0 text-white py-3 color-placeholder-light" placeholder="Mês/Ano" minlength="7" required value="{{ $experiencia->data_inicio }}">
                        </div>

                        <div class="w-lg-40">
                            <label class="text-light">Data de Fim</label>
                            <input type="text" name="data_fim" class="mask_mes_ano form-control p-form bg-444 border-0 text-white py-3 color-placeholder-light" placeholder="Mês/Ano" minlength="7" value="{{ $experiencia->data_fim }}">
                        </div>

                    </div>
                    
                    <div class="mb-3">
                        <label class="text-light">Sua as atividades exercidas na empresa</label>
                        <textarea type="text" name="atividades" class="form-control p-form bg-444 border-0 text-white py-3 color-placeholder-light h-min-100" placeholder="Sua função na empresa" required>{{ $experiencia->atividades }}</textarea>
                    </div>

                    <div class="mb-3">
                        <label class="text-light">Faça uma breve descrição sobre a empresa</label>
                        <textarea name="sobre" class="form-control p-form bg-444 border-0 text-white py-3 color-placeholder-light h-min-100" placeholder="Pode contar um pouco sobre o que você achou de trabalhar nesta empresa e os prós e contras" required>{{ $experiencia->sobre }}</textarea>
                    </div>

                    <div class="mb-3">

                        <label class="text-light">Avalie esta Empresa</label>

                        <div class="mt-1">
                          <a href="#" data-star="1" class="btn-setStar cursor-pointer hover-d-none">
                            <i class="fa fa-star star-1 chooseStar color-777 fs-25"></i>
                          </a>
                          <a href="#" data-star="2" class="btn-setStar cursor-pointer hover-d-none">
                            <i class="fa fa-star star-2 chooseStar color-777 fs-25"></i>
                          </a>
                          <a href="#" data-star="3" class="btn-setStar cursor-pointer hover-d-none">
                            <i class="fa fa-star star-3 chooseStar color-777 fs-25"></i>
                          </a>
                          <a href="#" data-star="4" class="btn-setStar cursor-pointer hover-d-none">
                            <i class="fa fa-star star-4 chooseStar color-777 fs-25"></i>
                          </a>
                          <a href="#" data-star="5" class="btn-setStar cursor-pointer hover-d-none">
                            <i class="fa fa-star star-5 chooseStar color-777 fs-25"></i>
                          </a>
                        </div>

                        <div class="alert alert-danger mt-1" id="errorStar" style="display:none;">
                            <b>Ops!</b> Escolha uma estrela das 5
                        </div>

                        <input type="text" name="estrelas" id="valueStar" class="ms-5 h-0px w-0px p-0 m-0 outline-0 bg-transparent border-color-transparent" required value="{{ $experiencia->estrelas }}">

                    </div>

                    <button type="submit" name="sent" value="S" class="btn-suporte-enviar px-4-5">
                        Salvar Alterações
                    </button>

    			</div>

        </form>

		</div>

	</div>

</div>
@endsection

@push('scripts')
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.js"></script>
    <script>

        $('.mask_mes_ano').mask('00/0000');

        document.getElementById('url-{{\Request::segment(3)}}').classList.add('show');

        $(document).ready(function()
        {
            for (var i = {{ $experiencia->estrelas ?? 0 }}; i >= 0; i--)
            {
                $(".star-"+i).removeClass('color-777').addClass('text-warning');
            }
        });

        $(".btn-setStar").click(function(e) {

          e.preventDefault();

          star = $(this).attr("data-star");

          $("#valueStar").val(star);

          $('.chooseStar').removeClass('text-warning').addClass('color-777');
          $(this).find('.chooseStar').first().removeClass('color-777').addClass('text-warning');

          $(this).prev().children().removeClass('color-777').addClass('text-warning');
          $(this).prev().prev().children().removeClass('color-777').addClass('text-warning');
          $(this).prev().prev().prev().children().removeClass('color-777').addClass('text-warning');
          $(this).prev().prev().prev().prev().children().removeClass('color-777').addClass('text-warning');

        });

    </script>

@endpush