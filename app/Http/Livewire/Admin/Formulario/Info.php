<?php

namespace App\Http\Livewire\Admin\Formulario;

use Livewire\Component;
use App\Models\Formulario;
use App\Models\FormularioPergunta;
use App\Models\FormularioCategoria;
use App\Models\User;
use App\Models\Turma;
use App\Models\Trilha;
use App\Models\FormularioHubspotProperty;
use App\Models\FormularioIntegracao;
use Illuminate\Support\Facades\Http;

class Info extends Component
{
    public $formulario;
    public $perguntas;
    public $cadastrante;
    public $linksTurmas;
    public $linksTrilhas;
    public $password_to_delete;


    // Configurações de Perguntas
    public $pergunta;
    public $tipo_input = 'text';
    public $options = [];
    public $ordem;
    public $obrigatorio = 'S';
    public $validation;

    public $check_integracao_hubspot = false;
    public $hubspot_propriedade;

    public $perguntaEditar;
    public $editar_pergunta_id;
    public $deletar_pergunta_id;

    public $i = 0;


    public function mount($formulario_id)
    {
        $this->formulario = Formulario::plataforma()->find($formulario_id);
            
        $this->linksTrilhas = Trilha::plataforma()->where('formulario_id', $formulario_id)->select('id','referencia')->orderBy('referencia')->get();
        $this->linksTurmas  = Turma::plataforma()->where('formulario_id', $formulario_id)->select('id','nome')->orderBy('nome')->get();

        $this->cadastrante = User::plataforma()->where('id', $this->formulario->cadastrante_id)->first();

        // Set Params Editar Pergunta
        $this->editar_pergunta_id = $_GET['editar_pergunta_id'] ?? null;

        $this->perguntaEditar = FormularioPergunta::plataforma()->where('id', $this->editar_pergunta_id)->first() ?? null; 

        $this->check_integracao_hubspot = FormularioIntegracao::verificaSeIntegrado('hubspot');

        if($this->perguntaEditar)
        {
            $this->pergunta    = $this->perguntaEditar->pergunta;
            $this->tipo_input  = $this->perguntaEditar->tipo_input;
            $this->ordem       = $this->perguntaEditar->ordem;
            $this->obrigatorio = $this->perguntaEditar->obrigatorio;
            $this->validation  = $this->perguntaEditar->validation;

            if($this->check_integracao_hubspot == true)
            {
                $this->hubspot_propriedade = FormularioHubspotProperty::plataforma()->where('formulario_id', $formulario_id)->where('pergunta_id', $this->editar_pergunta_id)->pluck('property')[0] ?? null;
            }

            if($this->perguntaEditar->options)
                $this->options = unserialize($this->perguntaEditar->options);

            foreach($this->options as $key => $opt)
            {
                $this->options[$key] = $opt;
                $this->addOptions($key);
            }

            $this->options = array_filter($this->options);
        }
    }

    public function render()
    {
        $this->perguntas = FormularioPergunta::plataforma()->where('formulario_id', $this->formulario->id)->orderBy('ordem')->get();

        if(empty($this->options))
            $this->addOptions($this->i);

        return view('livewire.admin.formulario.info', ['perguntas' => $this->perguntas]);
    }

    public function addOptions($i)
    {
        $this->i = $i+1;
        $this->options[$this->i] = null;
    }

    public function removeOptions($i)
    {
        unset($this->options[$i]);
    }

    public function addPergunta()
    {
        if($this->options)
            $options = serialize(array_filter($this->options));
        else
            $options = null;


        $pergunta = FormularioPergunta::create([
            'formulario_id' => $this->formulario->id,
            'pergunta'      => $this->pergunta,
            'tipo_input'    => $this->tipo_input,
            'options'       => $options,
            'validation'    => $this->validation,
            'obrigatorio'   => $this->obrigatorio,
            'ordem'         => $this->ordem,
        ]);

        if($this->hubspot_propriedade)
        {
            FormularioHubspotProperty::create([
                'formulario_id' => $this->formulario->id,
                'pergunta_id'   => $pergunta->id,
                'property'      => $this->hubspot_propriedade,
                'input'         => $this->validation ?? $this->pergunta,
            ]);
        }

        $this->resetFields();

        session()->flash('success-livewire', 'Pergunta adicionada com êxito.');

        $this->dispatchBrowserEvent('toast');
        $this->dispatchBrowserEvent('dissmiss-modal');
    }

    public function editarPergunta()
    {
        if($this->options)
            $options = serialize(array_filter($this->options));
        else
            $options = null;

        if($this->validation == '')
        {
            $this->validation = null;
        }

        $this->perguntaEditar->update([
            'pergunta'      => $this->pergunta,
            'tipo_input'    => $this->tipo_input,
            'options'       => $options,
            'validation'    => $this->validation,
            'obrigatorio'   => $this->obrigatorio,
            'ordem'         => $this->ordem,
        ]);


        if($this->check_integracao_hubspot == true)
        {
            FormularioHubspotProperty::updateOrCreate(['formulario_id' => $this->formulario->id, 'pergunta_id' => $this->editar_pergunta_id], 
                [
                    'property' => $this->hubspot_propriedade,
                    'input'    => $this->validation ?? $this->pergunta,
                ]
            );
        }

        session()->flash('success-livewire', 'Pergunta atualizada com êxito.');

        $this->dispatchBrowserEvent('toast');
        $this->dispatchBrowserEvent('dissmiss-modal');
    }

    public function setDeletePerguntaId($pergunta_id)
    {
        $this->deletar_pergunta_id = $pergunta_id;
    }
    public function deletePergunta()
    {
        FormularioPergunta::plataforma()->where('formulario_id', $this->formulario->id)->where('id', $this->deletar_pergunta_id)->delete();
        
        if($this->check_integracao_hubspot == true)
        {
            FormularioHubspotProperty::plataforma()->where('formulario_id', $this->formulario->id)->where('pergunta_id', $this->deletar_pergunta_id)->delete();
        }

        session()->flash('success-livewire', 'Pergunta removida com êxito.');
        $this->dispatchBrowserEvent('toast');
        $this->dispatchBrowserEvent('dissmiss-modal');
    }

    public function resetFields()
    {
        $this->pergunta    = null;
        $this->tipo_input  = 'text';
        $this->options     = [];
        $this->ordem       = null;
        $this->obrigatorio = 'S';
        $this->validation  = null;
        $this->hubspot_propriedade = null;
    }

    public function delete()
    {
        if(\Auth::attempt(['id' => \Auth::id(), 'password' => $this->password_to_delete, 'status' => 0]))
        {
            $n_perguntas_deletadas = FormularioCategoria::plataforma()->where('formulario_id', $this->formulario->id)->delete();
         
            \App\Models\Log::create([
                'tipo'  => 'formulario',
                'ref'   => 'Formulário: ' . $this->formulario->referencia . ' deletado com ' . $n_perguntas_deletadas . ' perguntas.'
            ]);

            Formulario::plataforma()->where('id', $this->formulario->id)->delete();

            session()->flash('success', 'Formulario deletado com êxito.');
            
            return redirect(route('admin.formulario.lista'));

        } else {

            session()->flash('danger-livewire', 'Senha incorreta.');

            $this->dispatchBrowserEvent('toast');
        }
    }
}
