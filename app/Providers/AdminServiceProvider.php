<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Models\PlataformaModulo;
use App\Models\Plataforma;
use App\Models\AclPlataformaModuloUser;

class AdminServiceProvider extends ServiceProvider
{
    public function boot()
    {
        view()->composer('layouts.admin.master', function($view)
        {
            $meusModulosSidebar = PlataformaModulo::meusModulosSidebar();

            $url  = \Request::segment(2);
            $acao = \Request::segment(3);

            $getAtualModulo = PlataformaModulo::plataforma()->where('url', $url)->first();

            $nomePaginaAtual = $this->nomePaginaAtual($acao, $getAtualModulo->texto ?? null);

            $forbidden = array('errors' => false, 'label' => null);
            
            if($getAtualModulo)
            {
                $getAtualAcl = AclPlataformaModuloUser::getModuloId($getAtualModulo->id);

                if(is_null($getAtualAcl))
                {
                    $forbidden['label'] = 'Sem permissão de acesso.';
                    $forbidden['errors'] = true;

                } else {

                    $forbidden = $this->acl($url, $acao, $getAtualModulo->status, $getAtualAcl->nivel);
                }
            }

            $view->with([ 
                'meusModulosSidebar'  => $meusModulosSidebar,
                'forbidden'           => $forbidden       ?? null,
                'getAtualAcl'         => $getAtualAcl     ?? null,
                'nomePaginaAtual'     => $nomePaginaAtual ?? null,
            ]);

        });

    }

    public function nomePaginaAtual($acao, $nome)
    {
        switch ($acao)
        {
            case 'lista':
                return $nome;

            case 'info':
                return $this->singular($nome);

            case 'add':
                return 'Adicionar ' . $this->singular($nome);
            
            case 'editar':
                return 'Editar ' . $this->singular($nome);

            case 'delete':
                return 'Delete ' . $this->singular($nome);
        }
    }

    public function singular($nome)
    {
        $nome = str_replace('ções','ção', $nome);
        $nome = str_replace('tões','tão', $nome);
        $nome = str_replace('ssões','ssão', $nome);
        $nome = str_replace('res','r', $nome);
        $nome = str_replace('ers','er', $nome);
        $nome = str_replace('etão','estão', $nome);
        $nome = str_replace('os','o', $nome);
        $nome = str_replace('as','a', $nome);
        $nome = str_replace('ais','al', $nome);
        $nome = str_replace('dades','dade', $nome);
        $nome = str_replace('Dahboard','Dashboard', $nome);

        return $nome;
    }

    public function acl($url, $acao, $getAtualModuloStatus, $getAtualAclNivel)
    {
        $forbidden = array('errors' => false, 'label' => null);

        if($getAtualModuloStatus == 1 || $getAtualModuloStatus == 2)
        {
            $forbidden['label'] = 'Este módulo está desativado nesta plataforma.';
        }

        if($getAtualAclNivel == 0)
        {
            $forbidden['label'] = 'Você não tem permissão para acessar este módulo.';
        }

        if($getAtualAclNivel < 2 && $acao == 'add')
        {
            $forbidden['label'] = 'Você não tem permissão de cadastro neste módulo.';
        }

        if($getAtualAclNivel < 2 && $acao == 'editar')
        {
            $forbidden['label'] = 'Você não tem permissão de cadastro neste módulo.';
        }

        if($getAtualAclNivel < 3 && $acao == 'delete')
        {
            $forbidden['label'] = 'Você não tem permissão para deletar neste módulo.';
        }

        if($forbidden['label'])
        {
            $forbidden['errors'] = true;
        }

        return $forbidden;

    }

}
