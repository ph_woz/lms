<div>

    <form>

        <div class="d-lg-flex mb-3">

            <input wire:model="search" type="search" class="form-control p-form" placeholder="Buscar">

            <select wire:model="publicado" class="ms-lg-3 mt-3 mt-lg-0 select-form w-lg-45">
                <option value="S">Publicados</option>
                <option value="N">Não publicados</option>
            </select>

            <select wire:model="arquivado" class="mx-lg-3 my-3 my-lg-0 select-form w-lg-45">
                <option value="N">Não arquivados</option>
                <option value="S">Arquivados</option>
            </select>

            <select wire:model="ordem" class="select-form w-lg-40">
                <option value="nome">Alfabética</option>
                <option value="desc">Mais novos</option>
                <option value="asc">Mais antigos</option>
            </select>

        </div>

        <div wire:ignore>
            <select wire:model="categoriasSelected" class="selectpicker" multiple data-width="100%" data-live-search="true" title="Categorias">
                @foreach($categorias as $categoria)
                    <option value="{{ $categoria->id }}">{{ $categoria->nome }}</option>
                @endforeach
            </select>
        </div>

    </form>

    <div class="mt-5 d-flex justify-content-between">

        <div class="align-self-end mb-2">
            <span class="resultados-encontrados">
                {{ count($documentos) }} resultados de {{ $countDocumentos }}.
            </span>
        </div>

        <div class="mb-3">
            <a href="{{ route('admin.documento.add') }}" class="btn-add aclNivel2">
                + Adicionar
            </a>
        </div>

    </div>

    <div class="row">
        <div class="col-12">

            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col">Nome</th>
                                    <th scope="col">Referência</th>
                                    <th scope="col">
                                        <span class="float-end">
                                            Ações
                                        </span>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($documentos as $documento)
                                    <tr>
                                        <td>{{ $documento->nome }}</td>
                                        <td>{{ $documento->referencia ?? 'Não definido' }}</td>
                                        <td>
                                            <span class="float-end">

                                                <a href="{{ route('admin.documento.info', ['id' => $documento->id]) }}" class="btn bg-base text-white">
                                                    <i class="fa fa-eye"></i>
                                                </a>

                                            </span>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            {{ $documentos->links() }}

        </div>
    </div>

</div>