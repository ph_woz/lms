@extends('layouts.aluno')
@section('content')

@section('seo_title', 'Suporte')
<div class="container-fluid px-xl-5 mt-5">

	<div class="mb-4 p-4 rounded-10 box-shadow bg-14">

		<div class="row">

			<div class="col-lg-6">

				<form method="POST">
				@csrf
					<input type="hidden" name="unidade_id" value="{{ \Auth::user()->unidade_id }}">
					<input type="text" name="nome" class="form-control p-form bg-form-suporte border-0 color-form-suporte py-3 color-placeholder-light" placeholder="Nome" required value="{{ \Auth::user()->nome }}">
					<input type="email" name="email" class="my-2 form-control p-form bg-form-suporte border-0 color-form-suporte py-3 color-placeholder-light" placeholder="Email" required value="{{ \Auth::user()->email }}">
					<input type="text" name="assunto" class="mb-2 form-control p-form bg-form-suporte border-0 color-form-suporte py-3 color-placeholder-light" placeholder="Assunto" required autofocus maxlength="75">
					<textarea name="mensagem" class="form-control p-form bg-form-suporte border-0 color-form-suporte py-3 color-placeholder-light h-min-100" placeholder="Mensagem" required></textarea>
					<button type="submit" name="sentContato" value="S" class="btn-suporte-enviar mt-3">
						Enviar
					</button>

				</form>

			</div>

			<div class="col-lg-6 mt-5 mt-lg-0">

				<div class="ps-lg-3 pt-lg-2">

					@if($plataforma->horario)
					<div class="mb-3">
			            <span class="color-light-soft">
			                <i class="fa fa-headset"></i>
			                {{ $plataforma->horario }}
		            	</span>
		            </div>
		            @endif

		            @if($plataforma->email)
					<div class="mb-3">
						<span class="color-light-soft">
							<i class="fa fa-envelope"></i> 
							{{ $plataforma->email }}
						</span>
					</div>
					@endif

					@if($plataforma->telefone)
					<div class="mb-3">
						<a href="tel:+55{{preg_replace('/\D/', '', $plataforma->telefone)}}" class="color-light-soft">
							<i class="fa fa-phone"></i> 
							{{ $plataforma->telefone }}
						</a>
					</div>
					@endif

					@if($plataforma->whatsapp)
					<div class="mb-3">
						<a href="https://api.whatsapp.com/send?phone=55{{preg_replace('/\D/', '', $plataforma->whatsapp)}}&text=Olá, vim através da Área do Aluno {{ $plataforma->nome }}." class="color-light-soft">
							<i class="fab fa-whatsapp"></i> 
							{{ $plataforma->whatsapp }}
						</a>
					</div>
					@endif

				</div>

			</div>

		</div>

	</div>

</div>
@endsection