<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Exports\RelatorioExport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\DB;
use App\Models\Unidade;
use App\Models\Trilha;
use App\Models\Turma;
use App\Models\User;
use App\Models\Contato;
use App\Models\Produto;
use App\Models\Plano;
use App\Models\Servico;
use App\Models\Avaliacao;
use App\Models\Curso;
use App\Models\Categoria;
use App\Models\Formulario;
use App\Models\Util;
use Illuminate\Database\Eloquent\Builder;

class RelatorioController extends Controller
{
    public function lista()
    {
        $unidade_id = null;

        if(\Auth::user()->restringir_unidade == 'S' && \Auth::user()->unidade_id != null)
        {
            $unidade_id = \Auth::user()->unidade_id;
        }

        $campos   = $this->getCampos();        
        $cursos   = Curso::select('id','referencia')->plataforma()->ativo()->orderBy('referencia')->get();
        $unidades = Unidade::select('id','nome')->plataforma()->ativo()->orderBy('nome')->get();

        $categoriasA = Categoria::select('id','nome')->plataforma()->ativo()->tipo('aluno')->orderBy('nome')->get();
        $categoriasC = Categoria::select('id','nome')->plataforma()->ativo()->tipo('contato')->orderBy('nome')->get();

        if($unidade_id)
        {
            $trilhas      = Trilha::select('id','referencia')->plataforma()->where('unidade_id', $unidade_id)->ativo()->orderBy('referencia')->get();
            $turmas       = Turma::select('id','nome')->plataforma()->where('unidade_id', $unidade_id)->ativo()->orderBy('nome')->get();
            $cadastrantes = User::select('id','nome')->plataforma()->where('unidade_id', $unidade_id)->colaborador()->ativo()->orderBy('nome')->get();
            $formularios  = Formulario::select('id','referencia')->plataforma()->where('unidade_id', $unidade_id)->ativo()->orderBy('referencia')->get();

            $planos   = Plano::select('id','nome')->plataforma()->ativo()->where('unidade_id', $unidade_id)->orderBy('nome')->get();
            $produtos = Produto::select('id','nome')->plataforma()->ativo()->where('unidade_id', $unidade_id)->orderBy('nome')->get();
            $servicos = Servico::select('id','nome')->plataforma()->ativo()->where('unidade_id', $unidade_id)->orderBy('nome')->get();

        } else {

            $trilhas      = Trilha::select('id','referencia')->plataforma()->ativo()->orderBy('referencia')->get();
            $turmas       = Turma::select('id','nome')->plataforma()->ativo()->orderBy('nome')->get();
            $cadastrantes = User::select('id','nome')->plataforma()->colaborador()->ativo()->orderBy('nome')->get();
            $formularios  = Formulario::select('id','referencia')->plataforma()->ativo()->orderBy('referencia')->get();
        
            $planos   = Plano::select('id','nome')->plataforma()->ativo()->orderBy('nome')->get();
            $produtos = Produto::select('id','nome')->plataforma()->ativo()->orderBy('nome')->get();
            $servicos = Servico::select('id','nome')->plataforma()->ativo()->orderBy('nome')->get();
        }

        return view('admin.relatorio.lista', 
            compact(
                'unidades',
                'campos',
                'trilhas',
                'cursos',
                'turmas',
                'cadastrantes',
                'formularios',
                'planos',
                'produtos',
                'servicos',
                'categoriasA',
                'categoriasC'
            )
        );
    }

    public function buscar()
    {
        if(request('tipo-perfil-aba') == 'A')
        {
            return $this->buscarAlunos();

        } else {

            return $this->buscarContatos();
        }
    }

    public function buscarAlunos()
    {
        $getNomePaginaInterno = 'Resultado de Relatórios Gerais de Alunos';

        $route = '/admin/aluno/info';

        $filtrosAplicados = collect();

        $camposSelected = request('campos');
        $fields = 'users.id, '. implode(', ', $camposSelected);

        $plataforma_id = session('plataforma_id') ?? Auth::user()->plataforma_id;

        $fields = str_replace('data_matricula_turma', 'DATE_FORMAT(turmas_alunos.created_at, "%d/%m/%Y %H:%i") as data_matricula_turma', $fields);
        $fields = str_replace('data_matricula_trilha', 'DATE_FORMAT(trilhas_alunos.created_at, "%d/%m/%Y %H:%i") as data_matricula_trilha', $fields);
        $fields = str_replace('data_avaliacao_finalizada', 'DATE_FORMAT(avaliacoes_alunos.data_finalizada, "%d/%m/%Y %H:%i") as data_avaliacao_finalizada', $fields);

        $fields = str_replace('users_data_cadastro', 'DATE_FORMAT(users.created_at, "%d/%m/%Y %H:%i") as users_data_cadastro', $fields);
        $fields = str_replace('users_data_nascimento', 'DATE_FORMAT(users.data_nascimento, "%d/%m/%Y") as users_data_nascimento', $fields);
        $fields = str_replace('users_data_ultimo_acesso', 'COALESCE(DATE_FORMAT(users.data_ultimo_acesso, "%d/%m/%Y %H:%i"), "<i>Ainda não acessou</i>") as users_data_ultimo_acesso', $fields);
        $fields = str_replace('data_conclusao_trilha', 'DATE_FORMAT(trilhas_alunos.data_conclusao, "%d/%m/%Y %H:%i") as data_conclusao_trilha', $fields);
        $fields = str_replace('users_data_conclusao_curso', 'DATE_FORMAT(turmas_alunos.data_conclusao_curso, "%d/%m/%Y %H:%i") as users_data_conclusao_curso', $fields);
        $fields = str_replace('data_ultimo_acesso_curso', 'COALESCE(DATE_FORMAT(turmas_alunos.data_ultimo_acesso, "%d/%m/%Y %H:%i"), "<i>Ainda não acessou</i>") as data_ultimo_acesso_curso', $fields);
        $fields = str_replace('users_data_docs_recebidas', 'DATE_FORMAT(users.data_docs_recebidas, "%d/%m/%Y %H:%i") as users_data_docs_recebidas', $fields);
        $fields = str_replace("users_status", "(CASE WHEN users.status = 0 THEN 'Ativo' ELSE 'Desativado' END) as users_status", $fields);
        $fields = str_replace("users_idade", "(FLOOR(DATEDIFF (NOW(), users.data_nascimento) / 365.25)) as users_idade", $fields);

        $fields = str_replace('curso_referencia', 'COALESCE(cursos.referencia, "<i>Sem curso</i>") as curso_referencia', $fields);
        $fields = str_replace('trilha_referencia', 'COALESCE(trilhas.referencia, "<i>Sem trilha</i>") as trilha_referencia', $fields);
        $fields = str_replace('turma_referencia', 'COALESCE(turmas.nome, "<i>Sem turma</i>") as turma_referencia', $fields);
        $fields = str_replace('avaliacao_referencia', 'COALESCE(avaliacoes.referencia, "<i>Sem avaliação</i>") as avaliacao_referencia', $fields);
        $fields = str_replace('unidade_referencia', 'COALESCE(unidades.nome, "<i>Sem unidade</i>") as unidade_referencia', $fields);
        $fields = str_replace('responsaveis_nome', 'COALESCE(responsaveis.nome, "<i>Sem responsável</i>") as responsaveis_nome', $fields);
        $fields = str_replace('responsaveis_cpf', 'COALESCE(responsaveis.cpf, "<i>Não definido</i>") as responsaveis_cpf', $fields);
        $fields = str_replace('responsaveis_rg', 'COALESCE(responsaveis.rg, "<i>Não definido</i>") as responsaveis_rg', $fields);
        $fields = str_replace('responsaveis_celular', 'COALESCE(responsaveis.celular, "<i>Não definido</i>") as responsaveis_celular', $fields);
        $fields = str_replace('responsaveis_email', 'COALESCE(responsaveis.email, "<i>Não definido</i>") as responsaveis_email', $fields);
        $fields = str_replace('responsaveis_profissao', 'COALESCE(responsaveis.profissao, "<i>Não definido</i>") as responsaveis_profissao', $fields);
        $fields = str_replace('responsaveis_cep', 'COALESCE(responsaveis.cep, "<i>Não definido</i>") as responsaveis_cep', $fields);
        $fields = str_replace('responsaveis_rua', 'COALESCE(responsaveis.nome, "<i>Não definido</i>") as responsaveis_nome', $fields);
        $fields = str_replace('responsaveis_numero', 'COALESCE(responsaveis.numero, "<i>Não definido</i>") as responsaveis_numero', $fields);
        $fields = str_replace('responsaveis_bairro', 'COALESCE(responsaveis.bairro, "<i>Não definido</i>") as responsaveis_bairro', $fields);
        $fields = str_replace('responsaveis_cidade', 'COALESCE(responsaveis.cidade, "<i>Não definido</i>") as responsaveis_cidade', $fields);
        $fields = str_replace('responsaveis_estado', 'COALESCE(responsaveis.estado, "<i>Não definido</i>") as responsaveis_estado', $fields);
        $fields = str_replace('responsaveis_complemento', 'COALESCE(responsaveis.complemento, "<i>Não definido</i>") as responsaveis_complemento', $fields);
        $fields = str_replace('responsaveis_ponto_referencia', 'COALESCE(responsaveis.ponto_referencia, "<i>Não definido</i>") as responsaveis_ponto_referencia', $fields);

        $fields = str_replace('fluxo_caixa_data_vencimento', 'DATE_FORMAT(fluxo_caixa.data_vencimento, "%d/%m/%Y") as fluxo_caixa_data_vencimento', $fields);
        $fields = str_replace('fluxo_caixa_data_recebimento', 'DATE_FORMAT(fluxo_caixa.data_recebimento, "%d/%m/%Y") as fluxo_caixa_data_recebimento', $fields);
        $fields = str_replace('fluxo_caixa_data_estorno', 'DATE_FORMAT(fluxo_caixa.data_estorno, "%d/%m/%Y") as fluxo_caixa_data_estorno', $fields);
        $fields = str_replace('fluxo_caixa_data_cadastro', 'DATE_FORMAT(fluxo_caixa.created_at, "%d/%m/%Y") as fluxo_caixa_data_cadastro', $fields);

        $fields = str_replace('fluxo_caixa_n_parcela', 'CONCAT(fluxo_caixa.n_parcela,"/",fluxo_caixa.n_parcelas) as fluxo_caixa_n_parcela', $fields);

        $fields = str_replace('responsaveis_data_nascimento', 'DATE_FORMAT(responsaveis.data_nascimento, "%d/%m/%Y") as responsaveis_data_nascimento', $fields);

        $fields = str_replace("fluxo_caixa_status", "( CASE WHEN fluxo_caixa.status = 0 and CURDATE() > fluxo_caixa.data_vencimento THEN 'Inadimplente' WHEN fluxo_caixa.status = 0 and CURDATE() <= fluxo_caixa.data_vencimento THEN 'A receber' WHEN fluxo_caixa.status = 1 THEN 'Recebido' WHEN fluxo_caixa.status = 2 THEN 'Estornado' END ) as fluxo_caixa_status", $fields);

        $fields = str_replace('cadastrante_aluno', 'COALESCE(cadastrante_aluno.nome, "<i>Não definido</i>") as cadastrante_aluno', $fields);

        $fields = str_replace('categoria_aluno', 'COALESCE(categoriasa.nome, "<i>Sem categoria</i>") as categoria_aluno', $fields);


        $queryString = "
            SELECT
                ".$fields."
            FROM
                users
        ";

        $andFinal = null;

        if(in_array('fluxo_caixa.vendedor_nome', $camposSelected) || in_array('fluxo_caixa.cadastrante_nome', $camposSelected) || \Str::contains($fields, 'fluxo_caixa') == true || request('fin_status_link_pagamento') || request('fin_forma_pagamento') || request('fin_data_vencimento_de') || request('fin_data_vencimento_ate') || request('fin_status_pagamento') || request('fin_cadastrante_id') || request('fin_vendedor_id') || request('fin_produto_id') || request('fin_nome'))
        {
            $queryString .= " \n LEFT JOIN fluxo_caixa ON users.id = fluxo_caixa.cliente_id ";
        }

        $unidade_id = null;

        if(\Auth::user()->restringir_unidade == 'S' && \Auth::user()->unidade_id != null)
        {
            $unidade_id = \Auth::user()->unidade_id;
        
        } else {

            if(request('unidade_id'))
            {
                $unidade_id = request('unidade_id');
            }
        }

        if(in_array('unidade_referencia', $camposSelected))
        {
            $queryString .= " \n LEFT JOIN unidades ON users.unidade_id = unidades.id ";
        }

        if(in_array('categoria_aluno', $camposSelected) || request('categoria_id'))
        {
            $queryString .= " \n LEFT JOIN users_categorias as categoria_aluno ON users.id = categoria_aluno.user_id ";
            $queryString .= " \n LEFT JOIN categorias as categoriasa ON categoria_aluno.categoria_id = categoriasa.id ";
        }

        if(request('categoria_id'))
        {
            $filtrosAplicados->push(['column' => 'Categoria', 'value' => Categoria::select('nome')->plataforma()->tipo('aluno')->where('id', request('categoria_id'))->value('nome')]);

            $andFinal .= " \n AND categoria_aluno.categoria_id = " . request('categoria_id');
        }

        if($unidade_id != null || request('unidade_id'))
        {
            $filtrosAplicados->push(['column' => 'Unidade', 'value' => Unidade::select('nome')->plataforma()->where('id', $unidade_id)->value('nome')]);

            $andFinal .= " \n AND users.unidade_id = " . $unidade_id;
        }

        if(request('trilha_id') != null || request('trilha_tipo') != null || in_array('trilha_referencia', $camposSelected) || in_array('data_conclusao_trilha', $camposSelected) || in_array('data_matricula_trilha', $camposSelected))
        {
            $queryString .= " \n LEFT JOIN trilhas_alunos ON users.id = trilhas_alunos.aluno_id ";
            $queryString .= " \n LEFT JOIN trilhas ON trilhas_alunos.trilha_id = trilhas.id ";
        }
        if(request('trilha_id'))
        {
            $filtrosAplicados->push(['column' => 'Trilha', 'value' => Trilha::select('referencia')->plataforma()->where('id', request('trilha_id'))->value('referencia')]);

            $andFinal .= " \n AND trilhas_alunos.trilha_id = " . request('trilha_id');
        }
        if(request('trilha_tipo') == 'C')
        {
            $filtrosAplicados->push(['column' => 'Condição de conclusão de Trilha', 'value' => 'Que concluíram']);

            $andFinal .= " \n AND trilhas_alunos.data_conclusao IS NOT NULL ";
        }
        if(request('trilha_tipo') == 'NC')
        {
            $filtrosAplicados->push(['column' => 'Condição de conclusão de Trilha', 'value' => 'Que não concluíram']);

            $andFinal .= " \n AND trilhas_alunos.data_conclusao IS NULL ";
        }

        $checkJoin_turmas_alunos = false;
        $checkJoin_avaliacoes_alunos = false;

        $checkHasJoinTurmasAlunos = false;

        if(request('avaliacao_id') || in_array('avaliacoes_alunos.n_tentativa', $camposSelected) || in_array('data_avaliacao_finalizada', $camposSelected) || in_array('avaliacao_referencia', $camposSelected) || in_array('avaliacoes_alunos.nota', $camposSelected) || request('nota_avaliacao_tamanho') || request('avaliacao_tipo') || request('nota_avaliacao'))
        {
            $checkJoin_avaliacoes_alunos = true;

            $queryString .= " \n LEFT JOIN avaliacoes_alunos ON users.id = avaliacoes_alunos.aluno_id ";
            $queryString .= " \n LEFT JOIN avaliacoes ON avaliacoes_alunos.avaliacao_id = avaliacoes.id ";
        }

        if(in_array('cadastrante_aluno', $camposSelected))
        {
            $queryString .= " \n LEFT JOIN users as cadastrante_aluno ON users.cadastrante_id = cadastrante_aluno.id ";
        }

        if(in_array('fluxo_caixa.cadastrante_nome', $camposSelected))
        {
            $queryString .= " \n LEFT JOIN users as cadastrante ON fluxo_caixa.cadastrante_id = cadastrante.id ";
        }

        if(in_array('fluxo_caixa.vendedor_nome', $camposSelected))
        {
            $queryString .= " \n LEFT JOIN users as vendedor ON fluxo_caixa.vendedor_id = vendedor.id ";
        }

        if(request('curso_id') || in_array('curso_referencia', $camposSelected) || in_array('data_ultimo_acesso_curso', $camposSelected) || in_array('users_data_conclusao_curso', $camposSelected) || request('curso_data_conclusao_de') || request('curso_data_conclusao_ate') || request('curso_data_ultimo_acesso_de') || request('curso_data_ultimo_acesso_ate'))
        {
            if($checkJoin_avaliacoes_alunos == true)
            {
                $queryString .= " \n JOIN turmas_alunos ON avaliacoes_alunos.aluno_id = turmas_alunos.aluno_id ";
                $queryString .= " \n JOIN cursos ON turmas_alunos.curso_id = cursos.id ";

            } else {

                $queryString .= " \n JOIN turmas_alunos ON users.id = turmas_alunos.aluno_id ";
                $queryString .= " \n JOIN cursos ON turmas_alunos.curso_id = cursos.id ";
            }

            $checkHasJoinTurmasAlunos = true;
        }

        if(request('turma_id') || in_array('turma_referencia', $camposSelected) || in_array('data_matricula_turma', $camposSelected))
        {  
            if($checkHasJoinTurmasAlunos == false)
            {
                if($checkJoin_avaliacoes_alunos == false)
                {
                    $queryString .= " \n JOIN turmas_alunos ON users.id = turmas_alunos.aluno_id ";

                } else {

                    $queryString .= " \n JOIN turmas_alunos ON avaliacoes_alunos.aluno_id = turmas_alunos.aluno_id ";
                }
            }

            $queryString .= " \n JOIN turmas ON turmas_alunos.turma_id = turmas.id ";
        }

        if(request('avaliacao_tipo') == 'C')
        {
            $filtrosAplicados->push(['column' => 'Condição de Status de Avaliação', 'value' => 'Que finalizaram']);

            $andFinal .= " \n AND avaliacoes_alunos.data_finalizada IS NOT NULL ";
        }
        if(request('avaliacao_tipo') == 'NC')
        {
            $filtrosAplicados->push(['column' => 'Condição de Status de Avaliação', 'value' => 'Que não finalizaram']);

            $andFinal .= " \n AND avaliacoes_alunos.data_finalizada IS NULL ";
        }

        if(request('nota_avaliacao_tamanho') == 'nota_maior_que')
        {
            $filtrosAplicados->push(['column' => 'Condição de Nota de Avaliação', 'value' => 'Maior ou igual a ' . request('nota_avaliacao')]);

            $nota = floatval(str_replace(',','.', request('nota_avaliacao')));

            $andFinal .= " \n AND CAST(avaliacoes_alunos.nota AS DECIMAL) >= " . $nota;
        }
        if(request('nota_avaliacao_tamanho') == 'nota_menor_que')
        {
            $filtrosAplicados->push(['column' => 'Condição de Nota de Avaliação', 'value' => 'Menor ou igual a ' . request('nota_avaliacao')]);

            $nota = floatval(str_replace(',','.', request('nota_avaliacao')));

            $andFinal .= " \n AND CAST(avaliacoes_alunos.nota AS DECIMAL) <= " . $nota;
        }


        if(request('turma_id'))
        {  
            $filtrosAplicados->push(['column' => 'Turma', 'value' => Turma::select('nome')->plataforma()->where('id', request('turma_id'))->value('nome')]);

            $andFinal .= " \n AND turmas.id = " . request('turma_id');
        }

        if(request('avaliacao_id'))
        {  
            $filtrosAplicados->push(['column' => 'Avaliação', 'value' => Avaliacao::select('referencia')->plataforma()->where('id', request('avaliacao_id'))->value('referencia')]);

            $andFinal .= " \n AND avaliacoes.id = " . request('avaliacao_id');
        }

        if(request('curso_id'))
        {
            $filtrosAplicados->push(['column' => 'Curso', 'value' => Curso::select('referencia')->plataforma()->where('id', request('curso_id'))->value('referencia')]);

            $andFinal .= " \n AND turmas_alunos.curso_id = " . request('curso_id');
        }
        if(request('curso_tipo') == 'C')
        {
            $filtrosAplicados->push(['column' => 'Condição de conclusão de Curso', 'value' => 'Que concluíram']);

            $andFinal .= " \n AND turmas_alunos.data_conclusao_curso IS NOT NULL ";
        }
        if(request('curso_tipo') == 'NC')
        {
            $filtrosAplicados->push(['column' => 'Condição de conclusão de Curso', 'value' => 'Que não concluíram']);

            $andFinal .= " \n AND turmas_alunos.data_conclusao_curso IS NULL ";
        }


        if(\Str::contains($fields, 'responsaveis') == true)
        {
            $queryString .= " \n LEFT JOIN responsaveis_alunos ON users.id = responsaveis_alunos.aluno_id ";
            $queryString .= " \n LEFT JOIN responsaveis ON responsaveis_alunos.responsavel_id = responsaveis.id ";
        }


        $endereco = [
            'cep'              => request('cep'),
            'rua'              => request('rua'),
            'numero'           => request('numero'),
            'bairro'           => request('bairro'),
            'cidade'           => request('cidade'),
            'estado'           => request('estado'),
            'complemento'      => request('complemento'),
            'ponto_referencia' => request('ponto_referencia'),
        ];

        if(\Str::contains($fields, 'endereco') == true || count(array_filter($endereco)) > 0)
        {
            $queryString .= " \n LEFT JOIN users_enderecos as endereco ON users.id = endereco.user_id ";
        }

        if(request('nome'))
        {
            $filtrosAplicados->push(['column' => 'Nome', 'value' => request('nome')]);

            $andFinal .= " \n AND users.nome LIKE '%" . request('nome') . "%' ";
        }

        if(request('email'))
        {
            $filtrosAplicados->push(['column' => 'Email', 'value' => request('email')]);

            $andFinal .= " \n AND users.email = '" . request('email') . "' ";
        }

        if(request('cpf'))
        {
            $filtrosAplicados->push(['column' => 'CPF', 'value' => request('cpf')]);

            $andFinal .= " \n AND users.cpf = '" . request('cpf') . "' ";
        }

        if(request('matricula'))
        {
            $filtrosAplicados->push(['column' => 'Matrícula', 'value' => request('matricula')]);

            $andFinal .= " \n AND users.id = " . request('matricula');
        }

        if(request('rg'))
        {
            $filtrosAplicados->push(['column' => 'RG', 'value' => request('rg')]);

            $andFinal .= " \n AND users.rg = '" . request('rg') . "' ";
        }

        if(request('genero'))
        {
            $filtrosAplicados->push(['column' => 'Gênero', 'value' => request('genero')]);

            $andFinal .= " \n AND users.genero = '" . request('genero') . "' ";
        }

        if(request('profissao'))
        {
            $filtrosAplicados->push(['column' => 'Profissão', 'value' => request('profissao')]);

            $andFinal .= " \n AND users.profissao LIKE '%" . request('profissao') . "%' ";
        }

        if(request('obs'))
        {
            $filtrosAplicados->push(['column' => 'Observações', 'value' => request('obs')]);

            $andFinal .= " \n AND users.obs LIKE '%" . request('obs') . "%' ";
        }

        if(request('juridico_status_doc') == 'C')
        {
            $filtrosAplicados->push(['column' => 'Documentação', 'value' => 'Que já enviaram todos os documentos']);

            $andFinal .= " \n AND users.data_docs_recebidas IS NOT NULL ";
        }

        if(request('juridico_status_doc') == 'NC')
        {
            $filtrosAplicados->push(['column' => 'Documentação', 'value' => 'Que ainda não enviaram todos os documentos']);

            $andFinal .= " \n AND users.data_docs_recebidas IS NULL ";
        }

        if(request('status') == 1)
        {
            $filtrosAplicados->push(['column' => 'Status de Matrícula', 'value' => 'Desativados']);

            $andFinal .= " \n AND users.status = 1 ";
        
        } else {

            $filtrosAplicados->push(['column' => 'Status de Matrícula', 'value' => 'Ativos']);

            $andFinal .= " \n AND users.status = 0 ";
        }

        if(request('celular'))
        {
            $filtrosAplicados->push(['column' => 'Celular', 'value' => request('celular')]);

            $andFinal .= " \n AND users.celular = '" . request('celular') . "' ";
        }

        if(request('telefone'))
        {
            $filtrosAplicados->push(['column' => 'Telefone', 'value' => request('telefone')]);

            $andFinal .= " \n AND users.telefone = '" . request('telefone') . "' ";
        }
                            
        if(request('data_cadastro_de'))
        {
            $filtrosAplicados->push(['column' => 'Período de Data de Cadastro Inicial', 'value' => Util::replaceDatePt(request('data_cadastro_de'))]);

            $andFinal .= " \n AND users.created_at >= '" . request('data_cadastro_de') . "' ";
        }

        if(request('data_cadastro_ate'))
        {
            $filtrosAplicados->push(['column' => 'Período de Data de Cadastro Final', 'value' => Util::replaceDatePt(request('data_cadastro_ate'))]);

            $andFinal .= " \n AND users.created_at <= '" . request('data_cadastro_ate') . "' ";
        }

        if(request('dias_sem_acessar'))
        {
            $filtrosAplicados->push(['column' => 'Que não acessam há mais de X dias', 'value' => request('dias_sem_acessar')]);

            $andFinal .= " \n AND FLOOR(DATEDIFF (NOW(), users.data_ultimo_acesso)) >= " . request('dias_sem_acessar');
        }

        if(request('data_ultimo_acesso_de'))
        {
            $filtrosAplicados->push(['column' => 'Período de Data de Último Acesso Inicial', 'value' => Util::replaceDatePt(request('data_ultimo_acesso_de'))]);

            $andFinal .= " \n AND users.data_ultimo_acesso >= " . request('data_ultimo_acesso_de');
        }
        if(request('data_ultimo_acesso_ate'))
        {
            $filtrosAplicados->push(['column' => 'Período de Data de Último Acesso Final', 'value' => Util::replaceDatePt(request('data_ultimo_acesso_ate'))]);

            $andFinal .= " \n AND users.data_ultimo_acesso <= " . request('data_ultimo_acesso_ate');
        }

        if(request('trilha_data_conclusao_de'))
        {
            $filtrosAplicados->push(['column' => 'Período de Data de Conclusão de Trilha Inicial', 'value' => Util::replaceDatePt(request('trilha_data_conclusao_de'))]);

            $andFinal .= " \n AND trilhas_alunos.data_conclusao >= " . request('trilha_data_conclusao_de');
        }
        if(request('trilha_data_conclusao_ate'))
        {
            $filtrosAplicados->push(['column' => 'Período de Data de Conclusão de Trilha Final', 'value' => Util::replaceDatePt(request('trilha_data_conclusao_ate'))]);

            $andFinal .= " \n AND trilhas_alunos.data_conclusao <= " . request('trilha_data_conclusao_ate');
        }

        if(request('curso_data_conclusao_de'))
        {
            $filtrosAplicados->push(['column' => 'Período de Data de Conclusão de Curso Inicial', 'value' => Util::replaceDatePt(request('curso_data_conclusao_de'))]);

            $andFinal .= " \n AND turmas_alunos.data_conclusao_curso >= " . request('curso_data_conclusao_de');
        }
        if(request('curso_data_conclusao_ate'))
        {
            $filtrosAplicados->push(['column' => 'Período de Data de Conclusão de Curso Final', 'value' => Util::replaceDatePt(request('curso_data_conclusao_ate'))]);

            $andFinal .= " \n AND turmas_alunos.data_conclusao_curso <= " . request('curso_data_conclusao_ate');
        }

        if(request('data_docs_recebidas_de'))
        {
            $filtrosAplicados->push(['column' => 'Período de Data de Conclusão de Documentação Inicial', 'value' => Util::replaceDatePt(request('data_docs_recebidas_de'))]);

            $andFinal .= " \n AND users.data_docs_recebidas >= " . request('data_docs_recebidas_de');
        }
        if(request('data_docs_recebidas_ate'))
        {
            $filtrosAplicados->push(['column' => 'Período de Data de Conclusão de Documentação Final', 'value' => Util::replaceDatePt(request('data_docs_recebidas_ate'))]);

            $andFinal .= " \n AND users.data_docs_recebidas <= " . request('data_docs_recebidas_ate');
        }

        if(request('mes_nascimento'))
        {
            $allMeses = Util::getMeses();
            $filtrosAplicados->push(['column' => 'Mês de Nascimento', 'value' => $allMeses[request('mes_nascimento')]]);

            $andFinal .= " \n AND MONTH(users.data_nascimento) = " . request('mes_nascimento');
        }

        if(request('dia_nascimento_de'))
        {
            $filtrosAplicados->push(['column' => 'Período de Dia de Nascimento Inicial', 'value' => request('dia_nascimento_de')]);

            $andFinal .= " \n AND DAY(users.data_nascimento) >= " . request('dia_nascimento_de');
        }

        if(request('dia_nascimento_ate'))
        {
            $filtrosAplicados->push(['column' => 'Período de Dia de Nascimento Final', 'value' => request('dia_nascimento_ate')]);

            $andFinal .= " \n AND DAY(users.data_nascimento) >= " . request('dia_nascimento_ate');
        }

        if(request('faixa_etaria_de'))
        {
            $filtrosAplicados->push(['column' => 'Período de Faixa Etária Inicial', 'value' => request('faixa_etaria_de') . ' anos']);

            $andFinal .= " \n AND FLOOR(DATEDIFF (NOW(), users.data_nascimento) / 365.25) >= " . request('faixa_etaria_de');
        }

        if(request('faixa_etaria_ate'))
        {
            $filtrosAplicados->push(['column' => 'Período de Faixa Etária Final', 'value' => request('faixa_etaria_ate') . ' anos']);

            $andFinal .= " \n AND FLOOR(DATEDIFF (NOW(), users.data_nascimento) / 365.25) <= " . request('faixa_etaria_ate');
        }

        if(request('cep'))
        {
            $filtrosAplicados->push(['column' => 'CEP', 'value' => request('cep')]);

            $andFinal .= " \n AND endereco.cep = " . request('cep');
        }

        if(request('rua'))
        {
            $filtrosAplicados->push(['column' => 'Rua', 'value' => request('rua')]);

            $andFinal .= " \n AND endereco.rua = " . request('rua');
        }

        if(request('numero'))
        {
            $filtrosAplicados->push(['column' => 'Número', 'value' => request('numero')]);

            $andFinal .= " \n AND endereco.numero = '" . request('numero') . "' ";
        }

        if(request('bairro'))
        {
            $filtrosAplicados->push(['column' => 'Bairro', 'value' => request('bairro')]);

            $andFinal .= " \n AND endereco.bairro = '" . request('bairro') . "' ";
        }

        if(request('cidade'))
        {
            $filtrosAplicados->push(['column' => 'Cidade', 'value' => request('cidade')]);

            $andFinal .= " \n AND endereco.cidade = '" . request('cidade') . "' ";
        }

        if(request('estado'))
        {
            $filtrosAplicados->push(['column' => 'Estado', 'value' => request('estado')]);

            $andFinal .= " \n AND endereco.estado = '" . request('estado') . "' ";
        }

        if(request('complemento'))
        {
            $filtrosAplicados->push(['column' => 'Complemento', 'value' => request('complemento')]);

            $andFinal .= " \n AND endereco.complemento = '" . request('complemento') . "' ";
        }

        if(request('ponto_referencia'))
        {
            $filtrosAplicados->push(['column' => 'Ponto de Referência', 'value' => request('ponto_referencia')]);

            $andFinal .= " \n AND endereco.ponto_referencia = '" . request('ponto_referencia') . "' ";
        }

        if(request('fin_nome'))
        {
            $filtrosAplicados->push(['column' => 'Referência', 'value' => request('fin_nome')]);

            $andFinal .= " \n AND (fluxo_caixa.cliente_nome LIKE '%" . request('fin_nome') . "%' OR fluxo_caixa.valor_parcela LIKE '%" . request('fin_nome') . "%' )";
        }

        if(request('fin_produto_id'))
        {
            $dados_produto = request('fin_produto_id');
            $dados_produto = explode('-', $dados_produto);
            $tipo_produto = $dados_produto[0];
            $produto_id = $dados_produto[1];

            if($tipo_produto == 'A')
            {
                $filtrosAplicados->push(['column' => 'Produto', 'value' => 'Plano: ' . Plano::select('nome')->plataforma()->where('id', $produto_id)->value('nome')]);
            }

            if($tipo_produto == 'S')
            {
                $filtrosAplicados->push(['column' => 'Produto', 'value' => 'Servico: ' . Servico::select('nome')->plataforma()->where('id', $produto_id)->value('nome')]);
            }

            if($tipo_produto == 'P')
            {
                $filtrosAplicados->push(['column' => 'Produto', 'value' => 'Produto: ' . Produto::select('nome')->plataforma()->where('id', $produto_id)->value('nome')]);
            }

            if($tipo_produto == 'T')
            {
                $filtrosAplicados->push(['column' => 'Produto', 'value' => 'Trilha: ' . Trilha::select('referencia')->plataforma()->where('id', $produto_id)->value('referencia')]);
            }

            if($tipo_produto == 'C')
            {
                $filtrosAplicados->push(['column' => 'Produto', 'value' => 'Turma: ' . Turma::select('nome')->plataforma()->where('id', $produto_id)->value('nome')]);
            }

            $andFinal .= " \n AND tipo_produto = '".$tipo_produto."' AND fluxo_caixa.registro_id = " . $produto_id;
        }

        if(request('fin_vendedor_id'))
        {
            $filtrosAplicados->push(['column' => 'Vendedor', 'value' => User::select('nome')->plataforma()->where('id', request('fin_vendedor_id'))->value('nome')]);

            $andFinal .= " \n AND fluxo_caixa.vendedor_id = " . request('fin_vendedor_id');
        }

        if(request('fin_cadastrante_id'))
        {
            $filtrosAplicados->push(['column' => 'Cadastrante', 'value' => User::select('nome')->plataforma()->where('id', request('fin_cadastrante_id'))->value('nome')]);

            $andFinal .= " \n AND fluxo_caixa.cadastrante_id = " . request('fin_cadastrante_id');
        }

        if(request('fin_status_pagamento'))
        {
            if(request('fin_status_pagamento') == 'A receber')
            {   
                $andFinal .= " \n AND fluxo_caixa.status = 0 AND fluxo_caixa.data_vencimento >= '" . date('Y-m-d') . "' ";
            }
            if(request('fin_status_pagamento') == 'Inadimplentes')
            {
                $andFinal .= " \n AND fluxo_caixa.status = 0 AND fluxo_caixa.data_vencimento < '" . date('Y-m-d') . "' ";
            }

            if(request('fin_status_pagamento') == 'Recebidos')
            {
                $andFinal .= " \n AND fluxo_caixa.status = 1 ";
            }

            if(request('fin_status_pagamento') == 'Estornados')
            {
                $andFinal .= " \n AND fluxo_caixa.status = 2 ";
            }

            $filtrosAplicados->push(['column' => 'Status de Pagamento', 'value' => request('fin_status_pagamento') ]);
        }

        if(request('fin_data_vencimento_de'))
        {
            $filtrosAplicados->push(['column' => 'Período de Data de Vencimento Inicial', 'value' => Util::replaceDatePt(request('fin_data_vencimento_de'))]);

            $andFinal .= " \n AND fluxo_caixa.data_vencimento >= " . request('fin_data_vencimento_de');
        }
        if(request('fin_data_vencimento_ate'))
        {
            $filtrosAplicados->push(['column' => 'Período de Data de Vencimento Final', 'value' => Util::replaceDatePt(request('fin_data_vencimento_ate'))]);

            $andFinal .= " \n AND fluxo_caixa.data_vencimento <= " . request('fin_data_vencimento_ate');
        }

        if(request('fin_forma_pagamento'))
        {
            $filtrosAplicados->push(['column' => 'Forma de Pagamento', 'value' => request('fin_forma_pagamento')]);

            $andFinal .= " \n AND fluxo_caixa.forma_pagamento = '" . request('fin_forma_pagamento') . "' ";
        }

        if(request('fin_status_link_pagamento') == 'C')
        {
            $filtrosAplicados->push(['column' => 'Link de Pagamento', 'value' => 'Com Link']);

            $andFinal .= " \n AND fluxo_caixa.link IS NOT NULL ";
        }
        if(request('fin_status_link_pagamento') == 'S')
        {
            $filtrosAplicados->push(['column' => 'Link de Pagamento', 'value' => 'Sem Link']);

            $andFinal .= " \n AND fluxo_caixa.link IS NULL ";
        }


        $queryString .= " \n WHERE users.plataforma_id = ".$plataforma_id." \n AND users.tipo = 'aluno' ";

        $queryString .= " \n " . $andFinal;
        $queryString .= " \n ORDER BY users.nome ";

        // echo nl2br($queryString); die();

        $query = $this->paginateArray(
            DB::select( DB::raw($queryString))
        ); 

        $countQuery = $query->countQuery;

        $allCampos = $this->getCampos();

        return view('admin.relatorio.buscar', compact('query','queryString','countQuery','camposSelected','filtrosAplicados','allCampos','getNomePaginaInterno','route') );
    }

    public function paginateArray($data, $perPage = 10)
    {
        $page = Paginator::resolveCurrentPage();
        $total = count($data);
        $results = array_slice($data, ($page - 1) * $perPage, $perPage);
        return new LengthAwarePaginator($results, $total, $perPage, $page, [
            'path' => Paginator::resolveCurrentPath(),
            'countQuery' => $total,
        ]);
    }

    public function buscarContatos()
    {
        $getNomePaginaInterno = 'Resultado de Relatórios Gerais de Contatos';

        $route = '/admin/contato/info';

        $filtrosAplicados = collect();

        $camposSelected = request('camposContatos');
        $fields = 'contatos.id, '. implode(', ', $camposSelected);

        $queryString = "
            SELECT
                ".$fields."
            FROM
                contatos
        ";

        $andFinal = null;

        $unidade_id = null;

        if(\Auth::user()->restringir_unidade == 'S' && \Auth::user()->unidade_id != null)
        {
            $unidade_id = \Auth::user()->unidade_id;
        
        } else {

            if(request('unidade_id'))
            {
                $unidade_id = request('unidade_id');
            }
        }

        if(request('contatos_condicao_perfil') == 'C')
        {
            $filtrosAplicados->push(['column' => 'Condição de Perfil', 'value' => 'Apenas os que Converteram em Aluno']);

            $queryString .= " \n JOIN users ON contatos.email = users.email ";

            $andFinal .= " \n AND users.id IS NOT NULL ";
        }

        if(request('contatos_condicao_perfil') == 'NC')
        {
            $filtrosAplicados->push(['column' => 'Condição de Perfil', 'value' => 'Apenas os que Não Converteram em Aluno']);

            $queryString .= " \n JOIN users ON contatos.email = users.email ";

            $andFinal .= " \n AND users.id IS NULL ";
        }

        if(request('contatos_tipo') == 'L')
        {
            $filtrosAplicados->push(['column' => 'Tipo', 'value' => 'Lead']);

            $andFinal .= " \n AND contatos.tipo = '" . request('contatos_tipo') . "' ";
        
        } else {

            $filtrosAplicados->push(['column' => 'Tipo', 'value' => 'Suporte']);

            $andFinal .= " \n AND contatos.tipo = '" . request('contatos_tipo') . "' ";
        }

        if(request('contatos_arquivado') == 'N')
        {
            $filtrosAplicados->push(['column' => 'Arquivados', 'value' => 'Não']);

            $andFinal .= " \n AND contatos.arquivado = '" . request('contatos_arquivado') . "' ";
        
        } else {

            $filtrosAplicados->push(['column' => 'Arquivados', 'value' => 'Sim']);

            $andFinal .= " \n AND contatos.arquivado = '" . request('contatos_arquivado') . "' ";
        }

        if(in_array('categoria_contato', $camposSelected) || request('categoria_id'))
        {
            $queryString .= " \n LEFT JOIN contatos_categorias as categoria_contato ON contatos.id = categoria_contato.contato_id ";
            $queryString .= " \n LEFT JOIN categorias as categoriasc ON categoria_contato.categoria_id = categoriasc.id ";
        }

        if(request('categoria_id'))
        {
            $filtrosAplicados->push(['column' => 'Categoria', 'value' => Categoria::select('nome')->plataforma()->tipo('contato')->where('id', request('categoria_id'))->value('nome')]);

            $andFinal .= " \n AND categoria_contato.categoria_id = " . request('categoria_id');
        }

        if(in_array('unidade_referencia', $camposSelected))
        {
            $queryString .= " \n LEFT JOIN unidades ON unidades.id = contatos.unidade_id ";
        }

        if($unidade_id != null)
        {
            $filtrosAplicados->push(['column' => 'Unidade', 'value' => Unidade::select('nome')->plataforma()->where('id', $unidade_id)->value('nome')]);

            $andFinal .= " \n AND contatos.unidade_id = " . $unidade_id;
        }

        if(request('contatos_status') !== null)
        {
            if(request('contatos_status') == 0)
            {
                $status = 'Pendente';
            }
            if(request('contatos_status') == 1)
            {
                $status = 'Em análise';
            }
            if(request('contatos_status') == 2)
            {
                $status = 'Atendidos';
            }
            if(request('contatos_status') == 3)
            {
                $status = 'Desativados';
            }

            $filtrosAplicados->push(['column' => 'Status de Atendimento', 'value' => $status]);

            $andFinal .= " \n AND contatos.status = " . request('contatos_status');
        }

        if(request('trilha_id') || in_array('trilha_referencia', $camposSelected))
        {
            $queryString .= " \n LEFT JOIN trilhas ON contatos.trilha_id = trilhas.id ";
        }
        if(request('trilha_id'))
        {
            $filtrosAplicados->push(['column' => 'Trilha', 'value' => Trilha::select('referencia')->plataforma()->where('id', request('trilha_id'))->value('referencia')]);

            $andFinal .= " \n AND contatos.trilha_id = " . request('trilha_id');
        }

        if(request('curso_id') || in_array('curso_referencia', $camposSelected))
        {
            $queryString .= " \n JOIN cursos ON contatos.curso_id = cursos.id ";
        }

        if(request('turma_id') || in_array('turma_referencia', $camposSelected))
        {
            $queryString .= " \n JOIN turmas ON contatos.turma_id = turmas.id ";
        }

        if(request('formulario_id') || in_array('formulario_referencia', $camposSelected))
        {
            $queryString .= " \n LEFT JOIN formularios ON contatos.formulario_id = formularios.id ";
        }

        if(request('formulario_id'))
        {
            $andFinal .= " \n AND contatos.formulario_id = " . request('formulario_id');
        }

        if(request('turma_id'))
        {  
            $filtrosAplicados->push(['column' => 'Turma', 'value' => Turma::select('nome')->plataforma()->where('id', request('turma_id'))->value('nome')]);

            $andFinal .= " \n AND contatos.turma_id = " . request('turma_id');
        }

        if(request('curso_id'))
        {
            $filtrosAplicados->push(['column' => 'Curso', 'value' => Curso::select('referencia')->plataforma()->where('id', request('curso_id'))->value('referencia')]);

            $andFinal .= " \n AND contatos.curso_id = " . request('curso_id');
        }

        if(request('contatos_nome'))
        {
            $filtrosAplicados->push(['column' => 'Nome', 'value' => request('contatos_nome')]);

            $andFinal .= " \n AND contatos.nome LIKE '%" . request('contatos_nome') . "%' ";
        }

        if(request('contatos_email'))
        {
            $filtrosAplicados->push(['column' => 'Email', 'value' => request('contatos_email')]);

            $andFinal .= " \n AND contatos.email = '" . request('contatos_email') . "' ";
        }

        if(request('contatos_cpf'))
        {
            $filtrosAplicados->push(['column' => 'CPF', 'value' => request('contatos_cpf')]);

            $andFinal .= " \n AND contatos.cpf = '" . request('contatos_cpf') . "' ";
        }

        if(request('contatos_genero'))
        {
            $filtrosAplicados->push(['column' => 'Gênero', 'value' => request('contatos_genero')]);

            $andFinal .= " \n AND contatos.genero = '" . request('contatos_genero') . "' ";
        }

        if(request('contatos_mensagem'))
        {
            $filtrosAplicados->push(['column' => 'Mensagem', 'value' => request('contatos_mensagem')]);

            $andFinal .= " \n AND contatos.mensagem LIKE '%" . request('contatos_mensagem') . "%' ";
        }

        if(request('contatos_celular'))
        {
            $filtrosAplicados->push(['column' => 'Celular', 'value' => request('contatos_celular')]);

            $andFinal .= " \n AND contatos.celular = '" . request('contatos_celular') . "' ";
        }

        if(request('contatos_telefone'))
        {
            $filtrosAplicados->push(['column' => 'Telefone', 'value' => request('contatos_telefone')]);

            $andFinal .= " \n AND contatos.telefone = '" . request('contatos_telefone') . "' ";
        }
                            
        if(request('contatos_data_cadastro_de'))
        {
            $filtrosAplicados->push(['column' => 'Período de Data de Cadastro Inicial', 'value' => Util::replaceDatePt(request('data_cadastro_de'))]);

            $andFinal .= " \n AND contatos.created_at >= '" . request('contatos_data_cadastro_de') . "' ";
        }

        if(request('contatos_data_cadastro_ate'))
        {
            $filtrosAplicados->push(['column' => 'Período de Data de Cadastro Final', 'value' => Util::replaceDatePt(request('data_cadastro_ate'))]);

            $andFinal .= " \n AND contatos.created_at <= '" . request('contatos_data_cadastro_ate') . "' ";
        }

        if(request('contatos_mes_nascimento'))
        {
            $allMeses = Util::getMeses();
            $filtrosAplicados->push(['column' => 'Mês de Nascimento', 'value' => $allMeses[request('mes_nascimento')]]);

            $andFinal .= " \n AND MONTH(contatos.data_nascimento) = " . request('contatos_mes_nascimento');
        }

        if(request('contatos_dia_nascimento_de'))
        {
            $filtrosAplicados->push(['column' => 'Período de Dia de Nascimento Inicial', 'value' => request('contatos_dia_nascimento_de')]);

            $andFinal .= " \n AND DAY(contatos.data_nascimento) >= " . request('contatos_dia_nascimento_de');
        }

        if(request('contatos_dia_nascimento_ate'))
        {
            $filtrosAplicados->push(['column' => 'Período de Dia de Nascimento Final', 'value' => request('contatos_dia_nascimento_ate')]);

            $andFinal .= " \n AND DAY(contatos.data_nascimento) <= " . request('contatos_dia_nascimento_ate');
        }

        if(request('contatos_faixa_etaria_de'))
        {
            $filtrosAplicados->push(['column' => 'Período de Faixa Etária Inicial', 'value' => request('contatos_faixa_etaria_de') . ' anos']);

            $andFinal .= " \n AND FLOOR(DATEDIFF (NOW(), contatos.data_nascimento) / 365.25) >= " . request('contatos_faixa_etaria_de');
        }

        if(request('contatos_faixa_etaria_ate'))
        {
            $filtrosAplicados->push(['column' => 'Período de Faixa Etária Final', 'value' => request('contatos_faixa_etaria_ate') . ' anos']);

            $andFinal .= " \n AND FLOOR(DATEDIFF (NOW(), contatos.data_nascimento) / 365.25) <= " . request('contatos_faixa_etaria_ate');
        }

        if(request('contatos_cep'))
        {
            $filtrosAplicados->push(['column' => 'CEP', 'value' => request('contatos_cep')]);

            $andFinal .= " \n AND contatos.cep = '" . request('contatos_cep') . "' ";
        }

        if(request('contatos_rua'))
        {
            $filtrosAplicados->push(['column' => 'Rua', 'value' => request('contatos_rua')]);

            $andFinal .= " \n AND contatos.rua = '" . request('contatos_rua') . "' ";
        }

        if(request('contatos_numero'))
        {
            $filtrosAplicados->push(['column' => 'Número', 'value' => request('contatos_numero')]);

            $andFinal .= " \n AND contatos.numero = '" . request('contatos_numero') . "' ";
        }

        if(request('contatos_bairro'))
        {
            $filtrosAplicados->push(['column' => 'Bairro', 'value' => request('contatos_bairro')]);

            $andFinal .= " \n AND contatos.bairro = '" . request('contatos_bairro') . "' ";
        }

        if(request('contatos_cidade'))
        {
            $filtrosAplicados->push(['column' => 'Cidade', 'value' => request('contatos_cidade')]);

            $andFinal .= " \n AND contatos.cidade = '" . request('contatos_cidade') . "' ";
        }

        if(request('contatos_estado'))
        {
            $filtrosAplicados->push(['column' => 'Estado', 'value' => request('contatos_estado')]);

            $andFinal .= " \n AND contatos.estado = '" . request('contatos_estado') . "' ";
        }

        if(request('contatos_complemento'))
        {
            $filtrosAplicados->push(['column' => 'Complemento', 'value' => request('contatos_complemento')]);

            $andFinal .= " \n AND contatos.complemento = '" . request('contatos_complemento') . "' ";
        }

        if(request('contatos_ponto_referencia'))
        {
            $filtrosAplicados->push(['column' => 'Ponto de Referência', 'value' => request('contatos_ponto_referencia')]);

            $andFinal .= " \n AND contatos.ponto_referencia = '" . request('contatos_ponto_referencia') . "' ";
        }

        $fields = str_replace('contatos_data_cadastro', 'DATE_FORMAT(contatos.created_at, "%d/%m/%Y %H:%i") as contatos_data_cadastro', $fields);
        $fields = str_replace('contatos_data_nascimento', 'DATE_FORMAT(contatos.data_nascimento, "%d/%m/%Y") as contatos_data_nascimento', $fields);
        $fields = str_replace("contatos_status", "(CASE WHEN contatos.status = 0 THEN 'Pendente' WHEN contatos.status = 1 THEN 'Em análise' WHEN contatos.status = 2 THEN 'Atendido' WHEN contatos.status = 3 THEN 'Desativado' END) as contatos_status", $fields);
        $fields = str_replace("contatos_idade", "(FLOOR(DATEDIFF (NOW(), contatos.data_nascimento) / 365.25)) as contatos_idade", $fields);

        $fields = str_replace('curso_referencia', 'COALESCE(cursos.referencia, "<i>Sem curso</i>") as curso_referencia', $fields);
        $fields = str_replace('trilha_referencia', 'COALESCE(trilhas.referencia, "<i>Sem trilha</i>") as trilha_referencia', $fields);
        $fields = str_replace('turma_referencia', 'COALESCE(turmas.nome, "<i>Sem turma</i>") as turma_referencia', $fields);
        $fields = str_replace('unidade_referencia', 'COALESCE(unidades.nome, "<i>Sem unidade</i>") as unidade_referencia', $fields);
        $fields = str_replace('formulario_referencia', 'COALESCE(formularios.referencia, "<i>Sem formulário</i>") as formulario_referencia', $fields);
        $fields = str_replace('categoria_contato', 'COALESCE(categoriasc.nome, "<i>Sem categoria</i>") as categoria_contato', $fields);


        $queryString .= " \n WHERE contatos.plataforma_id = ".\Auth::user()->plataforma_id." \n ";

        $queryString .= " \n " . $andFinal;
        $queryString .= " \n ORDER BY contatos.nome ";

        $query = $this->paginateArray(
            DB::select( DB::raw($queryString))
        ); 

        $countQuery = $query->countQuery;


        $allCampos = $this->getCamposContatos();

        return view('admin.relatorio.buscar', compact('query','countQuery','camposSelected','queryString','filtrosAplicados','allCampos','getNomePaginaInterno','route') );
    }



    public function getCampos()
    {
        $campos = [
            ['select' => 'users.nome',                      'nome' => 'Nome'],
            ['select' => 'users.email',                     'nome' => 'Email'],
            ['select' => 'users.cpf',                       'nome' => 'CPF'],
            ['select' => 'users.id',                        'nome' => 'Matrícula'],
            ['select' => 'users.rg',                        'nome' => 'RG'],
            ['select' => 'users.genero',                    'nome' => 'Gênero'],
            ['select' => 'users.profissao',                 'nome' => 'Profissão'],
            ['select' => 'users.obs',                       'nome' => 'Observações'],
            ['select' => 'users_status',                    'nome' => 'Status de Matrícula'],
            ['select' => 'users.celular',                   'nome' => 'Celular'],
            ['select' => 'users.telefone',                  'nome' => 'Telefone'],
            ['select' => 'endereco.cep',                    'nome' => 'CEP'],
            ['select' => 'endereco.rua',                    'nome' => 'Rua'],
            ['select' => 'endereco.numero',                 'nome' => 'Número'],
            ['select' => 'endereco.bairro',                 'nome' => 'Bairro'],
            ['select' => 'endereco.cidade',                 'nome' => 'Cidade'],
            ['select' => 'endereco.estado',                 'nome' => 'Estado'],
            ['select' => 'endereco.complemento',            'nome' => 'Complemento'],
            ['select' => 'endereco.ponto_referencia',       'nome' => 'Ponto de Referência'],
            ['select' => 'trilha_referencia',               'nome' => 'Trilha'],
            ['select' => 'curso_referencia',                'nome' => 'Curso'],
            ['select' => 'turma_referencia',                'nome' => 'Turma'],
            ['select' => 'avaliacao_referencia',            'nome' => 'Avaliação'],
            ['select' => 'avaliacoes_alunos.nota',          'nome' => 'Nota de Avaliação'],
            ['select' => 'avaliacoes_alunos.n_tentativa',   'nome' => 'N° de Tentativas feitas na Avaliação'],
            ['select' => 'users_data_nascimento',           'nome' => 'Data de Nascimento'],
            ['select' => 'users_data_cadastro',             'nome' => 'Data de Cadastro'],
            ['select' => 'users_data_ultimo_acesso',        'nome' => 'Data de Último Acesso'],
            ['select' => 'data_matricula_trilha',           'nome' => 'Data de Matrícula de Trilha'],
            ['select' => 'data_conclusao_trilha',           'nome' => 'Data de Conclusão de Trilha'],
            ['select' => 'data_matricula_turma',            'nome' => 'Data de Matrícula de Turma'],
            ['select' => 'users_data_conclusao_curso',      'nome' => 'Data de Conclusão de Curso'],
            ['select' => 'data_avaliacao_finalizada',       'nome' => 'Data de Avaliação Finalizada'],
            ['select' => 'data_ultimo_acesso_curso',        'nome' => 'Data de Último Acesso do Conteúdo do Curso'],
            ['select' => 'users_data_docs_recebidas',       'nome' => 'Data de Conclusão de envio de Documentação'],
            ['select' => 'cadastrante_aluno',               'nome' => 'Cadastrante do Aluno'],
            ['select' => 'users_idade',                     'nome' => 'Idade'],
            ['select' => 'unidade_referencia',              'nome' => 'Unidade'],
            ['select' => 'responsaveis_nome',               'nome' => 'Nome do Responsável'],
            ['select' => 'responsaveis_cpf',                'nome' => 'CPF do Responsável'],
            ['select' => 'responsaveis_rg',                 'nome' => 'RG do Responsável'],
            ['select' => 'responsaveis_celular',            'nome' => 'Celular do Responsável'],
            ['select' => 'responsaveis_email',              'nome' => 'Email do Responsável'],
            ['select' => 'responsaveis_data_nascimento',    'nome' => 'Data de Nascimento do Responsável'],
            ['select' => 'responsaveis_profissao',          'nome' => 'Nome do Responsável'],
            ['select' => 'responsaveis_cep',                'nome' => 'CEP do Responsável'],
            ['select' => 'responsaveis_rua',                'nome' => 'Rua do Responsável'],
            ['select' => 'responsaveis_numero',             'nome' => 'Número do Responsável'],
            ['select' => 'responsaveis_bairro',             'nome' => 'Bairro do Responsável'],
            ['select' => 'responsaveis_cidade',             'nome' => 'Cidade do Responsável'],
            ['select' => 'responsaveis_estado',             'nome' => 'Estado do Responsável'],
            ['select' => 'responsaveis_complemento',        'nome' => 'Complemento do Responsável'],
            ['select' => 'responsaveis_ponto_referencia',   'nome' => 'Ponto de Referência do Responsável'],

            ['select' => 'fluxo_caixa.valor_parcela',       'nome' => 'Valor de Parcela'],
            ['select' => 'fluxo_caixa.valor_total',         'nome' => 'Valor Total'],
            ['select' => 'fluxo_caixa.produto_nome',        'nome' => 'Produto'],
            ['select' => 'fluxo_caixa.cadastrante_nome',    'nome' => 'Cadastrante do Financeiro'],
            ['select' => 'fluxo_caixa.vendedor_nome',       'nome' => 'Vendedor'],
            ['select' => 'fluxo_caixa_data_vencimento',     'nome' => 'Data de Vencimento'],
            ['select' => 'fluxo_caixa_data_recebimento',    'nome' => 'Data de Recebimento'],
            ['select' => 'fluxo_caixa_data_cadastro',       'nome' => 'Data de Cadastro'],
            ['select' => 'fluxo_caixa_data_estorno',        'nome' => 'Data de Estorno'],
            ['select' => 'fluxo_caixa.forma_pagamento',     'nome' => 'Forma de Pagamento'],
            ['select' => 'fluxo_caixa.link',                'nome' => 'Link de Pagamento'],
            ['select' => 'fluxo_caixa_n_parcela',           'nome' => 'Parcelas'],
            ['select' => 'fluxo_caixa_status',              'nome' => 'Status de Pagamento'],
            ['select' => 'fluxo_caixa.obs',                 'nome' => 'Observações'],
            ['select' => 'categoria_aluno',                 'nome' => 'Categorias do Aluno'],
        ];

        return $campos;
    }


    public function getCamposContatos()
    {
        $campos = [
            ['select' => 'contatos.assunto',                   'nome' => 'Assunto'],
            ['select' => 'contatos.nome',                      'nome' => 'Nome'],
            ['select' => 'contatos.mensagem',                  'nome' => 'Mensagem'],
            ['select' => 'contatos.email',                     'nome' => 'Email'],
            ['select' => 'contatos.cpf',                       'nome' => 'CPF'],
            ['select' => 'contatos.id',                        'nome' => 'Matrícula'],
            ['select' => 'contatos.rg',                        'nome' => 'RG'],
            ['select' => 'contatos.genero',                    'nome' => 'Gênero'],
            ['select' => 'contatos.profissao',                 'nome' => 'Profissão'],
            ['select' => 'contatos.obs',                       'nome' => 'Observações'],
            ['select' => 'contatos_status',                    'nome' => 'Status de Atendimento'],
            ['select' => 'contatos.celular',                   'nome' => 'Celular'],
            ['select' => 'contatos.telefone',                  'nome' => 'Telefone'],
            ['select' => 'contatos.cep',                       'nome' => 'CEP'],
            ['select' => 'contatos.rua',                       'nome' => 'Rua'],
            ['select' => 'contatos.numero',                    'nome' => 'Número'],
            ['select' => 'contatos.bairro',                    'nome' => 'Bairro'],
            ['select' => 'contatos.cidade',                    'nome' => 'Cidade'],
            ['select' => 'contatos.estado',                    'nome' => 'Estado'],
            ['select' => 'contatos.complemento',               'nome' => 'Complemento'],
            ['select' => 'contatos.ponto_referencia',          'nome' => 'Ponto de Referência'],
            ['select' => 'trilha_referencia',                  'nome' => 'Trilha'],
            ['select' => 'curso_referencia',                   'nome' => 'Curso'],
            ['select' => 'turma_referencia',                   'nome' => 'Turma'],
            ['select' => 'formulario_referencia',              'nome' => 'Formulário'],
            ['select' => 'unidade_referencia',                 'nome' => 'Unidade'],
            ['select' => 'contatos_data_nascimento',           'nome' => 'Data de Nascimento'],
            ['select' => 'contatos_data_cadastro',             'nome' => 'Data de Cadastro'],
            ['select' => 'contatos_idade',                     'nome' => 'Idade'],
            ['select' => 'contatos.anotacao',                  'nome' => 'Anotações'],
            ['select' => 'categoria_contato',                  'nome' => 'Categorias do Contato'],
        ];

        return $campos;
    }

    public function export() 
    {
        return Excel::download(new RelatorioExport, 'relatorio.xlsx');
    }

}