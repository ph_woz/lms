<div>

	<div class="d-lg-flex justify-content-end mb-4">

		@if($contato->tipo == 'S' && $this->user_id)
			<a href="#ModalEnviarComunicado" data-bs-toggle="modal" class="ms-lg-3 btn btn-primary fs-13 py-2">
				Responder ao Aluno &nbsp; <i class="fa fa-bullhorn"></i>
			</a>
            <a href="{{ route('admin.aluno.info', ['id' => $this->user_id]) }}" class="ms-lg-3 btn bg-base text-white" title="Visualizar conta do aluno">
                <i class="fa fa-eye"></i>
            </a>
		@endif

		@if($countCheck == 0)
			@if($turma)
				@if(\App\Models\AclPlataformaModuloUser::verificaMeuAcessoUrlNivel('turma', 1))
					<a href="#ModalMatricular" data-bs-toggle="modal" class="ms-lg-3 mt-3 mt-lg-0 btn btn-primary fs-13 py-2">
						Matricular
					</a>
					<form method="POST" action="{{ route('admin.contato.matricular-aluno', ['contato_id' => $contato->id]) }}">
					@csrf
						<div class="modal fade" id="ModalMatricular" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
						  	<div class="modal-dialog modal-lg modal-dialog-centered" role="document">
						    	<div class="modal-content">
						      		<div class="modal-header">
						        		<h3 class="modal-title">Matricular Contato como Aluno na Turma {{ $turma->nome }}</h3>
						    			<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
						      		</div>
						      		<div class="modal-body">

						      			<div class="mb-1">
						      				<label for="matricular_aluno_turma_enviar_email">
						      					<input type="checkbox" id="matricular_aluno_turma_enviar_email" name="matricular_aluno_turma_enviar_email" class="square-15">
						      					Enviar Email de Boas Vindas
						      				</label>
						      			</div>

										<div>
											<textarea name="email_contratacao" class="descricao textarea-form">{{ $plataforma->email_contratacao_turma }}</textarea>
										</div>

						      		</div>
						      		<div class="modal-footer">
						      			<button type="submit" class="btn btn-primary weight-600 py-2">Confirmar</button>
						      		</div>
						    	</div>
						  	</div>
						</div>
					</form>
				@endif
			@endif
			@if($trilha)
				@if(\App\Models\AclPlataformaModuloUser::verificaMeuAcessoUrlNivel('trilha', 1))
					<a href="#ModalMatricular" data-bs-toggle="modal" class="ms-lg-3 mt-3 mt-lg-0 btn btn-primary fs-13 py-2">
						Matricular
					</a>
					<form method="POST" action="{{ route('admin.contato.matricular-aluno', ['contato_id' => $contato->id]) }}">
					@csrf
						<div class="modal fade" id="ModalMatricular" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
						  	<div class="modal-dialog modal-lg modal-dialog-centered" role="document">
						    	<div class="modal-content">
						      		<div class="modal-header">
						        		<h3 class="modal-title">Matricular Contato como Aluno na Trilha {{ $trilha->referencia }}</h3>
						    			<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
						      		</div>
						      		<div class="modal-body">

						      			<div class="mb-1">
						      				<label for="matricular_aluno_trilha_enviar_email">
						      					<input type="checkbox" id="matricular_aluno_trilha_enviar_email" name="matricular_aluno_trilha_enviar_email" class="square-15">
						      					Enviar Email de Boas Vindas
						      				</label>
						      			</div>

										<div>
											<textarea name="email_contratacao" class="descricao textarea-form">{{ $plataforma->email_contratacao_trilha }}</textarea>
										</div>

						      		</div>
						      		<div class="modal-footer">
						      			<button type="submit" class="btn btn-primary weight-600 py-2">Confirmar</button>
						      		</div>
						    	</div>
						  	</div>
						</div>
					</form>
				@endif
			@endif
		@else
			<a href="{{ route('admin.aluno.info', ['id' => $this->user_id]) }}" class="ms-lg-3 mt-3 mt-lg-0 btn btn-outline-primary weight-600 fs-13 py-2">
				Matriculado
			</a>
		@endif

	</div>

	<div class="section-title rounded-bottom-0 d-flex justify-content-between align-items-center">
		{{ $contato->nome }}
		<div>
			<a href="#ModalMetaDados" data-bs-toggle="modal" class="btn btn-info me-2 fs-14 mb-1 py-1 px-2-3">
				<i class="fa fa-align-left"></i>
			</a>
			<a href="#ModalEditarContato" data-bs-toggle="modal" class="aclNivel2 btn btn-warning text-dark fs-14 mb-1 py-1 px-2">
				<i class="fa fa-edit"></i>
			</a>
			<a href="#ModalDeletarContato" data-bs-toggle="modal" class="aclNivel3 btn btn-danger mb-1 fs-15 py-1 px-2 ms-2">
				<i class="fa fa-trash"></i>
			</a>
		</div>
	</div>

	<div class="bg-white border d-lg-flex justify-content-between align-items-center p-4">

		<div class="mb-2 mb-lg-0">
			<p class="mb-0 weight-600 color-444">Assunto</p>
			<p class="mb-0">
				{{ $contato->assunto  ?? 'Não definido' }}
			</p>
		</div>
		
		@if($contato->tipo == 'L')
		<div class="mb-2 mb-lg-0">
			<p class="mb-0 weight-600 color-444">Formulário de Inscrição</p>
			@if($formulario)
				<a href="{{ route('admin.formulario.info', ['id' => $formulario->id]) }}" class="color-blue-info-link">
					{{ $formulario->referencia }}
				</a>
			@else
				<p class="mb-0">
					Não definido
				</p>
			@endif
		</div>
		@endif

		@if(isset($curso))
		<div class="mb-2 mb-lg-0">
			<p class="mb-0 weight-600 color-444">Curso de Interesse</p>
			<a href="{{ route('admin.curso.info', ['id' => $curso->id]) }}" class="color-blue-info-link">
				{{ $curso->referencia }}
			</a>
		</div>
		@endif

		@if(isset($turma))
		<div class="mb-2 mb-lg-0">
			<p class="mb-0 weight-600 color-444">Turma de Interesse</p>
			<a href="{{ route('admin.turma.info', ['id' => $turma->id]) }}" class="color-blue-info-link">
				{{ $turma->nome }}
			</a>
		</div>
		@endif

		@if(isset($trilha))
		<div class="mb-2 mb-lg-0">
			<p class="mb-0 weight-600 color-444">Trilha</p>
			<a href="{{ route('admin.trilha.info', ['id' => $trilha->id]) }}" class="color-blue-info-link">
				{{ $trilha->referencia }}
			</a>
		</div>
		@endif

	</div>

	@if(isset($unidade))
	<div class="bg-white border d-lg-flex justify-content-between align-items-center p-4">

		<div class="mb-2 mb-lg-0">
			<p class="mb-0 weight-600 color-444">Unidade</p>
			@if($unidade)
				<a href="{{ route('admin.unidade.info', ['id' => $unidade->id]) }}" class="color-blue-info-link">
					{{ $unidade->nome }}
				</a>
			@else
				<p class="mb-0">
					Não definido
				</p>
			@endif
		</div>

	</div>
	@endif

	<div class="bg-white border p-4">

		<div class="row">
			<div class="col-lg-6">
				<ul class="list-group list-group-flush">

  					<li class="list-group-item">
						<span class="mb-0 weight-600 color-444">Status:</span>
						<span class="weight-700 {{ $contato->getStatusColor($contato->status) }}">
							{{ $contato->getStatusNome($contato->status) }}
						</span>
  					</li>
			  		<li class="list-group-item">
			  			<span class="mb-0 weight-600 color-444">Tag:</span>
			  			@if($contato->tipo == 'L')
			  				Lead
			  			@else
			  				Suporte
			  			@endif
			  		</li>
			  		<li class="list-group-item">
			  			<span class="mb-0 weight-600 color-444">Data de Cadastro:</span>
			  			{{ $contato->created_at->format('d/m/Y H:i') ?? null }} ({{$contato->created_at->diffForHumans()}})
			  		</li>
			  		<li class="list-group-item">
			  			<span class="mb-0 weight-600 color-444">Email:</span>
			  			{{ $contato->email }}
			  		</li>
			  		@if($contato->cpf)
				  		<li class="list-group-item">
				  			<span class="mb-0 weight-600 color-444">CPF:</span>
				  			{{ $contato->cpf }}
				  		</li>
			  		@endif
			  		@if($contato->rg)
				  		<li class="list-group-item">
				  			<span class="mb-0 weight-600 color-444">RG:</span>
				  			{{ $contato->rg }}
				  		</li>
			  		@endif
			  		@if($contato->naturalidade)
				  		<li class="list-group-item">
				  			<span class="mb-0 weight-600 color-444">Naturalidade:</span>
				  			{{ $contato->naturalidade }}
				  		</li>
			  		@endif
			  		@if($contato->valor_contratado)
				  		<li class="list-group-item">
				  			<span class="mb-0 weight-600 color-444">Valor contratado:</span>
				  			R$ {{ $contato->valor_contratado }}
				  		</li>
			  		@endif
			  		@if($contato->celular)
			  		<li class="list-group-item">
			  			<span class="mb-0 weight-600 color-444">Celular:</span>
			  			{{ $contato->celular }}
			  		</li>
			  		@endif
			  		@if($contato->telefone)
			  		<li class="list-group-item">
			  			<span class="mb-0 weight-600 color-444">Telefone:</span>
			  			{{ $contato->telefone }}
			  		</li>
			  		@endif
			  		@if($contato->data_nascimento)
			  		<li class="list-group-item">
			  			<span class="mb-0 weight-600 color-444">Data de Nascimento:</span>
			  			{{ \App\Models\Util::replaceDatePt($contato->data_nascimento) }}
			  		</li>
			  		@endif
			  		@if($contato->genero)
			  		<li class="list-group-item">
			  			<span class="mb-0 weight-600 color-444">Gênero:</span>
			  			{{ $contato->genero }}
			  		</li>
			  		@endif
			  		@if($contato->cep)
			  		<li class="list-group-item">
			  			<span class="mb-0 weight-600 color-444">CEP:</span>
			  			{{ $contato->cep }}
			  		</li>
			  		@endif
			  		@if($contato->rua)
			  		<li class="list-group-item">
			  			<span class="mb-0 weight-600 color-444">Rua:</span>
			  			{{ $contato->rua }}
			  		</li>
			  		@endif
			  		@if($contato->numero)
			  		<li class="list-group-item">
			  			<span class="mb-0 weight-600 color-444">N°:</span>
			  			{{ $contato->numero }}
			  		</li>
			  		@endif
			  		@if($contato->bairro)
			  		<li class="list-group-item">
			  			<span class="mb-0 weight-600 color-444">Bairro:</span>
			  			{{ $contato->bairro }}
			  		</li>
			  		@endif
			  		@if($contato->cidade)
			  		<li class="list-group-item">
			  			<span class="mb-0 weight-600 color-444">Cidade:</span>
			  			{{ $contato->cidade }}
			  		</li>
			  		@endif
			  		@if($contato->estado)
			  		<li class="list-group-item">
			  			<span class="mb-0 weight-600 color-444">Estado:</span>
			  			{{ $contato->estado }}
			  		</li>
			  		@endif
			  		@if($contato->complemento)
			  		<li class="list-group-item">
			  			<span class="mb-0 weight-600 color-444">Complemento:</span>
			  			{{ $contato->complemento }}
			  		</li>
			  		@endif
			  		@if($contato->ponto_referencia)
			  		<li class="list-group-item">
			  			<span class="mb-0 weight-600 color-444">Ponto de Referência:</span>
			  			{{ $contato->ponto_referencia }}
			  		</li>
			  		@endif
			  	</ul>
		  	</div>
			<div class="col-lg-6">
				@if($contato->mensagem)
					<span class="ms-lg-3 mb-0 weight-600 color-444">Mensagem:</span>
					<hr class="d-lg-none">
					<div class="ms-lg-3">{!! nl2br($contato->mensagem) !!}</div>
				@endif
			</div>
		</div>

	</div>

	@if($contato->anotacao)
	<div class="bg-white border p-4">
		<p class="mb-0 weight-600 color-444">Anotações</p>
		{!! nl2br($contato->anotacao) !!}
	</div>
	@endif

	@if(count($documentos) > 0)
	<div class="bg-white border p-4 d-lg-flex justify-content-between">

		<div class="mb-2 mb-lg-0">
			<p class="mb-0 weight-600 color-444">Documentos</p>
	  		@foreach($documentos as $documento)
				<div>
					<span>
						{{ $documento->nome }}:
						<a href="{{ $documento->link }}" class="color-blue-info-link" target="_blank">
							Abrir documento
						</a>
					</span>
				</div>
	  		@endforeach
		</div>
	</div>
	@endif

	<div class="bg-white border p-4 d-lg-flex justify-content-between">

		<div class="mb-2 mb-lg-0">
			<p class="mb-0 weight-600 color-444">Categorias</p>
			@if(count($categoriasSelecionadas) > 0)
				<ul class="list-group list-group-flush">
			  		@foreach($categoriasSelecionadas as $categoria)
			  			<li class="list-group-item">{{ $categoria }}</li>
			  		@endforeach
			  	</ul>
			@else
				<p class="mb-0 font-nunito">Não definido</p>
			@endif
		</div>

	</div>
	
	<div class="modal fade" id="ModalMetaDados" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	  	<div class="modal-dialog modal-dialog-centered" role="document">
	    	<div class="modal-content">
	      		<div class="modal-header">
	        		<h3 class="modal-title">Metadados</h3>
	    			<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
	      		</div>
	      		<div class="modal-body">

					<ul class="list-group list-group-flush"> 
	  					<li class="list-group-item">
							<span class="mb-0 weight-600 color-444">Status:</span>
							<span class="weight-700 {{ $contato->getStatusColor($contato->status) }}">
								{{ $contato->getStatusNome($contato->status) }}
							</span>
	  					</li>
	  					<li class="list-group-item">
							<span class="mb-0 weight-600 color-444">Arquivado:</span>
							@if($contato->arquivado == 0)
								<span class="text-primary weight-700">Não</span>
							@else
								<span class="text-secondary weight-700">Sim</span>
							@endif
	  					</li>
	  					<li class="list-group-item">
							<span class="mb-0 weight-600 color-444">Data de Cadastro:</span>
							@if($contato->created_at)
								<span class="mb-0">{{ $contato->created_at->format('d/m/Y H:i') ?? null }} ({{$contato->created_at->diffForHumans()}}) </span>
	  						@else
	  							Não definido
	  						@endif
	  					</li>   
	  				</ul>

	      		</div>
	    	</div>
	  	</div>
	</div>

	<form wire:submit.prevent="editar" method="POST">
		<div wire:ignore class="modal fade" id="ModalEditarContato" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  		<div class="modal-dialog modal-dialog-centered">
	    		<div class="modal-content">
	      			<div class="modal-header">
	        			<h3 class="modal-title" id="exampleModalLabel">Editar Informações</h3>
	        			<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
	      			</div>
	      			<div class="modal-body">

	      				<textarea wire:model="anotacao" class="textarea-form" placeholder="Faça aqui anotações"></textarea>
			            <select wire:model="status" class="mb-1 select-form">
							<option value="0">Pendente</option>
							<option value="1">Em análise</option>
							<option value="2">Atendido</option>
							<option value="3">Desativado</option>
			            </select>
			            <select wire:model="arquivado" class="mb-1 select-form">
			                <option value="N">Não arquivados</option>
			                <option value="S">Arquivados</option>
			            </select>
						<div class="w-100" wire:ignore>
							<select wire:model="categoriasSelected" class="selectpicker" data-live-search="true" data-width="100%" multiple title="Categorias">
								@foreach($categorias as $categoria)
									<option value="{{ $categoria->id }}">
										{{ $categoria->nome }}
									</option>
								@endforeach
							</select>
						</div>

	      			</div>
	      			<div class="modal-footer">
		        		<button type="submit" class="btn btn-primary weight-600 py-2">Confirmar</button>
	      			</div>
	    		</div>
	  		</div>
		</div>
	</form>

	<form wire:submit.prevent="delete" method="POST">
		<div wire:ignore class="modal fade" id="ModalDeletarContato" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  		<div class="modal-dialog modal-dialog-centered">
	    		<div class="modal-content">
	      			<div class="modal-header">
	        			<h3 class="modal-title" id="exampleModalLabel">Deletar Contato</h3>
	        			<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
	      			</div>
	      			<div class="modal-body text-center py-5">
	        			<p class="m-0 fs-16">
	        				Você tem certeza que deseja deletar este contato?
	        				<br>
	        				Todas as informações serão perdidas definitivamente.
	        			</p>
	      			</div>
	      			<div class="modal-footer">
	      				<div class="d-flex w-100">
		      				<input type="password" wire:model="password_to_delete" class="w-100 form-control p-form me-3" placeholder="Digite sua senha" required>
		        			<button type="submit" class="btn btn-danger weight-600">Confirmar</button>
	    				</div>
	      			</div>
	    		</div>
	  		</div>
		</div>
	</form>

	<form wire:submit.prevent="addRespostaAoAluno" method="POST">
		<div wire:ignore class="modal fade" id="ModalEnviarComunicado" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  		<div class="modal-dialog modal-dialog-centered">
	    		<div class="modal-content">
	      			<div class="modal-header">
	        			<h3 class="modal-title" id="exampleModalLabel">Responder ao Aluno</h3>
	        			<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
	      			</div>
	      			<div class="modal-body">

	      				<div class="mb-4">
	      					<label>Assunto</label>
	      					<input type="text" wire:model="resposta_assunto" class="form-control p-form" placeholder="Assunto" required maxlength="255">
	      				</div>

	      				<div>
	      					<label>Descreva sua Resposta ao Aluno</label>
	        				<textarea wire:model="resposta_descricao" class="textarea-form" placeholder="Mensagem" required></textarea>
	      				</div>

	      			</div>
	      			<div class="modal-footer">
		        		<button type="submit" class="btn btn-primary weight-600 py-2">Confirmar</button>
	      			</div>
	    		</div>
	  		</div>
		</div>
	</form>

	@include('livewire.util.toast')

</div>