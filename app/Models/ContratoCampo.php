<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ContratoCampo extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $table = 'contratos_campos';

    protected $fillable = [ 
    	'plataforma_id', 
    	'contrato_id',
    	'campo',
    	'py',
    	'px',
        'n_pagina',
        'fontSize',
        'color',
    ];

    public static  function boot()
    {
        parent::boot(); 
        static::creating(function ($model) {
            $model->plataforma_id = session('plataforma_id') ?? \Auth()->user()->plataforma_id ?? Plataforma::dominio()->pluck('id')[0] ?? null;
        });
    }

    public function scopePlataforma($query)
    {
        return $query->where('plataforma_id', session('plataforma_id') ?? Plataforma::dominio()->pluck('id')[0] ?? null);
    }

}
