<?php

namespace App\Http\Livewire\Admin\Avaliacao;

use Livewire\WithPagination;
use Livewire\Component;
use DB;
use App\Models\Avaliacao;
use App\Models\TurmaAluno;
use App\Models\User;
use App\Models\Log;

class Finalizaram extends Component
{
    use WithPagination;

    protected $paginationTheme = 'bootstrap';

    public $avaliacao_id;
    public $avaliacao;
    public $search;

    protected $queryString = ['search','alterar_nota_aluno_id','alterar_nota_aluno_nota'];

    public $resetar_aluno_id;
    public $alterar_nota_aluno_nome;
    public $alterar_nota_aluno_nota;
    public $alterar_nota_aluno_id;
    public $set_nova_nota;
    public $password_to_delete;

    public function mount($avaliacao_id)
    {
        $this->avaliacao_id = $avaliacao_id;
        $this->avaliacao = Avaliacao::select('id','referencia')->plataforma()->find($this->avaliacao_id);

        $this->alterar_nota_aluno_id   = $_GET['alterar_nota_aluno_id'] ?? null;
        $this->alterar_nota_aluno_nome = User::select('nome')->plataforma()->where('id', $this->alterar_nota_aluno_id)->pluck('nome')[0] ?? null;
        $this->alterar_nota_aluno_nota = $_GET['alterar_nota_aluno_nota'] ?? null;
    }

    public function render()
    {
        $alunosQueFinalizaram = DB::table('avaliacoes_alunos')
            ->select(
                'users.id',
                'users.nome',
                'users.email',
                'avaliacoes_alunos.nota',
                'avaliacoes_alunos.data_finalizada'
            )
            ->join('users','avaliacoes_alunos.aluno_id','users.id')
            ->where('users.plataforma_id', session('plataforma_id') ?? \Auth::user()->plataforma_id)
            ->where('users.tipo','aluno')
            ->where('avaliacoes_alunos.plataforma_id', session('plataforma_id') ?? \Auth::user()->plataforma_id)
            ->where('avaliacoes_alunos.avaliacao_id', $this->avaliacao->id)
            ->where('users.status', 0)
            ->where('avaliacoes_alunos.status', 'F');

        if($this->search)
        {
            $alunosQueFinalizaram->where(function ($query)
            {
                $query
                    ->orWhere('users.nome', 'like', '%'.$this->search.'%')
                    ->orWhere('users.email', 'like', '%'.$this->search.'%')
                    ->orWhere('users.cpf', 'like', '%'.$this->search.'%');
            });
        }

        $countAlunosQueFinalizaram = $alunosQueFinalizaram->count();

        $alunosQueFinalizaram = $alunosQueFinalizaram->paginate(10);


        $listaNotas = Avaliacao::listaNotas();

        return view('livewire.admin.avaliacao.finalizaram', [
            'avaliacao'                  => $this->avaliacao,
            'alunosQueFinalizaram'       => $alunosQueFinalizaram,
            'countAlunosQueFinalizaram'  => $countAlunosQueFinalizaram,
            'listaNotas'                 => $listaNotas,
        ]);
    }

    public function setResetar($aluno_id)
    {
        $this->resetar_aluno_id = $aluno_id;      
    }

    public function setAlterarNotaManualmente($aluno_id, $nota)
    {
        $this->alterar_nota_aluno_id = $aluno_id;
        $this->alterar_nota_aluno_nome = User::select('nome')->plataforma()->where('id', $aluno_id)->pluck('nome')[0] ?? null;

        if($nota != 'Não fez')
        {
            $this->alterar_nota_aluno_nota = number_format($nota, 2, ',', '.');
        
        } else {

            $this->alterar_nota_aluno_nota = $nota;
        }
    }

    public function resetarAvaliacao()
    {
        if(\Auth::attempt(['id' => \Auth::id(), 'password' => $this->password_to_delete, 'status' => 0]))
        {
            \App\Models\AvaliacaoAluno::plataforma()->where('aluno_id', $this->resetar_aluno_id)->where('avaliacao_id', $this->avaliacao_id)->delete();
            \App\Models\AvaliacaoAlunoResposta::plataforma()->where('aluno_id', $this->resetar_aluno_id)->where('avaliacao_id', $this->avaliacao_id)->delete();

            $aluno = User::plataforma()->aluno()->where('id', $this->resetar_aluno_id)->first();

            Log::create([
                'tipo' => 'avaliacao',
                'ref'  => 'Aluno ' . $aluno->nome . ' ('.$aluno->email .' - ' . $aluno->celular.')' . ' teve sua avaliação (' . $this->avaliacao->referencia . ') resetada.',
            ]);

            session()->flash('success-livewire', 'Avaliação do Aluno resetada com êxito.');

            $this->dispatchBrowserEvent('dissmiss-modal');
            $this->dispatchBrowserEvent('toast');

        } else {

            session()->flash('danger-livewire', 'Senha incorreta.');

            $this->dispatchBrowserEvent('toast');
        } 
    }

    public function alterarNotaManualmente()
    {
        if(\Auth::attempt(['id' => \Auth::id(), 'password' => $this->password_to_delete, 'status' => 0]))
        {
            $aluno = User::plataforma()->aluno()->where('id', $this->alterar_nota_aluno_id)->first();

            $alunoNota = \App\Models\AvaliacaoAluno::plataforma()->where('aluno_id', $this->alterar_nota_aluno_id)->where('avaliacao_id', $this->avaliacao_id)->first();
            
            if(isset($alunoNota))
            {
                Log::create([
                    'tipo' => 'avaliacao',
                    'ref'  => 'Aluno ' . $aluno->nome . ' ('.$aluno->email .' - ' . $aluno->celular.')' . ' teve sua nota da avaliação (' . $this->avaliacao->referencia . ') alterada manualmente de ' . $alunoNota->nota . ' para ' . $this->set_nova_nota . '.',
                ]);

                $alunoNota->update(['nota' => floatval(str_replace(',','.', $this->set_nova_nota))]);

            } else {

                \App\Models\AvaliacaoAluno::create([
                    'curso_id'        => $this->avaliacao->curso_id,
                    'turma_id'        => $this->avaliacao->turma_id,
                    'avaliacao_id'    => $this->avaliacao->id,
                    'aluno_id'        => $aluno->id,
                    'nota'            => floatval(str_replace(',','.', $this->set_nova_nota)),
                    'status'          => 'F',
                    'questoesIds'     => '0',
                    'data_finalizada' => date('Y-m-d H:i'),
                ]);

                Log::create([
                    'tipo' => 'avaliacao',
                    'ref'  => 'Aluno ' . $aluno->nome . ' ('.$aluno->email .' - ' . $aluno->celular.')' . ' teve uma nota ('.$this->set_nova_nota.') adicionada manualmente da avaliação (' . $this->avaliacao->referencia . ').',
                ]);
            }

            session()->flash('success-livewire', 'Nota Avaliação do Aluno foi alterada com êxito.');

            $this->dispatchBrowserEvent('dissmiss-modal');
            $this->dispatchBrowserEvent('toast');

            $this->alterar_nota_aluno_id = null;
            $this->set_nova_nota = null;
            $this->alterar_nota_aluno_nota = null;

        } else {

            session()->flash('danger-livewire', 'Senha incorreta.');

            $this->dispatchBrowserEvent('toast');
        } 
    }

}
