<!doctype html>
<html lang="pt-br">
<head>

  <!-- Google tag (gtag.js) -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=G-MREB450HCT"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'G-MREB450HCT');
  </script>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    {{-- <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/uikit@3.5.5/dist/css/uikit.min.css" /> --}}
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/uikit@3.16.15/dist/css/uikit.min.css" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/5.0.0-alpha1/css/bootstrap.min.css" integrity="sha384-r4NyP46KrjDleawBgD5tp8Y7UzmLA05oM1iAEQ17CSuDqnUK2+k9luXQOfXJCJ4I" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('sites-proprios/resgate-vertical/css/home.css') }}">
    <link rel="stylesheet" href="{{ asset('css/lmc-old.css') }}">

    <link rel="shortcut icon" href="{{ $plataforma->favicon ?? asset('sites-proprios/resgate-vertical/images/favicon.ico') }}">
    <link href="{{ Request::url() }}" rel="canonical">

   <title>{{ $seo->seo_title ?? $plataforma->nome }}</title>
   <meta name="robots" content="index, follow">
   <meta name="description" content="{{ $seo->seo_description ?? null }}"/>
   <meta name="keywords" content="{{ $seo->seo_keywords ?? null }}"/>
   <link rel="shortcut icon" href="{{ $plataforma->favicon ?? asset('sites-proprios/resgate-vertical/images/favicon.ico') }}">
   <meta property="og:locale" content="pt_BR" />
   <meta property="og:type" content="website" />
   <meta property="og:title" content="{{ $seo->seo_title ?? $plataforma->nome }}" />
   <meta property="og:description" content="{{ $seo->seo_description ?? null }}" />
   <meta property="og:url" content="{{ Request::url() }}" />
   <meta property="og:site_name" content="{{ $plataforma->nome }}" />
   <meta property="og:image" content="{{ $plataforma->logotipo ?? asset('sites-proprios/resgate-vertical/images/logotipo.png') }}" />
   <meta name='author' content='WOZCODE | Pedro Henrique' />

</head>
<body id="inicio">
   
   <header id="menuPlataforma">
      <nav class="navbar navbar-expand-lg navbar-light" id="menu">
         <div class="container-fluid px-xl-7">
            <a class="navbar-brand menu-link scroll-link" href="#inicio">
               <img class="d-none d-md-block" src="{{ $plataforma->logotipo ?? asset('sites-proprios/resgate-vertical/images/logotipo.png') }}" width="275" alt="Logotipo Resgate Vertical">
               <img class="d-block d-md-none" src="{{ $plataforma->logotipo ?? asset('sites-proprios/resgate-vertical/images/logotipo.png') }}" width="180" alt="Logotipo Resgate Vertical">
            </a>
            <button class="navbar-toggler outline-0 border-0" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
               <i class="fa fa-bars text-white"></i>
            </button>
            <div class="collapse navbar-collapse bg-xs-danger" id="navbarSupportedContent">
               <ul class="navbar-nav ml-auto mb-2 mb-lg-0">

                    <li class="nav-item">
                        <a class="menu-link nav-link" href="/#cursos">
                            Nossos cursos
                        </a>
                    </li>
                    
                    <li class="nav-item">
                        <a class="menu-link nav-link" href="#servicos" uk-scroll="offset: 80;">
                            Nossos serviços
                        </a>
                    </li>
                    
                    <li class="nav-item">
                        <a class="menu-link nav-link" href="/sobre-nos">
                            Quem Somos
                        </a>
                    </li>

                                          <div class="line-vertical"></div>
                                                          <li class="nav-item">
                        <a class="menu-link nav-link" 
                           href="/consultar-certificado" >
                            Consultar Certificado
                        </a>
                    </li>

                  <li class="nav-item">
                     <a class="nav-link activeNavLink" href="/login">
                        <i class="fa fa-user-graduate mr-1"></i>
                        Área do Aluno
                     </a>
                  </li>

               </ul>
            </div>
         </div>
      </nav>
   </header>

   <main>

      <section class="pb-5 cursor-drag">

         <div class="uk-position-relative uk-visible-toggle uk-light" tabindex="-1" uk-slideshow="animation: fade; ratio: false; autoplay: true; autoplay-interval: 5000; pause-on-hover: false">

            <ul class="uk-slideshow-items" uk-height-viewport>
               @foreach($banners as $banner)
               <li>
                  <div class="uk-background-cover" uk-parallax="bgy: -100" style="background-image: url('{{ $banner->link }}');" uk-cover>
                     <div class="background-overlay" style="background: {{ $banner->overlay_color ?? 'unset' }};opacity: {{ $banner->overlay_opacity ?? '0' }};"></div>
                     <div class="uk-position-left uk-flex uk-flex-middle uk-position-small uk-text-left uk-light px-xl-8">
                        <div>
                           <h2 uk-slideshow-parallax="x: 300,0,-100" class="font-poppins fs-md-50 fs-xs-35 weight-700 text-shadow-2">
                              {!! $banner->titulo !!}
                           </h2>
                           @if($banner->subtitulo)
                           <p uk-slideshow-parallax="x: 300,0,-100" class="mt-3 mb-4-5 font-poppins line-height-1-4 fs-17 text-light">
                              {!! $banner->subtitulo !!}
                           </p>
                           @endif
                           @if($banner->botao_texto)
                           <div uk-slideshow-parallax="x: 300,0,-100">
                              <a href="{{ $banner->botao_link }}" @if($banner->botao_nova_aba == 'S') target="_blank" @endif uk-scroll="offset: 80;" class="btnSaibaMais">
                                 {{ $banner->botao_texto }}
                              </a>
                           </div>
                           @endif
                        </div>
                     </div>
                  </div>
               </li>
               @endforeach
            </ul>

            <ul class="uk-slideshow-nav uk-dotnav uk-flex-center mt--md-130 position-relative"></ul>

            <div class="uk-light">
               <a class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous uk-slideshow-item="previous"></a>
               <a class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-next uk-slideshow-item="next"></a>
            </div>

         </div>

      </section>

      <section class="pb-lg-3" id="servicos">
         <div class="container-fluid px-xl-7 mb-4">

            <h1 class="text-center font-raleway weight-700 fs-30 color-333">
               Serviços
            </h1>

            <p class="mt-3 mb-5 text-center font-raleway weight-600 color-666 col-md-10 offset-md-1 fs-18">
               A RESGATE VERTICAL, desenvolve, moderniza e inova os seus serviços pela constante atualização de seu capital intelectual, decorrente do aprendizado em seus trabalhos. Adota no desenvolvimento dos produtos e serviços as diretrizes que asseguram pró-atividade
            </p>

            <div class="pt-4-5 uk-child-width-1-3@m" uk-grid uk-scrollspy="cls: uk-animation-fade; target: .uk-card; delay: 300;">

               <div class="mb-lg-4-5 mb-2">
                  <div class="uk-card bg-white border p-3 box-info rounded h-lg-475 mb-5 mb-lg-0 border-servicos">
                     
                     <div class="flex-center mt--67">
                        <div class="square-115 flex-center bg-danger rounded-circle box-info border-solutions">
                           <i class="fas fa-atom text-light fs-45 text-shadow-3"></i>
                        </div>
                     </div>

                     <div class="h-lg-325">

                        <p class="text-center font-montserrat weight-600 pt-4 fs-20 text-dark">
                           Higiene<br>Ocupacional
                        </p>

                        <hr class="border border-secondary">

                        <p class="font-raleway weight-500">
                           Avaliações ambientais e exposição de pessoas à agentes físicos, químicos, biológicos e ergonômicos. Metodologias para identificação de perigos e riscos à saúde, e identificação de necessidades, além de Gestão de Afastados (reabilitação).
                        </p>

                     </div>

                     <div>

                        <hr class="border border-secondary">

                        <a href="#contato" uk-scroll="offset: 80;" class="btn btn-danger w-100 font-nunito weight-600 hover-d-none">
                           Saber Mais
                        </a>

                     </div>

                  </div>
               </div>

               <div class="mb-lg-4-5 mb-2">
                  <div class="uk-card bg-white border p-3 box-info rounded h-lg-475 mb-5 mb-lg-0 border-servicos">
                     
                     <div class="flex-center mt--67">
                        <div class="square-115 flex-center bg-danger rounded-circle box-info border-solutions">
                           <i class="fas fa-tree text-light fs-40 text-shadow-3"></i>
                        </div>
                     </div>

                     <div class="h-lg-325">

                        <p class="text-center font-montserrat weight-600 pt-4 fs-20 text-dark">
                           Engenharia<br>Ambiental
                        </p>

                        <hr class="border border-secondary">

                        <p class="font-raleway weight-500">
                           Construção de Sistema de Gestão Ambiental e diagnóstico de eficácia de sistemas existentes.
                        </p>

                     </div>

                     <div>

                        <hr class="border border-secondary">

                        <a href="#contato" uk-scroll="offset: 80;" class="btn btn-danger w-100 font-nunito weight-600 hover-d-none">
                           Saber Mais
                        </a>

                     </div>

                  </div>
               </div>

               <div class="mb-lg-4-5 mb-2">
                  <div class="uk-card bg-white border p-3 box-info rounded h-lg-475 mb-5 mb-lg-0 border-servicos">
                     
                     <div class="flex-center mt--67">
                        <div class="square-115 flex-center bg-danger rounded-circle box-info border-solutions">
                           <i class="fas fa-file-signature text-light fs-40 ml-2 text-shadow-3"></i>
                        </div>
                     </div>

                     <div class="h-lg-325">

                        <p class="text-center font-montserrat weight-600 pt-4 fs-20 text-dark">
                           Engenharia de Segurança do Trabalho
                        </p>

                        <hr class="border border-secondary">

                        <p class="font-raleway weight-500">
                           Assessoria técnica, jurídica e legal. Identificação, elaboração e implantação de procedimentos e práticas de trabalho em atendimento aos requisitos legais. Assessoria em perícia técnica, realização de cursos, seminários e workshops para editar.
                        </p>

                     </div>

                     <div>

                        <hr class="border border-secondary">

                        <a href="#contato" uk-scroll="offset: 80;" class="btn btn-danger w-100 font-nunito weight-600 hover-d-none">
                           Saber Mais
                        </a>

                     </div>

                  </div>
               </div>

               <div class="mb-lg-4-5 mb-2">
                  <div class="uk-card bg-white border p-4 box-info rounded h-lg-475 mb-5 mb-lg-0 border-servicos">
                     
                     <div class="flex-center mt--67">
                        <div class="square-115 flex-center bg-danger rounded-circle box-info border-solutions">
                           <i class="fas fa-fire text-light fs-45 text-shadow-3"></i>
                        </div>
                     </div>

                     <div class="h-lg-310">

                        <p class="text-center font-montserrat weight-600 pt-4 fs-20 text-dark">
                           Engenharia de Combate a Incêndio
                        </p>

                        <hr class="border border-secondary">

                        <p class="font-raleway weight-500">
                           Projetar as medidas de segurança contra incêndio conforme legislação para aprovação de projetos contra incêndio junto aos órgãos competentes.
                           <br>
                           Acompanhar e executar a instalação, manutenção e execução de sistemas contra incêndio.
                        </p>

                     </div>

                     <div>

                        <hr class="border border-secondary">

                        <a href="#contato" uk-scroll="offset: 80;" class="btn btn-danger w-100 font-nunito weight-600 hover-d-none">
                           Saber Mais
                        </a>

                     </div>

                  </div>
               </div>

               <div class="mb-lg-4-5 mb-2">
                  <div class="uk-card bg-white border p-4 box-info rounded h-lg-475 mb-5 mb-lg-0 border-servicos">
                     
                     <div class="flex-center mt--67">
                        <div class="square-115 flex-center bg-danger rounded-circle box-info border-solutions">
                           <i class="fas fa-fire-extinguisher text-light fs-45 text-shadow-3"></i>
                        </div>
                     </div>

                     <div class="h-lg-310">

                        <p class="text-center font-montserrat weight-600 pt-4 fs-20 text-dark">
                           Manutenção e Recarga de Extintores de Incêndio
                        </p>

                        <hr class="border border-secondary">

                        <p class="font-raleway weight-500">
                           Pó químico, CO2 – Gás Carbônico, Água, entre outros. Realizamos a manutenção, pintura e recarga do extintor conforme a NBR 12962 e RTQ 173 INMETRO.Atendemos comércios, indústrias, condomínios, hospitais, construtoras entre outros.
                        </p>

                     </div>

                     <div>

                        <hr class="border border-secondary">

                        <a href="#contato" uk-scroll="offset: 80;" class="btn btn-danger w-100 font-nunito weight-600 hover-d-none">
                           Saber Mais
                        </a>

                     </div>

                  </div>
               </div>

               <div class="mb-lg-4-5 mb-2">
                  <div class="uk-card bg-white border p-4 box-info rounded h-lg-475 mb-5 mb-lg-0 border-servicos">
                     
                     <div class="flex-center mt--67">
                        <div class="square-115 flex-center bg-danger rounded-circle box-info border-solutions">
                           <i class="fas fa-users-cog text-light fs-40 text-shadow-3"></i>
                        </div>
                     </div>

                     <div class="h-lg-310">

                        <p class="text-center font-montserrat weight-600 pt-4 fs-20 text-dark">
                           Manutenção de Mangueiras de Incêndio
                        </p>

                        <hr class="border border-secondary">

                        <p class="font-raleway weight-500">
                           Realizamos teste de mangueira no local com equipamento próprio. Assim você pode acompanhar de perto o teste. O teste e secagem são executados conforme descrito na NBR12779. Também realizamos Empatação em mangueiras de incêndio 1 1/2” e 2 1/2”, e personalização de caixa de hidrantes.
                        </p>

                     </div>

                     <div>

                        <hr class="border border-secondary">

                        <a href="#contato" uk-scroll="offset: 80;" class="btn btn-danger w-100 font-nunito weight-600 hover-d-none">
                           Saber Mais
                        </a>

                     </div>

                  </div>
               </div>

            </div>

         </div>
      </section>

      <section id="diferenciais">
         <div class="uk-background-cover" style="background-attachment: fixed; clip-path: polygon(0 0, 100% 0, 100% 95%, 0% 100%); background-image: url('https://lmcv2.s3.sa-east-1.amazonaws.com/padroes-lmcv2/bombeiro-form-foto_fundo.jfif');">

         <div class="background-overlay-diferenciais"></div>

            <div class="container-fluid px-xl-7 pt-5-5 pb-5-5 uk-position-relative">

            <div class="row align-items-center">
               
               <div class="col-lg-5">

                  <h1 class="font-montserrat weight-900 fs-md-60 text-light text-shadow-1">
                     CONHEÇA OS NOSSOS CURSOS
                  </h1>

                  <p class="pr-lg-5 mb-lg-5 mb-4 mt-3 text-light text-shadow-1 font-montserrat weight-500 fs-16">
                     Garanta sua vaga e comece a estudar para se tornar um Bombeiro Civil profissional agora!
                  </p>
                   
                  <div class="d-none d-md-block" uk-scrollspy="cls: uk-animation-slide-bottom;">
                                 
                     <a href="#cursos" uk-scroll="offset: 80;" class="btnSaibaMais shadow">
                        Quero conhecer os Cursos!
                     </a> 

                  </div>
               
               </div>

               <div class="col-lg-7">
                  <div class="row">
                     <div class="col-lg-6 px-lg-1 mb-lg-2 mb-1" uk-scrollspy="cls: uk-animation-slide-bottom;">
                        <div class="p-4 bg-white rounded-3 box-info h-lg-250 border">
                           <p class="d-flex align-items-center weight-700 text-shadow-1 fs-17 font-opensans text-dark mb-1">
                              <i class="fas fa-play-circle fs-40"></i>
                              <span class="ml-3">
                                 Vídeo Aulas Dinâmicas
                              </span>
                           </p>

                           <p class="text-dark font-opensans">
                              Uma vídeo aula atraente é o melhor método para aprender novos conhecimentos. Nossos conteúdos são excelência em qualidade. Você poderá estudar num notebook, num tablet ou no seu smarphone.
                           </p>
                        </div>
                     </div>
                     <div class="col-lg-6 px-lg-1 mb-lg-2 mb-1" uk-scrollspy="cls: uk-animation-slide-bottom;">
                        <div class="p-4 bg-white rounded-3 box-info h-lg-250 border">
                           <p class="d-flex align-items-center weight-700 text-shadow-1 fs-17 font-opensans text-dark mb-1">
                              <i class="fas fa-desktop fs-40"></i>
                              <span class="ml-3">
                                 Plataforma
                              </span>
                           </p>

                           <p class="text-dark font-opensans">
                             Todos os conteúdos foram enriquecidos com apostilas, jogos, exercícios dinâmicos para você utilizar. Você terá uma verdadeira biblioteca virtual com vários e-books para baixar e estudar.
                           </p>
                        </div>
                     </div>
                     <div class="col-lg-6 px-lg-1 mb-lg-0 mb-1" uk-scrollspy="cls: uk-animation-slide-bottom;">
                        <div class="p-4 bg-white rounded-3 box-info h-lg-250 border">
                           <p class="d-flex align-items-center weight-700 text-shadow-1 fs-17 font-opensans text-dark mb-1">
                              <i class="fas fa-chalkboard-teacher fs-40"></i>
                              <span class="ml-3">
                                 Nossos Professores
                              </span>
                           </p>

                           <p class="text-dark font-opensans">
                             Todos os nossos professores oferecem aulões semanais de revisão de cada disciplina. Aproveite para tirar as suas dúvidas participando dos nossos encontros presencias. Junte-se a nossa galera!
                           </p>
                        </div>
                     </div>         
                     <div class="col-lg-6 px-lg-1" uk-scrollspy="cls: uk-animation-slide-bottom;">
                        <div class="p-4 bg-white rounded-3 box-info h-lg-250 border">
                           <p class="d-flex align-items-center weight-700 text-shadow-1 fs-17 font-opensans text-dark mb-1">
                              <i class="fa fa-clock fs-40"></i>
                              <span class="ml-3">
                                 Faça seu próprio horário
                              </span>
                           </p>

                           <p class="text-dark font-opensans">
                             Aqui conosco você pode estudar no horário que planejar melhor sua rotina. Tenha um estudo flexível e inicie as aulas no seu tempo, no seu horário que melhor desejar.
                           </p>
                        </div>
                     </div>
                  </div>
               </div>
             
            </div>

            <div class="d-block d-md-none pt-4 text-xs-center pt-5 pb-3">
               <a href="#cursos" uk-scroll="offset: 80;" class="btnSaibaMais shadow">
                  Quero conhecer os Cursos!
               </a> 
            </div>

            </div>
         </div>
      </section>

      <section class="bg-white py-5" id="nossas-aulas">
         <div class="container-fluid px-xl-7 my-3">

            <h1 class="text-center font-montserrat weight-800 fs-35 text-danger text-shadow-1">
               Clientes
            </h1>

            <p class="mt-3 mb-4-5 text-center font-raleway weight-600 color-666 col-md-8 offset-md-2 fs-18">
               Conheça alguns dos nossos clientes
            </p>

            <div class="cursor-drag uk-position-relative uk-visible-toggle uk-light" tabindex="-1" uk-slider="autoplay: true; autoplay-interval: 800; pause-on-hover: false">

               <ul class="uk-slider-items uk-child-width-1-2 uk-child-width-1-5@m uk-grid">
                  @foreach($clientes as $cliente)
                    <li>
                        <div class="uk-panel">
                           <a>
                              <img src="{{ $cliente->logotipo }}" width="{{ $cliente->logotipoWidth ?? 200 }}" alt="{{ $cliente->nome }} - Resgate Vertical">
                           </a>
                        </div>
                    </li>
                  @endforeach
               </ul>

               <div class="uk-light">
                  <a class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous uk-slider-item="previous"></a>
                  <a class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-next uk-slider-item="next"></a>
               </div>

            </div>

         </div>
      </section>

      <section class="bg-f5 py-5" id="cursos">
         <div class="container-fluid px-xl-7 bg-lines">

            <h1 class="text-center font-montserrat weight-800 fs-35 text-shadow-1">
               Cursos
            </h1>

            <p class="mt-3 mb-4-5 text-center font-raleway weight-600 color-666 col-md-8 offset-md-2 fs-18">
              Conheça todos os nossos cursos para você
            </p>

            <div class="mb-4-5 d-lg-flex justify-content-center">

              <button type="button" data-nivel="todos" class="d-block mb-3 mb-lg-0 btn-categoria bg-categoria-active px-3-5 py-2 font-raleway weight-600" style="border:3px solid #333; border-radius: 50px;">
                Todos
              </button>

              <button type="button" data-nivel="curso-nivel-curso-tecnico" class="d-block mb-3 mb-lg-0 mx-lg-3 btn-categoria px-3-5 py-2 font-raleway weight-600" style="border:3px solid #333; border-radius: 50px;">
                Cursos Técnicos
              </button>

              <button type="button" data-nivel="curso-nivel-curso-profissionalizante" class="d-block mb-3 mb-lg-0 btn-categoria px-3-5 py-2 font-raleway weight-600" style="border:3px solid #333; border-radius: 50px;">
                Cursos Profissionalizantes
              </button>

              <button type="button" data-nivel="curso-nivel-norma-regulamentadora" class="ml-lg-3 d-block mb-3 mb-lg-0 btn-categoria px-3-5 py-2 font-raleway weight-600" style="border:3px solid #333; border-radius: 50px;">
                Norma Regulamentadora
              </button>

            </div>

            <div id="sliders" class="cursor-drag uk-position-relative uk-visible-toggle uk-light" tabindex="-1" uk-slider="autoplay: true; autoplay-interval: 1500; pause-on-hover: true">

               <ul class="uk-slider-items uk-child-width-1-1 uk-child-width-1-4@m uk-grid">
                  @foreach($cursos as $curso)
                    <li class="curso-slide curso-nivel-{{\Str::slug($curso->nivel)}}">
                        <div class="uk-panel mb-4">
                           <div class="card border-0 rounded-5 boxArticle">
                              <a href="/curso/{{$curso->slug}}" class="hover-d-none text-d-none">
                                 <div class="p-3 rounded-bottom-0 h-lg-90 flex-center bg-danger rounded-5">
                                    <p class="mb-0 font-opensans weight-700 fs-18 text-light text-shadow-2 text-center">
                                       {{ $curso->nome }}
                                    </p>
                                 </div>
                                 <div>
                                    <img src="{{ $curso->foto_miniatura ?? asset('images/no-image.jpeg') }}" class="img-fluid object-fit-cover rounded-bottom h-200 w-100">
                                 </div>
                              </a>
                           </div>
                        </div>
                    </li>
                  @endforeach
               </ul>

               <div class="uk-light">
                  <a class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous uk-slider-item="previous"></a>
                  <a class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-next uk-slider-item="next"></a>
               </div>

            </div>

         </div>
      </section>

      <section class="py-5 bg-danger">
         <div class="container-fluid px-xl-7">     
            <div class="row align-items-center">

               <div class="col-lg-6">
                  <h1 class="font-montserrat weight-900 fs-md-60 text-light text-shadow-1">
                     CONHEÇA-NOS!
                  </h1>
                  <p class="mb-0 text-light font-opensaans fs-18 weight-600 text-shadow-2">
                     Você tem interesse em nossos treinamentos ou precisa de algum serviço que prestamos? entre em contato agora mesmo.
                  </p>
               </div>
               <div class="col-lg-6 text-lg-right mt-xs-4-5">
                   <div uk-slideshow-parallax="x: 300,0,-100">
                     <a href="https://api.whatsapp.com/send?phone=5569993212121&text=Olá, vim através do Site da Resgate Vertical" target="_blank" class="btnSaibaMais px-lg-5 py-3 shadow">
                        <i class="fab fa-whatsapp color-jumb-wpp weight-600"></i>
                        <span class="ml-2">WhatsApp</span>
                     </a>
                  </div>
               </div>
          
            </div>
         </div>
      </section>

      <section class="py-5 bg-1" id="contato">
         <div class="container-fluid px-xl-7">

            <h1 class="fs-40 text-center font-raleway text-light weight-800">SOLICITE JÁ SEU ORÇAMENTO</h1>
            <div class="flex-center mt-4-5 mb-4-5"><div class="w-80px h-2px bg-danger rounded"></div></div>

            <div class="row py-lg-4 px-3 px-md-0">

               <div class="col-lg-6">

                      <div class="mb-4">
                          <p class="mb-0 font-poppins text-white d-flex align-items-center">
                            <i class="fa fa-clock fs-30 text-danger"></i>
                            <span class="ml-4">Segunda - Sexta: 8h as 12h - 14h as 18h<br>Sábados: 8h as 12h</span>
                          </p>
                      </div>

                      <div class="mb-4">
                          <p class="mb-0 font-poppins text-white d-flex align-items-center">
                            <i class="fa fa-envelope fs-30 text-danger"></i>
                            <span class="ml-4">contato@resgatevertical.com.br</span>
                          </p>
                      </div>

                      <div class="mb-4 width-large">
                          <a href="https://api.whatsapp.com/send?phone=5569993212121&text=Olá, vim através do Site da Resgate Vertical" target="_blank" class="text-d-none hover-d-none font-poppins text-white d-flex align-items-center">
                            <i class="fab fa-whatsapp fs-30 text-danger"></i>
                            <span class="text-light text-d-none ml-3 ls-05">&nbsp;(69) 99321-2121</span>
                          </a>
                      </div>

                      <div class="mb-4 width-large">
                          <a href="tel:+5530157772" class="text-d-none hover-d-none font-poppins text-white d-flex align-items-center">
                            <i class="mr-2 fa fa-phone fs-25 text-danger"></i>
                            <span class="text-light text-d-none ml-3 ls-05">(69) 3015-7772</span>
                          </a>
                      </div>

                      <div class="mb-lg-5 mb-4">
                          <div class="d-flex align-items-center">
                            <i class="mr-2 fas fa-map-marker-alt fs-30 text-danger"></i>
                            <address class="py-0 ml-3 my-0 text-light font-poppins">
                              Av. 7 de Setembro, 3457 - Centro
                            </address>
                          </div>
                      </div>

                      <div>
                        
                                                                                        <a href="https://facebook.com/resgateverticalpvh" target="_blank" class="btn btn-danger text-white text-shadow-3 square-40 fs-17 rounded-2">
                                    <i class="fab fa-facebook-f"></i>
                                </a>
                                                                                                                                                                                                                                                                                                                
                        
                                                                                                                    <a href="https://www.instagram.com/resgatevertical/" target="_blank" class="btn btn-danger text-white text-shadow-3 square-40 fs-17 rounded-2">
                                    <i class="fab fa-instagram"></i>
                                </a>
                                                                                                                                                                                                                                                                                    
                        
                                                                                                                                                <a href="https://www.youtube.com/channel/UCuz17E3nR5kw2Mo169P4JKA" target="_blank" class="btn btn-danger text-white text-shadow-3 square-40 fs-17 rounded-2">
                                    <i class="fab fa-youtube ml--3"></i>
                                </a>
                                                                                                                                                                                                                                                        
                                              </div>

               </div>

               <div class="col-lg-6 mt-lg-0 mt-5">
                <form id="form-contato" method="POST">
                  @csrf
                       

                        @if(session('success'))
                           <div class='alert alert-success'><b>Sucesso!</b> {{ session('success') }}</div>
                        @endif
               
                  <input type="text" name="nome" class="form-control placeholder-edit py-2-5 outline-0 rounded-2" placeholder="Nome Pessoa Física ou Jurídica" required>
                  <input type="tel" name="celular" class="my-2 telefone form-control placeholder-edit py-2-5 outline-0 rounded-2" placeholder="Telefone" required>
                  <input type="email" name="email" class="rounded-left-0 form-control placeholder-edit py-2-5 outline-0 rounded-2" placeholder="Email" required>
                  <textarea name="mensagem" placeholder="Mensagem" class="mt-2 form-control placeholder-edit h-min-100 w-100 outline-0 rounded-2"></textarea>
                  <input type="text" name="check" class="invisible h-0px" placeholder="Não preencha este campo.">
                  <input type="submit" value="Enviar" name="btnSend" class="w-100 mt--15px btn btn-danger font-raleway weight-600 fs-17">

                </form>
               </div>
               
            </div>
         
         </div>
      </section>

      <a href="https://api.whatsapp.com/send?phone=5569993212121&text=Olá, vim através do site da Resgate Verical" target="_blank" class="text-white fs-30">
          <div class="p-fixed bottom-0 right-0 mr-4 mb-4">
              <div class="square-60 rounded-circle bg-success flex-center shadow">
                  <i class="fab fa-whatsapp"></i>
              </div>
          </div>
      </a>
   
   </main>

  
   <footer>
       <div class="bg-e8 py-2">
          <div class="container-fluid px-xl-7 d-lg-flex align-items-center justify-content-between">
            <p class="font-roboto fs-13 weight-600 text-center mb-1 mb-lg-0 color-copyright">
                2020 &copy; <a href="https://wozcode.com" class="color-copyright hover-d-none text-d-none color-hover-copyright" target="_blank">
                Resgate Vertical • Todos os direitos reservados               </a>
            </p>
            <p class="font-roboto fs-13 weight-600 text-center mb-0 color-copyright">
               Desenvolvido por <a href="https://wozcode.com" target="_blank" class="text-danger text-d-none fs-11 weight-600 text-shadow-1">WOZCODE</a>
            </p>
          </div>
       </div>
   </footer>

   {{-- <script src="https://cdn.jsdelivr.net/npm/uikit@3.5.5/dist/js/uikit.min.js"></script> --}}
   <script src="https://kit.fontawesome.com/2f91eaaef4.js" crossorigin="anonymous"></script>  
   <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>    
   <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
   <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
   <script src="https://cdn.jsdelivr.net/npm/uikit@3.16.15/dist/js/uikit.min.js"></script>
   <script>

      var el = document.getElementById('menu');
      var numPx = '50'; 

      window.addEventListener('scroll', function()
      {
         if(window.scrollY > numPx) 
            el.classList.add('mudaCor'); 
         else 
            el.classList.remove('mudaCor');
      });


      $('a[href^="/#"]').each(function(){ 
          var oldUrl = $(this).attr("href"); // Get current url
          var newUrl = oldUrl.replace("/#", "#"); // Create new url
          $(this).attr("href", newUrl).addClass('scroll-link'); // Set herf value
      });


        $('.scroll-link').on('click', function(e) {

            e.preventDefault();

            var target = $( $(this).attr('href') );

            if( target.length ) {
                $('html, body').animate({
                    scrollTop: target.offset().top - 70
                }, 500);
            }

              $(".navbar-collapse").removeClass('show');
        });


        let countDesativados = 0;
        let countAtivos = 0;

        $('.btn-categoria').click(function() {

            $('.btn-categoria').removeClass('bg-categoria-active');
            $(this).addClass('bg-categoria-active');

            let nivel = $(this).attr('data-nivel');

            if(nivel != 'todos')
            {
               $('.curso-slide').hide();
               $('.'+nivel).show();
            
            } else {

               $('.curso-slide').show();
            }

            UIkit.slider('#sliders').$destroy();
            UIkit.slider('#sliders').$startAutoplay();
        });

    </script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.js"></script>
    <script>
       $(document).on("focus", ".telefone", function() { 
         jQuery(this)
             .mask("(99) 9999-99999")
             .change(function (event) {  
                 target = (event.currentTarget) ? event.currentTarget : event.srcElement;  
                 phone = target.value.replace(/\D/g, '');
                 element = $(target);  
                 element.unmask();  
                 if(phone.length > 10) {  
                     element.mask("(99) 99999-9999");  
                 } else {  
                     element.mask("(99) 9999-9999?9");  
                 }  
         });
       });
    </script>

</body>
</html>