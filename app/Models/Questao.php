<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Questao extends Model
{
    use HasFactory;

    protected $table = 'questoes';

    public $timestamps = true;

    public $fillable = [
        'plataforma_id',
        'cadastrante_id',
        'assunto',
        'enunciado',
        'arquivo',
        'modelo', // (D)iscursiva ou (O)bjetiva
        'gabarito',
        'status',
    ];

    public static  function boot()
    {
        parent::boot(); 
        static::creating(function ($model)
        {
            $model->cadastrante_id = \Auth()->user()->id ?? null;
            $model->plataforma_id  = session('plataforma_id') ?? \Auth()->user()->plataforma_id ?? Plataforma::dominio()->pluck('id')[0] ?? null;
        });
    }
    
    public function scopePlataforma($query)
    {
        return $query->where('plataforma_id', session('plataforma_id') ?? Plataforma::dominio()->pluck('id')[0] ?? null);
    }

    public function scopeAtivo($query)
    {
        return $query->where('status', 0);
    }

    public function isImage($path)
    {
        $extension = pathinfo($path, PATHINFO_EXTENSION);
        
        if($extension == 'jpg')
            return true;
        if($extension == 'jpeg')
            return true;
        if($extension == 'tiff')
            return true;
        if($extension == 'webpt')
            return true;
        if($extension == 'png')
            return true;

        return false;
    }

}
