<footer id="footer" class="footer container-fluid content-info" style="background-image: url('{{ asset('sites-proprios/images/cefaep/bg-footer.jpg') }}');" role="contentinfo">
   
   <div class="container-fluid px-xl-7">
      <div class="row">

         <div class="col-lg-3 footer__item">

            <h2 class="footer__title text-uppercase">CONTATO</h2>

            <div class="mb-2 d-flex align-items-center">
               @if($plataforma->telefone)
               <div class="address__icon me-1">
                  <i class="bi bi-telephone"></i>                           
               </div>
               <a href="tel:+55{{preg_replace('/\D/', '', $plataforma->telefone)}}" class="address__phone" itemprop="telephone">
                  {{ $plataforma->telefone ?? '(00) 0000-0000' }}
               </a>
               @endif
            </div>

            <div class="mb-2 d-flex align-items-center">
               @if($plataforma->whatsapp)
               <div class="address__icon me-1">
                  <i class="bi bi-whatsapp"></i>
               </div>
               <a href="https://api.whatsapp.com/send?phone=55{{preg_replace('/\D/', '', $plataforma->whatsapp)}}&text=Olá, vim através do Site {{ $plataforma->nome }}" target="_blank" class="address__phone" itemprop="telephone">
                  {{ $plataforma->whatsapp ?? '(00) 0000-0000' }}
               </a>
               @endif
            </div>

            <div class="mb-2 d-flex align-items-center"> 
               <div class="address__icon me-1">
                  <i class="bi bi-envelope"></i>
               </div>
               <div>
                  <a href="mailto:{{ $plataforma->email }}" class="link--white">{{ $plataforma->email ?? 'example@domain.com' }}</a>
               </div>
            </div>

         </div>

         <div class="col-lg-3 footer__item">

            <h2 class="footer__title text-uppercase">ENDEREÇO</h2>

            <div class="d-flex align-items-center">
               <div class="address__icon me-1">
                  <i class="bi bi-geo-alt"></i>
               </div>
               <div>
                  <address class="m-0 p-0 address__phone">
                     {!! $plataforma->endereco ?? 'Sem endereço definido' !!}
                  </address>
               </div>
            </div>

         </div>

         <div class="col-lg-3 footer__item">

            <ul class="nav-neutro nav-neutro--inline nav-social pull-right-sm">
               <li class="nav-neutro__item nav-social__item">
                  <h2 class="footer__title text-uppercase">Redes Sociais</h2>
               </li>
               <li class="nav-neutro__item nav-social__item">
                  <a href="{{ $plataforma->facebook }}" class="nav-social__link" target="_blank">
                  	<i class="bi bi-facebook"></i>
                  </a>
               </li>
               <li class="nav-neutro__item nav-social__item">
                  <a href="{{ $plataforma->instagram }}" class="nav-social__link" target="_blank">
                  	<i class="bi bi-instagram"></i>
                  </a>
               </li>
            </ul>

         </div>

         <div class="col-lg-3 footer__item" align="right">
           <img alt="Logotipo" class="img-fluid lazyloaded" width="150" src="{{ $plataforma->logotipo }}">
         </div>

      </div>
   </div>

   <div class="container-fluid px-xl-7 footer__copyright">
      <div class="row">
         <div class="col-sm-6 text-xs-center">
            <span class="weight-600">{{ $plataforma->nome }}</span> © 2021 - Todos os direitos reservados <span class="d-xs-none">/</span> <span class="d-xs-block">CNPJ: {{ $plataforma->cnpj ?? ' 00.000.000/0000-00' }}</span>
         </div>
         <div class="col-sm-6 text-xs-center footer__copyright--by">
            Desenvolvido por <a href="https://wozcode.com" class="link--white" target="_blank"><strong class="footer__copyright--by">WOZCODE.</strong></a>
         </div> 
      </div>
   </div>

</footer>

<a href="https://api.whatsapp.com/send?phone=55{{preg_replace('/\D/', '', $plataforma->whatsapp)}}&text=Olá, vim através do Site {{ $plataforma->nome }}" target="_blank" class="text-white fs-30">
   <div class="position-fixed bottom-0 right-0 me-3 me-lg-4 mb-3 mb-lg-4 z-4 wpp-pulse-button z-8">
      <i class="bi bi-whatsapp"></i>
   </div>
</a>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ENjdO4Dr2bkBIFxQpeoTz1HIcje39Wm4jDKdf19U8gI4ddQ3GYNS7NTKfAdVQSZe" crossorigin="anonymous"></script>

{!! $plataforma->scripts_final_body !!}