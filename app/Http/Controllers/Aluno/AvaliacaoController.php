<?php

namespace App\Http\Controllers\Aluno;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\TurmaAluno;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Filesystem\Cache;

class AvaliacaoController extends Controller
{
    public function avaliacoes()
    {
        $getNomePage = 'Avaliações';
        $listar = request('listar') ?? 'a-fazer';

        $user_id = Auth::id();
        $plataforma_id = session('plataforma_id') ?? Auth::user()->plataforma_id;
        if ($listar == 'feitas')
        {
            $avaliacoes = DB::select( DB::raw("
                SELECT
                    avaliacoes.id,
                    avaliacoes.turma_id,
                    avaliacoes.referencia,
                    avaliacoes.instrucoes,
                    avaliacoes.n_tentativas,
                    avaliacoes.condicao_fazer_avaliacao,
                    avaliacoes_alunos.n_tentativa AS n_tentativa_atual,
                    avaliacoes_alunos.status,
                    avaliacoes_alunos.nota,
                    avaliacoes.liberar_gabarito
                FROM
                    avaliacoes
                INNER JOIN avaliacoes_alunos ON
                    avaliacoes.id = avaliacoes_alunos.avaliacao_id
                WHERE
                        avaliacoes.status = 0
                    AND avaliacoes.plataforma_id = ".$plataforma_id."
                    AND avaliacoes_alunos.aluno_id = ".$user_id."
                    AND avaliacoes_alunos.status = 'F';
            "));
        }
        else
        {
            $avaliacoes = DB::select( DB::raw("
                SELECT
                    avaliacoes.id,
                    avaliacoes.turma_id,
                    avaliacoes.referencia,
                    avaliacoes.instrucoes,
                    avaliacoes.n_tentativas,
                    avaliacoes.condicao_fazer_avaliacao,
                    avaliacoes.liberar_gabarito,
                    avaliacoes_alunos.n_tentativa AS n_tentativa_atual,
                    avaliacoes_alunos.status,
                    avaliacoes_alunos.nota,
                    turmas_alunos.data_conclusao_conteudo
                FROM
                    turmas_alunos
                JOIN avaliacoes ON
                    turmas_alunos.turma_id = avaliacoes.turma_id
                LEFT JOIN avaliacoes_alunos ON
                        avaliacoes.id = avaliacoes_alunos.avaliacao_id
                    AND avaliacoes_alunos.aluno_id = ".$user_id."
                WHERE
                        avaliacoes.status = 0
                    AND avaliacoes_alunos.status IS NULL
                    AND turmas_alunos.status = 0
                    AND turmas_alunos.plataforma_id = ".$plataforma_id."
                    AND turmas_alunos.aluno_id = ".$user_id."
                ORDER BY
                    turmas_alunos.data_conclusao_conteudo DESC,
                    avaliacoes.condicao_fazer_avaliacao;
            "));
        }
        

        return view('aluno.avaliacoes', compact('listar', 'avaliacoes', 'getNomePage') );
    }

    public function avaliacao($id)
    {
        $avaliacao = \App\Models\Avaliacao::plataforma()->ativo()->findOrFail($id);

        $getNomePage = 'Avaliação: ' . $avaliacao->referencia;

        $avaliacaoAluno = \App\Models\AvaliacaoAluno::plataforma()->where('aluno_id', Auth::id())->where('avaliacao_id', $avaliacao->id)->first();

        if(is_null($avaliacaoAluno))
        {
            if($avaliacao->condicao_fazer_avaliacao != null)
            {
                $checkConclusaoConteudo = TurmaAluno::select('id')->plataforma()->where('aluno_id', Auth::id())->where('curso_id', $avaliacao->curso_id)->whereNotNull('data_conclusao_conteudo')->exists();

                if($checkConclusaoConteudo == false)
                {
                    return back()->with('danger', 'Você ainda não concluiu este curso.');
                }
            }

            $avaliacaoAluno = \App\Models\AvaliacaoAluno::create([
                'aluno_id'     => Auth::id(),
                'avaliacao_id' => $avaliacao->id,
                'curso_id'     => $avaliacao->curso_id,
                'turma_id'     => $avaliacao->turma_id,
            ]);
        }

        $questoesIds = [];

        if($avaliacaoAluno->questoesIds == null)
        {
            $getQuestoesPorNota = \App\Models\AvaliacaoConfigNota::plataforma()->where('avaliacao_id', $id)->get();

            foreach($getQuestoesPorNota as $questaoQntNota)
            {
                $setQuestoes = DB::table('avaliacoes_questoes')
                    ->select('avaliacoes_questoes.questao_id')
                    ->join('questoes','avaliacoes_questoes.questao_id','questoes.id')
                    ->where('questoes.status', 0)
                    ->where('avaliacoes_questoes.plataforma_id', Auth::user()->plataforma_id)
                    ->where('questoes.plataforma_id', Auth::user()->plataforma_id)
                    ->where('avaliacao_id', $id)
                    ->where('valor_nota', $questaoQntNota->valor_nota)
                    ->limit(intval($questaoQntNota->n_questoes))
                    ->get()
                    ->pluck('questao_id')
                    ->toArray();

                array_push($questoesIds, ...$setQuestoes);
            }

            if(empty($questoesIds))
            {
                return back()->with('danger', 'Não há questões nesta avaliação.');
            }

            $avaliacaoAluno->update(['questoesIds' => implode(',', $questoesIds)]);
        
        } else {

            $questoesIds = explode(',', $avaliacaoAluno->questoesIds);
        }

        $questoes = \App\Models\Questao::plataforma()->whereIn('id', $questoesIds)->inRandomOrder()->get();

        return view('aluno.avaliacao', compact('getNomePage','avaliacao','questoes') );
    }

    public function finalizar($avaliacao_id)
    {
        $avaliacaoAluno = \App\Models\AvaliacaoAluno::plataforma()->where('avaliacao_id', $avaliacao_id)->where('aluno_id', Auth::id())->first();

        $nota = 0;

        if(request('respostasObjetivas'))
        {   
            $alternativasIds = [];
            foreach(request('respostasObjetivas') as $questao_id => $alternativa)
            {
                $alternativasIds[] = $alternativa;
            }
        
            $tentativa = DB::table('avaliacoes_questoes')
                ->join('questoes_alternativas','avaliacoes_questoes.questao_id','questoes_alternativas.questao_id')
                ->whereIn('questoes_alternativas.id', $alternativasIds)
                ->where('avaliacoes_questoes.plataforma_id', Auth::user()->plataforma_id)
                ->where('questoes_alternativas.plataforma_id', Auth::user()->plataforma_id)
                ->where('avaliacoes_questoes.avaliacao_id', $avaliacaoAluno->avaliacao_id)
                ->select(
                    'avaliacoes_questoes.questao_id as questao_id',
                    'questoes_alternativas.id as alternativa_id',
                    'avaliacoes_questoes.valor_nota',
                    'questoes_alternativas.correta as correta'
                )
                ->get();

            \App\Models\AvaliacaoAlunoResposta::plataforma()
                ->where('aluno_id', Auth::id())
                ->where('avaliacao_id', $avaliacaoAluno->avaliacao_id)
                ->delete();

            foreach($tentativa as $resposta)
            {
                \App\Models\AvaliacaoAlunoResposta::create([
                    'avaliacao_id'   => $avaliacaoAluno->avaliacao_id,
                    'turma_id'       => $avaliacaoAluno->turma_id,
                    'aluno_id'       => Auth::id(),
                    'questao_id'     => $resposta->questao_id,
                    'alternativa_id' => $resposta->alternativa_id,
                    'acertou'        => $resposta->correta,
                ]);
            }

            $corretas = $tentativa->where('correta','S')->pluck('valor_nota')->toArray();

            $nota = $avaliacaoAluno->getNota($corretas);
        }

        if(request('respostasDiscursivas'))
        {
            foreach(request('respostasDiscursivas') as $questao_id => $resposta)
            {
                \App\Models\AvaliacaoAlunoResposta::create([
                    'avaliacao_id'          => $avaliacaoAluno->avaliacao_id,
                    'turma_id'              => $avaliacaoAluno->turma_id,
                    'aluno_id'              => Auth::id(),
                    'questao_id'            => $questao_id,
                    'resposta_dissertativa' => $resposta,
                    'acertou'               => 'Em correção',
                ]);
            }
        }


        $avaliacaoAluno->update([
            'data_finalizada' => date('Y-m-d H:i'),
            'nota'            => $nota,
            'status'          => 'F',
        ]);


        $this->atualizaConclusaoCurso($avaliacaoAluno);


        return redirect()->route('aluno.avaliacoes')->with('success', 'Avaliação concluída com êxito.');
    }

    public function atualizaConclusaoCurso($avaliacaoAluno) // Esta função é responsável por verificar se o aluno concluiu o curso/trilha ou não.
    {
        $turmaAluno = TurmaAluno::plataforma()->where('aluno_id', \Auth::id())->where('curso_id', $avaliacaoAluno->curso_id)->first();
        
        if($turmaAluno->data_conclusao_curso == null)
        {
            $aulasConcluidas = \App\Models\AlunoAulaConcluida::select('aula_id')->plataforma()->where('aluno_id', \Auth::id())->where('curso_id', $turmaAluno->curso_id)->get()->pluck('aula_id')->toArray();

            $countAulas = \App\Models\CursoAula::select('id')->plataforma()->ativo()->where('curso_id', $turmaAluno->curso_id)->count();

            if(count($aulasConcluidas) >= $countAulas)
            {
                $nota_minima_aprovacao = \App\Models\Turma::select('nota_minima_aprovacao')->plataforma()->ativo()->where('id', $turmaAluno->turma_id)->pluck('nota_minima_aprovacao')[0] ?? null;

                if(isset($nota_minima_aprovacao))
                {         
                    $notas = \App\Models\AvaliacaoAluno::select('nota')->plataforma()->finalizada()->where('aluno_id', \Auth::id())->where('turma_id', $turmaAluno->turma_id)->get()->pluck('nota')->toArray();
                    $nota  = \App\Models\AvaliacaoAluno::staticSomaNotas($notas);

                    if($nota >= floatval($nota_minima_aprovacao))
                    {
                        $turmaAluno->update([
                            'data_conclusao_curso' => date('Y-m-d H:i'),
                            'codigo_certificado'   => \App\Models\TurmaAluno::geraCodigoCertificado($turmaAluno->id)
                        ]);
                        
                        $trilha_id = \App\Models\TrilhaTurma::plataforma()->where('turma_id', $turmaAluno->turma_id)->pluck('trilha_id')[0] ?? null;
                         
                        if($trilha_id)
                        {
                            $alunoTrilha = \App\Models\TrilhaAluno::plataforma()->where('trilha_id', $trilha_id)->where('aluno_id', \Auth::id())->first();

                            if($alunoTrilha)
                            {
                                $turmasIdsParaConcluir = \App\Models\TrilhaTurma::plataforma()->where('trilha_id', $trilha_id)->select('turma_id')->get()->pluck('turma_id')->toArray();
                                $turmasConcluidas = \App\Models\TurmaAluno::plataforma()->where('aluno_id', \Auth::id())->whereNotNull('data_conclusao_curso')->whereIn('turma_id', $turmasIdsParaConcluir)->count();

                                if(count($turmasIdsParaConcluir) == $turmasConcluidas) // CONCLUIU A TRILHA
                                    $alunoTrilha->update(['data_conclusao' => date('Y-m-d H:i'), 'codigo_certificado' => \App\Models\TrilhaAluno::geraCodigoCertificado($alunoTrilha->id)]);
                            }
                        }
                    }

                } else {

                    $turmaAluno->update([
                        'data_conclusao_curso' => date('Y-m-d H:i'),
                        'codigo_certificado'   => TurmaAluno::geraCodigoCertificado($turmaAluno->id),
                    ]);

                    $trilha_id = \App\Models\TrilhaTurma::plataforma()->where('turma_id', $turmaAluno->turma_id)->pluck('trilha_id')[0] ?? null;

                    if($trilha_id)
                    {
                        $alunoTrilha = \App\Models\TrilhaAluno::plataforma()->where('trilha_id', $trilha_id)->where('aluno_id', \Auth::id())->first();

                        if($alunoTrilha)
                        {
                            $turmasIdsParaConcluir = \App\Models\TrilhaTurma::plataforma()->where('trilha_id', $trilha_id)->select('turma_id')->get()->pluck('turma_id')->toArray();
                            $turmasConcluidas = \App\Models\TurmaAluno::plataforma()->where('aluno_id', \Auth::id())->whereNotNull('data_conclusao_curso')->whereIn('turma_id', $turmasIdsParaConcluir)->count();

                            if(count($turmasIdsParaConcluir) == $turmasConcluidas) // CONCLUIU A TRILHA
                                $alunoTrilha->update(['data_conclusao' => date('Y-m-d H:i'), 'codigo_certificado' => \App\Models\TrilhaAluno::geraCodigoCertificado($alunoTrilha->id)]);
                        }
                    }
                }
            }
        }
    }

    public function gabarito($avaliacao_id)
    {
        $avaliacao = \App\Models\Avaliacao::plataforma()->ativo()->where('id', $avaliacao_id)->first();
        $avaliacaoAluno = \App\Models\AvaliacaoAluno::plataforma()->where('aluno_id', Auth::id())->where('avaliacao_id', $avaliacao_id)->first();

        if($avaliacao->liberar_gabarito == 'N')
        {
            return back()->with('danger', 'Gabarito não liberado');
        }

        $questoesIds = explode(',', $avaliacaoAluno->questoesIds);
        $questoes = \App\Models\Questao::plataforma()->whereIn('id', $questoesIds)->get();

        $getNomePage = 'Avaliação: ' . $avaliacao->referencia;
        
        return view('aluno.gabarito', compact('getNomePage','avaliacao','avaliacaoAluno','questoes') );
    }

    public function reiniciar($avaliacao_id)
    {
        $avaliacao = \App\Models\Avaliacao::plataforma()->ativo()->where('id', $avaliacao_id)->first();

        $avaliacaoAluno = \App\Models\AvaliacaoAluno::plataforma()->where('aluno_id', Auth::id())->where('avaliacao_id', $avaliacao_id)->first();

        if($avaliacaoAluno->n_tentativa >= $avaliacao->n_tentativas)
        {
            return back()->with('danger', 'Você não pode tentar novamente.');
        }

        $nota_minima_aprovacao = \App\Models\Turma::plataforma()->ativo()->where('id', $avaliacao->turma_id)->pluck('nota_minima_aprovacao')[0] ?? 0;

        if($avaliacaoAluno->nota >= floatval($nota_minima_aprovacao))
        {
            return back()->with('danger', 'Você não pode tentar novamente.');
        }

        $avaliacaoAluno->update(['n_tentativa' => $avaliacaoAluno->n_tentativa + 1, 'nota' => 0]);
        
        \App\Models\AvaliacaoAlunoResposta::plataforma()->where('aluno_id', Auth::id())->where('avaliacao_id', $avaliacao_id)->delete();
    
        return redirect()->route('aluno.avaliacao', ['id' => $avaliacao_id]);
    }

    public function correcao($avaliacao_id, $aluno_id)
    {
        if(Auth::user()->tipo == 'aluno')
        {
            return back()->with('danger', 'Você não pode acessar esta página.');
        }

        $avaliacao = \App\Models\Avaliacao::plataforma()->ativo()->where('id', $avaliacao_id)->first();
        $avaliacaoAluno = \App\Models\AvaliacaoAluno::plataforma()->where('aluno_id', $aluno_id)->where('avaliacao_id', $avaliacao_id)->first();

        $questoesIds = explode(',', $avaliacaoAluno->questoesIds);
        $questoes = \App\Models\Questao::plataforma()->whereIn('id', $questoesIds)->get();

        $nome = \App\Models\User::plataforma()->where('id', $aluno_id)->pluck('nome')[0] ?? null;

        $getNomePage = 'Avaliação: ' . $avaliacao->referencia;

        if(request('confirmar_correcao'))
        {
            if(request('respostasDiscursivasCorrecao'))
            {
                foreach(request('respostasDiscursivasCorrecao') as $aluno_resposta_id => $correcao)
                {
                    if($correcao)
                    {
                        \App\Models\AvaliacaoAlunoResposta::plataforma()
                            ->where('id', $aluno_resposta_id)
                            ->update(['resposta_dissertativa_correcao' => $correcao]);
                    }
                }
            }

            if(request('respostasDiscursivasCorrecaoAcertou'))
            {
                $alternativasIds = \App\Models\AvaliacaoAlunoResposta::plataforma()->where('avaliacao_id', $avaliacao->id)->where('aluno_id', $aluno_id)->select('alternativa_id')->get()->pluck('alternativa_id')->toArray();
                
                $tentativa = DB::table('avaliacoes_questoes')
                    ->join('questoes_alternativas','avaliacoes_questoes.questao_id','questoes_alternativas.questao_id')
                    ->whereIn('questoes_alternativas.id', $alternativasIds)
                    ->where('avaliacoes_questoes.plataforma_id', Auth::user()->plataforma_id)
                    ->where('questoes_alternativas.plataforma_id', Auth::user()->plataforma_id)
                    ->where('avaliacoes_questoes.avaliacao_id', $avaliacaoAluno->avaliacao_id)
                    ->select(
                        'avaliacoes_questoes.questao_id as questao_id',
                        'questoes_alternativas.id as alternativa_id',
                        'avaliacoes_questoes.valor_nota as valor_nota_questao',
                        'questoes_alternativas.correta as correta'
                    )
                    ->get();

                $corretas = $tentativa->where('correta','S')->pluck('valor_nota_questao')->toArray();

                $notaAtual = $avaliacaoAluno->getNota($corretas);

                $notaParaSomar = [];
                foreach(request('respostasDiscursivasCorrecaoAcertou') as $aluno_resposta_id => $acertouOuNao)
                {
                    $avaliacaoAlunoResposta = \App\Models\AvaliacaoAlunoResposta::plataforma()
                        ->where('id', $aluno_resposta_id)
                        ->first();

                    $avaliacaoAlunoResposta->update(['acertou' => $acertouOuNao]);

                    if($acertouOuNao == 'S')
                    {
                        $getValorNotaQuestao = \App\Models\AvaliacaoQuestao::plataforma()->where('avaliacao_id', $avaliacao_id)->where('questao_id', $avaliacaoAlunoResposta->questao_id)->pluck('valor_nota')[0] ?? null; 
                        $getValorNotaQuestao = floatval(str_replace(',','.', $getValorNotaQuestao));

                        $notaParaSomar[] = $getValorNotaQuestao;
                    }
                } 

                $notaTotal = array_sum($notaParaSomar) + $notaAtual;
                $avaliacaoAluno->update(['nota' => $notaTotal]);
            }

            return back()->with('success', 'Correção realizada com êxito.');
        }

        return view('aluno.correcao', compact('getNomePage','nome','avaliacao','avaliacaoAluno','questoes','aluno_id') );
    }

}