@extends('layouts.admin.master')

@section('css')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.14.0-beta2/dist/css/bootstrap-select.min.css">
@endsection

@section('content')

<div>

    <div class="mb-3 d-flex justify-content-end">
	    <a href="{{ route('admin.aluno.info', ['id' => $user->id]) }}" class="btn bg-base text-white">
	        <i class="fa fa-eye"></i>
	    </a>
	</div>

    <form method="POST" class="mb-4">
    @csrf

		<section class="mb-4-5">
			
			<a href="#boxInfoPrincipal" data-bs-toggle="collapse" aria-expanded="false" class="hover-d-none">
				<div class="section-title">
					Informações Principais
				</div>
			</a>
			<hr/>
			
			<div class="collapse show" id="boxInfoPrincipal">

		    	<div class="d-lg-flex mb-3">

			    	<div class="w-100">
			    		<label>Nome</label>
			    		<input type="text" name="nome" class="form-control p-form" placeholder="Nome" required value="{{ old('nome') ?? $user->nome }}">
			    	</div>
			    	<div class="w-100 mx-lg-3 my-3 my-lg-0">
			    		<label>Email</label>
			    		<input type="email" name="email" class="form-control p-form" placeholder="Email" required value="{{ old('email') ?? $user->email }}">
			    	</div>
			    	<div class="w-lg-45">
			    		<label>Nova Senha</label>
			    		<input type="text" name="new_password" class="form-control p-form" placeholder="Nova Senha" minlength="3">
			    	</div>
			    	<div class="w-lg-45 ms-lg-3 mt-3 mt-lg-0">
			    		<label>Status de Matrícula</label>
			    		<select name="status" class="select-form">
			    			<option value="0">Ativo</option>
			    			<option value="1" @if(old('status') == 1  || $user->status == 1) selected @endif>Desativado</option>
			    		</select>
			    	</div>

			    </div>

			    <div class="d-lg-flex mb-3">

					<div class="w-100">
						<label>CPF</label>
						<input type="text" name="cpf" class="cpf form-control p-form" placeholder="000.000.000-00" minlength="14" maxlength="14" value="{{ old('cpf') ?? $user->cpf }}">
					</div>
					<div class="w-100 mx-lg-3 my-3 my-lg-0">
						<label>Data de Nascimento</label>
						<input type="date" name="data_nascimento" class="form-control p-form" value="{{ old('data_nascimento') ?? $user->data_nascimento }}">
					</div>
					<div class="w-100">
						<label>Celular</label>
						<input type="text" name="celular" class="phone form-control p-form" placeholder="(00) 00000-0000" value="{{ old('celular') ?? $user->celular }}">
					</div>
					<div class="w-100 ms-lg-3 mt-3 mt-lg-0">
						<label>Telefone</label>
						<input type="text" name="telefone" class="phone form-control p-form" placeholder="(00) 0000-0000" value="{{ old('telefone') ?? $user->telefone }}">
					</div>

			    </div>

			    <div class="d-lg-flex mb-3">

					<div class="w-100">
						<label>RG</label>
						<input type="text" name="rg" class="form-control p-form" placeholder="Registro Geral" value="{{ old('rg') ?? $user->rg }}">
					</div>

					<div class="w-100 ms-lg-3 mt-3 mt-lg-0">
						<label>Naturalidade</label>
						<input type="text" name="naturalidade" class="form-control p-form" placeholder="Cidade - Estado" value="{{ old('naturalidade') ?? $user->naturalidade }}">
					</div>

					<div class="w-100 ms-lg-3 mt-3 mt-lg-0">
						<label>Gênero</label>
						<select name="genero" class="select-form">
							<option value="">Selecione</option>
							<option value="M" @if(old('genero') == 'M' || $user->genero == 'M') selected @endif>Masculino</option>
							<option value="F" @if(old('genero') == 'F' || $user->genero == 'F') selected @endif>Feminino</option>
						</select>
					</div>

			    	<div class="w-100 ms-lg-3 mt-lg-0">
			    		<label>Profissão</label>
			    		<input type="text" name="profissao" class="form-control p-form" placeholder="Profissão" value="{{ old('profissao') ?? $user->profissao }}">
			    	</div>

			    </div>

				<div class="mb-3">
					<label>Descrição</label>
					<textarea name="descricao" class="textarea-form" placeholder="Descrição">{{ old('descricao') ?? $user->descricao }}</textarea>
				</div>
				
				<div class="mb-3">
					<label>Observações</label>
					<textarea name="obs" class="textarea-form" placeholder="Observações">{{ old('obs') ?? $user->obs }}</textarea>
				</div>

			</div>

			@if(\App\Models\Unidade::checkActive() === true)
			<div class="mb-3 boxUnidadeRemove">
				<label>Unidade</label>
				<select name="unidade_id" id="unidade_id" class="selectpicker" data-width="100%">
					<option value="">Selecione</option>
					@foreach($unidades as $unidade)
						<option value="{{ $unidade->id }}" @if(old('unidade_id') == $unidade->id || $user->unidade_id == $unidade->id) selected @endif>
							{{ $unidade->nome }}
						</option>
					@endforeach
				</select>
			</div>
			@endif

		</section>

   <section class="mb-4-5">
      
      <a href="#boxEndereco" data-bs-toggle="collapse" aria-expanded="false" class="hover-d-none">
         <div class="section-title">
            Endereço
         </div>
      </a>
      <hr/>

      <div class="collapse show" id="boxEndereco">

			<div class="d-lg-flex mb-3">
				
				<div class="w-lg-50">
					<label>CEP</label>
					<input type="text" name="endereco[cep]" id="cep" class="cep form-control p-form" placeholder="CEP" minlength="8" maxlength="9" onkeyup="pesquisacep(this.value);" value="{{ old('endereco.cep') ?? $endereco->cep ?? null }}">
				</div>
				<div class="w-100 mx-lg-3 my-3 my-lg-0">
					<label>Rua</label>
					<input type="text" name="endereco[rua]" id="rua" class="form-control p-form" placeholder="Rua" value="{{ old('endereco.rua') ?? $endereco->rua ?? null }}">
				</div>
				<div class="w-lg-30">
					<label>Estado</label>
					<input type="text" name="endereco[estado]" id="estado" class="form-control p-form" placeholder="UF" minlength="2" maxlength="2" onkeyup="this.value = this.value.toUpperCase();" value="{{ old('endereco.estado') ?? $endereco->estado ?? null }}">
				</div>
				<div class="w-100 mx-lg-3 my-3 my-lg-0">
					<label>Bairro</label>
					<input type="text" name="endereco[bairro]" id="bairro" class="form-control p-form" placeholder="Bairro" value="{{ old('endereco.bairro') ?? $endereco->bairro ?? null }}">
				</div>
				<div class="w-100">
					<label>Cidade</label>
					<input type="text" name="endereco[cidade]" id="cidade" class="form-control p-form" placeholder="Cidade" value="{{ old('endereco.cidade') ?? $endereco->cidade ?? null }}">
				</div>
			</div>


			<div class="d-lg-flex">

				<div class="w-lg-30">
					<label>Nº</label>
					<input type="text" name="endereco[numero]" id="numero" class="form-control p-form" placeholder="Nº" value="{{ old('endereco.numero') ?? $endereco->numero ?? null }}">
				</div>
				<div class="w-lg-50 mx-lg-3 my-3 my-lg-0">
					<label>Complemento</label>
					<input type="text" name="endereco[complemento]" id="complemento" class="form-control p-form" placeholder="Complemento" value="{{ old('endereco.complemento') ?? $endereco->complemento ?? null }}">
				</div>
				<div class="w-lg-80">
					<label>Ponto de Referência</label>
					<input type="text" name="endereco[ponto_referencia]" id="ponto_referencia" class="form-control p-form" placeholder="Ponto de Referência" value="{{ old('endereco.ponto_referencia') ?? $endereco->ponto_referencia ?? null }}">
				</div>

			</div>

      </div>

   </section>

		<section class="mb-4-5">
			
			<a href="#boxCategorias" data-bs-toggle="collapse" aria-expanded="false" class="hover-d-none">
				<div class="section-title">
					Categorias 
				</div>
			</a>
			<hr/>

			<div class="collapse" id="boxCategorias">
				<select name="categoriasSelected[]" class="selectpicker" multiple data-width="100%" data-live-search="true" title="Selecione">
					@foreach($categorias as $categoria)
						<option value="{{ $categoria->id }}" @if(in_array($categoria->id, $categoriasSelected)) selected @endif>{{ $categoria->nome }}</option>
					@endforeach
				</select>
			</div>

		</section>

		<button type="submit" class="btn-add mt-3">Salvar Alterações</button>	

    </form>

    <div class="modal fade" id="ModalPlanoMaxQntAlunosExcedido" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header bg-warning">
                    <h3 class="modal-title text-dark weight-600" id="exampleModalLabel">Plano Excedido</h3>
                </div>
                <div class="modal-body py-4 px-4">
                    <p class="fs-17 mb-0">
                        O seu plano atual foi excedido.
                        <br>
                        Para que possamos continuar atendê-lo com qualidade dos serviços, 
                        é necessário que contrate um novo plano para o nosso servidor suportar a sua quantidade de alunos atuais, mantendo a segurança dos seus dados e a alta perfomance do software.
                    </p>
                </div>
                <div class="modal-footer">
                    <a href="{{ route('admin.billing') }}" class="btn btn-success fs-16 weight-600 py-2 text-shadow-1 box-shadow">
                        Contratar um novo plano
                    </a>
                </div>
            </div>
        </div>
    </div>

</div>


@endsection

@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.14.0-beta2/dist/js/bootstrap-select.min.js"></script>
<script>
	
	$(".cpf").mask('000.000.000-00');
	$(".cep").mask('00000-000');

	@if(\Auth::user()->restringir_unidade == 'S' && \Auth::user()->unidade_id != null)
		jQuery("#unidade_id option:contains('Selecione')").remove();
	@endif

	@if($planoMaxQntAlunosExcedido == true)
		var myModal = new bootstrap.Modal(document.getElementById("ModalPlanoMaxQntAlunosExcedido"), {});
		document.onreadystatechange = function () {
		  myModal.show();
		};
	@endif


    function meu_callback(conteudo) {

      if (!("erro" in conteudo)) {
      
          document.getElementById('rua').value=(conteudo.logradouro);
          document.getElementById('bairro').value=(conteudo.bairro);
          document.getElementById('cidade').value=(conteudo.localidade);
          document.getElementById('estado').value=(conteudo.uf);
          document.getElementById('numero').focus();

      } else {

            document.getElementById('rua').value=("");
            document.getElementById('bairro').value=("");
            document.getElementById('cidade').value=("");
            document.getElementById('estado').value=("");
      }
    
    }
        
    function pesquisacep(valor) {

        var cep = valor.replace(/\D/g, '');

        if (cep != "" && cep.length >= 8) {

            var validacep = /^[0-9]{8}$/;

            document.getElementById('rua').value="...";
            document.getElementById('bairro').value="...";
            document.getElementById('cidade').value="...";
            document.getElementById('estado').value="...";

            var script = document.createElement('script');
            script.src = 'https://viacep.com.br/ws/'+ cep + '/json/?callback=meu_callback';
            document.body.appendChild(script);
        } 
        
    }

</script>
@endpush