<?php

namespace App\Http\Livewire\Admin\Ajuda\Tutorial;

use Livewire\WithPagination;
use Livewire\Component;
use App\Models\Tutorial;

class Lista extends Component
{
    use WithPagination;

    protected $paginationTheme = 'bootstrap';

    public $status;
    public $ordem = 'desc';

    protected $queryString = [
        'status',
        'ordem',
    ];


    public function render()
    {
        $tutoriais = Tutorial::orderBy('id', $this->ordem)->ativo()->paginate(10);

        return view('livewire.admin.ajuda.tutorial.lista', [
            'tutoriais'      => $tutoriais,
        ]);
    }
}
