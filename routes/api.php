<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\MigracaoController;
use App\Http\Controllers\API\APIPlataformaController;
use App\Http\Controllers\API\APIAlunoController;
use App\Http\Controllers\API\APIAulaController;
use App\Http\Controllers\API\APICategoriaController;
use App\Http\Controllers\API\APIContatoController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
*/

Route::any('/contato/add', [APIContatoController::class, 'addContato'])->name('api.contato.add');
Route::any('/contato/add/greatpages/fadec', [APIContatoController::class, 'addContatoGreatpagesFadec'])->name('api.contato.add.greatpages.fadec');

Route::get('/subcategoria/lista', [APICategoriaController::class, 'listaSubcategorias'])->name('api.subcategoria.lista');

Route::group(['prefix' => 'migracao'], function() {
	Route::any('/cursos', [MigracaoController::class, 'cursos'])->name('migracao.cursos');
	Route::any('questoes', [MigracaoController::class, 'questoes'])->name('migracao.questoes');
	Route::any('avaliacoes', [MigracaoController::class, 'avaliacoes'])->name('migracao.avaliacoes');
	Route::any('avaliacoes-alunos', [MigracaoController::class, 'avaliacoesAlunos'])->name('migracao.avaliacoes-alunos');
	Route::any('dados-principais', [MigracaoController::class, 'dadosPrincipais'])->name('migracao.dados-principais');
	Route::any('users-com-documentos', [MigracaoController::class, 'usersComDocumentos'])->name('migracao.users-com-documentos');
	Route::any('contatos', [MigracaoController::class, 'contatos'])->name('migracao.contatos');
});

Route::group(['prefix' => 'plataforma'], function() {
	Route::get('info/{id}', [APIPlataformaController::class, 'info'])->name('api.plataforma.info');
});

Route::group(['prefix' => 'aluno'], function() {
	Route::get('lista', [APIAlunoController::class, 'lista'])->name('api.aluno.lista');
	Route::post('add', [APIAlunoController::class, 'add'])->name('api.aluno.add');
	Route::put('editar/{email}', [APIAlunoController::class, 'editar'])->name('api.aluno.editar');
	Route::get('info/{email}', [APIAlunoController::class, 'info'])->name('api.aluno.info');
	Route::get('info/{email}/financeiro', [APIAlunoController::class, 'infoFinanceiro'])->name('api.aluno.info.financeiro');
	Route::delete('delete/{email}', [APIAlunoController::class, 'delete'])->name('api.aluno.delete');
});

Route::get('/aula-by-id', [APIAulaController::class, 'getAulaById'])->name('api.aula-by-id');
Route::put('/marcar-como-conluida', [APIAulaController::class, 'marcarComoConcluida'])->name('api.marcar-como-conluida');
Route::put('/desmarcar-como-conluida', [APIAulaController::class, 'desmarcarComoConcluida'])->name('api.desmarcar-como-conluida');
