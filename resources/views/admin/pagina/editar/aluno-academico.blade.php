@extends('layouts.admin.master')
@section('content')

<nav aria-label="breadcrumb" class="mb-4-5 p-3 bg-db rounded ashadow border">
	<ol class="breadcrumb p-0 m-0">
    	<li class="breadcrumb-item"><a href="{{ route('admin.pagina.lista') }}" class="color-blue-info-link">Páginas</a></li>
    	<li class="breadcrumb-item text-dark" aria-current="page">Aluno > Acadêmico</li>
  	</ol>
</nav>

<form method="POST">
@csrf

	<section class="mb-4-5">

		<div class="d-lg-flex align-items-center justify-content-between section-title">
			<span>
				Links de Navegação
			</span>
			<a href="#ModalLinkAdd" data-bs-toggle="modal" class="btn btn-primary">
				+ Adicionar
			</a>
		</div>

        <div class="table-responsive bg-white px-4 py-3 border">
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">Página</th>
                        <th scope="col">
                            <span class="float-end">
                                Ações
                            </span>
                        </th>
                    </tr>
                </thead>
                <tbody>
                	@foreach($links as $link)
                    <tr>
                        <td>
                            <a href="{{ $link->link }}" target="_blank" class="color-blue-info-link">
                                {{ $link->titulo }}
                            </a>
                        </td>
                        <td>
                            <span class="float-end">
                               
                                <a href="#ModalLinkEditar" data-bs-toggle="modal" class="btn btn-warning text-dark"
                                	onclick="document.getElementById('editar_id').value = '{{ $link->id }}'; document.getElementById('editar_titulo').value = '{{ $link->titulo }}'; document.getElementById('editar_link').value = '{{ $link->link }}'; document.getElementById('editar_ordem').value = '{{ $link->ordem }}';">
                                    <i class="fa fa-edit"></i>
                                </a>

                                <a href="#ModalLinkDeletar" data-bs-toggle="modal" class="btn btn-danger"
                                	onclick="deletar()">
                                    <i class="fa fa-trash"></i>
                                </a>
                                <script>
                                	function deletar()
                                	{
                                		document.getElementById('delete_id').value = '{{ $link->id }}';
                                		document.getElementById('delete_titulo').innerHTML = '{{ $link->titulo }}';
                                	}
                                </script>

                            </span>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

	</section>

</form>


<form method="POST">
@csrf
<div class="modal fade" id="ModalLinkAdd" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title" id="exampleModalLabel">Adicionar Link de Navegação</h3>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body d-lg-flex">
		<input type="text" name="titulo" class="form-control p-form" placeholder="Título" required>
		<input type="text" name="link" class="mx-lg-3 my-3 my-lg-0 form-control p-form" placeholder="Link" required>
		<input type="number" name="ordem" class="w-lg-25 form-control p-form" placeholder="Ordem">
      </div>
      <div class="modal-footer">
        <button type="submit" name="add" value="ok" class="btn btn-primary">Confirmar</button>
      </div>
    </div>
  </div>
</div>
</form>

<form method="POST">
@csrf
<input type="hidden" name="editar_id" id="editar_id">
<div class="modal fade" id="ModalLinkEditar" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title" id="exampleModalLabel">Editar Link de Navegação</h3>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body d-lg-flex">
		<input type="text" name="titulo" id="editar_titulo" class="form-control p-form" placeholder="Título" required>
		<input type="text" name="link" id="editar_link" class="mx-lg-3 my-3 my-lg-0 form-control p-form" placeholder="Link" required>
		<input type="number" name="ordem" id="editar_ordem" class="w-lg-25 form-control p-form" placeholder="Ordem">
      </div>
      <div class="modal-footer">
        <button type="submit" name="editar" value="ok" class="btn btn-primary">Salvar Alterações</button>
      </div>
    </div>
  </div>
</div>
</form>

<form method="POST">
@csrf
<input type="hidden" name="delete_id" id="delete_id">
<div class="modal fade" id="ModalLinkDeletar" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title" id="exampleModalLabel">Deletar Link de Navegação</h3>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body text-center py-5">
		<p class="m-0 fs-15">
			<span class="weight-600" id="delete_titulo"></span>
			<br>
			Você tem certeza que deseja deletar este Link?
		</p>
      </div>
      <div class="modal-footer">
        <button type="submit" name="delete" value="ok" class="btn btn-danger">Confirmar</button>
      </div>
    </div>
  </div>
</div>
</form>

@push('scripts')

@endpush

@endsection