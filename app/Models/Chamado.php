<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Chamado extends Model
{
    use HasFactory;

    public $timestamps = true;

    protected $table = 'chamados';

    public $fillable = [
    	'plataforma_id',
        'cadastrante_id',
        'assunto',
        'descricao',
        'status',
        'arquivado',
    ];

    public static  function boot()
    {
        parent::boot(); 
        static::creating(function ($model)
        {
            $model->plataforma_id = session('plataforma_id') ?? \Auth()->user()->plataforma_id ?? Plataforma::dominio()->pluck('id')[0] ?? null;
            $model->cadastrante_id = \Auth()->user()->id ?? null;
        });
    }
    
    public function scopePlataforma($query)
    {
        return $query->where('plataforma_id', session('plataforma_id') ?? Plataforma::dominio()->pluck('id')[0] ?? null);
    }

    public function scopeNaoArquivados($query)
    {
        return $query->where('arquivado', 'N');
    }

    public function scopePendente($query)
    {
        return $query->where('status', 0);
    }

    public function getStatusNome($status)
    {
        switch ($status)
        {
            case 0:
                return 'Pendente';

            case 1:
                return 'Em análise';

            case 2:
                return 'Atendido';

            case 3:
                return 'Desativado';
        }
    }

    public function getStatusColor($status)
    {
        switch ($status)
        {
            case 0:
                return 'text-danger';

            case 1:
                return 'text-primary';

            case 2:
                return 'text-success';

            case 3:
                return 'text-secondary';
        }
    }

}
