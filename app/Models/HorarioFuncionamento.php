<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HorarioFuncionamento extends Model
{
	protected $table = 'horarios_funcionamentos';

    public $timestamps = false;

    public $fillable = [
    	'plataforma_id',
    	'dia',
    	'de', 
    	'ate',
    	'fechado',
    ];

    public function scopePlataforma($query)
    {
        return $query->where('plataforma_id', session('plataforma_id') ?? \Auth::user()->plataforma_id);
    }
}
