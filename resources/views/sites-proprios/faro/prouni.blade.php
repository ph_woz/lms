<!doctype html>
<html lang="pt-br">
<head>

   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1">

   <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/uikit@3.5.9/dist/css/uikit.min.css" />
   <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
   <link rel="stylesheet" href="{{ asset('css/util.css') }}?v=3">
   <link rel="stylesheet" href="{{ asset('sites-proprios/faro/css/home.css') }}?v=3">

   <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">

   <title>{{ $seo->seo_title ?? $plataforma->nome }}</title>
   <meta name="robots" content="index, follow">
   <meta name="description" content="{{ $seo->seo_description ?? null }}"/>
   <meta name="keywords" content="{{ $seo->seo_keywords ?? null }}"/>
   <link rel="canonical" href="{{ Request::url() }}" />
   <link rel="shortcut icon" href="{{ $plataforma->favicon }}">
   <meta property="og:locale" content="pt_BR" />
   <meta property="og:type" content="website" />
   <meta property="og:title" content="{{ $seo->seo_title ?? $plataforma->nome }}" />
   <meta property="og:description" content="{{ $seo->seo_description ?? null }}" />
   <meta property="og:url" content="{{ Request::url() }}" />
   <meta property="og:site_name" content="{{ $plataforma->nome }}" />
   <meta property="og:image" content="{{ $plataforma->logotipo }}" />
   <meta name='author' content='WOZCODE | Pedro Henrique' />

</head>
<body class="bg-f4" id="inicio">

   @include('sites-proprios/faro/componentes/header')

   <main class="pb-5">

      <section class="w-100 h-lg-450 background-size-cover background-position-center" style="background-image: url('{{ $plataforma->jumbotron ?? asset('sites-proprios/faro/images/jumbotron.jpeg') }}');">
              <div class="position-relative h-100">
              <div class="background-overlay opacity-08 h-100" style="background: rgba(0,0,0,1);"></div>
               <div class="container h-100">
                 <div class="row container align-items-center h-100 position-relative">
                    <div class="mt--lg-75px py-5 py-lg-0">
                       <h1 class="text-light font-poppins weight-800 fs-xs-35 fs-md-50 text-shadow-3 col-lg-6" id="tituloProUni">
                          ProUni
                       </h1>
                       <h2 class="text-light font-poppins weight-600 fs-20 text-shadow-3 col-lg-6">
                           Bolsas de até 100% para os melhores colocados no ENEM
                       </h2>
                    </div>
                 </div>
              </div>
            </div>
      </section>


      <section class="py-5 z-3 position-relative mt--lg-125px">
         <div class="container">
            <div class="p-lg-5 p-4 bg-white box-shadow border rounded-20">
               <div class="row">
                  <div class="col-lg-7">
                     <h1 class="color-base font-poppins weight-800 fs-35">Como Funciona</h1>
                     <p class="font-opensans fs-22 color-555">
                       O ProUni, criado pelo Governo Federal, tem o objetivo de conceder bolsas de estudo integrais em cursos de graduação e sequenciais de formação específica. O programa conta com um sistema de seleção informatizado e impessoal, que confere transparência e segurança ao processo. Os candidatos são selecionados pelas notas obtidas no ENEM – Exame Nacional do Ensino Médio, desse modo, reconhece os estudantes com melhores desempenhos acadêmicos.
                     </p>
                     <div class="mt-4 mb-3 mb-lg-0">
                        <a href="/f/formulario-de-matricula/45/t/631" target="_blank" class="bg-faixa bg-hover-gradient fs-14 text-center px-3-5 rounded box-shadow text-light weight-700 py-2-5 font-poppins hover-d-none d-xs-block">
                           QUERO ME INSCREVER
                        </a>
                        <a href="https://api.whatsapp.com/send?phone=55{{preg_replace('/\D/', '', $plataforma->whatsapp)}}&text=Olá, vim através do Site {{ $plataforma->nome }}" target="_blank" class="ms-lg-3 bg-wpp text-shadow-1 fs-14 text-center px-3-5 rounded box-shadow text-light weight-700 py-2-5 font-poppins hover-d-none d-xs-block mt-2 mt-lg-0">
                           <span class="me-2">QUERO RECEBER UMA MENSAGEM</span>
                           <i class="fab fa-whatsapp d-xs-none"></i>
                        </a>
                     </div>
                  </div>
                  <div class="col-lg-5 text-xs-center">
                     <div class="mt--lg-175px">

                        <div class="mb-4">
                           <img src="{{ asset('sites-proprios/faro/images/mulher.jpg') }}" alt="Faro - Mulher" class="img-fluid rounded-20">
                        </div>

                        <div class="d-lg-flex align-items-center justify-content-end">

                           <div class="mb-3 mb-lg-0 me-lg-4">
                              <p class="mb-0 color-azul weight-700 font-poppins">
                                 Compartilhe com um amigo
                              </p>
                           </div>
                           <div class="d-flex justify-content-center">
                              <a href="{{ $plataforma->facebook }}" target="_blank" class="hover-d-none">
                                 <div class="bg-faixa square-40 flex-center rounded-circle">
                                    <i class="fab fa-facebook-f color-icon-footer fs-20"></i>
                                 </div>
                              </a>
                              <a href="" target="_blank" class="mx-2 hover-d-none">
                                 <div class="bg-faixa square-40 flex-center rounded-circle">
                                    <i class="fab fa-linkedin-in color-icon-footer fs-20"></i>
                                 </div>
                              </a>
                              <a href="" target="_blank" class="hover-d-none">
                                 <div class="bg-faixa square-40 flex-center rounded-circle">
                                    <i class="fab fa-whatsapp color-icon-footer fs-20"></i>
                                 </div>
                              </a>
                           </div>

                        </div>

                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>

      <section class="p-lg-5 p-3 bg-f5">
         <div class="container">

            <h1 class="text-center text-dark font-poppins weight-700">
               Quais são os requisitos necessários?
            </h1>

            <div class="mt-5 row align-items-center">

               <div class="col-lg-5">

                  <div class="mb-4 d-flex align-items-center bg-white box-shadow rounded-20 p-4">
                     <i class="far fa-check-circle fs-30 me-3 color-azul"></i>
                     <p class="mb-0 font-poppins weight-600 fs-17">
                        Poderá se inscrever aos processos seletivos do ProUni somente o estudante brasileiro não portador de diploma de curso superior que tenha participado do Exame Nacional do Ensino Médio – Enem.
                     </p>
                  </div>
                  <div class="mb-4 d-flex align-items-center bg-white box-shadow rounded-20 p-4">
                     <i class="far fa-check-circle fs-30 me-3 color-azul"></i>
                     <p class="mb-0 font-poppins weight-600 fs-17">
                        Ter nota mínima de 450 pontos e não ter zerado a redação.
                     </p>
                  </div>
                  <div class="mb-4 d-flex align-items-center bg-white box-shadow rounded-20 p-4">
                     <i class="far fa-check-circle fs-30 me-3 color-azul"></i>
                     <p class="mb-0 font-poppins weight-600 fs-17">
                        Não possuir vínculo com Instituições Federais de Ensino Superior (IFES).
                     </p>
                  </div>
                  <div class="mb-4 d-flex align-items-center bg-white box-shadow rounded-20 p-4">
                     <i class="far fa-check-circle fs-30 me-3 color-azul"></i>
                     <p class="mb-0 font-poppins weight-600 fs-17">
                        Cursar o ensino médio integralmente em instituição privada, na condição de bolsista parcial da respectiva instituição ou sem a condição de bolsista.
                     </p>
                  </div>

               </div>

               <div class="col-lg-6 offset-lg-1" align="right">

                  <img src="{{ asset('sites-proprios/faro/images/plataforma-elearning.png') }}" aclass="img-fluid">

               </div>

            </div>

         </div>
      </section>

      <section class="p-lg-5 p-3 mb-5 bg-f5">
         <div class="container">

            <h2 class="text-center text-dark font-poppins weight-500 fs-20">Confira o passo a passo</h2>
            <h1 class="text-center text-dark font-poppins weight-700">
               Como participar do FIES
            </h1>

            <div class="row align-items-center px-xl-7" style="margin-top: 75px;">

               <div class="col-lg-4 mb-5 mb-lg-0">

                  <div class="bg-white p-4 box-shadow rounded-10 h-lg-350">
                     <div class="text-center">

                        <div class="d-flex justify-content-center">
                           <div class="square-75 rounded-circle bg-gradient flex-center mt--60px">
                              <span class="fs-50">1</span>
                           </div>
                        </div>

                        <h1 class="mb-0 fs-25 weight-700 mt-3 font-opensans">
                           Escolha seu curso
                        </h1>

                        <div class="flex-center mt-3"><div class="bg-gradient" style="height: 2px;width: 50px;"></div></div>

                        <p class="mt-3 weight-600 font-opensans">
                           Confira a oferta, e o nome dos cursos e unidades que você encontrará na ficha de inscrição do PROUNI. Os dados estão exatamente iguais nesta tabela.
                        </p>
                     
                     </div>
                  </div>

               </div>

               <div class="col-lg-4 mb-5 mb-lg-0">

                  <div class="bg-white p-4 box-shadow rounded-10 h-lg-350">
                     <div class="text-center">

                        <div class="d-flex justify-content-center">
                           <div class="square-75 rounded-circle bg-gradient flex-center mt--60px">
                              <span class="fs-50">2</span>
                           </div>
                        </div>

                        <h1 class="mb-0 fs-25 weight-700 mt-3 font-opensans">
                           Portal Único de Acesso ao Ensino Superior
                        </h1>

                        <div class="flex-center mt-3"><div class="bg-gradient" style="height: 2px;width: 50px;"></div></div>

                        <p class="mt-3 weight-600 font-opensans">
                           Acesse aqui o Portal do Prouni no MEC, entre os dias 01 de agosto até 05 de agosto de 2022, siga os passos e informe os dados que você levantou no passo anterior.
                        </p>
                     
                     </div>
                  </div>

               </div>

               <div class="col-lg-4 mb-5 mb-lg-0">

                  <div class="bg-white p-4 box-shadow rounded-10 h-lg-350">
                     <div class="text-center">

                        <div class="d-flex justify-content-center">
                           <div class="square-75 rounded-circle bg-gradient flex-center mt--60px">
                              <span class="fs-50">3</span>
                           </div>
                        </div>

                        <h1 class="mb-0 fs-25 weight-700 mt-3 font-opensans">
                           Acompanhe seu processo
                        </h1>

                        <div class="flex-center mt-3"><div class="bg-gradient" style="height: 2px;width: 50px;"></div></div>

                        <p class="mt-3 weight-600 font-opensans">
                           Acompanhe seu processo seletivo e fique de olho no resultado de aprovação da chamada regular.
                        </p>
                     
                     </div>
                  </div>

               </div>

            </div>

         </div>
      </section>

      <section class="px-lg-5 py-lg-5 py-4 px-3 mb-lg-5 mb-3 bg-gradient">
         <div class="container">

            <h2 class="text-center text-dark font-poppins weight-500 fs-20">Confira informações do</h2>
            <h1 class="text-center text-dark font-poppins weight-700">
               ProUni
            </h1>

            <div id="myGroup">
               @foreach($secoes as $secao)
                  <button class="btn-info m-2 outline-0" type="button" data-bs-toggle="collapse" data-bs-target="#collapseSecao{{$secao->id}}" aria-expanded="false" aria-controls="collapseWidthExample">
                     {{ $secao->titulo }}
                  </button>
               @endforeach

               @foreach($secoes as $secao)            
               <div class="collapse" id="collapseSecao{{$secao->id}}">
                  <div class="bg-white p-lg-5 p-4 rounded-20 box-shadow mt-5">

                     <h1 class="color-base font-poppins weight-800 fs-md-35 fs-xs-30">{{ $secao->titulo }}</h1>
                     <p class="font-opensans fs-21 color-555">
                        {!!  nl2br($secao->descricao) !!}
                     </p>

                  </div>
               </div>
               @endforeach
            </div>

         </div>
      </section>

      <section class="py-5">
         <div class="container">
            <div class="bg-gradient rounded-top-20 box-shadow p-5">
               <h1 class="text-dark font-poppins weight-800 fs-35">Transfira-se</h1>
                <h2 class="text-dark font-poppins weight-500 fs-25">também com ProUni</h2>
            </div>
            <div class="bg-white p-lg-4 px-1 pb-1 box-shadow">
               <p class="px-4 pb-4 font-opensans fs-21 color-555">
                  {!! nl2br('

                     <b>Saiba mais sobre o processo de transferência externa</b>

                     A Portaria Normativa N° 19, de 20 de novembro de 2008 do MEC, estabelece que o processo de transferências para bolsistas ProUni e de critério de cada instituição de ensino. A Faculdade de Roseira já encontra dentro do limite exigido pelo MEC, dessa forma não aceitaremos transferência externas de bolsas para o primeiro semestre de 2021.

                     Manual do Bolsista (acesse aqui) Transferência.

                     O bolsista do Prouni poderá transferir a utilização da sua bolsa de estudo para outro curso afim e, ainda, que para turno, campus ou mesmo outra instituição de ensino.

                     Para que a transferência seja efetivada é necessário que:

                     As instituições de origem e de destino estejam de acordo com a transferência;
                     A instituição e o respectivo curso para o qual o estudante deseja se transferir estejam regularmente credenciados no Programa;
                     Exista vaga no curso para o qual o estudante deseja se transferir.
                     O processo de transferência somente é considerado concluído após a formalização da aceitação do estudante pela instituição de ensino de destino, por meio da emissão do Termo de Transferência do Usufruto de Bolsa. Uma vez concluída a transferência, o prazo de utilização da bolsa passará a ser o prazo do curso de destino, subtraído o período utilizado e suspenso no curso de origem.

                     O procedimento de transferência de bolsa é de caráter interno das instituições de ensino envolvidas, efetivando-se no âmbito do Sisprouni, não cabendo intermediação do MEC.
                  ') !!}
               </p>
            </div>
            <div class="bg-gradient rounded-bottom-20 box-shadow p-lg-5 p-4">
               <div class="d-lg-flex justify-content-between align-items-center">
                  
                  <div class="w-100">
                  
                     <h2 class="text-dark font-poppins weight-500 fs-20">
                        Transferência Externa
                     </h2>
                     
                     <p class="text-dark font-poppins weight-500">
                        Informamos que a Faculdade de Roseira está aceitando transferência externa de aluno bolsista do Prouni (integral ou parcial), pois já estamos com a quantidade de alunos suficientes de acordo com a regra do MEC. A Instituição tem o direito de aceitar ou não conforme o Manual do Bolsista.
                     </p>

                  </div>

                  <div class="d-xs-none px-lg-5">
                     <div class="h-lg-300 w-2px bg-white"></div>
                  </div>

                  <div class="my-4 d-lg-none px-lg-5">
                     <div class="w-100 h-2px bg-white"></div>
                  </div>

                  <div class="w-100">
                  
                     <h2 class="text-dark font-poppins weight-500 fs-20">
                        Transferência Interna
                     </h2>
                     
                     <p class="text-dark font-poppins weight-500">
                        Você não pode ter concluído mais de 75% do curso que está solicitando a bolsa. E, fique de olho, a permissão ocorre somente para transferência de curso para área afim de acordo com a Tabela Capes.
                     </p>

                  </div>

               </div>
            </div>
         </div>
      </section>

      <section class="p-5 bg-gradient">
         <div class="container">

            <h1 class="text-center text-dark font-poppins weight-700">
               Perguntas Frequentes
            </h1>

            <p class="mb-0 text-center font-poppins text-dark fs-17 col-md-10 offset-md-1">
               Aqui estão todas as informações necessárias para você tirar suas dúvidas sobre nosso processo de ensino.
            </p>

            <div class="pt-4-5 px-xl-8 ml-lg-2">

               <a href="#collapseFaq1" data-bs-toggle="collapse" href="#collapseFaq1" class="text-d-none hover-d-none text-dark font-poppins weight-600 fs-16">
                  <div class="mb-2 bg-white p-3 rounded box-shadow">
                     <i class="fas fa-caret-right"></i>
                     <span class="fs-17">O que é ProUni?</span>
                     <p class="collapse mb-0 mt-1 font-poppins text-dark weight-500" id="collapseFaq1">
                        O Prouni – Programa Universidade para Todos – é um sistema que faz concede bolsas de estudo integrais e parciais de 50% em instituições privadas de educação superior, em cursos de graduação e sequenciais de formação específica a estudantes brasileiros, desde que ainda não tenham diploma de nível superior.
                     </p>
                  </div>
               </a>
               <a href="#collapseFaq2" data-bs-toggle="collapse" href="#collapseFaq2" class="text-d-none hover-d-none text-dark font-poppins weight-600 fs-16">
                  <div class="mb-2 bg-white p-3 rounded box-shadow">
                     <i class="fas fa-caret-right"></i>
                     <span class="fs-17">Fiz o ENEM há 2 anos, posso tentar o ProUni?</span>
                     <p class="collapse mb-0 mt-1 font-poppins text-dark weight-500" id="collapseFaq2">
                        Não. O Prouni só aceita a nota do Enem do ano anterior ao processo de concessão da bolsa.
                     </p>
                  </div>
               </a>
               <a href="#collapseFaq3" data-bs-toggle="collapse" href="#collapseFaq3" class="text-d-none hover-d-none text-dark font-poppins weight-600 fs-16">
                  <div class="mb-2 bg-white p-3 rounded box-shadow">
                     <i class="fas fa-caret-right"></i>
                     <span class="fs-17">Qual a menor nota aceita para solicitar o ProUni?</span>
                     <p class="collapse mb-0 mt-1 font-poppins text-dark weight-500" id="collapseFaq3">
                        Sua nota tem que ser de no mínimo de 450 pontos e você não pode ter zerado a redação.
                     </p>
                  </div>
               </a>
               <a href="#collapseFaq4" data-bs-toggle="collapse" href="#collapseFaq4" class="text-d-none hover-d-none text-dark font-poppins weight-600 fs-16">
                  <div class="mb-2 bg-white p-3 rounded box-shadow">
                     <i class="fas fa-caret-right"></i>
                     <span class="fs-17">Posso solicitar o ProUni antes de concluir o ensino médio?</span>
                     <p class="collapse mb-0 mt-1 font-poppins text-dark weight-500" id="collapseFaq4">
                        Não. É necessário ter cursado as 3 séries do ensino médio.
                     </p>
                  </div>
               </a>
               <a href="#collapseFaq5" data-bs-toggle="collapse" href="#collapseFaq5" class="text-d-none hover-d-none text-dark font-poppins weight-600 fs-16">
                  <div class="mb-2 bg-white p-3 rounded box-shadow">
                     <i class="fas fa-caret-right"></i>
                     <span class="fs-17">O ProUni é concedido em caso de segunda graduação?</span>
                     <p class="collapse mb-0 mt-1 font-poppins text-dark weight-500" id="collapseFaq5">
                        Não. O Prouni é exclusivo para quem não possui nível superior completo.
                     </p>
                  </div>
               </a>

            </div>

         </div>
      </section>

   </main>

   @include('sites-proprios/faro/componentes/footer')

   <a href="https://api.whatsapp.com/send?phone=55{{preg_replace('/\D/', '', $plataforma->whatsapp)}}&text=Olá, vim através do Site {{ $plataforma->nome }}" target="_blank" class="text-white fs-30">
       <div class="fixed-bottom right-0 me-3 mb-3 z-3">
           <div class="square-60 rounded-circle bg-wpp flex-center box-shadow-wpp">
               <i class="fab fa-whatsapp"></i>
           </div>
       </div>
   </a>

   <script src="https://cdn.jsdelivr.net/npm/uikit@3.5.9/dist/js/uikit.min.js"></script>
   <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-U1DAWAznBHeqEIlVSCgzq+c9gqGAJn5c/t99JyeKa9xxaYpSvHU5awsuZVVFIhvj" crossorigin="anonymous"></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>
   <script>

      var $myGroup = $('#myGroup');
      $myGroup.on('show.bs.collapse','.collapse', function() {
         $myGroup.find('.collapse.show').collapse('hide');
      });

      window.onload = function()
      {
         $(".btn-info").first().click().focus();
         window.scrollTo(0, 0);
      };

      window.onscroll = function() {myFunction()};

      var header = document.getElementById("menu");
      var sticky = header.offsetTop;

      function myFunction() {
        if (window.pageYOffset > sticky) {
          header.classList.add("fixed-top");
        } else {
          header.classList.remove("fixed-top");
        }
      }
   </script>

</body>
</html>