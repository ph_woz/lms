<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\IndexController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Admin\AlunoController;
use App\Http\Controllers\Admin\FluxoCaixaController;
use App\Http\Controllers\Admin\ColaboradorController;
use App\Http\Controllers\Admin\CursoController;
use App\Http\Controllers\Admin\TurmaController;
use App\Http\Controllers\Admin\TrilhaController;
use App\Http\Controllers\Admin\AvaliacaoController;
use App\Http\Controllers\Admin\QuestaoController;
use App\Http\Controllers\Admin\CertificadoController;
use App\Http\Controllers\Admin\ContatoController;
use App\Http\Controllers\Admin\FormularioController;
use App\Http\Controllers\Admin\PaginaController;
use App\Http\Controllers\Admin\PlataformaController;
use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\ComunicadoController;
use App\Http\Controllers\Admin\DocumentoController;
use App\Http\Controllers\Admin\UnidadeController;
use App\Http\Controllers\Admin\ResponsavelController;
use App\Http\Controllers\Admin\AjudaTutorialController;
use App\Http\Controllers\Admin\AjudaChamadoController;
use App\Http\Controllers\Admin\BillingController;
use App\Http\Controllers\Admin\RelatorioController;
use App\Http\Controllers\API\APIUtilController;
use App\Http\Controllers\Admin\PlanoController;
use App\Http\Controllers\Admin\ServicoController;
use App\Http\Controllers\Admin\ProdutoController;
use App\Http\Controllers\Admin\ContratoController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::any('/', [IndexController::class, 'index'])->name('index');
Route::any('/limpa-cache', [IndexController::class, 'limpaCache'])->name('limpaCache');

Route::get('/segunda-graduacao', [IndexController::class, 'categoriaGeral'])->name('categoriaGeral');
Route::get('/segunda-licenciatura', [IndexController::class, 'categoriaGeral'])->name('categoriaGeral');
Route::get('/formacao-pedagogica-r2', [IndexController::class, 'categoriaGeral'])->name('categoriaGeral');
Route::get('/aperfeicoamento', [IndexController::class, 'categoriaGeral'])->name('categoriaGeral');
Route::get('/graduacao', [IndexController::class, 'categoriaGeral'])->name('categoriaGeral');
Route::get('/fies', [IndexController::class, 'fies'])->name('fies');
Route::get('/ouvidoria', [IndexController::class, 'ouvidoria'])->name('ouvidoria');
Route::get('/cpa', [IndexController::class, 'cpa'])->name('cpa');
Route::get('/transferencia', [IndexController::class, 'transferencia'])->name('transferencia');
Route::get('/vestibular', [IndexController::class, 'vestibular'])->name('vestibular');
Route::get('/enem', [IndexController::class, 'enem'])->name('enem');
Route::get('/prouni', [IndexController::class, 'prouni'])->name('prouni');
Route::get('/pos-graduacao', [IndexController::class, 'categoriaGeral'])->name('categoriaGeral');
Route::get('/cursos-de-extensao', [IndexController::class, 'categoriaGeral'])->name('categoriaGeral');

Route::get('/como-funciona', [IndexController::class, 'comoFunciona'])->name('comoFunciona');
Route::get('/faq', [IndexController::class, 'faq'])->name('faq');
Route::any('/contato', [IndexController::class, 'contato'])->name('contato');
Route::get('/sobre-nos', [IndexController::class, 'sobreNos'])->name('sobreNos');
Route::get('/agradecimentos', [IndexController::class, 'agradecimentos'])->name('agradecimentos');
Route::get('/quem-somos', [IndexController::class, 'sobreNos'])->name('sobreNos');
Route::get('/biblioteca', [IndexController::class, 'biblioteca'])->name('biblioteca');
Route::get('/editais', [IndexController::class, 'editais'])->name('editais');
Route::get('/dou', [IndexController::class, 'dou'])->name('dou');
Route::get('/polos', [IndexController::class, 'polos'])->name('polos');
Route::get('/parceiros', [IndexController::class, 'parceiros'])->name('parceiros');
Route::get('/contato', [IndexController::class, 'contato'])->name('contato');
Route::get('/termos-de-uso', [IndexController::class, 'termosDeUso'])->name('termos.de.uso');
Route::get('/politica-de-privacidade', [IndexController::class, 'politicaDePrivacidade'])->name('politica.de.privacidade');
Route::get('/buscar', [IndexController::class, 'buscar'])->name('buscar');
Route::get('/categoria/{slug}/{id}', [IndexController::class, 'categoria'])->name('categoria');
Route::get('/curso/{slug}', [IndexController::class, 'curso'])->name('curso');
Route::get('/categoria/{categoria}/curso/{slug}', [IndexController::class, 'categoriaCurso'])->name('categoria.curso');
Route::get('/cursos', [IndexController::class, 'cursos'])->name('cursos');

Route::any('/consultar-diploma', [IndexController::class, 'consultarDiploma'])->name('consultar-diploma');
Route::any('/consultar-certificado', [IndexController::class, 'consultarDiploma'])->name('consultar-diploma');

Route::any('/blog', [IndexController::class, 'blog'])->name('blog');
Route::any('/blog/post/{slug}/{id}', [IndexController::class, 'blogPost'])->name('blog.post');


# Formulário - tipo: c = turma | t = trilha 
Route::any('/f/{slug}/{formulario_id}/{tipo}/{turma_id_ou_trilha_id}', [IndexController::class, 'formulario'])->name('formulario');
Route::any('/f/{formulario_id}/{tipo}/{turma_id_ou_trilha_id}', [IndexController::class, 'redirectParaFormulario'])->name('redirect.formulario');

Route::any('/login-auto/{plataforma_id}/{cpf_ou_email}', [LoginController::class, 'loginAuto'])->name('loginAuto');
Route::any('/login-by-hash/{hashcode}/{user_id}', [LoginController::class, 'loginByHash'])->name('loginByHash');
Route::any('/recuperar-senha', [LoginController::class, 'recuperarSenha'])->name('recuperar-senha');
Route::any('/redefinir-senha/token/{token}/email/{email}/id/{id}', [LoginController::class, 'redefinirSenha'])->name('redefinirSenha');

Route::any('/solicitar-assinatura-contrato-digital/token/{token}/email/{email}/id/{id}/{user_contrato_id}', [ContratoController::class, 'solicitarAssinaturaContratoDigital'])->name('solicitarAssinaturaContratoDigital');

Auth::routes();

# Logout
Route::get('/logout', [LoginController::class, 'logout'])->name('logout');

Route::group(['middleware' => 'auth'], function () {

    // Rotas da Área do Aluno do LMS
    Route::group(['prefix' => 'aluno', 'namespace'=> 'Aluno'], function(){

        # Dashboard
        Route::get('/dashboard', [App\Http\Controllers\Aluno\DashboardController::class, 'dashboard'])->name('aluno.dashboard');
        Route::any('/aceitar-termos', [App\Http\Controllers\Aluno\DashboardController::class, 'aceitarTermosDeUso'])->name('aluno.dashboard.aceitar_termos');
        
        # Aulas do Curso
        Route::get('/curso/{slug}/{curso_id}', [App\Http\Controllers\Aluno\CursoController::class, 'aulas'])->name('aluno.aulas');

        # Trilha
        Route::get('/trilha/{slug}/{trilha_id}', [App\Http\Controllers\Aluno\TrilhaController::class, 'trilha'])->name('aluno.trilha');

        # Avaliações
        Route::get('/avaliacoes', [App\Http\Controllers\Aluno\AvaliacaoController::class, 'avaliacoes'])->name('aluno.avaliacoes');
        Route::get('/avaliacoes2', [App\Http\Controllers\Aluno\AvaliacaoController::class, 'avaliacoes2'])->name('aluno.avaliacoes2');
        
        # Inicia a Avaliação
        Route::any('/avaliacao/{id}', [App\Http\Controllers\Aluno\AvaliacaoController::class, 'avaliacao'])->name('aluno.avaliacao');

        # Finaliza a Avaliação
        Route::any('/avaliacao/{id}/finalizar', [App\Http\Controllers\Aluno\AvaliacaoController::class, 'finalizar'])->name('aluno.finalizar');

        # Gabarito da Avaliação
        Route::any('/avaliacao/{id}/gabarito', [App\Http\Controllers\Aluno\AvaliacaoController::class, 'gabarito'])->name('aluno.avaliacao.gabarito');

        # Tentar Novamente - Avaliação
        Route::any('/avaliacao/{id}/reiniciar', [App\Http\Controllers\Aluno\AvaliacaoController::class, 'reiniciar'])->name('aluno.avaliacao.reiniciar');

        # Correção
        Route::any('/avaliacao/{id}/correcao/{aluno_id}', [App\Http\Controllers\Aluno\AvaliacaoController::class, 'correcao'])->name('aluno.avaliacao.correcao');

        # Gerar Certificado de Conclusão Turma
        Route::any('/gerar-certificado/{certificado_id}/turma/{turma_id}', [App\Http\Controllers\Aluno\CertificadoController::class, 'gerarCertificado'])->name('aluno.gerar-certificado');

        # Gerar Certificado de Conclusão Trilha
        Route::any('/gerar-certificado/{certificado_id}/trilha/{trilha_id}', [App\Http\Controllers\Aluno\CertificadoController::class, 'gerarCertificadoTrilha'])->name('aluno.gerar-certificado-trilha');

        # Gerar Atestado de Matrícula Trilha
        Route::any('/gerar-atestado/{atestado_id}/trilha/{trilha_id}', [App\Http\Controllers\Aluno\CertificadoController::class, 'gerarAtestadoTrilha'])->name('aluno.gerar-atestado-trilha');

        # Gerar Atestado de Matrícula Turma
        Route::any('/gerar-atestado/{atestado_id}/turma/{turma_id}', [App\Http\Controllers\Aluno\CertificadoController::class, 'gerarAtestadoTurma'])->name('aluno.gerar-atestado-turma');

        # Suporte
        Route::any('/suporte', [App\Http\Controllers\Aluno\SuporteController::class, 'suporte'])->name('aluno.suporte');    

        # Acadêmico
        Route::any('academico', [App\Http\Controllers\Aluno\AcademicoController::class, 'academico'])->name('aluno.academico');    
        Route::any('academico/documentos', [App\Http\Controllers\Aluno\AcademicoController::class, 'documentos'])->name('aluno.academico.documentos');    

        # Comunicados
        Route::any('comunicados', [App\Http\Controllers\Aluno\ComunicadoController::class, 'comunicados'])->name('aluno.comunicados');

        # Financeiro
        Route::any('financeiro', [App\Http\Controllers\Aluno\FinanceiroController::class, 'financeiro'])->name('aluno.financeiro');

        # Perfil
        Route::any('meu-perfil', [App\Http\Controllers\Aluno\MinhaContaController::class, 'meuPerfil'])->name('aluno.meu-perfil');
        Route::any('minha-conta', [App\Http\Controllers\Aluno\MinhaContaController::class, 'minhaConta'])->name('aluno.minha-conta');
        Route::any('minha-conta/alterar-senha', [App\Http\Controllers\Aluno\MinhaContaController::class, 'alterarSenha'])->name('aluno.minha-conta.alterar-senha');
        Route::any('minha-conta/experiencia/lista', [App\Http\Controllers\Aluno\MinhaContaController::class, 'experienciaLista'])->name('aluno.minha-conta.experiencia.lista');
        Route::any('minha-conta/experiencia/add', [App\Http\Controllers\Aluno\MinhaContaController::class, 'experienciaAdd'])->name('aluno.minha-conta.experiencia.add');
        Route::any('minha-conta/experiencia/editar/{id}', [App\Http\Controllers\Aluno\MinhaContaController::class, 'experienciaEditar'])->name('aluno.minha-conta.experiencia.editar');

        Route::any('minha-conta/formacao/lista', [App\Http\Controllers\Aluno\MinhaContaController::class, 'formacaoLista'])->name('aluno.minha-conta.formacao.lista');
        Route::any('minha-conta/formacao/add', [App\Http\Controllers\Aluno\MinhaContaController::class, 'formacaoAdd'])->name('aluno.minha-conta.formacao.add');
        Route::any('minha-conta/formacao/editar/{id}', [App\Http\Controllers\Aluno\MinhaContaController::class, 'formacaoEditar'])->name('aluno.minha-conta.formacao.editar');

    });

    // Rotas do Admin do LMS
    Route::group(['prefix' => 'admin', 'namespace'=> 'Admin'], function(){

    	# Dashboard
    	Route::get('/dashboard/info', [DashboardController::class, 'dashboard'])->name('admin.dashboard.info');

        # Página
        Route::get('/pagina/lista', [PaginaController::class, 'lista'])->name('admin.pagina.lista');
        Route::group(['prefix' => 'pagina/editar'], function(){
            Route::any('/home', [PaginaController::class, 'home'])->name('admin.pagina.editar.home');
            Route::any('/blog', [PaginaController::class, 'blog'])->name('admin.pagina.editar.blog');
            Route::any('/blog/add', [PaginaController::class, 'blogPostAdd'])->name('admin.pagina.editar.blog.post.add');
            Route::any('/blog/editar', [PaginaController::class, 'blogPostEditar'])->name('admin.pagina.editar.blog.post.editar');
            Route::any('/home/banners-disponiveis', [PaginaController::class, 'home'])->name('admin.pagina.editar.home.banners-disponiveis');
            Route::any('/parceiros', [PaginaController::class, 'parceiros'])->name('admin.pagina.editar.parceiros');
            Route::any('/polos', [PaginaController::class, 'polos'])->name('admin.pagina.editar.polos');
            Route::any('/editais', [PaginaController::class, 'editais'])->name('admin.pagina.editar.editais');
            Route::any('/biblioteca', [PaginaController::class, 'biblioteca'])->name('admin.pagina.editar.biblioteca');
            Route::any('/dou', [PaginaController::class, 'dou'])->name('admin.pagina.editar.dou');
            Route::any('/cpa', [PaginaController::class, 'cpa'])->name('admin.pagina.editar.cpa');
            Route::any('/ouvidoria', [PaginaController::class, 'ouvidoria'])->name('admin.pagina.editar.ouvidoria');
            Route::any('/fies', [PaginaController::class, 'fies'])->name('admin.pagina.editar.fies');
            Route::any('/prouni', [PaginaController::class, 'prouni'])->name('admin.pagina.editar.prouni');
            Route::any('/enem', [PaginaController::class, 'enem'])->name('admin.pagina.editar.enem');
            Route::any('/vestibular', [PaginaController::class, 'vestibular'])->name('admin.pagina.editar.vestibular');
            Route::any('/transferencia', [PaginaController::class, 'transferencia'])->name('admin.pagina.editar.transferencia');
            Route::any('/sobre-nos', [PaginaController::class, 'sobreNos'])->name('admin.pagina.editar.sobrenos');
            Route::any('/como-funciona', [PaginaController::class, 'comoFunciona'])->name('admin.pagina.editar.como-funciona');
            Route::any('/faq', [PaginaController::class, 'faq'])->name('admin.pagina.editar.faq');
            Route::any('/contato', [PaginaController::class, 'contato'])->name('admin.pagina.editar.contato');
            Route::any('/cursos', [PaginaController::class, 'cursos'])->name('admin.pagina.editar.cursos');
            Route::any('/termos-de-uso', [PaginaController::class, 'termosDeUso'])->name('admin.pagina.editar.termos.de.uso');
            Route::any('/politica-de-privacidade', [PaginaController::class, 'politicaDePrivacidade'])->name('admin.pagina.editar.politica.de.privacidade');
            Route::any('/aluno-info-0-historico-escolar', [PaginaController::class, 'historicoEscolar'])->name('admin.pagina.editar.aluno-historico-escolar');
            Route::any('/aluno-academico', [PaginaController::class, 'alunoAcademico'])->name('admin.pagina.editar.aluno-academico');
        });

    	# Aluno
        Route::get('/aluno/lista', [AlunoController::class, 'lista'])->name('admin.aluno.lista');
        Route::any('/aluno/info/{id}', [AlunoController::class, 'info'])->name('admin.aluno.info');
        Route::any('/aluno/info/{id}/historico-escolar', [AlunoController::class, 'historicoEscolar'])->name('admin.aluno.info.historico-escolar');
        Route::any('/aluno/info/{id}/set-foto-perfil', [AlunoController::class, 'setFotoPerfil'])->name('admin.aluno.info.setFotoPerfil');
        Route::any('/aluno/add', [AlunoController::class, 'add'])->name('admin.aluno.add');
        Route::any('/aluno/editar/{id}', [AlunoController::class, 'editar'])->name('admin.aluno.editar');
        Route::any('/aluno/info/{id}/matricular-aluno-na-turma', [AlunoController::class, 'matricularAlunoNaTurma'])->name('admin.aluno.matricular-aluno-na-turma');
        Route::any('/aluno/info/{id}/matricular-aluno-na-trilha', [AlunoController::class, 'matricularAlunoNaTrilha'])->name('admin.aluno.matricular-aluno-na-trilha');
        Route::any('/aluno/info/{id}/cadastrar-documento', [AlunoController::class, 'cadastrarDocumento'])->name('admin.aluno.cadastrar-documento');
        Route::any('/aluno/info/{id}/cadastrar-contrato', [AlunoController::class, 'cadastrarContrato'])->name('admin.aluno.cadastrar-contrato');
        Route::any('/aluno/info/{id}/enviar-contrato', [AlunoController::class, 'enviarContrato'])->name('admin.aluno.enviar-contrato');
        Route::any('/aluno/info/{id}/deletar-documento', [AlunoController::class, 'deletarDocumento'])->name('admin.aluno.deletar-documento');
        Route::any('/aluno/info/{id}/deletar-contrato', [AlunoController::class, 'deletarContrato'])->name('admin.aluno.deletar-contrato');

        # Controle Financeiro | Fluxo de Caixa
        Route::any('/controle-financeiro/lista/fluxo-caixa', [FluxoCaixaController::class, 'lista'])->name('admin.controle-financeiro.lista.fluxo-caixa');
        Route::any('/controle-financeiro/lista/fluxo-caixa/add-entrada', [FluxoCaixaController::class, 'addEntrada'])->name('admin.controle-financeiro.lista.fluxo-caixa.add-entrada');
        Route::any('/controle-financeiro/lista/fluxo-caixa/add-saida', [FluxoCaixaController::class, 'addSaida'])->name('admin.controle-financeiro.lista.fluxo-caixa.add-saida');
        Route::any('/controle-financeiro/lista/fluxo-caixa/editar-data_vencimento', [FluxoCaixaController::class, 'editarDataVencimento'])->name('admin.controle-financeiro.lista.editar-datavencimento');
        Route::any('/controle-financeiro/lista/fluxo-caixa/editar-data_lancamento', [FluxoCaixaController::class, 'editarDataLancamento'])->name('admin.controle-financeiro.lista.editar-datalancamento');
        Route::any('/controle-financeiro/lista/fluxo-caixa/editar-data_recebimento', [FluxoCaixaController::class, 'editarDataRecebimento'])->name('admin.controle-financeiro.lista.editar-datarecebimento');
        Route::any('/controle-financeiro/lista/fluxo-caixa/editar-data_estorno', [FluxoCaixaController::class, 'editarDataEstorno'])->name('admin.controle-financeiro.lista.editar-dataestorno');
        Route::any('/controle-financeiro/lista/fluxo-caixa/editar-obs', [FluxoCaixaController::class, 'editarObs'])->name('admin.controle-financeiro.lista.editar-obs');
        Route::any('/controle-financeiro/lista/fluxo-caixa/editar-motivo_estorno', [FluxoCaixaController::class, 'editarMotivoEstorno'])->name('admin.controle-financeiro.lista.editar-motivoestorno');
        Route::any('/controle-financeiro/lista/fluxo-caixa/editar-valor', [FluxoCaixaController::class, 'editarValor'])->name('admin.controle-financeiro.lista.editar-valor');
        Route::any('/controle-financeiro/lista/fluxo-caixa/editar-referencia', [FluxoCaixaController::class, 'editarReferencia'])->name('admin.controle-financeiro.lista.editar-referencia');
        Route::any('/controle-financeiro/lista/fluxo-caixa/editar-link', [FluxoCaixaController::class, 'editarLink'])->name('admin.controle-financeiro.lista.editar-link');
        Route::any('/controle-financeiro/lista/fluxo-caixa/editar-formapagamento', [FluxoCaixaController::class, 'editarFormaPagamento'])->name('admin.controle-financeiro.lista.editar-formapagamento');
        Route::any('/controle-financeiro/lista/fluxo-caixa/editar-vendedor', [FluxoCaixaController::class, 'editarVendedor'])->name('admin.controle-financeiro.lista.editar-vendedor');
        Route::any('/controle-financeiro/lista/fluxo-caixa/editar-unidade', [FluxoCaixaController::class, 'editarUnidade'])->name('admin.controle-financeiro.lista.editar-unidade');
        Route::any('/controle-financeiro/lista/fluxo-caixa/add-entrada', [FluxoCaixaController::class, 'addEntrada'])->name('admin.controle-financeiro.lista.fluxo-caixa.add-entrada');
        Route::any('/controle-financeiro/lista/fluxo-caixa/deletar-entrada', [FluxoCaixaController::class, 'deletarEntrada'])->name('admin.controle-financeiro.lista.fluxo-caixa.deletar-entrada');
        Route::any('/controle-financeiro/lista/fluxo-caixa/deletar-saida', [FluxoCaixaController::class, 'deletarSaida'])->name('admin.controle-financeiro.lista.fluxo-caixa.deletar-saida');
        Route::any('/controle-financeiro/lista/fluxo-caixa/conversar/{aluno_id}', [FluxoCaixaController::class, 'conversar'])->name('admin.controle-financeiro.lista.fluxo-caixa.conversar');
        Route::any('/controle-financeiro/lista/fluxo-caixa/getDadosEntradaById', [FluxoCaixaController::class, 'getDadosEntradaById'])->name('admin.controle-financeiro.lista.fluxo-caixa.getDadosEntradaById');
        Route::any('/controle-financeiro/lista/fluxo-caixa/getDadosSaidaById', [FluxoCaixaController::class, 'getDadosSaidaById'])->name('admin.controle-financeiro.lista.fluxo-caixa.getDadosSaidaById');
        Route::any('/controle-financeiro/lista/fluxo-caixa/editar-setstatus', [FluxoCaixaController::class, 'setStatus'])->name('admin.controle-financeiro.lista.editar-setstatus');
        

        # Responsável
        Route::any('/responsavel/lista', [ResponsavelController::class, 'lista'])->name('admin.responsavel.lista');
        Route::any('/responsavel/add', [ResponsavelController::class, 'add'])->name('admin.responsavel.add');
        Route::any('/responsavel/editar/{id}', [ResponsavelController::class, 'editar'])->name('admin.responsavel.editar');
        Route::any('/responsavel/info/{id}', [ResponsavelController::class, 'info'])->name('admin.responsavel.info');


        # Colaborador
        Route::get('/colaborador/lista', [ColaboradorController::class, 'lista'])->name('admin.colaborador.lista');
        Route::get('/colaborador/info/{id}', [ColaboradorController::class, 'info'])->name('admin.colaborador.info');
        Route::any('/aluno/info/{id}/set-foto-perfil', [ColaboradorController::class, 'setFotoPerfil'])->name('admin.colaborador.info.setFotoPerfil');
        Route::any('/colaborador/add', [ColaboradorController::class, 'add'])->name('admin.colaborador.add');
        Route::any('/colaborador/editar/{id}', [ColaboradorController::class, 'editar'])->name('admin.colaborador.editar');

        # Curso
        Route::get('/curso/lista', [CursoController::class, 'lista'])->name('admin.curso.lista');
        Route::get('/curso/info/{id}', [CursoController::class, 'info'])->name('admin.curso.info');
        Route::any('/curso/{id}/modulo/lista', [CursoController::class, 'listaModulos'])->name('admin.curso.modulo.lista');
        Route::any('/curso/{curso_id}/modulo/{modulo_id}/aula/lista', [CursoController::class, 'listaAulas'])->name('admin.curso.modulo.aula.lista');
        Route::any('/curso/{curso_id}/modulo/{modulo_id}/aula/add', [CursoController::class, 'addAula'])->name('admin.curso.modulo.aula.add');
        Route::any('/curso/{curso_id}/modulo/{modulo_id}/aula/editar', [CursoController::class, 'editarAula'])->name('admin.curso.modulo.aula.editar');
        Route::any('/curso/{curso_id}/modulo/{modulo_id}/aula/delete', [CursoController::class, 'deleteAula'])->name('admin.curso.modulo.aula.delete');
        Route::any('/curso/add', [CursoController::class, 'add'])->name('admin.curso.add');
        Route::any('/curso/editar/{id}', [CursoController::class, 'editar'])->name('admin.curso.editar');
        Route::any('/curso/editar/{id}/secoes-descritivas', [CursoController::class, 'editarSecoesDescritivas'])->name('admin.curso.editar-secoes');
        Route::any('/curso/editar/{id}/corpo-docente', [CursoController::class, 'editarCorpoDocente'])->name('admin.curso.editar-corpodocente');

        # Turma
        Route::get('/turma/lista', [TurmaController::class, 'lista'])->name('admin.turma.lista');
        Route::get('/turma/info/{id}', [TurmaController::class, 'info'])->name('admin.turma.info');
        Route::any('/turma/add', [TurmaController::class, 'add'])->name('admin.turma.add');
        Route::any('/turma/editar/{id}', [TurmaController::class, 'editar'])->name('admin.turma.editar');
        Route::any('/turma/editar/{id}/lista-de-presenca', [TurmaController::class, 'listaDePresenca'])->name('admin.turma.editar.lista-de-presenca');
        Route::any('/turma/editar/{id}/lista-de-presenca/add', [TurmaController::class, 'addlistaDePresenca'])->name('admin.turma.editar.lista-de-presenca-add');
        Route::any('/turma/editar/{id}/lista-de-presenca/editar/{data}', [TurmaController::class, 'editarlistaDePresenca'])->name('admin.turma.editar.lista-de-presenca-editar');

        # Trilha
        Route::get('/trilha/lista', [TrilhaController::class, 'lista'])->name('admin.trilha.lista');
        Route::get('/trilha/info/{id}', [TrilhaController::class, 'info'])->name('admin.trilha.info');
        Route::any('/trilha/add', [TrilhaController::class, 'add'])->name('admin.trilha.add');
        Route::any('/trilha/editar/{id}', [TrilhaController::class, 'editar'])->name('admin.trilha.editar');
        Route::any('/trilha/editar/{id}/secoes-descritivas', [TrilhaController::class, 'editarSecoesDescritivas'])->name('admin.trilha.editar-secoes');

        # Avaliação
        Route::get('/avaliacao/lista', [AvaliacaoController::class, 'lista'])->name('admin.avaliacao.lista');
        Route::get('/avaliacao/info/{id}', [AvaliacaoController::class, 'info'])->name('admin.avaliacao.info');
        Route::get('/avaliacao/info/{id}/finalizaram', [AvaliacaoController::class, 'finalizaram'])->name('admin.avaliacao.info.finalizaram');
        Route::get('/avaliacao/info/{id}/nao-finalizaram', [AvaliacaoController::class, 'naoFinalizaram'])->name('admin.avaliacao.info.nao-finalizaram');
        Route::any('/avaliacao/add', [AvaliacaoController::class, 'add'])->name('admin.avaliacao.add');
        Route::any('/avaliacao/editar/{id}', [AvaliacaoController::class, 'editar'])->name('admin.avaliacao.editar');

        # Questão
        Route::get('/questao/lista', [QuestaoController::class, 'lista'])->name('admin.questao.lista');
        Route::get('/questao/info/{id}', [QuestaoController::class, 'info'])->name('admin.questao.info');
        Route::any('/questao/add', [QuestaoController::class, 'add'])->name('admin.questao.add');
        Route::any('/questao/editar/{id}', [QuestaoController::class, 'editar'])->name('admin.questao.editar');

        # Comunicado
        Route::get('/comunicado/lista', [ComunicadoController::class, 'lista'])->name('admin.comunicado.lista');
        Route::get('/comunicado/info/{id}', [ComunicadoController::class, 'info'])->name('admin.comunicado.info');
        Route::any('/comunicado/add', [ComunicadoController::class, 'add'])->name('admin.comunicado.add');
        Route::any('/comunicado/editar/{id}', [ComunicadoController::class, 'editar'])->name('admin.comunicado.editar');

        # Gestão de Documentos
        Route::get('/documento/lista', [DocumentoController::class, 'lista'])->name('admin.documento.lista');
        Route::get('/documento/info/{id}', [DocumentoController::class, 'info'])->name('admin.documento.info');
        Route::any('/documento/add', [DocumentoController::class, 'add'])->name('admin.documento.add');
        Route::any('/documento/editar/{id}', [DocumentoController::class, 'editar'])->name('admin.documento.editar');

        # Certificado
        Route::get('/certificado/lista', [CertificadoController::class, 'lista'])->name('admin.certificado.lista');
        Route::get('/certificado/info/{id}', [CertificadoController::class, 'info'])->name('admin.certificado.info');
        Route::get('/certificado/info/{id}/gerar', [CertificadoController::class, 'gerar'])->name('admin.certificado.info.gerar');
        Route::any('/certificado/add', [CertificadoController::class, 'add'])->name('admin.certificado.add');
        Route::any('/certificado/editar/{id}', [CertificadoController::class, 'editar'])->name('admin.certificado.editar');

        # Contrato
        Route::get('/contrato/lista', [ContratoController::class, 'lista'])->name('admin.contrato.lista');
        Route::get('/contrato/info/{id}', [ContratoController::class, 'info'])->name('admin.contrato.info');
        Route::get('/contrato/info/{id}/gerar', [ContratoController::class, 'gerar'])->name('admin.contrato.info.gerar');
        Route::get('/contrato/info/{id}/gerar-simulacao', [ContratoController::class, 'gerarSimulacao'])->name('admin.contrato.info.gerar-simulacao');
        Route::any('/contrato/add', [ContratoController::class, 'add'])->name('admin.contrato.add');
        Route::any('/contrato/editar/{id}', [ContratoController::class, 'editar'])->name('admin.contrato.editar');

        # Contato
        Route::get('/contato/lista', [ContatoController::class, 'lista'])->name('admin.contato.lista');
        Route::get('/contato/info/{id}', [ContatoController::class, 'info'])->name('admin.contato.info');
        Route::any('/contato/add', [ContatoController::class, 'add'])->name('admin.contato.add');
        Route::any('/contato/editar/{id}', [ContatoController::class, 'editar'])->name('admin.contato.editar');
        Route::any('/contato/matricular-aluno/{contato_id}', [ContatoController::class, 'matricularAluno'])->name('admin.contato.matricular-aluno');

        # Unidades
        Route::get('/unidade/lista', [UnidadeController::class, 'lista'])->name('admin.unidade.lista');
        Route::get('/unidade/info/{id}', [UnidadeController::class, 'info'])->name('admin.unidade.info');
        Route::any('/unidade/add', [UnidadeController::class, 'add'])->name('admin.unidade.add');
        Route::any('/unidade/editar/{id}', [UnidadeController::class, 'editar'])->name('admin.unidade.editar');

        # Formulário
        Route::get('/formulario/lista', [FormularioController::class, 'lista'])->name('admin.formulario.lista');
        Route::get('/formulario/info/{id}', [FormularioController::class, 'info'])->name('admin.formulario.info');
        Route::any('/formulario/add', [FormularioController::class, 'add'])->name('admin.formulario.add');
        Route::any('/formulario/editar/{id}', [FormularioController::class, 'editar'])->name('admin.formulario.editar');

        Route::get('/relatorio/lista', [RelatorioController::class, 'lista'])->name('admin.relatorio.lista');
        Route::get('/relatorio/buscar', [RelatorioController::class, 'buscar'])->name('admin.relatorio.buscar');
        Route::any('/relatorio/export', [RelatorioController::class, 'export'])->name('admin.relatorio.export');


        Route::group(['prefix' => 'plano'], function(){
            Route::any('/lista', [PlanoController::class, 'lista'])->name('admin.plano.lista');
            Route::any('/add', [PlanoController::class, 'add'])->name('admin.plano.add');
            Route::any('/editar/{id}', [PlanoController::class, 'editar'])->name('admin.plano.editar');
            Route::any('/info/{id}', [PlanoController::class, 'info'])->name('admin.plano.info');
        });

        Route::group(['prefix' => 'servico'], function(){
            Route::any('/lista', [ServicoController::class, 'lista'])->name('admin.servico.lista');
            Route::any('/add', [ServicoController::class, 'add'])->name('admin.servico.add');
            Route::any('/editar/{id}', [ServicoController::class, 'editar'])->name('admin.servico.editar');
            Route::any('/info/{id}', [ServicoController::class, 'info'])->name('admin.servico.info');
        });

        Route::group(['prefix' => 'produto'], function(){
            Route::any('/lista', [ProdutoController::class, 'lista'])->name('admin.produto.lista');
            Route::any('/add', [ProdutoController::class, 'add'])->name('admin.produto.add');
            Route::any('/editar/{id}', [ProdutoController::class, 'editar'])->name('admin.produto.editar');
            Route::any('/info/{id}', [ProdutoController::class, 'info'])->name('admin.produto.info');
        });


        # Plataforma
        Route::any('/plataforma/editar/configuracoes', [PlataformaController::class, 'configuracoes'])->name('admin.plataforma.editar.configuracoes');
        Route::any('/plataforma/editar/configuracoes/categorias', [PlataformaController::class, 'categorias'])->name('admin.plataforma.editar.configuracoes.categorias');
        Route::any('/plataforma/editar/configuracoes/logs', [PlataformaController::class, 'logs'])->name('admin.plataforma.editar.configuracoes.logs');
        Route::any('/plataforma/editar/configuracoes/modelo-de-emails', [PlataformaController::class, 'modeloDeEmails'])->name('admin.plataforma.editar.configuracoes.modelo-de-emails');

        # Tutorial Ajuda
        Route::get('/ajuda/tutorial/lista', [AjudaTutorialController::class, 'lista'])->name('admin.ajuda.tutorial.lista');

        # Chamado Ajuda
        Route::get('/ajuda/chamado/lista', [AjudaChamadoController::class, 'lista'])->name('admin.ajuda.chamado.lista');
        Route::get('/ajuda/chamado/add', [AjudaChamadoController::class, 'add'])->name('admin.ajuda.chamado.add');
        Route::get('/ajuda/chamado/info/{id}', [AjudaChamadoController::class, 'info'])->name('admin.ajuda.chamado.info');

        # Billing
        Route::any('billing', [BillingController::class, 'info'])->name('admin.billing');

    });


    Route::group(['prefix' => 'api-util', 'namespace' => 'api-util'], function() {
        
        Route::get('/api-get-turmas-by-curso_id', [APIUtilController::class, 'getTurmasByCursoId'])->name('api-util.get-turmas-by-curso_id');
        Route::get('/api-get-avaliacoes-by-turma_id', [APIUtilController::class, 'getAvaliacoesByTurmaId'])->name('api-util.get-avaliacoes-by-turma_id');
    
    });

});