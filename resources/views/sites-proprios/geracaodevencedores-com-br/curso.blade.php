<!doctype html>
<html lang="pt-br">
<head>

  <!-- Google tag (gtag.js) -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=G-MREB450HCT"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'G-MREB450HCT');
  </script>

  {!! $plataforma->scripts_start_head !!}

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/uikit@3.5.5/dist/css/uikit.min.css" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/5.0.0-alpha1/css/bootstrap.min.css" integrity="sha384-r4NyP46KrjDleawBgD5tp8Y7UzmLA05oM1iAEQ17CSuDqnUK2+k9luXQOfXJCJ4I" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('sites-proprios/geracaodevencedores-com-br/css/home.css') }}?v=8">
    <link rel="stylesheet" href="{{ asset('css/util.css') }}?v=2">
    <link rel="stylesheet" href="{{ asset('css/pages.css') }}">

    <link rel="shortcut icon" href="{{ $plataforma->favicon ?? asset('sites-proprios/geracaodevencedores-com-br/images/favicon.ico') }}">
    <link href="{{ Request::url() }}" rel="canonical">

    <meta name="robots" content="index, follow">
    <title>{{ $seo->title ?? $plataforma->nome }}</title>
    <meta name="description" content="{{ $seo->description ?? null }}">
    <meta name="keywords" content="{{ $seo->keywords ?? null }}">

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <meta name='author' content='WOZCODE | Pedro Henrique' />

    <meta property="og:locale" content="pt_BR">
    <meta property="og:type" content="website">
    <meta property="og:image" content="{{ $plataforma->logotipoMenu ?? asset('sites-proprios/geracaodevencedores-com-br/images/logotipo.png') }}">
    <meta property="og:url" content="{{ Request::url() }}">
    <meta property="og:description" content="{{ $seo->description ?? null }}">
    <meta property="og:title" content="{{ $seo->title ?? $plataforma->nome }}">

    <style>
    
      #menuPlataforma nav { background: #355a7f; }
      .menu-link, .menu-link-color { color: #F5F5F5;; }
      .menu-link:hover { text-decoration: none; background: #1e3b57; color: #FFF!important; }
      .navbar-brand span, .activeNavLink { text-decoration: none; background: #1e3b57; color: #FFF!important; }

      #menu {
          width: 100%;
          box-sizing: border-box;
          padding: 9px 10px!important;
          position: fixed;
          z-index: 200;
          box-shadow: 0px -1px 8px 0px rgba(0, 0, 0, 0.60);
          background: transparent;
          transition: background-color 0.3s ease-in-out;
      }
      @media (min-width: 769px) 
      {
          #menu { height: 75px; }

          .menu-link { 
            margin: 0 10px;
            padding-top: 27.2px!important;
            padding-bottom: 27.2px!important;
            padding-left: 10px!important; 
            padding-right: 10px!important; 
          }
      }
      .menu-link {
          font-family: 'Open Sans',sans-serif;
          font-size: 13.5px;
          text-transform: uppercase;
          text-decoration: none;
          font-weight: 700;
          color: #F5F5F5!important;
          cursor: pointer;
          transition: all .2s;
          text-shadow: 1px 1px rgba(0, 0, 0, .1);
      }

      .btn-entrar { 
        background: #f5f5f5!important; 
        color: #355a7f!important; 
        font-family: 'Poppins', sans-serif; 
        font-weight: 600!important; 
        border-radius: 3px!important;
        padding: 12px 24px!important;
      }
          
      #footer { position: absolute!important; }
    
</style>

{!! $plataforma->scripts_final_head !!}

</head>
<body id="inicio">

   {!! $plataforma->scripts_start_body !!}

   @include('sites-proprios/geracaodevencedores-com-br/componentes/header')

   <main>

      <section>
       <div class="w-100 h-lg-450 h-xs-100vh pt-5" style="background-size: cover; background-position: center; background-image: url('{{ $curso->foto_capa ?? asset('sites-proprios/cedetep/images/footer.jpg') }}">
          <div class="position-relative h-100 px-xl-7 px-3">
          <div class="background-overlay h-100" style="background: rgba(0,0,0,1);"></div>

             <div class="row align-items-center h-100 position-relative">
                <div class="mt-2">
                   <h1 class="mb-4-5 text-light font-poppins weight-800 fs-xs-35 fs-md-50 text-shadow-3">
                      <span id="cursoTiulo">{!! $curso->nome !!}</span>
                   </h1>
                   <a id="btnCursoInscrevaSe" class="btn btn-primary weight-700 box-shadow text-shadow-2 fs-17 ls-05 text-light hover-d-none px-4 py-3 font-poppins" href="#turmas">
                      INSCREVA-SE AGORA!
                   </a>
                </div>
             </div>
          </div>
       </div>
      </section>

      <section class="pb-lg-3">
         <div class="row container-fluid px-3 gx-0 gx-lg-4 px-xl-5 mt--15px position-relative pb-5">
            <div class="col-lg-8">
               <div class="bg-white px-lg-5 px-4 pt-4-5 pb-4-5 box-shadow border-top rounded-20">

                  <p class="mb-0 pb-0 font-poppins weight-600 fs-14 color-666">
                     Sobre o curso
                  </p>

                  <hr class="border border-secondary">

                  @if($curso->descricao == null)
                     <p class="mb-0 pb-0 font-poppins weight-600 fs-13 color-666">
                        <i class="fas fa-exclamation-circle"></i> Descrição do Curso não definida.
                     </p>
                  @endif

                  <p id="cursoDescricao">
                     {!! nl2br($curso->descricao) !!}
                  </p>

               </div>
            </div>
            <div class="col-lg-4 mt-4 mt-lg-0">
               <div class="bg-white px-4-5 pt-4-5 pb-4-5 box-shadow border-top rounded-20">

                  @if($curso->video)
                     <iframe width="100%" height="225" class="rounded mb-4" src="{{ $curso->video }}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                  @endif

                      @if($curso->nivel)
                      <div>                                
                          <p class="fs-15 mb-0 weight-600 color-555">NÍVEL:</p>
                          <p class="fs-14 mb-0 weight-600 font-poppins color-444">{{ $curso->nivel }}</p>
                          <hr/>
                      </div>
                      @endif

                      @if($curso->tipo_formacao)
                      <div>                                
                          <p class="fs-15 mb-0 weight-600 color-555">TIPO DE FORMAÇÃO:</p>
                          <p class="fs-14 mb-0 weight-600 font-poppins color-444">{{ $curso->tipo_formacao }}</p>
                          <hr/>
                      </div>
                      @endif
                      
                      @if($curso->carga_horaria)
                      <div>                                
                          <p class="fs-15 mb-0 weight-600 color-555">CARGA HORÁRIA:</p>
                          <p class="fs-14 mb-0 weight-600 font-poppins color-444">{{ $curso->carga_horaria }} horas</p>
                      </div>
                      @endif

               </div>
            </div>
         </div>
      </section>

      <section class="pb-5" id="turmas">
         <div class="row container-fluid px-3 gx-0 gx-lg-4 px-xl-5 mt--15px position-relative pb-5">
            <div class="col-lg-8">
               <div class="bg-white px-lg-5 px-4 pt-4-5 pb-4-5 box-shadow border-top rounded-20">

                  <p class="font-poppins weight-600 fs-14 color-666">
                     Turmas
                  </p>

                     @if(count($turmas) == 0)
                        <p class="mb-0 pb-0 font-poppins weight-600 fs-13 color-666">
                           <i class="fas fa-exclamation-circle"></i> Não há turmas publicadas disponíveis para inscrição.
                        </p>
                     @endif

                  @foreach($turmas as $turma)
                     <div class="mb-4 p-4 border rounded-20">
                        <div class="d-lg-flex align-items-end justify-content-between">
                           <h1 class="m-0 p-0 fs-17 font-poppins color-blue-dark">
                              {{ $turma->nome }}
                           </h1>
                           @if($turma->modalidade)
                              <span class="modalidade mx-xs-0 mt-2 mt-lg-0">
                                 {{ $turma->modalidade }}
                              </span>
                           @endif
                        </div>
                        <hr/>
                        <div>
                           <div class="d-lg-flex justify-content-between">
                              
                              @if($turma->data_inicio)
                              <div class="mb-3 mb-lg-0 d-flex align-items-center">
                                 <span class="mr-3">
                                    <i class="fs-25 far fa-calendar-alt"></i>
                                 </span>
                                 <p class="m-0">
                                    <span class="d-block font-poppins fs-13 color-666">Data:</span>
                                    <span class="d-block fs-15 weight-600 color-555">
                                       Início: {{ \App\Models\Util::replaceDatePt($turma->data_inicio) }}
                                    </span>
                                    @if($turma->data_termino)
                                    <span class="d-block fs-15 weight-600 color-555">
                                       Término: {{ \App\Models\Util::replaceDatePt($turma->data_termino) }}
                                    </span>
                                    @endif
                                 </p>
                              </div>
                              @endif

                              @if($turma->horario)
                              <div class="mb-3 mb-lg-0 d-flex align-items-center pr-lg-2">
                                 <span class="mr-3">
                                    <i class="fs-25 far fa-clock"></i>
                                 </span>
                                 <p class="m-0">
                                    <span class="d-block font-poppins fs-13 color-666">Dias e Horário:</span>
                                    <span class="d-block fs-15 weight-600 color-555">
                                       {{ $turma->frequencia }}
                                    </span>
                                    <span class="d-block fs-15 weight-600 color-555">
                                       {{ $turma->horario }}
                                    </span>
                                 </p>
                              </div>
                              @endif

                              @if($turma->duracao)
                              <div class="mb-3 mb-lg-0 d-flex align-items-center pr-lg-2">
                                 <span class="mr-3">
                                    <i class="fs-25 far fa-clock"></i>
                                 </span>
                                 <p class="m-0">
                                    <span class="d-block font-poppins fs-13 color-666">Duração:</span>
                                    <span class="d-block fs-15 weight-600 color-555">
                                       {{ $turma->duracao }}
                                    </span>
                                 </p>
                              </div>
                              @endif

                           </div>

                           @if($turma->data_inicio || $turma->horario || $turma->duracao)
                              <hr/>
                           @endif

                           <div class="d-lg-flex justify-content-between align-items-center">

                              <div class="pl-lg-1">
                                 <p>
                                    <span class="d-block fs-16 font-poppins color-blue-dark weight-600">Faça sua matrícula e ganhe<br>
                                      50% DE DESCONTO<br>
                                      em todas as parcelas
                                    </span>
                                 </p>
                                 @if($turma->valor_a_vista)
                                 <p class="d-flex align-items-center m-0 p-0">
                                    <span class="mr-2 weight-600 color-555">
                                       por:
                                    </span>
                                    <span class="fs-20 font-poppins color-blue-dark weight-700">
                                       R$ {{ $turma->valor_a_vista }}
                                    </span>
                                 </p>
                                 @endif
                                 @if($turma->valor_parcelado)
                                 <p class="mb-0 mr-2 weight-600 color-555">
                                    <span class="mr-2 weight-600 color-555">
                                       ou: 
                                    </span>
                                    <span class="fs-20 font-poppins color-blue-dark weight-700">
                                       R$ {{ $turma->valor_parcelado }}
                                    </span>
                                 </p>
                                 @endif
                              </div>

                              <div class="mt-3 mt-lg-0">
                                 <a href="/f/{{$turma->formulario_id ?? 0}}/c/{{$turma->id}}" target="_blank" id="btnCursoMatricular" class="btn-comprar py-3 px-4">
                                    QUERO ME MATRICULAR COM DESCONTO!
                                 </a>
                              </div>
                           </div>

                        </div>
                     </div>
                  @endforeach

               </div>
            </div>
         </div>
      </section>

   </main>

   @include('sites-proprios/geracaodevencedores-com-br/componentes/footer')

   <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
   <script src="https://cdn.jsdelivr.net/npm/uikit@3.5.5/dist/js/uikit.min.js"></script>
   <script src="https://kit.fontawesome.com/2f91eaaef4.js" crossorigin="anonymous"></script>  
   <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>    
   <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
   <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

   <script>

      window.onscroll = function() {myFunction()};

      var header = document.getElementById("menu");
      var sticky = header.offsetTop;

      function myFunction() {
        if (window.pageYOffset > sticky) {
          header.classList.add("fixed-top");
          header.classList.add("mudaCor");
          $(".nav-link").addClass("nav-link-after-hover");
        } else {
          header.classList.remove("fixed-top");
          header.classList.remove("mudaCor");
          $(".nav-link").removeClass("nav-link-after-hover");
        }
      }

   </script>

   {!! $plataforma->scripts_final_body !!}

</body>
</html>