<?php

namespace App\Http\Livewire\Admin\Curso;

use Livewire\Component;
use App\Models\Curso;
use App\Models\Turma;
use App\Models\Categoria;
use App\Models\CursoCategoria;
use App\Models\User;
use App\Models\Certificado;
use DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Http;

class Info extends Component
{
    public $curso;
    public $certificado;
    public $cadastrante;
    public $categorias;
    public $password_to_delete;

    public function mount($curso_id)
    {
        $this->curso = Curso::plataforma()->find($curso_id);
    
        $this->cadastrante = User::plataforma()->where('id', $this->curso->cadastrante_id)->first();
        $this->certificado = Certificado::plataforma()->where('id', $this->curso->certificado_id)->first();

        $categoriasIds = CursoCategoria::plataforma()->where('curso_id', $this->curso->id)->get()->pluck('categoria_id')->toArray();
        $this->categorias = Categoria::plataforma()->ativo()->whereIn('id', $categoriasIds)->select('nome')->orderBy('nome')->get()->pluck('nome')->toArray();
    }

    public function render()
    {
        $turmas = Turma::plataforma()->where('curso_id', $this->curso->id)->select('id','nome')->get();

        $countAlunos = DB::table('turmas_alunos')
            ->join('users','turmas_alunos.aluno_id','users.id')
            ->where('users.plataforma_id', session('plataforma_id'))
            ->where('turmas_alunos.plataforma_id', session('plataforma_id'))
            ->where('turmas_alunos.curso_id', $this->curso->id)
            ->count();

        return view('livewire.admin.curso.info', compact('turmas','countAlunos') );
    }

    public function delete()
    {
        if(\Auth::attempt(['id' => \Auth::id(), 'password' => $this->password_to_delete, 'status' => 0]))
        {
            $countsAlunosMatriculados = \App\Models\TurmaAluno::plataforma()->where('curso_id', $this->curso->id)->count();

            if($countsAlunosMatriculados == 0)
            {
                CursoCategoria::plataforma()->where('curso_id', $this->curso->id)->delete();
                $deleteAndReturnCountDeleteds = \App\Models\AlunoAulaConcluida::plataforma()->where('curso_id', $this->curso->id)->delete();

                \App\Models\Log::create([
                    'tipo' => 'curso',
                    'ref'  => 'Curso: ' . $this->curso->referencia . ' foi deletado. Quantidade de aulas concluídas por alunos que foram deletadas: ' . $deleteAndReturnCountDeleteds,
                ]);

                $path = str_replace(\App\Models\Util::getLinkStorage(), '', $this->curso->foto_capa);
                Storage::delete($path);

                $this->curso->delete();

                session()->flash('success', 'Curso deletado com êxito.');
                
                return redirect(route('admin.curso.lista'));

            } else {

                session()->flash('danger-livewire', 'Não é possível deletar, pois há alunos matriculados em turma(s) deste curso.');

                $this->dispatchBrowserEvent('toast');

            }

        } else {

            session()->flash('danger-livewire', 'Senha incorreta.');

            $this->dispatchBrowserEvent('toast');
        }
    }
}
