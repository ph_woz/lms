<?php

namespace App\Http\Livewire\Admin\Pagina\Editar\Blog;

use Livewire\WithPagination;
use Livewire\Component;
use Illuminate\Support\Facades\Storage;
use App\Models\BlogPost;
use App\Models\BlogPostCategoria;
use App\Models\Categoria;
use App\Models\User;

class Lista extends Component
{
    use WithPagination;

    protected $paginationTheme = 'bootstrap';

    public $categoria_id;
    public $search;
    public $publicar = 'S';
    public $ordem;

    protected $queryString = ['categoria_id','search','publicar','ordem'];

    public $postMetaDado;
    public $editar_post;
    public $categoriasSelected;
    public $password_to_delete;

    public function mount()
    {
        $this->editar_post = BlogPost::plataforma()->where('id', $_GET['editar_post'] ?? 0)->first();

        if(isset($this->editar_post))
        {
            $this->categoriasSelected = BlogPostCategoria::plataforma()->where('post_id', $this->editar_post->id)->get()->pluck('categoria_id')->toArray();
        }
    }

    public function render()
    {
        $posts = BlogPost::select('id','titulo','slug')->plataforma();
        
        if($this->categoria_id)
        {
            $postsIds = BlogPostCategoria::select('post_id')->plataforma()->where('categoria_id', $this->categoria_id)->get()->pluck('post_id')->toArray();

            $posts = $posts->whereIn('id', $postsIds);
        }

        if($this->search)
        {
            $posts = $posts->where('titulo', 'like', '%'.$this->search.'%');
        }

        $posts = $posts->where('publicar', $this->publicar);

        $countPosts = $posts->count();


        if($this->ordem == 'desc' || $this->ordem == 'asc')
        {
            $posts = $posts->orderBy('id', $this->ordem);
        
        } else {

            $posts = $posts->orderBy('titulo');
        }


        $posts = $posts->paginate(10);

        $autores = User::select('id','nome')->plataforma()->ativo()->colaborador()->orderBy('nome')->get();

        $categorias = Categoria::select('id','nome')->plataforma()->ativo()->tipo('blog')->orderBy('nome')->get();

        return view('livewire.admin.pagina.editar.blog.lista', [
            'posts'      => $posts,
            'countPosts' => $countPosts,
            'autores'    => $autores,
            'categorias' => $categorias,
        ]);
    }

    public function modalMetaDados($post_id)
    {
        $this->postMetaDado = BlogPost::
        select(
            'blog_posts.id',
            'blog_posts.titulo',

            'cadastrante.id as cadastrante_id',
            'cadastrante.nome as cadastrante',

            'autor.id as autor_id',
            'autor.nome as autor',

            'blog_posts.publicar',
            'blog_posts.created_at'
        )
        ->leftJoin('users as cadastrante','blog_posts.cadastrante_id','cadastrante.id')
        ->leftJoin('users as autor','blog_posts.autor_id','autor.id')
        ->where('blog_posts.plataforma_id', session('plataforma_id') ?? \Auth::user()->plataforma_id)
        ->where('blog_posts.id', $post_id)
        ->first();
    }

    public function delete()
    {
        if(\Auth::attempt(['id' => \Auth::id(), 'password' => $this->password_to_delete, 'status' => 0]))
        {
            BlogPostCategoria::plataforma()->where('post_id', $this->postMetaDado->id)->delete();

            $path = str_replace(\App\Models\Util::getLinkStorage(), '', $this->postMetaDado->foto_capa);
            Storage::delete($path);

            $this->postMetaDado->delete();

            session()->flash('success', 'Post deletado com êxito.');
            
            return redirect(route('admin.pagina.editar.blog'));

        } else {

            session()->flash('danger-livewire', 'Senha incorreta.');

            $this->dispatchBrowserEvent('toast');
        }
    }

}
