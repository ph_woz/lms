<?php

namespace App\Http\Livewire\Admin\Plataforma;

use Livewire\WithPagination;
use Livewire\Component;
use App\Models\Categoria;

class Categorias extends Component
{
    use WithPagination;

    protected $paginationTheme = 'bootstrap';

    public $search;
    public $tipo;
    public $publicados;
    public $status = 0;
    public $ordem;
    public $categoriasSelected;

    protected $queryString = ['search','tipo','status','publicados','ordem'];

    public function render()
    {
        $categorias = Categoria::plataforma();

        if($this->search)
        {
            $categorias->where(function ($query)
            {
                $query
                    ->orWhere('nome', 'like', '%'.$this->search.'%')
                    ->orWhere('referencia', 'like', '%'.$this->search.'%');
            });
        }

        if($this->status !== null)
        {
            $categorias = $categorias->where('status', $this->status);
        }

        if($this->publicados !== null)
        {
            $categorias = $categorias->where('publicar', $this->publicados);
        }

        if($this->tipo)
        {
            $categorias = $categorias->where('tipo', $this->tipo);
        }

        $countCategorias = $categorias->count();


        if($this->ordem == 'desc' || $this->ordem == 'asc')
        {
            $categorias = $categorias->orderBy('id', $this->ordem);

        } else {

            $categorias = $categorias->orderBy('nome');
        }
        

        $categorias = $categorias->paginate(10);


        return view('livewire.admin.plataforma.categorias', [
            'categorias'      => $categorias,
            'countCategorias' => $countCategorias,
        ]);
    }
}
