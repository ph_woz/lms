<!doctype html>
<html lang="pt-br">
<head>

  <!-- Google tag (gtag.js) -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=G-MREB450HCT"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'G-MREB450HCT');
  </script>

  {!! $plataforma->scripts_start_head !!}

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/uikit@3.5.5/dist/css/uikit.min.css" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/5.0.0-alpha1/css/bootstrap.min.css" integrity="sha384-r4NyP46KrjDleawBgD5tp8Y7UzmLA05oM1iAEQ17CSuDqnUK2+k9luXQOfXJCJ4I" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('sites-proprios/geracaodevencedores-com-br/css/home.css') }}?v=8">
    <link rel="stylesheet" href="{{ asset('css/util.css') }}?v=2">

   <title>{{ $seo->seo_title ?? $plataforma->nome }}</title>
   <meta name="robots" content="index, follow">
   <meta name="description" content="{{ $seo->seo_description ?? null }}"/>
   <meta name="keywords" content="{{ $seo->seo_keywords ?? null }}"/>
   <link rel="canonical" href="{{ Request::url() }}" />
   <link rel="shortcut icon" href="{{ $plataforma->favicon }}">
   <meta property="og:locale" content="pt_BR" />
   <meta property="og:type" content="website" />
   <meta property="og:title" content="{{ $seo->seo_title ?? $plataforma->nome }}" />
   <meta property="og:description" content="{{ $seo->seo_description ?? null }}" />
   <meta property="og:url" content="{{ Request::url() }}" />
   <meta property="og:site_name" content="{{ $plataforma->nome }}" />
   <meta property="og:image" content="{{ $plataforma->logotipo }}" />
   <meta name='author' content='WOZCODE | Pedro Henrique' />

   {!! $plataforma->scripts_final_head !!}
   
</head>
<body id="inicio">
   
   {!! $plataforma->scripts_start_body !!}

   <header id="menuPlataforma">

      <div class="bg-f9 py-2 px-2">
         <div class="container-fluid px-xl-7 d-flex align-items-center justify-content-between">
            <div class="d-lg-flex align-items-center">
               <p class="mb-0 mr-4 font-poppins weight-600 fs-12 color-submenu d-xs-none">
                  <i class="fa fa-clock"></i>
                  09h às 19h
               </p>
               <address class="d-xs-none my-0 py-0 font-poppins weight-600 fs-12 color-submenu mr-lg-4">
                  <i class="fa fa-map-marker-alt"></i>
                  Rua Conde de Bonfim, 277 sobreloja, Tijuca
               </address>
               <div>
                  <a href="tel:+552125703515" class="d-xs-none text-d-none hover-d-none font-poppins weight-600 fs-12 color-submenu">
                     <i class="fa fa-phone"></i>
                     (21) 2570-3515
                  </a>
                  <span class="color-submenu mx-lg-1 mt--5 d-xs-none">/</span>
                  <a href="tel:+552131773515" class="d-xs-none text-d-none hover-d-none font-poppins weight-600 fs-12 color-submenu">
                     <i class="fa fa-phone"></i>
                     (21) 3177-3515
                  </a>
                  <span class="color-submenu mx-1 mt--5 d-xs-none">/</span>
                  <a href="https://api.whatsapp.com/send?phone=5521985105348&text=Olá, vim através do Site Geração de Vencedores" target="_blank" class="mr-5 text-d-none hover-d-none font-poppins weight-600 fs-12 color-submenu">
                     <i class="fab fa-whatsapp"></i>
                     (21) 98510-5348
                  </a>
               </div>
            </div>
            <div class="d-lg-flex align-items-center">
               <div class="flex-center mr-lg-4">
                 <a href="https://www.facebook.com/www.geracaodevencedores.com.br/" target="_blank" class="color-submenu hover-d-none mr-3">
                     <i class="fab fa-facebook-f"></i>
                 </a>
                 <a href="https://www.instagram.com/cursogeracaodevencedores/" target="_blank" class="color-submenu hover-d-none">
                     <i class="fab fa-instagram"></i>
                 </a>
               </div>
            </div>
         </div>
      </div>

      <nav class="navbar navbar-expand-lg navbar-light" id="menu">
         <div class="container-fluid px-xl-7">
            <a class="navbar-brand menu-link scroll-link" href="#inicio">
               <img src="{{ $plataforma->logotipoMenu ?? asset('sites-proprios/geracaodevencedores-com-br/images/logotipo.png') }}" width="180" alt="Logotipo {{ $plataforma->nome }}">
            </a>
            <button class="navbar-toggler outline-0 border-0" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
               <i class="fa fa-bars text-white"></i>
            </button>
            <div class="collapse navbar-collapse bg-base-xs" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto mb-2 mb-lg-0">
                                      <li class="nav-item">
                        <a class="menu-link nav-link scroll-link" href="#inicio">
                            Início
                        </a>
                    </li>
                                      <li class="nav-item">
                        <a class="menu-link nav-link scroll-link" href="#sobre-nos">
                            Quem somos
                        </a>
                    </li>
                                      <li class="nav-item">
                        <a class="menu-link nav-link scroll-link" href="#cursos">
                            Cursos
                        </a>
                    </li>
                                      <li class="nav-item">
                        <a class="menu-link nav-link " href="/blog">
                            Blog
                        </a>
                    </li>
                                      <li class="nav-item">
                        <a class="menu-link nav-link scroll-link" href="#contato">
                            Fale Conosco
                        </a>
                    </li>
                                    <li class="nav-item">
                     <a class="nav-link" href="/login">
                        <i class="fa fa-user-graduate mr-1" aria-hidden="true"></i>
                        Portal do Aluno
                     </a>
                  </li>
               </ul>
            </div>
         </div>
      </nav>
   </header>

   <main>

      <section class="cursor-drag">

         <div class="uk-position-relative uk-visible-toggle uk-light" tabindex="-1" uk-slideshow="animation: fade; ratio: false; autoplay: false; pause-on-hover: false">

            <ul class="uk-slideshow-items" uk-height-viewport>
               @foreach($banners as $banner)
               <li>
                  <div class="uk-background-cover" uk-parallax="bgy: -100" style="background-image: url('{{ $banner->link }}');" uk-cover>
                     <div class="background-overlay-gv"></div>
                     <div class="px-xl-7 uk-position-left uk-flex uk-flex-middle uk-position-small uk-text-left uk-light">
                        <div class="row align-items-center">
                           <div class="col-md-5">
                              <h2 uk-slideshow-parallax="x: 300,0,-100" class="font-montserrat fs-md-50 fs-xs-35 weight-900 text-shadow-2">
                                 {{ $banner->titulo }}
                              </h2>
                              @if($banner->subtitulo)
                              <p uk-slideshow-parallax="x: 300,0,-100" class="pr-lg-5 mt-3 mb-4-5 font-poppins line-height-1-4 fs-18 text-light">
                                 {{ $banner->subtitulo }}
                              </p>
                              @endif
                              @if($banner->botao_texto)
                              <div uk-slideshow-parallax="x: 300,0,-100">
                                 <a href="{{ $banner->botao_link }}" target="_blank" uk-scroll="offset: 80;" class="btnSaibaMais">
                                    {{ $banner->botao_texto }}
                                 </a>
                              </div>
                              @endif
                           </div>
                           <div class="col-md-6 offset-md-1 d-none d-md-block" align="center">
                              <div id="carouselExampleIndicators" class="carousel slide carousel-fade" data-bs-pause="false" data-bs-ride="carousel">
                                <div class="carousel-inner">
                                  <div class="carousel-item active" data-bs-interval="3000">
                                    <div class="d-flex align-items-top">
                                       
                                       <div class="text-center">
                                          <img class="shadow square-150 border-profile rounded-circle object-fit-cover" src="{{ asset('sites-proprios/geracaodevencedores-com-br/images/alunos-concursados/1.jpg') }}">
                                          <p class="mb-0 mt-2 fs-18">
                                            Murillo Brandão
                                          </p>
                                          <p class="mb-2 fs-18 weight-600">Mecânica - CEFET</p>
                                          <i class="fa fa-star color-star d-inline-block mb-3 fs-17"></i>
                                          <i class="fa fa-star color-star d-inline-block mb-3 fs-17"></i>
                                          <i class="fa fa-star color-star d-inline-block mb-3 fs-17"></i>
                                          <i class="fa fa-star color-star d-inline-block mb-3 fs-17"></i>
                                          <i class="fa fa-star color-star d-inline-block mb-3 fs-17"></i>
                                          <br>
                                       </div>
                                       <div class="text-center mx-4">
                                          <img class="shadow square-150 border-profile rounded-circle object-fit-cover" src="{{ asset('sites-proprios/geracaodevencedores-com-br/images/alunos-concursados/2.jpg') }}">
                                          <p class="mb-0 mt-2 fs-18">
                                            Mariana Thuller
                                          </p>
                                          <p class="mb-2 fs-18 weight-600">EEAR</p>
                                          <i class="fa fa-star color-star d-inline-block mb-3 fs-17"></i>
                                          <i class="fa fa-star color-star d-inline-block mb-3 fs-17"></i>
                                          <i class="fa fa-star color-star d-inline-block mb-3 fs-17"></i>
                                          <i class="fa fa-star color-star d-inline-block mb-3 fs-17"></i>
                                          <i class="fa fa-star color-star d-inline-block mb-3 fs-17"></i>
                                          <br>
                                       </div>
                                       <div class="text-center">
                                          <img class="shadow square-150 border-profile rounded-circle object-fit-cover" src="{{ asset('sites-proprios/geracaodevencedores-com-br/images/alunos-concursados/3.JPG') }}">
                                          <p class="mb-0 mt-2 fs-18">
                                            João Vitor
                                          </p>
                                          <p class="mb-2 fs-18 weight-600">Eng. da Computação<br>ENEM/UERJ/USP</p>
                                          <i class="fa fa-star color-star d-inline-block mb-3 fs-17"></i>
                                          <i class="fa fa-star color-star d-inline-block mb-3 fs-17"></i>
                                          <i class="fa fa-star color-star d-inline-block mb-3 fs-17"></i>
                                          <i class="fa fa-star color-star d-inline-block mb-3 fs-17"></i>
                                          <i class="fa fa-star color-star d-inline-block mb-3 fs-17"></i>
                                          <br>
                                       </div>

                                    </div>
                                  </div>
                                </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </li>
               @endforeach

            </ul>

            <ul class="uk-slideshow-nav uk-dotnav uk-flex-center mt--md-130 position-relative"></ul>

            <div class="uk-light">
               <a class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous uk-slideshow-item="previous"></a>
               <a class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-next uk-slideshow-item="next"></a>
            </div>

         </div>

      </section>

      <section class="py-5 mb-4" id="jumbotron">
         <div class="container-fluid px-xl-7">
            <div class="row align-items-center">
               <div class="col-lg-8 mb-4 mb-lg-0">

                  <h1 id="titleMatriculeSeJa" class="font-montserrat weight-800 fs-md-50 text-shadow-2">
                    MATRICULE-SE JÁ!
                  </h1>

                  <p id="sloganMatriculeSeJa" class="mb-0 text-shadow-2 font-montserrat weight-600 fs-20">
                    Faça sua matrícula e ganhe<br>
                    50% DE DESCONTO<br>
                    em todas as parcelas
                  </p>

               </div>
               <div class="col-lg-4 text-right">

                  <div uk-scrollspy="cls: uk-animation-slide-top; target: #btnMatricula; delay: 300;">
                     <a href="https://geracaodevencedores.com.br/f/inscricao-de-curso/50/t/672" target="_blank" id="btnMatriculaOnline" class="box-info">
                        QUERO FAZER MINHA<br>MATRÍCULA ONLINE AGORA!
                     </a>
                  </div>

               </div>
            </div>
         </div>
      </section>

      <section class="py-5" id="cursos">
         <div class="container-fluid px-xl-7 mb-4">

            <h2 class="font-rajdhani fs-25 weight-700 text-center color-grafite">
               TENHA UM PROPÓSITO
            </h2>

            <h1 class="text-center font-rajdhani weight-700 fs-50 color-base">
               AQUI, AJUDAMOS VOCÊ A <br> CONSTRUIR O SEU FUTURO
            </h1>

            <div class="pt-4-5 uk-child-width-1-4@m" uk-grid uk-scrollspy="cls: uk-animation-fade; target: .box-info; delay: 175;">

               @foreach($cursos as $curso)
               <div class="px-lg-1">
                  <div class="card box-info border bg-white rounded-3">
                     <a href="/curso/{{$curso->slug}}" class="text-d-none hover-d-none">
                        <img src="{{ $curso->foto_capa }}" class="cardCursos card-img-top border-bottom" alt="{{ $curso->nome }}">
                        <div class="card-body text-center h-lg-240">
                           <h3 class="font-poppins weight-600 fs-20 color-base-blue">
                              {{ $curso->nome }}
                           </h3>
                           <div>
                              <i class="fa fa-star color-star"></i>
                              <i class="fa fa-star color-star"></i>
                              <i class="fa fa-star color-star"></i>
                              <i class="fa fa-star color-star"></i>
                              <i class="fa fa-star color-star"></i>
                           </div>
                           <p class="mt-2-5 mb-lg-4-5 card-text fs-14 font-poppins color-grafite weight-500">
                              {{ $curso->slogan }}
                           </p>
                        </div>
                        <div class="boxCardBtnMatriculeSe p-3">
                          <p class="mb-0 mt-lg-4 btnCard fs-14 btnCardMatriculeSe">Matricule-se</p>
                        </div>
                       </a>
                  </div>
               </div>
               @endforeach

            </div>

         </div>
      </section>

         <section class="pt-4 pb-lg-5 bg-f5" id="foco">
            <div class="container-fluid px-md-5 py-5 bg-lines">

               <h1 class="mb-5 text-center font-rajdhani weight-700 fs-50 color-base">
                  SEU SONHO, NOSSA PAIXÃO!
               </h1>

               <div class="row align-items-center align-items-center">

                  <div class="col-lg-5 pr-lg-5 mb-5 mb-lg-0 mt--md-100">
                     <div uk-scrollspy="cls: uk-animation-slide-left; target: #img1; delay: 300;">
                        <div>
                         <img src="{{ asset('sites-proprios/geracaodevencedores-com-br/images/img-foco.png') }}" class="img-fluid" id="img1">
                        </div>
                     </div>
                  </div>
                  <div class="col-lg-6">

                     <div class="d-flex flex-column align-items-center">
                        
                        <div class="d-flex pr-lg-5 mb-4 mb-lg-0">
                           <div class="flex-column d-flex align-items-center">
                              <div class="bg-gv square-40 rounded-circle box-info flex-center">
                                 <i class="fas fa-check-circle text-white fs-22"></i>
                              </div>
                              <div class="d-none d-md-block">
                                 <hr class="uk-divider-vertical h-75 m-0 p-0 border border-base-gv">
                              </div>
                           </div>
                           <div class="ml-4">
                              <h1 class="display-7 weight-700 color-base font-raleway">Apoio Pedagógico</h1>
                              <p class="font-opensans weight-500 invisible-">
                                 O curso possui uma equipe pedagógica que tem como objetivo primordial de acompanhar o desenvolvimento do aluno, observar suas dificuldades e auxiliar no processo de aprendizagem. 
                              </p>
                           </div>
                        </div>
                        <div class="d-flex pr-lg-5 mb-4 mb-lg-0">
                           <div class="flex-column d-flex align-items-center">
                              <div class="bg-gv square-40 rounded-circle box-info flex-center">
                                 <i class="fas fa-check-circle text-white fs-22"></i>
                              </div>
                              <div class="d-none d-md-block">
                                 <hr class="uk-divider-vertical h-75 m-0 p-0 border border-base-gv">
                              </div>
                           </div>
                           <div class="ml-4">
                              <h1 class="display-7 weight-700 color-base font-raleway">Aulas Temáticas</h1>
                              <p class="font-opensans weight-500 invisible-">
                                 São aulas de conteúdos e exercícios dirigidos focados em preparar os alunos para o desenvolvimento das questões de provas. 
                              </p>
                           </div>
                        </div>
                        <div class="d-flex pr-lg-5 mb-4 mb-lg-0">
                           <div class="flex-column d-flex align-items-center">
                              <div class="bg-gv square-40 rounded-circle box-info flex-center">
                                 <i class="fas fa-check-circle text-white fs-22"></i>
                              </div>
                              <div class="d-none d-md-block">
                                 <hr class="uk-divider-vertical h-75 m-0 p-0 border border-base-gv">
                              </div>
                           </div>
                           <div class="ml-4">
                              <h1 class="display-7 weight-700 color-base font-raleway">Monitorias</h1>
                              <p class="font-opensans weight-500 invisible-">
                                São aulas ministradas ao longo de todo ano por um monitor para um pequeno grupo de alunos que buscam tirar as dúvidas e fixar conteúdo.
                              </p>
                           </div>
                        </div>
                        <div class="d-flex pr-lg-5 mb-4 mb-lg-0">
                           <div class="flex-column d-flex align-items-center">
                              <div class="bg-gv square-40 rounded-circle box-info flex-center">
                                 <i class="fas fa-check-circle text-white fs-22"></i>
                              </div>
                           </div>
                           <div class="ml-4">
                              <h1 class="display-7 weight-700 color-base font-raleway">Simulados</h1>
                              <p class="font-opensans weight-500 invisible-">
                                São aplicados vários simulados durante o ano, com questões similares dos concursos, com o objetivo de preparar, identificar dúvidas, melhorar o desempenho dos alunos e aumentar a taxa de aprovação nos concursos.
                              </p>
                           </div>
                        </div>

                     </div>

                     <div class="mt-4">
                        <a href="https://geracaodevencedores.com.br/f/inscricao-de-curso/50/t/672" target="_blank" class="mb-3 w-xs-100 btnCall mr-lg-1">
                           Quero garantir minha vaga agora
                        </a>
                        <a href="https://api.whatsapp.com/send?phone=5521985105348&text=Olá, vim através do Site Geração de Vencedores" target="_blank" class="mb-3 w-xs-100 btnCall bg-success">
                           <i class="fab fa-whatsapp mr-2"></i>
                           Gostaria de saber mais
                        </a>
                     </div>

                  </div>

               </div>

            </div>
         </section>

         <section id="sobre-nos" class="clip-path-horizontal bg-no-repeat bg-size-cover bg-fixed" style="background-image: url('{{ asset('sites-proprios/geracaodevencedores-com-br/images/bg-about.jpg') }}');">
            <div class="background-overlay-about"></div>
            <div class="container-fluid position-relative py-5 px-xl-5">
               <div class="row">

                  <div class="col-lg-6 mb-xs-4-5">

                     <img src="https://lmcv2.s3-sa-east-1.amazonaws.com/plataformas/64/quem-somos.JPG" alt="Quem somos - Geração de Vencedores" class="img-fluid w-100 object-fit-cover h-lg-300">
              
                     <div class="p-5 bg-white">

                        <p class="px-lg-4 pt-2 color-444 font-rajdhani weight-700 fs-28 line-height-30">
                          INSTITUCIONAL
                        </p>

                        <p class="px-lg-4 fs-15 text-align-justify color-grafite font-raleway weight-500">
                          O Curso GERAÇÃO DE VENCEDORES nasceu do desejo de criar uma Instituição de Ensino adequada às necessidades de cada aluno. Além do gosto pelo conhecimento, nosso projeto pedagógico objetiva estimular talentos, fortalecer conteúdos e investir no desenvolvimento socioemocional do educando.
                          <br><br>
                          Desde o ano de 2006, muitas conquistas fazem parte da nossa história, o que nos enche de orgulho e motiva a buscar, constantemente, novas formas de “GERAR SUCESSO”. O diferencial do Curso GERAÇÃO DE VENCEDORES está justamente no ambiente acolhedor construído ao longo desse tempo. Em nossas AULAS, o aluno sente-se em casa e, com isso, não vivencia a exaustiva pressão que acomete os jovens e crianças que se propõem à aprovação em um Concurso.
                          <br><br>
                          O aluno não precisa se sentir pressionado. Ao contrário disso, os estímulos do bem-estar são benéficos à organização de rotinas de estudo, à manutenção de impulsos positivos e, principalmente, viabilizam o cumprimento das metas e a tão almejada aprovação em Concursos Técnicos, Militares, Vestibulares e Enem.                    
                        </p>

                     </div>

                  </div>

                  <div class="col-lg-6 pl-lg-5">

                     <h2 class="font-rajdhani fs-25 weight-700 text-center color-para-os-pais">
                        PARA OS PAIS
                     </h2>

                     <h1 class="text-center font-rajdhani weight-700 fs-50 text-white">
                        SAIBA COMO CONTRIBUIR NOS ESTUDOS DO SEU FILHO
                     </h1>

                     <div class="p-5 bg-white">

                        <p class="px-lg-4 pt-2 color-444 font-rajdhani weight-700 fs-28 line-height-30">
                          FAÇA PARTE DESSA EQUIPE VENCEDORA!
                        </p>

                        <p class="px-lg-4 fs-15 text-align-justify color-grafite font-raleway weight-500">
                          O Curso GERAÇÃO DE VENCEDORES, com mais de 15 anos de excelência no mercado, tem investido em novos métodos e tecnologias para melhor atender aos nossos alunos. Além dos cursos presenciais, disponibilizamos dois formatos em EAD, adequados para as necessidades do estudante e dos editais dos Concursos. As aulas on-line ao vivo, são organizadas conforme horários pré-estabelecidos e contam com a presença ao vivo do professor, já no caso das videoaulas, além de vídeos interativos, slides e recursos áudio visuais, os alunos terão aulões on-line, chat de bate papo, simulados e tutoriais.
                        </p>

                        <div class="px-lg-4 mt-5">
                           <a href="#contato" uk-scroll="offset: 80;" class="btnCall">
                              QUERO SABER MAIS
                           </a>
                        </div>

                     </div>

                  </div>

               </div>
            </div>
         </section>

      <section class="py-5 bg-base-secondary" id="contato">
         <div class="container-fluid px-xl-7">

            <h1 class="text-center font-rajdhani weight-700 fs-50 text-white">
               CONTATO
            </h1>

            <h2 class="font-rajdhani fs-20 weight-700 text-center color-copyright">
               Entre em contato conosco para iniciar sua matrícula, tirar alguma dúvida ou enviar uma sugestão.
               <br>Preencha o formulário abaixo ou entre em contato via Whatsapp.
            </h2>

            <div class="row align-items-center pt-5">

               <div class="col-lg-6">

                      <div class="mb-4">
                          <div class="mb-0 font-poppins text-white d-flex align-items-center">
                           <div class="square-50 bg-f1 bg-white-hover box-info rounded-circle flex-center">
                             <i class="fa fa-clock fs-30 color-base color-hover-white"></i>
                           </div>
                            <span class="ml-4">
                              Segunda a sexta: 08h às 19h<br>
                              Sábado: 08h às 13h
                            </span>
                          </div>
                      </div>

                      <div class="mb-4">
                          <div class="mb-0 font-poppins text-white d-flex align-items-center">
                              <div>
                               <div class="square-50 bg-f1 bg-white-hover box-info rounded-circle flex-center">
                                 <i class="fa fa-envelope fs-30 color-base color-hover-white"></i>
                               </div>
                              </div>
                            <span class="ml-4 fs-xs-15 overflow-hidden">faleconosco@geracaodevencedores.com.br</span>
                          </div>
                      </div>

                      <div class="mb-4 width-large">
                          <a href="https://api.whatsapp.com/send?phone=5521985105348&text=Olá, vim através do Site Geração de Vencedores" target="_blank" class="text-d-none hover-d-none font-poppins text-white d-flex align-items-center">
                            <div class="square-50 bg-f1 bg-white-hover box-info rounded-circle flex-center">
                              <i class="fab fa-whatsapp fs-30 color-base color-hover-white"></i>
                            </div>
                            <span class="text-light text-d-none ml-lg-3 ml-2 ls-05">&nbsp;(21) 98510-5348</span>
                          </a>
                      </div>

                      <div class="mb-4">
                          <div class="text-d-none hover-d-none font-poppins d-flex align-items-center">
                            <div class="square-50 bg-f1 bg-white-hover box-info rounded-circle flex-center">
                              <i class="fa fa-phone fs-25 color-base"></i>
                            </div>
                            <span class="ml-3 ls-05">
                              <a href="tel:+552131773515" class="text-white text-d-none hover-d-none">
                                (21) 3177-3515 
                              </a>
                              /
                              <a href="tel:+552125703515" class="text-white text-d-none hover-d-none">
                                (21) 2570-3515
                              </a>
                            </span>
                          </div>
                      </div>

                      <div class="mb-4 width-xl-large">
                        <a class="text-d-none hover-d-none" href="https://www.google.com/maps/place/Curso+Preparat%C3%B3rio+Gera%C3%A7%C3%A3o+de+Vencedores+-+Pr%C3%A9+Militar+e+Vestibular+RJ/@-22.9235116,-43.2303729,15z/data=!4m5!3m4!1s0x0:0x659e312e4318103d!8m2!3d-22.9235116!4d-43.2303729" target="_blank">
                          <div class="d-flex align-items-center">
                              <div>
                               <div class="square-50 bg-f1 bg-white-hover box-info rounded-circle flex-center">
                                 <i class="fas fa-map-marker-alt fs-30 color-base color-hover-white"></i>
                               </div>
                              </div>
                            <address class="py-0 ml-3 my-0 text-light font-poppins">
                              Rua Conde de Bonfim, 277 sobreloja,Tijuca
                            </address>
                          </div>
                        </a>
                      </div>

               </div>

               <div class="col-lg-6 mt-3 mt-lg-0">

                      <form method="POST" id="formSolicitarContato" class="bg-base-blue p-lg-4 rounded-2">
                        @csrf
                        
                        @if(session('success'))
                        <div class='alert alert-success'><b>Sucesso!</b> Email enviado com êxito.</div>
                        @endif
                        
                        <div class="input-group mb-2">
                          <div class="input-group-prepend">
                            <div class="input-group-text bg-gv rounded-right-0 text-light border-0 py-4" style="padding-right: 13.5px;">
                              <i class="fa fa-user"></i>
                            </div>
                          </div>
                          <input type="text" name="nome" class="placeholder-edit form-control py-2-5 outline-0 rounded-2" placeholder="Seu Nome" required>
                        </div>

                        <div class="input-group mb-2">
                          <div class="input-group-prepend">
                            <div class="input-group-text bg-gv rounded-right-0 text-light border-0 py-4">
                              <i class="fa fa-phone"></i>
                            </div>
                          </div>
                          <input type="tel" name="celular" class="celular placeholder-edit rounded-left-0 form-control py-2-5 outline-0 rounded-2" placeholder="Celular" required>
                        </div>

                        <div class="input-group mb-0">
                          <div class="input-group-prepend">
                            <div class="input-group-text bg-gv rounded-right-0 text-light border-0 py-4">
                              <i class="fa fa-envelope"></i>
                            </div>
                          </div>
                          <input type="email" name="email" class="placeholder-edit rounded-left-0 form-control py-2-5 outline-0 rounded-2" placeholder="Email" required>
                        </div>

                        <input type="text" name="spamVerify" class="invisible h-0px" placeholder="Não preencha este campo.">

                        <input type="submit" name="btnSend" value="Solicitar Contato!" class="btnEntrarEmContato mt--5 rounded-3 border-0 w-100 weight-600 outline-0">
                                                
                      </form>

               </div>

            </div>

         </div>
      </section>


      <a href="https://api.whatsapp.com/send?phone=5521985105348&text=Olá, vim através do Site Geração de Vencedores" target="_blank" class="text-white fs-30">
          <div class="p-fixed bottom-0 right-0 mr-4 mb-4">
              <div class="square-60 rounded-circle bg-success flex-center shadow">
                  <i class="fab fa-whatsapp"></i>
              </div>
          </div>
      </a>
   
   </main>

   @include('sites-proprios/geracaodevencedores-com-br/componentes/footer')

   <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
   <script src="https://cdn.jsdelivr.net/npm/uikit@3.5.5/dist/js/uikit.min.js"></script>
   <script src="https://kit.fontawesome.com/2f91eaaef4.js" crossorigin="anonymous"></script>  
   <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>    
   <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
   <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

   <script>

      window.onscroll = function() {myFunction()};

      var header = document.getElementById("menu");
      var sticky = header.offsetTop;

      function myFunction() {
        if (window.pageYOffset > sticky) {
          header.classList.add("fixed-top");
          header.classList.add("mudaCor");
          $(".nav-link").addClass("nav-link-after-hover");
        } else {
          header.classList.remove("fixed-top");
          header.classList.remove("mudaCor");
          $(".nav-link").removeClass("nav-link-after-hover");
        }
      }

   </script>

    <script>
        $('.scroll-link').on('click', function(e) {

            e.preventDefault();

            var target = $( $(this).attr('href') );

            if( target.length ) {
                $('html, body').animate({
                    scrollTop: target.offset().top - 70
                }, 500);
            }

              $(".navbar-collapse").removeClass('show');
        });
    </script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.js"></script>
    <script>
       $(document).on("focus", ".celular", function() { 
         jQuery(this)
             .mask("(99) 9999-99999")
             .change(function (event) {  
                 target = (event.currentTarget) ? event.currentTarget : event.srcElement;  
                 phone = target.value.replace(/\D/g, '');
                 element = $(target);  
                 element.unmask();  
                 if(phone.length > 10) {  
                     element.mask("(99) 99999-9999");  
                 } else {  
                     element.mask("(99) 9999-9999?9");  
                 }  
         });
       });
    </script>

    {!! $plataforma->scripts_final_body !!}

</body>
</html>