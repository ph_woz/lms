<!doctype html>
<html lang="pt-br">
<head>

   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1">

   <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/uikit@3.5.9/dist/css/uikit.min.css" />
   <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
   <link rel="stylesheet" href="{{ asset('css/util.css') }}?v=3">
   <link rel="stylesheet" href="{{ asset('sites-proprios/faro/css/home.css') }}?v=3">

   <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
   
   <title>{{ $categoria->seo_title ?? $categoria->nome ?? 'FARO - Categoria' }}</title>
   <meta name="robots" content="index, follow">
   <meta name="description" content="{{ $categoria->seo_description ?? null }}"/>
   <meta name="keywords" content="{{ $categoria->seo_keywords ?? null }}"/>
   <link rel="canonical" href="{{ Request::url() }}" />
   <link rel="shortcut icon" href="{{ $plataforma->favicon }}">
   <meta property="og:locale" content="pt_BR" />
   <meta property="og:type" content="website" />
   <meta property="og:title" content="{{ $categoria->seo_title ?? $categoria->nome ?? 'FARO - Categoria' }}" />
   <meta property="og:description" content="{{ $categoria->seo_description ?? null }}" />
   <meta property="og:url" content="{{ Request::url() }}" />
   <meta property="og:site_name" content="{{ $plataforma->nome }}" />
   <meta property="og:image" content="{{ $plataforma->logotipo }}" />
   <meta name='author' content='WOZCODE | Pedro Henrique' />

</head>
<body class="bg-f4" id="inicio">

   @include('sites-proprios/faro/componentes/header')

   <main>

      <section class="py-5 mb-lg-5">
         <div class="container-fluid px-xl-7 py-lg-5 py-3 mt-lg-2">

           <h1 class="font-roboto weight-600 color-titulo fs-25">
             {{ $categoria->nome ?? 'Categoria não encontrada' }}
           </h1>

            <hr class="mb-4">

            <div class="row">
              @foreach($cursos as $curso)
               <div class="col-lg-3 mb-4 mb-lg-4-5">
                  <a href="/curso/{{$curso->slug}}" class="w-100">
                     <div class="w-100 uk-inline-clip uk-transition-toggle rounded box-shadow mb-4 mb-lg-0" tabindex="0">
                        <img src="{{ $curso->foto_miniatura ?? asset('images/no-image.jpeg') }}" class="uk-transition-scale-up uk-transition-opaque object-fit-cover w-100 h-lg-350 h-xs-350">
                        <div class="uk-position-bottom fig-caption flex-center px-3">
                           <h5 class="text-light fs-17 font-montserrat weight-700 ls-05 text-uppercase text-center">
                              {{ $curso->nome }}
                           </h5>
                        </div>
                     </div>
                  </a>
               </div>
              @endforeach
            </div>

         </div>
      </section>

   </main>

   @include('sites-proprios/faro/componentes/footer')

   <a href="https://api.whatsapp.com/send?phone=55{{preg_replace('/\D/', '', $plataforma->whatsapp)}}&text=Olá, vim através do Site {{ $plataforma->nome }}" target="_blank" class="text-white fs-30">
       <div class="fixed-bottom right-0 me-3 mb-3 z-3">
           <div class="square-60 rounded-circle bg-wpp flex-center box-shadow-wpp">
               <i class="fab fa-whatsapp"></i>
           </div>
       </div>
   </a>

   <script src="https://cdn.jsdelivr.net/npm/uikit@3.5.9/dist/js/uikit.min.js"></script>
   <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-U1DAWAznBHeqEIlVSCgzq+c9gqGAJn5c/t99JyeKa9xxaYpSvHU5awsuZVVFIhvj" crossorigin="anonymous"></script>
   <script>

      window.onscroll = function() {myFunction()};

      var header = document.getElementById("menu");
      var sticky = header.offsetTop;

      function myFunction() {
        if (window.pageYOffset > sticky) {
          header.classList.add("fixed-top");
        } else {
          header.classList.remove("fixed-top");
        }
      }
   </script>

</body>
</html>