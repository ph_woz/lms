<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PlanoBeneficio extends Model
{
    protected $table = 'planos_beneficios';

	public $timestamps = false;

    public $fillable = [
        'plataforma_id',
        'plano_id',
        'descricao',
    ];

    public static  function boot()
    {
        parent::boot(); 
        static::creating(function ($model)
        {
            $model->plataforma_id = session('plataforma_id') ?? \Auth::user()->plataforma_id ?? 1;
        });
    }
    
    public function scopePlataforma($query)
    {
        return $query->where('plataforma_id', session('plataforma_id') ?? \Auth::user()->plataforma_id);
    }
    
}
