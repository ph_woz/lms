<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AjudaTutorialController extends Controller
{
    public function lista()
    {
        $getNomePaginaInterno = 'Tutoriais';

        return view('admin.ajuda.tutorial.lista', compact('getNomePaginaInterno') );
    }
}