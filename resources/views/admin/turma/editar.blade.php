@extends('layouts.admin.master')

@section('css')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.14.0-beta2/dist/css/bootstrap-select.min.css">
@endsection

@section('content')

<div>

    <div class="mb-3 d-flex justify-content-end">
        <a href="{{ route('admin.turma.info', ['id' => $turma->id ?? null]) }}" class="btn bg-base text-white">
            <i class="fa fa-eye"></i>
        </a>
    </div>

    <form method="POST" class="mb-4">
    @csrf

        <section class="mb-4-5">
            
            <a href="#boxInfoPrincipal" data-bs-toggle="collapse" aria-expanded="false" class="hover-d-none">
                <div class="section-title">
                    Informações Gerais
                </div>
            </a>
            <hr/>
            
            <div class="collapse show" id="boxInfoPrincipal">

                <div class="d-lg-flex mb-3">

                    <div class="w-100">
                        <label>Nome</label>
                        <input type="text" name="nome" class="form-control p-form" placeholder="Nome" required value="{{ $turma->nome }}">
                    </div>
                    <div class="w-100 mx-lg-3 my-3 my-lg-0">
                        <label>Curso</label>
                        <select name="curso_id" class="selectpicker" title="Selecione" data-width="100%" data-live-search="true" required>
                            @foreach($cursos as $curso)
                                <option value="{{ $curso->id }}" @if($turma->curso_id == $curso->id) selected @endif>
                                    {{ $curso->referencia }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="w-lg-35">
                        <label>Status</label>
                        <select name="status" class="select-form">
                            <option value="0">Ativo</option>
                            <option value="1" @if($turma->status == 1) selected @endif>Desativado</option>
                        </select>
                    </div>

                </div>

                <div class="d-lg-flex mb-3">

                    <div class="w-lg-65">
                        <label>Nota mínima para aprovação</label>
                        <input type="text" name="nota_minima_aprovacao" class="form-control p-form" placeholder="Digite o valor da nota" value="{{ $turma->nota_minima_aprovacao }}">
                    </div>
                    <div class="w-100 mx-lg-3 my-3 my-lg-0">
                        <label>Formulário de Inscrição de Matrícula</label>
                        <select name="formulario_id" class="selectpicker" title="Selecione" data-width="100%" data-live-search="true">
                            <option value="">Selecione</option>
                            @foreach($formularios as $formulario)
                                <option value="{{ $formulario->id }}" @if($turma->formulario_id == $formulario->id) selected @endif>
                                    {{ $formulario->referencia }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="w-lg-70">
                        <label>Tipo de Inscrição de Matrícula</label>
                        <select name="tipo_inscricao" class="select-form">
                            <option value="requer-aprovacao">Requer aprovação</option>
                            <option value="auto-inscricao" @if($turma->tipo_inscricao == 'auto-inscricao') selected @endif>Auto Inscrição</option>
                        </select>
                    </div>
                    <div class="w-lg-30 ms-lg-3 mt-3 mt-lg-0">
                        <label>Publicar</label>
                        <select name="publicar" onchange="controlBoxApresentacao(this.value)" class="select-form">
                            <option value="N">Não</option>
                            <option value="S" @if($turma->publicar == 'S') selected @endif>Sim</option>
                        </select>
                    </div>

                </div>
                    
                @if(\App\Models\Unidade::checkActive() === true)
                <div class="mb-3 boxUnidadeRemove">
                    <label>Unidade</label>
                    <select name="unidade_id" id="unidade_id" class="selectpicker" data-width="100%">
                        <option value="">Selecione</option>
                        @foreach($unidades as $unidade)
                            <option value="{{ $unidade->id }}" @if($turma->unidade_id == $unidade->id) selected @endif>
                                {{ $unidade->nome }}
                            </option>
                        @endforeach
                    </select>
                </div>
                @endif

                <div id="boxTurmaApresentacao" @if($turma->publicar == 'N') style="display: none;" @endif>

                    <div class="d-lg-flex mb-3">

                        <div class="w-100">
                            <label>Data de Início</label>
                            <input type="date" name="data_inicio" class="form-control p-form" value="{{ $turma->data_inicio }}">
                        </div>
                        <div class="w-100 mx-lg-3 my-3 my-lg-0">
                            <label>Data de Término</label>
                            <input type="date" name="data_termino" class="form-control p-form" value="{{ $turma->data_termino }}">
                        </div>
                        <div class="w-100">
                            <label>Duração</label>
                            <input type="text" name="duracao" class="form-control p-form" placeholder="Ex: 6 meses" value="{{ $turma->duracao }}">
                        </div>
                        <div class="w-100 ms-lg-3 mt-3 mt-lg-0">
                            <label>Modalidade</label>
                            <select name="modalidade" onchange="controlBoxModalidade(this.value)" class="select-form">
                                <option value="">Selecione</option>
                                <option value="EaD" @if($turma->modalidade == 'EaD') selected @endif>EaD</option>
                                <option value="Presencial" @if($turma->modalidade == 'Presencial') selected @endif>Presencial</option>
                                <option value="Semipresencial" @if($turma->modalidade == 'Semipresencial') selected @endif>Semipresencial</option>
                                <option value="Presencial/Online/EaD" @if($turma->modalidade == 'Presencial/Online/EaD') selected @endif>Presencial/Online/EaD</option>
                            </select>
                        </div>

                    </div>

                    <div id="boxPresencial" @if($turma->modalidade == 'EaD' || $turma->modalidade == '') style="display:none;" @endif>
                        <div class="d-lg-flex mb-3">

                            <div class="w-lg-25 me-lg-3 mb-3 mb-lg-0">
                                <label>Turno</label>
                                <select name="turno" class="select-form">
                                    <option value="">Selecione</option>
                                    <option value="M" @if($turma->turno == 'M') selected @endif>Manhã</option>
                                    <option value="T" @if($turma->turno == 'T') selected @endif>Tarde</option>
                                    <option value="N" @if($turma->turno == 'N') selected @endif>Noturno</option>
                                    <option value="I" @if($turma->turno == 'I') selected @endif>Integral</option>
                                </select>
                            </div>
                            
                            <div class="w-100 me-lg-3 mb-3 mb-lg-0">
                                <label>Dias de Aula</label>
                                <select name="frequencia[]" class="selectpicker" data-width="100%" title="Selecione" multiple> 
                                    <option value="Segunda" @if(\Str::contains($turma->frequencia, 'Segunda')) selected @endif>Segunda-feira</option>
                                    <option value="Terça"   @if(\Str::contains($turma->frequencia, 'Terça'))   selected @endif>Terça-feira</option>
                                    <option value="Quarta"  @if(\Str::contains($turma->frequencia, 'Quarta'))  selected @endif>Quarta-feira</option>
                                    <option value="Quinta"  @if(\Str::contains($turma->frequencia, 'Quinta'))  selected @endif>Quinta-feira</option>
                                    <option value="Sexta"   @if(\Str::contains($turma->frequencia, 'Sexta'))   selected @endif>Sexta-feira</option>
                                    <option value="Sábado"  @if(\Str::contains($turma->frequencia, 'Sábado'))  selected @endif>Sábado</option>
                                    <option value="Domingo" @if(\Str::contains($turma->frequencia, 'Domingo')) selected @endif>Domingo</option>
                                </select>
                            </div>
                            <div class="w-lg-50">
                                <label>Horário</label>
                                <input type="text" name="horario" class="horario form-control p-form" placeholder="00:00 - 00:00" value="{{ $turma->horario }}">
                            </div>

                        </div>
                    </div>

                </div>

            </div>

        </section>

        <section class="mb-4-5" id="boxSecValores">

            <a href="#boxValores" data-bs-toggle="collapse" aria-expanded="false" class="hover-d-none">
                <div class="section-title">
                    Valores e Forma de Pagamento
                </div>
            </a>
            <hr/>

            <div class="collapse" id="boxValores">

                <div class="d-lg-flex mb-3">

                    <div class="w-lg-75">
                        <label>Tipo de Valor</label>
                        <select name="tipo_valor" onchange="setTipoValor(this.value)" class="select-form">
                            <option value="F">Fechado</option>
                            <option value="M" @if(old('tipo_valor') == 'M' || $turma->tipo_valor == 'M') selected @endif>Mensalidade</option>
                        </select>
                    </div>

                    <div class="boxValorFechado w-100 ms-lg-3 mt-3 mt-lg-0">
                        <label>Valor a vista</label>
                        <input type="text" name="valor_a_vista" class="valor form-control p-form" placeholder="R$" value="{{ old('valor_a_vista') ?? $turma->valor_a_vista }}">
                    </div>
                    <div class="boxValorFechado w-100 ms-lg-3 mt-3 mt-lg-0">
                        <label>Valor a vista promocional</label>
                        <input type="text" name="valor_promocional" class="valor form-control p-form" placeholder="R$" value="{{ old('valor_promocional') ?? $turma->valor_promocional }}">
                    </div>
                    <div class="boxValorFechado w-100 ms-lg-3 mt-3 mt-lg-0">
                        <label>Valor parcelado</label>
                        <input type="text" name="valor_parcelado" class="form-control p-form" placeholder="Ex: 3x de R$ 97,99" value="{{ old('valor_parcelado') ?? $turma->valor_parcelado }}">
                    </div>
                    <div class="boxValorMensalidade w-100 ms-lg-3 mt-3 mt-lg-0" style="display: none;">
                        <label>Valor da Mensalidade</label>
                        <div class="d-flex w-100">
                            <input type="text" name="valor_mensalidade" class="valor form-control p-form rounded-right-0" placeholder="R$" value="{{ old('valor_mensalidade') ?? $turma->valor_mensalidade }}">
                            <select name="tipo_mensalidade" onchange="setBoxProximosSemestre(this.value)" class="w-lg-80 select-form rounded-left-0">
                                <option value="F">Fixo (até a conclusão do curso)</option>
                                <option value="R" @if(old('tipo_mensalidade') == 'R' || $turma->tipo_mensalidade == 'R') selected @endif>Primeiro Semestre, somente</option>
                            </select>
                        </div>
                    </div>
                    <div class="w-100 ms-lg-3 mt-3 mt-lg-0">
                        <label>% de desconto com Agilpass</label>
                        <input type="text" name="desconto_agilpass" class="number form-control p-form" placeholder="Desconto em cada pagamento com Agilpass" value="{{ old('desconto_agilpass') ?? $turma->desconto_agilpass }}">
                    </div>

                </div>

                <hr/>

                <div id="boxProximosSemestres" class="w-100 boxValorMensalidade mb-3" @if(old('tipo_valor') == 'F' || $turma->tipo_valor == 'F')  style="display: none;" @endif>
                    <div class="p-3 rounded" style="border:1.5px solid #dbdbdb;">
                        <div class="mb-1 w-100">
                            <p class="mb-0">Próximos Semestres</p>
                            <hr class="mt-2">
                        </div>
                        <div class="boxLineSemestre" id="boxPrimeiraLineSemestre">
                            <div class="mb-2 d-flex align-items-center">
                                <input type="text" name="semestres[]" style="width: 120px;" readonly class="form-control p-form text-center px-0 me-2" value="2° Semestre">
                                <input type="text" name="valores_por_semestres[]" class="valor form-control p-form" placeholder="R$ (valor da mensalidade deste semestre)">
                                <a href="#" class="btnRemoveSemestre btn btn-danger py-2 ms-2">
                                    <i class="fa fa-trash"></i>
                                </a>
                            </div>
                        </div>
                        <div id="appendBoxLineSemestre"></div>
                        <button type="button" class="btn-append btn-append-semestre btn btn-primary py-1-5 fs-13">Adicionar +</button>
                    </div>
                </div>

                <div class="w-100 boxValorFormaIngresso mb-3" id="boxValoresPorFormasDeIngresso">
                    <div class="p-3 rounded" style="border:1.5px solid #dbdbdb;">
                        <div class="mb-1 w-100">
                            <p class="mb-0">Formas de Ingresso</p>
                            <hr class="mt-2">
                        </div>
                        <div class="boxLineFormaIngresso">
                            <div class="mb-2 d-flex align-items-center">
                                <input type="text" name="formas[]" style="width: 180px;" readonly class="form-control p-form me-2" value="Vestibular">
                                <input type="text" name="valores_formas[]" class="valor form-control p-form" placeholder="% a mais do valor de cada pagamento" value="{{ $formasIngresso->where('nome','Vestibular')->pluck('desconto')[0] ?? null }}">
                            </div>
                        </div>
                        <div class="boxLineFormaIngresso">
                            <div class="mb-2 d-flex align-items-center">
                                <input type="text" name="formas[]" style="width: 180px;" readonly class="form-control p-form me-2" value="Enem">
                                <input type="text" name="valores_formas[]" class="valor form-control p-form" placeholder="% a mais do valor de cada pagamento" value="{{ $formasIngresso->where('nome','Enem')->pluck('desconto')[0] ?? null }}">
                            </div>
                        </div>
                        <div class="boxLineFormaIngresso">
                            <div class="mb-2 d-flex align-items-center">
                                <input type="text" name="formas[]" style="width: 180px;" readonly class="form-control p-form me-2" value="Transferência Externa">
                                <input type="text" name="valores_formas[]" class="valor form-control p-form" placeholder="% a mais do valor de cada pagamento" value="{{ $formasIngresso->where('nome','Transferência Externa')->pluck('desconto')[0] ?? null }}">
                            </div>
                        </div>
                        <div class="boxLineFormaIngresso">
                            <div class="mb-2 d-flex align-items-center">
                                <input type="text" name="formas[]" style="width: 180px;" readonly class="form-control p-form me-2" value="Portador de Diploma">
                                <input type="text" name="valores_formas[]" class="valor form-control p-form" placeholder="% a mais do valor de cada pagamento" value="{{ $formasIngresso->where('nome','Portador de Diploma')->pluck('desconto')[0] ?? null }}">
                            </div>
                        </div>
                        <div class="boxLineFormaIngresso">
                            <div class="mb-2 d-flex align-items-center">
                                <input type="text" name="formas[]" style="width: 180px;" readonly class="form-control p-form me-2" value="Histórico Escolar">
                                <input type="text" name="valores_formas[]" class="valor form-control p-form" placeholder="% a mais do valor de cada pagamento" value="{{ $formasIngresso->where('nome','Histórico Escolar')->pluck('desconto')[0] ?? null }}">
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="boxValorFechado row mb-3">

                    <div class="col-lg-4 mb-4">
                        <div class="p-3 rounded" style="border:1.5px solid #dbdbdb;">
                            <div class="mb-1 w-100">
                                <p class="mb-0">Cartão de Crédito</p>
                                <hr class="mt-2">
                            </div>
                            <div class="boxLineParcela">
                                @foreach($formasPagamento->where('tipo','CC') as $formaPagamento)
                                <div class="mb-2 d-flex align-items-center">
                                    <input type="text" name="cartao-de-credito[parcelas][]" class="number form-control p-form" placeholder="Parcela" value="{{  $formaPagamento->parcela }}">
                                    <input type="text" style="width: 45px;" disabled readonly class="form-control p-form text-center px-0 mx-2" placeholder="de">
                                    <input type="text" name="cartao-de-credito[valores][]" class="valor form-control p-form" placeholder="R$" value="{{  $formaPagamento->valor }}">
                                    <a href="#" class="btnRemoveParcela btn btn-danger py-2 ms-2">
                                        <i class="fa fa-trash"></i>
                                    </a>
                                </div>
                                @endforeach
                                <div class="mb-2 d-flex align-items-center">
                                    <input type="text" name="cartao-de-credito[parcelas][]" class="number form-control p-form" placeholder="Parcela">
                                    <input type="text" style="width: 45px;" disabled readonly class="form-control p-form text-center px-0 mx-2" placeholder="de">
                                    <input type="text" name="cartao-de-credito[valores][]" class="valor form-control p-form" placeholder="R$">
                                    <a href="#" class="btnRemoveParcela btn btn-danger py-2 ms-2">
                                        <i class="fa fa-trash"></i>
                                    </a>
                                </div>
                            </div>
                            <button type="button" class="btn-append btn-append-parcela-cc btn btn-primary py-1-5 fs-13">Adicionar +</button>
                        </div>
                    </div>
                    <div class="col-lg-4 mb-4">
                        <div class="p-3 rounded" style="border:1.5px solid #dbdbdb;">
                            <div class="mb-1 w-100">
                                <p class="mb-0">Cartão de Débito</p>
                                <hr class="mt-2">
                            </div>
                            <div class="boxLineParcela">
                                @foreach($formasPagamento->where('tipo','CD') as $formaPagamento)
                                <div class="mb-2 d-flex align-items-center">
                                    <input type="text" name="cartao-de-debito[parcelas][]" class="number form-control p-form" placeholder="Parcela" value="{{  $formaPagamento->parcela }}">
                                    <input type="text" style="width: 45px;" disabled readonly class="form-control p-form text-center px-0 mx-2" placeholder="de">
                                    <input type="text" name="cartao-de-debito[valores][]" class="valor form-control p-form" placeholder="R$" value="{{  $formaPagamento->valor }}">
                                    <a href="#" class="btnRemoveParcela btn btn-danger py-2 ms-2">
                                        <i class="fa fa-trash"></i>
                                    </a>
                                </div>
                                @endforeach
                                <div class="mb-2 d-flex align-items-center">
                                    <input type="text" name="cartao-de-debito[parcelas][]" class="number form-control p-form" placeholder="Parcela">
                                    <input type="text" style="width: 45px;" disabled readonly class="form-control p-form text-center px-0 mx-2" placeholder="de">
                                    <input type="text" name="cartao-de-debito[valores][]" class="valor form-control p-form" placeholder="R$">
                                    <a href="#" class="btnRemoveParcela btn btn-danger py-2 ms-2">
                                        <i class="fa fa-trash"></i>
                                    </a>
                                </div>
                            </div>
                            <button type="button" class="btn-append btn-append-parcela-cd btn btn-primary py-1-5 fs-13">Adicionar +</button>
                        </div>
                    </div>
                    <div class="col-lg-4 mb-4">
                        <div class="p-3 rounded" style="border:1.5px solid #dbdbdb;">
                            <div class="mb-1 w-100">
                                <p class="mb-0">Boleto</p>
                                <hr class="mt-2">
                            </div>
                            <div class="boxLineParcela">
                                @foreach($formasPagamento->where('tipo','BO') as $formaPagamento)
                                <div class="mb-2 d-flex align-items-center">
                                    <input type="text" name="boleto[parcelas][]" class="number form-control p-form" placeholder="Parcela" value="{{  $formaPagamento->parcela }}">
                                    <input type="text" style="width: 45px;" disabled readonly class="form-control p-form text-center px-0 mx-2" placeholder="de">
                                    <input type="text" name="boleto[valores][]" class="valor form-control p-form" placeholder="R$" value="{{  $formaPagamento->valor }}">
                                    <a href="#" class="btnRemoveParcela btn btn-danger py-2 ms-2">
                                        <i class="fa fa-trash"></i>
                                    </a>
                                </div>
                                @endforeach
                                <div class="mb-2 d-flex align-items-center">
                                    <input type="text" name="boleto[parcelas][]" class="number form-control p-form" placeholder="Parcela">
                                    <input type="text" style="width: 45px;" disabled readonly class="form-control p-form text-center px-0 mx-2" placeholder="de">
                                    <input type="text" name="boleto[valores][]" class="valor form-control p-form" placeholder="R$">
                                    <a href="#" class="btnRemoveParcela btn btn-danger py-2 ms-2">
                                        <i class="fa fa-trash"></i>
                                    </a>
                                </div>
                            </div>
                            <button type="button" class="btn-append btn-append-parcela-bo btn btn-primary py-1-5 fs-13">Adicionar +</button>
                        </div>
                    </div>

                </div>

            </div>

        </section>

        <button type="submit" name="sent" value="ok" class="btn-add mt-3">Salvar Alterações</button>   

    </form>

</div>    

@endsection

@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.14.0-beta2/dist/js/bootstrap-select.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.js"></script>
<script>

    $(".horario").mask("00:00 - 00:00");

    @if(\Auth::user()->restringir_unidade == 'S' && \Auth::user()->unidade_id != null)
        jQuery("#unidade_id option:contains('Selecione')").remove();
    @endif

    $(document).on("focus", ".valor", function() { 
        $(this).mask('#.##0,00', {
          reverse: true
        });
    });

    $(document).on("focus", ".number", function() { 
        $(this).mask("000");
    });


    $(".btn-append-parcela-cc").click(function()
    {
        $(this).prev().append('<div class="mb-2 d-flex align-items-center"> <input type="text" name="cartao-de-credito[parcelas][]" class="number form-control p-form" placeholder="Parcela"> <input type="text" style="width: 45px;" disabled readonly class="form-control p-form text-center px-0 mx-2" placeholder="de"> <input type="text" name="cartao-de-credito[valores][]" class="valor form-control p-form" placeholder="R$"> <a href="#" class="btnRemoveParcela btn btn-danger py-2 ms-2"> <i class="fa fa-trash"></i> </a> </div> </div>');
    });

    $(".btn-append-parcela-cd").click(function()
    {
        $(this).prev().append('<div class="mb-2 d-flex align-items-center"> <input type="text" name="cartao-de-debito[parcelas][]" class="number form-control p-form" placeholder="Parcela"> <input type="text" style="width: 45px;" disabled readonly class="form-control p-form text-center px-0 mx-2" placeholder="de"> <input type="text" name="cartao-de-debito[valores][]" class="valor form-control p-form" placeholder="R$"> <a href="#" class="btnRemoveParcela btn btn-danger py-2 ms-2"> <i class="fa fa-trash"></i> </a> </div> </div>');
    });

    $(".btn-append-parcela-bo").click(function()
    {
        $(this).prev().append('<div class="mb-2 d-flex align-items-center"> <input type="text" name="boleto[parcelas][]" class="number form-control p-form" placeholder="Parcela"> <input type="text" style="width: 45px;" disabled readonly class="form-control p-form text-center px-0 mx-2" placeholder="de"> <input type="text" name="boleto[valores][]" class="valor form-control p-form" placeholder="R$"> <a href="#" class="btnRemoveParcela btn btn-danger py-2 ms-2"> <i class="fa fa-trash"></i> </a> </div> </div>');
    });

    $(document).on('click', '.btnRemoveParcela', function(e)
    {
        e.preventDefault();
        $(this).parent().remove();
    });

    function controlBoxApresentacao(value)
    {
        if(value == 'S')
        {
            document.getElementById('boxTurmaApresentacao').style.display = 'block';
        
        } else {

            document.getElementById('boxTurmaApresentacao').style.display = 'none';
        }
    }

    function controlBoxModalidade(value)
    {
        if(value == 'EaD' || value == '')
        {
            document.getElementById('boxPresencial').style.display = 'none';
        
        } else {

            document.getElementById('boxPresencial').style.display = 'block';
        }
    }

    let iSemestre = 2;
    $(".btn-append-semestre").click(function()
    {
        iSemestre++;
        $(this).prev().append('<div class="boxLineSemestre"> <div class="mb-2 d-flex align-items-center"> <input type="text" name="semestres[]" style="width: 120px;" readonly class="form-control p-form text-center px-0 me-2" value="'+iSemestre+'° Semestre"> <input type="text" name="valores_por_semestres[]" class="valor form-control p-form" placeholder="R$ (valor da mensalidade deste semestre)" required> <a href="#" class="btnRemoveSemestre btn btn-danger py-2 ms-2"> <i class="fa fa-trash"></i> </a> </div> </div>');
    });

    $(document).on('click', '.btnRemoveSemestre', function(e)
    {
        e.preventDefault();
        $(this).parent().remove();
    });

    $(document).ready(function() {

        setTipoValor('{{$turma->tipo_valor}}');
        setBoxProximosSemestre('{{$turma->tipo_mensalidade}}');

        @if(count($mensalidadesPorSemestres) > 0)

            $("#boxPrimeiraLineSemestre").remove();

            @foreach($mensalidadesPorSemestres as $mensalidade)
          
                $("#appendBoxLineSemestre").append('<div class="boxLineSemestre"> <div class="mb-2 d-flex align-items-center"> <input type="text" name="semestres[]" style="width: 120px;" readonly class="form-control p-form text-center px-0 me-2" value="{{$mensalidade->semestre}}° Semestre"> <input type="text" name="valores_por_semestres[]" class="valor form-control p-form" placeholder="R$ (valor da mensalidade deste semestre)" required value="{{$mensalidade->valor}}"> <a href="#" class="btnRemoveSemestre btn btn-danger py-2 ms-2"> <i class="fa fa-trash"></i> </a> </div> </div>');
          
            @endforeach

            iSemestre = {{ count($mensalidadesPorSemestres) }} + 1;

        @endif
    });

    function setTipoValor(tipo_valor)
    {
        if(tipo_valor == 'M')
        {
            $('.boxValorFechado').hide();
            $('.boxValorMensalidade').show();
        
        } else {

            $('.boxValorMensalidade').hide();
            $('.boxValorFechado').show();
        }
        
        $("#boxProximosSemestres").hide();
    }

    function setBoxProximosSemestre(tipo_mensalidade)
    {
        if(tipo_mensalidade == 'R')
        {
            document.getElementById('boxProximosSemestres').style.display = 'block';
        
        } else {

            document.getElementById('boxProximosSemestres').style.display = 'none';
        }
    }

</script>
@endpush