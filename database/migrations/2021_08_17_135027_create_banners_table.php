<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBannersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('banners', function (Blueprint $table) {
            $table->id();
            $table->integer('plataforma_id');
            $table->text('titulo')->nullable();
            $table->text('subtitulo')->nullable();
            $table->text('link')->nullable();
            $table->string('botao_texto')->nullable();
            $table->string('botao_link')->nullable();
            $table->string('overlay', 1)->default('N');
            $table->string('overlay_color')->nullable();
            $table->string('overlay_opacity')->nullable();
            $table->string('ordem', 3)->nullable();
            $table->integer('status')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('banners');
    }
}
