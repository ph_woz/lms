<?php

namespace App\Http\Livewire\Admin\Unidade;

use Livewire\WithPagination;
use Livewire\Component;
use App\Models\Unidade;

class Lista extends Component
{
    use WithPagination;

    protected $paginationTheme = 'bootstrap';

    public $search;
    public $status = 0;
    public $ordem;

    protected $queryString = ['search','status','ordem'];

    public function render()
    {
        $unidades = Unidade::select('id','nome')->plataforma()->where('status', $this->status);

        if($this->search)
        {
            $unidades = $unidades->where('nome', 'like', '%'.$this->search.'%');
        }

        $countUnidades = $unidades->count();


        if($this->ordem == 'desc' || $this->ordem == 'asc')
        {
            $unidades = $unidades->orderBy('id', $this->ordem);
        
        } else {

            $unidades = $unidades->orderBy('nome');
        }
        

        $unidades = $unidades->paginate(10);


        return view('livewire.admin.unidade.lista', [
            'unidades'      => $unidades,
            'countUnidades' => $countUnidades,
        ]);
    }
}
