<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Util;
use App\Models\TrilhaAluno;
use App\Models\TrilhaTurma;
use App\Models\Turma;
use App\Models\TurmaAluno;
use App\Models\User;
use App\Models\UserEndereco;
use App\Models\Plataforma;
use App\Models\PlataformaPlano;
use App\Models\Categoria;
use App\Models\UserCategoria;
use App\Models\Unidade;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;


class AlunoController extends Controller
{
    public function lista()
    {
        return view('admin.aluno.lista');
    }
    
    public function info($user_id)
    {
        return view('admin.aluno.info', compact('user_id') );
    }

    public function add()
    {
        $plano_id = Plataforma::select('plano_id')->plataforma()->pluck('plano_id')[0] ?? 0;
        
        $maxQntAlunos = PlataformaPlano::select('max_qnt_alunos')->plataforma()->where('id', $plano_id)->pluck('max_qnt_alunos')[0] ?? null;
        $countAlunos  = User::select('id')->plataforma()->aluno()->ativo()->count();

        $categorias = Categoria::select('id','nome')->plataforma()->ativo()->tipo('aluno')->orderBy('nome')->get();
        $unidades   = Unidade::select('id','nome')->plataforma()->ativo()->unidade()->orderBy('nome')->get();

        $planoMaxQntAlunosExcedido = false;

        if($plano_id)
        {
            if($countAlunos >= $maxQntAlunos)
            {
                $planoMaxQntAlunosExcedido = true;
                
            }
        }

        if(request()->has('email'))
        {        
            $checkEmailExists = User::select('id')->plataforma()->where('email', request('email'))->exists();

            if($checkEmailExists == true)
            {
                return back()->withInput()->with('danger','Já existe uma pessoa cadastrada com este Email.');
            }

            if(request('cpf'))
            {
                $checkCPFExists = User::select('id')->plataforma()->where('cpf', request('cpf'))->exists();

                if($checkCPFExists == true)
                {
                    return back()->withInput()->with('danger','Já existe uma pessoa cadastrada com este CPF.');
                }
            }

            if(request('rg'))
            {
                $checkRGExists = User::select('id')->plataforma()->where('rg', request('rg'))->exists();

                if($checkRGExists == true)
                {
                    return back()->withInput()->with('danger','Já existe uma pessoa cadastrada com este RG.');
                }
            }

            request()->merge(['data_nascimento' => Util::replaceDateEn(request('data_nascimento')) ?? null]);

            $user = User::create(request()->all() + ['tipo' => 'aluno', 'password' => Hash::make(request('new_password'))]);

            if(array_filter(request('endereco')))
            { 
                $endereco = UserEndereco::create(request('endereco') + ['plataforma_id' => session('plataforma_id') ?? \Auth::user()->plataforma_id, 'user_id' => $user->id]);
            }

            $this->syncCategorias($user->id);

            return redirect()->route('admin.aluno.info', ['id' => $user->id])->with('success', 'Aluno cadastrado com êxito.');
        }

    	return view('admin.aluno.add', compact('categorias','unidades','planoMaxQntAlunosExcedido') );
    }

    public function editar($id)
    {
        $user = User::plataforma()->where('id', $id)->first();
        $endereco = UserEndereco::plataforma()->where('user_id', $user->id)->first();
        $categoriasSelected = UserCategoria::select('categoria_id')->plataforma()->where('user_id', $user->id)->get()->pluck('categoria_id')->toArray();

        $categorias = Categoria::select('id','nome')->plataforma()->ativo()->tipo('aluno')->orderBy('nome')->get();
        $unidades   = Unidade::select('id','nome')->plataforma()->ativo()->unidade()->orderBy('nome')->get();

        $planoMaxQntAlunosExcedido = false;

        if(request()->has('email'))
        {        
            $checkEmailExists = User::select('id')->where('id','<>', $user->id)->plataforma()->where('email', request('email'))->exists();

            if($checkEmailExists == true)
            {
                return back()->withInput()->with('danger','Já existe uma pessoa cadastrada com este Email.');
            }

            if(request('cpf'))
            {
                $checkCPFExists = User::select('id')->where('id','<>', $user->id)->plataforma()->where('cpf', request('cpf'))->exists();

                if($checkCPFExists == true)
                {
                    return back()->withInput()->with('danger','Já existe uma pessoa cadastrada com este CPF.');
                }
            }

            if(request('rg'))
            {
                $checkRGExists = User::select('id')->where('id','<>', $user->id)->plataforma()->where('rg', request('rg'))->exists();

                if($checkRGExists == true)
                {
                    return back()->withInput()->with('danger','Já existe uma pessoa cadastrada com este RG.');
                }
            }

            request()->merge(['data_nascimento' => Util::replaceDateEn(request('data_nascimento')) ?? null]);

            $user->update(request()->all());

            if(isset($endereco))
            {
                $endereco->update(request('endereco'));
            
            } else {

                UserEndereco::create(request('endereco') + ['plataforma_id' =>  $user->plataforma_id, 'user_id' => $user->id]);
            }

            if(request('new_password'))
            {
                $user->update(['password' => Hash::make(request('new_password'))]);
            }

            $this->syncCategorias($user->id);

            return redirect()->route('admin.aluno.info', ['id' => $user->id])->with('success', 'Aluno atualizado com êxito.');
        }

        return view('admin.aluno.editar', compact('user','endereco','categoriasSelected','categorias','unidades','planoMaxQntAlunosExcedido') );
    }

    public function matricularAlunoNaTrilha($user_id)
    {
        $verificaSeJaMatriculado = TrilhaAluno::select('id')->plataforma()
            ->where('aluno_id', $user_id)
            ->where('trilha_id', request('matricular_aluno_trilha_id'))
            ->exists();

        if($verificaSeJaMatriculado == false)
        {
            $trilhaAluno = TrilhaAluno::create([
                'plataforma_id' => session('plataforma_id'),
                'trilha_id' => request('matricular_aluno_trilha_id'),
                'aluno_id'  => $user_id,
            ]);

            $turmasIds = TrilhaTurma::select('turma_id')->plataforma()->where('trilha_id', request('matricular_aluno_trilha_id'))->get()->pluck('turma_id')->toArray();

            foreach($turmasIds as $turma_id)
            {
                $verificaSeJaMatriculado = TurmaAluno::select('id')->plataforma()
                    ->where('aluno_id', $user_id)
                    ->where('turma_id', $turma_id)
                    ->exists();

                if($verificaSeJaMatriculado == false)
                {
                    TurmaAluno::create([
                        'plataforma_id' => session('plataforma_id'),
                        'turma_id' => $turma_id,
                        'curso_id' => Turma::plataforma()->where('id', $turma_id)->pluck('curso_id')[0] ?? null,
                        'aluno_id' => $user_id,
                    ]);
                }
            }

            if(request('matricular_aluno_trilha_enviar_email') && request('email_contratacao_trilha') != null)
            {
                $user = User::plataforma()->aluno()->where('id', $user_id)->first();
                $trilha = \App\Models\Trilha::plataforma()->where('id', request('matricular_aluno_trilha_id'))->first();

                \Mail::to($user->email)->send(new \App\Mail\EmailContratacaoTrilha($user, $trilha));
            }

            return back()->with('success', 'Aluno matriculado na Trilha com êxito.');

        } else {

            return back()->with('danger', 'O Aluno já está matriculado nesta Trilha.');
        }
    }

    public function matricularAlunoNaTurma($user_id)
    {
        $verificaSeJaMatriculado = TurmaAluno::select('id')->plataforma()
            ->where('aluno_id', $user_id)
            ->where('turma_id', request('matricular_aluno_turma_id'))
            ->exists();

        if($verificaSeJaMatriculado == false)
        {
            $turmaAluno = TurmaAluno::create([
                'plataforma_id' => session('plataforma_id'),
                'curso_id' => request('matricular_aluno_curso_id'),
                'turma_id' => request('matricular_aluno_turma_id'),
                'aluno_id' => $user_id,
            ]);

            if(request('matricular_aluno_turma_enviar_email') && request('email_contratacao_turma') != null)
            {
                $user = User::plataforma()->aluno()->where('id', $user_id)->first();

                \Mail::to($user->email)->send(new \App\Mail\EmailContratacaoTurma($user, $turmaAluno));
            }

            return back()->with('success', 'Aluno matriculado na Turma com êxito.');

        } else {

            return back()->with('danger', 'O Aluno já está matriculado nesta Turma.');
        }
    }

    public function cadastrarDocumento($user_id)
    {
        $documento = \App\Models\Documento::create(request()->all() + ['user_id' => $user_id]);
        
        $this->syncDocumento($documento, 'link', 'arquivo');

        return back()->with('success', 'Documento anexado com êxito.');
    }

    public function cadastrarContrato($user_id)
    {
        $userContrato = \App\Models\UserContrato::create([
            'contrato_id'    => request('contrato_id'),
            'user_id'        => $user_id,
            'referencia'     => request('referencia'),
        ]);

        $campos = array_combine(request('camposIds') ?? array(), request('values') ?? array());

        foreach($campos as $campo_id => $value)
        {
            \App\Models\UserContratoCampo::create([
                'contrato_id'      => request('contrato_id'),
                'user_contrato_id' => $userContrato->id,
                'user_id'          => $user_id,
                'campo_id'         => $campo_id,
                'value'            => $value,
            ]);
        }

        app('App\Http\Controllers\Admin\ContratoController')->gerar($userContrato);

        return back()->with('success','Contrato gerado e adicionado com êxito.')
            ->with('showModal_SendMailContrato','S')
            ->with('user_contrato_referencia', $userContrato->referencia)
            ->with('user_contrato_id', $userContrato->id)
            ->with('user_contrato_pdf', $userContrato->pdf);
    }

    public function enviarContrato($user_id)
    {
        $user = User::plataforma()->where('id', $user_id)->first();

        $userContrato = \App\Models\UserContrato::plataforma()
            ->where('user_id', $user_id)
            ->where('id', request('user_contrato_id'))
            ->first();

        \Mail::to($user->email)->send(new \App\Mail\EmailContrato($user, $userContrato));

        return back()->with('success', 'Email de Contrato enviado com êxito.');
    }

    public function deletarDocumento($user_id)
    {
        $documento = \App\Models\Documento::plataforma()->where('user_id', $user_id)->where('id', request('documento_id'))->first();
        
        $path = str_replace(\App\Models\Util::getLinkStorage(), '', $documento->link);
        Storage::delete($path);

        $aluno = User::select('nome','email')->plataforma()->where('id', $user_id)->first();

        \App\Models\Log::create([
            'tipo' => 'documento',
            'ref'  => 'Documento do Aluno ('.$aluno->nome . ' - ' . $aluno->email .') ('.$documento->nome.') deletado com êxito.',
        ]);

        $documento->delete();

        return back()->with('success', 'Documento deletado com êxito.');
    }

    public function deletarContrato($user_id)
    {
        $userContrato = \App\Models\UserContrato::plataforma()->where('user_id', $user_id)->where('id', request('user_contrato_id'))->first();
        
        $path = str_replace(\App\Models\Util::getLinkStorage(), '', $userContrato->pdf);
        Storage::delete($path);

        $caminhoPDF = str_replace(\Request::root().'/', '', $userContrato->pdf);

        if(file_exists($caminhoPDF))
        {
            unlink($caminhoPDF);
        }

        if($userContrato->pdf_assinado)
        {
            $path = str_replace(\App\Models\Util::getLinkStorage(), '', $userContrato->pdf_assinado);
            Storage::delete($path);

            $caminhoPDF = str_replace(\Request::root().'/', '', $userContrato->pdf_assinado);

            if(file_exists($caminhoPDF))
            {
                unlink($caminhoPDF);
            }
        }

        if($userContrato->foto_assinatura)
        {
            $path = str_replace(\App\Models\Util::getLinkStorage(), '', $userContrato->foto_assinatura);
            Storage::delete($path);

            $caminhoPDF = str_replace(\Request::root().'/', '', $userContrato->foto_assinatura);

            if(file_exists($caminhoPDF))
            {
                unlink($caminhoPDF);
            }
        }

        \App\Models\UserContratoCampo::plataforma()->where('user_id', $user_id)->where('user_contrato_id', $userContrato->id)->delete();

        $aluno = User::select('nome','email')->plataforma()->where('id', $user_id)->first();

        \App\Models\Log::create([
            'tipo' => 'contrato',
            'ref'  => 'Contrato do Aluno ('.$aluno->nome . ' - ' . $aluno->email .') ('.$userContrato->referencia.') deletado com êxito.',
        ]);

        $userContrato->delete();

        return back()->with('success', 'Contrato deletado com êxito.');
    }

    public function syncCategorias($user_id)
    {
        UserCategoria::plataforma()->where('user_id', $user_id)->delete();

        if(request('categoriasSelected') == null)
        {
            return;
        }

        foreach(request('categoriasSelected') as $categoria_id)
        {
            UserCategoria::create([
                'plataforma_id' => session('plataforma_id'),
                'user_id'       => $user_id, 
                'categoria_id'  => $categoria_id,
            ]);
        }
    }

    public function syncDocumento($documento, $tipo, $inputFileName)
    {
        $file = request()->file($inputFileName);
     
        if($file)
        {
            $path = str_replace(\App\Models\Util::getLinkStorage(), '', $documento->$tipo);
            Storage::delete($path);

            $diretorio = 'plataformas/'.\Auth::user()->plataforma_id.'/users/documentos';
            $filename = $documento->id.'.'.$file->getClientOriginalExtension();

            $path = Storage::putFileAs($diretorio, $file, $filename, 'public');
            $linkStorage = \App\Models\Util::getLinkStorage();
            $caminho = $linkStorage . $path;

            $documento->update([$tipo => $caminho]);
        }
    }

    public function setFotoPerfil($user_id)
    {
        $file = request()->file('foto');

        if($file)
        {
            $diretorio = 'plataformas/'.session('plataforma_id').'/users/'.$user_id;
            $filename = 'foto_perfil.'.$file->getClientOriginalExtension();

            $path = Storage::putFileAs($diretorio, $file, $filename, 'public');
            $linkStorage = \App\Models\Util::getLinkStorage();
            $caminho = $linkStorage . $path;

            User::plataforma()->where('id', $user_id)->update(['foto_perfil' => $caminho]);
        }

        return back()->with('success', 'Foto de Perfil atualizada com êxito.');
    }


    public function historicoEscolar($aluno_id)
    {
        $conteudo = \App\Models\PaginaConteudo::select('seo_keywords','seo_title','conteudo')->plataforma()->where('url','/aluno/info/0/historico-escolar')->first();

        $plataforma_id = session('plataforma_id') ?? Auth::user()->plataforma_id;

        $aluno = User::select('id','nome','cpf','data_nascimento')->plataforma()->find($aluno_id);

        $getNomePaginaInterno = 'Histórico Escolar de ' . $aluno->nome;

        $trilhas = \DB::select( \DB::raw("
            SELECT 
                trilhas.id,
                trilhas.nome,
                trilhas_alunos.data_conclusao
                
            FROM trilhas_alunos

                JOIN trilhas ON trilhas_alunos.trilha_id = trilhas.id

                WHERE trilhas.plataforma_id = ".$plataforma_id."
                AND trilhas_alunos.plataforma_id = ".$plataforma_id."
                AND trilhas_alunos.aluno_id = ".$aluno_id."
        "));

        return view('admin.aluno.info.historico-escolar', compact('trilhas','aluno','aluno_id','plataforma_id','conteudo','getNomePaginaInterno') );
    }

}