<?php

namespace App\Http\Livewire\Admin\Produto;

use Livewire\Component;
use App\Models\Produto;
use App\Models\User;
use App\Models\Unidade;
use App\Models\ProdutoCategoria;
use App\Models\Categoria;
use Illuminate\Support\Facades\Http;

class Info extends Component
{
    public $produto;
    public $unidade;
    public $cadastrante;
    public $password_to_delete;

    public function mount($produto_id)
    {
        $this->produto = Produto::plataforma()->find($produto_id);
    
        $this->unidade = Unidade::select('id','nome')->plataforma()->where('id', $this->produto->unidade_id)->first();

        $this->cadastrante = User::select('id','nome')->plataforma()->where('id', $this->produto->cadastrante_id)->first();

        $categoriasIds = ProdutoCategoria::plataforma()->where('produto_id', $this->produto->id)->get()->pluck('categoria_id')->toArray();
        $this->categorias = Categoria::select('id','nome')->plataforma()->ativo()->whereIn('id', $categoriasIds)->orderBy('nome')->get();
    }

    public function render()
    {
        return view('livewire.admin.produto.info');
    }

    public function delete()
    {
        if(\Auth::attempt(['id' => \Auth::id(), 'password' => $this->password_to_delete, 'status' => 0]))
        {
            Produto::plataforma()->where('id', $this->produto->id)->delete();

            session()->flash('success', 'Produto deletado com êxito.');
            
            return redirect(route('admin.produto.lista'));

        } else {

            session()->flash('danger-livewire', 'Senha incorreta.');

            $this->dispatchBrowserEvent('toast');
        }
    }
}
