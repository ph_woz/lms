<?php

namespace App\Http\Livewire\Admin\Plano;

use Livewire\Component;
use App\Models\Plano;
use App\Models\PlanoCategoria;
use App\Models\Categoria;
use App\Models\PlanoBeneficio;
use App\Models\User;
use App\Models\Unidade;
use Illuminate\Support\Facades\Http;

class Info extends Component
{
    public $plano;
    public $cadastrante;
    public $unidade;
    public $beneficios;
    public $categorias;
    public $password_to_delete;

    public function mount($plano_id)
    {
        $this->plano = Plano::plataforma()->find($plano_id);
        
        $this->unidade = Unidade::select('id','nome')->plataforma()->where('id', $this->plano->unidade_id)->first();

        $this->cadastrante = User::select('id','nome')->plataforma()->where('id', $this->plano->cadastrante_id)->first();

        $this->beneficios = PlanoBeneficio::select('descricao')->plataforma()->where('plano_id', $plano_id)->get()->pluck('descricao')->toArray();
    
        $categoriasIds = PlanoCategoria::plataforma()->where('plano_id', $this->plano->id)->get()->pluck('categoria_id')->toArray();
        $this->categorias = Categoria::select('id','nome')->plataforma()->ativo()->whereIn('id', $categoriasIds)->orderBy('nome')->get();
    }

    public function render()
    {
        return view('livewire.admin.plano.info');
    }

    public function delete()
    {
        if(\Auth::attempt(['id' => \Auth::id(), 'password' => $this->password_to_delete, 'status' => 0]))
        {
            Plano::plataforma()->where('id', $this->plano->id)->delete();

            session()->flash('success', 'Plano deletado com êxito.');

            return redirect(route('admin.plano.lista'));

        } else {

            session()->flash('danger-livewire', 'Senha incorreta.');

            $this->dispatchBrowserEvent('toast');
        }
    }
}
