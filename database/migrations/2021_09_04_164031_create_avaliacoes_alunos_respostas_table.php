<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAvaliacoesAlunosRespostasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('avaliacoes_alunos_respostas', function (Blueprint $table) {
            $table->id();
            $table->integer('plataforma_id');
            $table->integer('avaliacao_id');
            $table->integer('turma_id');
            $table->integer('aluno_id');
            $table->integer('questao_id')->nullable();
            $table->integer('alternativa_id')->nullable();
            $table->string('acertou')->nullable();
            $table->text('resposta_dissertativa')->nullable();
            $table->text('resposta_dissertativa_correcao')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('avaliacoes_alunos_respostas');
    }
}
