<?php

namespace App\Http\Livewire\Admin\Curso;

use Livewire\WithPagination;
use Livewire\Component;
use App\Models\Curso;
use App\Models\Categoria;
use App\Models\CursoCategoria;

class Lista extends Component
{
    use WithPagination;

    protected $paginationTheme = 'bootstrap';

    public $search;
    public $status = 0;
    public $publicar;
    public $ordem;
    public $categoriasSelected;

    protected $queryString = ['search','status','publicar','ordem','categoriasSelected'];

    public function render()
    {
        $cursos = Curso::select('id','referencia','nivel')->plataforma()->where('status', $this->status);

        if($this->publicar)
        {
            $cursos = $cursos->where('publicar', $this->publicar);
        }

        if($this->categoriasSelected != null)
        {
            $cursosIds = CursoCategoria::plataforma()->whereIn('categoria_id', $this->categoriasSelected ?? array())->get()->pluck('curso_id')->toArray();
            $cursos = $cursos->whereIn('id', $cursosIds);
        }

        if($this->search)
        {
            $cursos->where(function ($query)
            {
                $query
                    ->orWhere('nome', 'like', '%'.$this->search.'%')
                    ->orWhere('referencia', 'like', '%'.$this->search.'%');
            });
        }


        $countCursos = $cursos->count();


        if($this->ordem == 'desc' || $this->ordem == 'asc')
        {
            $cursos = $cursos->orderBy('id', $this->ordem);
        
        } else {

            $cursos = $cursos->orderBy('referencia');
        }
        

        $cursos = $cursos->paginate(10);


        $categorias = Categoria::select('id','nome')->plataforma()->ativo()->tipo('curso')->orderBy('nome')->get();

        return view('livewire.admin.curso.lista', [
            'cursos'      => $cursos,
            'countCursos' => $countCursos,
            'categorias'  => $categorias,
        ]);
    }
}
