<?php

namespace App\Http\Livewire\Admin\Aluno;

use Livewire\Component;
use App\Models\User;
use App\Models\UserEndereco;
use App\Models\UserCategoria;
use App\Models\Categoria;
use App\Models\Unidade;
use App\Models\FluxoCaixa;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Http;

class Editar extends Component
{
    public $user;
    public $endereco;

    public $nome;
    public $email;
    public $new_password;
    public $status = 0;
    public $unidade_id;
    public $restringir_unidade = 'N';
    public $cpf;
    public $rg;
    public $naturalidade;
    public $matricula;
    public $data_nascimento;
    public $celular;
    public $telefone;
    public $genero;
    public $profissao;
    public $obs;
    public $descricao;

    public $endereco_cep;
    public $endereco_rua;
    public $endereco_numero;
    public $endereco_bairro;
    public $endereco_cidade;
    public $endereco_estado;
    public $endereco_complemento;
    public $endereco_ponto_referencia;

    public $categoriasSelected = [];


    public function mount($user_id)
    {
        $this->user = User::plataforma()->find($user_id);
        $this->endereco = UserEndereco::plataforma()->where('user_id', $this->user->id)->first();
        $this->categoriasSelected = UserCategoria::plataforma()->where('user_id', $this->user->id)->get()->pluck('categoria_id')->toArray();

        $this->nome                      = $this->user->nome;
        $this->email                     = $this->user->email;
        $this->status                    = $this->user->status;
        $this->unidade_id                = $this->user->unidade_id;
        $this->restringir_unidade        = $this->user->restringir_unidade;
        $this->cpf                       = $this->user->cpf;
        $this->rg                        = $this->user->rg;
        $this->naturalidade              = $this->user->naturalidade;
        $this->matricula                 = $this->user->matricula;
        $this->data_nascimento           = $this->user->data_nascimento;
        $this->celular                   = $this->user->celular;
        $this->telefone                  = $this->user->telefone;
        $this->genero                    = $this->user->genero;
        $this->profissao                 = $this->user->profissao;
        $this->obs                       = $this->user->obs;
        $this->descricao                 = $this->user->descricao;

        $this->endereco_cep              = $this->endereco->cep              ?? null;
        $this->endereco_rua              = $this->endereco->rua              ?? null;
        $this->endereco_numero           = $this->endereco->numero           ?? null;
        $this->endereco_bairro           = $this->endereco->bairro           ?? null;
        $this->endereco_cidade           = $this->endereco->cidade           ?? null;
        $this->endereco_estado           = $this->endereco->estado           ?? null;
        $this->endereco_complemento      = $this->endereco->complemento      ?? null;
        $this->endereco_ponto_referencia = $this->endereco->ponto_referencia ?? null;

    }

    public function editar()
    {
        $checkEmailExists = User::plataforma()->where('email', $this->email)->where('id','<>', $this->user->id)->exists();
        
        if($checkEmailExists == true)
        {
            session()->flash('danger-livewire', 'Já existe uma pessoa cadastrada com este Email.');
            $this->dispatchBrowserEvent('toast');
            return;
        }

        if($this->cpf)
        {            
            $checkCPFExists = User::plataforma()->where('cpf', $this->cpf)->where('id','<>', $this->user->id)->exists();

            if($checkCPFExists == true)
            {
                session()->flash('danger-livewire', 'Já existe uma pessoa cadastrada com este CPF.');
                $this->dispatchBrowserEvent('toast');
                return;
            }
        }

        if($this->rg)
        {
            $checkRGExists = User::plataforma()->where('rg', $this->rg)->where('id','<>', $this->user->id)->exists();

            if($checkRGExists == true)
            {
                session()->flash('danger-livewire', 'Já existe uma pessoa cadastrada com este RG.');
                $this->dispatchBrowserEvent('toast');
                return;
            }
        }

        if($this->matricula)
        {
            $checkMatriculaExists = User::plataforma()->where('matricula', $this->matricula)->where('id','<>', $this->user->id)->exists();

            if($checkMatriculaExists == true)
            {
                session()->flash('danger-livewire', 'Já existe uma pessoa cadastrada com esta Matrícula.');
                $this->dispatchBrowserEvent('toast');
                return;
            }
        }

        $this->user->update([
            'nome'               => $this->nome,
            'email'              => $this->email,
            'status'             => $this->status,
            'unidade_id'         => $this->unidade_id,
            'restringir_unidade' => $this->restringir_unidade,
            'cpf'                => $this->cpf,
            'rg'                 => $this->rg,
            'naturalidade'       => $this->naturalidade,
            'matricula'          => $this->matricula,
            'data_nascimento'    => $this->data_nascimento,
            'celular'            => $this->celular,
            'telefone'           => $this->telefone,
            'genero'             => $this->genero,
            'profissao'          => $this->profissao,
            'obs'                => $this->obs,
            'descricao'          => $this->descricao,
        ]);

        if($this->new_password)
        {
            $this->user->update(['password' => Hash::make($this->new_password)]);
        }

        
        $endereco = UserEndereco::plataforma()->where('user_id', $this->user->id)->first();

        if(is_null($endereco))
        {
            if($this->endereco_cep != null || $this->endereco_rua != null || $this->endereco_numero != null || $this->endereco_bairro != null || $this->endereco_cidade != null || $this->endereco_estado != null || $this->endereco_complemento != null || $this->endereco_ponto_referencia != null)
                UserEndereco::create([
                    'plataforma_id'    => $this->user->plataforma_id,
                    'user_id'          => $this->user->id,
                    'cep'              => $this->endereco_cep,
                    'rua'              => $this->endereco_rua,
                    'numero'           => $this->endereco_numero,
                    'bairro'           => $this->endereco_bairro,
                    'cidade'           => $this->endereco_cidade,
                    'estado'           => $this->endereco_estado,
                    'complemento'      => $this->endereco_complemento,
                    'ponto_referencia' => $this->endereco_ponto_referencia,
                ]);

        } else {

            $endereco->update([
                'user_id'          => $this->user->id,
                'cep'              => $this->endereco_cep,
                'rua'              => $this->endereco_rua,
                'numero'           => $this->endereco_numero,
                'bairro'           => $this->endereco_bairro,
                'cidade'           => $this->endereco_cidade,
                'estado'           => $this->endereco_estado,
                'complemento'      => $this->endereco_complemento,
                'ponto_referencia' => $this->endereco_ponto_referencia,
            ]);
        }


        if(FluxoCaixa::checkActive())
        {
            $cliente_nome = $this->nome . ' / ' . $this->email;

            if(isset($this->cpf))
            {
                $cliente_nome = $cliente_nome . ' / ' . $this->cpf;
            }
            if(isset($this->celular))
            {
                $cliente_nome = $cliente_nome . ' / ' . $this->celular;
            }

            FluxoCaixa::plataforma()->where('cliente_id', $this->user->id)->update(['cliente_nome' => $cliente_nome]);
        }

        $this->syncCategorias($this->user->id);

        session()->flash('success-livewire', 'Aluno atualizado com êxito.');

        $this->dispatchBrowserEvent('toast');
    }

    public function syncCategorias($user_id)
    {
        UserCategoria::plataforma()->where('user_id', $user_id)->delete();

        if($this->categoriasSelected == null)
            return;

        foreach($this->categoriasSelected as $categoria_id)
        {
            $countDuplicate = UserCategoria::plataforma()->where('user_id', $user_id)->where('categoria_id', $categoria_id)->count();
            
            if($countDuplicate <= 0)
                UserCategoria::create([
                    'plataforma_id' => session('plataforma_id'),
                    'user_id'       => $user_id, 
                    'categoria_id'  => $categoria_id,
                ]);
        }
    }

    public function setEndereco()
    {
        // Retorna endereço do ViaCEP.
        $response = Http::get("http://viacep.com.br/ws/{$this->endereco_cep}/json/");
        $viacep = $response->json();
        $this->endereco_rua    = $viacep['logradouro'] ?? null;
        $this->endereco_bairro = $viacep['bairro'] ?? null;
        $this->endereco_cidade = $viacep['localidade'] ?? null;
        $this->endereco_estado = $viacep['uf'] ?? null;
    }

    public function render()
    {
        $categorias = Categoria::plataforma()->ativo()->tipo('aluno')->orderBy('nome')->get();
        $unidades   = Unidade::plataforma()->ativo()->unidade()->orderBy('nome')->get();

        return view('livewire.admin.aluno.editar', ['categorias' => $categorias, 'unidades' => $unidades] );
    }
}
