<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SecaoDescritiva extends Model
{
    use HasFactory;

    protected $table = 'secoes_descritivas';

    public $timestamps = false;

    public $fillable = [
    	'plataforma_id',
    	'registro_id',
    	'tipo',
    	'titulo',
        'expandido',
    	'descricao',
    	'ordem',
    ];

    public static  function boot()
    {
        parent::boot(); 
        static::creating(function ($model)
        {
            $model->plataforma_id = session('plataforma_id') ?? \Auth()->user()->plataforma_id ?? Plataforma::dominio()->pluck('id')[0] ?? null;
        });
    }

    public function getPlataforma()
    {
        return $this->belongsTo(Plataforma::class, 'plataforma_id');
    }

    public function scopePlataforma($query)
    {
        return $query->where('plataforma_id', session('plataforma_id') ?? Plataforma::dominio()->pluck('id')[0] ?? null);
    }

    public function scopeTrilha($query)
    {
        return $query->where('tipo', 'T');
    }

    public function scopeCurso($query)
    {
        return $query->where('tipo', 'C');
    }

    public function scopeFies($query)
    {
        return $query->where('tipo', 'FIES');
    }

    public function scopeProuni($query)
    {
        return $query->where('tipo', 'ProUni');
    }

}
