<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Responsavel;


class ResponsavelController extends Controller
{
    public function lista()
    {
        $getNomePaginaInterno = 'Responsáveis';

        return view('admin.responsavel.lista', compact('getNomePaginaInterno') );
    }
    
    public function info($responsavel_id)
    {
        $getNomePaginaInterno = 'Responsável';

        return view('admin.responsavel.info', compact('responsavel_id','getNomePaginaInterno') );
    }

    public function add()
    {
        $aluno_id = request('aluno_id');
        
        if(is_null($aluno_id))
            return redirect()->route('admin.aluno.lista');

        $getNomePaginaInterno = 'Adicionar Responsável';

    	return view('admin.responsavel.add', compact('aluno_id','getNomePaginaInterno') );
    }

    public function editar($responsavel_id)
    {
        $getNomePaginaInterno = 'Editar Responsável';

        return view('admin.responsavel.editar', compact('responsavel_id','getNomePaginaInterno') );
    }
}