<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFormulariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('formularios', function (Blueprint $table) {
            $table->id();
            $table->integer('plataforma_id');
            $table->integer('cadastrante_id')->nullable();
            $table->integer('unidade_id')->nullable();
            $table->string('referencia');
            $table->string('slug');
            $table->integer('status')->default(0);
            $table->text('texto')->nullable();
            $table->text('texto_retorno')->nullable();
            $table->string('texto_botao_retorno')->nullable();
            $table->text('link_retorno')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('formularios');
    }
}
