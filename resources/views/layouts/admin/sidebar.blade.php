<ul class="sidebar-nav">

    <li class="sidebar-item" id="url-dashboard">
        <a class="side-color sidebar-link" href="{{ route('admin.dashboard.info') }}">
            <i class="side-color fa fa-home"></i> <span class="align-middle"></span>
        </a>
    </li>	
    <li class="sidebar-item" id="url-aluno">
        <a class="side-color sidebar-link" href="{{ route('admin.aluno.lista') }}">
            <i class="side-color fa fa-user-graduate"></i> <span class="align-middle"></span>
        </a>
    </li>
    <li class="sidebar-item" id="url-controle-financeiro">
        <a class="side-color sidebar-link" href="{{ route('admin.controle-financeiro.lista.fluxo-caixa') }}">
            <i class="side-color fas fa-hand-holding-usd"></i> <span class="align-middle"></span>
        </a>
    </li>
    <li class="sidebar-item" id="url-colaborador">
        <a class="side-color sidebar-link" href="{{ route('admin.colaborador.lista') }}">
            <i class="side-color fa fa-users"></i> <span class="align-middle"></span>
        </a>
    </li>
    <li class="sidebar-item" id="url-turma">
        <a class="side-color sidebar-link" href="{{ route('admin.turma.lista') }}">
            <i class="side-color fa fa-table"></i> <span class="align-middle"></span>
        </a>
    </li>
    <li class="sidebar-item" id="url-curso">
        <a class="side-color sidebar-link" href="{{ route('admin.curso.lista') }}">
            <i class="side-color fa fa-book-open"></i> <span class="align-middle"></span>
        </a>
    </li>
    <li class="sidebar-item" id="url-trilha">
        <a class="side-color sidebar-link" href="{{ route('admin.trilha.lista') }}">
            <i class="side-color fas fa-chart-line"></i> <span class="align-middle"></span>
        </a>
    </li>
    <li class="sidebar-item" id="url-plano">
        <a class="side-color sidebar-link" href="{{ route('admin.plano.lista') }}">
            <i class="side-color w-15px fa fa-book-open"></i><span class="align-middle"></span>
        </a>
    </li>
    <li class="sidebar-item" id="url-servico">
        <a class="side-color sidebar-link" href="{{ route('admin.servico.lista') }}">
            <i class="side-color w-15px fas fa-hand-holding-heart"></i><span class="align-middle"></span>
        </a>
    </li>
    <li class="sidebar-item" id="url-produto">
        <a class="side-color sidebar-link" href="{{ route('admin.produto.lista') }}">
            <i class="side-color w-15px fas fa-store-alt"></i><span class="align-middle"></span>
        </a>
    </li>
    <li class="sidebar-item" id="url-avaliacao">
        <a class="side-color sidebar-link" href="{{ route('admin.avaliacao.lista') }}">
            <i class="side-color fa fa-edit"></i> <span class="align-middle"></span>
        </a>
    </li>
    <li class="sidebar-item" id="url-questao">
        <a class="side-color sidebar-link" href="{{ route('admin.questao.lista') }}">
            <i class="side-color fa fa-question-circle"></i> <span class="align-middle"></span>
        </a>
    </li>
    <li class="sidebar-item" id="url-comunicado">
        <a class="side-color sidebar-link" href="{{ route('admin.comunicado.lista') }}">
            <i class="side-color fa fa-bullhorn"></i> <span class="align-middle"></span>
        </a>
    </li>
    <li class="sidebar-item" id="url-documento">
        <a class="side-color sidebar-link" href="{{ route('admin.documento.lista') }}">
            <i class="side-color fas fa-folder-minus"></i> <span class="align-middle"></span>
        </a>
    </li>
    <li class="sidebar-item" id="url-certificado">
        <a class="side-color sidebar-link" href="{{ route('admin.certificado.lista') }}">
            <i class="side-color fa fa-certificate"></i> <span class="align-middle"></span>
        </a>
    </li>
    <li class="sidebar-item" id="url-contrato">
        <a class="side-color sidebar-link" href="{{ route('admin.contrato.lista') }}">
            <i class="side-color fas fa-file-pdf"></i> <span class="align-middle"></span>
        </a>
    </li>
    <li class="sidebar-item" id="url-contato">
        <a class="side-color sidebar-link" href="{{ route('admin.contato.lista', ['status' => 0]) }}">
            <i class="side-color fa fa-envelope"></i> <span class="align-middle"></span>
        </a>
    </li>
    <li class="sidebar-item" id="url-formulario">
        <a class="side-color sidebar-link" href="{{ route('admin.formulario.lista') }}">
            <i class="side-color fa fa-edit"></i> <span class="align-middle"></span>
        </a>
    </li>
    <li class="sidebar-item" id="url-unidade">
        <a class="side-color sidebar-link" href="{{ route('admin.unidade.lista') }}">
            <i class="side-color fas fa-map-marker-alt"></i> <span class="align-middle"></span>
        </a>
    </li>
    <li class="sidebar-item" id="url-pagina">
        <a class="side-color sidebar-link" href="{{ route('admin.pagina.lista') }}">
            <i class="side-color fa fa-pager"></i> <span class="align-middle"></span>
        </a>
    </li>   
    <li class="sidebar-item" id="url-relatorio">
        <a class="side-color sidebar-link" href="{{ route('admin.relatorio.lista') }}">
            <i class="side-color far fa-chart-bar"></i> <span class="align-middle"></span>
        </a>
    </li>
    <li class="sidebar-item" id="url-plataforma">
        <a class="side-color sidebar-link" href="{{ route('admin.plataforma.editar.configuracoes') }}">
            <i class="side-color fa fa-desktop"></i> <span class="align-middle"></span>
        </a>
    </li>

</ul>
