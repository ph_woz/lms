@extends('layouts.aluno')
@section('seo_title', 'Avaliações')
@section('content')
<section class="container-fluid px-xl-5 z-3 mt-5 position-relative">

@foreach($categorias as $categoria)
	
	@php

		$avaliacoesIds = \App\Models\AvaliacaoCategoria::select('avaliacao_id')->plataforma()->where('categoria_id', $categoria->id)->get()->pluck('avaliacao_id')->toArray();

        $turmasIds  = \App\Models\TurmaAluno::plataforma()->ativo()->where('aluno_id', \Auth::id())->select('turma_id')->get()->pluck('turma_id')->toArray();
            
        $avaliacoes = \DB::table('avaliacoes');
        $avaliacoes = $avaliacoes
            ->leftJoin('avaliacoes_alunos', function($join) {
                $join->on('avaliacoes.id', '=', 'avaliacoes_alunos.avaliacao_id')
                    ->where('avaliacoes_alunos.plataforma_id', session('plataforma_id') ?? \Auth::user()->plataforma_id)
                    ->where('avaliacoes_alunos.aluno_id', \Auth::id());
            });

        $avaliacoes = $avaliacoes
            ->where('avaliacoes.plataforma_id', session('plataforma_id') ?? \Auth::user()->plataforma_id)
            ->where('avaliacoes.status', 0)
            ->whereIn('avaliacoes.turma_id', $turmasIds)
            ->whereIn('avaliacoes.id', $avaliacoesIds)
            ->select(
                'avaliacoes.id',
                'avaliacoes.turma_id',
                'avaliacoes.referencia',
                'avaliacoes.instrucoes',
                'avaliacoes.n_tentativas',
                'avaliacoes.condicao_fazer_avaliacao',
                'avaliacoes_alunos.n_tentativa as n_tentativa_atual',
                'avaliacoes_alunos.status',
                'avaliacoes_alunos.nota',
                'avaliacoes.liberar_gabarito'
            )
            ->orderBy('avaliacoes.referencia')
            ->get();

	@endphp

	<section class="mb-4-5">
		
		<a href="#boxInfoPrincipal" data-bs-toggle="collapse" aria-expanded="false" class="hover-d-none">
			<div class="section-title bg-dark rounded-bottom-0 py-3">
				{{ $categoria->nome }}
			</div>
		</a>
		
		<div class="pt-4-5 px-4 pb-3 rounded-bottom collapse show bg-14" id="boxInfoPrincipal">

			@foreach($avaliacoes as $avaliacao)
				
				<div class="mb-4 px-4-5 py-4 rounded-10 box-shadow bg-dark">

					<h5 class="mb-3 title fs-17">
						{{ $avaliacao->referencia }}
					</h5>

					<hr/>

					<div class="row align-items-center">

						<div class="col-lg-9">
					
							@if($avaliacao->n_tentativas || $avaliacao->instrucoes)

								@if($avaliacao->n_tentativas)
									<p class="mb-2 color-light-soft">N° de Tentativas: {{ $avaliacao->n_tentativa_atual }} de {{ $avaliacao->n_tentativas }}</p>
								@endif

								@if($avaliacao->instrucoes)
								<p class="mb-0 color-light-soft">
									<span class="d-block">Instruções:</span>
									<span class="color-b7">
										{{ $avaliacao->instrucoes ?? 'Não definido' }}
									</span>
								</p>
								@endif

								<hr class="d-md-none" />

							@endif

						</div>

						<div class="col-lg-3">

							<div class="text-lg-center ms-lg-4 my-2"> 

								@if($avaliacao->status == null)
									@if($avaliacao->condicao_fazer_avaliacao == null)
										<a href="{{ route('aluno.avaliacao', ['id' => $avaliacao->id]) }}" class="btn-suporte-enviar py-2-5 px-5">
											Iniciar
										</a>
									@else
										@if(\App\Models\TurmaAluno::plataforma()->where('aluno_id', \Auth::id())->where('turma_id', $avaliacao->turma_id)->whereNotNull('data_conclusao_conteudo')->exists())
											<a href="{{ route('aluno.avaliacao', ['id' => $avaliacao->id]) }}" class="btn-suporte-enviar py-2-5 px-5">
												Iniciar
											</a>
										@else
											<div class="mensagemCondicaoAvaliacao color-light">
												É necessário que você conclua todas as aulas do curso para que esta avaliação seja liberada
											</div>
										@endif
									@endif
								@endif

								@if($avaliacao->status == 'F')
									<p class="mb-0 color-light">
										Finalizada. <br> Sua nota foi: <strong>{{ number_format($avaliacao->nota, 2, ',', '.') }}</strong>.
									</p>
									@if($avaliacao->liberar_gabarito == 'S')
										<div class="mt-4">
											@if($avaliacao->n_tentativa_atual >= $avaliacao->n_tentativas)
												<a href="{{ route('aluno.avaliacao.gabarito', ['id' => $avaliacao->id]) }}" class="btn-suporte-enviar py-2-5 px-4">
													Ver Gabarito
												</a>
											@else
												@php
													$nota_minima_aprovacao = \App\Models\Turma::plataforma()->ativo()->where('id', $avaliacao->turma_id)->pluck('nota_minima_aprovacao')[0] ?? 0;
												@endphp
												@if($avaliacao->nota < $nota_minima_aprovacao)
													<a href="{{ route('aluno.avaliacao.reiniciar', ['id' => $avaliacao->id]) }}" class="btn-suporte-enviar py-2-5 px-4">
														Tentar Novamente
													</a>
												@else
													<a href="{{ route('aluno.avaliacao.gabarito', ['id' => $avaliacao->id]) }}" class="btn-suporte-enviar py-2-5 px-4">
														Ver Gabarito
													</a>
												@endif
											@endif
										</div>
									@endif
								@endif

							</div>

						</div>

					</div>

				</div>

			@endforeach

		</div>

	</section>

@endforeach

</section>
@endsection