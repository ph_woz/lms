<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTurmasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('turmas', function (Blueprint $table) {
            $table->id();
            $table->integer('plataforma_id')->nullable();
            $table->integer('curso_id')->nullable();
            $table->integer('formulario_id')->nullable();
            $table->integer('cadastrante_id')->nullable();
            $table->integer('unidade_id')->nullable();
            $table->string('nome')->nullable();
            $table->integer('status')->default(0);
            $table->string('tipo_inscricao')->nullable();
            $table->string('publicar', 1)->default('N');
            $table->string('modalidade')->nullable();
            $table->date('data_inicio')->nullable();
            $table->date('data_termino')->nullable();
            $table->string('frequencia')->nullable();
            $table->string('horario')->nullable();
            $table->string('duracao')->nullable();
            $table->string('tipo_valor', 1)->nullable();
            $table->string('tipo_mensalidade', 1)->nullable();
            $table->string('valor_parcelado')->nullable();
            $table->string('valor_a_vista')->nullable();
            $table->string('valor_promocional')->nullable();
            $table->string('valor_mensalidade')->nullable();
            $table->string('desconto_agilpass')->nullable();
            $table->string('nota_minima_aprovacao')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('turmas');
    }
}
