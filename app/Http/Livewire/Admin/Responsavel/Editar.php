<?php

namespace App\Http\Livewire\Admin\Responsavel;

use Livewire\Component;
use App\Models\Responsavel;
use App\Models\ResponsavelAluno;
use App\Models\User;
use Illuminate\Support\Facades\Http;

class Editar extends Component
{
    public $responsavel;
    public $nome;
    public $email;
    public $password;
    public $status = 0;
    public $cpf;
    public $rg;
    public $naturalidade;
    public $data_nascimento;
    public $celular;
    public $telefone;
    public $genero;
    public $profissao;
    public $obs;

    public $cep;
    public $rua;
    public $numero;
    public $bairro;
    public $cidade;
    public $estado;
    public $complemento;
    public $ponto_referencia;
    

    public function mount($responsavel_id)
    {   
        $this->responsavel      = Responsavel::plataforma()->where('id', $responsavel_id)->first();
        $this->nome             = $this->responsavel->nome;
        $this->email            = $this->responsavel->email;
        $this->status           = $this->responsavel->status;
        $this->cpf              = $this->responsavel->cpf;
        $this->rg               = $this->responsavel->rg;
        $this->naturalidade     = $this->responsavel->naturalidade;
        $this->data_nascimento  = $this->responsavel->data_nascimento;
        $this->celular          = $this->responsavel->celular;
        $this->telefone         = $this->responsavel->telefone;
        $this->genero           = $this->responsavel->genero;
        $this->profissao        = $this->responsavel->profissao;
        $this->obs              = $this->responsavel->obs;
        $this->cep              = $this->responsavel->cep              ?? null;
        $this->rua              = $this->responsavel->rua              ?? null;
        $this->numero           = $this->responsavel->numero           ?? null;
        $this->bairro           = $this->responsavel->bairro           ?? null;
        $this->cidade           = $this->responsavel->cidade           ?? null;
        $this->estado           = $this->responsavel->estado           ?? null;
        $this->complemento      = $this->responsavel->complemento      ?? null;
        $this->ponto_referencia = $this->responsavel->ponto_referencia ?? null;
    }

    public function editar()
    {
        if($this->email)
        {
            $checkEmailExists = Responsavel::plataforma()->where('id','<>', $this->responsavel->id)->where('email', $this->email)->exists();

            if($checkEmailExists == true)
            {
                session()->flash('danger-livewire', 'Já existe uma pessoa cadastrada com este Email.');
                $this->dispatchBrowserEvent('toast');
                return;
            }
        }

        if($this->cpf)
        {
            $checkCPFExists = Responsavel::plataforma()->where('id','<>', $this->responsavel->id)->where('cpf', $this->cpf)->exists();

            if($checkCPFExists == true)
            {
                session()->flash('danger-livewire', 'Já existe uma pessoa cadastrada com este CPF.');
                $this->dispatchBrowserEvent('toast');
                return;
            }
        }

        if($this->rg)
        {
            $checkRGExists = Responsavel::plataforma()->where('id','<>', $this->responsavel->id)->where('rg', $this->rg)->exists();

            if($checkRGExists == true)
            {
                session()->flash('danger-livewire', 'Já existe uma pessoa cadastrada com este RG.');
                $this->dispatchBrowserEvent('toast');
                return;
            }
        }

        $this->responsavel->update([
            'nome'               => $this->nome,
            'email'              => $this->email,
            'status'             => $this->status,
            'cpf'                => $this->cpf,
            'rg'                 => $this->rg,
            'naturalidade'       => $this->naturalidade,
            'data_nascimento'    => $this->data_nascimento,
            'celular'            => $this->celular,
            'telefone'           => $this->telefone,
            'genero'             => $this->genero,
            'profissao'          => $this->profissao,
            'obs'                => $this->obs,
            'cep'                => $this->cep,
            'rua'                => $this->rua,
            'numero'             => $this->numero,
            'bairro'             => $this->bairro,
            'cidade'             => $this->cidade,
            'estado'             => $this->estado,
            'complemento'        => $this->complemento,
            'ponto_referencia'   => $this->ponto_referencia,
        ]);

        session()->flash('success-livewire', 'Responsável atualizado com êxito.');

        $this->dispatchBrowserEvent('toast');
    }

    public function setEndereco()
    {
        // Retorna endereço do ViaCEP.
        $response = Http::get("http://viacep.com.br/ws/{$this->cep}/json/");
        $viacep = $response->json();
        $this->rua    = $viacep['logradouro'] ?? null;
        $this->bairro = $viacep['bairro'] ?? null;
        $this->cidade = $viacep['localidade'] ?? null;
        $this->estado = $viacep['uf'] ?? null;
    }

    public function render()
    {
        return view('livewire.admin.responsavel.editar');
    }
}
