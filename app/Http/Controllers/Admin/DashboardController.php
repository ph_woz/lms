<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Models\FluxoCaixa;

class DashboardController extends Controller
{
    public function dashboard()
    {
        if(\Auth::user()->tipo == 'aluno')
        {
            return redirect()->route('aluno.dashboard');
        }

        // Verifica se há algum pagamento em atraso
        $data_vencimento = FluxoCaixa::select('data_vencimento')->billing()
            ->where('plataforma_id', 0)
            ->where('referencia', \Request::getHost())
            ->where('status', 0)
            ->whereDate('data_vencimento', '<', date('Y-m-d'))
            ->orderBy('id','desc')
            ->pluck('data_vencimento')[0] ?? null;

        $diaBloqueio = null;
        
        if($data_vencimento)
        {
            $dataBloqueio = Carbon::createFromFormat('Y-m-d', $data_vencimento)->addDays(4);

            $diaBloqueio = $dataBloqueio->isoFormat('LL'); 
            $diaBloqueio = ucwords($diaBloqueio);
            $diaBloqueio = str_replace('De', 'de', $diaBloqueio);
        }

        return view('admin.dashboard.info', compact('diaBloqueio') );
    }

}