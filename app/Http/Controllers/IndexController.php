<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;
use App\Models\Questao;
use App\Models\QuestaoAlternativa;
use App\Models\ContatoDocumento;
use App\Models\FormularioIntegracao;

class IndexController extends Controller
{
    public function limpaCache()
    {
        \Artisan::call('cache:clear');

        return 'runned';
    }

    public function index()
    {
        $pagina = \App\Models\PaginaConteudo::plataforma()->where('url','/')->first();

        if(isset($pagina))
        {
            if($pagina->status == 1)
            {
                return redirect('/login');
            }
        
            if($pagina->view_propria)
            {
                return app('App\Http\Controllers\SiteProprio'.$pagina->view_propria)->index();
            }
        }

        $banners = \App\Models\Banner::plataforma()->ativo()->whereNotnull('link')->orderBy('ordem')->get();
        
        $categorias = \App\Models\Categoria::plataforma()->tipo('curso')->orderBy('nome')->get();

        if(request('sentContato'))
        {
            if(request('check'))
                return back()->with('danger', 'SPAM Detected!');

            $plataforma_id = \App\Models\Plataforma::plataforma()->pluck('id')[0] ?? null;

            \App\Models\Contato::create(request()->all() + [
                'plataforma_id' => $plataforma_id,
                'tipo' => 'L',
            ]);
            
            return back()->with('success', 'Inscrição enviada com êxito.');
        }

        return view('site.index', compact('pagina','banners','categorias') );
    }

    public function consultarDiploma()
    {
        $pagina = \App\Models\PaginaConteudo::plataforma()->where('url','/consultar-diploma')->first();

        if(isset($pagina) && $pagina->view_propria != null)
        {
            return app('App\Http\Controllers\SiteProprio'.$pagina->view_propria)->consultarDiploma();
        }

        return view('site.consultar-diploma', compact('pagina') );
    }

    public function sobreNos()
    {
        $pagina = \App\Models\PaginaConteudo::plataforma()->where('url','/sobre-nos')->first();

        if(isset($pagina) && $pagina->view_propria != null)
        {
            return app('App\Http\Controllers\SiteProprio'.$pagina->view_propria)->sobreNos();
        }

    	return view('site.sobre-nos', compact('pagina') );
    }

    public function agradecimentos()
    {
        $pagina = \App\Models\PaginaConteudo::plataforma()->where('url','/agradecimentos')->first();

        if(isset($pagina) && $pagina->view_propria != null)
        {
            return app('App\Http\Controllers\SiteProprio'.$pagina->view_propria)->agradecimentos();
        }

        return view('site.agradecimentos', compact('pagina') );
    }

    public function biblioteca()
    {
        $pagina = \App\Models\PaginaConteudo::plataforma()->where('url','/biblioteca')->first();

        if(isset($pagina) && $pagina->view_propria != null)
        {
            return app('App\Http\Controllers\SiteProprio'.$pagina->view_propria)->biblioteca();
        }

        return view('site.biblioteca', compact('pagina') );
    }

    public function editais()
    {
        $pagina = \App\Models\PaginaConteudo::plataforma()->ativo()->where('url','/editais')->first();

        if(isset($pagina) && $pagina->view_propria != null)
        {
            return app('App\Http\Controllers\SiteProprio'.$pagina->view_propria)->editais();
        }

        return view('site.editais', compact('pagina') );
    }

    public function dou()
    {
        $pagina = \App\Models\PaginaConteudo::plataforma()->ativo()->where('url','/dou')->first();

        if(isset($pagina) && $pagina->view_propria != null)
        {
            return app('App\Http\Controllers\SiteProprio'.$pagina->view_propria)->dou();
        }

        return view('site.dou', compact('pagina') );
    }

    public function categoriaGeral()
    {
        $pagina = \App\Models\PaginaConteudo::plataforma()->ativo()->where('url','/categoria')->first();

        if(isset($pagina) && $pagina->view_propria != null)
        {
            return app('App\Http\Controllers\SiteProprio'.$pagina->view_propria)->categoria();
        }
    }

    public function fies()
    {
        $pagina = \App\Models\PaginaConteudo::select('view_propria')->plataforma()->ativo()->where('url','/fies')->first();

        if(isset($pagina) && $pagina->view_propria != null)
        {
            return app('App\Http\Controllers\SiteProprio'.$pagina->view_propria)->fies();
        }
    }

    public function comoFunciona()
    {
        $pagina = \App\Models\PaginaConteudo::select('view_propria')->plataforma()->ativo()->where('url','/como-funciona')->first();

        if(isset($pagina) && $pagina->view_propria != null)
        {
            return app('App\Http\Controllers\SiteProprio'.$pagina->view_propria)->comoFunciona();
        }
    }

    public function faq()
    {
        $pagina = \App\Models\PaginaConteudo::select('view_propria')->plataforma()->ativo()->where('url','/faq')->first();

        if(isset($pagina) && $pagina->view_propria != null)
        {
            return app('App\Http\Controllers\SiteProprio'.$pagina->view_propria)->faq();
        }
    }

    public function cpa()
    {
        $pagina = \App\Models\PaginaConteudo::select('view_propria')->plataforma()->ativo()->where('url','/cpa')->first();

        if(isset($pagina) && $pagina->view_propria != null)
        {
            return app('App\Http\Controllers\SiteProprio'.$pagina->view_propria)->cpa();
        }
    }

    public function ouvidoria()
    {
        $pagina = \App\Models\PaginaConteudo::select('view_propria')->plataforma()->ativo()->where('url','/ouvidoria')->first();

        if(isset($pagina) && $pagina->view_propria != null)
        {
            return app('App\Http\Controllers\SiteProprio'.$pagina->view_propria)->ouvidoria();
        }
    }

    public function enem()
    {
        $pagina = \App\Models\PaginaConteudo::select('view_propria')->plataforma()->ativo()->where('url','/enem')->first();

        if(isset($pagina) && $pagina->view_propria != null)
        {
            return app('App\Http\Controllers\SiteProprio'.$pagina->view_propria)->enem();
        }
    }

    public function transferencia()
    {
        $pagina = \App\Models\PaginaConteudo::select('view_propria')->plataforma()->ativo()->where('url','/transferencia')->first();

        if(isset($pagina) && $pagina->view_propria != null)
        {
            return app('App\Http\Controllers\SiteProprio'.$pagina->view_propria)->transferencia();
        }
    }

    public function vestibular()
    {
        $pagina = \App\Models\PaginaConteudo::select('view_propria')->plataforma()->ativo()->where('url','/vestibular')->first();

        if(isset($pagina) && $pagina->view_propria != null)
        {
            return app('App\Http\Controllers\SiteProprio'.$pagina->view_propria)->vestibular();
        }
    }

    public function prouni()
    {
        $pagina = \App\Models\PaginaConteudo::select('view_propria')->plataforma()->ativo()->where('url','/prouni')->first();

        if(isset($pagina) && $pagina->view_propria != null)
        {
            return app('App\Http\Controllers\SiteProprio'.$pagina->view_propria)->prouni();
        }
    }

    public function polos()
    {
        $pagina = \App\Models\PaginaConteudo::plataforma()->ativo()->where('url','/polos')->first();

        if(isset($pagina) && $pagina->view_propria != null)
        {
            return app('App\Http\Controllers\SiteProprio'.$pagina->view_propria)->polos();
        }

        return view('site.polos', compact('pagina') );
    }

    public function parceiros()
    {
        $pagina = \App\Models\PaginaConteudo::plataforma()->ativo()->where('url','/parceiros')->first();

        if(isset($pagina) && $pagina->view_propria != null)
        {
            return app('App\Http\Controllers\SiteProprio'.$pagina->view_propria)->parceiros();
        }

        return view('site.parceiros', compact('pagina') );
    }

    public function termosDeUso()
    {
        $pagina = \App\Models\PaginaConteudo::plataforma()->where('url','/termos-de-uso')->first();

        if($pagina)
        {
            if($pagina->view_propria)
            {
                return view($pagina->view_propria, compact('pagina') );
            }
        }

        return view('site.termos-de-uso', compact('pagina') );
    }

    public function politicaDePrivacidade()
    {
        $pagina = \App\Models\PaginaConteudo::select('view_propria')->plataforma()->ativo()->where('url','/politica-de-privacidade')->first();

        if(isset($pagina) && $pagina->view_propria != null)
        {
            return app('App\Http\Controllers\SiteProprio'.$pagina->view_propria)->politicaDePrivacidade();
        }

        return view('site.politica-de-privacidade', compact('pagina') );
    }

    public function buscar()
    {
        $pagina = \App\Models\PaginaConteudo::plataforma()->where('url','/buscar')->first();

        if($pagina)
        {
            if($pagina->view_propria)
            {
                return view($pagina->view_propria, compact('pagina') );
            }
        }

        return view('site.buscar', compact('pagina') );
    }

    public function categoria($slug, $id)
    {
        $pagina = \App\Models\PaginaConteudo::plataforma()->where('url','/categoria')->first();

        if($pagina)
        {
            if($pagina->view_propria)
            {
                return view($pagina->view_propria, compact('pagina') );
            }
        }
        
        return view('site.categoria', compact('pagina') );
    }

    public function contato()
    {
        $pagina = \App\Models\PaginaConteudo::plataforma()->where('url','/contato')->first();

        if(isset($pagina) && $pagina->view_propria != null)
        {
            return app('App\Http\Controllers\SiteProprio'.$pagina->view_propria)->contato();
        }

        return view('site.contato', compact('pagina') );
    }

    public function cursos()
    {
        $pagina = \App\Models\PaginaConteudo::plataforma()->where('url','/cursos')->first();

        $categorias = \App\Models\Categoria::plataforma()->tipo('curso')->orderBy('nome')->get();

        return view('site.cursos', compact('pagina','categorias') );
    }

    public function categoriaCurso($categoria, $slug)
    {
        $check_view_propria = \App\Models\PaginaConteudo::plataforma()->ativo()->where('url','/categoria-curso')->select('view_propria')->pluck('view_propria')[0] ?? null;

        if($check_view_propria)
        {
            return app('App\Http\Controllers\SiteProprio'.$check_view_propria)->categoriaCurso($categoria, $slug);
        }

        $curso = \App\Models\Curso::plataforma()->publico()->where('slug', $slug)->firstOrFail();

        $turmas = \App\Models\Turma::plataforma()->ativo()->publico()->where('curso_id', $curso->id)->get();

        return view('site.categoria-curso', compact('curso','turmas') );
    }

    public function curso($slug)
    {
        $check_view_propria = \App\Models\PaginaConteudo::plataforma()->ativo()->where('url','/curso')->select('view_propria')->pluck('view_propria')[0] ?? null;

        if($check_view_propria)
        {
            return app('App\Http\Controllers\SiteProprio'.$check_view_propria)->curso($slug);
        }

        $curso = \App\Models\Curso::plataforma()->publico()->where('slug', $slug)->firstOrFail();

        $turmas = \App\Models\Turma::plataforma()->ativo()->publico()->where('curso_id', $curso->id)->get();

        return view('site.curso', compact('curso','turmas') );
    }

    public function blog()
    {
        $pagina = \App\Models\PaginaConteudo::plataforma()->where('url','/blog')->first();

        if($pagina->view_propria)
        {
            return app('App\Http\Controllers\SiteProprio'.$pagina->view_propria)->blog();
        }

        $postDestaque1 = \App\Models\BlogPost::select('id','titulo','slug','foto_capa')->plataforma()->publico()->where('destaque_ordem', 1)->first();
        $postDestaque2 = \App\Models\BlogPost::select('id','titulo','slug','foto_capa')->plataforma()->publico()->where('destaque_ordem', 2)->first();
        $postDestaque3 = \App\Models\BlogPost::select('id','titulo','slug','foto_capa')->plataforma()->publico()->where('destaque_ordem', 3)->first();
        $postDestaque4 = \App\Models\BlogPost::select('id','titulo','slug','foto_capa')->plataforma()->publico()->where('destaque_ordem', 4)->first();

        return view('site.blog.lista', compact('pagina','postDestaque1','postDestaque2','postDestaque3','postDestaque4') );
    }

    public function blogPost($slug, $id)
    {
        $check_view_propria = \App\Models\PaginaConteudo::select('view_propria')->plataforma()->ativo()->where('url','/blog')->pluck('view_propria')[0] ?? null;

        if($check_view_propria)
        {
            return app('App\Http\Controllers\SiteProprio'.$check_view_propria)->blogPost($id);
        }

        $post = \App\Models\BlogPost::plataforma()->where('id', $id)->first();

        return view('site.blog.post', compact('post') );
    }

    public function redirectParaFormulario($formulario_id, $tipo, $tipo_id)
    {
        $slug = \App\Models\Formulario::plataforma()->where('id', $formulario_id)->pluck('slug')[0] ?? 'nao definido';

        return redirect()->route('formulario', ['slug' => $slug, 'formulario_id' => $formulario_id, 'tipo' => $tipo, 'turma_id_ou_trilha_id' => $tipo_id]);
    }

    public function formulario($slug, $formulario_id, $tipo, $tipo_id)
    {
        $formulario = \App\Models\Formulario::plataforma()->ativo()->where('id', $formulario_id)->first();

        if(is_null($formulario))
        {
            return back()->with('danger', 'Formulário de Inscrição de Matrícula não foi definido.');
        }

        $perguntas = \App\Models\FormularioPergunta::plataforma()->where('formulario_id', $formulario->id)->orderBy('ordem')->get();

	    if(request('sent'))
	    {
            if(request('check'))
                return back()->with('danger', 'SPAM Detected!');

	    	if(request('password'))
	    		request()->merge(['password' => Hash::make(request('password'))]);

	    	if(request('data_nascimento'))
	    		request()->merge(['data_nascimento' => \App\Models\Util::replaceDateEn(request('data_nascimento'))]);


            $mensagem = null;

            if(request('respostas'))
            {
                foreach(request('respostas') as $pergunta_id => $resposta)
                {
                    $pergunta = \App\Models\FormularioPergunta::plataforma()->where('id', $pergunta_id)->pluck('pergunta')[0] ?? null;
                    
                    if($pergunta)
                    {
                        $checkIsArray = is_array($resposta);
                        if($checkIsArray == true)
                            $resposta = implode(', ' , $resposta);

                        $mensagem = $mensagem . $pergunta . PHP_EOL . $resposta . PHP_EOL . PHP_EOL;
                    }
                }

                request()->merge(['mensagem' => $mensagem]);
            }


            $trilha_id = null;
            $curso_id  = null;
            $turma_id  = null;

            if($tipo == 'c') // C de Classe
            {
            	$turma = \App\Models\Turma::plataforma()->where('id', $tipo_id)->first();
                $turma_id = $turma->id ?? null;
            	$curso_id = $turma->curso_id ?? null;
            
            } else { // T de Trilha

            	$trilha = \App\Models\Trilha::plataforma()->where('id', $tipo_id)->first();
            	$trilha_id = $trilha->id ?? null;

                $curso_id = request('curso_id');
            }


            $contato = \App\Models\Contato::create(
                request()->all() + 
                [
                	'plataforma_id' =>  $formulario->plataforma_id,
			        'formulario_id' =>  $formulario->id,
                    'unidade_id'    =>  $formulario->unidade_id,
			        'curso_id'      =>  $curso_id,
			        'turma_id'      =>  $turma_id,
			        'trilha_id'     =>  $trilha_id,
			        'tipo'          =>  'L',
			        'assunto'       =>  $formulario->referencia,
			        'mensagem'      =>  $mensagem,
                ]
            );

            if(request('files'))
            {
                foreach(request('files') as $nome => $file)
                {
                    $documento = ContatoDocumento::create([
                        'plataforma_id' => $contato->plataforma_id,
                        'contato_id'    => $contato->id,
                        'nome'          => $nome,
                    ]);

                    $diretorio = 'plataformas/'.$formulario->plataforma_id.'/contato/'.$contato->id.'/arquivos/'.$documento->id;
                    $filename = $documento->id.'.'.$file->getClientOriginalExtension();

                    $path = Storage::putFileAs($diretorio, $file, $filename, 'public');
                    $linkStorage = \App\Models\Util::getLinkStorage();
                    $caminho = $linkStorage . $path;

                    $documento->update(['link' => $caminho]);
                }
            }

            $categoriasIds = \App\Models\FormularioCategoria::plataforma()->where('formulario_id', $formulario->id)->get()->pluck('categoria_id')->toArray();

            foreach($categoriasIds as $categoria_id)
                \App\Models\ContatoCategoria::create([
                	'plataforma_id' =>  $formulario->plataforma_id,
                    'categoria_id'  =>  $categoria_id,
                    'contato_id'    =>  $contato->id, 
                ]);


            if($contato->email)
            {
                if(isset($turma) && $turma->tipo_inscricao == 'auto-inscricao') // Matricular Aluno na Turma
                {
                    $aluno = $this->cadastrarAluno($contato, $categoriasIds);

                    \App\Models\TurmaAluno::create([
    			        'plataforma_id' => $formulario->plataforma_id,
    			        'turma_id'      => $turma_id,
    			        'curso_id'      => $curso_id,
    			        'aluno_id'      => $aluno->id,
                    ]);
                }

                if(isset($trilha) && $trilha->tipo_inscricao == 'auto-inscricao') // Matricular Aluno na Trilha
                {
                    $aluno = $this->cadastrarAluno($contato, $categoriasIds);

                    $countDuplicate = \App\Models\TrilhaAluno::plataforma()->where('aluno_id', $aluno->id)->where('trilha_id', $trilha->id)->count();
                    
                    if($countDuplicate == 0)
                        \App\Models\TrilhaAluno::create([
                            'plataforma_id' => $formulario->plataforma_id,
                            'trilha_id'     => $trilha->id,
                            'aluno_id'      => $aluno->id,
                        ]);

                    $turmasIds = \App\Models\TrilhaTurma::plataforma()->where('trilha_id', $trilha_id)->get()->pluck('turma_id')->toArray();
                    $turmas = \App\Models\Turma::plataforma()->whereIn('id', $turmasIds)->select('id','curso_id')->get();

                    foreach($turmas as $turma)
                    {
                        $countDuplicate = \App\Models\TurmaAluno::plataforma()->where('aluno_id', $aluno->id)->where('turma_id', $turma->id)->count();

                        if($countDuplicate == 0)
                            \App\Models\TurmaAluno::create([
                                'plataforma_id' => $formulario->plataforma_id,
                                'turma_id'      => $turma->id,
                                'curso_id'      => $turma->curso_id,
                                'aluno_id'      => $aluno->id,
                            ]);
                    }
                }
            }

            if(FormularioIntegracao::verificaSeIntegrado('hubspot'))
            {
                FormularioIntegracao::sendToContatosHubspot($contato);
            }

            if(FormularioIntegracao::verificaSeIntegrado('email'))
            {
                FormularioIntegracao::sendToEmail($contato);
            }

            if($formulario->texto_retorno)
                return back()->with('success', 'Inscrição enviada com êxito.')->with('contato_id', $contato->id);
            else
                return redirect($formulario->link_retorno ?? \Request::fullUrl())->with('success', 'Inscrição enviada com êxito.');

	    } // end sent


        $check_view_propria = \App\Models\PaginaConteudo::plataforma()->ativo()->where('url','/f')->select('view_propria')->pluck('view_propria')[0] ?? null;

        if($check_view_propria)
           return view($check_view_propria, compact('formulario','perguntas','tipo_id') );
        else
    	   return view('site.formulario', compact('formulario','perguntas','tipo_id') );
    }

    public function cadastrarAluno($contato, $categoriasIds)
    {
        $aluno = \App\Models\User::plataforma()->where('email', $contato->email)->first();

        if(is_null($aluno))
        {
            $password = $contato->password ?? Hash::make($contato->cpf ?? \App\Models\Util::generateHash());

            $aluno = \App\Models\User::create(request()->all() + [
                'plataforma_id' => $contato->plataforma_id,
                'tipo'          => 'aluno',
                'password'      => $password,
            ]);
        }

        foreach($categoriasIds as $categoria_id)
        {
            $countDuplicate = \App\Models\UserCategoria::plataforma()->where('user_id', $aluno->id)->where('categoria_id', $categoria_id)->count();
            
            if($countDuplicate == 0)
                \App\Models\UserCategoria::create([
                    'plataforma_id' => $contato->plataforma_id,
                    'categoria_id'  => $categoria_id,
                    'user_id'       => $aluno->id,
                ]);
        }

        $endereco['cep']              = request('cep');
        $endereco['rua']              = request('rua');
        $endereco['numero']           = request('numero');
        $endereco['bairro']           = request('bairro');
        $endereco['cidade']           = request('cidade');
        $endereco['estado']           = request('estado');
        $endereco['complemento']      = request('complemento');
        $endereco['ponto_referencia'] = request('ponto_referencia');

        if(array_filter($endereco))
        {
            $userEndereco = \App\Models\UserEndereco::where('user_id', $aluno->id)->first();

            if(is_null($userEndereco))
                \App\Models\UserEndereco::create($endereco + ['plataforma_id' => $contato->plataforma_id, 'user_id' => $aluno->id]);
            else
                \App\Models\UserEndereco::where('user_id', $aluno->id)->update($endereco);
        }

        return $aluno;
    }

}
