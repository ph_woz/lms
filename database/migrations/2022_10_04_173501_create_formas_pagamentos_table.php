<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFormasPagamentosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('formas_pagamentos', function (Blueprint $table) {
            $table->id();
            $table->integer('plataforma_id'); 
            $table->string('tipo_cadastro')->nullable();
            $table->integer('registro_id')->nullable();
            $table->integer('trilha_id')->nullable();
            $table->integer('turma_id')->nullable();
            $table->string('tipo')->nullable();
            $table->string('parcela')->nullable();
            $table->string('valor')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('formas_pagamentos');
    }
}
