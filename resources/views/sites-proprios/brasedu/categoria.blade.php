<!doctype html>
<html lang="pt-br">
<head>

  <!-- Google tag (gtag.js) -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=G-MREB450HCT"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'G-MREB450HCT');
  </script>
  
   <meta charset="utf-8">
   <meta name="viewport" content="width=device-width, initial-scale=1">

   <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/uikit@3.5.9/dist/css/uikit.min.css" />
   <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
   <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
   <link rel="stylesheet" href="{{ asset('css/util.css') }}?v=14">
   <link rel="stylesheet" href="{{ asset('sites-proprios/brasedu/css/home.css') }}?v=23">

   <title>{{ $categoria->seo_title ?? $categoria->nome }}</title>
   <meta name="robots" content="index, follow">
   <meta name="description" content="{{ $categoria->seo_description ?? null }}"/>
   <meta name="keywords" content="{{ $categoria->seo_keywords ?? null }}"/>
   <link rel="canonical" href="{{ Request::url() }}" />
   <link rel="shortcut icon" href="{{ $plataforma->favicon }}">
   <meta property="og:locale" content="pt_BR" />
   <meta property="og:type" content="website" />
   <meta property="og:title" content="{{ $categoria->seo_title ?? $categoria->nome }}" />
   <meta property="og:description" content="{{ $categoria->seo_description ?? null }}" />
   <meta property="og:url" content="{{ Request::url() }}" />
   <meta property="og:site_name" content="{{ $plataforma->nome }}" />
   <meta property="og:image" content="{{ $plataforma->logotipo }}" />
   <meta name='author' content='WOZCODE | Pedro Henrique' />

</head>
<body class="bg-f4" id="inicio">

   @include('sites-proprios/brasedu/componentes/header')

   <main class="pt-9rem">

      <section class="py-5 mb-lg-5">
         <div class="container-fluid px-xl-7 py-lg-5 py-3 mt-lg-2">

           <h1 class="font-roboto weight-600 color-titulo fs-25">
             {{ $categoria->nome }}
           </h1>

            <hr class="mb-4">

            <div class="row">
              @foreach($cursos as $cursoDaCategoria)

               <div class="col-lg-4 mb-4">
                  <div class="card border-0 box-shadow rounded">

                     <div class="card-header px-3 px-1 d-flex justify-content-center align-items-center position-relative text-center" style="min-height: 75px;">
                        <h5 class="mb-0 pb-0 text-white weight-700 text-shadow-2 ls-05 font-montserrat">{{$cursoDaCategoria->nome}}</h5>
                     </div>
                     
                     <img src="{{ $cursoDaCategoria->foto_capa ?? asset('images/no-image.jpeg') }}" class="img-fluid h-200 object-fit-cover">

                      <div class="position-relative p-4">

                         <div>
                            <span class="d-inline-block mb-2 me-1 text-white font-poppins weight-500 bg-newsletter px-3 py-1-5 box-shadow text-shadow-1 rounded fs-14 ls-05">{{ $cursoDaCategoria->nivel ?? 'Graduação' }}</span>
                            <span class="d-inline-block mb-2 me-1 text-white font-poppins weight-500 bg-newsletter px-3 py-1-5 box-shadow text-shadow-1 rounded fs-14 ls-05">{{ $cursoDaCategoria->carga_horaria ?? '0 horas' }}</span>
                            <span class="d-inline-block mb-2 me-1 text-white font-poppins weight-500 bg-newsletter px-3 py-1-5 box-shadow text-shadow-1 rounded fs-14 ls-05">{{ $cursoDaCategoria->modalidade ?? 'EAD' }}</span>
                         </div>

                         <hr class="border border-secondary" />

                         <div>
                            <div class="text-dark weight-600">
                               <p class="m-0">
                                  R$ {{ $cursoDaCategoria->valor_promocional ?? '0.000,00' }} a vista ou
                               </p>
                               <p class="m-0 fs-23 font-montserrat weight-800 ls-05">
                                  {{ $cursoDaCategoria->valor_parcelado ?? '00x de R$ 000,00' }}
                               </p>
                            </div>
                         </div>

                         <hr class="border border-secondary">

                         <div class="text-center w-100 mt-3">
                             <a href="/curso/{{$cursoDaCategoria->slug}}" class="btnSaibaMais">
                                SAIBA MAIS
                             </a>
                         </div>

                      </div>

                   </div>
                </div>

              @endforeach
            </div>

         </div>
      </section>

   </main>


   @include('sites-proprios/brasedu/componentes/footer')
    
</body>
</html>