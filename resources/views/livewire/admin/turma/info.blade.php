<div>

	<section>

		<div class="d-flex justify-content-end mb-3">

			@if($turma->modalidade != 'EaD' && $turma->modalidade != null)
				<a href="{{ route('admin.turma.editar.lista-de-presenca', ['id' => $turma->id]) }}" class="btn btn-primary fs-13 py-1-5 mb-1">
					Lista de Presença
				</a>
			@endif

			<a href="{{ route('admin.aluno.lista', ['add' => 'S', 'add_alunos_para_turma_id' => $turma->id, 'add_alunos_para_curso_id' => $turma->curso_id]) }}" class="btn btn-primary fs-13 py-1-5 ms-2 mb-1">
				Matricular alunos em massa
			</a>

		</div>

		<div class="section-title rounded-bottom-0 d-flex justify-content-between align-items-center">
			{{ $turma->nome }}
			<div>

				<a href="#" data-bs-toggle="modal" data-bs-target="#ModalMetaDados" class="btn btn-info me-2 fs-14 mb-1 py-1 px-2-3">
					<i class="fa fa-align-left"></i>
				</a>
				<a href="/admin/turma/editar/{{$turma->id}}" class="aclNivel2 btn btn-warning text-dark fs-14 mb-1 py-1 px-2">
					<i class="fa fa-edit"></i>
				</a>
				<a href="#" data-bs-toggle="modal" data-bs-target="#ModalDeletarTurma" class="aclNivel3 btn btn-danger mb-1 fs-15 py-1 px-2 ms-2">
					<i class="fa fa-trash"></i>
				</a>
			</div>
		</div>

		<div class="bg-white border p-4 d-lg-flex justify-content-between">

			<div class="mb-2 mb-lg-0">
				<p class="mb-0 weight-600 color-444">Curso</p>
				@if($curso)
					<a href="{{ route('admin.curso.info', ['id' => $curso->id]) }}" class="color-blue-info-link">
						{{ $curso->referencia }}
					</a>
				@else
					<p class="mb-0">
						Não definido
					</p>
				@endif
			</div>

			<div class="mb-2 mb-lg-0">
				<p class="mb-0 weight-600 color-444">
					Formulário de Inscrição
					@if($turma->tipo_inscricao == 'requer-aprovacao')
						(Requer aprovação)
					@else
						(Auto inscrição)
					@endif
					@if($formulario)
						<a href="/f/{{$formulario->slug}}/{{$formulario->id}}/c/{{$turma->id}}" target="_blank" class="color-blue-info-link hover-d-none">
							- Abrir Link <i class="fas fa-link"></i>
						</a>
					@endif
				</p>
				@if($formulario)
					<a href="{{ route('admin.formulario.info', ['id' => $formulario->id]) }}" class="color-blue-info-link">
						{{ $formulario->referencia }}
					</a>
				@else
					<p class="mb-0">
						Não definido
					</p>
				@endif
			</div>

			@if($unidade)
			<div class="mb-2 mb-lg-0">
				<p class="mb-0 weight-600 color-444">Unidade</p>
				<a href="{{ route('admin.unidade.info', ['id' => $unidade->id]) }}" class="color-blue-info-link">
					{{ $unidade->nome }}
				</a>
			</div>
			@endif

		</div>

		<div class="bg-white border p-4 d-lg-flex justify-content-between">

			@if($turma->nota_minima_aprovacao != null)
			<div class="mb-2 mb-lg-0">
				<p class="mb-0 weight-600 color-444">Nota mínima para aprovação</p>
				<p class="mb-0">
					{{ $turma->nota_minima_aprovacao }}
				</p>
			</div>
			@endif

			<div class="mb-2 mb-lg-0">
				<p class="mb-0 weight-600 color-444">Quantidade de Alunos</p>
				<p class="mb-0">
					@if($countAlunos > 0)
						{{ $countAlunos }}
					@else
						Não há nenhum aluno nesta turma
					@endif
				</p>
			</div>

		</div>

		@if(count($avaliacoes) > 0)
		<div class="bg-white border p-4 d-lg-flex justify-content-between">
			<div class="mb-2 mb-lg-0">
				<p class="mb-0 weight-600 color-444">Avaliações ({{ count($avaliacoes) }})</p>
				<ul class="list-group list-group-flush">
			  		@foreach($avaliacoes as $avaliacao)
			  			<a href="{{ route('admin.avaliacao.info', ['id' => $avaliacao->id]) }}" class="list-group-item">
			  				{{ $avaliacao->referencia }}
			  			</a>
			  		@endforeach
			  	</ul>
			</div>
		</div>
		@endif

	</section>

	<div class="modal fade" id="ModalMetaDados" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	  	<div class="modal-dialog modal-dialog-centered" role="document">
	    	<div class="modal-content">
	      		<div class="modal-header">
	        		<h3 class="modal-title">Metadados</h3>
	    			<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
	      		</div>
	      		<div class="modal-body">

					<ul class="list-group list-group-flush">
	  					<li class="list-group-item">
							<span class="mb-0 weight-600 color-444">Cadastrante:</span>
							@if($cadastrante)
								<a href="/admin/colaborador/info/{{$cadastrante->id}}" class="color-blue-info-link">
									{{ $cadastrante->nome }}
								</a>
							@else
								Não definido
							@endif
	  					</li>   
	  					<li class="list-group-item">
							<span class="mb-0 weight-600 color-444">Status:</span>
							@if($turma->status == 0)
								<span class="text-success weight-700">Ativo</span>
							@else
								<span class="text-danger weight-700">Desativado</span>
							@endif
	  					</li>
	  					<li class="list-group-item">
							<span class="mb-0 weight-600 color-444">Data de Cadastro:</span>
							@if($turma->created_at)
								<span class="mb-0">{{ $turma->created_at->format('d/m/Y H:i') ?? null }} ({{$turma->created_at->diffForHumans()}}) </span>
	  						@else
	  							Não definido
	  						@endif
	  					</li>   
	  				</ul>

	      		</div>
	    	</div>
	  	</div>
	</div>

	<form wire:submit.prevent="delete" method="POST">
		<div wire:ignore class="modal fade" id="ModalDeletarTurma" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  		<div class="modal-dialog modal-dialog-centered">
	    		<div class="modal-content">
	      			<div class="modal-header">
	        			<h3 class="modal-title" id="exampleModalLabel">Deletar Turma</h3>
	        			<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
	      			</div>
	      			<div class="modal-body text-center py-5">
	        			<p class="m-0 fs-16">
	        				Você tem certeza que deseja deletar esta turma?
	        				<br>
	        				Todas as informações serão perdidas definitivamente.
	        			</p>
	      			</div>
	      			<div class="modal-footer">
	      				<div class="d-flex w-100">
		      				<input type="password" wire:model="password_to_delete" class="w-100 form-control p-form me-3" placeholder="Digite sua senha" required>
		        			<button type="submit" class="btn btn-danger weight-600">Confirmar</button>
	    				</div>
	      			</div>
	    		</div>
	  		</div>
		</div>
	</form>

	@include('livewire.util.toast')

</div>

