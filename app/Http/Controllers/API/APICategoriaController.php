<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use App\Models\WozcodeSubcategoria;


class APICategoriaController extends Controller
{
    public function listaSubcategorias(Request $request)
    {
        $subcategorias = WozcodeSubcategoria::select('id','nome')->where('categoria_id', $request->categoria_id)->orderBy('nome')->get();

        return response()->json(['data' => $subcategorias]);
    }
}