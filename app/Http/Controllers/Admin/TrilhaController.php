<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Models\Trilha;
use App\Models\FormaPagamento;
use App\Models\TrilhaTurma;
use App\Models\Turma;
use App\Models\Certificado;
use App\Models\Formulario;
use App\Models\FluxoCaixa;
use App\Models\User;
use App\Models\Unidade;
use App\Models\Categoria;
use App\Models\TrilhaCategoria;
use App\Models\CorpoDocenteTrilha;
use App\Models\SecaoDescritiva;
use App\Models\MensalidadePorSemestre;

class TrilhaController extends Controller
{
    public function lista()
    {
        return view('admin.trilha.lista');
    }
    
    public function info($trilha_id)
    {
        return view('admin.trilha.info', compact('trilha_id') );
    }

    public function add()
    {
        $turmas        = Turma::plataforma()->ativo()->orderBy('nome')->get();
        $certificados  = Certificado::plataforma()->ativo()->orderBy('referencia')->get();
        $formularios   = Formulario::plataforma()->ativo()->orderBy('referencia')->get();
        $unidades      = Unidade::plataforma()->ativo()->orderBy('nome')->get();
        $categorias    = Categoria::plataforma()->ativo()->tipo('trilha')->orderBy('nome')->get();
        $colaboradores = User::select('id','nome')->plataforma()->colaborador()->ativo()->orderBy('nome')->get();

        if(request('sent'))
        {
            $countDuplicado = Trilha::plataforma()->where('referencia', request('referencia'))->exists();

            if($countDuplicado == true)
            {
                return back()->withInput()->with('danger', 'Já existe uma Trilha cadastrada com esta Referência.');
            }

            $trilha = Trilha::create(request()->all() + ['slug' => \Str::slug(request('nome'))] );

            $this->syncFoto($trilha, 'foto_capa', 'foto');
            $this->syncFoto($trilha, 'foto_miniatura', 'foto_min');
            $this->syncTrilhasTurmas($trilha->id);
            $this->syncCategorias($trilha->id);
            $this->syncMensalidadesPorSemestre($trilha->id);
            $this->syncFormasPagamento($trilha->id);
            $this->syncMatrizCurricular($trilha, 'matriz_curricular', 'matriz');

            return redirect()->route('admin.trilha.info', ['id' => $trilha->id])->with('success', 'Trilha criada com êxito.');
        }

        return view('admin.trilha.add', 
            compact(
                'turmas',
                'certificados',
                'formularios',
                'unidades',
                'categorias',
                'colaboradores'
            ) 
        );

    	return view('admin.trilha.add');
    }

    public function editar($trilha_id)
    {
        $trilha = Trilha::plataforma()->findOrFail($trilha_id);
        $turmasSelected = TrilhaTurma::plataforma()->where('trilha_id', $trilha_id)->get()->pluck('turma_id')->toArray();

        $turmas       = Turma::plataforma()->ativo()->orderBy('nome')->get();
        $certificados = Certificado::plataforma()->ativo()->orderBy('referencia')->get();
        $formularios  = Formulario::plataforma()->ativo()->orderBy('referencia')->get();
        $unidades     = Unidade::plataforma()->ativo()->orderBy('nome')->get();

        $categorias = Categoria::plataforma()->ativo()->tipo('trilha')->orderBy('nome')->get();
        $categoriasSelected = TrilhaCategoria::plataforma()->where('trilha_id', $trilha_id)->get()->pluck('categoria_id')->toArray();

        $formasPagamento = FormaPagamento::plataforma()->trilha()->where('registro_id', $trilha_id)->get();

        $colaboradores = User::select('id','nome')->plataforma()->colaborador()->ativo()->orderBy('nome')->get();

        $mensalidadesPorSemestres = MensalidadePorSemestre::plataforma()->trilha()->where('registro_id', $trilha_id)->get();

        if(request('sent'))
        {
            $countDuplicado = Trilha::plataforma()->where('id','<>', $trilha_id)->where('referencia', request('referencia'))->exists();

            if($countDuplicado == true)
            {
                return back()->withInput()->with('danger', 'Já existe uma Trilha cadastrada com esta Referência.');
            }

            if(request('referencia') != $trilha->referencia)
            {
                if(FluxoCaixa::checkActive())
                {
                    FluxoCaixa::plataforma()->trilha()->where('produto_id', $trilha_id)->update(['produto_nome' => request('referencia')]);
                }
            }
            
            $trilha->update(request()->all() + ['slug' => \Str::slug(request('nome'))] );

            $this->syncFoto($trilha, 'foto_capa', 'foto');
            $this->syncFoto($trilha, 'foto_miniatura', 'foto_min');
            $this->syncTrilhasTurmas($trilha_id);
            $this->syncCategorias($trilha->id);
            $this->syncMensalidadesPorSemestre($trilha->id);
            $this->syncFormasPagamento($trilha->id);
            $this->syncMatrizCurricular($trilha, 'matriz_curricular', 'matriz');


            return redirect()->route('admin.trilha.info', ['id' => $trilha->id])->with('success', 'Trilha atualizada com êxito.');
        }

        return view('admin.trilha.editar', 
            compact(
                'trilha',
                'turmasSelected',
                'turmas',
                'certificados',
                'formularios',
                'unidades',
                'categorias',
                'categoriasSelected',
                'formasPagamento',
                'colaboradores',
                'mensalidadesPorSemestres'
            ) 
        );
    }

    public function editarSecoesDescritivas($trilha_id)
    {
        $trilha = Trilha::select('referencia')->plataforma()->where('id', $trilha_id)->value('referencia');
        $secoes = SecaoDescritiva::where('registro_id', $trilha_id)->orderBy('ordem')->get();

        if(request('add'))
        {
            SecaoDescritiva::create([
                'tipo'        => 'T',
                'registro_id' => $trilha_id,
                'titulo'      => request('titulo'),
                'descricao'   => request('descricao'),
                'expandido'   => request('expandido'),
                'ordem'       => request('ordem'),
            ]);

            return back()->with('success', 'Seção cadastrada com êxito.');
        }

        if(request('editar'))
        {
            foreach(request('secao') as $secao)
            {
                SecaoDescritiva::plataforma()
                    ->where('registro_id', $trilha_id)
                    ->where('id', $secao['id'])
                    ->update([
                        'titulo'    => $secao['titulo'],
                        'descricao' => $secao['descricao'],
                        'expandido' => $secao['expandido'],
                        'ordem'     => $secao['ordem'],
                    ]);
            }

            return back()->with('success', 'Seções atualizada com êxito.');
        }

        if(request('deletar'))
        {
            SecaoDescritiva::plataforma()
                ->where('registro_id', $trilha_id)
                ->where('id', request('deletar_id'))
                ->delete();

            return back()->with('success', 'Seção deletada com êxito.');
        }

        return view('admin.trilha.secoes', compact('trilha','trilha_id','secoes') );
    }

    public function syncCorpoDocente($trilha_id)
    {
        CorpoDocenteTrilha::plataforma()->where('trilha_id', $trilha_id)->delete();

        $corpoDocente = request('corpoDocente');
       
        if($corpoDocente != null)
        {
            foreach($corpoDocente as $user_id)
            {
                CorpoDocenteTrilha::create([
                    'plataforma_id' => session('plataforma_id') ?? \Auth::user()->plataforma_id,
                    'trilha_id'     => $trilha_id, 
                    'user_id'       => $user_id,
                ]);
            }
        }
    }

    public function syncMensalidadesPorSemestre($trilha_id)
    {
        MensalidadePorSemestre::plataforma()->trilha()->where('registro_id', $trilha_id)->delete();

        $mensalidades = array_combine(request('semestres') ?? array(), request('valores_por_semestres') ?? array());

        foreach($mensalidades as $mensalidade => $valor)
        {
            if($mensalidades && $valor)
            {
                MensalidadePorSemestre::create([
                    'plataforma_id' => session('plataforma_id') ?? \Auth::user()->plataforma_id,
                    'registro_id'   => $trilha_id,
                    'tipo'          => 'T',
                    'semestre'      => intval($mensalidade),
                    'valor'         => $valor,
                ]);
            }
        }
    }

    public function syncMatrizCurricular($trilha, $tipo, $inputFileName)
    {
        $file = request()->file($inputFileName);
     
        if($file)
        {
            $path = str_replace(\App\Models\Util::getLinkStorage(), '', $trilha->$tipo);
            Storage::delete($path);
            
            $diretorio = 'plataformas/'.session('plataforma_id').'/trilhas/'.$trilha->id;
            $filename = $tipo.'.'.$file->getClientOriginalExtension();

            $path = Storage::putFileAs($diretorio, $file, $filename, 'public');
            $linkStorage = \App\Models\Util::getLinkStorage();
            $caminho = $linkStorage . $path;

            $trilha->update([$tipo => $caminho]);
        }
    }

    public function syncFormasPagamento($trilha_id)
    {
        FormaPagamento::plataforma()->where('registro_id', $trilha_id)->delete();

        $formaCartaoDeCredito = array_combine(request('cartao-de-credito')['parcelas'], request('cartao-de-credito')['valores']);
        foreach($formaCartaoDeCredito as $parcela => $valor)
        {
            if($parcela && $valor)
            {
                FormaPagamento::create([
                    'registro_id'   => $trilha_id,
                    'tipo_cadastro' => 'Trilha',
                    'tipo'          => 'CC',
                    'parcela'       => $parcela,
                    'valor'         => $valor,
                ]);
            }
        }

        $formaCartaoDeDebito = array_combine(request('cartao-de-debito')['parcelas'], request('cartao-de-debito')['valores']);
        foreach($formaCartaoDeDebito as $parcela => $valor)
        {
            if($parcela && $valor)
            {
                FormaPagamento::create([
                    'registro_id'   => $trilha_id,
                    'tipo_cadastro' => 'Trilha',
                    'tipo'          => 'CD',
                    'parcela'       => $parcela,
                    'valor'         => $valor,
                ]);
            }
        }

        $formaBoleto = array_combine(request('boleto')['parcelas'], request('boleto')['valores']);
        foreach($formaBoleto as $parcela => $valor)
        {
            if($parcela && $valor)
            {
                FormaPagamento::create([
                    'registro_id'   => $trilha_id,
                    'tipo_cadastro' => 'Trilha',
                    'tipo'          => 'BO',
                    'parcela'       => $parcela,
                    'valor'         => $valor,
                ]);
            }
        }
    }

    public function syncTrilhasTurmas($trilha_id)
    {
        TrilhaTurma::plataforma()->where('trilha_id', $trilha_id)->whereNotIn('turma_id', request('turmas') ?? array())->delete();
        
        if(request('turmas'))
        {
            foreach(request('turmas') as $turma_id) {

                $countDuplicate = TrilhaTurma::plataforma()->where('trilha_id', $trilha_id)->where('turma_id', $turma_id)->count();
            
                if($countDuplicate <= 0)
                    if($turma_id)
                        TrilhaTurma::create(['trilha_id' => $trilha_id, 'turma_id' => $turma_id]);
            }
        }
        
    }

    public function syncFoto($trilha, $tipo, $inputFileName)
    {
        $file = request()->file($inputFileName);

        if($file)
        {
            $path = str_replace(\App\Models\Util::getLinkStorage(), '', $trilha->$tipo);
            Storage::delete($path);
            
            $diretorio = 'plataformas/'.session('plataforma_id').'/trilhas/'.$trilha->id;
            $filename = $tipo.'.'.$file->getClientOriginalExtension();

            $path = Storage::putFileAs($diretorio, $file, $filename, 'public');
            $linkStorage = \App\Models\Util::getLinkStorage();
            $caminho = $linkStorage . $path;

            $trilha->update([$tipo => $caminho]);
        }
    }

    public function syncCategorias($trilha_id)
    {
        TrilhaCategoria::plataforma()->where('trilha_id', $trilha_id)->whereNotIn('categoria_id', request('categorias') ?? array())->delete();

        if(request('categorias') == null)
        {
            return;
        }

        foreach(request('categorias') as $categoria_id)
        {
            $countDuplicate = TrilhaCategoria::plataforma()->where('trilha_id', $trilha_id)->where('categoria_id', $categoria_id)->count();
            
            if($countDuplicate <= 0)
            {
                TrilhaCategoria::create([
                    'plataforma_id'  => session('plataforma_id'),
                    'trilha_id'      => $trilha_id, 
                    'categoria_id'   => $categoria_id,
                ]);
            }
        }
    }

}