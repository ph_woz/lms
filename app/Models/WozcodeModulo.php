<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WozcodeModulo extends Model
{
    use HasFactory;

    protected $table = 'wozcode_modulos';

    public $timestamps = false;

    protected $fillable = ['texto','url','ordem','status'];


    public function scopeUrl($query, $url)
    {
        return $query->where('url', $url);
    }

    public static function getAtualUrlStatus()
    {
        $status = WozcodeModulo::where('url', \Request::segment(2))->pluck('status');
        $status = $status[0];
        
        return $status;
    }

}
