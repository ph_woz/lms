@extends('layouts.site')

@section('seo_title', $curso->seo_title ?? $nomePlataforma)
@section('seo_description', $curso->seo_description ?? null)
@section('seo_keywords', $curso->seo_keywords ?? null)

@section('content')
<section>
 <div class="w-100 h-lg-400 h-xs-100vh pt-lg-5" style="background-size: cover; background-position: center; background-image: url('{{ $curso->foto_capa ?? asset('images/bg-login.png') }}');">
    <div class="position-relative h-100 px-xl-7 px-3">
    <div class="background-overlay h-100" style="background: rgba(0,0,0,1);"></div>

       <div class="row align-items-center h-100 position-relative">
          <div class="mt-2">
             <h1 class="mb-4-5 text-light font-poppins weight-800 fs-xs-35 fs-md-50 text-shadow-3">
                <span id="cursoTiulo">{!! $curso->nome !!}</span>
             </h1>
             <a id="btnCursoInscrevaSe" class="btn btn-primary weight-700 box-shadow text-shadow-2 fs-17 ls-05 text-light hover-d-none px-4 py-3 font-poppins" href="#turmas">
                INSCREVA-SE AGORA!
             </a>
          </div>
       </div>
    </div>
 </div>
</section>

<section class="pb-lg-3">
	<div class="row container-fluid px-3 gx-0 gx-lg-4 px-xl-5 mt--15px position-relative pb-5">
		<div class="col-lg-8">
			<div class="bg-white px-lg-5 px-4 pt-4-5 pb-4-5 box-shadow border-top rounded-20">

				<p class="mb-0 pb-0 font-poppins weight-600 fs-14 color-666">
					Sobre o curso
				</p>

				<hr class="border border-secondary">

				@if($curso->descricao == null)
					<p class="mb-0 pb-0 font-poppins weight-600 fs-13 color-666">
						<i class="fas fa-exclamation-circle"></i> Descrição do Curso não definida.
					</p>
				@endif

				<p id="cursoDescricao">
					{!! nl2br($curso->descricao) !!}
				</p>

			</div>
		</div>
		<div class="col-lg-4 mt-4 mt-lg-0">
			<div class="bg-white px-4-5 pt-4-5 pb-4-5 box-shadow border-top rounded-20">

				@if($curso->video)
					<iframe width="100%" height="225" class="rounded mb-4" src="{{ $curso->video }}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
				@endif

                @if($curso->nivel)
                <div>                                
                    <p class="fs-15 mb-0 weight-600 color-555">NÍVEL:</p>
                    <p class="fs-14 mb-0 weight-600 font-poppins color-444">{{ $curso->nivel }}</p>
                    <hr/>
                </div>
                @endif

                @if($curso->tipo_formacao)
                <div>                                
                    <p class="fs-15 mb-0 weight-600 color-555">TIPO DE FORMAÇÃO:</p>
                    <p class="fs-14 mb-0 weight-600 font-poppins color-444">{{ $curso->tipo_formacao }}</p>
                    <hr/>
                </div>
                @endif
                
                @if($curso->carga_horaria)
                <div>                                
                    <p class="fs-15 mb-0 weight-600 color-555">CARGA HORÁRIA:</p>
                    <p class="fs-14 mb-0 weight-600 font-poppins color-444">{{ $curso->carga_horaria }} horas</p>
                </div>
                @endif

                @if($curso->matriz_curricular)
                <div>                                
                    <p class="fs-15 mb-0 weight-600 color-555">MATRIZ CURRICULAR:</p>
                    <a href="{{ $curso->matriz_curricular }}" target="_blank" class="fs-14 mb-0 weight-600 font-poppins color-444">{{ $curso->matriz_curricular }} horas</a>
                </div>
                @endif


			</div>
		</div>
	</div>
</section>

<section class="pb-5" id="turmas">
	<div class="row container-fluid px-3 gx-0 gx-lg-4 px-xl-5 mt--15px position-relative pb-5">
		<div class="col-lg-8">
			<div class="bg-white px-lg-5 px-4 pt-4-5 pb-4-5 box-shadow border-top rounded-20">

				<p class="font-poppins weight-600 fs-14 color-666">
					Turmas
				</p>

              	@if(count($turmas) == 0)
                 	<p class="mb-0 pb-0 font-poppins weight-600 fs-13 color-666">
                    	<i class="fas fa-exclamation-circle"></i> Não há turmas publicadas disponíveis para inscrição.
                	</p>
              	@endif

				@foreach($turmas as $turma)
					<div class="mb-4 p-4 border rounded-20">
						<div class="d-flex align-items-end justify-content-between">
							<h1 class="m-0 p-0 fs-17 font-poppins color-blue-dark">
								{{ $turma->nome }}
							</h1>
							@if($turma->modalidade)
								<span class="modalidade">
									{{ $turma->modalidade }}
								</span>
							@endif
						</div>
						<hr/>
						<div>
							<div class="d-lg-flex justify-content-between">
								
								@if($turma->data_inicio)
								<div class="mb-3 mb-lg-0 d-flex align-items-center">
									<span class="me-3">
										<i class="fs-25 far fa-calendar-alt"></i>
									</span>
									<p class="m-0">
										<span class="d-block font-poppins fs-13 color-666">Data:</span>
										<span class="d-block fs-15 weight-600 color-555">
											Início: {{ \App\Models\Util::replaceDatePt($turma->data_inicio) }}
										</span>
										<span class="d-block fs-15 weight-600 color-555">
											Término: {{ \App\Models\Util::replaceDatePt($turma->data_termino) }}
										</span>
									</p>
								</div>
								@endif

								@if($turma->horario)
								<div class="mb-3 mb-lg-0 d-flex align-items-center pe-lg-2">
									<span class="me-3">
										<i class="fs-25 far fa-clock"></i>
									</span>
									<p class="m-0">
										<span class="d-block font-poppins fs-13 color-666">Dias e Horário:</span>
										<span class="d-block fs-15 weight-600 color-555">
											{{ $turma->frequencia }}
										</span>
										<span class="d-block fs-15 weight-600 color-555">
											{{ $turma->horario }}
										</span>
									</p>
								</div>
								@endif

								@if($turma->duracao)
								<div class="mb-3 mb-lg-0 d-flex align-items-center pe-lg-2">
									<span class="me-3">
										<i class="fs-25 far fa-clock"></i>
									</span>
									<p class="m-0">
										<span class="d-block font-poppins fs-13 color-666">Duração:</span>
										<span class="d-block fs-15 weight-600 color-555">
											{{ $turma->duracao }}
										</span>
									</p>
								</div>
								@endif

							</div>

							@if($turma->data_inicio || $turma->horario || $turma->duracao)
								<hr/>
							@endif

							<div class="d-lg-flex justify-content-between align-items-center">

								<div class="ps-lg-1">
									@if($turma->valor_a_vista)
									<p class="d-flex align-items-center m-0 p-0">
										<span class="me-2 weight-600 color-555">
											por:
										</span>
										<span class="fs-20 font-poppins color-blue-dark weight-700">
											R$ {{ $turma->valor_a_vista }}
										</span>
									</p>
									@endif
									@if($turma->valor_parcelado)
									<p class="mb-0 me-2 weight-600 color-555">
										<span class="me-2 weight-600 color-555">
											ou: 
										</span>
										<span class="fs-20 font-poppins color-blue-dark weight-700">
											R$ {{ $turma->valor_parcelado }}
										</span>
									</p>
									@endif
								</div>

								<div class="mt-3 mt-lg-0">
									<a href="/f/{{$turma->formulario_id ?? 0}}/c/{{$turma->id ?? 0}}" target="_blank" id="btnCursoMatricular" class="btn-comprar">
										QUERO ME MATRICULAR!
									</a>
								</div>
							</div>

						</div>
					</div>
				@endforeach

			</div>
		</div>
	</div>
</section>

@endsection