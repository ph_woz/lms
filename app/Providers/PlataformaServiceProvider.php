<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Models\Plataforma;
use App\Models\PaginaConteudo;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Request;

class PlataformaServiceProvider extends ServiceProvider
{
    public function boot()
    {
        view()->composer('*', function($view)
        {           
         
            $dominio = Request::getHost();
            $expiresAt = Carbon::now()->addHour();

            $plataforma = Cache::remember('plataforma-dominio-'.$dominio, $expiresAt, function () {
                return Plataforma::dominio()->first();
            });

            if(is_null($plataforma))
            {
                die($this->viewPlataformaNaoEncontrada());
            }

            if($plataforma->status == 1)
            {
                $url = Request::segment(1);
                
                if($url == null)
                {
                    $checkDesativarSite = Cache::remember('plataforma-checkDesativarSite-'.$dominio, $expiresAt, function () {
                        return PaginaConteudo::select('status')->plataforma()->where('url','/')->where('status', 2)->exists();
                    });

                    if($checkDesativarSite == false)
                    {
                        die($this->viewPlataformaDesativada());
                    }

                } else {

                    die($this->viewPlataformaDesativada());
                }
            }

            $nomePlataforma = $plataforma->nome;

            if($plataforma->id !== session('plataforma_id'))
            {
                session(['plataforma_id' => $plataforma->id]);
            }

            $view->with([ 
                'plataforma'      => $plataforma     ?? null,
                'nomePlataforma'  => $nomePlataforma ?? null,
            ]);
        });

    }

    public function viewPlataformaDesativada()
    {
        $view = '

            <!DOCTYPE html>
            <html lang="pt-br">
            <head>

                <meta charset="utf-8">
                <meta http-equiv="X-UA-Compatible" content="IE=edge">
                <meta name="viewport" content="width=device-width, initial-scale=1">

                <title>Plataforma Desativada</title>

                <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
                <link rel="stylesheet" href="/css/util.css">
                <link rel="shortcut icon" href="">

                <meta name="robots" content="noindex, nofollow">

            </head>
            <body class="bg-light">

                <div class="h-100vh">
                    <div class="container h-100">
                        <div class="d-flex justify-content-center align-items-center h-100">
                            <div class="text-center mb-5">
                                
                                <h1 class="display-2">
                                    Plataforma Desativada
                                </h1>
                                <p style="font-size:20px;">
                                    Se você é o gestor desta Plataforma, para maiores informações contacte a sua escola.
                                    <!--
                                    <a href="https://wozcode.com" target="_blank" class="weight-600 text-danger">
                                        WOZCODE
                                    </a>
                                    -->
                                    <br>
                                </p>

                            </div>
                        </div>
                    </div>
                </div>

            </body>
            </html>

        ';

        return $view;
    }

    public function viewPlataformaNaoEncontrada()
    {
        $view = '

            <!DOCTYPE html>
            <html lang="pt-br">
            <head>

                <meta charset="utf-8">
                <meta http-equiv="X-UA-Compatible" content="IE=edge">
                <meta name="viewport" content="width=device-width, initial-scale=1">

                <title>Plataforma não encontrada</title>

                <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
                <link rel="stylesheet" href="/css/util.css">

                <meta name="robots" content="noindex, nofollow">

            </head>
            <body class="bg-light">

                <div class="h-100vh">
                    <div class="container h-100">
                        <div class="d-flex justify-content-center align-items-center h-100">
                            <div class="text-center mb-5">
                                
                                <h1 class="display-2">
                                    Plataforma não encontrada
                                </h1>
                                <p style="font-size:20px;">
                                     Não foi possível identificar a plataforma acessada. Em caso de dúvidas, entre em contato com o suporte
                                    <a href="https://wozcode.com/" target="_blank" class="weight-600 text-danger">
                                        WOZCODE
                                    </a>
                                    <br>
                                </p>

                            </div>
                        </div>
                    </div>
                </div>

            </body>
            </html>

        ';

        return $view;
    }

}