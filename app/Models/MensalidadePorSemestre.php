<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MensalidadePorSemestre extends Model
{

    protected $table = 'mensalidades_por_semestres';

    public $timestamps = false;

    public $fillable = [
        'plataforma_id',
        'registro_id',
        'tipo', // T(trilha) / C(lass)/Turma ...
        'semestre',
        'valor',
    ];

    public static  function boot()
    {
        parent::boot(); 
        static::creating(function ($model)
        {
            $model->plataforma_id  = \Auth()->user()->plataforma_id;
        });
    }

    public function scopePlataforma($query)
    {
        return $query->where('plataforma_id', session('plataforma_id') ?? Plataforma::dominio()->pluck('id')[0] ?? null);
    }

    public function scopeTrilha($query)
    {
        return $query->where('tipo', 'T');
    }
   
    public function scopeTurma($query)
    {
        return $query->where('tipo', 'C');
    }
}
