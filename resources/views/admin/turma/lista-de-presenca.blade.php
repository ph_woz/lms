@extends('layouts.admin.master')
@section('content')

<nav aria-label="breadcrumb" class="mb-4-5 p-3 bg-db rounded ashadow border">
	<ol class="breadcrumb p-0 m-0">
    	<li class="breadcrumb-item"><a href="{{ route('admin.turma.lista') }}" class="color-blue-info-link">Turmas</a></li>
    	<li class="breadcrumb-item"><a href="{{ route('admin.turma.info', ['id' => $turma->id]) }}" class="color-blue-info-link">{{ $turma->nome }}</a></li>
    	<li class="breadcrumb-item text-dark" aria-current="page">Lista de Presença</li>
  	</ol>
</nav>

<div class="mt-5 d-flex justify-content-between">

    <div class="align-self-end mb-2">
        <span class="resultados-encontrados">
        </span>
    </div>

    <div class="mb-3">
        <a href="{{ route('admin.turma.editar.lista-de-presenca-add', ['id' => $turma->id]) }}" class="btn-add aclNivel2">
            + Adicionar
        </a>
    </div>

</div>

<div class="row">
    <div class="col-12">

        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">Data da Aula</th>
                                <th scope="col">
                                    <span class="float-end">
                                        Ações
                                    </span>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($aulas as $aula)
                                <tr>
                                    <td>{{ \App\Models\Util::replaceDatePt($aula) }}</td>
                                    <td>
                                        <span class="float-end">

                                            <a href="/admin/turma/editar/{{$turma->id}}/lista-de-presenca/editar/{{$aula}}" class="btn bg-base text-white">
                                                <i class="fa fa-eye"></i>
                                            </a>

                                        </span>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
</div>

@endsection