@extends('layouts.admin.master')

@section('css')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.14.0-beta2/dist/css/bootstrap-select.min.css">
@endsection

@section('content')

<div>

    <div class="col-12">
        @if (session()->has('success'))
            <div class="alert alert-success alert-dismissible" role="alert">
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                <div class="alert-message">
                    Turma criada:
                    <span>
                        <a href="{{ route('admin.turma.info', ['id' => session('turma_id') ?? null]) }}" class="weight-600">
                            {{ session('turma_nome') }} 
                        </a>
                    </span>
                </div>
            </div>
        @endif
    </div>

    <form method="POST" class="mb-4">
    @csrf

        <section class="mb-4-5">
            
            <a href="#boxInfoPrincipal" data-bs-toggle="collapse" aria-expanded="false" class="hover-d-none">
                <div class="section-title">
                    Informações Gerais
                </div>
            </a>
            <hr/>
            
            <div class="collapse show" id="boxInfoPrincipal">

                <div class="d-lg-flex mb-3">

                    <div class="w-100">
                        <label>Nome</label>
                        <input type="text" name="nome" class="form-control p-form" placeholder="Nome" required>
                    </div>
                    <div class="w-100 mx-lg-3 my-3 my-lg-0">
                        <label>Curso</label>
                        <select name="curso_id" class="selectpicker" title="Selecione" data-width="100%" data-live-search="true" required>
                            @foreach($cursos as $curso)
                                <option value="{{ $curso->id }}">
                                    {{ $curso->referencia }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="w-lg-35">
                        <label>Status</label>
                        <select name="status" class="select-form">
                            <option value="0">Ativo</option>
                            <option value="1">Desativado</option>
                        </select>
                    </div>

                </div>

                <div class="d-lg-flex mb-3">

                    <div class="w-lg-65">
                        <label>Nota mínima para aprovação</label>
                        <input type="text" name="nota_minima_aprovacao" class="form-control p-form" placeholder="Digite o valor da nota">
                    </div>
                    <div class="w-100 mx-lg-3 my-3 my-lg-0">
                        <label>Formulário de Inscrição de Matrícula</label>
                        <select name="formulario_id" class="selectpicker" title="Selecione" data-width="100%" data-live-search="true">
                            <option value="">Selecione</option>
                            @foreach($formularios as $formulario)
                                <option value="{{ $formulario->id }}">
                                    {{ $formulario->referencia }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="w-lg-70">
                        <label>Tipo de Inscrição de Matrícula</label>
                        <select name="tipo_inscricao" class="select-form">
                            <option value="requer-aprovacao">Requer aprovação</option>
                            <option value="auto-inscricao">Auto Inscrição</option>
                        </select>
                    </div>
                    <div class="w-lg-30 ms-lg-3 mt-3 mt-lg-0">
                        <label>Publicar</label>
                        <select name="publicar" onchange="controlBoxApresentacao(this.value)" class="select-form">
                            <option value="N">Não</option>
                            <option value="S">Sim</option>
                        </select>
                    </div>

                </div>
                    
                @if(\App\Models\Unidade::checkActive() === true)
                <div class="mb-3 boxUnidadeRemove">
                    <label>Unidade</label>
                    <select name="unidade_id" id="unidade_id" class="selectpicker" data-width="100%">
                        <option value="">Selecione</option>
                        @foreach($unidades as $unidade)
                            <option value="{{ $unidade->id }}">
                                {{ $unidade->nome }}
                            </option>
                        @endforeach
                    </select>
                </div>
                @endif

                <div id="boxTurmaApresentacao" style="display: none;">

                    <div class="d-lg-flex mb-3">

                        <div class="w-100">
                            <label>Data de Início</label>
                            <input type="date" name="data_inicio" class="form-control p-form">
                        </div>
                        <div class="w-100 mx-lg-3 my-3 my-lg-0">
                            <label>Data de Término</label>
                            <input type="date" name="data_termino" class="form-control p-form">
                        </div>
                        <div class="w-100">
                            <label>Duração</label>
                            <input type="text" name="duracao" class="form-control p-form" placeholder="Ex: 6 meses">
                        </div>
                        <div class="w-100 ms-lg-3 mt-3 mt-lg-0">
                            <label>Modalidade</label>
                            <select name="modalidade" onchange="controlBoxModalidade(this.value)" class="select-form">
                                <option value="">Selecione</option>
                                <option value="EaD">EaD</option>
                                <option value="Presencial">Presencial</option>
                                <option value="Semipresencial">Semipresencial</option>
                                <option value="Presencial/Online/EaD">Presencial/Online/EaD</option>
                            </select>
                        </div>

                    </div>

                    <div id="boxPresencial" style="display:none;">
                        <div class="d-lg-flex mb-3">

                            <div class="w-lg-25 me-lg-3 mb-3 mb-lg-0">
                                <label>Turno</label>
                                <select name="turno" class="select-form">
                                    <option value="">Selecione</option>
                                    <option value="M">Manhã</option>
                                    <option value="T">Tarde</option>
                                    <option value="N">Noturno</option>
                                    <option value="I">Integral</option>
                                </select>
                            </div>

                            <div class="w-100 me-lg-3 mb-3 mb-lg-0">
                                <label>Dias de Aula</label>
                                <select name="frequencia[]" class="selectpicker" data-width="100%" title="Selecione" multiple> 
                                    <option value="Segunda">Segunda-feira</option>
                                    <option value="Terça">Terça-feira</option>
                                    <option value="Quarta">Quarta-feira</option>
                                    <option value="Quinta">Quinta-feira</option>
                                    <option value="Sexta">Sexta-feira</option>
                                    <option value="Sábado">Sábado</option>
                                    <option value="Domingo">Domingo</option>
                                </select>
                            </div>
                            <div class="w-lg-50">
                                <label>Horário</label>
                                <input type="text" name="horario" class="horario form-control p-form" placeholder="00:00 - 00:00">
                            </div>

                        </div>
                    </div>

                </div>

            </div>

        </section>

        <section class="mb-4-5" id="boxSecValores">

            <a href="#boxValores" data-bs-toggle="collapse" aria-expanded="false" class="hover-d-none">
                <div class="section-title">
                    Valores e Forma de Pagamento
                </div>
            </a>
            <hr/>

            <div class="collapse" id="boxValores">

                <div class="d-lg-flex mb-3">

                    <div class="w-lg-75">
                        <label>Tipo de Valor</label>
                        <select name="tipo_valor" onchange="setTipoValor(this.value)" class="select-form">
                            <option value="F">Fechado</option>
                            <option value="M" @if(old('tipo_valor') == 'M') selected @endif>Mensalidade</option>
                        </select>
                    </div>

                    <div class="boxValorFechado w-100 ms-lg-3 mt-3 mt-lg-0">
                        <label>Valor a vista</label>
                        <input type="text" name="valor_a_vista" class="valor form-control p-form" placeholder="R$" value="{{ old('valor_a_vista') }}">
                    </div>
                    <div class="boxValorFechado w-100 ms-lg-3 mt-3 mt-lg-0">
                        <label>Valor a vista promocional</label>
                        <input type="text" name="valor_promocional" class="valor form-control p-form" placeholder="R$" value="{{ old('valor_promocional') }}">
                    </div>
                    <div class="boxValorFechado w-100 ms-lg-3 mt-3 mt-lg-0">
                        <label>Valor parcelado</label>
                        <input type="text" name="valor_parcelado" class="form-control p-form" placeholder="Ex: 3x de R$ 97,99" value="{{ old('valor_parcelado') }}">
                    </div>
                    <div class="boxValorMensalidade w-100 ms-lg-3 mt-3 mt-lg-0" style="display: none;">
                        <label>Valor da Mensalidade</label>
                        <div class="d-flex w-100">
                            <input type="text" name="valor_mensalidade" class="valor form-control p-form rounded-right-0" placeholder="R$" value="{{ old('valor_mensalidade') }}">
                            <select name="tipo_mensalidade" onchange="setBoxProximosSemestre(this.value)" class="w-lg-80 select-form rounded-left-0">
                                <option value="F">Fixo (até a conclusão do curso)</option>
                                <option value="R" @if(old('tipo_mensalidade') == 'R') selected @endif>Primeiro Semestre</option>
                            </select>
                        </div>
                    </div>
                    <div class="w-100 ms-lg-3 mt-3 mt-lg-0">
                        <label>% de desconto com Agilpass</label>
                        <input type="text" name="desconto_agilpass" class="number form-control p-form" placeholder="Desconto em cada pagamento com Agilpass" value="{{ old('desconto_agilpass') }}">
                    </div>

                </div>

                <hr/>

                <div class="w-100 boxValorMensalidade mb-3" id="boxProximosSemestres" style="display: none;">
                    <div class="p-3 rounded" style="border:1.5px solid #dbdbdb;">
                        <div class="mb-1 w-100">
                            <p class="mb-0">Próximos Semestres</p>
                            <hr class="mt-2">
                        </div>
                        <div class="boxLineParcela">
                            <div class="mb-2 d-flex align-items-center">
                                <input type="text" name="valores_por_semestres[]" style="width: 120px;" readonly class="form-control p-form text-center px-0 me-2" value="2° Semestre">
                                <input type="text" name="semestres[]" class="valor form-control p-form" placeholder="R$ (valor da mensalidade deste semestre)">
                                <a href="#" class="btnRemoveSemestre btn btn-danger py-2 ms-2">
                                    <i class="fa fa-trash"></i>
                                </a>
                            </div>
                        </div>
                        <button type="button" class="btn-append btn-append-semestre btn btn-primary py-1-5 fs-13">Adicionar +</button>
                    </div>
                </div>

                <div class="w-100 boxValorFormaIngresso mb-3" id="boxValoresPorFormasDeIngresso">
                    <div class="p-3 rounded" style="border:1.5px solid #dbdbdb;">
                        <div class="mb-1 w-100">
                            <p class="mb-0">Formas de Ingresso</p>
                            <hr class="mt-2">
                        </div>
                        <div class="boxLineFormaIngresso">
                            <div class="mb-2 d-flex align-items-center">
                                <input type="text" name="formas[]" style="width: 180px;" readonly class="form-control p-form me-2" value="Vestibular">
                                <input type="text" name="valores_formas[]" class="valor form-control p-form" placeholder="% a mais do valor de cada pagamento">
                            </div>
                        </div>
                        <div class="boxLineFormaIngresso">
                            <div class="mb-2 d-flex align-items-center">
                                <input type="text" name="formas[]" style="width: 180px;" readonly class="form-control p-form me-2" value="Enem">
                                <input type="text" name="valores_formas[]" class="valor form-control p-form" placeholder="% a mais do valor de cada pagamento">
                            </div>
                        </div>
                        <div class="boxLineFormaIngresso">
                            <div class="mb-2 d-flex align-items-center">
                                <input type="text" name="formas[]" style="width: 180px;" readonly class="form-control p-form me-2" value="Transferência Externa">
                                <input type="text" name="valores_formas[]" class="valor form-control p-form" placeholder="% a mais do valor de cada pagamento">
                            </div>
                        </div>
                        <div class="boxLineFormaIngresso">
                            <div class="mb-2 d-flex align-items-center">
                                <input type="text" name="formas[]" style="width: 180px;" readonly class="form-control p-form me-2" value="Portador de Diploma">
                                <input type="text" name="valores_formas[]" class="valor form-control p-form" placeholder="% a mais do valor de cada pagamento">
                            </div>
                        </div>
                        <div class="boxLineFormaIngresso">
                            <div class="mb-2 d-flex align-items-center">
                                <input type="text" name="formas[]" style="width: 180px;" readonly class="form-control p-form me-2" value="Histórico Escolar">
                                <input type="text" name="valores_formas[]" class="valor form-control p-form" placeholder="% a mais do valor de cada pagamento">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="boxValorFechado row mb-3">

                    <div class="col-lg-4 mb-4">
                        <div class="p-3 rounded" style="border:1.5px solid #dbdbdb;">
                            <div class="mb-1 w-100">
                                <p class="mb-0">Cartão de Crédito</p>
                                <hr class="mt-2">
                            </div>
                            <div class="boxLineParcela">
                                <div class="mb-2 d-flex align-items-center">
                                    <input type="text" name="cartao-de-credito[parcelas][]" class="number form-control p-form" placeholder="Parcela">
                                    <input type="text" style="width: 45px;" disabled readonly class="form-control p-form text-center px-0 mx-2" placeholder="de">
                                    <input type="text" name="cartao-de-credito[valores][]" class="valor form-control p-form" placeholder="R$">
                                    <a href="#" class="btnRemoveParcela btn btn-danger py-2 ms-2">
                                        <i class="fa fa-trash"></i>
                                    </a>
                                </div>
                            </div>
                            <button type="button" class="btn-append btn-append-parcela-cc btn btn-primary py-1-5 fs-13">Adicionar +</button>
                        </div>
                    </div>
                    <div class="col-lg-4 mb-4">
                        <div class="p-3 rounded" style="border:1.5px solid #dbdbdb;">
                            <div class="mb-1 w-100">
                                <p class="mb-0">Cartão de Débito</p>
                                <hr class="mt-2">
                            </div>
                            <div class="boxLineParcela">
                                <div class="mb-2 d-flex align-items-center">
                                    <input type="text" name="cartao-de-debito[parcelas][]" class="number form-control p-form" placeholder="Parcela">
                                    <input type="text" style="width: 45px;" disabled readonly class="form-control p-form text-center px-0 mx-2" placeholder="de">
                                    <input type="text" name="cartao-de-debito[valores][]" class="valor form-control p-form" placeholder="R$">
                                    <a href="#" class="btnRemoveParcela btn btn-danger py-2 ms-2">
                                        <i class="fa fa-trash"></i>
                                    </a>
                                </div>
                            </div>
                            <button type="button" class="btn-append btn-append-parcela-cd btn btn-primary py-1-5 fs-13">Adicionar +</button>
                        </div>
                    </div>
                    <div class="col-lg-4 mb-4">
                        <div class="p-3 rounded" style="border:1.5px solid #dbdbdb;">
                            <div class="mb-1 w-100">
                                <p class="mb-0">Boleto</p>
                                <hr class="mt-2">
                            </div>
                            <div class="boxLineParcela">
                                <div class="mb-2 d-flex align-items-center">
                                    <input type="text" name="boleto[parcelas][]" class="number form-control p-form" placeholder="Parcela">
                                    <input type="text" style="width: 45px;" disabled readonly class="form-control p-form text-center px-0 mx-2" placeholder="de">
                                    <input type="text" name="boleto[valores][]" class="valor form-control p-form" placeholder="R$">
                                    <a href="#" class="btnRemoveParcela btn btn-danger py-2 ms-2">
                                        <i class="fa fa-trash"></i>
                                    </a>
                                </div>
                            </div>
                            <button type="button" class="btn-append btn-append-parcela-bo btn btn-primary py-1-5 fs-13">Adicionar +</button>
                        </div>
                    </div>

                </div>

            </div>

        </section>

        <button type="submit" name="sent" value="ok" class="btn-add mt-3">Cadastrar</button>   

    </form>

</div>    

@endsection

@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.14.0-beta2/dist/js/bootstrap-select.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.js"></script>
<script>

    $(".horario").mask("00:00 - 00:00");

	@if(\Auth::user()->restringir_unidade == 'S' && \Auth::user()->unidade_id != null)
		jQuery("#unidade_id option:contains('Selecione')").remove();
	@endif

    $(document).on("focus", ".valor", function() { 
        $(this).mask('#.##0,00', {
          reverse: true
        });
    });

    $(document).on("focus", ".number", function() { 
        $(this).mask("000");
    });


    $(".btn-append-parcela-cc").click(function()
    {
        $(this).prev().append('<div class="mb-2 d-flex align-items-center"> <input type="text" name="cartao-de-credito[parcelas][]" class="number form-control p-form" placeholder="Parcela"> <input type="text" style="width: 45px;" disabled readonly class="form-control p-form text-center px-0 mx-2" placeholder="de"> <input type="text" name="cartao-de-credito[valores][]" class="valor form-control p-form" placeholder="R$"> <a href="#" class="btnRemoveParcela btn btn-danger py-2 ms-2"> <i class="fa fa-trash"></i> </a> </div> </div>');
    });

    $(".btn-append-parcela-cd").click(function()
    {
        $(this).prev().append('<div class="mb-2 d-flex align-items-center"> <input type="text" name="cartao-de-debito[parcelas][]" class="number form-control p-form" placeholder="Parcela"> <input type="text" style="width: 45px;" disabled readonly class="form-control p-form text-center px-0 mx-2" placeholder="de"> <input type="text" name="cartao-de-debito[valores][]" class="valor form-control p-form" placeholder="R$"> <a href="#" class="btnRemoveParcela btn btn-danger py-2 ms-2"> <i class="fa fa-trash"></i> </a> </div> </div>');
    });

    $(".btn-append-parcela-bo").click(function()
    {
        $(this).prev().append('<div class="mb-2 d-flex align-items-center"> <input type="text" name="boleto[parcelas][]" class="number form-control p-form" placeholder="Parcela"> <input type="text" style="width: 45px;" disabled readonly class="form-control p-form text-center px-0 mx-2" placeholder="de"> <input type="text" name="boleto[valores][]" class="valor form-control p-form" placeholder="R$"> <a href="#" class="btnRemoveParcela btn btn-danger py-2 ms-2"> <i class="fa fa-trash"></i> </a> </div> </div>');
    });

    $(document).on('click', '.btnRemoveParcela', function(e)
    {
        e.preventDefault();
        $(this).parent().remove();
    });

    function controlBoxApresentacao(value)
    {
        if(value == 'S')
        {
            document.getElementById('boxTurmaApresentacao').style.display = 'block';
        
        } else {

            document.getElementById('boxTurmaApresentacao').style.display = 'none';
        }
    }

    function controlBoxModalidade(value)
    {
        if(value == 'EaD' || value == '')
        {
            document.getElementById('boxPresencial').style.display = 'none';
        
        } else {

            document.getElementById('boxPresencial').style.display = 'block';
        }
    }


    let iSemestre = 2;
    $(".btn-append-semestre").click(function()
    {
        iSemestre++;
        $(this).prev().append('<div class="boxLineSemestre"> <div class="mb-2 d-flex align-items-center"> <input type="text" name="semestres[]" style="width: 120px;" readonly class="form-control p-form text-center px-0 me-2" value="'+iSemestre+'° Semestre"> <input type="text" name="valores_por_semestres[]" class="valor form-control p-form" placeholder="R$ (valor da mensalidade deste semestre)" required> <a href="#" class="btnRemoveSemestre btn btn-danger py-2 ms-2"> <i class="fa fa-trash"></i> </a> </div> </div>');
    });

    $(document).on('click', '.btnRemoveSemestre', function(e)
    {
        e.preventDefault();
        $(this).parent().remove();
    });

    function setTipoValor(tipo_valor)
    {
        if(tipo_valor == 'M')
        {
            $('.boxValorFechado').hide();
            $('.boxValorMensalidade').show();
        
        } else {

            $('.boxValorMensalidade').hide();
            $('.boxValorFechado').show();
        }

        $("#boxProximosSemestres").hide();
    }

    function setBoxProximosSemestre(tipo_mensalidade)
    {
        if(tipo_mensalidade == 'R')
        {
            document.getElementById('boxProximosSemestres').style.display = 'block';
        
        } else {

            document.getElementById('boxProximosSemestres').style.display = 'none';
        }
    }

</script>
@endpush