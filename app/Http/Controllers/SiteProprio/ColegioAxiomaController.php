<?php

namespace App\Http\Controllers\SiteProprio;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Banner;
use App\Models\PaginaConteudo;

class ColegioAxiomaController extends Controller
{
    public function index()
    {
        $banners = Banner::plataforma()->ativo()->orderBy('ordem')->get();

        $seo = PaginaConteudo::plataforma()->where('url','/')->select('seo_title','seo_keywords','seo_description')->first();

        if(request('sentContato'))
        {
            if(request('check'))
                return back()->with('danger', 'SPAM Detected!');

            \App\Models\Contato::create(request()->all() + [
                'plataforma_id' => 14,
                'unidade_id' => 3,
                'tipo' => 'L',
            ]);
            
            if(request('assunto') == 'Fale Conosco')
                return redirect('/#contato')->with('success', 'Inscrição enviada com êxito.');
            else
                return redirect('/#pre-matricula')->with('success_prematricula', 'Inscrição enviada com êxito.');
        }

    	return view('sites-proprios.colegio-axioma.home', compact('banners','seo') );
    }
}