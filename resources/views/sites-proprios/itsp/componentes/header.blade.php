 <header>
    
    <div class="sub-header container-fluid px-xl-7 py-2-3">
       <div class="row align-items-center">
          <div class="col-6 col-lg-4 d-xs-none">

             <a href="https://api.whatsapp.com/send?phone=55{{preg_replace('/\D/', '', $plataforma->whatsapp)}}&text=Olá, vim através do Site {{ $plataforma->nome }}." target="_blank" class="btn-submenu d-xs-none">
                <i class="fab fa-whatsapp fs-17"></i>
                <span class="d-xs-none"> 
                  Matricule-se pelo WhatsApp
                </span>
                <span class="d-lg-none ml-1">
                  {{ $plataforma->whatsapp }}
                </span>
             </a>

             <a href="tel:+55{{preg_replace('/\D/', '', $plataforma->telefone)}}" class="d-xs-none btn-submenu">
                <i class="fa fa-phone fs-17"></i>
                {{ $plataforma->telefone }}
             </a>

          </div>

          <div class="col-12 col-lg-8" align="right">

             <a href="https://canaleducacionalonline.com.br/" target="_blank" class="btn-submenu d-xs-none">
                Cursos Profissionalizantes
             </a>             

             <a href="/sobre-nos" class="btn-submenu d-xs-none">
                Institucional
             </a>

             <a href="https://itspensino.com.br/f/trabalhe-conosco/49/t/652" target="_blank" class="btn-submenu d-xs-none">
                Trabalhe Conosco
             </a>

             <a href="https://itspensino.com.br/f/formulario-de-matricula/47/t/650" target="_blank" class="btn-submenu btnPortalAluno d-lg-none">
                Matricule-se
             </a>

             <a href="/login" class="btn-submenu btnPortalAluno">
                Portal do Aluno
             </a>

          </div>

       </div>
    </div>

    <nav class="navbar navbar-expand-lg bg-white box-shadow" id="menu">
       <div class="container-fluid px-xl-7">
           <a class="navbar-brand" href="/#inicio">
             <img src="{{ $plataforma->logotipo }}" width="75" alt="Logotipo {{ $plataforma->nome }}">
           </a>
           <button class="navbar-toggler outline-0 border-0 text-dark" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
             <span class="fa fa-bars"></span>
           </button>
           <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
                   <li class="nav-item">
                      <a class="nav-link" href="/#inicio">HOME</a>
                   </li>
                   <li class="nav-item">
                      <a class="nav-link" href="/#cursos">CURSOS</a>
                   </li>
                   <li class="nav-item">
                      <a class="nav-link" href="/editais">EDITAIS</a>
                   </li>
                   <li class="nav-item">
                      <a class="nav-link d-md-none" href="https://canaleducacionalonline.com.br/" target="_blank">CURSOS PROFISSIONALIZANTES</a>
                   </li>
                   <li class="nav-item">
                      <a class="nav-link" href="https://aeducacao.com.br/saladeaula/" target="_blank">PÓS</a>
                    </li>
                   <li class="nav-item">
                      <a class="nav-link" href="/graduacao">GRADUAÇÃO</a>
                   </li>
                   <li class="nav-item">
                      <a class="nav-link" href="/#contato">
                         CONTATO
                      </a>
                   </li>
                   <li class="nav-item ms-lg-4 mt-3 mt-lg-0">
                     <a class="nav-link btnAreaAluno animation-grow d-inline-block" @if(isset($curso)) href="/f/formulario-de-matricula/47/t/{{$curso->id}}" @else href="https://itspensino.com.br/f/formulario-de-matricula/47/t/650" @endif target="_blank">
                        <i class="fas fa-graduation-cap mr-1"></i>
                        MATRICULE-SE
                     </a>
                   </li>
               </ul>
           </div>
       </div>
    </nav>

 </header>