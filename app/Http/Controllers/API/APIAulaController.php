<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\CursoAula;
use App\Models\AlunoAulaConcluida;
use App\Models\Turma;

class APIAulaController extends Controller
{
    public function getAulaById()
    {
        $aula = CursoAula::select('id','modulo_id','nome','descricao','tipo_objeto','conteudo')->plataforma()->ativo()->where('id', request('aula_id'))->first();

        return response()->json(['aula' => $aula]);
    }

    public function marcarComoConcluida()
    {
        AlunoAulaConcluida::create([
            'aluno_id'  => request('aluno_id'),
            'curso_id'  => request('curso_id'),
            'modulo_id' => request('modulo_id'),
            'aula_id'   => request('aula_id'),
        ]);
           
        $this->atualizaConclusaoCurso(request('curso_id'), request('aluno_id'));
    }

    public function desmarcarComoConcluida()
    {
        AlunoAulaConcluida::plataforma()
            ->where('aluno_id', request('aluno_id'))
            ->where('curso_id', request('curso_id'))
            ->where('aula_id', request('aula_id'))
            ->delete();

        $turmaAluno = \App\Models\TurmaAluno::plataforma()
            ->where('aluno_id', request('aluno_id'))
            ->where('curso_id', request('curso_id'))
            ->update(['data_conclusao_conteudo' => NULL]);
    }

    public function atualizaConclusaoCurso($curso_id, $user_id)
    {
        $turmaAluno = \App\Models\TurmaAluno::plataforma()->where('aluno_id', $user_id)->where('curso_id', $curso_id)->first();

        if(isset($turmaAluno))
        {
            if($turmaAluno->data_conclusao_curso == null || $turmaAluno->data_conclusao_conteudo == null)
            {
                $aulasConcluidas = AlunoAulaConcluida::select('aula_id')->plataforma()->where('aluno_id', $user_id)->where('curso_id', $curso_id)->get()->pluck('aula_id')->toArray();

                $countAulas = CursoAula::select('id')->plataforma()->ativo()->where('curso_id', $curso_id)->count();

                if(count($aulasConcluidas) >= $countAulas)
                { 
                    if($turmaAluno->data_conclusao_conteudo == null)
                    {
                        $turmaAluno->update(['data_conclusao_conteudo' => date('Y-m-d H:i')]);
                    }
                    
                    $nota_minima_aprovacao = Turma::select('nota_minima_aprovacao')->plataforma()->ativo()->where('id', $turmaAluno->turma_id)->pluck('nota_minima_aprovacao')[0] ?? null;

                    if(isset($nota_minima_aprovacao))
                    {
                        $notas = \App\Models\AvaliacaoAluno::select('nota')->plataforma()->finalizada()->where('aluno_id', $user_id)->where('turma_id', $turmaAluno->turma_id)->get()->pluck('nota')->toArray();
                        $nota  = \App\Models\AvaliacaoAluno::staticSomaNotas($notas);

                        if($nota >= floatval($nota_minima_aprovacao))
                        {
                            $turmaAluno->update([
                                'data_conclusao_curso' => date('Y-m-d H:i'),
                                'codigo_certificado'   => \App\Models\TurmaAluno::geraCodigoCertificado($turmaAluno->id)
                            ]);

                            $trilha_id = \App\Models\TrilhaTurma::plataforma()->where('turma_id', $turmaAluno->turma_id)->pluck('trilha_id')[0] ?? null;
                             
                            if($trilha_id)
                            {
                                $alunoTrilha = \App\Models\TrilhaAluno::plataforma()->where('trilha_id', $trilha_id)->where('aluno_id', $user_id)->first();

                                if($alunoTrilha)
                                {
                                    $turmasIdsParaConcluir = \App\Models\TrilhaTurma::plataforma()->where('trilha_id', $trilha_id)->select('turma_id')->get()->pluck('turma_id')->toArray();
                                    $turmasConcluidas = \App\Models\TurmaAluno::plataforma()->where('aluno_id', $user_id)->whereNotNull('data_conclusao_curso')->whereIn('turma_id', $turmasIdsParaConcluir)->count();

                                    if(count($turmasIdsParaConcluir) == $turmasConcluidas) // CONCLUIU A TRILHA
                                    {
                                        $alunoTrilha->update(['data_conclusao' => date('Y-m-d H:i'), 'codigo_certificado' => \App\Models\TrilhaAluno::geraCodigoCertificado($alunoTrilha->id)]);
                                    }
                                }
                            }
                        }

                    } else {

                        $turmaAluno->update([
                            'data_conclusao_curso' => date('Y-m-d H:i'),
                            'codigo_certificado'   => \App\Models\TurmaAluno::geraCodigoCertificado($turmaAluno->id),
                        ]);

                        $trilha_id = \App\Models\TrilhaTurma::plataforma()->where('turma_id', $turmaAluno->turma_id)->pluck('trilha_id')[0] ?? null;

                        if($trilha_id)
                        {
                            $alunoTrilha = \App\Models\TrilhaAluno::plataforma()->where('trilha_id', $trilha_id)->where('aluno_id', $user_id)->first();

                            if($alunoTrilha)
                            {
                                $turmasIdsParaConcluir = \App\Models\TrilhaTurma::plataforma()->where('trilha_id', $trilha_id)->select('turma_id')->get()->pluck('turma_id')->toArray();
                                $turmasConcluidas = \App\Models\TurmaAluno::plataforma()->where('aluno_id', $user_id)->whereNotNull('data_conclusao_curso')->whereIn('turma_id', $turmasIdsParaConcluir)->count();

                                if(count($turmasIdsParaConcluir) == $turmasConcluidas) // CONCLUIU A TRILHA
                                {
                                    $alunoTrilha->update(['data_conclusao' => date('Y-m-d H:i'), 'codigo_certificado' => \App\Models\TrilhaAluno::geraCodigoCertificado($alunoTrilha->id)]);
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}