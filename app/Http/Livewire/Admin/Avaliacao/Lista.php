<?php

namespace App\Http\Livewire\Admin\Avaliacao;

use Livewire\WithPagination;
use Livewire\Component;
use App\Models\Avaliacao;
use App\Models\Curso;
use App\Models\Categoria;
use App\Models\AvaliacaoCategoria;
use App\Models\Turma;

class Lista extends Component
{
    use WithPagination;

    protected $paginationTheme = 'bootstrap';

    public $curso_id;
    public $turmas = [];
    public $turma_id;
    public $search;
    public $status = 0;
    public $ordem;
    public $categoriasSelected = [];

    protected $queryString = ['search','status','ordem','curso_id','turma_id','categoriasSelected'];

    public function updated()
    {
        if($this->curso_id)
        {
            $this->turmas = Turma::select('id','nome')->plataforma()->ativo()->where('curso_id', $this->curso_id)->orderBy('nome')->get();
        }
    }

    public function render()
    {
        $avaliacoes = Avaliacao::select('id','referencia')->plataforma();

        if($this->search)
        {
            $avaliacoes = $avaliacoes->where('referencia', 'like', '%'.$this->search.'%');
        }

        if($this->curso_id)
        {
            $avaliacoes = $avaliacoes->where('curso_id', $this->curso_id);
        }

        if($this->turma_id)
        {
            $avaliacoes = $avaliacoes->where('turma_id', $this->turma_id);
        }

        if($this->categoriasSelected != null)
        {
            $avaliacoesIds = AvaliacaoCategoria::plataforma()->whereIn('categoria_id', $this->categoriasSelected ?? array())->get()->pluck('avaliacao_id')->toArray();
            $avaliacoes = $avaliacoes->whereIn('id', $avaliacoesIds);
        }

        $avaliacoes = $avaliacoes->where('status', $this->status);

        $countAvaliacoes = $avaliacoes->count();


        if($this->ordem == 'desc' || $this->ordem == 'asc')
        {
            $avaliacoes = $avaliacoes->orderBy('id', $this->ordem);
        
        } else {

            $avaliacoes = $avaliacoes->orderBy('referencia');
        }
        

        $avaliacoes = $avaliacoes->paginate(10);

        $cursos = Curso::plataforma()->ativo()->orderBy('referencia')->get();
        $categorias = Categoria::select('id','nome')->plataforma()->ativo()->tipo('avaliacao')->orderBy('nome')->get();

        return view('livewire.admin.avaliacao.lista', [
            'avaliacoes'      => $avaliacoes,
            'countAvaliacoes' => $countAvaliacoes,
            'cursos'          => $cursos,
            'turmas'          => $this->turmas,
            'categorias'      => $categorias,
        ]);
    }
}
