@extends('layouts.admin.master')
@section('css')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.14.0-beta2/dist/css/bootstrap-select.min.css">
<style>
    .btn-action-active { background: #2f6cc4; }
    .btn-action:hover { background: #3b7ddd!important; }
    .sections-contatos { display: none; }
</style>
@endsection
@section('content')

<div class="d-flex align-items-center p-3 bg-white box-shadow rounded-1" style="border: 2px solid #dee2e6!important;">
    <a data-tipo="A" class="btn-action btn-action-active w-100 text-center btn-add py-2-4 fs-15 font-nunito rounded-right-0 weight-600 border-0">
        Alunos
    </a>
    <a data-tipo="C" class="btn-action w-100 text-center btn-add py-2-4 fs-15 font-nunito weight-600 border-0 rounded-left-0">
        Contatos
    </a>
</div>

<hr class="mb-5 mt-4">

<form action="buscar" target="_blank">

<input type="hidden" name="tipo-perfil-aba" id="tipo-perfil-aba" value="A">

@if(\App\Models\Unidade::checkActive() === true)
<div class="mb-4" id="boxUnidades">
    <div class="section-title py-1-5 rounded-bottom-0">
        <div class="d-flex justify-content-between align-items-center">
            <span class="text-light font-nunito fs-14">Unidades</span>
            <div class="d-flex">
                <button type="button" data-bs-target="#boxDadosUnidades" aria-expanded="false" data-bs-toggle="collapse" class="btnResize hover-d-none outline-0 btn btn-light px-2 py-0 border-0 text-dark">
                    <i class="seta rotate fas fa-caret-down"></i>
                </button>
            </div>
        </div>
    </div>
    <div id="boxDadosUnidades" class="collapse p-3 show bg-white box-shadow rounded-bottom">

        <div class="w-100">
            <label>Unidade</label>
            <select name="unidade_id" id="unidade_id" class="select-form">
                <option value="">Selecione</option>
                @foreach($unidades as $unidade)
                    <option value="{{ $unidade->id }}">
                        {{ $unidade->nome }}
                    </option>
                @endforeach
            </select>
        </div>

    </div>
</div>
@endif

<div class="sections-alunos mb-4" id="boxDadosControleFinanceiro">
    <div class="section-title py-1-5 rounded-bottom-0">
        <div class="d-flex justify-content-between align-items-center">
            <span class="text-light font-nunito fs-14">Controle Financeiro</span>
            <div class="d-flex">
                <button type="button" data-bs-target="#boxDadosControleFinanceiro" aria-expanded="false" data-bs-toggle="collapse" class="btnResize hover-d-none outline-0 btn btn-light px-2 py-0 border-0 text-dark">
                    <i class="seta rotate fas fa-caret-down"></i>
                </button>
            </div>
        </div>
    </div>
    <div id="boxDadosControleFinanceiro" class="collapse p-3 show bg-white box-shadow rounded-bottom">

        <div class="d-lg-flex mb-3">

            <div class="w-100 me-lg-3 mb-3 mb-lg-0">
                <label>Referência</label>
                <input type="text" name="fin_nome" class="form-control p-form" placeholder="Nome">
            </div>

            <div class="w-100 me-lg-3 mb-3 mb-lg-0">
                <label>Produto</label>
                <select name="fin_produto_id" class="me-lg-3 mb-3 mb-lg-0 selectpicker" data-width="100%" data-live-search="true">
                    <option value="">Selecione</option>
                    <optgroup label="Trilhas">
                        @foreach($trilhas as $trilha)
                            <option value="T-{{ $trilha->id }}">
                                {{ $trilha->nome }}
                            </option>
                        @endforeach
                    </optgroup>
                    <optgroup label="Turmas">
                        @foreach($turmas as $turma)
                            <option value="C-{{ $turma->id }}">
                                {{ $turma->nome }}
                            </option>
                        @endforeach
                    </optgroup>

                    @if(count($planos) > 0)
                    <optgroup label="Planos">
                        @foreach($planos as $plano)
                            <option value="A-{{ $plano->id }}">
                                {{ $plano->nome }}
                            </option>
                        @endforeach
                    </optgroup>
                    @endif

                    @if(count($servicos) > 0)
                    <optgroup label="Serviços">
                        @foreach($servicos as $servico)
                            <option value="S-{{ $servico->id }}">
                                {{ $servico->nome }}
                            </option>
                        @endforeach
                    </optgroup>
                    @endif

                    @if(count($produtos) > 0)
                    <optgroup label="Produtos">
                        @foreach($produtos as $produto)
                            <option value="P-{{ $produto->id }}">
                                {{ $produto->nome }}
                            </option>
                        @endforeach
                    </optgroup>
                    @endif

                </select>
            </div>

            <div class="w-100 me-lg-3 mb-3 mb-lg-0">
                <label>Vendedor</label>
                <select name="fin_vendedor_id" class="select-form">
                    <option value="">Selecione</option>
                    @foreach($cadastrantes as $vendedor)
                        <option value="{{ $vendedor->id }}">{{ $vendedor->nome }}</option>
                    @endforeach
                </select>
            </div>

            <div class="w-100 me-lg-3 mb-3 mb-lg-0">
                <label>Cadastrante</label>
                <select name="fin_cadastrante_id" class="select-form">
                    <option value="">Selecione</option>
                    @foreach($cadastrantes as $cadastrante)
                        <option value="{{ $cadastrante->id }}">{{ $cadastrante->nome }}</option>
                    @endforeach
                </select>
            </div>

            <div class="w-lg-85">
                <label>Status de Pagamento</label>
                <select name="fin_status_pagamento" class="select-form">
                    <option value="">Todos</option>
                    <option value="A receber">A receber</option>
                    <option value="Recebidos">Recebidos/Pagos</option>
                    <option value="Inadimplentes">Inadimplentes</option>
                    <option value="Estornados">Estornados</option>
                </select>
            </div>

        </div>

        <div class="d-lg-flex mb-3">

            <div class="w-100 me-lg-3 mb-3 mb-lg-0">
                <label>Período de Data de Vencimento</label>
                <div class="d-flex w-100">
                    <input type="date" name="fin_data_vencimento_de" class="form-control p-form rounded-right-0">
                    <input type="date" name="fin_data_vencimento_ate" class="form-control p-form rounded-left-0">
                </div>
            </div>

            <div class="w-100 me-lg-3 mb-3 mb-lg-0">
                <label>Forma de Pagamento</label>
                <select name="fin_forma_pagamento" class="select-form">
                    <option value="">Selecione</option>
                    <option value="Dinheiro">Dinheiro</option>
                    <option value="Cartão de débito">Cartão de débito</option>
                    <option value="Cartão de crédito">Cartão de crédito</option>
                    <option value="Boleto bancário">Boleto bancário</option>
                    <option value="Pix">Pix</option>
                    <option value="TED">TED</option>
                    <option value="DOC">DOC</option>
                </select>
            </div>

            <div class="w-100">
                <label>Link de Pagamento</label>
                <select name="fin_status_link_pagamento" class="select-form">
                    <option value="">Selecione</option>
                    <option value="C">Com Link</option>
                    <option value="S">Sem Link</option>
                </select>
            </div>
        </div>

    </div>
</div>

<div class="mb-4" id="boxCategorias">
    <div class="section-title py-1-5 rounded-bottom-0">
        <div class="d-flex justify-content-between align-items-center">
            <span class="text-light font-nunito fs-14">Categorias</span>
            <div class="d-flex">
                <button type="button" data-bs-target="#boxDadosCategorias" aria-expanded="false" data-bs-toggle="collapse" class="btnResize hover-d-none outline-0 btn btn-light px-2 py-0 border-0 text-dark">
                    <i class="seta rotate fas fa-caret-down"></i>
                </button>
            </div>
        </div>
    </div>
    <div id="boxDadosCategorias" class="collapse p-3 show bg-white box-shadow rounded-bottom">

        <div class="w-100">
            <label>Categoria</label>
            <select name="categoria_id" id="categoria_id" class="selectpicker" data-width="100%" data-live-search="true">
                <option value="">Selecione</option>
                <optgroup label="Categorias de Alunos" class="sections-alunos">
                    @foreach($categoriasA as $categoriaA)
                        <option value="{{ $categoriaA->id }}">{{ $categoriaA->nome }}</option>
                    @endforeach
                </optgroup>
                <optgroup label="Categorias de Contatos" class="sections-contatos">
                    @foreach($categoriasC as $categoriaC)
                        <option value="{{ $categoriaC->id }}">{{ $categoriaC->nome }}</option>
                    @endforeach
                </optgroup>
            </select>
        </div>

    </div>
</div>

<div class="mb-4 sections-contatos" id="boxFormularios">
    <div class="section-title py-1-5 rounded-bottom-0">
        <div class="d-flex justify-content-between align-items-center">
            <span class="text-light font-nunito fs-14">Formulários</span>
            <div class="d-flex">
                <button type="button" data-bs-target="#boxDadosFormularios" aria-expanded="false" data-bs-toggle="collapse" class="btnResize hover-d-none outline-0 btn btn-light px-2 py-0 border-0 text-dark">
                    <i class="seta rotate fas fa-caret-down"></i>
                </button>
            </div>
        </div>
    </div>
    <div id="boxDadosFormularios" class="collapse p-3 show bg-white box-shadow rounded-bottom">

        <div class="w-100">
            <label>Formulário</label>
            <select name="formulario_id" class="select-form">
                <option value="">Selecione</option>
                @foreach($formularios as $formulario)
                    <option value="{{ $formulario->id }}">{{ $formulario->referencia }}</option>
                @endforeach
            </select>
        </div>

    </div>
</div>

<div class="mb-4" id="boxTrilhas">
    <div class="section-title py-1-5 rounded-bottom-0">
        <div class="d-flex justify-content-between align-items-center">
            <span class="text-light font-nunito fs-14">Trilhas</span>
            <div class="d-flex">
                <button type="button" data-bs-target="#boxDadosTrilhas" aria-expanded="false" data-bs-toggle="collapse" class="btnResize hover-d-none outline-0 btn btn-light px-2 py-0 border-0 text-dark">
                    <i class="seta rotate fas fa-caret-down"></i>
                </button>
            </div>
        </div>
    </div>
    <div id="boxDadosTrilhas" class="collapse p-3 show bg-white box-shadow rounded-bottom">

        <div class="d-lg-flex">

            <div class="w-100 me-lg-3 mb-3 mb-lg-0">
                <label>Trilha</label>
                <select name="trilha_id" class="select-form">
                    <option value="">Selecione</option>
                    @foreach($trilhas as $trilha)
                        <option value="{{ $trilha->id }}">{{ $trilha->referencia }}</option>
                    @endforeach
                </select>
            </div>

            <div class="sections-alunos w-100 me-lg-3 mb-3 mb-lg-0">
                <label>Condição de conclusão</label>
                <select name="trilha_tipo" onchange="displayTrilhaPeriodoDeDataDeConclusao(this.value)" class="select-form">
                    <option value="">Todos</option>
                    <option value="C">Que concluíram</option>
                    <option value="NC">Que não concluíram</option>
                </select>
            </div>

            <div class="w-100" id="trilhaPeriodoDeDataDeConclusao" style="display: none;">
                <label>Período de Data de Conclusão</label>
                <div class="d-flex w-100">
                    <input type="date" name="trilha_data_conclusao_de" id="trilha_data_conclusao_de" class="form-control p-form rounded-right-0">
                    <input type="date" name="trilha_data_conclusao_ate" id="trilha_data_conclusao_ate" class="form-control p-form rounded-left-0">
                </div>
            </div>

        </div>

    </div>
</div>

<div class="mb-4" id="boxCursos">
    <div class="section-title py-1-5 rounded-bottom-0">
        <div class="d-flex justify-content-between align-items-center">
            <span class="text-light font-nunito fs-14">Cursos</span>
            <div class="d-flex">
                <button type="button" data-bs-target="#boxDadosCursos" aria-expanded="false" data-bs-toggle="collapse" class="btnResize hover-d-none outline-0 btn btn-light px-2 py-0 border-0 text-dark">
                    <i class="seta rotate fas fa-caret-down"></i>
                </button>
            </div>
        </div>
    </div>
    <div id="boxDadosCursos" class="collapse p-3 show bg-white box-shadow rounded-bottom">

        <div class="d-lg-flex mb-3">

            <div class="w-100 me-lg-3 mb-3 mb-lg-0">
                <label>Curso</label>
                <select name="curso_id" onchange="getTurmasByCursoId(this.value)" class="select-form">
                    <option value="">Selecione</option>
                    @foreach($cursos as $curso)
                        <option value="{{ $curso->id }}">{{ $curso->referencia }}</option>
                    @endforeach
                </select>
            </div>

            <div class="w-100 me-lg-3 mb-3 mb-lg-0" id="cursoPeriodoDeDataDeUltimoAcesso" style="display: none;">
                <label>Período de Data de Último Acesso do Conteúdo</label>
                <div class="d-flex w-100">
                    <input type="date" name="curso_data_ultimo_acesso_de" id="curso_data_ultimo_acesso_de" class="form-control p-form rounded-right-0">
                    <input type="date" name="curso_data_ultimo_acesso_ate" id="curso_data_ultimo_acesso_ate" class="form-control p-form rounded-left-0">
                </div>
            </div>

            <div class="sections-alunos w-100">
                <label>Condição de conclusão</label>
                <select name="curso_tipo" onchange="displayCursoPeriodoDeDataDeConclusao(this.value)" class="select-form">
                    <option value="">Todos</option>
                    <option value="C">Que concluíram</option>
                    <option value="NC">Que não concluíram</option>
                </select>
            </div>

            <div class="w-100 ms-lg-3 mt-3 mt-lg-0" id="cursoPeriodoDeDataDeConclusao" style="display: none;">
                <label>Período de Data de Conclusão</label>
                <div class="d-flex w-100">
                    <input type="date" name="curso_data_conclusao_de" id="curso_data_conclusao_de" class="form-control p-form rounded-right-0">
                    <input type="date" name="curso_data_conclusao_ate" id="curso_data_conclusao_ate" class="form-control p-form rounded-left-0">
                </div>
            </div>

        </div>

    </div>
</div>

<div class="mb-4" id="boxTurmas">
    <div class="section-title py-1-5 rounded-bottom-0">
        <div class="d-flex justify-content-between align-items-center">
            <span class="text-light font-nunito fs-14">Turmas</span>
            <div class="d-flex">
                <button type="button" data-bs-target="#boxDadosTurmas" aria-expanded="false" data-bs-toggle="collapse" class="btnResize hover-d-none outline-0 btn btn-light px-2 py-0 border-0 text-dark">
                    <i class="seta rotate fas fa-caret-down"></i>
                </button>
            </div>
        </div>
    </div>
    <div id="boxDadosTurmas" class="collapse p-3 show bg-white box-shadow rounded-bottom">

        <div class="w-100">
            <label>Turma</label>
            <select name="turma_id" id="select_turma_id" onchange="getAvaliacoesByTurmaId(this.value)" class="select-form rounded-right-0">
                <option value="">Selecione</option>
            </select>
        </div>

    </div>
</div>

<!--
<div class="sections-alunos mb-4" id="boxAvaliacoes">
    <div class="section-title py-1-5 rounded-bottom-0">
        <div class="d-flex justify-content-between align-items-center">
            <span class="text-light font-nunito fs-14">Avaliações</span>
            <div class="d-flex">
                <button type="button" data-bs-target="#boxDadosAvaliacoes" aria-expanded="false" data-bs-toggle="collapse" class="btnResize hover-d-none outline-0 btn btn-light px-2 py-0 border-0 text-dark">
                    <i class="seta rotate fas fa-caret-down"></i>
                </button>
            </div>
        </div>
    </div>
    <div id="boxDadosAvaliacoes" class="collapse p-3 show bg-white box-shadow rounded-bottom">

        <div class="d-lg-flex mb-3">

            <div class="w-100 me-lg-3 mb-3 mb-lg-0">
                <label>Avaliação</label>
                <select name="avaliacao_id" id="select_avaliacao_id" class="select-form">
                    <option value="">Selecione</option>
                </select>
            </div>

            <div class="w-100 me-lg-3 mb-3 mb-lg-0">
                <label>Condição de Status</label>
                <select name="avaliacao_tipo" class="select-form">
                    <option value="">Todos</option>
                    <option value="C">Que finalizaram</option>
                    <option value="NC">Que não finalizaram</option>
                </select>
            </div>

            <div class="w-100">
                <label>Nota</label>
                <div class="d-flex">
                    <select name="nota_avaliacao_tamanho" class="select-form rounded-right-0">
                        <option value="">Selecione</option>
                        <option value="nota_maior_que">Maior ou igual a</option>
                        <option value="nota_menor_que">Menor ou igual a</option>
                    </select>
                    <select name="nota_avaliacao" class="select-form rounded-left-0">
                        <option value="">Selecione a Nota</option>
                        @foreach(\App\Models\Avaliacao::listaNotas() as $nota)
                            <option value="{{ $nota }}">{{ $nota }}</option>
                        @endforeach
                    </select>
                </div>
            </div>

        </div>

    </div>
</div>
-->

<div class="sections-alunos mb-4" id="boxJuridico">
    <div class="section-title py-1-5 rounded-bottom-0">
        <div class="d-flex justify-content-between align-items-center">
            <span class="text-light font-nunito fs-14">Jurídico</span>
            <div class="d-flex">
                <button type="button" data-bs-target="#boxDadosJuridico" aria-expanded="false" data-bs-toggle="collapse" class="btnResize hover-d-none outline-0 btn btn-light px-2 py-0 border-0 text-dark">
                    <i class="seta rotate fas fa-caret-down"></i>
                </button>
            </div>
        </div>
    </div>
    <div id="boxDadosJuridico" class="collapse p-3 show bg-white box-shadow rounded-bottom">

        <div class="d-lg-flex mb-3">

            <div class="w-100 me-lg-3 mb-3 mb-lg-0">
                <label>Documentação</label>
                <div class="d-flex">
                    <select name="juridico_status_doc" onchange="juridicoPeriodoDeDataDeDocsRecebidas(this.value)" class="select-form rounded-right-0">
                        <option value="">Selecione</option>
                        <option value="C">Que já enviaram todos os documentos</option>
                        <option value="NC">Que ainda não enviaram todos os documentos</option>
                    </select>
                </div>
            </div>

            <div class="w-100" id="juridicoPeriodoDeDataDeDocsRecebidas" style="display: none;">
                <label>Período de Data de Conclusão de envio de toda a Documentação</label>
                <div class="d-flex w-100">
                    <input type="date" name="data_docs_recebidas_de" id="data_docs_recebidas_de" class="form-control p-form rounded-right-0">
                    <input type="date" name="data_docs_recebidas_ate" id="data_docs_recebidas_ate" class="form-control p-form rounded-left-0">
                </div>
            </div>

            <!--
            <div class="w-100">
                <label>Contrato</label>
                <div class="d-flex">
                    <select name="status_contrato" class="select-form rounded-right-0">
                        <option value="">Selecione</option>
                        <option value="">Que ainda não enviaram o contrato assinado</option>
                        <option value="">Que já enviaram o contrato assinado</option>
                    </select>
                </div>
            </div>
            -->

        </div>

    </div>
</div>
 
<div class="sections-alunos mb-4" id="boxDadosCadastrais">
    <div class="section-title py-1-5 rounded-bottom-0">
        <div class="d-flex justify-content-between align-items-center">
            <span class="text-light font-nunito fs-14">Dados Cadastrais de Alunos</span>
            <div class="d-flex">
                <button type="button" data-bs-target="#boxDadosCollapseCadastrais" aria-expanded="false" data-bs-toggle="collapse" class="btnResize hover-d-none outline-0 btn btn-light px-2 py-0 border-0 text-dark">
                    <i class="seta rotate fas fa-caret-down"></i>
                </button>
            </div>
        </div>
    </div>
    <div id="boxDadosCollapseCadastrais" class="collapse p-3 show bg-white box-shadow rounded-bottom">

        <div class="d-lg-flex mb-3">

            <div class="w-100">
                <label>Nome</label>
                <input type="text" name="nome" class="form-control p-form" placeholder="Nome">
            </div>
            <div class="w-100 mx-lg-3 my-3 my-lg-0">
                <label>Email</label>
                <input type="email" name="email" class="form-control p-form" placeholder="Email">
            </div>

            <div class="w-lg-45">
                <label>Status de Matrícula</label>
                <select name="status" class="select-form">
                    <option value="0">Ativos</option>
                    <option value="1">Desativados</option>
                </select>
            </div>

        </div>

        <div class="d-lg-flex mb-3">

            <div class="w-100">
                <label>CPF</label>
                <input type="text" name="cpf" class="cpf form-control p-form" placeholder="000.000.000-00" minlength="14" maxlength="14">
            </div>
            <div class="w-100 mx-lg-3 my-3 my-lg-0">
                <label>Data de Nascimento</label>
                <input type="date" name="data_nascimento" class="form-control p-form">
            </div>
            <div class="w-100">
                <label>Celular</label>
                <input type="text" name="celular" class="phone form-control p-form" placeholder="(00) 00000-0000" minlength="15">
            </div>
            <div class="w-100 ms-lg-3 mt-3 mt-lg-0">
                <label>Telefone</label>
                <input type="text" name="telefone" class="phone form-control p-form" placeholder="(00) 0000-0000">
            </div>

        </div>

        <div class="d-lg-flex mb-3">

            <div class="w-100">
                <label>Matrícula</label>
                <input type="text" name="matricula" class="form-control p-form" placeholder="N° de Matrícula">
            </div>

            <div class="w-100 mx-lg-3 my-3 my-lg-0">
                <label>RG</label>
                <input type="text" name="rg" class="form-control p-form" placeholder="Registro Geral">
            </div>

            <div class="w-100">
                <label>Gênero</label>
                <select name="genero" class="select-form">
                    <option value="">Selecione</option>
                    <option value="M">Masculino</option>
                    <option value="F">Feminino</option>
                </select>
            </div>

            <div class="w-100 ms-lg-3 mt-3 mt-lg-0">
                <label>Profissão</label>
                <input type="text" name="profissao" class="form-control p-form" placeholder="Profissão">
            </div>

        </div>

        <div class="mb-3">
            <label>Observações</label>
            <textarea name="obs" class="textarea-form" placeholder="Observações"></textarea>
        </div>

        <div class="d-lg-flex mb-3">
            
            <div class="w-lg-50">
                <label>CEP</label>
                <input type="text" name="cep" id="cep" class="cep form-control p-form" placeholder="CEP" minlength="8" maxlength="9" onkeyup="pesquisacep(this.value);">
            </div>
            <div class="w-100 mx-lg-3 my-3 my-lg-0">
                <label>Rua</label>
                <input type="text" name="rua" id="rua" class="form-control p-form" placeholder="Rua">
            </div>
            <div class="w-lg-30">
                <label>Estado</label>
                <input type="text" name="estado" id="estado" class="form-control p-form" placeholder="UF" minlength="2" maxlength="2" onkeyup="this.value = this.value.toUpperCase();">
            </div>
            <div class="w-100 mx-lg-3 my-3 my-lg-0">
                <label>Bairro</label>
                <input type="text" name="bairro" id="bairro" class="form-control p-form" placeholder="Bairro">
            </div>
            <div class="w-100">
                <label>Cidade</label>
                <input type="text" name="cidade" id="cidade" class="form-control p-form" placeholder="Cidade">
            </div>
        </div>


        <div class="mb-3 d-lg-flex">

            <div class="w-lg-30">
                <label>Nº</label>
                <input type="text" name="numero" id="numero" class="form-control p-form" placeholder="Nº">
            </div>
            <div class="w-lg-50 mx-lg-3 my-3 my-lg-0">
                <label>Complemento</label>
                <input type="text" name="complemento" id="complemento" class="form-control p-form" placeholder="Complemento">
            </div>
            <div class="w-lg-80">
                <label>Ponto de Referência</label>
                <input type="text" name="ponto_referencia" id="ponto_referencia" class="form-control p-form" placeholder="Ponto de Referência">
            </div>

        </div>

        <div class="mb-3 d-lg-flex">

            <div class="w-100 me-lg-3 mb-3 mb-lg-0">
                <label>Período de Data de Cadastro</label>
                <div class="d-flex w-100">
                    <input type="date" name="data_cadastro_de" class="form-control p-form rounded-right-0">
                    <input type="date" name="data_cadastro_ate" class="form-control p-form rounded-left-0">
                </div>
            </div>

            <div class="w-100 me-lg-3 mb-3 mb-lg-0">
                <label>Período de Data de Último acesso</label>
                <div class="d-flex w-100">
                    <input type="date" name="data_ultimo_acesso_de" class="form-control p-form rounded-right-0">
                    <input type="date" name="data_ultimo_acesso_ate" class="form-control p-form rounded-left-0">
                </div>
            </div>

            <div class="w-100">
                <label>Que não acessam há mais de X dias</label>
                <input type="text" name="dias_sem_acessar" class="number form-control p-form" placeholder="Dias">
            </div>

        </div>

        <div class="mb-3 d-lg-flex">

            <div class="w-100 me-lg-3 mb-3 mb-lg-0">
                <label>Mês de Nascimento</label>
                <select name="mes_nascimento" class="select-form">
                    <option value="">Selecione</option>
                    <option value="01">Janeiro</option>
                    <option value="02">Fevereiro</option>
                    <option value="03">Março</option>
                    <option value="04">Abril</option>
                    <option value="05">Maio</option>
                    <option value="06">Junho</option>
                    <option value="07">Julho</option>
                    <option value="08">Agosto</option>
                    <option value="09">Setembro</option>
                    <option value="10">Outubro</option>
                    <option value="11">Novembro</option>
                    <option value="12">Dezembro</option>
                </select>
            </div>

            <div class="w-100 me-lg-3 mb-3 mb-lg-0">
                <label>Período de Faixa Etária</label>
                <div class="d-flex w-100">
                    <input type="text" name="faixa_etaria_de" class="number form-control p-form rounded-right-0" placeholder="De">
                    <input type="text" name="faixa_etaria_ate" class="number form-control p-form rounded-left-0" placeholder="Até">
                </div>
            </div>

            <div class="w-100">
                <label>Período de Dia de Nascimento</label>
                <div class="d-flex w-100">
                    <input type="text" name="dia_nascimento_de" class="number form-control p-form rounded-right-0" placeholder="Dia 01">
                    <input type="text" name="dia_nascimento_ate" class="number form-control p-form rounded-left-0" placeholder="Até 31">
                </div>
            </div>

        </div>

   </div>

</div>

<div class="sections-contatos mb-4" id="boxDadosCadastraisContatos">
    <div class="section-title py-1-5 rounded-bottom-0">
        <div class="d-flex justify-content-between align-items-center">
            <span class="text-light font-nunito fs-14">Dados Cadastrais de Contatos</span>
            <div class="d-flex">
                <button type="button" data-bs-target="#boxDadosCollapseCadastrais" aria-expanded="false" data-bs-toggle="collapse" class="btnResize hover-d-none outline-0 btn btn-light px-2 py-0 border-0 text-dark">
                    <i class="seta rotate fas fa-caret-down"></i>
                </button>
            </div>
        </div>
    </div>
    <div id="boxDadosCollapseCadastrais" class="collapse p-3 show bg-white box-shadow rounded-bottom">

        <div class="d-lg-flex mb-3">

            <div class="w-lg-45 me-lg-3 mb-3 mb-lg-0">
                <label>Tipo</label>
                <select name="contatos_tipo" class="select-form">
                    <option value="L">Leads</option>
                    <option value="S">Suporte</option>
                </select>
            </div>

            <div class="w-100 me-lg-3 mb-3 mb-lg-0">
                <label>Status de Atendimento</label>
                <select name="contatos_status" class="select-form">
                    <option value="">Selecione</option>
                    <option value="0">Pendente</option>
                    <option value="1">Em análise</option>
                    <option value="2">Atendido</option>
                    <option value="3">Desativado</option>
                </select>
            </div>

            <div class="w-100 me-lg-3 mb-3 mb-lg-0">
                <label>Condição de Perfil</label>
                <select name="contatos_condicao_perfil" class="select-form">
                    <option value="">Convertidos e Não Convertidos</option>
                    <option value="C">Apenas os que Converteram em Aluno</option>
                    <option value="NC">Apenas os que Não Converteram em Aluno</option>
                </select>
            </div>

            <div class="w-lg-60">
                <label>Arquivados</label>
                <select name="contatos_arquivado" class="select-form">
                    <option value="N">Não</option>
                    <option value="S">Sim</option>
                </select>
            </div>

        </div>

        <div class="d-lg-flex mb-3">

            <div class="w-100 me-lg-3 mb-3 mb-lg-0">
                <label>Assunto</label>
                <input type="text" name="contatos_assunto" class="form-control p-form" placeholder="Assunto">
            </div>
            <div class="w-100 me-lg-3 mb-3 mb-lg-0">
                <label>Nome</label>
                <input type="text" name="contatos_nome" class="form-control p-form" placeholder="Nome">
            </div>
            <div class="w-100 me-lg-3 mb-3 mb-lg-0">
                <label>Email</label>
                <input type="email" name="contatos_email" class="form-control p-form" placeholder="Email">
            </div>

            <div class="w-100">
                <label>Gênero</label>
                <select name="contatos_genero" class="select-form">
                    <option value="">Selecione</option>
                    <option value="M">Masculino</option>
                    <option value="F">Feminino</option>
                </select>
            </div>

        </div>

        <div class="d-lg-flex mb-3">

            <div class="w-100">
                <label>CPF</label>
                <input type="text" name="contatos_cpf" class="cpf form-control p-form" placeholder="000.000.000-00" minlength="14" maxlength="14">
            </div>
            <div class="w-100 mx-lg-3 my-3 my-lg-0">
                <label>Data de Nascimento</label>
                <input type="date" name="contatos_data_nascimento" class="form-control p-form">
            </div>
            <div class="w-100">
                <label>Celular</label>
                <input type="text" name="contatos_celular" class="phone form-control p-form" placeholder="(00) 00000-0000" minlength="15">
            </div>
            <div class="w-100 ms-lg-3 mt-3 mt-lg-0">
                <label>Telefone</label>
                <input type="text" name="contatos_telefone" class="phone form-control p-form" placeholder="(00) 0000-0000">
            </div>

        </div>

        <div class="mb-3">
            <label>Mensagem</label>
            <textarea name="contatos_obs" class="textarea-form"></textarea>
        </div>

        <div class="d-lg-flex mb-3">
            
            <div class="w-lg-50">
                <label>CEP</label>
                <input type="text" name="contatos_cep" id="cep" class="cep form-control p-form" placeholder="CEP" minlength="8" maxlength="9" onkeyup="pesquisacep(this.value);">
            </div>
            <div class="w-100 mx-lg-3 my-3 my-lg-0">
                <label>Rua</label>
                <input type="text" name="contatos_rua" id="rua" class="form-control p-form" placeholder="Rua">
            </div>
            <div class="w-lg-30">
                <label>Estado</label>
                <input type="text" name="contatos_estado" id="estado" class="form-control p-form" placeholder="UF" minlength="2" maxlength="2" onkeyup="this.value = this.value.toUpperCase();">
            </div>
            <div class="w-100 mx-lg-3 my-3 my-lg-0">
                <label>Bairro</label>
                <input type="text" name="contatos_bairro" id="bairro" class="form-control p-form" placeholder="Bairro">
            </div>
            <div class="w-100">
                <label>Cidade</label>
                <input type="text" name="contatos_cidade" id="cidade" class="form-control p-form" placeholder="Cidade">
            </div>
        </div>


        <div class="mb-3 d-lg-flex">

            <div class="w-lg-30">
                <label>Nº</label>
                <input type="text" name="contatos_numero" id="numero" class="form-control p-form" placeholder="Nº">
            </div>
            <div class="w-lg-50 mx-lg-3 my-3 my-lg-0">
                <label>Complemento</label>
                <input type="text" name="contatos_complemento" id="complemento" class="form-control p-form" placeholder="Complemento">
            </div>
            <div class="w-lg-80">
                <label>Ponto de Referência</label>
                <input type="text" name="contatos_ponto_referencia" id="ponto_referencia" class="form-control p-form" placeholder="Ponto de Referência">
            </div>

        </div>

        <div class="mb-3 d-lg-flex">

            <div class="w-100">
                <label>Período de Data de Cadastro</label>
                <div class="d-flex w-100">
                    <input type="date" name="contatos_data_cadastro_de" class="form-control p-form rounded-right-0">
                    <input type="date" name="contatos_data_cadastro_ate" class="form-control p-form rounded-left-0">
                </div>
            </div>

        </div>

        <div class="mb-3 d-lg-flex">

            <div class="w-100 me-lg-3 mb-3 mb-lg-0">
                <label>Mês de Nascimento</label>
                <select name="contatos_mes_nascimento" class="select-form">
                    <option value="">Selecione</option>
                    <option value="01">Janeiro</option>
                    <option value="02">Fevereiro</option>
                    <option value="03">Março</option>
                    <option value="04">Abril</option>
                    <option value="05">Maio</option>
                    <option value="06">Junho</option>
                    <option value="07">Julho</option>
                    <option value="08">Agosto</option>
                    <option value="09">Setembro</option>
                    <option value="10">Outubro</option>
                    <option value="11">Novembro</option>
                    <option value="12">Dezembro</option>
                </select>
            </div>

            <div class="w-100 me-lg-3 mb-3 mb-lg-0">
                <label>Período de Faixa Etária</label>
                <div class="d-flex w-100">
                    <input type="text" name="contatos_faixa_etaria_de" class="number form-control p-form rounded-right-0" placeholder="De">
                    <input type="text" name="contatos_faixa_etaria_ate" class="number form-control p-form rounded-left-0" placeholder="Até">
                </div>
            </div>

            <div class="w-100">
                <label>Período de Dia de Nascimento</label>
                <div class="d-flex w-100">
                    <input type="text" name="contatos_dia_nascimento_de" class="number form-control p-form rounded-right-0" placeholder="Dia 01">
                    <input type="text" name="contatos_dia_nascimento_ate" class="number form-control p-form rounded-left-0" placeholder="Até 31">
                </div>
            </div>

        </div>

   </div>

</div>

<button type="button" data-bs-toggle="modal" data-bs-target="#ModalCamposAlunos" class="btn-add sections-alunos mt-3">Continuar</button>
<button type="button" data-bs-toggle="modal" data-bs-target="#ModalCamposContatos" class="btn-add sections-contatos mt-3">Continuar</button>

<div class="modal fade" data-bs-backdrop="static" data-bs-keyboard="false" id="ModalCamposAlunos" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4>
                    Selecione os Dados do Relatório
                    <span class="fs-14 d-block mt-2 font-nunito weight-600">
                        No máximo 5 campos
                    </span>
                </h4>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <select name="campos[]" class="selectpicker" id="select-campos" data-width="100%" title="Selecione" data-live-search="true" multiple>

                    <optgroup label="Dados Cadastrais de Alunos" id="optGroupCadastroAluno">
                        <option value="users.nome">Nome</option>
                        <option value="users.email">Email</option>
                        <option value="users.cpf">CPF</option>
                        <option value="users.id">Matrícula</option>
                        <option value="users.rg">RG</option>
                        <option value="users.genero">Gênero</option>
                        <option value="users.profissao">Profissão</option>
                        <option value="users.obs">Observações</option>
                        <option value="users_status">Status de Matrícula</option>
                        <option value="users.celular">Celular</option>
                        <option value="users.telefone">Telefone</option>
                        <option value="users_idade">Idade</option>
                        <option value="users_data_nascimento">Data de Nascimento</option>
                        <option value="users_data_cadastro">Data de Cadastro</option>
                        <option value="users_data_ultimo_acesso">Data de Último Acesso</option>
                        <option value="users_data_docs_recebidas">Data de Conclusão de envio de Documentação</option>
                        <option value="cadastrante_aluno">Cadastrante do Aluno</option>
                        <option value="categoria_aluno">Categorias do Aluno</option>
                    </optgroup>

                    <optgroup label="Dados de Endereço de Alunos" id="optGroupEnderecoAluno">
                        <option value="endereco.cep">CEP</option>
                        <option value="endereco.rua">Rua</option>
                        <option value="endereco.numero">Número</option>
                        <option value="endereco.bairro">Bairro</option>
                        <option value="endereco.cidade">Cidade</option>
                        <option value="endereco.estado">Estado</option>
                        <option value="endereco.complemento">Complemento</option>
                        <option value="endereco.ponto_referencia">Ponto de Referência</option>
                    </optgroup>

                    <optgroup label="Dados de Trilhas" id="optGroupTrilha">
                        <option value="trilha_referencia">Trilha</option>
                        <option value="data_matricula_trilha">Data de Matrícula de Trilha</option>
                        <option value="data_conclusao_trilha">Data de Conclusão de Trilha</option>
                    </optgroup>

                    <optgroup label="Dados de Cursos (Para liberar estes campos, é necessário escolher um Curso no filtro)" id="optGroupCurso" disabled>
                        <option value="curso_referencia">Curso</option>
                        <option value="users_data_conclusao_curso">Data de Conclusão de Curso</option>
                        <option value="data_ultimo_acesso_curso">Data de Último Acesso do Conteúdo do Curso</option>
                    </optgroup>

                    <optgroup label="Dados de Turmas (Para liberar estes campos, é necessário escolher uma Turma no filtro)" id="optGroupTurma">
                        <option value="turma_referencia">Turma</option>
                        <option value="data_matricula_turma">Data de Matrícula de Turma</option>
                    </optgroup>

                    <!--
                    <optgroup label="Dados de Avaliações" id="optGroupAvaliacao">
                        <option value="avaliacao_referencia">Avaliação</option>
                        <option value="avaliacoes_alunos.nota">Nota de Avaliação</option>
                        <option value="avaliacoes_alunos.n_tentativa">N° de Tentativas feitas na Avaliação</option>
                        <option value="data_avaliacao_finalizada">Data de Avaliação Finalizada</option>
                    </optgroup>
                    -->

                    <optgroup label="Dados de Responsável" id="optGroupResponsavel">
                        <option value="responsaveis_nome">Nome do Responsável</option>
                        <option value="responsaveis_cpf">CPF do Responsável</option>
                        <option value="responsaveis_rg">RG do Responsável</option>
                        <option value="responsaveis_celular">Celular do Responsável</option>
                        <option value="responsaveis_email">Email do Responsável</option>
                        <option value="responsaveis_data_nascimento">Data de Nascimento do Responsável</option>
                        <option value="responsaveis_profissao">Profissão do Responsável</option>
                        <option value="responsaveis_cep">CEP do Responsável</option>
                        <option value="responsaveis_rua">Rua do Responsável</option>
                        <option value="responsaveis_numero">Número do Responsável</option>
                        <option value="responsaveis_bairro">Bairro do Responsável</option>
                        <option value="responsaveis_cidade">Cidade do Responsável</option>
                        <option value="responsaveis_estado">Estado do Responsável</option>
                        <option value="responsaveis_complemento">Complemento do Responsável</option>
                        <option value="responsaveis_ponto_referencia">Ponto de Referência do Responsável</option>
                    </optgroup>

                    <optgroup label="Dados de Controle Financeiro" id="optGroupFinanceiro">
                        <option value="fluxo_caixa.valor_parcela">Valor de Parcela</option>
                        <option value="fluxo_caixa.valor_total">Valor Total</option>
                        <option value="fluxo_caixa_n_parcela">Parcelas</option>
                        <option value="fluxo_caixa.produto_nome">Produto</option>
                        <option value="fluxo_caixa.cadastrante_nome">Cadastrante do Financeiro</option>
                        <option value="fluxo_caixa.vendedor_nome">Vendedor</option>
                        <option value="fluxo_caixa.forma_pagamento">Forma de Pagamento</option>
                        <option value="fluxo_caixa_status">Status de Pagamento</option>
                        <option value="fluxo_caixa.link">Link de Pagamento</option>
                        <option value="fluxo_caixa_data_vencimento">Data de Vencimento</option>
                        <option value="fluxo_caixa_data_recebimento">Data de Recebimento</option>
                        <option value="fluxo_caixa_data_cadastro">Data de Cadastro</option>
                        <option value="fluxo_caixa_data_estorno">Data de Estorno</option>
                        <option value="fluxo_caixa.obs">Observações internas</option>
                    </optgroup>

                    <optgroup label="Dados de Unidades" id="optGroupAlunoUnidade">
                        <option value="unidade_referencia">Unidade</option>
                    </optgroup>

                </select>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary weight-600 py-2">Confirmar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" data-bs-backdrop="static" data-bs-keyboard="false" id="ModalCamposContatos" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4>
                    Selecione os Dados do Relatório
                    <span class="fs-14 d-block mt-2 font-nunito weight-600">
                        No máximo 5 campos
                    </span>
                </h4>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <select name="camposContatos[]" class="selectpicker" id="select-campos-contatos" data-width="100%" title="Selecione" data-live-search="true" multiple>

                    <optgroup label="Dados Cadastrais de Contatos" id="optGroupCadastroContato">
                        <option value="contatos.assunto">Assunto</option>
                        <option value="contatos.nome">Nome</option>
                        <option value="contatos.email">Email</option>
                        <option value="contatos.cpf">CPF</option>
                        <option value="contatos.genero">Gênero</option>
                        <option value="contatos.mensagem">Mensagem</option>
                        <option value="contatos_status">Status de Atendimento</option>
                        <option value="contatos.celular">Celular</option>
                        <option value="contatos.telefone">Telefone</option>
                        <option value="contatos_idade">Idade</option>
                        <option value="contatos_data_nascimento">Data de Nascimento</option>
                        <option value="contatos_data_cadastro">Data de Cadastro</option>
                        <option value="contatos.anotacao">Anotações</option>
                        <option value="categoria_contato">Categorias do Contato</option>
                    </optgroup>

                    <optgroup label="Dados de Endereço de Contatos" id="optGroupEnderecoContato">
                        <option value="contatos.cep">CEP</option>
                        <option value="contatos.rua">Rua</option>
                        <option value="contatos.numero">Número</option>
                        <option value="contatos.bairro">Bairro</option>
                        <option value="contatos.cidade">Cidade</option>
                        <option value="contatos.estado">Estado</option>
                        <option value="contatos.complemento">Complemento</option>
                        <option value="contatos.ponto_referencia">Ponto de Referência</option>
                    </optgroup>

                    <optgroup label="Dados de Formulários" id="optGroupFormularioContato">
                        <option value="formulario_referencia">Formulário</option>
                    </optgroup>

                    <optgroup label="Dados de Trilhas" id="optGroupTrilhaContato">
                        <option value="trilha_referencia">Trilha</option>
                    </optgroup>

                    <optgroup label="Dados de Cursos" id="optGroupContatoCurso">
                        <option value="curso_referencia">Curso</option>
                    </optgroup>

                    <optgroup label="Dados de Turmas (Para liberar estes campos, é necessário escolher uma Turma no filtro)" id="optGroupTurmaContato">
                        <option value="turma_referencia">Turma</option>
                    </optgroup>

                    <optgroup label="Dados de Unidades" id="optGroupContatoUnidade">
                        <option value="unidade_referencia">Unidade</option>
                    </optgroup>

                </select>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary weight-600 py-2">Confirmar</button>
            </div>
        </div>
    </div>
</div>

</form>

@endsection

@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/gh/farhadmammadli/bootstrap-select@main/js/bootstrap-select.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.js"></script>
<script>

    $(".number").mask('000');

    $(".cpf").mask('000.000.000-00');
    $(".cep").mask('00000-000');

    @if(\Auth::user()->restringir_unidade == 'S' && \Auth::user()->unidade_id != null)
        jQuery("#unidade_id option:contains('Selecione')").remove();
    @endif

    $(".btn-action").click(function(e) {

        e.preventDefault();
        tipo = $(this).data('tipo');

        $("#tipo-perfil-aba").val(tipo);

        console.log(tipo);

        if(tipo == 'A')
        {
            $("#categoria_id").val('');
            $("#categoria_id").selectpicker("refresh");

            $(".sections-contatos").hide();
            $(".sections-alunos").show();
        } else {

            $("#categoria_id").val('');
            $("#categoria_id").selectpicker("refresh");

            $(".sections-alunos").hide();
            $(".sections-contatos").show();
        }

        $(".btn-action").removeClass('btn-action-active');
        $(this).addClass('btn-action-active');
    });

    function juridicoPeriodoDeDataDeDocsRecebidas(value)
    {
        if(value == 'C')
        {
            document.getElementById('juridicoPeriodoDeDataDeDocsRecebidas').style.display = 'block';

        } else {

            document.getElementById('juridicoPeriodoDeDataDeDocsRecebidas').style.display = 'none';

            document.getElementById('data_docs_recebidas_de').value = '';
            document.getElementById('data_docs_recebidas_ate').value = '';
        }
    }

    function displayTrilhaPeriodoDeDataDeConclusao(value)
    {
        if(value == 'C')
        {
            document.getElementById('trilhaPeriodoDeDataDeConclusao').style.display = 'block';

        } else {

            document.getElementById('trilhaPeriodoDeDataDeConclusao').style.display = 'none';

            document.getElementById('trilha_data_conclusao_de').value = '';
            document.getElementById('trilha_data_conclusao_ate').value = '';
        }
    }

    function displayCursoPeriodoDeDataDeConclusao(value)
    {
        if(value == 'C')
        {
            document.getElementById('cursoPeriodoDeDataDeConclusao').style.display = 'block';

        } else {

            document.getElementById('cursoPeriodoDeDataDeConclusao').style.display = 'none';

            document.getElementById('curso_data_conclusao_de').value = '';
            document.getElementById('curso_data_conclusao_ate').value = '';
        }
    }

    function getTurmasByCursoId(curso_id)
    {
        if(curso_id != '')
        {
            document.getElementById('cursoPeriodoDeDataDeUltimoAcesso').style.display = 'block';

            $("#optGroupCurso").attr('disabled',false);
            $('#select-campos').selectpicker('refresh');

        } else {

            document.getElementById('cursoPeriodoDeDataDeUltimoAcesso').style.display = 'none';

            document.getElementById('curso_data_ultimo_acesso_de').value = '';
            document.getElementById('curso_data_ultimo_acesso_ate').value = '';

            $("#optGroupCurso").prop('disabled', true);
            $('#select-campos').selectpicker('refresh');
        }


        $("#select_turma_id").empty().append('<option value="">Selecione</option>');

        fetch('{{ route('api-util.get-turmas-by-curso_id') }}?curso_id='+curso_id)
        .then(response => response.text())
        .then(res => {
           try {
              
              data = JSON.parse(res);

              data.forEach((item) => {
                appendData(item, 'select_turma_id');
              });
        
           } catch (error) {
              
              console.log(error);
           }   
      
        });
    }

    function getAvaliacoesByTurmaId(turma_id)
    {
        $("#select_avaliacao_id").empty().append('<option value="">Selecione</option>');

        fetch('{{ route('api-util.get-avaliacoes-by-turma_id') }}?turma_id='+turma_id)
        .then(response => response.text())
        .then(res => { 
           try {
              
              data = JSON.parse(res);

              data.forEach((item) => {
                appendData(item, 'select_avaliacao_id');
              });
        
           } catch (error) {
              
              console.log(error);
           }   
      
        });
    }


    function appendData(data, select)
    {
        let row = `
            <option value="${data.id}">${data.referencia}</option>
        `;

        $("#"+select).append(row);
    }

</script>

<script>

    function meu_callback(conteudo) {

        if (!("erro" in conteudo)) {
        
            document.getElementById('rua').value=(conteudo.logradouro);
            document.getElementById('bairro').value=(conteudo.bairro);
            document.getElementById('cidade').value=(conteudo.localidade);
            document.getElementById('estado').value=(conteudo.uf);
            document.getElementById('numero').focus();

        } else {

            document.getElementById('rua').value=("");
            document.getElementById('bairro').value=("");
            document.getElementById('cidade').value=("");
            document.getElementById('estado').value=("");
        }
    
    }
        
    function pesquisacep(valor) {

        var cep = valor.replace(/\D/g, '');

        if (cep != "" && cep.length >= 8) {

            var validacep = /^[0-9]{8}$/;

            document.getElementById('rua').value="...";
            document.getElementById('bairro').value="...";
            document.getElementById('cidade').value="...";
            document.getElementById('estado').value="...";

            var script = document.createElement('script');
            script.src = 'https://viacep.com.br/ws/'+ cep + '/json/?callback=meu_callback';
            document.body.appendChild(script);
        } 
        
    }

</script>
@endpush