<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Http\Request;
use App\Models\Plataforma;
use App\Models\Util;
use App\Models\User;
use App\Models\Trilha;

class EmailContratacaoTrilha extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    public $user;
    public $trilha;

    public function __construct(User $user, Trilha $trilha)
    {
        $this->user = $user;
        $this->trilha = $trilha;
    }

    /**
     * Build the message.
     *
     * @return $this
     */

    public function generateToken()
    {
        return md5(rand(1, 10) . microtime());
    }

    public function replaceVariaveis($descricao)
    {
        $plataforma = Plataforma::plataforma()->first();

        $descricao = str_replace('[NOME]',      $this->user->nome,      $descricao);
        $descricao = str_replace('[EMAIL]',     $this->user->email,     $descricao);
        $descricao = str_replace('[CPF]',       $this->user->cpf,       $descricao);
        $descricao = str_replace('[MATRICULA]', $this->user->matricula, $descricao);
        $descricao = str_replace('[TRILHA]',    $this->trilha->nome,    $descricao);

        $descricao = str_replace('[PLATAFORMA_NOME]', $plataforma->nome, $descricao);
        $descricao = str_replace('[PLATAFORMA_LINK]', \Request::root().'/login', $descricao);

        return $descricao;
    }

    public function build()
    {
        $plataforma = Plataforma::plataforma()->first();

        $descricao  = $this->replaceVariaveis(request('email_contratacao_trilha') ?? $plataforma->email_contratacao_trilha);

        $statusBtnRedefinirSenha = false;
        if(strpos($descricao, '[BOTAO_REDEFINIR_SENHA]') !== false) {

            $descricao = str_replace('[BOTAO_REDEFINIR_SENHA]', '', $descricao);
            $token = $this->generateToken();
            $this->user->update(['token' => $token]);
            $statusBtnRedefinirSenha = true;
        }

        $categoria  = 'Matriculado!';
        $assunto    = $categoria .' | '. $plataforma->nome;
        return $this->view('mail.contratacao')
                ->subject($assunto)
                ->from($plataforma->email, $plataforma->nome)
                ->replyTo($plataforma->email, $plataforma->nome)
                ->with([
                    'descricao' => $descricao,
                    'token'     => $this->user->token, 
                    'email'     => $this->user->email, 
                    'id'        => $this->user->id,
                    'botaoRedefinirSenha' => $statusBtnRedefinirSenha,
                ]);
    }
}
