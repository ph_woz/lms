<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TrilhaCategoria extends Model
{

    protected $table = 'trilhas_categorias';

    public $timestamps = false;

    protected $fillable = [ 'plataforma_id', 'categoria_id', 'trilha_id', ];

    public function getPlataforma()
    {
        return $this->belongsTo(Plataforma::class, 'plataforma_id');
    }

    public static  function boot()
    {
        parent::boot(); 
        static::creating(function ($model)
        {
            $model->plataforma_id = session('plataforma_id') ?? \Auth()->user()->plataforma_id ?? Plataforma::dominio()->pluck('id')[0] ?? null;
        });
    }

    public function scopePlataforma($query)
    {
        return $query->where('plataforma_id', session('plataforma_id') ?? Plataforma::dominio()->pluck('id')[0] ?? null);
    }


}
