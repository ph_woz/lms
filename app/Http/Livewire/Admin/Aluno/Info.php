<?php

namespace App\Http\Livewire\Admin\Aluno;

use Livewire\Component;
use Illuminate\Support\Facades\Storage;
use App\Models\User;
use App\Models\UserEndereco;
use App\Models\UserCategoria;
use App\Models\Unidade;
use App\Models\Categoria;
use App\Models\Curso;
use App\Models\Turma;
use App\Models\Trilha;
use App\Models\TrilhaAluno;
use App\Models\TrilhaTurma;
use App\Models\TurmaAluno;
use App\Models\Plataforma;
use App\Models\Log;
use App\Models\Documento;
use App\Models\ComunicadoAluno;
use App\Models\Comunicado;
use App\Models\AclPlataformaModuloUser;
use App\Models\UserContrato;
use App\Models\Contrato;
use App\Models\FluxoCaixa;
use App\Models\ContratoCampo;
use DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Http;

class Info extends Component
{
    public $user;
    public $cadastrante;
    public $endereco;
    public $unidade;
    public $categorias;

    public $turmas = [];

    public $matricular_aluno_curso_id;
    public $matricular_aluno_turma_id;
    public $matricular_aluno_turma_enviar_email;
    public $email_contratacao_turma;
    public $allContratos = [];
    public $contratos = [];
    public $contrato_id;
    public $camposContrato = [];

    public $matricular_aluno_trilha_id;
    public $matricular_aluno_trilha_enviar_email;
    public $email_contratacao_trilha;

    public $forcar_conclusao_trilha_id;
    public $forcar_conclusao_turma_id;
    public $forcar_conclusao_data_conclusao;
    public $forcar_conclusao_data_colacao_grau;
    public $forcar_conclusao_codigo_certificado;

    public $remover_aluno_trilha_id;
    public $remover_aluno_turma_id;

    public $desativar_acesso_trilha_id;
    public $ativar_acesso_trilha_id;

    public $desativar_acesso_turma_id;
    public $ativar_acesso_turma_id;

    public $password_to_delete;

    public $resetar_avaliacao_id = null;

    public $desvincular_responsavel_id;
    public $desvincular_responsavel_nome;

    public $comunicado_assunto;
    public $comunicado_descricao;

    public $deletar_entrada_id = null;

    public $documentos   = [];
    public $entradas     = [];
    public $responsaveis = [];
    public $comunicados  = [];
    public $avaliacoesByTurmaId = [];

    public function updated()
    {
        if($this->matricular_aluno_curso_id)
        {
            $this->turmas = Turma::select('id','nome')->plataforma()->ativo()->where('curso_id', $this->matricular_aluno_curso_id)->orderBy('nome')->get();
        }

        if($this->contrato_id)
        {
            $this->camposContrato = ContratoCampo::plataforma()->where('contrato_id', $this->contrato_id)->whereNotIn('campo', ['Assinatura Contratado','Assinatura Contratante'])->get();
        }
    }

    public function mount($user_id)
    {
        $this->user = User::plataforma()->find($user_id);

        $this->cadastrante = User::select('id','nome')->plataforma()->where('id', $this->user->cadastrante_id ?? null)->first();
        
        $this->unidade = Unidade::select('id','nome')->plataforma()->where('id', $this->user->unidade_id ?? null)->first();

        $modeloDeEmails = Plataforma::select('email_contratacao_trilha','email_contratacao_turma')->plataforma()->first();
        $this->email_contratacao_turma  = $modeloDeEmails->email_contratacao_turma;
        $this->email_contratacao_trilha = $modeloDeEmails->email_contratacao_trilha;

        $this->endereco = UserEndereco::plataforma()->where('user_id', $this->user->id ?? null)->select('cep','rua','numero','bairro','cidade','estado','complemento','ponto_referencia')->first() ?? array();

        if($this->endereco)
        {
            $this->endereco = $this->endereco->toArray();
        }

        $categoriasIds = UserCategoria::plataforma()->where('user_id', $this->user->id ?? null)->get()->pluck('categoria_id')->toArray();
        $this->categorias = Categoria::select('nome')->plataforma()->ativo()->whereIn('id', $categoriasIds)->orderBy('nome')->get();
    }

    public function render()
    {
        if($this->user == null)
        {
            $erro = 'Aluno(a) não encontrado.';
            return view('admin.page-whoops', ['erro' => $erro]);
        }

        $cursos = Curso::select('id','referencia')->plataforma()->ativo()->orderBy('nome')->get();

        $turmasMatriculado = DB::table('turmas_alunos')
            ->select(
                'turmas.id',
                'turmas.nome',
                'turmas.curso_id',
                'turmas.nota_minima_aprovacao',
                'turmas_alunos.id as turma_aluno_id',
                'turmas_alunos.data_conclusao_curso',
                'turmas_alunos.status',
            )
            ->join('turmas','turmas_alunos.turma_id','turmas.id')
            ->where('turmas_alunos.plataforma_id', session('plataforma_id'))
            ->where('turmas_alunos.aluno_id', $this->user->id)
            ->orderBy('turmas.id')
            ->get();


        $trilhas = Trilha::select('id','referencia')->plataforma()->ativo()->orderBy('id')->get();

        $trilhasMatriculado = DB::table('trilhas_alunos')
            ->select(
                'trilhas.id',
                'trilhas.certificado_id',
                'trilhas.referencia',
                'trilhas_alunos.data_conclusao',
                'trilhas_alunos.status',
            )
            ->join('trilhas','trilhas_alunos.trilha_id','trilhas.id')
            ->where('trilhas_alunos.plataforma_id', session('plataforma_id'))
            ->where('trilhas_alunos.aluno_id', $this->user->id)
            ->orderBy('trilhas.id')
            ->get();

        return view('livewire.admin.aluno.info', 
            [
                'cursos'                => $cursos             ?? array(), 
                'turmasMatriculado'     => $turmasMatriculado  ?? array(),
                'trilhasMatriculado'    => $trilhasMatriculado ?? array(),
                'trilhas'               => $trilhas            ?? array(),
            ] 
        );
    }


    public function getAllContratos()
    {
        $this->allContratos = Contrato::select('id','referencia')->plataforma()->ativo()->orderBy('referencia')->get();

        /*
        if(\Auth::user()->restringir_unidade == 'S' && \Auth::user()->unidade_id != null)
        {
            $this->allContratos = Contrato::select('id','referencia')->plataforma()->ativo()->where('unidade_id', \Auth::user()->unidade_id)->orderBy('referencia')->get();

        } else {

            $this->allContratos = Contrato::select('id','referencia')->plataforma()->ativo()->orderBy('referencia')->get();
        }
        */
    }

    public function getComunicados()
    {
        $this->responsaveis = [];
        $this->contratos    = [];
        $this->documentos   = [];

        $this->comunicados = DB::table('comunicados')
            ->join('comunicados_alunos','comunicados.id','comunicados_alunos.comunicado_id')
            ->where('comunicados.plataforma_id', session('plataforma_id'))
            ->where('comunicados_alunos.plataforma_id', session('plataforma_id'))
            ->where('comunicados_alunos.aluno_id', $this->user->id)
            ->select(
                'comunicados.id',
                'comunicados.assunto',
                'comunicados_alunos.data_visualizacao',
            )
            ->orderBy('id','desc')
            ->get();
    }

    public function getResponsaveis()
    {
        $this->comunicados = [];

        $this->responsaveis = \App\Models\Responsavel::
            select(
                'responsaveis.id',
                'responsaveis.nome',
                'responsaveis_alunos.grau_parentesco'
            )
            ->join('responsaveis_alunos', 'responsaveis.id', 'responsaveis_alunos.responsavel_id')
            ->where('responsaveis_alunos.aluno_id', $this->user->id)
            ->where('responsaveis.plataforma_id', session('plataforma_id'))
            ->where('responsaveis_alunos.plataforma_id', session('plataforma_id'))
            ->get();
    }

    public function getEntradas()
    {
        $this->entradas = FluxoCaixa::select('id','produto_nome','valor_parcela','status','data_vencimento')->plataforma()->where('cliente_id', $this->user->id)->orderBy('data_vencimento')->get();
    }

    public function getDocumentos()
    {
        $this->responsaveis = [];
        $this->comunicados  = [];
        $this->contratos    = [];
      
        $this->documentos = Documento::select('id','cadastrante_id','nome','link','created_at')->plataforma()->where('user_id', $this->user->id)->orderBy('id','desc')->get();
    }

    public function getContratos()
    {
        $this->responsaveis = [];
        $this->comunicados  = [];
        $this->documentos   = [];

        $this->contratos = UserContrato::plataforma()->where('user_id', $this->user->id)->orderBy('id','desc')->get();
    }

    public function getAvaliacoesByTurmaId($turma_id)
    {
        $this->avaliacoesByTurmaId = DB::select( DB::raw("
            SELECT
                avaliacoes.id,
                avaliacoes.referencia,
                avaliacoes_alunos.nota
            FROM
                avaliacoes
            INNER JOIN avaliacoes_alunos ON
                avaliacoes.id = avaliacoes_alunos.avaliacao_id
            WHERE
                    avaliacoes.status = 0
                AND avaliacoes.plataforma_id = ".$this->user->plataforma_id."
                AND avaliacoes.turma_id = ".$turma_id."
                AND avaliacoes_alunos.aluno_id = ".$this->user->id."
                AND avaliacoes_alunos.status = 'F';
        "));
    }

    public function enviarComunicado()
    {
        $comunicado = Comunicado::create([
            'assunto'   => $this->comunicado_assunto,
            'descricao' => $this->comunicado_descricao,
            'arquivar'  => 'S',
        ]);

        ComunicadoAluno::create([
            'comunicado_id' => $comunicado->id,
            'aluno_id'      => $this->user->id,
        ]);

        session()->flash('success-livewire', 'Comunicado enviado com êxito.');

        $this->dispatchBrowserEvent('dissmiss-modal');
        $this->dispatchBrowserEvent('toast');
    }

    public function setRemoverAlunoDaTurmaId($turma_id)
    {
        $this->remover_aluno_turma_id = $turma_id;
    }

    public function setRemoverAlunoDaTrilhaId($trilha_id)
    {
        $this->remover_aluno_trilha_id = $trilha_id;
    }

    public function setForcarConclusaoDaTrilhaId($trilha_id, $alunoTrilhaId)
    {
        $this->forcar_conclusao_trilha_id = $trilha_id;
    
        $this->forcar_conclusao_data_conclusao = date('Y-m-d');
        $this->forcar_conclusao_data_colacao_grau = date('Y-m-d');

        $this->forcar_conclusao_codigo_certificado = \App\Models\TrilhaAluno::geraCodigoCertificado($alunoTrilhaId);
    }

    public function setForcarConclusaoDaTurmaId($turma_id, $alunoTurmaId)
    {
        $this->forcar_conclusao_turma_id = $turma_id;

        $this->forcar_conclusao_data_conclusao = date('Y-m-d');
        $this->forcar_conclusao_data_colacao_grau = date('Y-m-d');

        $this->forcar_conclusao_codigo_certificado = \App\Models\TurmaAluno::geraCodigoCertificado($alunoTurmaId);
    }

    public function setAtivarAcessoDaTrilhaId($trilha_id)
    {
        $this->ativar_acesso_trilha_id = $trilha_id;
    }

    public function setDesativarAcessoDaTrilhaId($trilha_id)
    {
        $this->desativar_acesso_trilha_id = $trilha_id;
    }

    public function setAtivarAcessoDaTurmaId($turma_id)
    {
        $this->ativar_acesso_turma_id = $turma_id;
    }

    public function setDeletarEntradaId($entrada_id)
    {
        $this->deletar_entrada_id = $entrada_id;
    }

    public function setDesativarAcessoDaTurmaId($turma_id)
    {
        $this->desativar_acesso_turma_id = $turma_id;
    }

    public function deletarEntradaFinanceira()
    {
        if(\Auth::attempt(['id' => \Auth::id(), 'password' => $this->password_to_delete, 'status' => 0]))
        {
            $entrada = FluxoCaixa::plataforma()->entrada()->where('id', $this->deletar_entrada_id)->first();

            $deleteAndReturnCountDeleteds = 0;

            if($entrada->registro_id == null)
            {
                $deleteAndReturnCountDeleteds = FluxoCaixa::plataforma()->entrada()->where('registro_id', $entrada->id)->delete();
            }

            Log::create([
                'tipo' => 'fluxo-caixa',
                'ref'  => 'Entrada do Fluxo de Caixa deletada. | ' . $entrada->cliente_nome . ' | ' . $entrada->produto_nome . ' | ' . $entrada->valor_parcela . ' | ' . $deleteAndReturnCountDeleteds . ' parcelas futuras afetadas.',
            ]);

            $entrada->delete();

            $this->deletar_entrada_id = null;

            return redirect(route('admin.aluno.info', ['id' => $this->user->id]))->with('success','Entrada Financeira do Aluno deletada com êxito.');

        } else {

            session()->flash('danger-livewire', 'Senha incorreta.');

            $this->dispatchBrowserEvent('toast');
        } 
    }

    public function ativarAcessoDaTrilha()
    {
        if(\Auth::attempt(['id' => \Auth::id(), 'password' => $this->password_to_delete, 'status' => 0]))
        {
            TrilhaAluno::plataforma()->where('aluno_id', $this->user->id)->where('trilha_id', $this->ativar_acesso_trilha_id)->update(['status' => 0]); 

            $turmasIds = TrilhaTurma::plataforma()->where('trilha_id', $this->ativar_acesso_trilha_id)->get()->pluck('turma_id')->toArray();

            $turmasAlunos = TurmaAluno::plataforma()->whereIn('turma_id', $turmasIds)->where('aluno_id', $this->user->id)->get();

            foreach($turmasAlunos as $turmaAluno)
            {
                $turmaAluno->update(['status' => 0]);
            }

            session()->flash('success-livewire', 'Acesso da trilha ativado com êxito.');

            $this->dispatchBrowserEvent('dissmiss-modal');
            $this->dispatchBrowserEvent('toast');

        } else {

            session()->flash('danger-livewire', 'Senha incorreta.');

            $this->dispatchBrowserEvent('toast');
        }  
    }

    public function desativarAcessoDaTrilha()
    {
        if(\Auth::attempt(['id' => \Auth::id(), 'password' => $this->password_to_delete, 'status' => 0]))
        {
            TrilhaAluno::plataforma()->where('aluno_id', $this->user->id)->where('trilha_id', $this->desativar_acesso_trilha_id)->update(['status' => 1]); 

            $turmasIds = TrilhaTurma::plataforma()->where('trilha_id', $this->desativar_acesso_trilha_id)->get()->pluck('turma_id')->toArray();

            $turmasAlunos = TurmaAluno::plataforma()->whereIn('turma_id', $turmasIds)->where('aluno_id', $this->user->id)->get();

            foreach($turmasAlunos as $turmaAluno)
            {
                $turmaAluno->update(['status' => 1]);
            }

            session()->flash('success-livewire', 'Acesso da trilha desativado com êxito.');

            $this->dispatchBrowserEvent('dissmiss-modal');
            $this->dispatchBrowserEvent('toast');

        } else {

            session()->flash('danger-livewire', 'Senha incorreta.');

            $this->dispatchBrowserEvent('toast');
        }  
    }

    public function ativarAcessoDaTurma()
    {
        if(\Auth::attempt(['id' => \Auth::id(), 'password' => $this->password_to_delete, 'status' => 0]))
        {
            TurmaAluno::plataforma()->where('aluno_id', $this->user->id)->where('turma_id', $this->ativar_acesso_turma_id)->update(['status' => 0]); 

            session()->flash('success-livewire', 'Acesso da turma ativado com êxito.');

            $this->dispatchBrowserEvent('dissmiss-modal');
            $this->dispatchBrowserEvent('toast');

        } else {

            session()->flash('danger-livewire', 'Senha incorreta.');

            $this->dispatchBrowserEvent('toast');
        }  
    }

    public function desativarAcessoDaTurma()
    {
        if(\Auth::attempt(['id' => \Auth::id(), 'password' => $this->password_to_delete, 'status' => 0]))
        {
            TurmaAluno::plataforma()->where('aluno_id', $this->user->id)->where('turma_id', $this->desativar_acesso_turma_id)->update(['status' => 1]); 

            session()->flash('success-livewire', 'Acesso da turma desativado com êxito.');

            $this->dispatchBrowserEvent('dissmiss-modal');
            $this->dispatchBrowserEvent('toast');

        } else {

            session()->flash('danger-livewire', 'Senha incorreta.');

            $this->dispatchBrowserEvent('toast');
        }  
    }

    public function removerAlunoDaTurma()
    {
        if(\Auth::attempt(['id' => \Auth::id(), 'password' => $this->password_to_delete, 'status' => 0]))
        {
            $turmaAluno = TurmaAluno::plataforma()->where('aluno_id', $this->user->id)->where('turma_id', $this->remover_aluno_turma_id)->first(); 

            \App\Models\AlunoAulaConcluida::plataforma()->where('aluno_id', $this->user->id)->where('curso_id', $turmaAluno->curso_id)->delete(); 
            \App\Models\AvaliacaoAluno::plataforma()->where('aluno_id', $this->user->id)->where('turma_id', $this->remover_aluno_turma_id)->delete();
            \App\Models\AvaliacaoAlunoResposta::plataforma()->where('aluno_id', $this->user->id)->where('turma_id', $this->remover_aluno_turma_id)->delete();

            $turmaAluno->delete();

            session()->flash('success-livewire', 'Aluno removido da turma com êxito.');

            $this->dispatchBrowserEvent('dissmiss-modal');
            $this->dispatchBrowserEvent('toast');

        } else {

            session()->flash('danger-livewire', 'Senha incorreta.');

            $this->dispatchBrowserEvent('toast');
        }
    }

    public function removerAlunoDaTrilha()
    {
        if(\Auth::attempt(['id' => \Auth::id(), 'password' => $this->password_to_delete, 'status' => 0]))
        {
            TrilhaAluno::plataforma()->where('aluno_id', $this->user->id)->where('trilha_id', $this->remover_aluno_trilha_id)->delete(); 

            $turmasIds = TrilhaTurma::plataforma()->where('trilha_id', $this->remover_aluno_trilha_id)->get()->pluck('turma_id')->toArray();

            $turmasAlunos = TurmaAluno::plataforma()->whereIn('turma_id', $turmasIds)->where('aluno_id', $this->user->id)->get();

            \App\Models\AvaliacaoAluno::plataforma()->where('aluno_id', $this->user->id)->whereIn('turma_id', $turmasIds)->delete();
            \App\Models\AvaliacaoAlunoResposta::plataforma()->where('aluno_id', $this->user->id)->whereIn('turma_id', $turmasIds)->delete();

            foreach($turmasAlunos as $turmaAluno)
            {
                \App\Models\AlunoAulaConcluida::plataforma()->where('aluno_id', $this->user->id)->where('curso_id', $turmaAluno->curso_id)->delete(); 
                $turmaAluno->delete();
            }

            session()->flash('success-livewire', 'Aluno removido da trilha com êxito.');

            $this->dispatchBrowserEvent('dissmiss-modal');
            $this->dispatchBrowserEvent('toast');
        
        } else {

            session()->flash('danger-livewire', 'Senha incorreta.');

            $this->dispatchBrowserEvent('toast');
        }
    }

    public function forcarConclusaoDaTrilha()
    {
        if(\Auth::attempt(['id' => \Auth::id(), 'password' => $this->password_to_delete, 'status' => 0]))
        {
            $alunoTrilha = TrilhaAluno::plataforma()
                ->where('aluno_id', $this->user->id)
                ->where('trilha_id', $this->forcar_conclusao_trilha_id)
                ->first();

            $alunoTrilha->update([
                'codigo_certificado' => $this->forcar_conclusao_codigo_certificado,
                'data_conclusao'     => $this->forcar_conclusao_data_conclusao,
                'data_colacao_grau'  => $this->forcar_conclusao_data_colacao_grau,
            ]);

            $trilha_nome = Trilha::select('referencia')->where('id', $this->forcar_conclusao_trilha_id)->value('referencia');

            \App\Models\Log::create([
                'tipo'  => 'trilha',
                'ref'   => 'Aluno ' . $this->user->nome . ' ('.$this->user->email .' - ' . $this->user->celular.')' . ' teve a Trilha ('.$trilha_nome.') concluída forçadamente.',
            ]);

            session()->flash('success-livewire', 'Conclusão forçada da trilha realizada com êxito.');

            $this->dispatchBrowserEvent('dissmiss-modal');
            $this->dispatchBrowserEvent('toast');
        
        } else {

            session()->flash('danger-livewire', 'Senha incorreta.');

            $this->dispatchBrowserEvent('toast');
        }   
    }

    public function forcarConclusaoDaTurma()
    {
        if(\Auth::attempt(['id' => \Auth::id(), 'password' => $this->password_to_delete, 'status' => 0]))
        {
            $alunoTurma = TurmaAluno::plataforma()
                ->where('aluno_id', $this->user->id)
                ->where('turma_id', $this->forcar_conclusao_turma_id)
                ->first();

            $alunoTurma->update([
                'data_conclusao_curso' => $this->forcar_conclusao_data_conclusao,
                'data_colacao_grau'    => $this->forcar_conclusao_data_colacao_grau,
                'codigo_certificado'   => $this->forcar_conclusao_codigo_certificado,
            ]);

            $turma_nome = Turma::select('nome')->where('id', $this->forcar_conclusao_turma_id)->value('nome');

            \App\Models\Log::create([
                'tipo'  => 'trilha',
                'ref'   => 'Aluno ' . $this->user->nome . ' ('.$this->user->email .' - ' . $this->user->celular.')' . ' teve a Turma ('.$turma_nome.') concluída forçadamente.',
            ]);

            session()->flash('success-livewire', 'Conclusão forçada da turma realizada com êxito.');

            $this->dispatchBrowserEvent('dissmiss-modal');
            $this->dispatchBrowserEvent('toast');
        
        } else {

            session()->flash('danger-livewire', 'Senha incorreta.');

            $this->dispatchBrowserEvent('toast');
        }   
    }

    public function setResetarAvaliacaoId($avaliacao_id)
    {
        $this->avaliacoesByTurmaId = [];
        $this->resetar_avaliacao_id = $avaliacao_id;
    }

    public function resetarAvaliacao()
    {
        if(\Auth::attempt(['id' => \Auth::id(), 'password' => $this->password_to_delete, 'status' => 0]))
        {
            \App\Models\AvaliacaoAluno::plataforma()->where('aluno_id', $this->user->id)->where('avaliacao_id', $this->resetar_avaliacao_id)->delete();
            \App\Models\AvaliacaoAlunoResposta::plataforma()->where('aluno_id', $this->user->id)->where('avaliacao_id', $this->resetar_avaliacao_id)->delete();

            $aluno = User::plataforma()->aluno()->where('id', $this->user->id)->first();

            $acoes_avaliacao_nome = \App\Models\Avaliacao::select('referencia')->plataforma()->where('id', $this->resetar_avaliacao_id)->pluck('referencia')[0] ?? 'Não encontrado';

            \App\Models\Log::create([
                'tipo'  => 'avaliacao',
                'ref'   => 'Aluno ' . $aluno->nome . ' ('.$aluno->email .' - ' . $aluno->celular.')' . ' teve sua avaliação (' . $acoes_avaliacao_nome . ') resetada.',
            ]);

            session()->flash('success-livewire', 'Avaliação do Aluno resetada com êxito.');

            $this->dispatchBrowserEvent('dissmiss-modal');
            $this->dispatchBrowserEvent('toast');

        } else {

            session()->flash('danger-livewire', 'Senha incorreta.');

            $this->dispatchBrowserEvent('toast');
        } 
    }


    public function setDesvincularResponsavelId($responsavel_id, $responsavel_nome)
    {        
        $this->desvincular_responsavel_id = $responsavel_id;
        $this->desvincular_responsavel_nome = $responsavel_nome;
    }

    public function desvincularResponsavel()
    {
        if(\Auth::attempt(['id' => \Auth::id(), 'password' => $this->password_to_delete, 'status' => 0]))
        {
            \App\Models\ResponsavelAluno::plataforma()->where('responsavel_id', $this->desvincular_responsavel_id)->where('aluno_id', $this->user->id)->delete();
            
            return redirect(route('admin.aluno.info', ['id' => $this->user->id]))->with('success','Responsável desvinculado do Aluno com êxito.');

        } else {

            session()->flash('danger-livewire', 'Senha incorreta.');

            $this->dispatchBrowserEvent('toast');
        }
    }

    public function delete()
    {
        if(\Auth::attempt(['id' => \Auth::id(), 'password' => $this->password_to_delete, 'status' => 0]))
        {
            UserEndereco::plataforma()->where('user_id', $this->user->id)->delete();
            UserCategoria::plataforma()->where('user_id', $this->user->id)->delete();
            TrilhaAluno::plataforma()->where('aluno_id', $this->user->id)->delete();
            TurmaAluno::plataforma()->where('aluno_id', $this->user->id)->delete();
            \App\Models\AvaliacaoAluno::plataforma()->where('aluno_id', $this->user->id)->delete();
            \App\Models\AvaliacaoAlunoResposta::plataforma()->where('aluno_id', $this->user->id)->delete();
            \App\Models\AlunoAulaConcluida::plataforma()->where('aluno_id', $this->user->id)->delete();
            \App\Models\ComunicadoAluno::plataforma()->where('aluno_id', $this->user->id)->delete();
            \App\Models\Experiencia::plataforma()->where('user_id', $this->user->id)->delete();
            \App\Models\Formacao::plataforma()->where('user_id', $this->user->id)->delete();

            $documentos = Documento::plataforma()->where('user_id', $this->user->id)->get();
            foreach($documentos as $documento)
            {
                $path = str_replace(\App\Models\Util::getLinkStorage(), '', $documento->link);
                Storage::delete($path);
                $documento->delete();
            }

            Log::create([
                'tipo' => 'aluno',
                'ref'  => 'Aluno ' . $this->user->nome . ' ('.$this->user->email . ' - ' . $this->user->celular . ' - ' . $this->user->cpf . ' - ' . \App\Models\Util::replaceDatePt($this->user->data_nascimento) . ') foi deletado.',
            ]);

            User::plataforma()->where('id', $this->user->id)->delete();
            
            return redirect(route('admin.aluno.lista'))->with('success','Aluno deletado com êxito.');

        } else {

            session()->flash('danger-livewire', 'Senha incorreta.');

            $this->dispatchBrowserEvent('toast');
        }
    }
}
