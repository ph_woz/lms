<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TrilhaAluno extends Model
{
    use HasFactory;

    protected $table = 'trilhas_alunos';

    public $timestamps = true;

    public $fillable = [
        'plataforma_id',
        'trilha_id',
        'aluno_id',
        'data_conclusao',
    	'data_colacao_grau',
        'codigo_certificado',
        'status',
    ];

    public function scopePlataforma($query)
    {
        return $query->where('plataforma_id', session('plataforma_id') ?? Plataforma::dominio()->pluck('id')[0] ?? null);
    }

    public function scopeAtivo($query)
    {
        return $query->where('status', 0);
    }

    public static function geraCodigoCertificado($trilhaAlunoId)
    {
        $codigo = session('plataforma_id') . '-' . $trilhaAlunoId . '-' . substr(str_shuffle("ABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 1);
        
        return $codigo;
    }
}
