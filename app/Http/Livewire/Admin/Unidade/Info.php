<?php

namespace App\Http\Livewire\Admin\Unidade;

use Livewire\Component;
use App\Models\Unidade;
use App\Models\User;
use Illuminate\Support\Facades\Http;

class Info extends Component
{
    public $unidade;
    public $cadastrante;
    public $password_to_delete;

    public function mount($unidade_id)
    {
        $this->unidade = Unidade::plataforma()->find($unidade_id);
    
        $this->cadastrante = User::plataforma()->where('id', $this->unidade->cadastrante_id)->first();
    }

    public function render()
    {
        return view('livewire.admin.unidade.info');
    }

    public function delete()
    {
        if(\Auth::attempt(['id' => \Auth::id(), 'password' => $this->password_to_delete, 'status' => 0]))
        {
            Unidade::plataforma()->where('id', $this->unidade->id)->delete();

            session()->flash('success', 'Unidade deletada com êxito.');
            
            return redirect(route('admin.unidade.lista'));

        } else {

            session()->flash('danger-livewire', 'Senha incorreta.');

            $this->dispatchBrowserEvent('toast');
        }
    }
}
