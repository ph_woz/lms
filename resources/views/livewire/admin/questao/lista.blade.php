<div>

    @if(isset($avaliacao))
        <style>
            .default-show { display: none; }
        </style>
        <h1 class="fs-18 bg-primary p-3 text-light rounded-2 shadow text-shadow-1">
            + adicione Questões na Avaliação<br>
            <a href="{{ route('admin.avaliacao.info', ['id' => $avaliacao->id]) }}" class="ms-3 text-light">
                <span class="weight-600">
                    {{ $avaliacao->referencia }}
                </span>
            </a>
        </h1>
        <hr>
    @else
        <style>
            .add_questoes_para_avaliacao_id { display: none; }
        </style>
    @endif

    <form>

        <input type="hidden" wire:model="add_questoes_para_avaliacao_id">

        <div class="d-lg-flex mb-3">

            <input wire:model="search" type="search" class="me-lg-3 mb-3 mb-lg-0 form-control p-form" placeholder="Buscar">

            <select wire:model="modelo" class="select-form w-lg-40">
                <option value="">Todos os Modelos</option>
                <option value="O">Objetivas, apenas</option>
                <option value="D">Discursivas, apenas</option>
            </select>

            <select wire:model="status" class="mx-lg-3 my-3 my-lg-0 select-form w-lg-40">
                <option value="0">Ativos</option>
                <option value="1">Desativados</option>
            </select>

            <select wire:model="ordem" class="select-form w-lg-40">
                <option value="assunto">Alfabética</option>
                <option value="desc">Mais novos</option>
                <option value="asc">Mais antigos</option>
            </select>

        </div>

        <div wire:ignore>
            <select wire:model="categoriasSelected" class="selectpicker" multiple data-width="100%" data-live-search="true" title="Categorias">
                @foreach($categorias as $categoria)
                    <option value="{{ $categoria->id }}">{{ $categoria->nome }}</option>
                @endforeach
            </select>
        </div>

    </form>

    <div class="mt-5 d-flex justify-content-between">

        <div class="align-self-end mb-2">
            <span class="resultados-encontrados">
                {{ count($questoes) }} resultados de {{ $countQuestoes }}.
            </span>
        </div>

        <div class="mb-3">
            <a href="{{ route('admin.questao.add') }}" class="btn-add aclNivel2 default-show">
                + Adicionar
            </a>
            <a href="#ModalAddTodasQuestoes" data-bs-toggle="modal" class="btn btn-primary add_questoes_para_avaliacao_id">
                + Adicionar todas
            </a>
        </div>

    </div>

    <div class="row">
        <div class="col-12">

            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col">Assunto</th>
                                    <th scope="col">
                                        <span class="float-end">
                                            Ações
                                        </span>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($questoes as $questao)
                                    <tr>
                                        <td>{{ $questao->assunto }}</td>
                                        <td>
                                            <span class="float-end">

                                                <a href="#ModalAddQuestaoParaAvaliacao" wire:click="setEventAddQuestaoId({{$questao->id}})" data-bs-toggle="modal" class="btn btn-primary add_questoes_para_avaliacao_id">
                                                    + Add
                                                </a>

                                                <a href="{{ route('admin.questao.info', ['id' => $questao->id]) }}" class="btn bg-base text-white">
                                                    <i class="fa fa-eye"></i>
                                                </a>

                                            </span>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            {{ $questoes->links() }}

        </div>
    </div>

    <form wire:submit.prevent="addQuestao">
        <div wire:ignore class="modal fade" id="ModalAddQuestaoParaAvaliacao" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3 class="modal-title" id="exampleModalLabel">Adicionar Questão na Avaliação</h3>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-footer">

                        <input type="hidden" wire:model="add_questao_id">
            
                        <div class="w-100 d-flex">

                            <select wire:model="valor_nota" class="w-100 me-3 select-form" required>
                                <option value="">Selecione a Nota</option>
                                @foreach($notasPossiveis as $notaPossivel)
                                    <option value="{{ $notaPossivel }}">{{ $notaPossivel }}</option>
                                @endforeach
                            </select>
                            
                            <button type="submit" class="btn btn-primary">
                                Confirmar
                            </button>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </form>

    <form wire:submit.prevent="addTodasQuestoes()">
        <div wire:ignore class="modal fade" id="ModalAddTodasQuestoes" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3 class="modal-title" id="exampleModalLabel">Adicionar Todas as Questões na Avaliação</h3>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-footer">

                        <div class="w-100 d-flex">

                            <select wire:model="valor_nota" class="w-100 me-3 select-form" required>
                                <option value="">Selecione a Nota</option>
                                @foreach($notasPossiveis as $notaPossivel)
                                    <option value="{{ $notaPossivel }}">{{ $notaPossivel }}</option>
                                @endforeach
                            </select>
                            
                            <button type="submit" class="btn btn-primary">
                                Confirmar
                            </button>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </form>

    @include('livewire.util.toast')

</div>