@extends('layouts.admin.master')

@section('content')
    
    @livewire('admin.formulario.info', ['formulario_id' => $formulario_id])

@endsection

@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.14.0-beta2/dist/js/bootstrap-select.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.js"></script>
<script>
	$(".number").mask('000');
</script>
<script>
	window.addEventListener('dissmiss-modal', event => {
		$(".modal").modal('hide');
	});
</script>
<script>
@if(isset($_GET['editar_pergunta_id']) && $_GET['editar_pergunta_id'] == 0)
var myModal = new bootstrap.Modal(document.getElementById("ModalPerguntaAdd"), {});
document.onreadystatechange = function () {
  myModal.show();
};
@endif
var myModal = new bootstrap.Modal(document.getElementById("ModalPerguntaEditar"), {});
document.onreadystatechange = function () {
  myModal.show();
};
</script>
@endsection