@extends('layouts.site')

@section('seo_title', $pagina->seo_title ?? $nomePlataforma)
@section('seo_description', $pagina->seo_description ?? null)
@section('seo_keywords', $pagina->seo_keywords ?? null)

@section('css')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/uikit@3.7.2/dist/css/uikit.min.css" />
@endsection

@section('content')

<section id="cursos" class="py-5 overflow-hidden">
   <div class="container-fluid px-xl-7 py-5">

      <div class="row align-items-center my-5">
         <div class="col-lg-6">
            <h1 class="p-0 m-0 font-poppins weight-700 text-dark">
               CURSOS
            </h1> 
         </div>
         <form class="col-lg-6 d-flex mt-1 mt-lg-0">
            <input type="search" name="buscar" class="form-control p-form rounded-right-0 border" placeholder="Qual curso está procurando?" value="{{ $_GET['buscar'] ?? null }}">
            <button type="submit" class="btn btn-primary weight-600 rounded-left-0">
               Buscar
            </button>
         </form>
      </div>

      @foreach($categorias as $categoria)

        @php
          $cursosIds = \App\Models\CursoCategoria::plataforma()->where('categoria_id', $categoria->id)->get()->pluck('curso_id')->toArray();
          $cursos    = \App\Models\Curso::plataforma()->ativo()->publico();

          if(isset($_GET['buscar']))
            $cursos = $cursos->where('nome', 'like', '%'.$_GET['buscar'].'%');

          $cursos = $cursos->whereIn('id', $cursosIds)->orderBy('nome')->get();  
        @endphp

        @if(count($cursos) > 0)
          <article class="mb-4">

             <p class="mb-0 pb-0 font-poppins fs-17">
                <a href="" class="text-dark hover-d-none"> 
                   {{ $categoria->nome }}
                </a>
             </p>

             <hr class="border border-secondary mt-1" />

             <div class="uk-slider-container-offset" uk-slider="autoplay: true;">

               <div class="uk-position-relative uk-visible-toggle uk-light" tabindex="-1">

                   <ul class="uk-slider-items uk-child-width-1-5@s uk-grid">
                       @foreach($cursos as $curso)
                       <li>
                         <a href="/curso/{{$curso->slug}}" class="hover-d-none">
                           <div class="mb-5 uk-card uk-card-default hoverBox">
                               <div class="uk-card-media-top">
                                   <img src="{{ $curso->foto_capa ?? asset('images/no-image.jpeg') }}" class="card-img-top h-lg-200 object-fit-cover">
                               </div>
                                <div class="card-body h-lg-90 flex-center">
                                   <p class="mb-0 text-dark font-poppins weight-600 text-center"> 
                                      {{ $curso->nome }}
                                   </p>
                                </div>
                               <div class="card-footer bg-primary font-poppins weight-600 text-white text-center text-shadow-1 ls-05">
                                  Matricule-se
                               </div>
                           </div>
                         </a>
                       </li>
                       @endforeach
                   </ul>

                   <a class="uk-position-center-left uk-position-small uk-hidden-hover text-dark" href="#" uk-slidenav-previous uk-slider-item="previous"></a>
                   <a class="uk-position-center-right uk-position-small uk-hidden-hover text-dark" href="#" uk-slidenav-next uk-slider-item="next"></a>

               </div>

             </div>

          </article>
        @endif

      @endforeach

   </div>
</section>

@endsection

@push('scripts')
<script src="https://cdn.jsdelivr.net/npm/uikit@3.7.2/dist/js/uikit.min.js"></script>
@endpush