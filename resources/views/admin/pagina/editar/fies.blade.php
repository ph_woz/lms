@extends('layouts.admin.master')
@section('content')

<nav aria-label="breadcrumb" class="mb-4-5 p-3 bg-db rounded ashadow border">
	<ol class="breadcrumb p-0 m-0">
    	<li class="breadcrumb-item"><a href="{{ route('admin.pagina.lista') }}" class="color-blue-info-link">Páginas</a></li>
    	<li class="breadcrumb-item text-dark" aria-current="page">FIES</li>
  	</ol>
</nav>


<section class="mb-4-5">

	<div class="section-title rounded-bottom-0 d-lg-flex justify-content-between align-items-center">
		<div>
			Seções ({{ count($secoes) }})
		</div>
		<div>
	    	<a href="#ModalSecaoAdd" data-bs-toggle="modal" class="btn btn-outline-primary border-light text-light">
	    		+ Adicionar novo
	    	</a>
	    </div>
	</div>

</section>

<div class="row">
    <div class="col-12">

        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">Título</th>
                                <th scope="col">
                                    <span class="float-end">
                                        Ações
                                    </span>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($secoes as $secao)
                                <tr>
                                    <td>{{ $secao->titulo }}</td>
                                    <td>
                                        <span class="float-end">

                                            <a href="?editar_secao_id={{$secao->id}}" class="aclNivel2 btn btn-warning text-dark">
                                                <i class="fa fa-edit"></i>
                                            </a>
									    	<a href="#ModalDeletar" onclick="document.getElementById('deletar_id').value = '{{$secao->id}}'; document.getElementById('secao_titulo').innerHTML = '{{ $secao->titulo }}';" data-bs-toggle="modal" class="aclNivel3 btn btn-danger">
									    		<i class="fa fa-trash"></i>
									    	</a>
									    	
                                        </span>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
</div>

<form method="POST" enctype="multipart/form-data">
@csrf
    <div class="modal fade" id="ModalSecaoAdd" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      	<div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        	<div class="modal-content">
	          	<div class="modal-header">
	            	<h3 class="modal-title color-444 font-nunito weight-600">Adicionar Seção</h3>
	            	<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
	          	</div>
	          	<div class="modal-body">
					
					<div class="d-lg-flex mb-3">

		       			<div class="w-100 me-lg-3 mb-3 mb-lg-0">
		       				<label>Título</label>
		       				<input type="text" name="titulo" class="form-control p-form" placeholder="Título" required>
		       			</div>
		       			<div class="w-lg-40 me-lg-3 mb-3 mb-lg-0">
		       				<label>Forma de Exibição</label>
		       				<select name="expandido" class="select-form">
		       					<option value="N">Não expandido</option>
		       					<option value="S">Expandido</option>
		       				</select>
		       			</div>
		       			<div class="w-lg-35">
		       				<label>Ordem</label>
		       				<input type="number" name="ordem" class="form-control p-form" placeholder="N° de Ordem">
		       			</div>

				    </div>

				    <div class="w-100">
				    	<label>Descrição</label>
				    	<textarea name="descricao" class="textarea-form" placeholder="Descreva o depoimento"></textarea>
				    </div>

	          	</div>
          		<div class="modal-footer">
            		<button type="submit" name="add" value="S" class="btn btn-primary py-2 weight-600">
                		Confirmar
            		</button>
          		</div>
	        </div>
      	</div>
    </div>
</form>

@if(isset($editarSecao))
<form method="POST" enctype="multipart/form-data">
@csrf
    <div class="modal fade" id="ModalSecaoEditar" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      	<div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        	<div class="modal-content">
	          	<div class="modal-header">
	            	<h3 class="modal-title color-444 font-nunito weight-600">Editar Seção</h3>
	            	<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
	          	</div>
	          	<div class="modal-body">
					
					<div class="d-lg-flex mb-3">

		       			<div class="w-100 me-lg-3 mb-3 mb-lg-0">
		       				<label>Título</label>
		       				<input type="text" name="titulo" class="form-control p-form" placeholder="Título" required value="{{ $editarSecao->titulo }}">
		       			</div>
		       			<div class="w-lg-40 me-lg-3 mb-3 mb-lg-0">
		       				<label>Forma de Exibição</label>
		       				<select name="expandido" class="select-form">
		       					<option value="S">Expandido</option>
		       					<option value="N" @if($editarSecao->expandido == 'N') selected @endif>Não expandido</option>
		       				</select>
		       			</div>
		       			<div class="w-lg-35">
		       				<label>Ordem</label>
		       				<input type="number" name="ordem" class="form-control p-form" placeholder="N° de Ordem" value="{{ $editarSecao->ordem }}">
		       			</div>

				    </div>

				    <div class="w-100">
				    	<label>Descrição</label>
				    	<textarea name="descricao" class="textarea-form" placeholder="Descreva o depoimento">{{ $editarSecao->descricao }}</textarea>
				    </div>

	          	</div>
          		<div class="modal-footer">
            		<button type="submit" name="editar" value="S" class="btn btn-primary py-2 weight-600">
                		Confirmar
            		</button>
          		</div>
	        </div>
      	</div>
    </div>
</form>
@endif


<form method="POST">
@csrf
		
	<input type="hidden" name="deletar_id" id="deletar_id">

	<div class="modal fade" id="ModalDeletar" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  		<div class="modal-dialog modal-dialog-centered">
    		<div class="modal-content">
      			<div class="modal-header">
        			<h3 class="modal-title" id="exampleModalLabel">Deletar Seção</h3>
        			<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      			</div>
      			<div class="modal-body text-center py-5">
      				<p class="m-0 weight-600 fs-17" id="secao_titulo"></p>
      				<hr>
        			<p class="m-0 fs-16">
        				Você tem certeza que deseja deletar esta seção?
        				<br>
        				Todas as informações serão perdidas definitivamente.
        			</p>
      			</div>
      			<div class="modal-footer">
	        		<button type="submit" name="deletar" value="S" class="btn btn-danger weight-600">Confirmar</button>
      			</div>
    		</div>
  		</div>
	</div>
</form>

@push('scripts')
<script>

	@if(isset($editarSecao))

		var myModal = new bootstrap.Modal(document.getElementById("ModalSecaoEditar"), {});
		document.onreadystatechange = function () {
		  myModal.show();
		};
	
	@endif

</script>
@endpush

<form method="POST">
@csrf

	<section class="mb-4-5" id="boxCursoSEO">

		<a href="#boxSeo" data-bs-toggle="collapse" aria-expanded="false" class="hover-d-none">
			<div class="section-title">
				Informações de SEO para resultados de buscas em Navegadores
			</div>
		</a>
		
		<div class="collapse show bg-white border p-4" id="boxSeo">
			
			<div class="d-lg-flex mb-3">

				<div class="w-100 me-lg-3 mb-3 mb-lg-0">
					<label>Título</label>
					<input type="text" name="seo_title" class="form-control p-form" placeholder="Title" value="{{ $conteudo->seo_title ?? null }}">
				</div>

				<div class="w-100">
					<label>Palavras-chaves</label>
					<input type="text" name="seo_keywords" class="form-control p-form" placeholder="Keywords" value="{{ $conteudo->seo_keywords ?? null }}">
				</div>

			</div>

			<div class="w-100 mb-3">
				<label>Descrição</label>
				<input type="text" name="seo_description" class="form-control p-form" placeholder="Description" value="{{ $conteudo->seo_description ?? null }}">
			</div>
			
		</div>

	</section>

	<button type="submit" name="sent" value="true" class="btn-add">
		Salvar
	</button>

</form>

@endsection