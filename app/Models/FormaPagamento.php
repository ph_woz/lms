<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FormaPagamento extends Model
{
    use HasFactory;

    protected $table = 'formas_pagamentos';

    public $timestamps = false;

    protected $fillable = [
        'plataforma_id',
        'tipo_cadastro',
        'registro_id',
        'trilha_id',
        'turma_id',
        'tipo',
        'parcela',
        'valor',
    ];

    public static  function boot()
    {
        parent::boot(); 
        static::creating(function ($model)
        {
            $model->plataforma_id = session('plataforma_id') ?? Auth()->user()->plataforma_id ?? Plataforma::dominio()->pluck('id')[0] ?? null;
        });
    }

    public function scopePlataforma($query)
    {
        return $query->where('plataforma_id', session('plataforma_id') ?? Plataforma::dominio()->pluck('id')[0] ?? null);
    }

    public function scopeProduto($query)
    {
        return $query->where('tipo_cadastro', 'Produto');
    }

    public function scopeTrilha($query)
    {
        return $query->where('tipo_cadastro', 'Trilha');
    }

    public function scopeTurma($query)
    {
        return $query->where('tipo_cadastro', 'Turma');
    }
}
