<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Collection;
use App\Models\Curso;
use App\Models\CursoModulo;
use App\Models\CursoAula;
use App\Models\Questao;
use App\Models\QuestaoAlternativa;
use App\Models\Avaliacao;
use App\Models\AvaliacaoQuestao;
use App\Models\AvaliacaoAluno;
use App\Models\AvaliacaoAlunoResposta;
use App\Models\AvaliacaoConfigNota;
use App\Models\User;
use App\Models\TurmaAluno;
use App\Models\Turma;
use App\Models\TrilhaAluno;
use App\Models\Trilha;
use App\Models\TrilhaTurma;
use App\Models\Documento;

class MigracaoController extends Controller
{
	public function cursos()
	{
		$response = Http::get('https://colegioejabrasil.com.br/api/aluno/cursos');
		$response = json_decode($response, true);

		foreach($response as $res)
		{
			$checkExists = Curso::plataforma()->where('referencia', $res['nome'])->exists();
			
			if($checkExists == false)
			{
				$curso = Curso::create([
					'nome'          => $res['nome'],
					'referencia'    => $res['referencia'],
					'slug'          => $res['slug'],
					'foto_capa'     => $res['foto_miniatura'],
					'descricao'     => $res['descricao'],
				]);

				$modulo = CursoModulo::create([
					'curso_id'      => $curso->id, 
					'nome'          => 'Módulo Único',
				]);

				foreach($res['aulas'] as $aula)
				{
					CursoAula::create([
				    	'curso_id'      => $curso->id,
				    	'modulo_id'     => $modulo->id,
				    	'nome'          => $aula['nome']      ?? null,
				        'descricao'     => $aula['descricao'] ?? null,
				        'conteudo'      => $aula['conteudo']  ?? null,
				    	'ordem'         => $aula['ordem']     ?? null,
				        'tipo_objeto'   => 'Vídeo',
					]);
				}
			}
		}
	}

    public function questoes()
    {
        $questoes = Http::get('https://colegioejabrasil.com.br/api/questao/lista')['questoes'];

        foreach($questoes as $questao)
        {
            $enunciado = $questao['questao']['enunciado'];
            $enunciado = html_entity_decode($enunciado);
            $enunciado = strip_tags($enunciado);

            $checkExists = Questao::plataforma()->where('enunciado', $enunciado)->exists();

            if($checkExists == false)
            {
            	$modelo = $questao['questao']['modelo'];
            	$modelo = $modelo[0];
            	
                $newQuestao = Questao::create([
                    'assunto'   => $questao['questao']['assunto'],
                    'enunciado' => $enunciado,
                    'arquivo'   => $questao['questao']['imagem'],
                    'modelo'    => $modelo,
                    'gabarito'  => $questao['questao']['gabarito'],
                    'status'    => $questao['questao']['status'],
                ]);
                
                foreach($questao['alternativas'] as $alternativa)
                {
                    if($alternativa['correta'] == 'C')
                        $correta = 'S';
                    else
                        $correta = 'N';

                    QuestaoAlternativa::create([
                        'questao_id'  => $newQuestao->id,
                        'alternativa' => $alternativa['alternativa'],
                        'correta'     => $correta,
                    ]);
                }
            }
        }
    }

    public function usersComDocumentos()
    {
        $response = Http::get('https://colegioejabrasil.com.br/migracao/get-users-com-documentos');
        $emails = json_decode($response, true);

        foreach($emails as $email)
        {
            $documentos = Http::get('https://colegioejabrasil.com.br/migracao/get-documentos-by-email/'.$email);
            $documentos = json_decode($documentos, true);
   
            $user_id = User::plataforma()->where('email', $email)->pluck('id')[0] ?? null;

            $documentosAtuais = Documento::plataforma()->where('user_id', $user_id)->select('nome')->get()->pluck('nome')->toArray();

            foreach($documentos as $link => $doc)
            {
                if(!in_array($doc, $documentosAtuais))
                {
                    Documento::create([
                        'user_id' => $user_id,
                        'nome'    => $doc,
                        'link'    => $link,
                    ]);
                }
            }
        }     
    }

    public function contatos()
    {
        $contatos = Http::get('https://colegioejabrasil.com.br/migracao/contatos');
        $contatos = json_decode($contatos, true);

        foreach($contatos as $contato)
            Contato::create($contato);
    }

    public function avaliacoesAlunos()
    {
        $response = Http::get('https://colegioejabrasil.com.br/migracao/users');
        $response = json_decode($response, true);

        $usersErrors = User::plataforma()->aluno()->whereNull('token')->where('id', 23443)->get();
        foreach($usersErrors as $user)
        {
            TurmaAluno::plataforma()->where('aluno_id', $user->id)->delete();
            TrilhaAluno::plataforma()->where('aluno_id', $user->id)->delete();
            AvaliacaoAluno::plataforma()->where('aluno_id', $user->id)->delete();
            AvaliacaoAlunoResposta::plataforma()->where('aluno_id', $user->id)->delete();

            $user->delete();
        }

        foreach($response as $res)
        {
            $aluno = User::plataforma()->where('email', $res['email'])->first();

            $aluno_id = $aluno->id ?? null;

            if(is_null($aluno_id))
            {
                $dadosUser = $res;
                $dadosUser['tipo'] = 'aluno';

                $aluno = User::create($dadosUser);
                $aluno_id = $aluno->id;
            }

            $trilhasDoAlunoParaMigrar = Http::get('https://colegioejabrasil.com.br/migracao/aluno/'.$res['id'].'/trilhas');
            $trilhasDoAlunoParaMigrar = json_decode($trilhasDoAlunoParaMigrar, true);

            foreach($trilhasDoAlunoParaMigrar as $trilhaDoAlunoParaMigrar)
            {
                $trilha_id = Trilha::plataforma()->where('referencia', $trilhaDoAlunoParaMigrar['referencia'])->pluck('id')[0] ?? null;
                
                if(is_null($trilha_id))
                {
                    $dadosTrilhaDoAlunoParaMigrar = $trilhaDoAlunoParaMigrar;
                    $dadosTrilhaDoAlunoParaMigrar['foto_capa'] = $trilhaDoAlunoParaMigrar['foto_miniatura'];
                    $dadosTrilhaDoAlunoParaMigrar['slug'] = \Str::slug($trilhaDoAlunoParaMigrar['nome']);

                    $newTrilha = Trilha::create($dadosTrilhaDoAlunoParaMigrar);
                    $trilha_id = $newTrilha->id;
                }

                $trilhasJaMatriculadas = TrilhaAluno::plataforma()->where('aluno_id', $aluno_id)->where('trilha_id', $trilha_id)->select('trilha_id')->get()->pluck('trilha_id')->toArray(); 
                $trilhasJaMatriculadas = Trilha::plataforma()->whereIn('id', $trilhasJaMatriculadas)->select('referencia')->get()->pluck('referencia')->toArray();

                if(!in_array($trilhaDoAlunoParaMigrar['referencia'], $trilhasJaMatriculadas))
                {
                    TrilhaAluno::create([
                        'plataforma_id' => $this->getPlataformaId(),
                        'trilha_id' => $trilha_id,
                        'aluno_id'  => $aluno_id,
                    ]);

                    $turmasDaTrilha = Http::get('https://colegioejabrasil.com.br/migracao/get-turmas-by-trilha-id/'.$trilhaDoAlunoParaMigrar['id']);
                    $turmasDaTrilha = json_decode($turmasDaTrilha, true);

                    foreach($turmasDaTrilha as $turmaDaTrilha)
                    {
                        $turma_id = Turma::plataforma()->where('nome', $turmaDaTrilha['nome'])->pluck('id')[0] ?? null;

                        $curso = Http::get('https://colegioejabrasil.com.br/migracao/get-curso-by-turma-id/'.$turmaDaTrilha['id']);
                        $curso = json_decode($curso, true);

                        $curso_id = Curso::plataforma()->where('referencia', $curso['referencia'] ?? $curso['nome'] ?? 'none')->pluck('id')[0] ?? 0;

                        if(is_null($turma_id))
                        {
                            $dadosTurmaDaTrilha = $turmaDaTrilha;
                            $dadosTurmaDaTrilha['plataforma_id'] = $this->getPlataformaId();
                            $dadosTurmaDaTrilha['curso_id'] = $curso_id;
                            $dadosTurmaDaTrilha['nota_minima_aprovacao'] = 6;

                            $newTurma = Turma::create($dadosTurmaDaTrilha);
                            $turma_id = $newTurma->id;
                            $turmasIdsDaTrilha[] = $turma_id;
                        }

                        $avaliacoes = Http::get('https://colegioejabrasil.com.br/migracao/get-avaliacoes-by-turma-id/'.$turmaDaTrilha['id']);
                        $avaliacoes = json_decode($avaliacoes, true);

                        if($avaliacoes != null)
                        {
                            foreach($avaliacoes as $avaliacao)
                            {
                                $avaliacao = $avaliacao[0];

                                $avaliacaoReferencia = $avaliacao['avaliacao']['referencia'] ?? $avaliacao['avaliacao']['nome'] ?? 'Não identificado';

                                $newAvaliacao = Avaliacao::plataforma()->where('referencia', $avaliacaoReferencia)->first();

                                if(is_null($newAvaliacao))
                                {
                                    $newAvaliacao = Avaliacao::create([
                                        'curso_id' => $curso_id,
                                        'turma_id' => $turma_id,
                                        'referencia' => $avaliacaoReferencia,
                                        'condicao_fazer_avaliacao' => 'concluir-conteudo-do-curso',
                                        'n_tentativas' => $avaliacao['avaliacao']['n_tentativas'],
                                        'liberar_gabarito' => $avaliacao['avaliacao']['liberar_gabarito'],
                                        'instrucoes' => $avaliacao['avaliacao']['instrucoes'],
                                    ]);

                                    $questoes = $avaliacao['questoes'];

                                    foreach($questoes as $questao)
                                    {
                                        $questao_id = Questao::plataforma()->where('assunto', $questao['assunto'])->select('id')->get()->pluck('id')[0] ?? null;

                                        if(isset($questao_id))
                                            AvaliacaoQuestao::create([
                                                'avaliacao_id' => $newAvaliacao->id,
                                                'questao_id'   => $questao_id,
                                                'valor_nota'   => $questao['valor_nota'],
                                            ]);
                                    }

                                    foreach($avaliacao['configNotas'] as $configNota)
                                    {
                                        AvaliacaoConfigNota::create([
                                            'avaliacao_id' => $newAvaliacao->id,
                                            'n_questoes'  => $configNota['n_questoes'],
                                            'valor_nota'  => $configNota['valor_nota'],
                                        ]);
                                    }

                                }

                                $avaliacaoAluno = Http::get('https://colegioejabrasil.com.br/migracao/get-avaliacao-aluno-by-avaliacao-id/'.$avaliacao['avaliacao']['id'].'/'.$res['id']);
                                $avaliacaoAluno = json_decode($avaliacaoAluno, true);

                                if(isset($avaliacaoAluno))
                                {
                                    $avAluno = AvaliacaoAluno::plataforma()->where('aluno_id', $aluno_id)->where('avaliacao_id', $newAvaliacao->id)->first();

                                    if(is_null($avAluno))
                                    {
                                        $dadosAvAluno['curso_id']     = $curso_id;
                                        $dadosAvAluno['turma_id']     = $turma_id;
                                        $dadosAvAluno['avaliacao_id'] = $newAvaliacao->id;
                                        $dadosAvAluno['aluno_id']     = $aluno_id;
                                        $dadosAvAluno['nota']         = $avaliacaoAluno['avaliacaoAluno']['nota'];
                                        $dadosAvAluno['status']       = 'F';
                                        $dadosAvAluno['n_tentativa']  = $avaliacaoAluno['avaliacaoAluno']['n_tentativa'];

                                        $avAluno = AvaliacaoAluno::create($dadosAvAluno);
                                    }

                                    $questoesIds = [];

                                    foreach($avaliacaoAluno['respostas'] as $resposta)
                                    {
                                        $questao_id = Questao::plataforma()->where('assunto', $resposta['assunto'])->pluck('id')[0] ?? null;
                                        $questoesIds[] = $questao_id;

                                        $alternativa_id = QuestaoAlternativa::plataforma()->where('questao_id', $questao_id)->where('alternativa', $resposta['alternativaEscolhida'])->pluck('id')[0] ?? null;

                                        if($resposta['acertou'] == 'S')
                                            $acertou = 'S';
                                        else
                                            $acertou = 'N';
                                        
                                        AvaliacaoAlunoResposta::create([
                                            'avaliacao_id'   => $newAvaliacao->id,
                                            'aluno_id'       => $aluno_id,
                                            'curso_id'       => $curso_id,
                                            'turma_id'       => $turma_id,
                                            'questao_id'     => $questao_id,
                                            'alternativa_id' => $alternativa_id,
                                            'acertou'        => $acertou,
                                        ]);
                                    }

                                    if($avAluno->questoesIds == null)
                                        $avAluno->update(['questoesIds' => implode(',', $questoesIds)]);
                                }

                            }
                        }

                        $turmasIdsJaVinculadas = TrilhaTurma::plataforma()->where('trilha_id', $trilha_id)->where('turma_id', $turma_id)->select('turma_id')->get()->pluck('turma_id')->toArray(); 
                        $turmasJaVinculadas = Turma::plataforma()->whereIn('id', $turmasIdsJaVinculadas)->select('nome')->get()->pluck('nome')->toArray();

                        if(!in_array($turmaDaTrilha['nome'], $turmasJaVinculadas))
                        {
                            TrilhaTurma::create([
                                'plataforma_id' => $this->getPlataformaId(),
                                'trilha_id' => $trilha_id,
                                'turma_id'  => $turma_id,
                            ]);
                        }

                        $turmasJaMatriculadas = TurmaAluno::plataforma()->where('aluno_id', $aluno_id)->where('turma_id', $turma_id)->select('turma_id')->get()->pluck('turma_id')->toArray(); 
                        $turmasJaMatriculadas = Turma::plataforma()->whereIn('id', $turmasJaMatriculadas)->select('nome')->get()->pluck('nome')->toArray();

                        if(!in_array($turmaDaTrilha['nome'], $turmasJaMatriculadas))
                        {
                            TurmaAluno::create([
                                'plataforma_id' => $this->getPlataformaId(),
                                'curso_id' => $curso_id ?? 0,
                                'turma_id' => $turma_id,
                                'aluno_id' => $aluno_id,
                            ]);
                        }
                    }

                }
            }

            $updateUserMigrado = Http::get('https://colegioejabrasil.com.br/migracao/update-user-migrado/'.$res['id']);
            $updateUserMigrado = json_decode($updateUserMigrado, true);
            
            $aluno->update(['token' => 'migrado']);
        }

    }

    public function getPlataformaId()
    {
        return session('plataforma_id') ?? \Auth()->user()->plataforma_id ?? \App\Models\Plataforma::dominio()->pluck('id')[0] ?? null;
    }
}