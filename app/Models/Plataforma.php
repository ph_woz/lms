<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Plataforma extends Model
{
    use HasFactory;

    protected $table = 'plataformas';

    public $timestamps = false;

    public $fillable = [
    	'nome',
    	'dominio',
    	'status', // 0 = ativo | 1 = inadimplente | 2 = cancelado | 3 = trial
        'logotipo',
        'favicon',
        'jumbotron',
        'fundo_login',
        'email_contratacao_turma',
        'email_contratacao_trilha',
        'responsavel_id',
        'plano_id',
        'cnpj',
        'endereco',
        'horario',
        'email',
        'whatsapp',
        'telefone',
        'facebook',
        'instagram',
        'youtube',
        'api_key',
        'tema',
        'forma_login',
        'scripts_start_head',
        'scripts_final_head',
        'scripts_start_body',
        'scripts_final_body',
    ];

    public function scopePlataforma($query)
    {
        return $query->where('dominio', \Request::getHost());
    }

    public function scopeAtivo($query)
    {
        return $query->where('status', 0);
    }
    public function scopeDominio($query)
    {    
        return $query->where('dominio', \Request::getHost());
    }

    public function modulos()
    {
        $modulosAtivos = DB::table('wozcode_modulos')

            ->join('plataformas_modulos', 'wozcode_modulos.url', '=', 'plataformas_modulos.url')
            ->where('plataformas_modulos.plataforma_id', session('plataforma_id'))
            ->whereIn('wozcode_modulos.status', array(0,2))
            ->orderBy('plataformas_modulos.ordem')
            ->get();

        return $modulosAtivos;
    }

}
