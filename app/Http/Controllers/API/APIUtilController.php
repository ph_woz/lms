<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class APIUtilController extends Controller
{
    public function getTurmasByCursoId()
    {
        $curso_id = request('curso_id');

        $turmas = \App\Models\Turma::select('id','nome as referencia')->plataforma()->ativo()->where('curso_id', $curso_id)->orderBy('referencia')->get();

        return response()->json($turmas);
    }

    public function getAvaliacoesByTurmaId()
    {
        $turma_id = request('turma_id');

        $avaliacoes = \App\Models\Avaliacao::select('id','referencia')->plataforma()->ativo()->where('turma_id', $turma_id)->orderBy('referencia')->get();

        return response()->json($avaliacoes);
    }
}