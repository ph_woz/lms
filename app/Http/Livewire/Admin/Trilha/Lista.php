<?php

namespace App\Http\Livewire\Admin\Trilha;

use Livewire\WithPagination;
use Livewire\Component;
use App\Models\Trilha;
use App\Models\Categoria;
use App\Models\TrilhaCategoria;

class Lista extends Component
{
    use WithPagination;

    protected $paginationTheme = 'bootstrap';

    public $unidade_id;
    public $search;
    public $status = 0;
    public $ordem;
    public $categoriasSelected;

    protected $queryString = ['unidade_id','search','status','ordem','categoriasSelected'];

    public function mount()
    {
        if(\Auth::user()->restringir_unidade == 'S' && \Auth::user()->unidade_id != null)
        {
            $this->unidade_id = \Auth::user()->unidade_id;
        }
    }

    public function render()
    {
        $trilhas = Trilha::select('id','referencia')->plataforma()->where('status', $this->status);

        if($this->unidade_id)
        {
            $trilhas = $trilhas->where('unidade_id', $this->unidade_id);
        }

        if($this->search)
        {
            $trilhas = $trilhas->where('referencia', 'like', '%'.$this->search.'%');
        }

        if($this->categoriasSelected != null)
        {
            $trilhasIds = TrilhaCategoria::plataforma()->whereIn('categoria_id', $this->categoriasSelected ?? array())->get()->pluck('trilha_id')->toArray();
            $trilhas = $trilhas->whereIn('id', $trilhasIds);
        }

        $countTrilhas = $trilhas->count();


        if($this->ordem == 'desc' || $this->ordem == 'asc')
        {
            $trilhas = $trilhas->orderBy('id', $this->ordem);
        } else {
            $trilhas = $trilhas->orderBy('referencia');
        }


        $trilhas = $trilhas->paginate(10);


        $categorias = Categoria::select('id','nome')->plataforma()->ativo()->tipo('trilha')->orderBy('nome')->get();

        return view('livewire.admin.trilha.lista', [
            'trilhas'      => $trilhas,
            'countTrilhas' => $countTrilhas,
            'categorias'   => $categorias
        ]);
    }
}
