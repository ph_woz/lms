<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Models\Plataforma;
use App\Models\Chamado;
use App\Models\User;

class EmailNotificaChamadoWozcode extends Mailable
{
    use Queueable, SerializesModels;

    public $chamado;
    public $nome; // Nome do user cadastrante.

    public function __construct(Chamado $chamado, $nome)
    {
        $this->chamado = $chamado;
        $this->nome    = $nome;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $plataforma = Plataforma::plataforma()->first();

        $subject = 'Novo chamado aberto | ' . $plataforma->nome;

        return $this->view('mail.notifica-chamado-wozcode')
                ->subject($subject)
                ->from($plataforma->email, $plataforma->nome)
                ->replyTo($plataforma->email, $plataforma->nome)
                ->with([
                    'id'        => $this->chamado->id,
                    'assunto'   => $this->chamado->assunto,
                    'descricao' => $this->chamado->descricao, 
                    'nome'      => $this->nome,
                ]);
    }
}
