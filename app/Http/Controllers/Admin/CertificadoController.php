<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Codedge\Fpdf\Fpdf\Fpdf;
use Illuminate\Support\Facades\Storage;
use App\Models\TrilhaTurma;
use App\Models\Certificado;
use App\Models\User;

class CertificadoController extends Controller
{
    public function lista()
    {
        return view('admin.certificado.lista');
    }
    
    public function info($certificado_id)
    {
        return view('admin.certificado.info', compact('certificado_id') );
    }

    public function gerar($certificado_id)
    {
        $certificado = Certificado::plataforma()->find($certificado_id);

        $user_id = $_GET['user_id'];
        $texto1  = $_GET['texto1'];
        $texto2  = $_GET['texto2'];


        $user = User::plataforma()->where('id', $user_id)->first();

        $title  = $certificado->referencia;
        $codigo = null;


        $posicaoNomeX   = $certificado->nome_posicao_x   ?? 20;
        $posicaoNomeY   = $certificado->nome_posicao_y   ?? 85;
        $fontSizeNome   = $certificado->nome_fontSize    ?? 30;

        $posicaoTexto1X = $certificado->texto1_posicao_x ?? 20;
        $posicaoTexto1Y = $certificado->texto1_posicao_y ?? 117;
        $fontSizeTexto1 = $certificado->texto1_fontSize  ?? 15;

        $posicaoTexto2X = $certificado->texto2_posicao_x ?? 20;
        $posicaoTexto2Y = $certificado->texto2_posicao_y ?? 150;
        $fontSizeTexto2 = $certificado->texto2_fontSize  ?? 15;

        $codigoPosicaoX = $certificado->codigo_posicao_x ?? 233;
        $codigoPosicaoY = $certificado->codigo_posicao_y ?? 179;
        $fontSizeCodigo = $certificado->codigo_fontSize  ?? 15;


        $pdf = new Fpdf();
        $pdf->SetTitle(utf8_decode($title));
        $pdf->AddPage('L');
        $pdf->SetLineWidth(1.5);
        $pdf->Image($certificado->modelo, 0, 0, $certificado->posicao_imagem ?? 300);

        // Nome
        $pdf->SetFont('Arial', '', $fontSizeNome);
        $pdf->SetXY($posicaoNomeX, $posicaoNomeY); 
        $pdf->MultiCell(265, 6, utf8_decode($user->nome), '', 'C', 0); 

        // Texto 1
        $pdf->SetFont('Arial', '', $fontSizeTexto1); 
        $pdf->SetXY($posicaoTexto1X, $posicaoTexto1Y);
        $pdf->MultiCell(265, 9, utf8_decode($texto1), '', 'C', 0); 

        // Texto 2
        $pdf->SetFont('Arial', '', $fontSizeTexto2); 
        $pdf->SetXY($posicaoTexto2X, $posicaoTexto2Y); 
        $pdf->MultiCell(165, 9, utf8_decode($texto2), '', 'L', 0);

        // Código de Validação
        $pdf->SetFont('Arial', '', $fontSizeCodigo); 
        $pdf->SetXY($codigoPosicaoX, $codigoPosicaoY);
        $pdf->MultiCell(165, 9, utf8_decode($codigo), '', 'L', 0);


        // Disciplinas
        $trilha_id = $_GET['trilha_id'] ?? null;
        
        if(isset($trilha_id) && $trilha_id != null)
        {
            $disciplinas = TrilhaTurma::select('turmas.nome')
                ->join('turmas','trilhas_turmas.turma_id','turmas.id')
                ->where('trilhas_turmas.plataforma_id', $certificado->plataforma_id)
                ->where('turmas.status', 0)
                ->where('trilhas_turmas.trilha_id', $trilha_id)
                ->get()
                ->pluck('nome')
                ->toArray();
        
            if(count($disciplinas) > 0)
            {
                $pdf->SetFont('Arial', 'B', 15); 
                $pdf->SetXY(10, 200);
                $pdf->MultiCell(165, 9, utf8_decode('DISCIPLINAS APLICADAS'), '', 'L', 0);

                $pdf->SetFont('Arial', '', 15); 
                foreach($disciplinas as $disciplina)
                {
                    $pdf->MultiCell(165, 9, utf8_decode($disciplina), '', 'L', 0);
                }
            }
        }

        $pdf->Output();
    }

    public function add()
    {
        if(request('sent'))
        {
            $certificado = Certificado::create(request()->all());

            $this->syncArquivo($certificado, 'modelo', 'img');

            return redirect()->route('admin.certificado.info', ['id' => $certificado->id])->with('success','Certificado cadastrado com êxito.');
        }

    	return view('admin.certificado.add');
    }

    public function editar($certificado_id)
    {
        $certificado = Certificado::plataforma()->where('id', $certificado_id)->first();

        if(request('sent'))
        {
            $certificado->update(request()->all());

            $this->syncArquivo($certificado, 'modelo', 'img');

            return back()->with('success','Certificado cadastrado com êxito.');
        }

        return view('admin.certificado.editar', compact('certificado') );
    }

    public function syncArquivo($certificado, $tipo, $inputFileName)
    {
        $file = request()->file($inputFileName);

        if($file)
        {
            $path = str_replace(\App\Models\Util::getLinkStorage(), '', $certificado->$tipo);
            Storage::delete($path);

            $diretorio = 'plataformas/'.session('plataforma_id').'/certificados/'.$certificado->id;
            $filename = $certificado->id.'.'.$file->getClientOriginalExtension();

            $path = Storage::putFileAs($diretorio, $file, $filename, 'public');

            $linkStorage = \App\Models\Util::getLinkStorage();
            $caminho = $linkStorage . $path;

            $certificado->update([$tipo => $caminho]);
        }
    }

}