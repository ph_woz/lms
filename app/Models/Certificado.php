<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Certificado extends Model
{
    use HasFactory;

    protected $table = 'certificados';

	public $timestamps = true;

    public $fillable = [
        
        'plataforma_id',
        'cadastrante_id',

        'referencia',

        'modelo',
        'posicao_imagem',

        'nome_posicao_x',
        'nome_posicao_y',
        'nome_fontSize',

        'texto1',
        'texto1_posicao_x',
        'texto1_posicao_y',
        'texto1_fontSize',

        'texto2',
        'texto2_posicao_x',
        'texto2_posicao_y',
        'texto2_fontSize',

        'codigo_posicao_x',
        'codigo_posicao_y',
        'codigo_fontSize',

        'status',
    ];

    public static  function boot()
    {
        parent::boot(); 
        static::creating(function ($model)
        {
            $model->plataforma_id = session('plataforma_id') ?? \Auth()->user()->plataforma_id ?? Plataforma::dominio()->pluck('id')[0] ?? null;
            $model->cadastrante_id = \Auth()->user()->id ?? null;

        });
    }

    public function scopePlataforma($query)
    {
        return $query->where('plataforma_id', session('plataforma_id') ?? Plataforma::dominio()->pluck('id')[0] ?? null);
    }

    public function scopeAtivo($query)
    {
        return $query->where('status', 0);
    }

    public function replaceVariaveis($text, $user_id, $turma_id, $trilha_id)
    {
        $estadoAluno = null;
        $cidadeAluno = null;
        $data_nascimento = null;
        $data_conclusao = null;
        $data_created_trilha = null;

        if($user_id)
        {
            $user = User::plataforma()->find($user_id);

            $endereco = UserEndereco::plataforma()->where('user_id', $user->id)->first();
            if(isset($endereco))
            {
                $estadoAluno = $endereco->estado ?? null;
                $cidadeAluno = $endereco->cidade ?? null;
            }

            $data_nascimento = Util::replaceDatePt($user->data_nascimento) ?? null;
        }

        if($turma_id)
        {
            $curso_id  = Turma::plataforma()->where('id', $turma_id)->pluck('curso_id')[0] ?? null;
            $curso = Curso::plataforma()->where('id', $curso_id)->first();

            $data_conclusao = TurmaAluno::plataforma()->where('turma_id', $turma_id)->where('aluno_id', $user_id)->pluck('data_conclusao_curso')[0] ?? null;
            $data_conclusao = Util::replaceDatePt($data_conclusao);
        }

        if($trilha_id)
        {
            $trilha = Trilha::plataforma()->where('id', $trilha_id)->first();
            $trilhaAluno = TrilhaAluno::plataforma()->where('trilha_id', $trilha->id)->where('aluno_id', $user_id)->first();

            $data_created_trilha = Util::replaceDatePt($trilhaAluno->created_at);
            $data_conclusao = Util::replaceDatePt($trilhaAluno->data_conclusao);
        }

        $text = str_replace('[NOME_ALUNO]', $user->nome ?? '[NOME_ALUNO]', $text);
        $text = str_replace('[CURSO]', $curso->nome ?? '[CURSO]', $text);
        $text = str_replace('[DATA_NASCIMENTO]', $data_nascimento ?? '[DATA_NASCIMENTO]', $text);
        $text = str_replace('[CPF]', $user->cpf ?? '[CPF]', $text);
        $text = str_replace('[RG]', $user->rg ?? '[RG]', $text);
        $text = str_replace('[NATURALIDADE]', $user->naturalidade ?? '[NATURALIDADE]', $text);
        $text = str_replace('[CURSO_CARGA_HORARIA]', $curso->carga_horaria ?? '[CURSO_CARGA_HORARIA]', $text);
        $text = str_replace('[TRILHA_CARGA_HORARIA]', $trilha->carga_horaria ?? '[TRILHA_CARGA_HORARIA]', $text);
        $text = str_replace('[DATA_CREATED_TRILHA]', $data_created_trilha ?? request('data_created_trilha') ?? '[DATA_CREATED_TRILHA]', $text);
        $text = str_replace('[DATA_CONCLUSAO]', $data_conclusao ?? request('data_conclusao') ?? '[DATA_CONCLUSAO]', $text);
        $text = str_replace('[DATA_ATUAL]', date('d/m/Y'), $text);
        $text = str_replace('[ESTADO_ALUNO]', $estadoAluno ?? '[ESTADO_ALUNO]', $text);
        $text = str_replace('[CIDADE_ALUNO]', $cidadeAluno ?? '[CIDADE_ALUNO]', $text);
        $text = str_replace('[TRILHA]', $trilha->nome ?? '[TRILHA]', $text);
        $text = str_replace('\n', PHP_EOL, $text);

        return $text;
    }

}
