<?php

namespace App\Http\Livewire\Admin\Comunicado;

use Livewire\WithPagination;
use Livewire\Component;
use App\Models\Comunicado;
use App\Models\Categoria;
use App\Models\ComunicadoCategoria;

class Lista extends Component
{
    use WithPagination;

    protected $paginationTheme = 'bootstrap';

    public $search;
    public $arquivar = 'N';
    public $ordem;

    protected $queryString = ['search','arquivar','ordem'];

    public function render()
    {
        $comunicados = Comunicado::select('id','assunto')->plataforma()->where('arquivar', $this->arquivar);

        if($this->search)
        {
            $comunicados = $comunicados->where('assunto', 'like', '%'.$this->search.'%');
        }

        $countComunicados = $comunicados->count();


        if($this->ordem == 'desc' || $this->ordem == 'asc')
        {
            $comunicados = $comunicados->orderBy('id', $this->ordem);
        
        } else {

            $comunicados = $comunicados->orderBy('assunto');
        }
        

        $comunicados = $comunicados->paginate(10);


        return view('livewire.admin.comunicado.lista', [
            'comunicados'      => $comunicados,
            'countComunicados' => $countComunicados,
        ]);
    }
}
