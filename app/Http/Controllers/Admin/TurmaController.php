<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Curso;
use App\Models\Turma;
use App\Models\Formulario;
use App\Models\Unidade;
use App\Models\FormaPagamento;
use App\Models\FluxoCaixa;
use App\Models\Chamada;
use App\Models\MensalidadePorSemestre;
use App\Models\FormaIngressoTurma;

class TurmaController extends Controller
{
    public function lista()
    {
        return view('admin.turma.lista');
    }
    
    public function info($turma_id)
    {
        return view('admin.turma.info', compact('turma_id') );
    }

    public function add()
    {
        $cursos       = Curso::plataforma()->ativo()->orderBy('referencia')->get();
        $formularios  = Formulario::plataforma()->ativo()->orderBy('referencia')->get();
        $unidades     = Unidade::plataforma()->ativo()->orderBy('nome')->get();

        if(request('sent'))
        {
            $frequencia = null;

            if(request('frequencia'))
            {
                $frequencia = implode(', ', request('frequencia'));
                
                request()->merge(['frequencia' => $frequencia]);
            }

            $turma = Turma::create(request()->all());

            $this->syncFormasPagamento($turma->id);
            $this->syncMensalidadesPorSemestre($turma->id);
            $this->syncFormasIngresso($turma->id);

            return back()->with('success', 'Turma criada com êxito.')->with('turma_id', $turma->id)->with('turma_nome', $turma->nome);
        }

    	return view('admin.turma.add', compact('cursos','formularios','unidades') );
    }

    public function editar($turma_id)
    {
        $turma = Turma::plataforma()->where('id', $turma_id)->first();

        $cursos          = Curso::plataforma()->ativo()->orderBy('referencia')->get();
        $formularios     = Formulario::plataforma()->ativo()->orderBy('referencia')->get();
        $unidades        = Unidade::plataforma()->ativo()->orderBy('nome')->get();
        $formasPagamento = FormaPagamento::plataforma()->turma()->where('registro_id', $turma_id)->get();
        $formasIngresso  = FormaIngressoTurma::plataforma()->where('turma_id', $turma_id)->get();
        $mensalidadesPorSemestres = MensalidadePorSemestre::plataforma()->turma()->where('registro_id', $turma_id)->get();

        if(request('sent'))
        {
            $frequencia = null;

            if(request('frequencia'))
            {
                $frequencia = implode(', ', request('frequencia'));
                
                request()->merge(['frequencia' => $frequencia]);
            }

            if($turma->nome != request('nome'))
            {
                if(FluxoCaixa::checkActive())
                {
                    FluxoCaixa::plataforma()->turma()->where('produto_id', $turma->id)->update(['produto_nome' => request('nome')]);
                }
            }

            $turma->update(request()->all());

            $this->syncFormasPagamento($turma->id);
            $this->syncMensalidadesPorSemestre($turma->id);
            $this->syncFormasIngresso($turma->id);

            return back()->with('success', 'Turma atualizada com êxito.');
        }

        return view('admin.turma.editar', compact('turma','cursos','formularios','unidades','formasPagamento','formasIngresso','mensalidadesPorSemestres') );
    }

    public function listaDePresenca($turma_id)
    {   
        $turma = Turma::select('id','nome','curso_id')->plataforma()->findOrFail($turma_id);

        $getNomePaginaInterno = 'Listas de Presença: '. $turma->nome;

        $aulas = Chamada::select('data_aula')->plataforma()->groupBy('data_aula')->where('turma_id', $turma_id)->pluck('data_aula')->toArray();

        return view('admin.turma.lista-de-presenca', compact('getNomePaginaInterno','aulas','turma') );
    }

    public function addListaDePresenca($turma_id)
    {
        $turma = Turma::select('id','nome','curso_id')->plataforma()->where('id', $turma_id)->first();

        $curso_id = $turma->curso_id;

        $getNomePaginaInterno = 'Lançar Lista de Presença';

        $users = \App\Models\TurmaAluno::select('users.id','users.nome','users.foto_perfil')
            ->leftJoin('users','turmas_alunos.aluno_id','users.id')
            ->where('turmas_alunos.turma_id', $turma_id)
            ->where('users.status', 0)
            ->where('users.tipo', 'aluno')
            ->where('users.plataforma_id', session('plataforma_id'))
            ->where('turmas_alunos.plataforma_id', session('plataforma_id'))
            ->orderBy('users.nome')
            ->get();

        if(request('sent'))
        {
            $data_aula  = request('data_aula');

            foreach(request('status') ?? array() as $aluno_id => $status)
            {
                Chamada::create([
                    'curso_id'   => $curso_id,
                    'turma_id'   => $turma_id,
                    'aluno_id'   => $aluno_id,
                    'status'     => $status,
                    'data_aula'  => $data_aula,
                ]);
            }

            return back()->with('success','Lista de Presença realizada com êxito.');
        }

        return view('admin.turma.add-lista-de-presenca', compact('users','getNomePaginaInterno','turma') );
    }

    public function editarlistaDePresenca($turma_id, $data_aula)
    {
        $turma = Turma::select('id','nome','curso_id')->plataforma()->where('id', $turma_id)->first();

        $chamadas = Chamada::plataforma()->where('turma_id', $turma_id)->whereDate('data_aula', $data_aula)->get();

        $faltaram = [];
        $compareceram = [];

        foreach($chamadas as $chamada)
        {
            if($chamada->status == 0)
            {
                $faltaram[] = $chamada->aluno_id;
            
            } else {

                $compareceram[] = $chamada->aluno_id;
            }
        }

        $curso_id = $turma->curso_id;

        $getNomePaginaInterno = 'Atualizar Lista de Presença';

        $users = \App\Models\TurmaAluno::select('users.id','users.nome','users.foto_perfil')
            ->leftJoin('users','turmas_alunos.aluno_id','users.id')
            ->where('turmas_alunos.turma_id', $turma_id)
            ->where('users.status', 0)
            ->where('users.tipo', 'aluno')
            ->where('users.plataforma_id', session('plataforma_id'))
            ->where('turmas_alunos.plataforma_id', session('plataforma_id'))
            ->orderBy('users.nome')
            ->get();

        if(request('sent'))
        {
            $data_aula  = request('data_aula');

            Chamada::plataforma()->where('turma_id', $turma_id)->whereDate('data_aula', $data_aula)->delete();

            foreach(request('status') ?? array() as $aluno_id => $status)
            {
                Chamada::create([
                    'curso_id'   => $curso_id,
                    'turma_id'   => $turma_id,
                    'aluno_id'   => $aluno_id,
                    'status'     => $status,
                    'data_aula'  => $data_aula,
                ]);
            }

            return back()->with('success','Lista de Presença realizada com êxito.');
        }

        return view('admin.turma.editar-lista-de-presenca', compact('users','getNomePaginaInterno','turma','chamadas','faltaram','compareceram') );
    }

    public function syncMensalidadesPorSemestre($turma_id)
    {
        MensalidadePorSemestre::plataforma()->turma()->where('registro_id', $turma_id)->delete();

        $mensalidades = array_combine(request('semestres') ?? array(), request('valores_por_semestres') ?? array());

        foreach($mensalidades as $mensalidade => $valor)
        {
            if($mensalidades && $valor)
            {
                MensalidadePorSemestre::create([
                    'plataforma_id' => session('plataforma_id') ?? \Auth::user()->plataforma_id,
                    'registro_id'   => $turma_id,
                    'tipo'          => 'C',
                    'semestre'      => intval($mensalidade),
                    'valor'         => $valor,
                ]);
            }
        }
    }

    public function syncFormasIngresso($turma_id)
    {
        FormaIngressoTurma::plataforma()->where('turma_id', $turma_id)->delete();

        $formasIgresso = array_combine(request('formas') ?? array(), request('valores_formas') ?? array());

        foreach($formasIgresso as $forma => $valor)
        {
            FormaIngressoTurma::create([
                'turma_id' => $turma_id,
                'nome'     => $forma,
                'desconto' => $valor,
            ]);
        }
    }

    public function syncFormasPagamento($turma_id)
    {
        FormaPagamento::plataforma()->turma()->where('registro_id', $turma_id)->delete();

        $formaCartaoDeCredito = array_combine(request('cartao-de-credito')['parcelas'], request('cartao-de-credito')['valores']);
        foreach($formaCartaoDeCredito as $parcela => $valor)
        {
            if($parcela && $valor)
            {
                FormaPagamento::create([
                    'tipo_cadastro' => 'Turma',
                    'registro_id'   => $turma_id,
                    'tipo'          => 'CC',
                    'parcela'       => $parcela,
                    'valor'         => $valor,
                ]);
            }
        }

        $formaCartaoDeDebito = array_combine(request('cartao-de-debito')['parcelas'], request('cartao-de-debito')['valores']);
        foreach($formaCartaoDeDebito as $parcela => $valor)
        {
            if($parcela && $valor)
            {
                FormaPagamento::create([
                    'tipo_cadastro' => 'Turma',
                    'registro_id'   => $turma_id,
                    'tipo'          => 'CD',
                    'parcela'       => $parcela,
                    'valor'         => $valor,
                ]);
            }
        }

        $formaBoleto = array_combine(request('boleto')['parcelas'], request('boleto')['valores']);
        foreach($formaBoleto as $parcela => $valor)
        {
            if($parcela && $valor)
            {
                FormaPagamento::create([
                    'tipo_cadastro' => 'Turma',
                    'registro_id'   => $turma_id,
                    'tipo'          => 'BO',
                    'parcela'       => $parcela,
                    'valor'         => $valor,
                ]);
            }
        }
    }


}