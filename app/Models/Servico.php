<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Servico extends Model
{
    protected $table = 'servicos';

	public $timestamps = true;

    public $fillable = [
        'plataforma_id',
        'cadastrante_id',
        'unidade_id',
        'nome',
        'descricao',
        'status',
        'publicar',
        'duracao',
        'valor',
        'valor_promocional',
        'tipo_valor',
    ];

    public static  function boot()
    {
        parent::boot(); 
        static::creating(function ($model)
        {
            $model->plataforma_id = session('plataforma_id') ?? \Auth::user()->plataforma_id ?? 1;
            $model->cadastrante_id = \Auth::user()->id ?? null;
        });
    }

    public function scopePlataforma($query)
    {
        return $query->where('plataforma_id', session('plataforma_id') ?? \Auth::user()->plataforma_id);
    }

    public function scopeAtivo($query)
    {
        return $query->where('status', 0);
    }

    public static function checkActive()
    {
        $check = PlataformaModulo::plataforma()->ativo()->where('url','servico')->exists();
        
        return $check;
    }
    
}
