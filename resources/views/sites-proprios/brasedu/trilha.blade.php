<!doctype html>
<html lang="pt-br">
<head>

  <!-- Google tag (gtag.js) -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=G-MREB450HCT"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'G-MREB450HCT');
  </script>
  
   <meta charset="utf-8">
   <meta name="viewport" content="width=device-width, initial-scale=1">

   <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/uikit@3.5.9/dist/css/uikit.min.css" />
   <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
   <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
   <link rel="stylesheet" href="{{ asset('css/util.css') }}?v=14">
   <link rel="stylesheet" href="{{ asset('sites-proprios/brasedu/css/home.css') }}?v=23">

   <title>{{ $trilha->seo_title ?? $trilha->nome }}</title>
   <meta name="robots" content="index, follow">
   <meta name="description" content="{{ $trilha->seo_description ?? null }}"/>
   <meta name="keywords" content="{{ $trilha->seo_keywords ?? null }}"/>
   <link rel="canonical" href="{{ Request::url() }}" />
   <link rel="shortcut icon" href="{{ $plataforma->favicon }}">
   <meta property="og:locale" content="pt_BR" />
   <meta property="og:type" content="website" />
   <meta property="og:title" content="{{ $trilha->seo_title ?? $trilha->nome }}" />
   <meta property="og:description" content="{{ $trilha->seo_description ?? null }}" />
   <meta property="og:url" content="{{ Request::url() }}" />
   <meta property="og:site_name" content="{{ $plataforma->nome }}" />
   <meta property="og:image" content="{{ $plataforma->logotipo }}" />
   <meta name='author' content='WOZCODE | Pedro Henrique' />

</head>
<body class="bg-f4" id="inicio">

   @include('sites-proprios/brasedu/componentes/header')

   <main class="pt-9rem">

      <section>
         <div class="w-100 h-lg-300 h-xs-40vh" style="background-size: cover; background-position: center; background-image: url('{{ $trilha->foto_capa ?? asset('sites-proprios/cedetep/images/footer.jpg') }}">
            <div class="position-relative h-100 px-xl-7 px-3">
            <div class="background-overlay h-100" style="background: rgba(0,0,0,1);"></div>

               <div class="row align-items-center h-100 position-relative">
                  <div class="mt-2">
                     <h1 class="text-light font-poppins weight-800 fs-xs-35 fs-md-50 text-shadow-3">
                        <span id="trilhaTiulo">{!! $trilha->nome !!}</span>
                     </h1>
                  </div>
               </div>
            </div>
         </div>
      </section>

      <section class="pb-lg-3">
         <div class="row container-fluid px-3 gx-0 gx-lg-4 px-xl-6 mt--15px position-relative pb-5">

            <div class="col-lg-4 mb-4 mb-lg-0">
               
               <div class="bg-footer px-4-5 pt-4-5 pb-4-5 box-shadow rounded-top-20 box-shadow">
                  
                  <div class="text-center">              
                     <a href="/f/{{$trilha->formulario_id ?? 0}}/t/{{$trilha->id}}" target="_blank" class="btnCursoInscreverSe animation-grow box-shadow-warning hover-d-none">
                        FAÇA SUA PRÉ-INSCRIÇÃO
                     </a>
                  </div>

               </div>

               <div class="bg-white px-4-5 pt-4-5 pb-4-5 rounded-top-0 rounded-bottom-20 box-shadow">

                  <div class="bg-investimento px-4-5 pt-4 pb-5 rounded box-shadow">

                     <h1 class="p-0 m-0 text-light font-inter text-center fs-25 text-shadow-1 weight-700">Investimento</h1>

                     <hr class="border">

                     <p class="mb-3 weight-700 font-montserrat text-dark fs-17 text-shadow-1" style="text-decoration: line-through;">
                        De R$ {{ $trilha->valor_a_vista ?? '0.000,00' }}
                     </p>
                     <span class="text-light weight-600 font-montserrat d-block" style="margin-bottom: -10px;">Por</span>
                     <div class="d-flex">
                        <p class="m-0 pe-2 fs-40 font-montserrat weight-700 text-light ls-05 text-shadow-1">
                           R$
                        </p>
                        <p class="mb-0 fs-40 font-montserrat weight-800 text-light ls-05 text-shadow-1">
                           {{ $trilha->valor_promocional }}
                        </p>
                     </div>

                  </div>

                  <div class="text-center mt--20px">
                     <span class="color-promo bg-promo px-3-5 py-2 rounded box-shadow weight-600 font-poppins ls-05 fs-18">
                        PROMOCIONAL
                     </span>
                  </div>

                  <div class="pt-5">
                     
                     <h1 class="weight-600 font-roboto weight-800 fs-22 text-center color-titulo">Formas de Pagamento</h1>

                     <div class="w-100 box-shadow mb-4-5 d-flex align-items-center">
                        <button type="button" class="btn-forma-pg-cc bg-forma-pg border-forma-pg w-100 weight-700 font-poppins text-shadow-1 fs-18 ls-05 px-3 rounded-0 height-categorias text-light">
                           Cartão de Crédito
                        </button>
                        <button type="button" class="btn-forma-pg-bo border-forma-pg color-forma-pg w-100 weight-700 font-poppins bg-light fs-18 ls-05 px-3 rounded-0 height-categorias text-light">
                           Boleto
                        </button>
                     </div>

                     <div>

                        @php
                           $formasPagamento = \App\Models\FormaPagamento::plataforma()->trilha()->where('registro_id', $trilha->id)->get();
                        @endphp

                        <div id="boxFormaPagamentoCC"> 
                           @foreach ($formasPagamento->where('tipo','CC') as $formaPagamento)
                              <div class="bg-f4 mb-1 px-3-5 py-2 rounded-2">
                                 <p class="m-0 font-montserrat ls-05 weight-800 fs-20 text-center">
                                    {{ $formaPagamento->parcela }}x de R$ {{ $formaPagamento->valor }}
                                 </p>
                              </div>
                           @endforeach
                        </div>

                        <div id="boxFormaPagamentoBoleto" style="display: none;">
                           @foreach($formasPagamento->where('tipo','BO') as $formaPagamento)
                              <div class="bg-f4 mb-1 px-3-5 py-2 rounded-2">
                                 <p class="m-0 font-montserrat ls-05 weight-800 fs-20 text-center">
                                 {{ $formaPagamento->parcela }}x de R$ {{ $formaPagamento->valor }}
                                 </p>
                              </div>
                           @endforeach
                        </div>

                     </div>

                  </div>

               </div>
            </div>

            <div class="col-lg-8">

               <div class="mb-4 bg-white px-lg-5 px-4 pt-4-5 pb-4-5 box-shadow border-top rounded-20">

                  <p class="mb-0 pb-0 font-poppins weight-600 fs-14 color-666">
                     Informações do Curso
                  </p>

                  <hr class="border border-secondary">

                     @if($trilha->video)
                        <iframe width="100%" height="225" class="rounded mb-4" src="{{ $trilha->video }}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                     @endif

                     <div class="d-lg-flex justify-content-between">
                        <div>                            
                           <p class="fs-15 mb-0 weight-600 font-poppins color-555">NÍVEL:</p>
                           <p class="fs-14 mb-0 weight-600 font-poppins color-444">{{ $trilha->nivel ?? 'Não definido' }}</p>
                        </div>

                        <div class="my-3 my-lg-0">                               
                           <p class="fs-15 mb-0 weight-600 font-poppins color-555">TIPO DE FORMAÇÃO:</p>
                           <p class="fs-14 mb-0 weight-600 font-poppins color-444">{{ $trilha->tipo_formacao ?? 'Não definido' }}</p>
                        </div>
                        
                        <div>                                
                           <p class="fs-15 mb-0 weight-600 font-poppins color-555">CARGA HORÁRIA:</p>
                           <p class="fs-14 mb-0 weight-600 font-poppins color-444">{{ $trilha->carga_horaria ?? '0.000' }} horas</p>
                        </div>
                     </div>

               </div>

               <div class="bg-white px-lg-5 px-4 pt-4-5 pb-4-5 box-shadow border-top rounded-20">

                  <p class="mb-0 pb-0 font-poppins weight-600 fs-14 color-666">
                     Sobre o curso
                  </p>

                  <hr class="border border-secondary">

                  @if($trilha->descricao == null)
                     <p class="mb-0 pb-0 font-poppins weight-600 fs-13 color-666">
                        <i class="fas fa-exclamation-circle"></i> Descrição do Curso não definida.
                     </p>
                  @endif

                  <p id="trilhaDescricao">
                     {!! nl2br($trilha->descricao) !!}
                  </p>

               </div>

               @if(count($turmas) > 0)
               <div class="mt-4 bg-white px-lg-5 px-4 pt-4-5 pb-4-5 box-shadow border-top rounded-20">

                  <p class="mb-0 pb-0 font-poppins weight-600 fs-14 color-666">
                     Disciplinas
                  </p>

                  <hr class="border border-secondary">

                  @foreach($turmas as $turma)
                     
                     <p id="turmaDisciplina" class="fs-14 mb-0 weight-600 font-poppins color-444">
                        {{ $turma->nome }}
                     </p>

                     <hr class="border border-secondary">

                  @endforeach

               </div>
               @endif
               
            </div>

         </div>
      </section>

      <section id="newsletter" class="py-5 bg-newsletter">
         <div class="container-fluid px-xl-7" id="contato">

            @if(session('success'))
               <div class='alert alert-success'><b>Sucesso!</b> {{ session('success') }}</div>
            @endif

            <div class="row justify-content-between align-items-center">

               <div class="col-lg-4">
                  <p class="mb-0 weight-700 text-shadow-2 fs-25 pe-lg-5 font-roboto">
                     <span class="text-white">Que tal receber nosso contato para tirar todas as suas dúvidas?</span><span class="color-amarelo"> Cadastre-se!</span>
                  </p>
               </div>

               <form method="POST" class="col-lg-8 mt-3 mt-lg-0">
               @csrf

                  <input type="hidden" name="assunto" value="Solicitar Contato">

                  <div class="d-lg-flex w-100">

                     <input type="text" name="nome" class="w-100 form-control lead-form rounded-left rounded-right-lg-0" placeholder="NOME" required>
                     
                     <div class="d-xs-none line-form"></div>
                     
                     <input type="tel" name="celular" class="my-2 my-lg-0 w-100 form-control lead-form rounded-lg-0" placeholder="(DDD) CELULAR" required>
                     
                     <div class="d-xs-none line-form"></div>

                     <input type="email" name="email" class="w-100 form-control lead-form rounded-lg-0" placeholder="EMAIL" required>

                     <input type="text" name="check" class="invisible w-0px h-0px" placeholder="Não preencha este campo.">

                     <button type="submit" class="btn-news" name="sentContato" value="S">
                        Enviar
                     </button>

                  </div>

               </form>
            </div>

         </div>
      </section>

      <a href="https://api.whatsapp.com/send?phone=55{{preg_replace('/\D/', '', $plataforma->whatsapp)}}&text=Olá, vim através do Site {{ $plataforma->nome }}." target="_blank" class="text-white fs-30">
            <div class="position-fixed bottom-0 right-0 me-3 me-lg-4 mb-3 mb-lg-4 z-4 wpp-pulse-button z-8">
            <i class="fab fa-whatsapp"></i>
            </div>
      </a>

   </main>

   @include('sites-proprios/brasedu/componentes/footer')

   <script>

      $(".btn-forma-pg-bo").click(function() {

        $(".btn-forma-pg-cc").removeClass('bg-forma-pg').removeClass('text-light').addClass('bg-light').addClass('color-forma-pg');
        $(this).removeClass('bg-light').removeClass('color-forma-pg').addClass('bg-forma-pg').addClass('text-light');

        $("#boxFormaPagamentoCC").hide();
        $("#boxFormaPagamentoBoleto").show();
      });

      $(".btn-forma-pg-cc").click(function() {

        $(".btn-forma-pg-bo").removeClass('bg-forma-pg').removeClass('text-light').addClass('bg-light').addClass('color-forma-pg');
        $(this).removeClass('bg-light').removeClass('color-forma-pg').addClass('bg-forma-pg').addClass('text-light');

        $("#boxFormaPagamentoCC").show();
        $("#boxFormaPagamentoBoleto").hide();
      });

    </script>

</body>
</html>