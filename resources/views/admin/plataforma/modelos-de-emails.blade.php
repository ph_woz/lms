@extends('layouts.admin.master')
@section('content')
    
<div>

   <form method="POST" class="mb-4">
   @csrf
    
      <section class="mb-4-5">
         
         <a href="#boxInfoPrincipal" data-bs-toggle="collapse" aria-expanded="false" class="hover-d-none">
           <div class="section-title">
             Emails
           </div>
         </a>
         <hr/>
         
         <div class="collapse show" id="boxInfoPrincipal">

             <div class="d-lg-flex mb-3">

               <div class="w-100 me-lg-3 mb-3 mb-lg-0">
                  <label>Email de Contração de Trilha</label>
                  <textarea name="email_contratacao_trilha" class="descricao">{{ $modeloDeEmails->email_contratacao_trilha }}</textarea>
               </div>

               <div class="w-100 me-lg-3 mb-3 mb-lg-0">
                  <label>Email de Contração de Turma</label>
                  <textarea name="email_contratacao_turma" class="descricao">{{ $modeloDeEmails->email_contratacao_turma }}</textarea>
               </div>

            </div>

         </div>

         <button type="submit" name="sent" value="ok" class="btn-add mt-3">Salvar Alterações</button> 

      </section>

   </form>

</div>

@endsection

@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>
<script src="https://cdn.tiny.cloud/1/f50dygt4s4q2k3urd4bh4c2hkjb74m6exr798tjwbayaa0om/tinymce/5/tinymce.min.js"></script>
<script>
   tinymce.init({
      selector: '.descricao',
      language: 'pt_BR',
      height: 300,
      plugins: 'image lists link code',
      toolbar: "image | undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link | code"
   });
</script>
@endpush