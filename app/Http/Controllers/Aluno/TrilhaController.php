<?php

namespace App\Http\Controllers\Aluno;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Trilha;
use App\Models\TrilhaTurma;
use DB;

class TrilhaController extends Controller
{
    public function trilha($slug, $id)
    {
        $trilha = Trilha::plataforma()->ativo()->findOrFail($id);

        $getNomePage = $trilha->nome;

        $user_id = \Auth::id();
        $plataforma_id = session('plataforma_id') ?? \Auth::user()->plataforma_id;

        $turmasIds = TrilhaTurma::plataforma()->where('trilha_id', $trilha->id)->select('turma_id')->get()->pluck('turma_id')->toArray();
        $check_data_conclusao = \App\Models\TrilhaAluno::plataforma()->where('trilha_id', $trilha->id)->where('aluno_id', $user_id)->whereNotNull('data_conclusao')->exists();

        $turmasEmAndamento = DB::table('turmas_alunos')
            ->join('cursos','turmas_alunos.curso_id','cursos.id')
            ->join('turmas','turmas_alunos.turma_id','turmas.id')
            ->where('cursos.status', 0)
            ->where('turmas.status', 0)
            ->whereNotNull('turmas_alunos.data_ultimo_acesso')
            ->whereNull('turmas_alunos.data_conclusao_curso')
            ->where('turmas_alunos.aluno_id', $user_id)
            ->where('cursos.plataforma_id', $plataforma_id)
            ->where('turmas.plataforma_id', $plataforma_id)
            ->where('turmas_alunos.plataforma_id', $plataforma_id)
            ->where('turmas_alunos.status', 0)
            ->whereIn('turmas.id', $turmasIds)
            ->select(
                'cursos.id',
                'cursos.nome',
                'cursos.slug',
                'cursos.foto_miniatura'
            )
            ->orderBy('turmas_alunos.data_ultimo_acesso')
            ->get();

        $turmasDisponiveis = DB::table('turmas_alunos')
            ->join('cursos','turmas_alunos.curso_id','cursos.id')
            ->join('turmas','turmas_alunos.turma_id','turmas.id')
            ->where('cursos.status', 0)
            ->where('turmas.status', 0)
            ->whereNull('turmas_alunos.data_primeiro_acesso')
            ->where('turmas_alunos.aluno_id', $user_id)
            ->where('cursos.plataforma_id', $plataforma_id)
            ->where('turmas.plataforma_id', $plataforma_id)
            ->where('turmas_alunos.plataforma_id', $plataforma_id)
            ->where('turmas_alunos.status', 0)
            ->whereIn('turmas.id', $turmasIds)
            ->select(
                'cursos.id',
                'cursos.nome',
                'cursos.slug',
                'cursos.foto_miniatura'
            )
            ->orderBy('turmas_alunos.data_ultimo_acesso')
            ->get();

        $turmasConcluidas = DB::table('turmas_alunos')
            ->join('cursos','turmas_alunos.curso_id','cursos.id')
            ->join('turmas','turmas_alunos.turma_id','turmas.id')
            ->where('cursos.status', 0)
            ->where('turmas.status', 0)
            ->whereNotNull('turmas_alunos.data_conclusao_curso')
            ->where('turmas_alunos.aluno_id', $user_id)
            ->where('cursos.plataforma_id', $plataforma_id)
            ->where('turmas.plataforma_id', $plataforma_id)
            ->where('turmas_alunos.plataforma_id', $plataforma_id)
            ->where('turmas_alunos.status', 0)
            ->whereIn('turmas.id', $turmasIds)
            ->select(
                'cursos.id',
                'cursos.nome',
                'cursos.slug',
                'cursos.foto_miniatura'
            )
            ->orderBy('turmas_alunos.data_ultimo_acesso')
            ->get();

        // progresso
        if(count($turmasIds) == 0)
        {
            $progresso = 0;

        } else {

            $progresso = round((count($turmasConcluidas)*100/count($turmasIds)), 1);
        }


        $checkComunicadosNaoLidos = \App\Models\ComunicadoAluno::plataforma()->where('aluno_id', $user_id)->whereNotNull('data_visualizacao')->exists();

        return view('aluno.trilha', 
            compact(
                'trilha',
                'getNomePage',
                'progresso',
                'check_data_conclusao',
                'turmasEmAndamento',
                'turmasDisponiveis',
                'turmasConcluidas',
                'checkComunicadosNaoLidos'
            ) 
        );
    }

}