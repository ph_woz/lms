<!doctype html>
<html lang="pt-br">
<head>

  <!-- Google tag (gtag.js) -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=G-MREB450HCT"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'G-MREB450HCT');
  </script>
   
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/uikit@3.5.5/dist/css/uikit.min.css" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/5.0.0-alpha1/css/bootstrap.min.css" integrity="sha384-r4NyP46KrjDleawBgD5tp8Y7UzmLA05oM1iAEQ17CSuDqnUK2+k9luXQOfXJCJ4I" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('sites-proprios/funesp/css/home.css') }}">
    <link rel="stylesheet" href="https://lmc.wozcode.com/css/util.css?v=2">

    <title>{{ $seo->title ?? $plataforma->nome }}</title>
    <meta name="robots" content="index, follow">
    <meta name="description" content="{{ $seo->description ?? null }}"/>
    <meta name="keywords" content="{{ $seo->keywords ?? null }}"/>
    <link rel="canonical" href="{{ Request::url() }}" />
    <link rel="shortcut icon" href="{{ $plataforma->favicon }}">
    <meta property="og:locale" content="pt_BR" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="{{ $seo->title ?? $plataforma->nome }}" />
    <meta property="og:description" content="{{ $seo->description ?? null }}" />
    <meta property="og:url" content="{{ Request::url() }}" />
    <meta property="og:site_name" content="{{ $plataforma->nome }}" />
    <meta property="og:image" content="{{ $plataforma->logotipo }}" />
    <meta name='author' content='WOZCODE | Pedro Henrique' />

</head>
<body id="inicio">

   <header>

      <div uk-parallax="bgy: -250" class="home-header uk-preserve-color">
        <div class="background-overlay"></div>
         <div class="d-none d-md-block">
            <div uk-sticky="animation: uk-animation-slide-top; sel-target: .uk-navbar-container; cls-active: uk-navbar-sticky; cls-inactive: uk-navbar-transparent uk-light; top: 550">
               <nav class="uk-navbar-container">
                  <div class="uk-container">
                     <div uk-navbar>
                        <ul class="uk-navbar-nav">
                           <li class="uk-active">
                              <a href="#inicio" uk-scroll>
                                  <img src="{{ $plataforma->logotipo }}" width="55" alt="Logotipo">
                              </a>
                           </li>
                        </ul>
                        <ul class="uk-navbar-nav ml-auto z-3">
                           <li><a href="#inicio" uk-scroll="offset:40;" class="scroll-link">Início</a></li>
                           <li><a href="#supletivo" uk-scroll="offset:60;" class="scroll-link">Supletivo</a></li>
                           <li><a href="#quem-somos" uk-scroll="offset: 80;" class="scroll-link">Quem somos</a></li>
                           <li><a href="#contato" uk-scroll="offset: 50;" class="scroll-link">Fale Conosco</a></li>
                           <li><a href="https://canaleducacional.com.br/f/formulario-ifope/8/t/22" target="_blank" class="scroll-link">Matricule-se</a></li>
                           <li><a href="https://canaleducacional.com.br/login" class="scroll-link" target="_blank"><i class="fa fa-user-graduate mr-2"></i> Área do Aluno</a></li>
                        </ul>
                     </div>
                  </div>
               </nav>
            </div>
         </div>
      </div>

     <nav class="navbar navbar-expand-lg navbar-light bg-white fixed-top d-md-none d-block shadow">
       <div class="container-fluid">
         <a class="navbar-brand" href="#inicio" uk-scroll>
            <img src="{{ $plataforma->logotipo }}" width="40" alt="Logotipo">
         </a>
         <button class="navbar-toggler border-0 outline-0" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
           <span class="fa fa-bars"></span>
         </button>
         <div class="collapse navbar-collapse pt-4" id="navbarSupportedContent">
           <ul class="navbar-nav mr-auto mb-2 mb-lg-0">
             <li class="nav-item">
               <a class="nav-link" href="#inicio" uk-scroll="offset: 55;" data-toggle="collapse" data-target="#navbarSupportedContent">Início</a>
             </li>
             <li class="nav-item">
               <a class="nav-link" href="#supletivo" uk-scroll="offset: 55;" data-toggle="collapse" data-target="#navbarSupportedContent">Supletivo</a>
             </li>
             <li class="nav-item">
               <a class="nav-link" href="#quem-somos" uk-scroll="offset: 55;" data-toggle="collapse" data-target="#navbarSupportedContent">Quem somos</a>
             </li>
             <li class="nav-item">
               <a class="nav-link" href="#contato" uk-scroll="offset: 55;" data-toggle="collapse" data-target="#navbarSupportedContent">Fale Conosco</a>
             </li>
             <li class="nav-item">
               <a class="nav-link" href="https://canaleducacional.com.br/f/formulario-ifope/8/t/22">Matricule-se</a>
             </li>
             <li class="nav-item">
               <a class="nav-link" href="https://funesp.eadforma.com.br/" target="_blank">Cursos Profissionalizantes</a>
             </li>
             <li class="nav-item">
               <a class="nav-link" href="https://canaleducacional.com.br/login" target="_blank">
                 <i class="fa fa-user-graduate mr-2"></i> Área do Aluno
               </a>
             </li>
           </ul>
         </div>
       </div>
     </nav>
   
   </header>

   <main class="overflow-hidden">

      <section class="position-absolute w-100" style="top: 10%;">
        <div class="uk-section uk-light">
           <div class="uk-container pt-lg-3">
              <div class="row align-items-center">

                <div class="col-lg-6">

                  <h1 class="fs-md-60 fs-xs-30 text-white text-shadow-3 font-nunito weight-800 pt-4">
                    Conclua seus Estudos
                  </h1>
                  <p class="mt-4 mb-0 font-nunito weight-800 fs-23 text-shadow-3 text-white ls-02">
                    Em poucos passos comece hoje mesmo a Estudar. Com Agilidade, Facilidade e sem Burocracia.
                  </p>
                  
                  <div class="mt-2 mt-lg-3">
                    <a href="https://canaleducacional.com.br/f/formulario-ifope/8/t/22" target="_blank" class="myButton shadow rounded hover-d-none bg-base bg-base-hover">
                      QUERO ME MATRICULAR
                    </a>
                  </div>

                </div>

                <div class="col-lg-6 mt-5 mt-lg-0">
                  <div class="card border-0 shadow">
                    <div class="card-header bg-base">
                        <p class="mb-0 font-nunito weight-800 text-center fs-md-30 fs-xs-20 color-light-085 text-shadow-1">
                          Dúvidas? Solicite em contato!
                        </p>
                    </div>
                    <div class="card-body bg-roxo">
                      <form method="POST" id="formSolicitarContato">
                        @csrf

                        <input type="hidden" name="assunto" value="Solicitar Contato">

                        @if(session('success_prematricula'))
                           <div class='alert alert-success'><b>Sucesso!</b> {{ session('success_prematricula') }}</div>
                        @endif

                        <div class="input-group mb-2">
                          <div class="input-group-prepend">
                            <div class="input-group-text rounded-right-0 bg-white" style="padding-right: 13.5px; padding-top: 17px;padding-bottom: 17px;">
                              <i class="fa fa-user"></i>
                            </div>
                          </div>
                          <input type="text" name="nome" class="form-control py-2-5 outline-0 rounded-2" placeholder="Seu Nome" required>
                        </div>

                        <div class="input-group mb-2">
                          <div class="input-group-prepend">
                            <div class="input-group-text rounded-right-0 bg-white" style="padding-top: 17px;padding-bottom: 17px;">
                              <i class="fa fa-phone"></i>
                            </div>
                          </div>
                          <input type="tel" name="celular" class="telefone rounded-left-0 form-control py-2-5 outline-0 rounded-2" placeholder="Telefone" required>
                        </div>

                        <div class="input-group mb-0">
                          <div class="input-group-prepend">
                            <div class="input-group-text rounded-right-0 bg-white" style="padding-top: 17px;padding-bottom: 17px;">
                              <i class="fa fa-envelope"></i>
                            </div>
                          </div>
                          <input type="email" name="email" class="rounded-left-0 form-control py-2-5 outline-0 rounded-2" placeholder="Email" required>
                        </div>

                        <input type="text" name="check" class="invisible h-0px" placeholder="Não preencha este campo.">

                        <input type="submit" name="sentContato" value="Solicitar Contato" class="w-100 border-0 rounded-3 bg-base bg-base-hover py-2 px-3 fs-18 font-raleway weight-600 text-white shadow">
                                                
                      </form>
                    </div>
                  </div>
                </div>  

              </div>
           </div> 
        </div>
      </section>

      <section class="pt-5-5 pb-5-5" id="como-funciona">
         <div class="container-fluid px-md-5 mt-xs-200">

            <h1 class="fs-40 text-center font-raleway text-dark ls-2 weight-800">
              COMO FUNCIONA?
            </h1>

            <div class="flex-center mt-4-5 mb-4-5"><div class="w-80px h-2px bg-base rounded"></div></div>

            <p class="text-center text-dark text-shadow-1 font-opensans fs-19 mt-4 pb-5 col-md-8 offset-md-2">
              O Colégio Axioma tem o ideal de promover a possibilidade de pessoas concluirem o Ensino Médio que acabou ficando pelo caminho. Além disso, o diploma fornecido no final dos estudos terá um papel fundamental em abrir novas portas.
            </p>

            <div class="uk-child-width-1-4@m" uk-grid uk-scrollspy="cls: uk-animation-slide-bottom; target: .uk-card; delay: 300;">
               <div>
                  <div class="uk-card flex-center flex-column">
                    <div class="rounded-circle border-profile border-hover-base square-100 flex-center mb-3">
                      <i class="fa fa-desktop fs-35"></i>
                      <i class="fa fa-mobile-alt fs-35 ml-1"></i>
                    </div>
                    <h3 class="font-montserrat weight-500 color-base fs-18">Estudo 100% Online</h3>
                  </div>
               </div>
               <div>
                  <div class="uk-card flex-center flex-column">
                    <div class="rounded-circle border-profile border-hover-base square-100 flex-center mb-3">
                      <i class="fas fa-file-signature fs-50 ml-3"></i>
                    </div>
                    <h3 class="font-montserrat weight-500 color-base fs-18">Reconhecida Pelo MEC</h3>
                  </div>
               </div>
               <div>
                  <div class="uk-card flex-center flex-column">
                    <div class="rounded-circle border-profile border-hover-base square-100 flex-center mb-3">
                      <i class="far fa-clock fs-50"></i>
                    </div>
                    <h3 class="font-montserrat weight-500 color-base fs-18">Faça seu Próprio Horário</h3>
                  </div>
               </div>
               <div>
                  <div class="uk-card flex-center flex-column">
                    <div class="rounded-circle border-profile border-hover-base square-100 flex-center mb-3">
                      <i class="far fa-flag fs-50"></i>
                    </div>
                    <h3 class="font-montserrat weight-500 color-base fs-18">Validade Nacional</h3>
                  </div>
               </div>
            </div>

         </div>   
      </section>

      <section class="pt-5-5 pb-5-5 border-top border-bottom bg-f5" id="supletivo">
         <div class="container-fluid px-xl-7 bg-supletivo">

            <h1 class="fs-40 text-center font-raleway text-dark ls-2 weight-800">
              O SUPLETIVO
            </h1>

            <div class="flex-center mt-4-5 mb-4-5"><div class="w-80px h-2px bg-base rounded"></div></div>

            <div class="row align-items-center">

              <div class="col-lg-5">

                <p class="font-opensans fs-17">
                  O principal objetivo do curso é avaliar as habilidades e competências básicas de jovens e adultos que não tiveram oportunidade de acesso à escolaridade regular na idade apropriada.
                </p>

                <p class="font-opensans fs-17">
                  Dessa forma, o participante se submete a uma prova e, alcançando a média mínima exigida, obtém a certificação de conclusão daquela etapa educacional.
                </p>

                <p class="font-opensans fs-17">
                  O exame também se propõe a oferecer às secretarias de Educação uma avaliação que lhes permita aferir os conhecimentos e habilidades dos participantes no nível de conclusão do Ensino Fundamental e do Ensino Médio.
                </p>

                <a href="#motivos" uk-scroll="offset: 40;" class="myButton shadow rounded hover-d-none bg-base bg-base-hover">
                  SABER MAIS
                </a>

              </div>
              
              <div class="col-lg-7" uk-scrollspy="cls: uk-animation-slide-right; target: #img; delay: 300;">
                <img src="https://eaducativa.com/wp-content/uploads/2019/05/treinamento-empresas.png" alt="Supletivo Colégio Axioma" id="img">
              </div>

            </div>

         </div>   
      </section>

      <section class="bg-white pt-5-5 pb-5 px-lg-5 px-1" id="motivos">
         <div class="container-fluid px-md-5 pb-lg-5">

            <h1 class="fs-40 text-center font-raleway text-dark weight-800 text-uppercase">
              Motivos para Terminar os Estudos
            </h1>

            <div class="flex-center mt-4-5 mb-4-5"><div class="w-80px h-2px bg-base rounded"></div></div>

            <div class="row pt-4">
              
               <div class="col-lg-6 mb-4 mb-lg-0">
                  
                  <p class="mb-4 text-center font-raleway color-base weight-800 text-uppercase fs-22">
                    COM ENSINO MÉDIO COMPLETO
                  </p>

                  <div class="d-flex flex-column">
                     
                     <div class="d-flex pr-lg-5 mb-4 mb-lg-0">
                        <div class="flex-column d-flex align-items-center">
                           <div class="bg-base square-55 rounded-circle shadow flex-center">
                            <i class="far fa-thumbs-up fs-22 text-white"></i>
                           </div>
                           <div class="d-none d-md-block">
                              <hr class="uk-divider-vertical border border-secondary h-lg-50 m-0 p-0">
                           </div>
                        </div>
                        <div class="ml-3 mt-lg-2-5">
                           <h1 class="display-7 weight-600 text-dark">
                              Oportunidades no Mercado de Trabalho
                           </h1>
                        </div>
                     </div>
                     <div class="d-flex pr-lg-5 mb-4 mb-lg-0">
                        <div class="flex-column d-flex align-items-center">
                           <div class="bg-base square-55 rounded-circle shadow flex-center">
                            <i class="far fa-thumbs-up fs-22 text-white"></i>
                           </div>
                           <div class="d-none d-md-block">
                              <hr class="uk-divider-vertical border border-secondary h-lg-50 m-0 p-0">
                           </div>
                        </div>
                        <div class="ml-3 mt-2-5">
                           <h1 class="display-7 weight-600 text-dark">
                              Iniciar uma Faculdade
                           </h1>
                        </div>
                     </div>
                     <div class="d-flex pr-lg-5 mb-4 mb-lg-0">
                        <div class="flex-column d-flex align-items-center">
                           <div class="bg-base square-55 rounded-circle shadow flex-center">
                            <i class="far fa-thumbs-up fs-22 text-white"></i>
                           </div>
                           <div class="d-none d-md-block">
                              <hr class="uk-divider-vertical border border-secondary h-lg-50 m-0 p-0">
                           </div>
                        </div>
                        <div class="ml-3 mt-lg-2-5">
                           <h1 class="display-7 weight-600 text-dark">
                              Iniciar um Curso Técnico
                           </h1>
                        </div>
                     </div>
                     <div class="d-flex pr-lg-5 mb-4 mb-lg-0">
                        <div class="flex-column d-flex align-items-center">
                           <div class="bg-base square-55 rounded-circle shadow flex-center">
                            <i class="far fa-thumbs-up fs-22 text-white"></i>
                           </div>
                           <div class="d-none d-md-block">
                              <hr class="uk-divider-vertical border border-secondary h-lg-50 m-0 p-0">
                           </div>
                        </div>
                        <div class="ml-3 mt-lg-2-5">
                           <h1 class="display-7 weight-600 text-dark">
                              Participar de concursos públicos
                           </h1>
                        </div>
                     </div>
                     <div class="d-flex pr-lg-5 mb-4 mb-lg-0">
                        <div class="flex-column d-flex align-items-center">
                           <div class="bg-base square-55 rounded-circle shadow flex-center">
                            <i class="far fa-thumbs-up fs-22 text-white"></i>
                           </div>
                           <div class="d-none d-md-block">
                              <hr class="uk-divider-vertical border border-secondary h-lg-50 m-0 p-0">
                           </div>
                        </div>
                        <div class="ml-3 mt-2-5">
                           <h1 class="display-7 weight-600 text-dark">
                              Realizar o ENEM
                           </h1>
                        </div>
                     </div>
                     <div class="d-flex pr-lg-5 mb-4 mb-lg-0">
                        <div class="flex-column d-flex align-items-center">
                           <div class="bg-base square-55 rounded-circle shadow flex-center">
                            <i class="far fa-thumbs-up fs-22 text-white"></i>
                           </div>
                           <div class="d-none d-md-block">
                              <hr class="uk-divider-vertical border border-secondary h-lg-50 m-0 p-0">
                           </div>
                        </div>
                        <div class="ml-3 mt-1">
                           <h1 class="display-7 weight-600 text-dark">
                              Crescer dentro de Empresas e melhora na Qualidade de Vida
                           </h1>
                        </div>
                     </div>
                     <div class="d-flex pr-lg-5 mb-4 mb-lg-0">
                        <div class="flex-column d-flex align-items-center">
                           <div class="bg-base square-55 rounded-circle shadow flex-center">
                            <i class="far fa-thumbs-up fs-22 text-white"></i>
                           </div>
                           <div class="d-none d-md-block">
                              <hr class="uk-divider-vertical border border-secondary h-lg-50 m-0 p-0">
                           </div>
                        </div>
                        <div class="ml-3 mt-lg-2-5">
                           <h1 class="display-7 weight-600 text-dark">
                              Oportunidades de Estudar no Exterior
                           </h1>
                        </div>
                     </div>
                     <div class="d-flex pr-lg-5 mb-4 mb-lg-0">
                        <div class="flex-column d-flex align-items-center">
                           <div class="bg-base square-55 rounded-circle shadow flex-center">
                            <i class="far fa-thumbs-up fs-22 text-white"></i>
                           </div>
                           <div class="d-none d-md-block">
                              <hr class="uk-divider-vertical border border-secondary h-lg-50 m-0 p-0">
                           </div>
                        </div>
                        <div class="ml-3 mt-lg-2-5">
                           <h1 class="display-7 weight-600 text-dark">
                              Conhecimento PARA TODA A VIDA!
                           </h1>
                        </div>
                     </div>
                     <div class="d-flex pr-lg-5 mb-4 mb-lg-0">
                        <div class="flex-column d-flex align-items-center">
                           <div class="bg-base square-55 rounded-circle shadow flex-center">
                            <i class="far fa-thumbs-up fs-22 text-white"></i>
                           </div>
                        </div>
                        <div class="ml-3 mt-2-5">
                           <h1 class="display-7 weight-600 text-dark">
                              E muito mais
                           </h1>
                        </div>
                     </div>

                  </div>

               </div>

               <div class="col-lg-6">

                  <p class="mb-4 ml-lg-5 text-xs-center font-raleway text-danger weight-800 text-uppercase fs-22">
                    SEM O ENSINO MÉDIO COMPLETO
                  </p>

                  <div class="d-flex flex-column">
                     
                     <div class="d-flex pr-lg-5 mb-4 mb-lg-0">
                        <div class="flex-column d-flex align-items-center">
                           <div class="bg-danger square-55 rounded-circle shadow flex-center">
                            <i class="far fa-thumbs-down fs-22 text-white"></i>
                           </div>
                           <div class="d-none d-md-block">
                              <hr class="uk-divider-vertical border border-secondary h-lg-50 m-0 p-0">
                           </div>
                        </div>
                        <div class="ml-3 mt-lg-2-5">
                           <h1 class="display-7 weight-600 text-dark">
                              Perde oportunidades de trabalho
                           </h1>
                        </div>
                     </div>
                     <div class="d-flex pr-lg-5 mb-4 mb-lg-0">
                        <div class="flex-column d-flex align-items-center">
                           <div class="bg-danger square-55 rounded-circle shadow flex-center">
                            <i class="far fa-thumbs-down fs-22 text-white"></i>
                           </div>
                           <div class="d-none d-md-block">
                              <hr class="uk-divider-vertical border border-secondary h-lg-50 m-0 p-0">
                           </div>
                        </div>
                        <div class="ml-3 mt-lg-2-5">
                           <h1 class="display-7 weight-600 text-dark">
                              Não poderá cursar Faculdade
                           </h1>
                        </div>
                     </div>
                     <div class="d-flex pr-lg-5 mb-4 mb-lg-0">
                        <div class="flex-column d-flex align-items-center">
                           <div class="bg-danger square-55 rounded-circle shadow flex-center">
                            <i class="far fa-thumbs-down fs-22 text-white"></i>
                           </div>
                           <div class="d-none d-md-block">
                              <hr class="uk-divider-vertical border border-secondary h-lg-50 m-0 p-0">
                           </div>
                        </div>
                        <div class="ml-3 mt-lg-2-5">
                           <h1 class="display-7 weight-600 text-dark">
                              Não poderá fazer Cursos Técnicos
                           </h1>
                        </div>
                     </div>
                     <div class="d-flex pr-lg-5 mb-4 mb-lg-0">
                        <div class="flex-column d-flex align-items-center">
                           <div class="bg-danger square-55 rounded-circle shadow flex-center">
                            <i class="far fa-thumbs-down fs-22 text-white"></i>
                           </div>
                           <div class="d-none d-md-block">
                              <hr class="uk-divider-vertical border border-secondary h-lg-50 m-0 p-0">
                           </div>
                        </div>
                        <div class="ml-3 mt-1">
                           <h1 class="display-7 weight-600 text-dark">
                              Não poderá participar de Concursos Públicos
                           </h1>
                        </div>
                     </div>
                     <div class="d-flex pr-lg-5 mb-4 mb-lg-0">
                        <div class="flex-column d-flex align-items-center">
                           <div class="bg-danger square-55 rounded-circle shadow flex-center">
                            <i class="far fa-thumbs-down fs-22 text-white"></i>
                           </div>
                           <div class="d-none d-md-block">
                              <hr class="uk-divider-vertical border border-secondary h-lg-50 m-0 p-0">
                           </div>
                        </div>
                        <div class="ml-3 mt-lg-2-5">
                           <h1 class="display-7 weight-600 text-dark">
                              Não pode realizar o ENEM
                           </h1>
                        </div>
                     </div>
                     <div class="d-flex pr-lg-5 mb-4 mb-lg-0">
                        <div class="flex-column d-flex align-items-center">
                           <div class="bg-danger square-55 rounded-circle shadow flex-center">
                            <i class="far fa-thumbs-down fs-22 text-white"></i>
                           </div>
                           <div class="d-none d-md-block">
                              <hr class="uk-divider-vertical border border-secondary h-lg-50 m-0 p-0">
                           </div>
                        </div>
                        <div class="ml-3 mt-1">
                           <h1 class="display-7 weight-600 text-dark">
                              Dificuldade no crescimento Pessoal e Profissional
                           </h1>
                        </div>
                     </div>
                     <div class="d-flex pr-lg-5 mb-4 mb-lg-0">
                        <div class="flex-column d-flex align-items-center">
                           <div class="bg-danger square-55 rounded-circle shadow flex-center">
                            <i class="far fa-thumbs-down fs-22 text-white"></i>
                           </div>
                           <div class="d-none d-md-block">
                              <hr class="uk-divider-vertical border border-secondary h-lg-50 m-0 p-0">
                           </div>
                        </div>
                        <div class="ml-3 mt-1">
                           <h1 class="display-7 weight-600 text-dark">
                              Perde a chance de crescer dentro de empresas
                           </h1>
                        </div>
                     </div>
                     <div class="d-flex pr-lg-5 mb-4 mb-lg-0">
                        <div class="flex-column d-flex align-items-center">
                           <div class="bg-danger square-55 rounded-circle shadow flex-center">
                            <i class="far fa-thumbs-down fs-22 text-white"></i>
                           </div>
                           <div class="d-none d-md-block">
                              <hr class="uk-divider-vertical border border-secondary h-lg-50 m-0 p-0">
                           </div>
                        </div>
                        <div class="ml-3 mt-lg-2-5">
                           <h1 class="display-7 weight-600 text-dark">
                              Não obtém conhecimento
                           </h1>
                        </div>
                     </div>
                     <div class="d-flex pr-lg-5 mb-4 mb-lg-0">
                        <div class="flex-column d-flex align-items-center">
                           <div class="bg-danger square-55 rounded-circle shadow flex-center">
                            <i class="far fa-thumbs-down fs-22 text-white"></i>
                           </div>
                        </div>
                        <div class="ml-3 mt-lg-2-5">
                           <h1 class="display-7 weight-600 text-dark">
                              Sem os estudos, não conseguirá avançar
                           </h1>
                        </div>
                     </div>

                  </div>

               </div>

            </div>

         </div>
      </section>

      <section id="faq" class="py-5 bg-f7">
         <div class="container-fluid px-xl-7 pb-3">

            <h1 class="fs-40 text-center font-raleway text-dark ls-1 weight-800">DÚVIDAS FREQUENTES</h1>
            <div class="flex-center mt-4 mb-4"><div class="w-80px h-2px bg-base rounded"></div></div>

            <div class="row pt-4-5">

              <div class="col-lg-4 mb-lg-4">
                <h5 class="font-opensans weight-600 fs-18">Quando poderei utilizar a Área do Aluno?</h5>
                <p class="mb-0 font-opensans">
                  Após a confirmação de pagamento, o seu acesso já estará totalmente liberado.
                </p>
                <hr class="d-lg-none" />
              </div>

              <div class="col-lg-4 mb-lg-4">
                <h5 class="font-opensans weight-600 fs-18">Qual a idade mínima para fazer o supletivo?</h5>
                <p class="mb-0 font-opensans">
                  A idade mínima para se fazer o supletivo é de 18 anos e 6 meses.
                </p>
                <hr class="d-lg-none" />
              </div>

              <div class="col-lg-4 mb-lg-4">
                <h5 class="font-opensans weight-600 fs-18">O curso é totalmente à distância?</h5>
                <p class="mb-0 font-opensans">
                  Sim, é possível fazer o curso de qualquer lugar do Brasil.
                </p>
                <hr class="d-lg-none" />
              </div>

              <div class="col-lg-4 mb-lg-4">
                <h5 class="font-opensans weight-600 fs-18">O curso é legalizado pelo Centro de Aplicações e Avaliações?</h5>
                <p class="mb-0 font-opensans">
                  Sim, nossos parceiros certificadores estão devidamente legalizados na aba supletivo, você irá ter acesso a todos os atos de legalidade dos cursos.
                </p>
                <hr class="d-lg-none" />
              </div>

              <div class="col-lg-4 mb-lg-4">
                <h5 class="font-opensans weight-600 fs-18">E se eu acabar reprovando em alguma matéria?</h5>
                <p class="mb-0 font-opensans">
                  Neste caso, após a correção da prova você receberá outra para poder fazer novamente, sem nenhum custo adicional.
                </p>
                <hr class="d-lg-none" />
              </div>

              <div class="col-lg-4 mb-lg-4">
                <h5 class="font-opensans weight-600 fs-18">Qual é o valor do curso?</h5>
                <p class="mb-0 font-opensans">
                  Neste caso, você deverá entrar em contato com nossa equipe atráves de um de nossos canais de comunicação (Telefone/E-mail/Contato Via Site) e assim um de nossos consultores entrará em contato com você.
                </p>
                <hr class="d-lg-none" />
              </div>

              <div class="col-lg-4 mb-lg-4">
                <h5 class="font-opensans weight-600 fs-18">Já conclui o 1º ou 2º ano, como faço para continuar?</h5>
                <p class="mb-0 font-opensans">
                  Neste caso, os dois anos que você já concluiu serão aceitos e as matérias eliminadas, porém o valor do curso não sofrerá alteração.
                </p>
                <hr class="d-lg-none" />
              </div>

              <div class="col-lg-4 mb-lg-4">
                <h5 class="font-opensans weight-600 fs-18">Qual o prazo para recebimento do certificado e demais documentos?</h5>
                <p class="mb-0 font-opensans">
                  Após a conclusão das provas e a quitação do curso, em até 120 dias uteis letivos você receberá o certificado, e sera publicado no diário oficial dos estudantes.
                </p>
                <hr class="d-lg-none" />
              </div>

              <div class="col-lg-4 mb-lg-4">
                <h5 class="font-opensans weight-600 fs-18">Qual a duração do curso?</h5>
                <p class="mb-0 font-opensans">
                  O tempo de conclusão depende do próprio aluno, você irá receber o material de apoio e as provas, quanto mais rápido responder e nos devolver, mais rápido receberá seu diploma.
                </p>
                <hr class="d-lg-none" />
              </div>

              <div class="col-lg-4 mb-lg-4">
                <h5 class="font-opensans weight-600 fs-18">Existe a possibilidade de eliminar matérias?</h5>
                <p class="mb-0 font-opensans">
                  Sim, porém não afetará o valor final do curso.
                </p>
                <hr class="d-lg-none" />
              </div>

              <div class="col-lg-4 mb-lg-4">
                <h5 class="font-opensans weight-600 fs-18">O certificado é válido em meu estado?</h5>
                <p class="mb-0 font-opensans">
                  Todo curso autorizado pelo Conselho Estadual de Educação é válido em todo o território nacional.
                </p>
                <hr class="d-lg-none" />
              </div>

              <div class="col-lg-4 mb-lg-4">
                <h5 class="font-opensans weight-600 fs-18">O certificado é válido em meu estado?</h5>
                <p class="mb-0 font-opensans">
                  Todo curso autorizado pelo Conselho Estadual de Educação é válido em todo o território nacional.
                </p>
                <hr class="d-lg-none" />
              </div>

            </div>

         </div>
      </section>

      <section class="py-5 bg-termine">
         <div class="container">
            <div class="row align-items-center">
            
               <div class="col-lg-8 mb-3 mb-lg-0">
                  <div>
                     <p class="mb-0 fs-md-55 fs-xs-35 weight-900 font-raleway text-shadow-1 text-light">
                       Termine seus Estudos
                     </p>
                     <p class="mb-0 weight-600 fs-22 text-light text-shadow-1 mr-lg-5">
                        Aproveite as vantagens que o EAD proporciona em sua vida e rotina. Matrícula 100% Online.
                     </p>
                  </div>
               </div>
               <div class="col-lg-4">
                  <div uk-scrollspy="cls: uk-animation-slide-top; target: #btnMatricula; delay: 300;">
                     <a href="https://canaleducacional.com.br/f/formulario-ifope/8/t/22" target="_blank" class="myButton w-100 shadow rounded hover-d-none bg-base bg-base-hover text-center py-4" id="btnMatricula">
                        QUERO FAZER MINHA MATRÍCULA!
                     </a>
                  </div>
               </div>

            </div>
         </div>
      </section>

      <section id="quem-somos" class="py-5 bg-quem-somos">
         <div class="container-fluid px-md-5 pb-4-5 pt-2">

            <h1 class="fs-40 text-center font-raleway text-light ls-2 weight-800">QUEM SOMOS</h1>

            <div class="flex-center mt-4 mb-4"><div class="w-80px h-2px bg-base rounded"></div></div>

            <p class="text-center text-shadow-1 text-light font-opensans fs-20 mt-4 mb-5 px-xl-7">
              Conheça mais um pouco sobre a história do Colégio Axioma
            </p>
            
            <div class="row align-items-center">
              
              <div class="col-lg-6">
                <img src="https://funespead.com.br/images/plataforma-elearning.png" class="img-fluid">
              </div>
              <div class="col-lg-5 ml-lg-5 mt-4 mt-lg-0">
                <p class="font-opensans fs-17 text-light">
                  O Colégio Axioma está no mercado de ensino a distância e já preparou centenas de alunos, prestando-lhes um atendimento personalizado, visando sempre a conclusão dos seus objetivos, de acordo com a comodidade e a disponibilidade de cada um. Temos o compromisso de atender o objetivo que o aluno pretende alcançar, com responsabilidade e segurança.
                </p>
                <p class="font-opensans fs-17 text-light">
                  Nossa instituição atua em todo o território nacional, tornando possível o término dos estudos em mais de 1612 cidades, nos 26 Estados Brasileiros e no Distrito Federal. Atuamos de acordo com as principais políticas de certificação do Ensino Fundamental e Ensino Médio vigentes no Brasil para oferecer o que há de mais acessível para nossos alunos.
                </p>
              </div>

            </div>

         </div>
      </section>

      <section class="py-5 bg-contact" id="contato">
         <div class="container-fluid px-xl-7 pt-lg-5">

            <h1 class="fs-40 text-center font-raleway text-light weight-800">FALE CONOSCO</h1>
            <div class="flex-center mt-4-5 mb-4-5"><div class="w-80px h-2px bg-base rounded"></div></div>

            <div class="row py-lg-4 px-3 px-md-0">

               <div class="col-lg-6 mt-lg-1">

                  <div class="mb-4">
                    <p class="mb-0 font-opensans weight-600 text-white d-flex align-items-center">
                      <i class="far fa-clock fs-35 text-primary"></i>
                      <span class="ml-3">HORÁRIO DE ATENDIMENTO</span>
                    </p>
                    <p class="mt-1 mt-lg-0 mb-0 font-opensans weight-600 fs-15 color-ccc ml-lg-5">
                      &nbsp;{{ $plataforma->horario }}
                    </p>
                  </div>

                  <div class="mb-4">
                    <p class="mb-0 font-opensans weight-600 text-white d-flex align-items-center">
                      <i class="fa fa-envelope fs-35 text-primary"></i>
                      <span class="ml-3">EMAIL </span>
                    </p>
                    <p class="mt-1 mt-lg-0 mb-0 font-opensans weight-600 fs-15 color-ccc ml-lg-5">
                      &nbsp;{{ $plataforma->email }}
                    </p>
                  </div>

                  <div class="mb-4">
                    <p class="mb-0 font-opensans weight-600 text-white d-flex align-items-center">
                      <i class="fab fa-whatsapp fs-40 text-primary"></i>
                      <span class="ml-3">CONTATO</span>
                    </p>
                    <div class="mt-1 mt-lg-0 mb-0 font-opensans weight-600 fs-15 color-ccc ml-lg-5">
                      <p class="mb-0">
                        Matrícule-se:
                        <a href="https://api.whatsapp.com/send?phone=55{{preg_replace('/\D/', '', $plataforma->whatsapp)}}&text=Olá, vim através do Site {{ $plataforma->nome }}" target="_blank" class="text-d-none color-ccc hover-white">{{ $plataforma->whatsapp }}</a>
                      </p>
                    </div>
                  </div>

                  <div class="mb-4">
                    <p class="mb-0 font-opensans weight-600 text-white d-flex align-items-center">
                      <i class="fa fa-phone fs-40 text-primary"></i>
                      <span class="ml-3">TELEFONE</span>
                    </p>
                    <div class="mt-1 mt-lg-0 mb-0 font-opensans weight-600 fs-15 color-ccc ml-lg-5">
                      <p class="mb-0">
                         &nbsp;&nbsp;Administrativo:
                        <a href="tel:+55{{preg_replace('/\D/', '', $plataforma->telefone)}}" target="_blank" class="text-d-none color-ccc hover-white">{{ $plataforma->telefone }}</a>
                      </p>
                    </div>
                  </div>

                  <div class="mb-3">
                    <p class="mb-0 font-opensans weight-600 text-white d-flex align-items-center">
                      <i class="fa fa-map-marker-alt fs-40 text-primary"></i>
                      <span class="ml-3">ENDEREÇO</span>
                    </p>
                    <address class="mt-1 mt-lg-0 ml-lg-5 mt-0 font-opensans weight-600 fs-15 color-ccc">
                      {{ $plataforma->endereco }}
                    </address>
                  </div>

               </div>

               <div class="col-lg-6 mt-2">
                <form method="POST">
                @csrf
                  <input type="hidden" name="assunto" value="Fale Conosco">
                  <input type="text" name="nome" class="form-control py-2-5 outline-0 rounded-2" placeholder="Seu Nome" required>
                  <input type="tel" name="celular" class="my-2 telefone form-control py-2-5 outline-0 rounded-2" placeholder="Telefone" required>
                  <input type="email" name="email" class="rounded-left-0 form-control py-2-5 outline-0 rounded-2" placeholder="Email" required>
                  <textarea name="mensagem" placeholder="Mensagem" class="mt-2 form-control h-min-100 w-100 outline-0 rounded-2"></textarea>
                  <input type="text" name="check" class="invisible h-0px" placeholder="Não preencha este campo.">
                  <input type="submit" value="Enviar" name="sentContato" class="mt--15px w-100 border-0 rounded-3 bg-base bg-base-hover py-2 px-3 fs-18 font-raleway weight-600 text-white shadow">
                </form>
               </div>
               
            </div>
         
         </div>
      </section>

      <a href="https://api.whatsapp.com/send?phone=55{{preg_replace('/\D/', '', $plataforma->whatsapp)}}&text=Olá, vim através do Site {{ $plataforma->nome }}" target="_blank" class="text-white fs-30">
          <div class="p-fixed bottom-0 left-0 ml-3 mb-3 z-5">
              <div class="square-60 rounded-circle bg-success flex-center shadow">
                  <i class="fab fa-whatsapp"></i>
              </div>
          </div>
      </a>
   
   </main>

   <footer>
       <div class="bg-base py-1-5 text-center">
          <div class="container-fluid px-xl-7 d-lg-flex justify-content-between align-items-center">
            <p class="text-xs-center mb-1 mb-lg-0 p-0 text-light font-poppins fs-11 ls-05">
               Colégio Axioma © 2021 - Todos os direitos reservados.
            </p>
            <p class="text-xs-center m-0 p-0 text-light font-poppins fs-11 ls-05">
               Desenvolvido por <a href="https://wozcode.com" target="_blank" class="text-light text-d-none fs-11 weight-600 text-shadow-1">WOZCODE</a>
            </p>
          </div>
       </div>
   </footer>

   <script src="https://kit.fontawesome.com/2f91eaaef4.js" crossorigin="anonymous"></script> 
   <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>    
   <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
   <script src="https://cdn.jsdelivr.net/npm/uikit@3.5.5/dist/js/uikit.min.js"></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.js"></script>
   <script>

    $(document).on("focus", ".telefone", function() { 
      jQuery(this)
          .mask("(99) 9999-99999")
          .change(function (event) {  
              target = (event.currentTarget) ? event.currentTarget : event.srcElement;  
              phone = target.value.replace(/\D/g, '');
              element = $(target);  
              element.unmask();  
              if(phone.length > 10) {  
                  element.mask("(99) 99999-9999");  
              } else {  
                  element.mask("(99) 9999-9999?9");  
              }  
      });
    });

   </script>

</body>
</html>