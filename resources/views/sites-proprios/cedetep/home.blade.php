<!doctype html>
<html lang="pt-br">
<head>

  <!-- Google tag (gtag.js) -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=G-MREB450HCT"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'G-MREB450HCT');
  </script>
  
   <meta charset="utf-8">
   <meta name="viewport" content="width=device-width, initial-scale=1">
   
   <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/uikit@3.5.9/dist/css/uikit.min.css" />
   <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
   <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
   <link rel="stylesheet" href="{{ asset('css/util.css') }}">
   <link rel="stylesheet" href="{{ asset('sites-proprios/cedetep/css/home.css') }}?v=3">

   <title>{{ $seo->seo_title ?? $plataforma->nome }}</title>
   <meta name="robots" content="index, follow">
   <meta name="description" content="{{ $seo->seo_description ?? null }}"/>
   <meta name="keywords" content="{{ $seo->seo_keywords ?? null }}"/>
   <link rel="canonical" href="{{ Request::url() }}" />
   <link rel="shortcut icon" href="{{ $plataforma->favicon }}">
   <meta property="og:locale" content="pt_BR" />
   <meta property="og:type" content="website" />
   <meta property="og:title" content="{{ $seo->seo_title ?? $plataforma->nome }}" />
   <meta property="og:description" content="{{ $seo->seo_description ?? null }}" />
   <meta property="og:url" content="{{ Request::url() }}" />
   <meta property="og:site_name" content="{{ $plataforma->nome }}" />
   <meta property="og:image" content="{{ $plataforma->logotipo }}" />
   <meta name='author' content='WOZCODE | Pedro Henrique' />

</head>
<body class="bg-f4" id="inicio">

   <header>
      
      <div class="sub-header container-fluid px-xl-7 py-2-3">
         <div class="row align-items-center">
            <div class="col-6 col-lg-4">

               <a href="https://api.whatsapp.com/send?phone=55{{preg_replace('/\D/', '', $plataforma->whatsapp)}}&text=Olá, vim através do Site {{ $plataforma->nome }}." target="_blank" class="btn-submenu">
                  <i class="fab fa-whatsapp fs-17"></i>
                  <span class="d-xs-none"> 
                    Matricule-se pelo WhatsApp
                  </span>
                  <span class="d-lg-none ml-1">
                    {{ $plataforma->whatsapp }}
                  </span>
               </a>

               <a href="tel:+55{{preg_replace('/\D/', '', $plataforma->telefone)}}" class="d-xs-none btn-submenu">
                  <i class="fa fa-phone fs-17"></i>
                  {{ $plataforma->telefone }}
               </a>

            </div>

            <div class="col-6 col-lg-8" align="right">

               <a href="https://canaleducacionalonline.com.br/" target="_blank" class="btn-submenu d-xs-none">
                  Cursos Profissionalizantes
               </a>

               <a href="/sobre-nos" class="btn-submenu d-xs-none">
                  Institucional
               </a>

               <a href="https://cedetep.com/f/trabalhe-conosco/36/t/597" target="_blank" class="btn-submenu d-xs-none">
                  Trabalhe Conosco
               </a>

               <a href="/login" class="btn-submenu btnPortalAluno">
                  Portal do Aluno
               </a>

            </div>

         </div>
      </div>

      <nav class="navbar navbar-expand-lg bg-white box-shadow" id="menu">
         <div class="container-fluid px-xl-7">
             <a class="navbar-brand" href="#inicio">
               <img src="{{ $plataforma->logotipo }}" width="75" alt="Logotipo {{ $plataforma->nome }}">
             </a>
             <button class="navbar-toggler outline-0 border-0 text-dark" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
               <span class="fa fa-bars"></span>
             </button>
             <div class="collapse navbar-collapse" id="navbarSupportedContent">
                  <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
                     <li class="nav-item">
                        <a class="nav-link" href="#inicio">HOME</a>
                     </li>
                     <li class="nav-item">
                        <a class="nav-link" href="#cursos">CURSOS</a>
                     </li>
                     <li class="nav-item">
                        <a class="nav-link" href="https://aeducacao.com.br/saladeaula/" target="_blank">PÓS</a>
                      </li>
                     <li class="nav-item">
                        <a class="nav-link" href="/graduacao">GRADUAÇÃO</a>
                     </li>
                     <li class="nav-item">
                        <a class="nav-link" href="#contato">
                           CONTATO
                        </a>
                     </li>
                     <li class="nav-item ms-lg-4 mt-3 mt-lg-0">
                       <a class="nav-link btnAreaAluno animation-grow d-inline-block" href="https://cedetep.com/f/formulario-de-matricula/34/t/595" target="_blank">
                          <i class="fas fa-graduation-cap mr-1"></i>
                          MATRICULE-SE
                       </a>
                     </li>
                 </ul>
             </div>
         </div>
      </nav>

   </header>

   <main>

      <section id="banners">
        <div class="uk-position-relative uk-visible-toggle" tabindex="-1" uk-slideshow="autoplay: true; autoplay-interval: 5000; pause-on-hover: false; max-height: 400">

            <ul class="uk-slideshow-items">
              @foreach($banners as $banner)
                <li>
                  <div style="background-image: url('{{ $banner->link }}');" class="bg-img-banner">
                     @if($banner->overlay == 'S')
                        <div class="background-overlay" style="background: {{ $banner->overlay_color ?? 'unset' }};opacity: {{ $banner->overlay_opacity ?? '0' }};"></div>
                     @endif
                     <div uk-scrollspy="cls: uk-animation-fade; delay: 400;" class="container mt--10px uk-position-center-left uk-position-small uk-light">
                      
                        <h2 class="text-center uk-margin-remove font-montserrat weight-800 display-3 ls--03" uk-slideshow-parallax="x: 300,0,-100">
                           {!! $banner->titulo !!}
                        </h2>

                        @if($banner->subtitulo)
                           <p uk-slideshow-parallax="x: 300,0,-75" class="text-center uk-margin-remove text-light fs-20">
                              {!! $banner->subtitulo !!}
                           </p>
                        @endif

                        @if($banner->botao_texto)
                           <div class="mt-4-5 text-center">
                              <a href="{{ $banner->botao_link }}" class="btnInscricao text-shadow-1 d-xs-block text-center text-shadow-1" uk-slideshow-parallax="x: 300,0,-50">
                                 {!! $banner->botao_texto !!}
                              </a>
                           </div>
                        @endif

                     </div>
                  </div>
                </li>
              @endforeach
            </ul>

            <div class="uk-light">
                <a class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous uk-slideshow-item="previous"></a>
                <a class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-next uk-slideshow-item="next"></a>
            </div>

        </div>
      </section>

      <section id="cursos">
         <div class="container py-5" id="boxContainerCursos">

            <div class="bg-verde-gradient p-4">
               <h1 class="text-light font-roboto-condensed weight-600 fs-21 p-0 m-0 text-center align-items-center">
                  CONHEÇA NOSSOS CURSOS
               </h1>
            </div>
            <div class="bg-white box-shadow w-100 h-150 d-flex align-items-center">

                  <a id="btnCursoSaude" data-area="curso-area-saude" class="btn-area hover-d-none color-verde d-block h-100 w-100 btnCategoria">
                     <div class="flex-center h-100 text-center">
                        <div>
                           <i class="fas fa-user-md fs-md-75 fs-xs-35 d-block"></i>
                           <p class="m-0 p-0 font-roboto-condensed fs-20 mt-2">
                              Saúde
                           </p>
                        </div>
                     </div>
                  </a>

                  <div class="line-v"></div>

                  <a id="btnCursoNegocios" data-area="curso-area-negocios" class="btn-area hover-d-none color-verde d-block h-100 w-100 btnCategoria">
                     <div class="flex-center h-100 text-center">
                        <div>
                           <i class="fas fa-user-tie fs-md-75 fs-xs-35 d-block"></i>
                           <p class="m-0 p-0 font-roboto-condensed fs-20 mt-2">
                              Negócios
                           </p>
                        </div>
                     </div>
                  </a>

            </div>

            <div id="boxAllCursos" class="boxAreas bg-base-soft p-4-5" style="display: none;">

               @foreach($allCursos as $curso)
                  <a href="/curso/{{ $curso->slug }}" class="btnAreasCursos curso-area-{{\Str::slug($curso->area_negocio)}} shadow mb-3 mb-lg-0">
                     {{ $curso->nome }}
                  </a>
               @endforeach

            </div>

         </div>
      </section>

      <section id="porque-nos-escolher">
         <div class="container">

            <div class="border-top-gradient p-1"></div>
            <div class="bg-white p-4 box-shadow">
               <h1 class="font-roboto-condensed fs-22 color-verde">
                  POR QUE ESCOLHER A <span class="weight-600">{{ $plataforma->nome }}</span>?
               </h1>
               <p class="mb-3 p-0 color-verde font-roboto fs-18 weight-400">
                  Presente em todas as regiões do Brasil, a {{ $plataforma->nome }} é a maior rede privada de Ensino Técnico no Brasil.
                  Oferecemos mais de 30 cursos e já são mais de 25 mil alunos matriculados.
               </p>
               <a class="nav-link btnAreaAluno d-inline-block rounded-3 box-shadow" href="https://itspensino.com.br/f/formulario-de-matricula/47/t/650" target="_blank">
                  EU QUERO SE TORNAR ALUNO
               </a>
            </div>

         </div>
      </section>

      <section id="nossos-cursos" class="pt-5">
         <div class="container">

            <div class="border-top-gradient p-1"></div>
            <div class="bg-white p-4 box-shadow">

               <h1 class="font-roboto-condensed fs-22 color-verde">
                  NOSSOS <span class="weight-600">CURSOS</span>
               </h1>

               <div class="my-4 w-80px h-2px bg-line rounded"></div>
               
               <div class="uk-slider-container-offset" uk-slider="autoplay: true; autoplay-interval: 1500; pause-on-hover: true">

                   <div class="uk-position-relative uk-visible-toggle uk-light" tabindex="-1">

                       <ul class="uk-slider-items uk-child-width-1-5@s">
                           @foreach($allCursos as $curso)
                           <li class="px-2">
                              <a href="/curso/{{$curso->slug}}" class="w-100">
                                 <div class="w-100 uk-inline-clip uk-transition-toggle rounded box-shadow mb-4 mb-lg-0" tabindex="0">
                                    <img src="{{ $curso->foto_miniatura ?? asset('images/no-image.jpeg') }}" class="uk-transition-scale-up uk-transition-opaque object-fit-cover w-100 h-lg-350 h-xs-350">
                                    <div class="uk-position-bottom fig-caption flex-center px-3">
                                       <h5 class="text-light fs-17 font-montserrat weight-700 ls-05 text-uppercase text-center">
                                          {{ $curso->nome }}
                                       </h5>
                                    </div>
                                 </div>
                              </a>
                           </li>
                           @endforeach
                       </ul>

                       <a class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous uk-slider-item="previous"></a>
                       <a class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-next uk-slider-item="next"></a>

                   </div>

                   <ul class="uk-slider-nav uk-dotnav uk-flex-center uk-margin"></ul>

               </div>

            </div>

         </div>
      </section>

      <section class="py-5 mb-3" id="pre-matricula">
         <div class="container">
            <div class="row">

               <div class="col-lg-6">

                     <img src="{{ asset('sites-proprios/cedetep/images/foto-matricula.jpeg') }}" class="img-fluid h-lg-500 box-shadow">

                     <div class="ms-4 p-absolute mt--80 d-xs-none">
                        <a href="https://api.whatsapp.com/send?phone=55{{preg_replace('/\D/', '', $plataforma->whatsapp)}}&text=Olá, vim através do Site da {{ $plataforma->nome }} e #queromeudesconto" target="_blank" class="btn-sell-matriculas-desconto">
                           QUERO MEU DESCONTO!
                        </a>
                     </div>

               </div>

               <div class="col-lg-6">

                  <div class="border-top-gradient p-1"></div>
                  <div class="bg-white box-shadow rounded-2 px-4 py-4 h-lg-540">

                     <h1 class="font-roboto-condensed color-verde fs-30">
                        CHEGOU A HORA DE <strong>MUDAR O SEU FUTURO</strong>
                     </h1>

                     <p class="mb-0 pb-0 color-suave font-poppins weight-600 fs-15">
                        Faça agora o seu cadastro e garanta uma oferta super especial para se matricular em qualquer uma de nossas unidades espalhadas pelo Brasil.
                     </p>

                     <form method="POST" id="formSolicitarContato" class="pt-4-5">
                     @csrf

                        @if(session('success'))
                           <div class='alert alert-success'><b>Sucesso!</b> {{ session('success') }}</div>
                        @endif

                        <div class="d-lg-flex mb-3">
                           <input type="text" name="nome" class="form-control lead-form me-lg-3 mb-3 mb-lg-0" placeholder="NOME" required>
                           <input type="email" name="email" class="form-control lead-form" placeholder="EMAIL" required>
                        </div>
                        <div class="d-lg-flex mb-3">
                           <select name="curso_id" class="form-control lead-form sec-lead-form me-lg-3 mb-3 mb-lg-0" required>
                              <option value="">CURSO</option>
                              @foreach($allCursos as $curso)
                                 <option value="{{ $curso->id }}">{{ $curso->nome }}</option>
                              @endforeach
                           </select>
                           <input type="phone" name="celular" class="form-control lead-form" placeholder="(DDD) CELULAR" required>
                        </div>

                        <div class="mt-4">
                           <label class="cursor-pointer font-poppins fs-14">
                              <input type="checkbox" class="" name="consentimento" value="1" required> Concordo que os dados pessoais fornecidos acima serão utilizados para envio de conteúdo informativo, analítico e publicitário sobre produtos, serviços e assuntos gerais, nos termos da Lei Geral de Proteção de Dados.
                           </label>
                        </div>

                        <input type="text" name="check" class="invisible h-0px" placeholder="Não preencha este campo.">

                        <input type="submit" name="sentContato" value="Solicitar Contato!" class="btnEntrarEmContato box-shadow">

                     </form>

                  </div>

               </div>

            </div>
         </div>
      </section>

      <section id="quem-somos" class="py-5 bg-base-soft">
         <div class="bg-faixa p-lg-5 p-4 h-lg-325">
            <div class="container-fluid px-xl-8">
               <div class="row">

                  <div class="col-xl-6 mb-4 mb-lg-0">

                     <div class="pe-lg-5">

                        <h1 class="fs-35 font-montserrat text-light weight-800 text-uppercase">O que falam de nós!</h1>
                        <div class="my-4 w-80px h-2px bg-line rounded"></div>

                        <p class="font-poppins text-light fs-16">
                           Ao longo dos anos, com base do que é visivel sobre nossos ensinos, envaidece.nos deixando assim a nosso  cargo a obrigação de proporcionar ao alunado  ensinamentos com tecnologias avançadas, pautadas nos melhores referênciais pedagógicas.
                        </p>

                     </div>

                  </div>

                  <div class="col-xl-6">

                     <div class="uk-slider-container-offset" uk-slider>

                         <div class="uk-position-relative uk-visible-toggle uk-light" tabindex="-1">

                              <ul class="uk-slider-items uk-child-width-1-1@s uk-grid">
                                 @foreach($depoimentos as $depoimento)
                                 <li>
                                     <div class="uk-card box-shadow bg-white rounded-10 h-lg-325 cursor-drag">
                                         <div class="uk-card-body text-dark">
                                             <i class="fas fa-quote-left fs-50 color-verde opacity-04"></i>
                                             <div class="mt-4 d-lg-flex align-items-center">
                                                <div style="width: 100px;">
                                                   <img src="{{ $depoimento->foto }}" alt="Depoimento de Aluno Formado - {{ $plataforma->nome }}" class="object-fit-cover rounded-circle w-100">
                                                   <p class="mb-0 mt-3 color-suave-fort text-lg-center ms--lg-5 weight-600 fs-17">
                                                      {{ $depoimento->nome }}
                                                   </p>
                                                </div>
                                                <p class="mt-lg-3 mt-0 ms-lg-4 font-poppins fs-16 color-suave-fort">
                                                   {{ $depoimento->descricao }}
                                                </p>
                                             </div>
                                         </div>
                                     </div>
                                 </li>
                                 @endforeach
                              </ul>

                             <a class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous uk-slider-item="previous"></a>
                             <a class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-next uk-slider-item="next"></a>

                         </div>

                         <ul class="uk-slider-nav uk-dotnav uk-flex-center uk-margin"></ul>

                     </div>

                  </div>

               </div>
            </div>
         </div>
      </section>

   </main>

   <footer id="contato" style="background-image: url('{{ asset('sites-proprios/cedetep/images/footer.jpg') }}'); background-repeat: no-repeat; background-size: cover; background-position: center;">
      <div class="container-fluid px-xl-8 pt-5 ps-lg-5" style="margin-top: -50px;">

         <div class="row align-items-end py-lg-4 px-3 px-md-0">

            <div class="col-lg-9">

               <h1 class="fs-35 font-montserrat text-light weight-800 text-uppercase">FALE CONOSCO</h1>
               <div class="my-4 w-80px h-2px bg-line rounded"></div>

               <img src="{{ $plataforma->logotipo }}" class="img-fluid" width="100">

               <div class="mt-2">                 

                  <div class="mb-3">
                     <span class="text-light font-poppins fs-14">
                        <i class="color-d4 fa fa-envelope"></i> 
                        {{ $plataforma->email ?? 'meuemail@exemlo.com' }}
                     </span>
                  </div>
                  <div class="mb-3">
                     <a href="https://api.whatsapp.com/send?phone=55{{preg_replace('/\D/', '', $plataforma->whatsapp)}}&text=Olá, vim através do Site {{ $plataforma->nome }}" class="text-light font-poppins fs-14">
                        <i class="color-d4 fab fa-whatsapp"></i> 
                        {{ $plataforma->whatsapp ?? '(00) 00000-0000' }}
                     </a>
                  </div>
                  <div class="mb-3">
                     <span class="text-light font-poppins fs-14">
                        <i class="color-d4 fa fa-clock"></i> 
                        {{ $plataforma->horario ?? 'Horário de Funcionamento: Segunda à Sexta, 07:00h às 22:40h' }}
                     </span>
                  </div> 
                  <div class="mb-3">
                    <address>
                      <a href="https://www.google.com/maps/place/CEDETEP+Escola+T%C3%A9cnica+-+Pindamonhangaba/@-22.924224,-45.468257,14z/data=!4m5!3m4!1s0x0:0x70581a8d7cfa2dad!8m2!3d-22.9242244!4d-45.4682567?hl=pt-BR" target="_blank" class="btnEnderecoSiteCedetep text-light font-poppins fs-14">
                           <i class="color-d4 fas fa-map-marker-alt"></i>
                           {{ $plataforma->endereco ?? 'Rua Bicudo Leme, 611 Jardim irmãos Braga - Pindamonhangaba' }}
                      </a>
                     </address>
                  </div>

                  <div class="mt-4 d-flex">

                     <a href="{{ $plataforma->facebook }}" target="_blank" class="me-2 hover-d-none">
                        <div class="square-40 rounded-circle social-icon social-icon-facebook flex-center">
                           <i class="text-white fab fa-facebook fs-20"></i>
                        </div>
                     </a>
                     <a href="{{ $plataforma->instagram }}" target="_blank" class="hover-d-none">
                        <div class="square-40 rounded-circle social-icon social-icon-instagram flex-center">
                           <i class="text-white fab fa-instagram fs-20"></i>
                        </div>
                     </a>

                  </div>

               </div>

            </div>

            <div class="col-lg-3 mt-5 mt-lg-0">

               <div class="mb-3">
                  <a href="https://cedetep.com/f/formulario-de-matricula/34/t/595" target="_blank" class="btn btn-primary weight-600 w-100 py-2-5 font-poppins hover-d-none">
                     MATRICULE-SE JÁ!
                  </a>
               </div>
               <div class="mb-3">
                  <a href="/login" class="btn btn-warning weight-600 w-100 py-2-5 font-poppins hover-d-none">
                     ÁREA DO ALUNO
                  </a>
               </div>
               <div class="mb-3">
                  <a href="https://cedetep.com/f/torne-se-parceiro/35/t/596" target="_blank" class="btn btn-light weight-600 w-100 py-2-5 font-poppins hover-d-none">
                     TORNE-SE PARCEIRO
                  </a>
               </div>
               <div class="mb-3">
                  <a href="https://api.whatsapp.com/send?phone=55{{preg_replace('/\D/', '', $plataforma->whatsapp)}}&text=Olá, vim através do Site {{ $plataforma->nome }}" class="btn btn-success bg-wpp text-shadow-1 weight-600 w-100 py-2-5 font-poppins hover-d-none">
                     ENTRAR EM CONTATO
                  </a>
               </div>

            </div>

         </div>

         <hr class="mt-4 border border-light" />

         <div class="pb-3 d-lg-flex justify-content-between align-items-center">

            <p class="text-xs-center mb-1 mb-lg-0 p-0 text-light font-poppins fs-11 ls-05">
               {{ $plataforma->nome }} © 2022 • Todos os direitos reservados • CNPJ: {{ $plataforma->cnpj }}
            </p>

            <p class="text-xs-center m-0 p-0 text-light font-poppins fs-11 ls-05">
               Desenvolvido por <a href="https://wozcode.com" target="_blank" class="text-light text-d-none fs-11 weight-600 text-shadow-1">WOZCODE</a>
            </p>

         </div>

      </div>
   </footer>

   <a href="https://api.whatsapp.com/send?phone=55{{preg_replace('/\D/', '', $plataforma->whatsapp)}}&text=Olá, vim através do Site {{ $plataforma->nome }}" target="_blank" class="text-white fs-30">
       <div class="fixed-bottom right-0 me-3 mb-3 z-4">
           <div class="square-60 rounded-circle bg-wpp flex-center box-shadow-wpp">
               <i class="fab fa-whatsapp"></i>
           </div>
       </div>
   </a>


   <script src="https://cdn.jsdelivr.net/npm/uikit@3.5.9/dist/js/uikit.min.js"></script>
   <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa" crossorigin="anonymous"></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.1/jquery.min.js" integrity="sha512-aVKKRRi/Q/YV+4mjoKBsE4x3H+BkegoM/em46NNlCqNTmUYADjBbeNefNxYV7giUp0VxICtqdrbqU7iVaeZNXA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
   <script>

      window.onscroll = function() {myFunction()};

      var header = document.getElementById("menu");
      var sticky = header.offsetTop;

      function myFunction()
      {
         if (window.pageYOffset > sticky) {
            header.classList.add("fixed-top");
         } else {
            header.classList.remove("fixed-top");
         }
      }

      let lastActive = null;

      $(".btn-area").hover(
         function()
         {
            $('.btnCategoria').removeClass('btnHovered');

            $(this).addClass('btnHovered');
            
            $('.boxAreas').show();
            $('.btnAreasCursos').hide();

            let area = $(this).attr('data-area');
            $('.'+area).show();

            lastActive = area;
            console.log(lastActive);

         }, function() {

            // $(".boxAreas").hide();
        }
      );

      $("#boxContainerCursos").hover(function() {

          $('.btnAreasCursos').hide();
          $('.btnCategoria').removeClass('btnHovered');
          $('.boxAreas').hide();

      });

    </script>

</body>
</html>