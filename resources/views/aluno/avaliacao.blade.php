@extends('layouts.aluno')
@section('seo_title', 'Avaliação ' . $avaliacao->referencia)

@section('css')
<style>
	.bg-alternativa { background: #4d5256; }
	.bg-selected { background: #16181b; }
</style>
@endsection

@section('content')
<form action="{{ route('aluno.finalizar', ['id' => $avaliacao->id]) }}" method="POST" class="container-fluid px-xl-5 z-3 mt-5 position-relative">
@csrf

    @foreach($questoes as $questao)

    	<div class="mb-4 px-4 py-4 rounded-10 box-shadow bg-14">

    		<div class="d-flex align-items-center">

    		    <div>
    		        <div class="bg-db mr-4 square-40 rounded-circle flex-center text-dark">
    		            <span class="fs-18 weight-600">{{ $loop->iteration }}</span>
    		        </div>
    		    </div>

                <div class="ps-4">
                    <p class="color-enunciado mb-0 fs-19">
                    	{!! nl2br($questao->enunciado) !!}
                    </p>

                    @if($questao->arquivo)
                        @if($questao->isImage($questao->arquivo))
                            <img src="{{ $questao->arquivo }}" class="img-fluid my-3" width="200">
                            <div></div>
                            <a href="{{ $questao->arquivo }}" target="_blank" class="btn btn-primary">
                                Ampliar imagem
                                &nbsp;<i class="fa fa-search-plus"></i>
                            </a>
                        @else
                            <div class="mt-3 mb-1">
                                <a href="{{ $questao->arquivo }}" target="_blank" class="btn-cadastrar">
                                    <i class="fas fa-external-link-alt"></i>
                                    Abrir Arquivo em Anexo
                                </a>
                            </div>
                        @endif
                    @endif
                </div>

    		</div>

            <hr/>

            @if($questao->modelo == 'D')
                <textarea name="respostasDiscursivas[{{$questao->id}}]" class="w-100 rounded p-form bg-alternativa border-0 text-white py-3 color-placeholder-light-light h-min-100" placeholder="Sua resposta" required></textarea>
            @endif
            
            @if($questao->modelo == 'O')

                @php
                    $alternativas = \App\Models\QuestaoAlternativa::plataforma()->where('questao_id', $questao->id)->inRandomOrder()->get();
                @endphp

                @foreach($alternativas as $alternativa)
                
                    <label for="opt{{$loop->index}}p{{$alternativa->id}}" class="w-100 cursor-pointer">

                        <input type="radio" name="respostasObjetivas[{{$questao->id}}]" id="opt{{$loop->index}}p{{$alternativa->id}}" class="optAlternativa questao{{$questao->id}} d-none" data-questao="{{ $questao->id }}" value="{{ $alternativa->id }}" required> 
                        
                        <div class="d-flex">
                            <span class="mb-1 border rounded-start bg-db rounded-right-0 w-40px flex-center weight-600">
                                {{ $alternativa->alphabetic($loop->index) }}
                            </span>
                            <span class="bg-alternativa w-100 weight-500 color-light p-3 mb-1 rounded-left-0 rounded-end fs-17">{!! $alternativa->alternativa !!}</span>
                        </div>

                    </label>
            
                @endforeach

            @endif

    	</div>

    @endforeach

    <button type="submit" class="btn-suporte-enviar px-5">
        Concluir
    </button>

</form>

@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>
<script>

    $(".optAlternativa").on('change', function() {

        let questao_id = $(this).attr('data-questao');

        $('.questao'+questao_id).next().find('.bg-alternativa').removeClass('bg-dark');

        $(this).next().find('.bg-alternativa').addClass('bg-dark');

    });

</script>
@endpush

@endsection