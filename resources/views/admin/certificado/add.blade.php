@extends('layouts.admin.master')

@section('css')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.14.0-beta2/dist/css/bootstrap-select.min.css">
@endsection

@section('content')
    
<div>

    <form method="POST" enctype="multipart/form-data" class="mb-4">
    @csrf

		<section class="mb-4-5">
			
			<a href="#boxInfoPrincipal" data-bs-toggle="collapse" aria-expanded="false" class="hover-d-none">
				<div class="section-title">
					Informações Principais
				</div>
			</a>
			<hr/>
			
			<div class="collapse show" id="boxInfoPrincipal">

		    	<div class="d-lg-flex mb-3">

			    	<div class="w-100 me-lg-3 mb-3 mb-lg-0">
			    		<label>Referência</label>
			    		<input type="text" name="referencia" class="form-control p-form" placeholder="Nome" required>
			    	</div>
					<div class="w-100 me-lg-3 mb-3 mb-lg-0">
						<label>Modelo em Imagem</label>
						<div class="d-lg-flex">
							<input type="file" name="img" class="file-form rounded-right-0">
							<input type="text" name="posicao_imagem" class="form-control p-form rounded-left-0" placeholder="N° de Posição da Imagem">
						</div>
					</div>
			    	<div class="w-lg-35">
			    		<label>Status</label>
			    		<select name="status" class="select-form">
			    			<option value="0">Ativo</option>
			    			<option value="1">Desativado</option>
			    		</select>
			    	</div>

			    </div>

			    <div class="d-lg-flex mb-3">
			    	<div class="w-100 me-lg-3 mb-3 mb-lg-0">
			    		<label>Posição do Nome do Responsável na Horizontal (X)</label>
			    		<input type="number" name="nome_posicao_x" class="form-control p-form" placeholder="N°">
			    	</div>
			    	<div class="w-100 me-lg-3 mb-3 mb-lg-0">
			    		<label>Posição do Nome do Responsável na Vertical (Y)</label>
			    		<input type="number" name="nome_posicao_y" class="form-control p-form" placeholder="N°">
			    	</div>
			    	<div class="w-lg-75 mb-3 mb-lg-0">
			    		<label>Tamanho da Fonte</label>
			    		<input type="number" name="nome_fontSize" class="form-control p-form" placeholder="N° do Tamanho da Fonte de Letras">
			    	</div>
			    </div>

			    <div class="d-lg-flex mb-3">
				    <div class="w-100 mb-3 mb-lg-0 me-lg-3">
				    	<label>Texto 1</label>
				    	<textarea name="texto1" class="textarea-form" placeholder="Texto"></textarea>
				    </div>
				    <div class="w-lg-75">
				    	<div class="d-lg-flex">
					    	<div class="mb-3 me-lg-3">
					    		<label>Posição na Horizontal (X)</label>
					    		<input type="number" name="texto1_posicao_x" class="form-control p-form" placeholder="N°">
					    	</div>
					    	<div class="mb-3 mb-lg-0">
					    		<label>Posição na Vertical (Y)</label>
					    		<input type="number" name="texto1_posicao_y" class="form-control p-form" placeholder="N°">
					    	</div>
					    </div>
				    	<div class="mb-3 mb-lg-0">
				    		<label>Tamanho da Fonte</label>
				    		<input type="number" name="texto1_fontSize" class="form-control p-form" placeholder="N° do Tamanho da Fonte de Letras">
				    	</div>
				    </div>
				</div>

			    <div class="d-lg-flex mb-3">
				    <div class="w-100 mb-3 mb-lg-0 me-lg-3">
				    	<label>Texto 2</label>
				    	<textarea name="texto2" class="textarea-form" placeholder="Texto"></textarea>
				    </div>
				    <div class="w-lg-75">
				    	<div class="d-lg-flex">
					    	<div class="mb-3 me-lg-3">
					    		<label>Posição na Horizontal (X)</label>
					    		<input type="number" name="texto2_posicao_x" class="form-control p-form" placeholder="N°">
					    	</div>
					    	<div class="mb-3 mb-lg-0">
					    		<label>Posição na Vertical (Y)</label>
					    		<input type="number" name="texto2_posicao_y" class="form-control p-form" placeholder="N°">
					    	</div>
					    </div>
				    	<div class="mb-3 mb-lg-0">
				    		<label>Tamanho da Fonte</label>
				    		<input type="number" name="texto2_fontSize" class="form-control p-form" placeholder="N° do Tamanho da Fonte de Letras">
				    	</div>
				    </div>
				</div>

			    <div class="d-lg-flex mb-3">
			    	<div class="w-100 me-lg-3 mb-3 mb-lg-0">
			    		<label>Posição do Código do Certificado na Horizontal (X)</label>
			    		<input type="number" name="codigo_posicao_x" class="form-control p-form" placeholder="N°">
			    	</div>
			    	<div class="w-100 me-lg-3 mb-3 mb-lg-0">
			    		<label>Posição do Código do Certificado na Vertical (Y)</label>
			    		<input type="number" name="codigo_posicao_y" class="form-control p-form" placeholder="N°">
			    	</div>
			    	<div class="w-lg-75 mb-3 mb-lg-0">
			    		<label>Tamanho da Fonte</label>
			    		<input type="number" name="codigo_fontSize" class="form-control p-form" placeholder="N° do Tamanho da Fonte de Letras">
			    	</div>
			    </div>

			</div>

		</section>

		<button type="submit" name="sent" value="ok" class="btn-add mt-3">Cadastrar</button>	

    </form>

</div>

@endsection

@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.14.0-beta2/dist/js/bootstrap-select.min.js"></script>
@endsection
