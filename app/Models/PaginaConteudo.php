<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Banner;

class PaginaConteudo extends Model
{
    use HasFactory;

    protected $table = 'paginas_conteudos';

    public $timestamps = false;
    
    public $fillable = [

    	'plataforma_id',
        'url',
        'conteudo',
        'view_propria',
        'seo_title',
        'seo_keywords',
        'seo_description',
        'status',
    ];

    public function scopePlataforma($query)
    {
        return $query->where('plataforma_id', session('plataforma_id') ?? Plataforma::dominio()->pluck('id')[0] ?? null);
    }

    public function scopeAtivo($query)
    {
        return $query->where('status', 0);
    }

}
