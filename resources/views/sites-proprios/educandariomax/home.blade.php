<!doctype html>
<html lang="pt-br">
<head>

   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1">

   <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/uikit@3.5.9/dist/css/uikit.min.css" />
   <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
   <link rel="stylesheet" href="{{ asset('css/util.css') }}?v=3">
   <link rel="stylesheet" href="{{ asset('sites-proprios/educandariomax/css/home.css') }}?v=3">

   <title>{{ $seo->seo_title ?? $plataforma->nome }}</title>
   <meta name="robots" content="index, follow">
   <meta name="description" content="{{ $seo->seo_description ?? null }}"/>
   <meta name="keywords" content="{{ $seo->seo_keywords ?? null }}"/>
   <link rel="canonical" href="{{ Request::url() }}" />
   <link rel="shortcut icon" href="{{ $plataforma->favicon }}">
   <meta property="og:locale" content="pt_BR" />
   <meta property="og:type" content="website" />
   <meta property="og:title" content="{{ $seo->seo_title ?? $plataforma->nome }}" />
   <meta property="og:description" content="{{ $seo->seo_description ?? null }}" />
   <meta property="og:url" content="{{ Request::url() }}" />
   <meta property="og:site_name" content="{{ $plataforma->nome }}" />
   <meta property="og:image" content="{{ $plataforma->logotipo }}" />
   <meta name='author' content='WOZCODE | Pedro Henrique' />

</head>
<body class="bg-f4" id="inicio">

   <header>
      
      <div class="sub-header container-fluid px-xl-7 py-2-3">
         <div class="d-flex justify-content-between align-items-center">

            <div>

               <span class="d-xs-none font-poppins text-light text-shadow-2 fs-12">
                  <i class="fa fa-envelope"></i>
                  {{ $plataforma->email ?? 'meuemail@exemlo.com' }}
               </span>

               <a href="https://api.whatsapp.com/send?phone=55{{preg_replace('/\D/', '', $plataforma->whatsapp)}}&text=Olá, vim através do Site {{ $plataforma->nome }}" target="_blank" class="ms-lg-3 text-shadow-2 text-d-none font-poppins text-light hover-d-none fs-12">
                  <i class="fab fa-whatsapp"></i>
                  {{ $plataforma->whatsapp ?? '(00) 00000-0000' }}
               </a>

            </div>

            <div class="d-flex justify-content-between align-items-end">

               <div>
                  <a href="{{ $plataforma->facebook }}" target="_blank" class="fs-17 text-d-none text-light text-shadow-2">
                       <i class="fab fa-facebook-f flex-center"></i>
                  </a>
                  <a href="{{ $plataforma->instagram }}" target="_blank" class="ms-2 fs-17 text-d-none text-light text-shadow-2">
                     <i class="fab fa-instagram flex-center"></i>
                  </a>
               </div>

            </div>

         </div>
      </div>

      <nav class="navbar navbar-expand-lg bg-white box-shadow py-0 z-3" id="menu">
         <div class="container-fluid px-xl-7">
             <a class="navbar-brand" href="#inicio" uk-scroll>
               <img src="{{ $plataforma->logotipo }}" class="d-xs-none" width="90" alt="Logotipo Educandario Max">
               <img src="{{ $plataforma->logotipo }}" class="d-md-none" id="logoMenuMobile" width="75" alt="Logotipo Educandario Max">
             </a>
             <button class="navbar-toggler outline-0 border-0 text-dark" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
               <span class="fa fa-bars"></span>
             </button>
             <div class="collapse navbar-collapse" id="navbarSupportedContent">
                  <ul id="ulLinks" class="navbar-nav ms-auto mb-2 mb-lg-0 align-items-lg-center p-3 p-lg-auto">
                     <li class="nav-item">
                        <a class="nav-link" href="#cursos" uk-scroll="offset: 140;">
                          Nossos Cursos
                        </a>
                     </li>
                     <li class="nav-item">
                        <a class="nav-link" href="#quem-somos" uk-scroll="offset: 140;">
                           Quem somos
                        </a>
                     </li>
                     <li class="nav-item">
                        <a class="nav-link" href="#contato" uk-scroll="offset: 100;">
                           Fale Conosco
                        </a>
                     </li>
                     <li class="nav-item">
                        <a class="nav-link" href="/login">
                           <i class="fas fa-graduation-cap"></i>
                           Área do Aluno
                        </a>
                     </li>
                     <li class="nav-item ms-lg-5 mt-lg-1 mt-4">
                       <a class="nav-link btnInscrever animation-grow d-inline-block" href="#pre-matricula" uk-scroll="offset: 140;">
                          Matricule-se
                       </a>
                     </li>
                 </ul>
             </div>
         </div>
      </nav>

   </header>

   <main>

      <section id="slides">
        <div class="uk-position-relative uk-visible-toggle" tabindex="-1" uk-slideshow="animation: push; autoplay: true; autoplay-interval: 5000; pause-on-hover: false; min-height: 550; max-height: 550">

            <ul class="uk-slideshow-items">
              @foreach($banners as $banner)
                <li>
                  <div class="uk-background-cover" uk-parallax="bgy: -100" style="background-image: url('{{ $banner->link }}');" uk-cover>
                     @if($banner->overlay == 'S')
                        <div class="background-overlay" style="background: {{ $banner->overlay_color ?? 'unset' }};opacity: {{ $banner->overlay_opacity ?? '0' }};"></div>
                     @endif
                     <div uk-scrollspy="cls: uk-animation-fade; delay: 400;" class="container-fluid px-xl-7 mt--10px uk-position-center-left uk-position-small uk-light">
                      
                        <h2 class="text-lg-center uk-margin-remove font-montserrat weight-800 display-3 ls--03" uk-slideshow-parallax="x: 300,0,-100">
                           {!! $banner->titulo !!}
                        </h2>

                        @if($banner->subtitulo)
                           <p uk-slideshow-parallax="x: 300,0,-75" class="text-lg-center uk-margin-remove text-light fs-20">
                              {!! $banner->subtitulo !!}
                           </p>
                        @endif

                        @if($banner->botao_texto)
                           <div class="mt-4-5 text-lg-center">
                              <a href="{{ $banner->botao_link }}" class="btnInscricao text-shadow-1 d-xs-block text-center text-shadow-1" uk-slideshow-parallax="x: 300,0,-50">
                                 {!! $banner->botao_texto !!}
                              </a>
                           </div>
                        @endif

                     </div>
                  </div>
                </li>
              @endforeach
            </ul>

            <div class="uk-light">
                <a class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous uk-slideshow-item="previous"></a>
                <a class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-next uk-slideshow-item="next"></a>
            </div>

        </div>
      </section>

      <section id="pre-matricula">
         <div style="background-image: url('{{ asset('images/bg-line.gif') }}');">
            <div class="container-fluid py-5 px-xl-7">

               <div class="bg-white p-lg-5 p-4 box-shadow rounded-20">

                  <h1 class="fs-40 text-center font-montserrat weight-800 text-dark">FAÇA SUA <span class="color-faixa-verde">PRÉ-MATRÍCULA</span></h1>

                  <p class="font-poppins text-center">
                     Preenchendo o formulário abaixo
                  </p>

                  <div class="flex-center mt-4 mb-4-5"><div class="w-80px h-2px bg-line rounded"></div></div>

                  <form method="POST">
                  @csrf

                    @if(session('success_prematricula'))
                       <div class='alert alert-success'><b>Sucesso!</b> {{ session('success_prematricula') }}</div>
                    @endif

                    <input type="hidden" name="assunto" value="Pré-Matrícula">
                     
                     <div class="d-lg-flex">
                        <input type="text" name="nome" class="form" placeholder="Seu nome" required>
                        <input type="text" name="email" class="mx-lg-3 my-3 my-lg-0 form" placeholder="Seu email" required>
                        <input type="text" name="telefone" class="form" placeholder="Telefone ou Celular" required>
                     </div>
                     <div class="w-100 mt-3">
                        <select name="curso_id" class="form weight-700 color-777" required>
                           <option value="">Selecione o Curso de Interesse</option>
                            @foreach($cursos as $curso)
                              <option value="{{ $curso->id }}" class="text-dark">{{ strip_tags($curso->nome) }}</option>
                            @endforeach
                        </select>
                     </div>
                     <div class="mt-3 mb-2">
                       <label class="cursor-pointer font-poppins fs-14 color-base-blue">
                          <input type="checkbox" class="" name="consentimento" value="1" required> Concordo que os dados pessoais fornecidos acima serão utilizados para envio de conteúdo informativo, analítico e publicitário sobre produtos, serviços e assuntos gerais, nos termos da Lei Geral de Proteção de Dados.
                       </label>
                     </div>
                     
                     <input type="text" name="check" class="invisible h-0px d-block" placeholder="Não preencha este campo.">

                     <button type="submit" name="sentContato" value="S" class="btnInscrever weight-700 box-shadow mt--20px">ENVIAR</button>

                  </form>

               </div>

            </div>
         </div>
      </section>

      <section id="quem-somos" class="py-5">
         <div class="bg-faixa p-lg-5 p-4 h-lg-550">
            <div class="container-fluid px-xl-4">

               <h1 class="font-montserrat text-light text-center weight-700 fs-md-55">
                  Mais de 3 anos <br> educando para o sucesso
               </h1>

               <div class="mt-4 mb-5 text-center px-xl-7">
                  <p class="px-xl-7 font-poppins fs-16 color-e2">
                    Este projeto teve início em 08/08/2018,com a finalidade em atender as crianças da nossas comunidade, oferecendo aos pais é responsável um serviço de segurança, qualidade a preço popular.
                    Nosso público alvo:as crianças da comunidade do Boogie Woogie de 0 á 6 anos e crianças com deficiência auditiva. Hoje atendemos 35 crianças em período integral e parcial.
                    Nossa equipe é composta por 10 funcionários,e três voluntários. Nossa meta é avançar! Com a finalidade em desenvolver crianças com habilidades pedagógicas e prepara-las para uma sociedade saudável.
                  </p>
               </div>

              <div class="uk-child-width-1-3@m" uk-grid>
                 <div>
                    <div class="uk-card-quem-somos text-center">
                     <div class="bg-white p-4 box-shadow rounded-20 h-lg-250">
                        <i class="fab fa-connectdevelop d-block mb-3 color-faixa-verde fs-50 mt-4"></i>
                        <p class="mb-0 font-montserrat weight-800 ls-02 color-navlink fs-20 mt-4">
                           Conectividade
                        </p>
                        <p class="mb-0 font-poppins weight-600 text-muted fs-14 mt-2">
                           Ensino dinâmico e interativo.
                        </p>
                     </div>
                    </div>
                 </div>
                 <div>
                    <div class="uk-card-quem-somos text-center">
                     <div class="bg-white p-4 box-shadow rounded-20 h-lg-250">
                        <i class="fas fa-chalkboard-teacher d-block mb-3 color-faixa-verde fs-50 mt-4"></i>
                        <p class="mb-0 font-montserrat weight-800 ls-02 color-navlink fs-20 mt-4">
                           Preparação
                        </p>
                        <p class="mb-0 font-poppins weight-600 text-muted fs-14 mt-2">
                           Ensino dinâmico e interativo.
                        </p>
                     </div>
                    </div>
                 </div>
                 <div>
                    <div class="uk-card-quem-somos text-center">
                     <div class="bg-white p-4 box-shadow rounded-20 h-lg-250">
                        <i class="fas fa-chart-line d-block mb-3 color-faixa-verde fs-50 mt-4"></i>
                        <p class="mb-0 font-montserrat weight-800 ls-02 color-navlink fs-20 mt-4">
                           Excelência
                        </p>
                        <p class="mb-0 font-poppins weight-600 text-muted fs-14 mt-2">
                           Ensino dinâmico e interativo.
                        </p>
                     </div>
                    </div>
                 </div>
              </div>

            </div>
         </div>
      </section>

      <section class="py-5">
         <div style="background-image: url('https://eaducativa.com/wp-content/uploads/2019/05/bg-gray.gif');">
            <div class="container-fluid px-xl-6 pt-lg-5" id="cursos">

               <h1 class="fs-37 text-center font-montserrat text-dark weight-800 mt-lg-5 pt-lg-3">NOSSOS <span class="color-faixa-verde">CURSOS</span></h1>

               <div class="mb-4-5 container px-xl-7">
                  <p class="font-poppins text-center px-xl-7">
                     A Educandário Max conta com diversas modalidades de cursos e serviços, adequando-se às necessidades específicas de cada cliente.
                  </p>
               </div>

               <div class="flex-center mt-4-5 mb-4-5"><div class="w-80px h-2px bg-line rounded"></div></div>

               <div class="pt-4-5 uk-child-width-1-3@m" uk-grid uk-scrollspy="cls: uk-animation-fade; target: .box-info; delay: 175;">

                  @foreach($cursos as $curso)
                  <div>
                     <div class="card box-info">
                        <a href="/curso/{{$curso->slug}}" class="text-d-none hover-d-none">
                           <img src="{{ $curso->foto_capa ?? asset('images/no-image.jpeg') }}" class="p-2 rounded card-img-top h-lg-200 object-fit-cover" alt="Curso de {{ $curso->nome }} - {{ $plataforma->nome }}">
                           <div class="card-body text-center h-min-lg-225">
                              <h3 class="font-poppins weight-600 fs-17 color-base-blue">
                                 {!! $curso->nome !!}
                              </h3>
                              <div>
                                 <i class="fa fa-star color-star"></i>
                                 <i class="fa fa-star color-star"></i>
                                 <i class="fa fa-star color-star"></i>
                                 <i class="fa fa-star color-star"></i>
                                 <i class="fa fa-star color-star"></i>
                              </div>
                              <p class="mt-2-5 mb-0 card-text font-poppins color-grafite weight-500">
                                 {{ $curso->seo_description }}
                              </p>
                           </div>
                           <div class="text-center pb-4-5">
                              <span class="mb-0 btnSaibaMais text-light fs-14">Saiba Mais</span>
                           </div>
                        </a>
                     </div>
                  </div>
                  @endforeach

               </div>

            </div>
         </div>
      </section>

      <section id="quem-somos" class="py-5">
         <div class="bg-faixa p-lg-5 p-4 h-lg-325">
            <div class="container-fluid px-xl-4">
               <div class="row">

                  <div class="col-lg-6 mb-4 mb-lg-0">

                     <div class="pe-lg-5">

                        <h1 class="fs-35 font-montserrat color-faixa weight-800 text-uppercase">O que falam de nós!</h1>
                        <div class="my-4 w-80px h-2px bg-line rounded"></div>

                        <p class="font-poppins color-faixa fs-14">
                          O CEEM agradece a todos os responsáveis que acreditam em nosso trabalho e confiam diariamente seu bem mais valioso em nossas mãos.
                          O Centro Educacional Educandário Max,oferecer ao nosso público alvo uma educação infantil 
                          com ética,valores e princípios.
                        </p>

                     </div>

                  </div>

                  <div class="col-lg-6">

                     <div class="uk-slider-container-offset" uk-slider>

                         <div class="uk-position-relative uk-visible-toggle uk-light" tabindex="-1">

                             <ul class="uk-slider-items uk-child-width-1-1@s uk-grid">
                                 <li>
                                     <div class="uk-card box-shadow bg-white rounded-10 h-lg-325 cursor-drag">
                                         <div class="uk-card-body text-dark">
                                             <i class="fas fa-quote-left fs-50 color-faixa-verde opacity-04"></i>
                                             <div class="mt-4 d-lg-flex align-items-center">
                                                <div style="width: 100px;">
                                                   <img src="{{ asset('sites-proprios/educandariomax/images/depoimentos/1.jpg') }}" class="object-fit-cover rounded-circle w-100">
                                                   <p class="mb-0 mt-3 color-faixa-verde text-lg-center ms--lg-5 weight-600">
                                                      Aquiles
                                                   </p>
                                                </div>
                                                <p class="mt-lg-3 mt-0 ms-lg-4 font-poppins fs-14 color-555">
                                                  No Educandário passei pela experiência de sentir a presença de Deus(me deu vontade de chorar,mas ficou preso na minha garganta), eu nem acredito que fiz um livro (risos)com várias histórias. Adoro os amigos que tenho na creche, eu sinto que estou numa igreja, eu penso na creche e e junto penso no espírito Santo de Deus.
                                                </p>
                                             </div>
                                         </div>
                                     </div>
                                 </li>
                                 <li>
                                     <div class="uk-card box-shadow bg-white rounded-10 h-lg-325 cursor-drag">
                                         <div class="uk-card-body text-dark">
                                             <i class="fas fa-quote-left fs-50 color-faixa-verde opacity-04"></i>
                                             <div class="mt-4 d-lg-flex align-items-center">
                                                <div style="width: 100px;">
                                                   <img src="{{ asset('sites-proprios/educandariomax/images/depoimentos/2.jpg') }}" class="object-fit-cover rounded-circle w-100">
                                                   <p class="mb-0 mt-3 color-faixa-verde text-lg-center ms--lg-5 weight-600">
                                                      Pedro Henryque
                                                   </p>
                                                </div>
                                                <p class="mt-lg-3 mt-0 ms-lg-4 font-poppins fs-14 color-555">
                                                  Eu gosto muito do Educandário Max 
                                                  Pois me sinto livre de ser eu mesmo, é uma família para mim. Gosto de estar na creche pois as tias me fazem ri e brincar. A gente fez um livro com muitas coisas.  Fiquei muito feliz por fazer o livro, as tias me ajudaram eu aprendi muito. Na creche também sinto a presença de Deus. Gosto de estar na creche é muito  legal.
                                                </p>
                                             </div>
                                         </div>
                                     </div>
                                 </li>
                             </ul>

                             <a class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous uk-slider-item="previous"></a>
                             <a class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-next uk-slider-item="next"></a>

                         </div>

                         <ul class="uk-slider-nav uk-dotnav uk-flex-center uk-margin"></ul>

                     </div>

                  </div>

               </div>
            </div>
         </div>
      </section>

   </main>

   <footer id="contato" style="background-image: url('{{ asset('sites-proprios/educandariomax/images/footer.jpg') }}'); background-repeat: no-repeat; background-size: cover; background-position: center;">
      <div class="container-fluid px-xl-7 pt-5" style="margin-top: -50px;">

         <div class="row align-items-end py-lg-4 px-3 px-md-0">

            <div class="col-lg-9">

               <h1 class="fs-35 font-montserrat color-faixa weight-800 text-uppercase">FALE CONOSCO</h1>
               <div class="my-4 w-80px h-2px bg-line rounded"></div>

               <img src="{{ $plataforma->logotipo }}" class="img-fluid" width="100">

               <div class="mt-2">                 

                  <div class="mb-3">
                     <span class="text-light font-poppins fs-14">
                        <i class="color-d4 fa fa-envelope"></i> 
                        {{ $plataforma->email ?? 'meuemail@exemlo.com' }}
                     </span>
                  </div>
                  <div class="mb-3">
                     <a href="https://api.whatsapp.com/send?phone=55{{preg_replace('/\D/', '', $plataforma->whatsapp)}}&text=Olá, vim através do Site {{ $plataforma->nome }}" class="text-light font-poppins fs-14">
                        <i class="color-d4 fab fa-whatsapp"></i> 
                        {{ $plataforma->whatsapp ?? '(00) 00000-0000' }}
                     </a>
                  </div>
                  <div class="mb-3">
                     <span class="text-light font-poppins fs-14">
                        <i class="color-d4 fa fa-clock"></i> 
                        Horário de Funcionamento: {{ $plataforma->horario ?? 'Segunda à sexta-feira das 09:00 às 18:00' }}
                     </span>
                  </div> 
                  <div class="mb-3">
                    <address>
                      <a href="https://www.google.com/maps/place/R.+Nossa+Sra.+das+Gra%C3%A7as,+16+-+Pitangueiras,+Rio+de+Janeiro+-+RJ,+21932-160/@-22.816727,-43.1837951,3a,90y,309.68h,81.41t/data=!3m6!1e1!3m4!1sEH9YpWezCMa2EgcnRZTlDQ!2e0!7i13312!8i6656!4m5!3m4!1s0x9978053a115757:0x6ce6de971070dcf7!8m2!3d-22.8167475!4d-43.1838824" target="_blank" class="text-light font-poppins fs-14">
                           <i class="color-d4 fas fa-map-marker-alt"></i>
                           {{ $plataforma->endereco ?? 'Pintangueiras - RJ. Rua Nossa Senhora das Graças, 16. 21932-230.' }}
                      </a>
                     </address>
                  </div>

                  <div class="mt-4 d-flex">

                     <a href="{{ $plataforma->facebook }}" target="_blank" class="me-2 hover-d-none">
                        <div class="square-40 rounded-circle social-icon-facebook flex-center">
                           <i class="text-white fab fa-facebook fs-20"></i>
                        </div>
                     </a>
                     <a href="{{ $plataforma->instagram }}" target="_blank" class="hover-d-none">
                        <div class="square-40 rounded-circle social-icon-instagram flex-center">
                           <i class="text-white fab fa-instagram fs-20"></i>
                        </div>
                     </a>

                  </div>

               </div>

            </div>

            <div class="col-lg-3 mt-5 mt-lg-0">

               <div class="mb-3">
                  <a href="/#pre-matricula" class="btn btn-primary weight-600 w-100 py-2-5 font-poppins hover-d-none">
                     MATRICULE-SE JÁ!
                  </a>
               </div>
               <div class="mb-3">
                  <a href="/login" class="btn btn-warning weight-600 w-100 py-2-5 font-poppins hover-d-none">
                     ÁREA DO ALUNO
                  </a>
               </div>
               <div class="mb-3">
                  <a href="/f/torne-se-parceiro/10/t/25" target="_blank" class="btn btn-light weight-600 w-100 py-2-5 font-poppins hover-d-none">
                     TORNE-SE PARCEIRO
                  </a>
               </div>
               <div class="mb-3">
                  <a href="https://api.whatsapp.com/send?phone=55{{preg_replace('/\D/', '', $plataforma->whatsapp)}}&text=Olá, vim através do Site {{ $plataforma->nome }}" class="btn btn-success bg-wpp text-shadow-1 weight-600 w-100 py-2-5 font-poppins hover-d-none">
                     ENTRAR EM CONTATO
                  </a>
               </div>

            </div>

         </div>

         <hr class="mt-4 border border-light" />

         <div class="pb-3 d-lg-flex justify-content-between align-items-center">

            <p class="text-xs-center mb-1 mb-lg-0 p-0 color-faixa font-poppins fs-11 ls-05">
               Centro Educacional Educandário Max © 2021 • Todos os direitos reservados • CNPJ: {{ $plataforma->cnpj }}
            </p>

            <p class="text-xs-center m-0 p-0 color-faixa font-poppins fs-11 ls-05">
               Desenvolvido por <a href="https://wozcode.com" target="_blank" class="text-danger text-d-none fs-11 weight-600 text-shadow-1">WOZCODE</a>
            </p>

         </div>

      </div>
   </footer>

   <a href="https://api.whatsapp.com/send?phone=55{{preg_replace('/\D/', '', $plataforma->whatsapp)}}&text=Olá, vim através do Site {{ $plataforma->nome }}" target="_blank" class="text-white fs-30">
       <div class="fixed-bottom right-0 me-3 mb-3 z-3">
           <div class="square-60 rounded-circle bg-wpp flex-center box-shadow-wpp">
               <i class="fab fa-whatsapp"></i>
           </div>
       </div>
   </a>

   <script src="https://kit.fontawesome.com/2f91eaaef4.js" crossorigin="anonymous"></script>  
   <script src="https://cdn.jsdelivr.net/npm/uikit@3.5.9/dist/js/uikit.min.js"></script>
   <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-U1DAWAznBHeqEIlVSCgzq+c9gqGAJn5c/t99JyeKa9xxaYpSvHU5awsuZVVFIhvj" crossorigin="anonymous"></script>
   <script>

      var isMobile = {
          Android: function() {
              return navigator.userAgent.match(/Android/i);
          },
          BlackBerry: function() {
              return navigator.userAgent.match(/BlackBerry/i);
          },
          iOS: function() {
              return navigator.userAgent.match(/iPhone|iPad|iPod/i);
          },
          Opera: function() {
              return navigator.userAgent.match(/Opera Mini/i);
          },
          Windows: function() {
              return navigator.userAgent.match(/IEMobile/i) || navigator.userAgent.match(/WPDesktop/i);
          },
          any: function() {
              return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
          }
      };

      if(isMobile.any() != null)
      {
        const navLinks = document.querySelectorAll('.nav-item');
        const menuToggle = document.getElementById('navbarSupportedContent');
        navLinks.forEach((l) => {
            l.addEventListener('click', () => { new bootstrap.Collapse(menuToggle) })
        })
      }

      window.onscroll = function() {myFunction()};

      var header = document.getElementById("menu");
      var sticky = header.offsetTop;

      function myFunction() {
        if (window.pageYOffset > sticky) {
          header.classList.add("fixed-top");
        } else {
          header.classList.remove("fixed-top");
        }
      }
   </script>

  <div class="box-cookies d-none fixed-bottom bg-white py-3 px-4 box-shadow">
    <div class="d-flex align-items-center justify-content-between px-xl-3">
       <p class="msg-cookies font-poppins mb-0 p-0 color-faixa-verde weight-600">
          Este site usa cookies para garantir que você obtenha a melhor experiência. 
          <a href="#ModalPoliticaPrivacidade" data-bs-toggle="modal" class="color-faixa-verde text-decoration-underline">
            Política de Privacidade
          </a>
       </p>
       <button class="btn-cookies btnInscrever weight-700 box-shadow">Ok!</button>
    </div>
  </div>
  <div class="modal fade" id="ModalPoliticaPrivacidade" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-dialog-scrollable modal-lg modal-dialog-centered border-0">
        <div class="modal-content border-0">
            <div class="modal-header bg-faixa">
              <h3 class="modal-title fs-20 text-white" id="exampleModalLabel">Política de Privacidade</h3>
              <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body px-lg-4">
              <p><strong><span style="font-size: 18px;">SEÇÃO 1 - INFORMAÇÕES GERAIS</span></strong></p><p>A presente Política de Privacidade contém informações sobre coleta, uso, armazenamento, tratamento e proteção dos dados pessoais dos usuários e visitantes do site, com a finalidade de demonstrar absoluta transparência quanto ao assunto e esclarecer a todos interessados sobre os tipos de dados que são coletados, os motivos da coleta e a forma como os usuários podem gerenciar ou excluir as suas informações pessoais.</p><p>Esta Política de Privacidade aplica-se a todos os usuários e visitantes do site e integra os Termos e Condições Gerais de Uso do site.</p><p>O presente documento foi elaborado em conformidade com a Lei Geral de Proteção de Dados Pessoais (Lei <a class="cite" href="https://www.jusbrasil.com.br/legislacao/612902269/lei-13709-18" rel="200399658" title="LEI Nº 13.709, DE 14 DE AGOSTO DE 2018.">13.709</a>/18), o <a class="cite" href="https://www.jusbrasil.com.br/legislacao/117197216/lei-12965-14" rel="27363947" title="LEI Nº 12.965, DE 23 ABRIL DE 2014.">Marco Civil da Internet</a> (Lei <a class="cite" href="https://www.jusbrasil.com.br/legislacao/117197216/lei-12965-14" rel="27363947" title="LEI Nº 12.965, DE 23 ABRIL DE 2014.">12.965</a>/14) (e o Regulamento da UE n. 2016/6790). Ainda, o documento poderá ser atualizado em decorrência de eventual atualização normativa, razão pela qual se convida o usuário a consultar periodicamente esta seção.</p><p><br></p><p><strong><span style="font-size: 18px;">SEÇÃO 2 - COMO RECOLHEMOS OS DADOS PESSOAIS DO USUÁRIO E DO VISITANTE?</span></strong></p><p>Os dados pessoais do usuário e visitante são recolhidos pela plataforma da seguinte forma:</p><ul><li>Quando o usuário cria uma conta/perfil na plataforma: Esses dados são os dados de identificação básicos, como: e-mail, nome completo, telefone e endereço completo e documento). A partir deles, podemos identificar o usuário e o visitante, além de garantir uma maior segurança e bem-estar às suas necessidades. Ficam cientes os usuários e visitantes de que seu perfil na plataforma estará acessível a todos demais usuários e visitantes da plataforma.</li><li>Quando um usuário e visitante acessa OU páginas do site: as informações sobre interação e acesso são coletadas pela empresa para garantir uma melhor experiência ao usuário e visitante. Estes dados podem tratar sobre as palavras-chaves utilizadas em uma busca, o compartilhamento de um documento específico, comentários, visualizações de páginas, perfis, a URL de onde o usuário e visitante provêm, o navegador que utilizam e seus IPs de acesso, dentre outras que poderão ser armazenadas e retidas.</li><li>Por intermédio de terceiro: a plataforma recebe dados de terceiros, como Facebook e Google, quando um usuário faz login com o seu perfil de um desses sites. A utilização desses dados é autorizada previamente pelos usuários junto ao terceiro em questão.</li></ul><p><br></p><p><strong><span style="font-size: 18px;">SEÇÃO 3 - QUAIS DADOS PESSOAIS RECOLHEMOS SOBRE O USUÁRIO E VISITANTE?</span></strong></p><p>Os dados pessoais do usuário e visitante recolhidos são os seguintes:</p><ul><li>Dados para a criação da conta/perfil na plataforma: e-mail, telefone, nome completo, endereço completo e documento).</li><li>Dados para otimização da navegação: acesso a páginas, palavras-chave utilizadas na busca, recomendações, comentários, interação com outros perfis e usuários, perfis seguidos, endereço de IP.</li><li>Dados para concretizar transações: dados referentes ao pagamento e transações, tais como, número do cartão de crédito e outras informações sobre o cartão, além dos pagamentos efetuados.</li><li>Newsletter: o e-mail cadastrado pelo visitante que optar por se inscrever na Newsletter será coletado e armazenado até que o usuário solicite o descadastro.</li><li>Dados sensíveis: a plataforma poderá coletar os seguintes dados sensíveis do usuário como: origem étnica ou racial, opinião política, convicção religiosa, dados genéticos, dados relativos à saúde, orientação sexual.</li><li>Dados relacionados a contratos: diante da formalização do contrato de compra e venda ou de prestação de serviços entre a plataforma e o usuário e visitante poderão ser coletados e armazenados dados relativos a execução contratual, inclusive as comunicações realizada entre a empresa e o usuário.</li></ul><p><br></p><p><strong><span style="font-size: 18px;">SEÇÃO 3 - PARA QUE FINALIDADES UTILIZAMOS OS DADOS PESSOAIS DO USUÁRIO E VISITANTE?</span></strong></p><p>Os dados pessoais do usuário e do visitante coletados e armazenados pela plataforma tem por finalidade:</p><ul><li>Bem-estar do usuário e visitante: aprimorar o produto e/ou serviço oferecido, facilitar, agilizar e cumprir os compromissos estabelecidos entre o usuário e a empresa, melhorar a experiência dos usuários e fornecer funcionalidades específicas a depender das características básicas do usuário.</li><li>Melhorias da plataforma: compreender como o usuário utiliza os serviços da plataforma, para ajudar no desenvolvimento de negócios e técnicas.</li><li>Anúncios: apresentar anúncios personalizados para o usuário com base nos dados fornecidos.</li><li>Comercial: os dados são usados para personalizar o conteúdo oferecido e gerar subsídio à plataforma para a melhora da qualidade no funcionamento dos serviços.</li><li>Previsão do perfil do usuário: tratamento automatizados de dados pessoais para avaliar o uso na plataforma.</li><li>Dados de cadastro: para permitir o acesso do usuário a determinados conteúdos da plataforma, exclusivo para usuários cadastrados.</li><li>Dados de contrato: conferir às partes segurança jurídica e facilitar a conclusão do negócio.</li><li>O tratamento de dados pessoais para finalidades não previstas nesta Política de Privacidade somente ocorrerá mediante comunicação prévia ao usuário, de modo que os direitos e obrigações aqui previstos permanecem aplicáveis.</li></ul><p><br></p><p><strong><span style="font-size: 18px;">SEÇÃO 4 - POR QUANTO TEMPO OS DADOS PESSOAIS FICAM ARMAZENADOS?</span></strong></p><p>Os dados pessoais do usuário e visitante são armazenados pela plataforma durante o período necessário para a prestação do serviço ou o cumprimento das finalidades previstas no presente documento, conforme o disposto no inciso <a class="cite" href="https://www.jusbrasil.com.br/topicos/200399061/inciso-i-do-artigo-15-da-lei-n-13709-de-14-de-agosto-de-2018" rel="200399061" title="Inciso I do Artigo 15 da Lei nº 13.709 de 14 de Agosto de 2018">I</a> do artigo <a class="cite" href="https://www.jusbrasil.com.br/topicos/200399064/artigo-15-da-lei-n-13709-de-14-de-agosto-de-2018" rel="200399064" title="Artigo 15 da Lei nº 13.709 de 14 de Agosto de 2018">15</a> da Lei <a class="cite" href="https://www.jusbrasil.com.br/legislacao/612902269/lei-13709-18" rel="200399658" title="LEI Nº 13.709, DE 14 DE AGOSTO DE 2018.">13.709</a>/18.</p><p>Os dados podem ser removidos ou anonimizados a pedido do usuário, excetuando os casos em que a lei oferecer outro tratamento.</p><p>Ainda, os dados pessoais dos usuários apenas podem ser conservados após o término de seu tratamento nas seguintes hipóteses previstas no artigo 16 da referida lei:&nbsp;</p><p>&nbsp; I - cumprimento de obrigação legal ou regulatória pelo controlador;</p><p>&nbsp; II - estudo por órgão de pesquisa, garantida, sempre que possível, a anonimização dos dados pessoais;</p><p>&nbsp; III - transferência a terceiro, desde que respeitados os requisitos de tratamento de dados dispostos nesta Lei;</p><p>&nbsp; IV - uso exclusivo do controlador, vedado seu acesso por terceiro, e desde que anonimizados os dados.</p><p><br></p><p><strong><span style="font-size: 18px;">SEÇÃO 5 - SEGURANÇA DOS DADOS PESSOAIS ARMAZENADOS</span></strong></p><p>A plataforma se compromete a aplicar as medidas técnicas e organizativas aptas a proteger os dados pessoais de acessos não autorizados e de situações de destruição, perda, alteração, comunicação ou difusão de tais dados.</p><p>Os dados relativos a cartões de crédito são criptografados usando a tecnologia "secure socket layer" (SSL) que garante a transmissão de dados de forma segura e confidencial, de modo que a transmissão dos dados entre o servidor e o usuário ocorre de maneira cifrada e encriptada.</p><p>A plataforma não se exime de responsabilidade por culpa exclusiva de terceiro, como em caso de ataque de hackers ou crackers, ou culpa exclusiva do usuário, como no caso em que ele mesmo transfere seus dados a terceiros. O site se compromete a comunicar o usuário em caso de alguma violação de segurança dos seus dados pessoais.</p><p>Os dados pessoais armazenados são tratados com confidencialidade, dentro dos limites legais. No entanto, podemos divulgar suas informações pessoais caso sejamos obrigados pela lei para fazê-lo ou se você violar nossos Termos de Serviço.</p><p><br></p><p><strong><span style="font-size: 18px;">SEÇÃO 6 - COMPARTILHAMENTO DOS DADOS</span></strong></p><p>O compartilhamento de dados do usuário ocorre apenas com os dados referentes a publicações realizadas pelo próprio usuário, tais ações são compartilhadas publicamente com os outros usuários.</p><p>Os dados do perfil do usuário não são compartilhados publicamente em sistemas de busca.</p><p><br></p><p><strong><span style="font-size: 18px;">SEÇÃO 6 - OS DADOS PESSOAIS ARMAZENADOS SERÃO TRANSFERIDOS A TERCEIROS?</span></strong></p><p>Com relação aos fornecedores de serviços terceirizados como processadores de transação de pagamento, informamos que cada qual tem sua própria política de privacidade. Desse modo, recomendamos a leitura das suas políticas de privacidade para compreensão de quais informações pessoais serão usadas por esses fornecedores.</p><p>Os fornecedores podem ser localizados ou possuir instalações localizadas em países diferentes. Nessas condições, os dados pessoais transferidos podem se sujeitar às leis de jurisdições nas quais o fornecedor de serviço ou suas instalações estão localizados.</p><p>Ao acessar nossos serviços e prover suas informações, você está consentindo o processamento, transferência e armazenamento desta informação em outros países.</p><p>Ao ser redirecionado para um aplicativo ou site de terceiros, você não será mais regido por essa Política de Privacidade ou pelos Termos de Serviço da nossa plataforma. Não somos responsáveis pelas práticas de privacidade de outros sites e lhe incentivamos a ler as declarações de privacidade deles.</p><p><br></p><p><strong><span style="font-size: 18px;">SEÇÃO 07 – COOKIES OU DADOS DE NAVEGAÇÃO</span></strong></p><p>Os cookies referem-se a arquivos de texto enviados pela plataforma ao computador do usuário e visitante e que nele ficam armazenados, com informações relacionadas à navegação no site. Tais informações são relacionadas aos dados de acesso como local e horário de acesso e são armazenadas pelo navegador do usuário e visitante para que o servidor da plataforma possa lê-las posteriormente a fim de personalizar os serviços da plataforma.</p><p>O usuário e o visitante da plataforma manifesta conhecer e aceitar que pode ser utilizado um sistema de coleta de dados de navegação mediante à utilização de cookies.</p><p>O cookie persistente permanece no disco rígido do usuário e visitante depois que o navegador é fechado e será usado pelo navegador em visitas subsequentes ao site. Os cookies persistentes podem ser removidos seguindo as instruções do seu navegador. Já o cookie de sessão é temporário e desaparece depois que o navegador é fechado. É possível redefinir seu navegador da web para recusar todos os cookies, porém alguns recursos da plataforma podem não funcionar corretamente se a capacidade de aceitar cookies estiver desabilitada.</p><p><br></p><p><strong><span style="font-size: 18px;">SEÇÃO 8 - CONSENTIMENTO</span></strong></p><p>Ao utilizar os serviços e fornecer as informações pessoais na plataforma, o usuário está consentindo com a presente Política de Privacidade.</p><p>O usuário, ao cadastrar-se, manifesta conhecer e pode exercitar seus direitos de cancelar seu cadastro, acessar e atualizar seus dados pessoais e garante a veracidade das informações por ele disponibilizadas.</p><p>O usuário tem direito de retirar o seu consentimento a qualquer tempo, para tanto deve entrar em contato através formulário no rodapé do site.</p><p><br></p><p><strong><span style="font-size: 18px;">SEÇÃO 9 - ALTERAÇÕES PARA ESSA POLÍTICA DE PRIVACIDADE</span></strong></p><p>Reservamos o direito de modificar essa Política de Privacidade a qualquer momento, então, é recomendável que o usuário e visitante revise-a com frequência.</p><p>As alterações e esclarecimentos vão surtir efeito imediatamente após sua publicação na plataforma. Quando realizadas alterações os usuários serão notificados. Ao utilizar o serviço ou fornecer informações pessoais após eventuais modificações, o usuário e visitante demonstra sua concordância com as novas normas.</p><p>Diante da fusão ou venda da plataforma à outra empresa os dados dos usuários podem ser transferidas para os novos proprietários para que a permanência dos serviços oferecidos.</p><p><br></p><p><strong><span style="font-size: 18px;">SEÇÃO 10 – JURISDIÇÃO PARA RESOLUÇÃO DE CONFLITOS</span></strong></p><p>Para a solução de controvérsias decorrentes do presente instrumento será aplicado integralmente o Direito brasileiro.</p><p>Os eventuais litígios deverão ser apresentados no foro da comarca em que se encontra a sede da empresa.</p>
            </div>
        </div>
      </div>
  </div>
  <script>
  (() => {
    if (!localStorage.getItem('accept')) {
      document.querySelector(".box-cookies").classList.remove('d-none');
    }
    
    const acceptCookies = () => {
      document.querySelector(".box-cookies").classList.add('d-none');
      localStorage.setItem('accept', 'S');
    };
    
    const btnCookies = document.querySelector(".btn-cookies");

    btnCookies.addEventListener('click', acceptCookies);
  })();
  </script>

</body>
</html>