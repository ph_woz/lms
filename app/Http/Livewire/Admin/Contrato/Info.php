<?php

namespace App\Http\Livewire\Admin\Contrato;

use Livewire\Component;
use App\Models\Contrato;
use App\Models\ContratoCampo;
use App\Models\User;
use App\Models\Unidade;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Http;

class Info extends Component
{
    public $contrato;
    public $password_to_delete;
    public $cadastrante;
    public $unidade;

    // ADD CAMPO
    public $campo_id;
    public $campo;
    public $n_pagina;
    public $px;
    public $py;
    public $fontSize;
    public $color;

    public $gerar_contrato_aluno_id;
    public $gerar_contrato_aluno_email;


    public function mount($contrato_id)
    {
        $this->gerar_contrato_aluno_id = $_GET['gerar_contrato_aluno_id'] ?? null;

        $this->contrato = Contrato::plataforma()->find($contrato_id);

        $this->unidade = Unidade::plataforma()->find($this->contrato->unidade_id);

        $this->cadastrante = User::plataforma()->where('id', $this->contrato->cadastrante_id)->first();
    }

    public function render()
    {
        $campos = ContratoCampo::plataforma()->where('contrato_id', $this->contrato->id)->get();

        $alunosGerarContrato = [];
        $endereco = [];
        $alunoEscolhidoGerarContrato = null;
        $enderecoCompleto = null;
        $enderecoRuaENumero = null;

        if(strlen($this->gerar_contrato_aluno_email) >= 5)
        {
            $alunosGerarContrato = User::select('id','nome','email')->plataforma()->ativo()->aluno()->where('email', 'like', '%'.$this->gerar_contrato_aluno_email.'%')->limit(5)->get();
        }

        if(isset($this->gerar_contrato_aluno_id))
        {
            $alunoEscolhidoGerarContrato = User::plataforma()->ativo()->where('id', $this->gerar_contrato_aluno_id)->first();

            $endereco = \App\Models\UserEndereco::plataforma()->where('user_id', $alunoEscolhidoGerarContrato->id)->first() ?? array();

            if($endereco)
            {
                $endereco = $endereco->toArray();
                
                $rua         = $endereco['rua']         ?? null;
                $numero      = $endereco['numero']      ?? null;
                $bairro      = $endereco['bairro']      ?? null;
                $cidade      = $endereco['cidade']      ?? null;
                $estado      = $endereco['estado']      ?? null;
                $cep         = $endereco['cep']         ?? null;
                $complemento = $endereco['complemento'] ?? null;

                $enderecoCompleto = $rua . ', ' . $numero  . ' - ' . $bairro . ', ' . $cidade . ' - ' . $estado . ', ' . $cep . '. ' . $complemento;
                $enderecoRuaENumero = $rua . ', ' . $numero;
            }
        }

        return view('livewire.admin.contrato.info', compact('campos','alunosGerarContrato','alunoEscolhidoGerarContrato','endereco','enderecoCompleto','enderecoRuaENumero') );
    }

    public function addCampo()
    {
        if($this->px == '')       { $this->px = null;       }
        if($this->py == '')       { $this->py = null;       }
        if($this->fontSize == '') { $this->fontSize = null; }
        if($this->n_pagina == '') { $this->n_pagina = null; }
        if($this->color == '')    { $this->color = null;    }

        ContratoCampo::create([
            'contrato_id' => $this->contrato->id,
            'campo'       => $this->campo,
            'n_pagina'    => $this->n_pagina,
            'px'          => $this->px,
            'py'          => $this->py,
            'fontSize'    => $this->fontSize,
            'color'       => $this->color,
        ]);

        $this->resetFields();

        session()->flash('success-livewire', 'Campo adicionado com êxito.');

        $this->dispatchBrowserEvent('toast');
        $this->dispatchBrowserEvent('dissmiss-modal');
    }

    public function editarCampo()
    {
        if($this->px == '')       { $this->px = null;       }
        if($this->py == '')       { $this->py = null;       }
        if($this->fontSize == '') { $this->fontSize = null; }
        if($this->n_pagina == '') { $this->n_pagina = null; }
        if($this->color == '')    { $this->color = null;    }

        ContratoCampo::plataforma()->where('id', $this->campo_id)->update([
            'campo'       => $this->campo,
            'px'          => $this->px,
            'py'          => $this->py,
            'fontSize'    => $this->fontSize,
            'n_pagina'    => $this->n_pagina,
            'color'       => $this->color,
        ]);

        session()->flash('success-livewire', 'Campo atualizado com êxito.');

        $this->dispatchBrowserEvent('toast');
    }

    public function deleteCampo()
    {
        ContratoCampo::plataforma()->where('id', $this->campo_id)->delete();
        
        session()->flash('success-livewire', 'Campo removido com êxito.');
        $this->dispatchBrowserEvent('toast');
        $this->dispatchBrowserEvent('dissmiss-modal');
    }

    public function setCamposParaEditar($campo_id)
    {
        $campo = ContratoCampo::plataforma()->where('id', $campo_id)->first();

        $this->campo_id = $campo_id;
        $this->campo    = $campo->campo;
        $this->px       = $campo->px;
        $this->py       = $campo->py;
        $this->fontSize = $campo->fontSize;
        $this->n_pagina = $campo->n_pagina;
        $this->color    = $campo->color;
    }

    public function setDeleteCampoId($campo_id)
    {
        $this->campo_id = $campo_id;
    }

    public function resetFields()
    {
        $this->campo_id = null;
        $this->campo    = null;
        $this->px       = null;
        $this->py       = null;
        $this->fontSize = null;
        $this->n_pagina = null;
        $this->color    = null;
    }

    public function delete()
    {
        if(\Auth::attempt(['id' => \Auth::id(), 'password' => $this->password_to_delete, 'status' => 0]))
        {
            $contrato = Contrato::plataforma()->where('id', $this->contrato->id)->first();

            ContratoCampo::plataforma()->where('contrato_id', $contrato->id)->delete();

            $path = str_replace(\App\Models\Util::getLinkStorage(), '', $contrato->modelo);
            Storage::delete($path);

            $caminhoPDF = str_replace(\Request::root().'/', '', $contrato->pdf);

            if(file_exists($caminhoPDF) && \Str::contains($caminhoPDF, '.pdf') == true)
            {
                unlink($caminhoPDF);
            }

            $contrato->delete();

            session()->flash('success', 'Contrato deletado com êxito.');
            
            return redirect(route('admin.contrato.lista'));

        } else {

            session()->flash('danger-livewire', 'Senha incorreta.');

            $this->dispatchBrowserEvent('toast');
        }
    }
}
