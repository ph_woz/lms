@extends('layouts.aluno')
@section('seo_title', 'Avaliações')
@section('content')
<section class="container-fluid px-xl-5 z-3 mt-5 position-relative">

<div class="text-center mb-5 mt-3">
	<a href="?listar=" class="me-2 btn-suporte-enviar py-2-3 px-lg-5 @if($listar == 'a-fazer') btn-active @endif">
		A fazer
	</a>
	<a href="?listar=feitas" class="ms-2 btn-suporte-enviar py-2-3 px-lg-5 @if($listar == 'feitas') btn-active @endif">
		Já feitas
	</a>
</div>

@foreach($avaliacoes as $avaliacao)
	
<div class="mb-4 px-4-5 py-4 rounded-10 box-shadow bg-14">

	<h5 class="mb-3 title fs-17">
		{{ $avaliacao->referencia }}
	</h5>

	<hr/>

	<div class="row align-items-center">

		<div class="col-lg-9"> 
	
			@if($avaliacao->n_tentativas || $avaliacao->instrucoes)

				@if($avaliacao->n_tentativas)
					<p class="mb-2 color-light-soft">N° de Tentativas: {{ $avaliacao->n_tentativa_atual ?? '0' }} de {{ $avaliacao->n_tentativas }}</p>
				@endif

				@if($avaliacao->instrucoes)
				<p class="mb-0 color-light-soft">
					<span class="d-block">Instruções:</span>
					<span class="color-b7">
						{{ $avaliacao->instrucoes ?? 'Não definido' }}
					</span>
				</p>
				@endif

				<hr class="d-md-none" />

			@endif

		</div>

		<div class="col-lg-3">

			<div class="text-lg-center ms-lg-4 my-2"> 

				@if($avaliacao->status == null)
					@if($avaliacao->condicao_fazer_avaliacao == null)
						<a href="{{ route('aluno.avaliacao', ['id' => $avaliacao->id]) }}" class="btn-suporte-enviar py-2-5 px-5">
							Iniciar
						</a>
					@else
						@if($avaliacao->data_conclusao_conteudo != null)
							<a href="{{ route('aluno.avaliacao', ['id' => $avaliacao->id]) }}" class="btn-suporte-enviar py-2-5 px-5">
								Iniciar
							</a>
						@else
							<div class="mensagemCondicaoAvaliacao color-light-soft">
								É necessário que você conclua todas as aulas do curso para que esta avaliação seja liberada
							</div>
						@endif
					@endif
				@endif

				@if($avaliacao->status == 'F')
					<p class="mb-0 color-light-soft">
						Finalizada. <br> Sua nota foi: <strong>{{ number_format($avaliacao->nota, 2, ',', '.') }}</strong>.
					</p>
					@if($avaliacao->liberar_gabarito == 'S')
						<div class="mt-4">
							@if($avaliacao->n_tentativa_atual >= $avaliacao->n_tentativas)
								<a href="{{ route('aluno.avaliacao.gabarito', ['id' => $avaliacao->id]) }}" class="btn-suporte-enviar py-2-5 px-4">
									Ver Gabarito
								</a>
							@else
								@php
									$nota_minima_aprovacao = \App\Models\Turma::select('nota_minima_aprovacao')->plataforma()->ativo()->where('id', $avaliacao->turma_id)->pluck('nota_minima_aprovacao')[0] ?? 0;
								@endphp
								@if($avaliacao->nota < floatval($nota_minima_aprovacao))
									<a href="{{ route('aluno.avaliacao.reiniciar', ['id' => $avaliacao->id]) }}" class="btn-suporte-enviar py-2-5 px-4">
										Tentar Novamente
									</a>
								@else
									<a href="{{ route('aluno.avaliacao.gabarito', ['id' => $avaliacao->id]) }}" class="btn-suporte-enviar py-2-5 px-4">
										Ver Gabarito
									</a>
								@endif
							@endif
						</div>
					@endif
				@endif

			</div>

		</div>

	</div>

</div>

@endforeach

</section>
@endsection