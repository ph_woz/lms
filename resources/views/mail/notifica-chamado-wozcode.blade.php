<h1 style="font-size: 20px;font-weight: 500;">
	{{ strtok($nome, " ") }} abriu este Chamado.
</h1>

<p style="font-size:16.5px;margin-bottom: 0em!important; font-weight: 600;">{{ $assunto }}</p>

<div style="font-size: 16.5px; margin-bottom: 2rem;">
	<p>
		{!! nl2br($descricao) !!}
	</p>
</div>

<div>
	<a href="https://admlms.wozcode.com/admin/chamado/info/{{$id}}" style="display: inline-block; color: #FFF; text-decoration: none; border-radius:2px; background:#1865d9; padding-left:20px; padding-right:20px; padding-top:10px; padding-bottom:10px;">
		Abrir Chamado
	</a>
</div>