<!doctype html>
<html lang="pt-br">
<head>

   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1">

   <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/uikit@3.5.9/dist/css/uikit.min.css" />
   <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
   <link rel="stylesheet" href="{{ asset('css/util.css') }}?v=4">
   <link rel="stylesheet" href="{{ asset('sites-proprios/faro/css/home.css') }}?v=3">

   <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">

   <title>{{ $pagina->seo_title ?? $plataforma->nome }}</title>
   <meta name="robots" content="index, follow">
   <meta name="description" content="{{ $pagina->seo_description ?? null }}"/>
   <meta name="keywords" content="{{ $pagina->seo_keywords ?? null }}"/>
   <link rel="canonical" href="{{ Request::url() }}" />
   <link rel="shortcut icon" href="{{ $plataforma->favicon }}">
   <meta property="og:locale" content="pt_BR" />
   <meta property="og:type" content="website" />
   <meta property="og:title" content="{{ $pagina->seo_title ?? $plataforma->nome }}" />
   <meta property="og:description" content="{{ $pagina->seo_description ?? null }}" />
   <meta property="og:url" content="{{ Request::url() }}" />
   <meta property="og:site_name" content="{{ $plataforma->nome }}" />
   <meta property="og:image" content="{{ $plataforma->logotipo }}" />
   <meta name='author' content='WOZCODE | Pedro Henrique' />

   <style>
      .line { background:rgba(120, 144, 156, 0.2); width: 5px; height: 100%; float:left; }
   </style>

</head>
<body class="bg-f4" id="inicio">

   @include('sites-proprios/faro/componentes/header')

   <main>

      <section>
           <div class="w-100" style="background-size: cover; background-position: center; background-image: url('{{ $plataforma->jumbotron ?? 'https://yata.s3-object.locaweb.com.br/8fd17de7588f66611d70c6df80b8e0dbefbf907add82ccb385b7b3bf642c3662' }}');">
              <div class="position-relative h-100 px-xl-7 px-3">
              <div class="background-overlay opacity-08 opacity-095 h-100" style="background: rgba(0,0,0,1);"></div>

                 <div class="container align-items-center h-100 position-relative">
                    <div class="py-5">
                       <h1 class="pt-5 text-light text-center font-poppins weight-800 fs-xs-35 fs-md-50 text-shadow-3" id="tituloOuvidoria">
                           OUVIDORIA FARO
                       </h1>
                       <p class="pt-4 text-center text-light font-poppins ls-05 fs-19">
                          A Ouvidoria é um órgão com uma função estratégica, que busca ouvir, acolher, mediar conflitos e promover mudanças. É um canal de comunicação imparcial e independente, que serve para defender direitos, minimizar demandas jurídicas e auxiliar na boa governança. 
                          <br><br>
                           A Ouvidoria leva a demanda, seja ela interna ou externa, a quem de fato tem poder para resolvê-la, além de acompanhar o andamento da sugestão, reclamação ou denúncia realizada. 
                           <br><br>
                           Aqui na Faculdade de Roseira, observando os princípios constitucionais de legalidade, a Ouvidoria tem o papel de aproximar a instituição de ensino e a população, garantindo direito de resposta sempre de forma objetiva e no menor prazo possível. 
                       </p>
                       <div class="text-center pt-lg-4 pb-lg-4">
                          <a href="/f/formulario-de-ouvidoria/56/t/680" target="_blank" class="btn-info color-icon-footer weight-500 ls-05 hover-d-none hover-dark fs-18">
                           Registre agora pela web
                          </a>
                        </div>
                    </div>
                 </div>
              </div>
           </div>
      </section>

      <section>
            
         <div class="row g-0 w-100">

             <div class="col-lg-6 h-100vh bg-gradient">
               <div class="p-lg-5 p-4 text-center">

                  <h1 class="mb-4 text-dark font-poppins weight-700">
                     Nós queremos ouvir o que você tem a dizer!
                  </h1>

                  <p class="font-poppins weight-600 fs-18">
                     Pode ser um elogio, uma crítica, uma solicitação, uma sugestão, uma reclamação ou uma denúncia sobre assuntos relacionados à Faculdade de Roseira. 
                     <br><br>
                     Todas as mensagens enviadas à Ouvidoria são tratadas de forma sigilosa e respeitando as regras de compliance e legislação. 
                     <br><br>
                     Mande sua mensagem para a Ouvidoria Faro por meio de nossa plataforma online
                  </p>

                  <div class="pt-4-5">
                     <a href="/f/formulario-de-ouvidoria/56/t/680" target="_blank" class="bg-faixa bg-hover-gradient fs-17 ls-05 text-center px-3-5 rounded box-shadow text-light weight-700 py-2-5 font-poppins hover-d-none d-xs-block">
                        Registre agora pela web
                     </a>
                  </div>

               </div>
             </div>
             
             <div class="col-lg-6 my-4 my-lg-0">

                  <div class="line"></div>

                  <div class="row align-items-center h-100">
                      
                      <div class="mx-auto px-5">

                          <div class="text-center mt--30px" id="boxLogotipoOuvidoria">
                              <img src="{{ asset('sites-proprios/faro/images/ouvidoria.png') }}" class="img-fluid" alt="Logotipo FARO Ouvidoria">
                          </div>

                      </div>

                  </div>
             </div>

         </div>

      </section>

   </main>

   @include('sites-proprios/faro/componentes/footer')

   <a href="https://api.whatsapp.com/send?phone=55{{preg_replace('/\D/', '', $plataforma->whatsapp)}}&text=Olá, vim através do Site {{ $plataforma->nome }}" target="_blank" class="text-white fs-30">
       <div class="fixed-bottom right-0 me-3 mb-3 z-3">
           <div class="square-60 rounded-circle bg-wpp flex-center box-shadow-wpp">
               <i class="fab fa-whatsapp"></i>
           </div>
       </div>
   </a>

   <script src="https://cdn.jsdelivr.net/npm/uikit@3.5.9/dist/js/uikit.min.js"></script>
   <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-U1DAWAznBHeqEIlVSCgzq+c9gqGAJn5c/t99JyeKa9xxaYpSvHU5awsuZVVFIhvj" crossorigin="anonymous"></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>
   <script>

      window.onscroll = function() {myFunction()};

      var header = document.getElementById("menu");
      var sticky = header.offsetTop;

      function myFunction() {
        if (window.pageYOffset > sticky) {
          header.classList.add("fixed-top");
        } else {
          header.classList.remove("fixed-top");
        }
      }
   </script>

</body>
</html>