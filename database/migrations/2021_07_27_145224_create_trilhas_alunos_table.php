<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTrilhasAlunosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trilhas_alunos', function (Blueprint $table) {
            $table->id();
            $table->integer('plataforma_id');
            $table->integer('trilha_id');
            $table->integer('aluno_id');
            $table->dateTime('data_conclusao')->nullable();
            $table->date('data_colacao_grau')->nullable();
            $table->text('codigo_certificado')->nullable();
            $table->integer('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trilhas_alunos');
    }
}
