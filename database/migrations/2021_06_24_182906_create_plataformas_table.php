<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlataformasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plataformas', function (Blueprint $table) {
            $table->id();
            $table->string('nome');
            $table->string('dominio')->unique();
            $table->integer('status')->default(0);
            $table->integer('categoria_id')->nullable();
            $table->text('logotipo')->nullable();
            $table->text('favicon')->nullable();
            $table->text('jumbotron')->nullable();
            $table->text('fundo_login')->nullable();
            $table->text('email_contratacao_turma')->nullable();
            $table->text('email_contratacao_trilha')->nullable();
            $table->integer('responsavel_id')->nullable();
            $table->integer('plano_id')->nullable();
            $table->string('cnpj')->nullable();
            $table->string('tipo_negocio')->nullable();
            $table->string('email')->nullable();
            $table->string('endereco')->nullable();
            $table->string('horario')->nullable();
            $table->string('whatsapp')->nullable();
            $table->string('telefone')->nullable();
            $table->text('facebook')->nullable();
            $table->text('instagram')->nullable();
            $table->text('youtube')->nullable();
            $table->text('api_key')->nullable();
            $table->string('tema', 1)->default('D')->nullable();
            $table->string('forma_login', 5)->default('email')->nullable();
            $table->text('scripts_start_head')->nullable();
            $table->text('scripts_final_head')->nullable();
            $table->text('scripts_start_body')->nullable();
            $table->text('scripts_final_body')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plataformas');
    }
}
