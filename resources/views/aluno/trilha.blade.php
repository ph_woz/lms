@extends('layouts.aluno')
@section('content')
@section('seo_title', 'Dashboard')

@if($checkComunicadosNaoLidos === true)
<script>
	localStorage.setItem('hasComunicado', true);
</script>
@endif

<section>
	<img src="{{ $plataforma->jumbotron ?? asset('images/jumbotron.png') }}" class="img-fluid w-100 object-fit-cover" style="max-height: 300px;">
</section>

<section class="container-fluid px-xl-4 z-3 position-relative">

	<div class="mb-4 px-4 pt-4 pb-4-5 rounded-10 box-shadow mt--15px bg-14">

		<h5 class="title mb-4" id="info-gerais-da-trilha-aluno"><span>Informações Gerais da Trilha</span></h5>
		
		<div class="progress" data-bs-toggle="tooltip" data-bs-placement="top" data-html="true" title="Progresso: {{$progresso}}%">
			<div class="progress-bar progress-bar-striped @if($progresso >= 100 && $check_data_conclusao == true) bg-success @endif" role="progressbar" style="width: {{$progresso}}%;" aria-valuenow="{{$progresso}}" aria-valuemin="0" aria-valuemax="100">{{$progresso}}%</div>
		</div>

		<div class="d-lg-flex">

			@if($trilha->atestado_id)
			<div class="mt-4-5">
				<a href="{{ route('aluno.gerar-atestado-trilha', ['atestado_id' => $trilha->atestado_id, 'trilha_id' => $trilha->id]) }}" target="_blank" class="hover-d-none btnMarcarComoConcluido px-3 py-2-5">
					<i class="far fa-file-pdf"></i>&nbsp;
					Baixar Atestado de Matrícula
				</a>
			</div>
			@endif

			@if($trilha->certificado_id && $check_data_conclusao == true)
			<div class="mt-4-5">
				<a href="{{ route('aluno.gerar-certificado-trilha', ['certificado_id' => $trilha->certificado_id, 'trilha_id' => $trilha->id]) }}" target="_blank" class="hover-d-none ms-lg-2 btnMarcarComoConcluido px-3 py-2-5">
					<i class="far fa-file-pdf"></i>&nbsp;
					Baixar Certificado
				</a>
			</div>
			@endif

		</div>
		
	</div>

	<div class="mb-4 px-4 pt-4 pb-3 rounded-10 box-shadow bg-14">

		<h5 class="title mb-4">Em andamento</h5>

		<div class="row">

			@foreach($turmasEmAndamento as $curso)
			<div class="col-lg-3 mb-4">
				<a href="{{ route('aluno.aulas', ['slug' => $curso->slug, 'curso_id' => $curso->id]) }}" class="color-b7 fs-16 hover-white hover-d-none">
					<img src="{{ $curso->foto_miniatura ?? asset('images/no-image.jpeg') }}" class="figure-img img-fluid rounded w-100 h-150 object-fit-cover">
					<div class="px-1">
						<span>
							{{ $curso->nome }}
						</span>
					</div>
				</a>
			</div>
			@endforeach

		</div>

	</div>

	<div class="mb-4 px-4 pt-4 pb-3 rounded-10 box-shadow bg-14">

		<h5 class="title mb-4">Disponíveis</h5>

		<div class="row">

			@foreach($turmasDisponiveis as $curso)
			<div class="col-lg-3 mb-4">
				<a href="{{ route('aluno.aulas', ['slug' => $curso->slug, 'curso_id' => $curso->id]) }}" class="color-b7 fs-16 hover-white hover-d-none">
					<img src="{{ $curso->foto_miniatura ?? asset('images/no-image.jpeg') }}" class="figure-img img-fluid rounded w-100 h-150 object-fit-cover">
					<div class="px-1">
						<span>
							{{ $curso->nome }}
						</span>
					</div>
				</a>
			</div>
			@endforeach

		</div>

	</div>

	<div class="mb-4 px-4 pt-4 pb-3 rounded-10 box-shadow bg-14">

		<h5 class="title mb-4">Concluído</h5>

		<div class="row">

			@foreach($turmasConcluidas as $curso)
			<div class="col-lg-3 mb-4">
				<a href="{{ route('aluno.aulas', ['slug' => $curso->slug, 'curso_id' => $curso->id]) }}" class="color-b7 fs-16 hover-white hover-d-none">
					<img src="{{ $curso->foto_miniatura ?? asset('images/no-image.jpeg') }}" class="figure-img img-fluid rounded w-100 h-150 object-fit-cover">
					<div class="px-1">
						<span>
							{{ $curso->nome }}
						</span>
					</div>
				</a>
			</div>
			@endforeach

		</div>

	</div>

</section>

@push('scripts')
<script>
	var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
	var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
	  return new bootstrap.Tooltip(tooltipTriggerEl)
	})
</script>
@endpush

@endsection