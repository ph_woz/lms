@php

  $cursosGraduacao = \App\Models\Curso::select('nome','slug','foto_miniatura')
      ->plataforma()
      ->ativo()
      ->publico()
      ->graduacao()
      ->orderBy('nome')
      ->get();

  $cursosPosGraduacao = \App\Models\Curso::select('nome','slug','foto_miniatura')
      ->plataforma()
      ->ativo()
      ->publico()
      ->posGraduacao()
      ->orderBy('nome')
      ->get();

  $cursosExtensao = \App\Models\Curso::select('nome','slug')
      ->plataforma()
      ->ativo()
      ->publico()
      ->cursosExtensao()
      ->orderBy('nome')
      ->get();

@endphp

<!doctype html>
<html lang="pt-br">
<head>

  <!-- Google tag (gtag.js) -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=G-MREB450HCT"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'G-MREB450HCT');
  </script>
    
    {!! $plataforma->scripts_start_head !!}

   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1">

   <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/uikit@3.5.9/dist/css/uikit.min.css" />
   <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
   <link rel="stylesheet" href="{{ asset('css/util.css') }}?v=5">
   <link rel="stylesheet" href="{{ asset('sites-proprios/faro/css/home.css') }}?v=4">

   <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
   
   <title>{{ $seo->seo_title ?? $plataforma->nome }}</title>
   <meta name="robots" content="index, follow">
   <meta name="description" content="{{ $seo->seo_description ?? null }}"/>
   <meta name="keywords" content="{{ $seo->seo_keywords ?? null }}"/>
   <link rel="canonical" href="{{ Request::url() }}" />
   <link rel="shortcut icon" href="{{ $plataforma->favicon }}">
   <meta property="og:locale" content="pt_BR" />
   <meta property="og:type" content="website" />
   <meta property="og:title" content="{{ $seo->seo_title ?? $plataforma->nome }}" />
   <meta property="og:description" content="{{ $seo->seo_description ?? null }}" />
   <meta property="og:url" content="{{ Request::url() }}" />
   <meta property="og:site_name" content="{{ $plataforma->nome }}" />
   <meta property="og:image" content="{{ $plataforma->logotipo }}" />
   <meta name='author' content='WOZCODE | Pedro Henrique' />

   {!! $plataforma->scripts_final_head !!}
   
</head>
<body class="bg-f4" id="inicio">

   @include('sites-proprios/faro/componentes/header')

   <main>

      <section id="banners">
        <div class="uk-position-relative uk-visible-toggle" tabindex="-1" uk-slideshow="autoplay: true; autoplay-interval: 5000; pause-on-hover: false; max-height: 450">

            <ul class="uk-slideshow-items">
              @foreach($banners as $banner)
                <li>
                  <div style="background-image: url('{{ $banner->link }}');" class="bg-img-banner">
                     @if($banner->overlay == 'S')
                        <div class="background-overlay" style="background: {{ $banner->overlay_color ?? 'unset' }};opacity: {{ $banner->overlay_opacity ?? '0' }};"></div>
                     @endif
                     <div uk-scrollspy="cls: uk-animation-fade; delay: 400;" class="container mt--10px uk-position-center-left uk-position-small uk-light">
                      
                        <h2 class="text-center uk-margin-remove font-montserrat weight-800 display-3 ls--03" uk-slideshow-parallax="x: 300,0,-100">
                           {!! $banner->titulo !!}
                        </h2>

                        @if($banner->subtitulo)
                           <p uk-slideshow-parallax="x: 300,0,-75" class="text-center uk-margin-remove text-light fs-20">
                              {!! $banner->subtitulo !!}
                           </p>
                        @endif

                        @if($banner->botao_texto)
                           <div class="mt-4-5 text-center">
                              <a href="{{ $banner->botao_link }}" class="btnInscricao text-shadow-1 d-xs-block text-center text-shadow-1" uk-slideshow-parallax="x: 300,0,-50">
                                 {!! $banner->botao_texto !!}
                              </a>
                           </div>
                        @endif

                     </div>
                  </div>
                </li>
              @endforeach
            </ul>

            <div class="uk-light">
                <a class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous uk-slideshow-item="previous"></a>
                <a class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-next uk-slideshow-item="next"></a>
            </div>

        </div>
      </section>

      <section id="cursos">
         <div>
            <div class="container py-5">

               <h1 class="fs-md-40 text-center font-montserrat weight-800 text-dark">NOSSOS <span class="color-faixa-verde">CURSOS</span></h1>

               <div class="mb-4-5 container px-xl-8">
                  <p class="font-poppins text-center">
                     A FARO conta com diversas modalidades de cursos e serviços, adequando-se às necessidades específicas de cada aluno.
                  </p>
               </div>

               <div class="flex-center mt-4 mb-4-5"><div class="w-80px h-2px bg-line rounded"></div></div>

               <div class="bg-white pt-5 px-5 pb-4 box-shadow rounded-20">

                  <h2 class="font-montserrat weight-800 fs-20">
                     GRADUAÇÃO
                  </h2>

                  <hr>

                  <div class="uk-slider-container-offset" uk-slider="autoplay: true; autoplay-interval: 1500; pause-on-hover: true">

                      <div class="uk-position-relative uk-visible-toggle uk-light" tabindex="-1">

                          <ul class="uk-slider-items uk-child-width-1-5@s">
                              @foreach($cursosGraduacao as $curso)
                              <li class="px-2">
                                 <a href="/curso/{{$curso->slug}}" class="w-100">
                                    <div class="w-100 uk-inline-clip uk-transition-toggle rounded box-shadow mb-4 mb-lg-0" tabindex="0">
                                       <img src="{{ $curso->foto_miniatura ?? asset('images/no-image.jpeg') }}" class="uk-transition-scale-up uk-transition-opaque object-fit-cover w-100 h-lg-350 h-xs-350">
                                       <div class="uk-position-bottom fig-caption flex-center px-3">
                                          <h5 class="text-light fs-17 font-montserrat weight-700 ls-05 text-uppercase text-center">
                                             {{ $curso->nome }}
                                          </h5>
                                       </div>
                                    </div>
                                 </a>
                              </li>
                              @endforeach
                          </ul>

                          <a class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous uk-slider-item="previous"></a>
                          <a class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-next uk-slider-item="next"></a>

                      </div>

                      <ul class="uk-slider-nav uk-dotnav uk-flex-center uk-margin"></ul>

                  </div>

               </div>

               <div class="mt-5 bg-white pt-5 px-5 pb-4 box-shadow rounded-20">

                  <h2 class="font-montserrat weight-800 fs-20">
                     PÓS-GRADUAÇÃO
                  </h2>

                  <hr>

                  <div class="uk-slider-container-offset" uk-slider="autoplay: true; autoplay-interval: 1500; pause-on-hover: true">

                      <div class="uk-position-relative uk-visible-toggle uk-light" tabindex="-1">

                          <ul class="uk-slider-items uk-child-width-1-5@s">
                              @foreach($cursosPosGraduacao as $curso)
                              <li class="px-2">
                                 <a href="/curso/{{$curso->slug}}" class="w-100">
                                    <div class="w-100 uk-inline-clip uk-transition-toggle rounded box-shadow mb-4 mb-lg-0" tabindex="0">
                                       <img src="{{ $curso->foto_miniatura ?? asset('images/no-image.jpeg') }}" class="uk-transition-scale-up uk-transition-opaque object-fit-cover w-100 h-lg-350 h-xs-350">
                                       <div class="uk-position-bottom fig-caption flex-center px-3">
                                          <h5 class="text-light fs-17 font-montserrat weight-700 ls-05 text-uppercase text-center">
                                             {{ $curso->nome }}
                                          </h5>
                                       </div>
                                    </div>
                                 </a>
                              </li>
                              @endforeach
                          </ul>

                          <a class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous uk-slider-item="previous"></a>
                          <a class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-next uk-slider-item="next"></a>

                      </div>

                      <ul class="uk-slider-nav uk-dotnav uk-flex-center uk-margin"></ul>

                  </div>

               </div>
            </div>
         </div>
      </section>

      <section id="a-faro" class="py-5">
         <div class="bg-faixa p-lg-5 p-4 h-lg-350">
            <div class="container">
               <div class="row">

                  <div class="col-lg-6 mb-4 mb-lg-0">

                     <div class="pe-lg-5">

                        <h1 class="fs-35 font-montserrat color-faixa weight-800 text-uppercase">A FARO</h1>
                        <div class="my-4 w-80px h-2px bg-line rounded"></div>

                        <p class="font-poppins color-faixa fs-16">
                          A FARO agradece a todos os responsáveis que acreditam em nosso trabalho e confiam diariamente seu bem mais valioso em nossas mãos.
                          O FARO, oferecer ao nosso público alvo uma educação com ética,valores e princípios.
                        </p>

                        <div class="mb-4 mb-lg-0">
                           <a href="/f/formulario-de-matricula/45/t/631" target="_blank" class="btnCall box-shadow font-montserrat weight-800 fs-17 ls-05">
                              MATRÍCULE-SE
                           </a>
                        </div>

                     </div>

                  </div>

                  <div class="col-lg-6">

                     <h1 class="fs-18 font-montserrat color-faixa weight-800 text-uppercase">O QUE ESTÃO DIZENDO NOSSOS ALUNOS FORMADOS</h1>

                     <div class="uk-slider-container-offset" uk-slider>

                         <div class="uk-position-relative uk-visible-toggle uk-light" tabindex="-1">

                             <ul class="uk-slider-items uk-child-width-1-1@s uk-grid">
                                 @foreach($depoimentos as $depoimento)
                                 <li>
                                     <div class="uk-card box-shadow bg-white rounded-10 h-lg-325 cursor-drag">
                                         <div class="uk-card-body text-dark">
                                             <i class="fas fa-quote-left fs-50 color-faixa-verde opacity-04"></i>
                                             <div class="mt-1 d-lg-flex align-items-center">
                                                <div class="my-2 mb-lg-0 d-xs-flex align-items-center w-200px">
                                                   <div class="w-100px text-center">
                                                      <img src="{{ $depoimento->foto }}" class="object-fit-cover rounded-circle square-75">
                                                   </div>
                                                   <p class="mb-0 color-faixa-verde weight-600 text-center">
                                                      {{ $depoimento->nome }}
                                                   </p>
                                                </div>
                                                <div>
                                                   <p class="mt--lg-15px ms-lg-4 font-poppins fs-14 color-555 mb-2">
                                                     {{ $depoimento->descricao }}
                                                   </p>
                                                   <div class="ms-lg-4">
                                                      <i class="fa fa-star color-star"></i>
                                                      <i class="fa fa-star color-star"></i>
                                                      <i class="fa fa-star color-star"></i>
                                                      <i class="fa fa-star color-star"></i>
                                                      <i class="fa fa-star color-star"></i>
                                                   </div>
                                                </div>
                                             </div>
                                         </div>
                                     </div>
                                 </li>
                                 @endforeach
                             </ul>

                             <a class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous uk-slider-item="previous"></a>
                             <a class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-next uk-slider-item="next"></a>

                         </div>

                         <ul class="uk-slider-nav uk-dotnav uk-flex-center uk-margin"></ul>

                     </div>

                  </div>

               </div>
            </div>
         </div>
      </section>
      
      @if(count($postsIdsBlog) >= 4) 
      <section id="blog">
         <div class="container pt-lg-4 pb-5 pb-lg-0">

            <h1 class="fs-md-40 font-montserrat weight-800 text-dark">NOTÍCIAS/<span class="color-faixa-verde">BLOG</span></h1>

            <div class="d-lg-none mb-5 mb-lg-0">

              <div class="uk-position-relative uk-visible-toggle" tabindex="-1" uk-slideshow="animation: push">

                  <ul class="uk-slideshow-items">
                      <li>
                        <a href="/blog/post/{{$postDestaque1->slug}}/{{$postDestaque1->id}}">
                           <div class="w-100 rounded-2">
                              <div class="bg-size-cover h-md-500 h-xs-250 rounded-2" style="background-image: linear-gradient(to bottom, rgba(0, 0, 0, .1), rgba(0, 0, 0, 1)), url('{{ $postDestaque1->foto_capa }}');"></div>
                              <div class="uk-position-bottom px-4 pb-4">
                                 <h5 class="text-light text-shadow-2 weight-700 fs-md-30 fs-xs-18 font-montserrat mb-0 p-0">
                                    {{ $postDestaque1->titulo }}
                                 </h5>
                                 <hr class="border" />
                                 <div class="d-flex justify-content-between align-items-center">
                                   <p class="mb-0 fs-12">
                                    @if($postDestaque1->autor)
                                     <span class="color-c9">por</span>
                                     <span class="color-c9 weight-600">{{ \App\Models\Util::getPrimeiroSegundoNome($postDestaque1->autor) }}</span>
                                    @endif
                                   </p>
                                  <p class="mb-0 font-nunito fs-12">
                                    <i class="fa fa-clock color-c9"></i>
                                    <span class="color-c9">{{ \Carbon\Carbon::createFromTimeStamp(strtotime($postDestaque1->created_at))->diffForHumans() }}</span>
                                  </p>
                                 </div>
                              </div>
                           </div>
                        </a>
                      </li>
                      <li>
                        <a href="/blog/post/{{$postDestaque2->slug}}/{{$postDestaque2->id}}">
                           <div class="w-100 rounded-2">
                              <div class="bg-size-cover h-md-500 h-xs-250 rounded-2" style="background-image: linear-gradient(to bottom, rgba(0, 0, 0, .1), rgba(0, 0, 0, 1)), url('{{ $postDestaque2->foto_capa }}');"></div>
                              <div class="uk-position-bottom px-4 pb-4">
                                 <h5 class="text-light text-shadow-2 weight-700 fs-md-25 fs-xs-18 font-montserrat mb-0 p-0">
                                    {{ $postDestaque2->titulo }}
                                 </h5>
                                 <hr class="border" />
                                 <div class="d-flex justify-content-between align-items-center">
                                   <p class="mb-0 fs-12">
                                    @if($postDestaque2->autor)
                                     <span class="color-c9">por</span>
                                     <span class="color-c9 weight-600">{{ \App\Models\Util::getPrimeiroSegundoNome($postDestaque2->autor) }}</span>
                                    @endif
                                   </p>
                                  <p class="mb-0 font-nunito fs-12">
                                    <i class="fa fa-clock color-c9"></i>
                                    <span class="color-c9">{{ \Carbon\Carbon::createFromTimeStamp(strtotime($postDestaque2->created_at))->diffForHumans() }}</span>
                                  </p>
                                 </div>
                              </div>
                           </div>
                        </a>
                      </li>
                      <li>
                       <a href="/blog/post/{{$postDestaque3->slug}}/{{$postDestaque3->id}}">
                          <div class="w-100 rounded-2">
                             <div class="bg-size-cover h-md-500 h-xs-250" style="background-image: linear-gradient(to bottom, rgba(0, 0, 0, .1), rgba(0, 0, 0, 1)), url('{{ $postDestaque3->foto_capa }}');"></div>
                             <div class="uk-position-bottom px-4 pb-4">
                                <h5 class="text-light text-shadow-2 weight-700 fs-18 font-montserrat mb-0 p-0">
                                   {{ $postDestaque3->titulo }}
                                </h5>
                                <hr class="border" />
                                <div class="d-flex justify-content-between align-items-center">
                                  <p class="mb-0 fs-12">
                                   @if($postDestaque3->autor)
                                    <span class="color-c9">por</span>
                                    <span class="color-c9 weight-600">{{ \App\Models\Util::getPrimeiroSegundoNome($postDestaque3->autor) }}</span>
                                   @endif
                                  </p>
                                 <p class="mb-0 font-nunito fs-12">
                                   <i class="fa fa-clock color-c9"></i>
                                   <span class="color-c9">{{ \Carbon\Carbon::createFromTimeStamp(strtotime($postDestaque3->created_at))->diffForHumans() }}</span>
                                 </p>
                                </div>
                             </div>
                          </div>
                       </a>
                      </li>
                      <li>
                         <a href="/blog/post/{{$postDestaque4->slug}}/{{$postDestaque4->id}}">
                            <div class="w-100 rounded-2">
                               <div class="bg-size-cover h-md-500 h-xs-250" style="background-image: linear-gradient(to bottom, rgba(0, 0, 0, .1), rgba(0, 0, 0, 1)), url('{{ $postDestaque4->foto_capa }}');"></div>
                               <div class="uk-position-bottom px-4 pb-4">
                                  <h5 class="text-light text-shadow-2 weight-700 fs-18 font-montserrat mb-0 p-0">
                                     {{ $postDestaque4->titulo }}
                                  </h5>
                                  <hr class="border" />
                                  <div class="d-flex justify-content-between align-items-center">
                                    <p class="mb-0 fs-12">
                                     @if($postDestaque4->autor)
                                      <span class="color-c9">por</span>
                                      <span class="color-c9 weight-600">{{ \App\Models\Util::getPrimeiroSegundoNome($postDestaque4->autor) }}</span>
                                     @endif
                                    </p>
                                   <p class="mb-0 font-nunito fs-12">
                                     <i class="fa fa-clock color-c9"></i>
                                     <span class="color-c9">{{ \Carbon\Carbon::createFromTimeStamp(strtotime($postDestaque4->created_at))->diffForHumans() }}</span>
                                   </p>
                                  </div>
                               </div>
                            </div>
                         </a>
                      </li>
                  </ul>

                  <div class="uk-light">
                      <a class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous uk-slideshow-item="previous"></a>
                      <a class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-next uk-slideshow-item="next"></a>
                  </div>

              </div>

            </div>

            <div class="d-md-none d-xs-none d-lg-block">

              <div class="row">

                 <div class="col-lg-6 px-lg-2">
                    
                    <a href="/blog/post/{{$postDestaque1->slug}}/{{$postDestaque1->id}}">
                       <div class="w-100 uk-inline-clip uk-transition-toggle box-convenios rounded-2">
                          <div class="uk-transition-scale-up uk-transition-opaque bg-size-cover h-lg-500 h-xs-250 rounded-2" style="background-image: linear-gradient(to bottom, rgba(0, 0, 0, .1), rgba(0, 0, 0, 1)), url('{{ $postDestaque1->foto_capa }}');"></div>
                          <div class="uk-position-bottom px-4 pb-4">
                             <h5 class="text-light text-shadow-2 weight-700 fs-md-30 fs-xs-18 font-montserrat mb-0 p-0">
                                {{ $postDestaque1->titulo }}
                             </h5>
                             <hr class="border" />
                             <div class="d-flex justify-content-between align-items-center">
                               <p class="mb-0 fs-12">
                                @if($postDestaque1->autor)
                                 <span class="color-c9">por</span>
                                 <span class="color-c9 weight-600">{{ \App\Models\Util::getPrimeiroSegundoNome($postDestaque1->autor) }}</span>
                                @endif
                               </p>
                              <p class="mb-0 font-nunito fs-12">
                                <i class="fa fa-clock color-c9"></i>
                                <span class="color-c9">{{ \Carbon\Carbon::createFromTimeStamp(strtotime($postDestaque1->created_at))->diffForHumans() }}</span>
                              </p>
                             </div>
                          </div>
                       </div>
                    </a>

                 </div>

                 <div class="col-lg-3 px-lg-2 py-3 py-lg-0">

                    <a href="/blog/post/{{$postDestaque2->slug}}/{{$postDestaque2->id}}">
                       <div class="w-100 uk-inline-clip uk-transition-toggle box-convenios rounded-2">
                          <div class="uk-transition-scale-up uk-transition-opaque bg-size-cover h-lg-500 h-xs-250 rounded-2" style="background-image: linear-gradient(to bottom, rgba(0, 0, 0, .1), rgba(0, 0, 0, 1)), url('{{ $postDestaque2->foto_capa }}');"></div>
                          <div class="uk-position-bottom px-4 pb-4">
                             <h5 class="text-light text-shadow-2 weight-700 fs-md-25 fs-xs-18 font-montserrat mb-0 p-0">
                                {{ $postDestaque2->titulo }}
                             </h5>
                             <hr class="border" />
                             <div class="d-flex justify-content-between align-items-center">
                               <p class="mb-0 fs-12">
                                @if($postDestaque2->autor)
                                 <span class="color-c9">por</span>
                                 <span class="color-c9 weight-600">{{ \App\Models\Util::getPrimeiroSegundoNome($postDestaque2->autor) }}</span>
                                @endif
                               </p>
                              <p class="mb-0 font-nunito fs-12">
                                <i class="fa fa-clock color-c9"></i>
                                <span class="color-c9">{{ \Carbon\Carbon::createFromTimeStamp(strtotime($postDestaque2->created_at))->diffForHumans() }}</span>
                              </p>
                             </div>
                          </div>
                       </div>
                    </a>

                 </div>

                 <div class="col-lg-3 px-lg-2">

                    <div>
                       <a href="/blog/post/{{$postDestaque3->slug}}/{{$postDestaque3->id}}">
                          <div class="w-100 uk-inline-clip uk-transition-toggle box-convenios rounded-2">
                             <div class="uk-transition-scale-up uk-transition-opaque bg-size-cover h-lg-240 h-xs-250" style="background-image: linear-gradient(to bottom, rgba(0, 0, 0, .1), rgba(0, 0, 0, 1)), url('{{ $postDestaque3->foto_capa }}');"></div>
                             <div class="uk-position-bottom px-4 pb-4">
                                <h5 class="text-light text-shadow-2 weight-700 fs-18 font-montserrat mb-0 p-0">
                                   {{ $postDestaque3->titulo }}
                                </h5>
                                <hr class="border" />
                                <div class="d-flex justify-content-between align-items-center">
                                  <p class="mb-0 fs-12">
                                   @if($postDestaque3->autor)
                                    <span class="color-c9">por</span>
                                    <span class="color-c9 weight-600">{{ \App\Models\Util::getPrimeiroSegundoNome($postDestaque3->autor) }}</span>
                                   @endif
                                  </p>
                                 <p class="mb-0 font-nunito fs-12">
                                   <i class="fa fa-clock color-c9"></i>
                                   <span class="color-c9">{{ \Carbon\Carbon::createFromTimeStamp(strtotime($postDestaque3->created_at))->diffForHumans() }}</span>
                                 </p>
                                </div>
                             </div>
                          </div>
                       </a>
                    </div>
                    <div class="py-2"></div>
                    <div>
                       <a href="/blog/post/{{$postDestaque4->slug}}/{{$postDestaque4->id}}">
                          <div class="w-100 uk-inline-clip uk-transition-toggle box-convenios rounded-2">
                             <div class="uk-transition-scale-up uk-transition-opaque bg-size-cover h-lg-240 h-xs-250" style="background-image: linear-gradient(to bottom, rgba(0, 0, 0, .1), rgba(0, 0, 0, 1)), url('{{ $postDestaque4->foto_capa }}');"></div>
                             <div class="uk-position-bottom px-4 pb-4">
                                <h5 class="text-light text-shadow-2 weight-700 fs-18 font-montserrat mb-0 p-0">
                                   {{ $postDestaque4->titulo }}
                                </h5>
                                <hr class="border" />
                                <div class="d-flex justify-content-between align-items-center">
                                  <p class="mb-0 fs-12">
                                   @if($postDestaque4->autor)
                                    <span class="color-c9">por</span>
                                    <span class="color-c9 weight-600">{{ \App\Models\Util::getPrimeiroSegundoNome($postDestaque4->autor) }}</span>
                                   @endif
                                  </p>
                                 <p class="mb-0 font-nunito fs-12">
                                   <i class="fa fa-clock color-c9"></i>
                                   <span class="color-c9">{{ \Carbon\Carbon::createFromTimeStamp(strtotime($postDestaque4->created_at))->diffForHumans() }}</span>
                                 </p>
                                </div>
                             </div>
                          </div>
                       </a>
                    </div>

                 </div>

              </div>

            </div>

         </div>
      </section>
      @endif

      <section id="pre-matricula" class="pt-lg-5 pb-lg-5">
         <div class="container pt-lg-5 pt-4 pb-5">

           @if(session('success_prematricula'))
              <div class='alert alert-success'><b>Sucesso!</b> {{ session('success_prematricula') }}</div>
           @endif

            <div class="row align-items-center">

               <div class="order-1 col-lg-6">

                  <div class="bg-white p-4 box-shadow rounded-lg-20 rounded-bottom-20 h-md-475">

                     <h1 class="fs-30 px-1 text-center font-montserrat weight-800 text-dark">FAÇA SUA <span class="color-faixa-verde">PRÉ-MATRÍCULA</span></h1>

                     <div class="flex-center mt-4 mb-4-5"><div class="w-80px h-2px bg-line rounded"></div></div>

                     <form method="POST">
                     @csrf

                       <input type="hidden" name="assunto" value="Pré-Matrícula">
                        
                        <input type="text" name="nome" class="form" placeholder="Seu nome" required>
                        <input type="text" name="email" class="my-1 form" placeholder="Seu email" required>
                        <input type="text" name="telefone" class="form" placeholder="Telefone ou Celular" required>

                        <div class="mt-3 mb-2">
                          <label class="cursor-pointer font-poppins fs-14 color-base-blue">
                             <input type="checkbox" class="" name="consentimento" value="1" required> Concordo que os dados pessoais fornecidos acima serão utilizados para envio de conteúdo informativo, analítico e publicitário sobre produtos, serviços e assuntos gerais, nos termos da Lei Geral de Proteção de Dados.
                          </label>
                        </div>
                        
                        <input type="text" name="check" class="invisible h-0px d-block">

                        <button type="submit" name="sentContato" value="S" class="mt-1 btnInscrever weight-700 box-shadow">ENVIAR</button>

                     </form>

                  </div>

               </div>

               <div class="order-0 col-lg-6">

                  <div class="bg-gradient box-shadow rounded-lg-20 rounded-top-20 h-md-475 h-xs-250">

                     <div class="position-relative h-md-475 h-xs-250">
                        <img src="{{ asset('sites-proprios/faro/images/mascote.png') }}" alt="Mascote | FARO" class="object-fit-conatain overflow-hidden" style="vertical-align:bottom; border:0; position: absolute; left: 0; bottom: 0;">
                     </div>

                  </div>

               </div>

            </div>      
         </div>
      </section>


   </main>

   @include('sites-proprios/faro/componentes/footer')

   <a href="https://api.whatsapp.com/send?phone=55{{preg_replace('/\D/', '', $plataforma->whatsapp)}}&text=Olá, vim através do Site {{ $plataforma->nome }}" target="_blank" class="text-white fs-30">
       <div class="fixed-bottom right-0 me-3 mb-3 z-3">
           <div class="square-60 rounded-circle bg-wpp flex-center box-shadow-wpp">
               <i class="fab fa-whatsapp"></i>
           </div>
       </div>
   </a>

   <script src="https://cdn.jsdelivr.net/npm/uikit@3.5.9/dist/js/uikit.min.js"></script>
   <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-U1DAWAznBHeqEIlVSCgzq+c9gqGAJn5c/t99JyeKa9xxaYpSvHU5awsuZVVFIhvj" crossorigin="anonymous"></script>
   <script>

      window.onscroll = function() {myFunction()};

      var header = document.getElementById("menu");
      var sticky = header.offsetTop;

      function myFunction() {
        if (window.pageYOffset > sticky) {
          header.classList.add("fixed-top");
        } else {
          header.classList.remove("fixed-top");
        }
      }
   </script>

  <div class="box-cookies d-none fixed-bottom bg-white py-3 px-4 box-shadow">
    <div class="d-flex align-items-center justify-content-between px-xl-3">
       <p class="msg-cookies font-poppins mb-0 p-0 color-faixa-verde weight-600">
          Este site usa cookies para garantir que você obtenha a melhor experiência. 
          <a href="/politica-de-privacidade" class="color-faixa-verde text-decoration-underline">
            Política de Privacidade
          </a>
       </p>
       <button class="btn-cookies btnInscrever weight-700 box-shadow">Ok!</button>
    </div>
  </div>

  <script>
  (() => {
    if (!localStorage.getItem('accept')) {
      document.querySelector(".box-cookies").classList.remove('d-none');
    }
    
    const acceptCookies = () => {
      document.querySelector(".box-cookies").classList.add('d-none');
      localStorage.setItem('accept', 'S');
    };
    
    const btnCookies = document.querySelector(".btn-cookies");

    btnCookies.addEventListener('click', acceptCookies);
  })();
  </script>

  {!! $plataforma->scripts_final_body !!}

</body>
</html>