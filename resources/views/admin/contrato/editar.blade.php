@extends('layouts.admin.master')
@section('content')
    
<div>

    <div class="mb-3 d-flex justify-content-end">
	    <a href="{{ route('admin.contrato.info', ['id' => $contrato->id ?? null]) }}" class="btn bg-base text-white">
	        <i class="fa fa-eye"></i>
	    </a>
	</div>

    <form method="POST" enctype="multipart/form-data" class="mb-4">
    @csrf

		<section class="mb-4-5">
			
			<a href="#boxInfoPrincipal" data-bs-toggle="collapse" aria-expanded="false" class="hover-d-none">
				<div class="section-title">
					Informações Principais
				</div>
			</a>
			<hr/>
			
			<div class="collapse show" id="boxInfoPrincipal">

		    	<div class="d-lg-flex mb-3">

			    	<div class="w-100 me-lg-3 mb-3 mb-lg-0">
			    		<label>Referência</label>
			    		<input type="text" name="referencia" class="form-control p-form" placeholder="Nome" required value="{{ $contrato->referencia }}">
			    	</div>

					<div class="w-100 me-lg-3 mb-3 mb-lg-0">
						<label>PDF</label>
						@if($contrato->pdf)
							<a href="{{ $contrato->pdf }}" class="font-nunito weight-600 fs-11 color-blue" target="_blank">
								visualizar anexo
							</a>
						@endif
						<input type="file" name="arquivo_pdf" class="file-form">
					</div>

					@if(\App\Models\Unidade::checkActive() === true)
					<div class="w-lg-60 me-lg-3 mb-3 mb-lg-0 boxUnidadeRemove">
						<label>Unidade</label>
						<select name="unidade_id" id="unidade_id" class="select-form">
							<option value="">Selecione</option>
							@foreach($unidades as $unidade)
								<option value="{{ $unidade->id }}" @if($unidade->id == $contrato->unidade_id) selected @endif>
									{{ $unidade->nome }}
								</option>
							@endforeach
						</select>
					</div>
					@endif

			    	<div class="w-lg-35">
			    		<label>Status</label>
			    		<select name="status" class="select-form">
			    			<option value="0">Ativo</option>
			    			<option value="1" @if($contrato->status == 1) selected @endif>Desativado</option>
			    		</select>
			    	</div>

			    </div>

			</div>

		</section>

		<button type="submit" name="sent" value="ok" class="btn-add mt-3">Salvar Alterações</button>	

    </form>

</div>

@endsection