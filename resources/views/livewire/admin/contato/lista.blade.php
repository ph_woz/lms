<div>

	<form>

		<input type="search" wire:model="search" class="w-100 form-control p-form" placeholder="Buscar" value="{{ $_GET['search'] ?? null }}">

		<div class="d-lg-flex my-3">
			
			<select wire:model="mes" class="w-lg-75 select-form">
				<option value="">Todos os meses</option>
				@foreach($meses as $numero => $nome)
					<option value="{{ $numero }}">
						{{ $nome }}
					</option>
				@endforeach
			</select>

			<select wire:model="tipo" class="ms-lg-3 mt-3 mt-lg-0 w-lg-50 select-form">
				<option value="">Leads e Suporte</option>
				<option value="L">Leads, apenas</option>
				<option value="S">Suporte, apenas</option>
			</select>

            <select wire:model="status" class="mx-lg-3 my-3 my-lg-0 select-form w-lg-40">
            	<option value="">Status</option>
				<option value="0">Pendente</option>
				<option value="1">Em análise</option>
				<option value="2">Atendido</option>
				<option value="3">Desativado</option>
            </select>

            <select wire:model="arquivado" class="select-form w-lg-45">
                <option value="N">Não arquivados</option>
                <option value="S">Arquivados</option>
            </select>

            <select wire:model="ordem" class="ms-lg-3 mt-3 mt-lg-0 select-form w-lg-40">
                <option value="desc">Mais novos</option>
                <option value="asc">Mais antigos</option>
            </select>
            
		</div>

		<div class="d-lg-flex mb-3">

			<div class="w-100 me-lg-3 mb-3 mb-lg-0" wire:ignore>
				<select wire:model="formulario_id" class="selectpicker" data-live-search="true" data-width="100%">
					<option value="">Formulários</option>
					@foreach($formularios as $formulario)
						<option value="{{ $formulario->id }}">
							{{ $formulario->referencia }}
						</option>
					@endforeach
				</select>
			</div>
			<div class="w-100 me-lg-3 mb-3 mb-lg-0" wire:ignore>
				<select wire:model="trilha_id" class="selectpicker" data-live-search="true" data-width="100%">
					<option value="">Trilhas</option>
					@foreach($trilhas as $trilha)
						<option value="{{ $trilha->id }}">
							{{ $trilha->referencia }}
						</option>
					@endforeach
				</select>
			</div>
			<div class="w-100" wire:ignore>
				<select wire:model="categoriasSelected" class="selectpicker" data-live-search="true" data-width="100%" multiple title="Categorias">
					@foreach($categorias as $categoria)
						<option value="{{ $categoria->id }}">
							{{ $categoria->nome }}
						</option>
					@endforeach
				</select>
			</div>

		</div>

		<div class="d-lg-flex mb-3">		

			<div class="w-100" wire:ignore>
				<select wire:model="curso_id" class="selectpicker" data-live-search="true" data-width="100%">
					<option value="">Curso</option>
					@foreach($cursos as $curso)
						<option value="{{ $curso->id }}">
							{{ $curso->nome }}
						</option>
					@endforeach
				</select>
			</div>
			
			<div class="w-100 ms-lg-3 mt-3 mt-lg-0">
				<select wire:model="turma_id" class="select-form">
					<option value="">Turma</option>
					@foreach($turmas as $turma)
						<option value="{{ $turma->id }}">
							{{ $turma->nome }}
						</option>
					@endforeach
				</select>
			</div>

            @if(\App\Models\Unidade::checkActive() === true && \Auth::user()->restringir_unidade == 'N')
            <div class="w-100 ms-lg-3 mt-3 mt-lg-0" wire:ignore>
                <select wire:model="unidade_id" class="selectpicker" data-width="100%" data-live-search="true">
                    <option value="">Unidades</option>
                    @foreach($unidades as $unidade)
                        <option value="{{ $unidade->id }}">{{ $unidade->nome }}</option>
                    @endforeach
                </select>
            </div>
            @endif

		</div>

	</form>

	<ul class="mt-5 mb-3 list-group list-group-horizontal-md text-center fs-14 p-0">
	
		<li class="w-100 weight-600 list-group-item text-danger">
			{{ $countPendentes }} em Pendente
		</li>
		<li class="w-100 weight-600 list-group-item text-primary">
			{{ $countEmAnalise }} em Análise
		</li>
		<li class="w-100 weight-600 list-group-item text-success">
			{{ $countAtendidas }} em Atentida
		</li>
		<li class="w-100 weight-600 list-group-item text-secondary">
			{{ $contatos->where('status', 3)->count() }} em Desativada
		</li>

	</ul>

    <div class="row">
        <div class="col-12">

            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col">Nome</th>
                                    <th scope="col">Email</th>
                                    <th scope="col">Assunto</th>
                                    <th scope="col">Data</th>
                                    <th scope="col">
                                        <span class="float-end">
                                            Ações
                                        </span>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($contatos as $contato)
                                    <tr>
                                        <td>{{ $contato->nome }}</td>
                                        <td>{{ $contato->email }}</td>
                                        <td>{{ $contato->assunto }}</td>
                                        <td>{{ $contato->created_at->diffForHumans() }}</td>
                                        <td>
                                            <span class="float-end">

                                                <a href="{{ route('admin.contato.info', ['id' => $contato->id]) }}" class="btn bg-base text-white">
                                                    <i class="fa fa-eye"></i>
                                                </a>

                                            </span>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            {{ $contatos->links() }}

        </div>
    </div>

</div>