<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Models\Plataforma;
use App\Models\Chamado;
use App\Models\User;

class EmailNotificaChamadoComentarioWozcode extends Mailable
{
    use Queueable, SerializesModels;

    public $assunto;
    public $comentario;

    public function __construct($assunto, $comentario)
    {
        $this->assunto   = $assunto;
        $this->comentario = $comentario;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $plataforma = Plataforma::where('id', $this->comentario->plataforma_id)->first();

        $nome = \Auth::user()->nome;

        $subject = strtok($nome, " ") . ' adicionou um Comentário ao Chamado | ' . $plataforma->nome;

        $url = 'https://admlms.wozcode.com/admin/chamado/info/'.$this->comentario->chamado_id;

        return $this->view('mail.notifica-chamado-comentario-wozcode')
                ->subject($subject)
                ->from($plataforma->email ?? 'wozcode@gmail.com', $plataforma->nome)
                ->replyTo($plataforma->email ?? 'wozcode@gmail.com', $plataforma->nome)
                ->with([
                    'url'       => $url,
                    'assunto'   => $this->assunto,
                    'descricao' => $this->comentario->descricao, 
                    'nome'      => $nome,
                ]);
    }
}
