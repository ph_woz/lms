@extends('layouts.admin.master')
@section('content')

<nav aria-label="breadcrumb" class="mb-4-5 p-3 bg-db rounded ashadow border">
	<ol class="breadcrumb p-0 m-0">
    	<li class="breadcrumb-item"><a href="{{ route('admin.pagina.lista') }}" class="color-blue-info-link">Páginas</a></li>
    	<li class="breadcrumb-item text-dark" aria-current="page">Parceiros</li>
  	</ol>
</nav>

<form method="POST">
@csrf

	<section class="mb-4-5">

		<div class="d-lg-flex align-items-center justify-content-between section-title">
			<span>
				Parceiros
			</span>
			<a href="#ModalAddParceiro" data-bs-toggle="modal" class="btn btn-primary">
				+ Adicionar
			</a>
		</div>

        <div class="table-responsive bg-white px-4 py-3 border">
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">Parceiro</th>
                        <th scope="col">Publicado</th>
                        <th scope="col">
                            <span class="float-end">
                                Ações
                            </span>
                        </th>
                    </tr>
                </thead>
                <tbody>
                	@foreach($parceiros as $parceiro)
                    <tr>
                        <td>
                            {{ $parceiro->nome }}
                        </td>
                        <td>
                            @if($parceiro->publicar == 'S')
                                Sim
                            @else
                                Não
                            @endif
                        </td>
                        <td>
                            <span class="float-end">
                               
                                <a href="#ModalEditarParceiro" data-bs-toggle="modal" class="btn btn-warning text-dark"
                                	onclick="editar()">
                                    <i class="fa fa-edit"></i>
                                </a>
                                <script>
                                	function editar()
                                	{
                                		document.getElementById('editar_id').value = '{{ $parceiro->id }}';
                                		document.getElementById('editar_nome').value = '{{ $parceiro->nome }}';
                                        document.getElementById('editar_logotipo').href = '{{ $parceiro->logotipo }}';
                                		document.getElementById('editar_site').value = '{{ $parceiro->site }}';
                                        document.getElementById('editar_publicar').value = '{{ $parceiro->publicar }}';
                                		document.getElementById('editar_ordem').value = '{{ $parceiro->ordem }}';
                                	}
                                </script>

                                <a href="#ModalDeletarParceiro" data-bs-toggle="modal" class="btn btn-danger"
                                	onclick="deletar()">
                                    <i class="fa fa-trash"></i>
                                </a>
                                <script>
                                	function deletar()
                                	{
                                		document.getElementById('delete_id').value = '{{ $parceiro->id }}';
                                		document.getElementById('delete_nome').innerHTML = '{{ $parceiro->nome }}';
                                	}
                                </script>

                            </span>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

	</section>

</form>

<form method="POST">
@csrf

    <section class="mb-4-5" id="boxParceiroSEO">

        <a href="#boxSeo" data-bs-toggle="collapse" aria-expanded="false" class="hover-d-none">
            <div class="section-title">
                Informações de SEO para resultados de buscas em Navegadores
            </div>
        </a>
        
        <div class="collapse show bg-white border p-4" id="boxSeo">
            
            <div class="d-lg-flex mb-3">

                <div class="w-100 me-lg-3 mb-3 mb-lg-0">
                    <label>Título</label>
                    <input type="text" name="seo_title" class="form-control p-form" placeholder="Title" value="{{ $conteudo->seo_title ?? null }}">
                </div>

                <div class="w-100">
                    <label>Palavras-chaves</label>
                    <input type="text" name="seo_keywords" class="form-control p-form" placeholder="Keywords" value="{{ $conteudo->seo_keywords ?? null }}">
                </div>

            </div>

            <div class="w-100 mb-3">
                <label>Descrição</label>
                <input type="text" name="seo_description" class="form-control p-form" placeholder="Description" value="{{ $conteudo->seo_description ?? null }}">
            </div>

            <button type="submit" name="sent" value="S" class="btn-add mt-2">
                Salvar
            </button>        

        </div>

    </section>

</form>

<form method="POST" enctype="multipart/form-data">
@csrf
<div class="modal fade" id="ModalAddParceiro" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title" id="exampleModalLabel">Adicionar Parceiro</h3>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
        <div class="modal-body">

            <div class="d-lg-flex mb-3">

                <div class="w-100">
                    <label>Nome</label>
        		    <input type="text" name="nome" class="form-control p-form" placeholder="Nome" required>
        		</div>
                <div class="w-lg-60 mx-lg-3 my-3 my-lg-0">
                    <label>Logotipo</label>
                    <input type="file" name="logo" class="form-control p-form">
                </div>
                <div class="w-lg-60">
                    <label>Site</label>
                    <input type="text" name="site" class="form-control p-form" placeholder="Ex: https://exemplo.com">
        		</div>
                <div class="w-lg-35 mx-lg-3 my-3 my-lg-0">
                    <label>Publicar</label>
                    <select name="publicar" class="select-form">
                        <option value="S">Sim</option>
                        <option value="N">Não</option>
                    </select>
                </div>
                <div class="w-lg-35">
                    <label>Ordem</label>
                    <input type="number" name="ordem" class="form-control p-form" placeholder="Ordem">
                </div>

            </div>

        </div>

      <div class="modal-footer">
        <button type="submit" name="add" value="ok" class="btn btn-primary py-2">Confirmar</button>
      </div>
    </div>
  </div>
</div>
</form>

<form method="POST" enctype="multipart/form-data">
@csrf
<div class="modal fade" id="ModalEditarParceiro" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title" id="exampleModalLabel">Editar Parceiro</h3>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
        <div class="modal-body">

            <input type="hidden" name="editar_id" id="editar_id">

            <div class="d-lg-flex mb-3">

                <div class="w-100">
                    <label>Nome</label>
                    <input type="text" name="nome" id="editar_nome" class="form-control p-form" placeholder="Nome" required>
                </div>
                <div class="w-lg-60 mx-lg-3 my-3 my-lg-0">
                    <label>Logotipo</label>
                    <a href="" target="_blank" class="color-blue-info-link" id="editar_logotipo">Visualizar</a>
                    <input type="file" name="logo" class="form-control p-form">
                </div>
                <div class="w-lg-60">
                    <label>Site</label>
                    <input type="text" name="site" id="editar_site" class="form-control p-form" placeholder="Ex: https://exemplo.com">
                </div>
                <div class="w-lg-35 mx-lg-3 my-3 my-lg-0">
                    <label>Publicar</label>
                    <select name="publicar" id="editar_publicar" class="select-form">
                        <option value="S">Sim</option>
                        <option value="N">Não</option>
                    </select>
                </div>
                <div class="w-lg-35">
                    <label>Ordem</label>
                    <input type="number" name="ordem" id="editar_ordem" class="form-control p-form" placeholder="Ordem">
                </div>

            </div>

        </div>

      <div class="modal-footer">
        <button type="submit" name="editar" value="ok" class="btn btn-primary py-2">Salvar Alterações</button>
      </div>
    </div>
  </div>
</div>
</form>

<form method="POST">
@csrf
<div class="modal fade" id="ModalDeletarParceiro" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title" id="exampleModalLabel">Deletar Parceiro</h3>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body text-center py-5">
		<p class="m-0 fs-15">
			<span class="weight-600" id="delete_nome"></span>
			<br>
			Você tem certeza que deseja deletar este Parceiro?
		</p>
        <input type="hidden" name="delete_id" id="delete_id">
      </div>
      <div class="modal-footer">
        <button type="submit" name="delete" value="ok" class="btn btn-danger">Confirmar</button>
      </div>
    </div>
  </div>
</div>
</form>

@endsection