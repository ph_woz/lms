<?php

namespace App\Http\Controllers\Aluno;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\TurmaAluno;
use App\Models\Curso;
use App\Models\CursoModulo;
use App\Models\AlunoAulaConcluida;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Auth;

class CursoController extends Controller
{
    public function aulas($slug, $curso_id)
    { 	
        $plataforma_id = session('plataforma_id') ?? Auth::user()->plataforma_id;
        $expiresAt = Carbon::now()->addMonth();
        $user_id = Auth::id();

        $curso = Cache::remember('Curso-'.$curso_id.'-'.$plataforma_id, $expiresAt, function() use ($curso_id) {
            return Curso::select('id','nome','certificado_id')->plataforma()->ativo()->where('id', $curso_id)->first();
        });

        $modulos = CursoModulo::select('id','nome')->plataforma()->ativo()->where('curso_id', $curso_id)->orderBy('ordem')->get();
        // $modulos = Cache::remember('CursoModulo-'.$curso_id.'-'.$plataforma_id, $expiresAt, function() use ($curso_id) {
        //     return CursoModulo::select('id','nome')->plataforma()->ativo()->where('curso_id', $curso_id)->orderBy('ordem')->get();
        // });

        $turmaAluno = TurmaAluno::plataforma()->where('aluno_id', $user_id)->where('curso_id', $curso_id)->first();

        $aulasConcluidas = [];

        if(isset($turmaAluno))
        {
            if($turmaAluno->status == 1)
            {
                session()->flash('danger', 'O seu acesso a este curso foi desativado.');
                
                return redirect(route('aluno.dashboard'));
            }

            if($turmaAluno->data_primeiro_acesso == null)
            {
                $turmaAluno->update(['data_primeiro_acesso' => date('Y-m-d H:i')]);
            }
        
            $turmaAluno->update(['data_ultimo_acesso' => date('Y-m-d H:i')]);
        

            $aulasConcluidas = AlunoAulaConcluida::select('aula_id')->plataforma()->where('aluno_id', $user_id)->where('curso_id', $curso_id)->get()->pluck('aula_id')->toArray();

        }

        return view('aluno.aulas', compact('curso','turmaAluno','modulos','aulasConcluidas') );
    }
}