<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PlataformaEndereco extends Model
{
    use HasFactory;

    protected $table = 'plataformas_enderecos';

    public $timestamps = false;

    protected $fillable = [
    	'plataforma_id',
        'cep',
        'rua',
        'estado',
        'bairro',
        'cidade',
        'numero',
        'complemento',
        'ponto_referencia',
    ];

    public function scopePlataforma($query)
    {
        return $query->where('plataforma_id', session('plataforma_id') ?? Auth()->user()->plataforma_id ?? Plataforma::dominio()->pluck('id')[0] ?? null);
    }

    public static  function boot()
    {
        parent::boot(); 
        static::creating(function ($model) {
            $model->plataforma_id  = session('plataforma_id') ?? Auth()->user()->plataforma_id ?? Plataforma::dominio()->pluck('id')[0] ?? null;
        });
    }

}
