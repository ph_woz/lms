<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class UnidadeController extends Controller
{
    public function lista()
    {
        return view('admin.unidade.lista');
    }
    
    public function info($unidade_id)
    {
        return view('admin.unidade.info', compact('unidade_id') );
    }

    public function add()
    {
    	return view('admin.unidade.add');
    }

    public function editar($unidade_id)
    {
        return view('admin.unidade.editar', compact('unidade_id') );
    }

}