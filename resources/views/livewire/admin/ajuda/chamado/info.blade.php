<div>

	<div class="d-lg-flex align-items-center">

		@if($chamado->status != 4)
			<a href="#" wire:click="setStatus(4)" class="mx-1 w-100 btn btn-success weight-600 text-shadow-1 py-2">
				Marcar como Finalizada
			</a>
		@endif

		@if($chamado->status != 3)
			<a href="#" wire:click="setStatus(3)" class="mx-1 w-100 btn btn-warning weight-600 text-dark py-2">
				Marcar para Revisão
			</a>
		@endif

		@if($chamado->status != 5)
			<a href="#" wire:click="setStatus(5)" class="mx-1 w-100 btn btn-dark weight-600 text-shadow-1 py-2">
				Marcar como Desativada
			</a>
		@endif

		@if($chamado->arquivado == 'S')
			<a href="#" wire:click="setArquivar('N')" class="mx-1 w-100 btn btn-secondary weight-600 text-shadow-1 py-2">
				Desarquivar
			</a>
		@else
			<a href="#" wire:click="setArquivar('S')" class="mx-1 w-100 btn btn-secondary weight-600 text-shadow-1 py-2">
				Arquivar
			</a>
		@endif

	</div>

	<section class="mb-4-5 mt-4">

		<div class="section-title rounded-bottom-0 d-flex justify-content-between align-items-center">
			{{ $chamado->assunto }}
			<div>
				<a href="#" data-bs-toggle="modal" data-bs-target="#ModalMetaDados" class="btn btn-info fs-14 mb-1 py-1 px-2-3">
					<i class="fa fa-align-left"></i>
				</a>
			</div>
		</div>

		<div class="bg-white border p-4 d-lg-flex justify-content-between">
			<div class="row">
				<div class="col-lg-8">
					<div class="mb-2 mb-lg-0">
						<p class="mb-0 weight-600 color-444">Descrição</p>
						<div>{!! nl2br($chamado->descricao) !!}</div>
					</div>
				</div>
				<div class="col-lg-3 offset-md-1">
					<div class="mb-2 mb-lg-0">
						<p class="mb-0 weight-600 color-444">Status</p>
						<p class="mb-0">
							@switch($chamado->status)
							    @case(0)
							        <span class="text-danger font-nunito weight-700">Pendente</span>
							    @break
							    @case(1)
							        <span class="text-secondary font-nunito weight-700">em Andamento</span>
							    @break
							    @case(2)
							        <span class="text-primary font-nunito weight-700">em Validação</span>
							    @break
							    @case(3)
							        <span class="text-warning text-shadow-1 font-nunito weight-700">para Revisão</span>
							    @break
							    @case(4)
							    	<span class="text-success font-nunito weight-700">Finalizada</span>
							    @break
							    @case(5)
							    	<span class="text-dark font-nunito weight-700">Desativada</span>
							    @break
							@endswitch
						</p>
					</div>
				</div>
			</div>
		</div>

	</section>

	<section class="mb-4-5">

		<div class="section-title py-2-5 rounded-bottom-0 d-flex justify-content-between align-items-center">
			Comentários ({{ count($comentarios) }})
		</div>

		@if(count($comentarios) <= 0)
			<div class="bg-white border p-4">
				<p class="mb-0">Nenhum comentário feito.</p>
			</div>
		@endif

		@foreach($comentarios as $comentario)
			<div class="bg-white border p-4">
				
				<p class="mb-0 color-444">
					@if($comentario->cadastrante_id)
						@if($comentario->tipo_autor == 'C')
							<a href="{{ route('admin.colaborador.info', [$comentario->cadastrante_id]) }}" class="text-dark"> 
								{{ $nome = App\Models\Util::getPrimeiroSegundoNome(\App\Models\User::plataforma()->where('id', $comentario->cadastrante_id)->pluck('nome')[0] ?? 'Não encontrado') }}
							</a>
						@else
							<a class="text-dark"> 
								{{ $nome = App\Models\Util::getPrimeiroSegundoNome(\DB::table('adm_users')->where('id', $comentario->cadastrante_id)->pluck('nome')[0] ?? 'Não encontrado') }}
							</a>
						@endif
					@else
						Não definido
					@endif
				</p>
				<p class="mb-0 color-666 fs-11 weight-600">
					{{ \Carbon\Carbon::createFromTimeStamp(strtotime($comentario->created_at))->diffForHumans() }} atrás.
				</p>
				<hr class="mt-2" />

				<p class="mb-0">{!! nl2br($comentario->descricao ?? 'Não definido') !!}</p>

			</div>
		@endforeach

	</section>

	<form wire:submit.prevent="addComentario" method="POST">

		<textarea wire:model="comentario_descricao" class="textarea-form" placeholder="Adicionar um comentário" required></textarea>
		
		<button type="submit" class="btn btn-primary mt-2 py-2">Comentar</button>

	</form>

	<div class="modal fade" id="ModalMetaDados" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	  	<div class="modal-dialog modal-dialog-centered" role="document">
	    	<div class="modal-content">
	      		<div class="modal-header">
	        		<h3 class="modal-title">Metadados</h3>
	    			<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
	      		</div>
	      		<div class="modal-body">

					<ul class="list-group list-group-flush">
	  					<li class="list-group-item">
							<span class="mb-0 weight-600 color-444">Cadastrante:</span>
							@if($cadastrante)
								<a href="/admin/colaborador/info/{{$cadastrante->id}}" class="color-blue-info-link">
									{{ $cadastrante->nome }}
								</a>
							@else
								Não definido
							@endif
	  					</li>   
	  					<li class="list-group-item">
							<span class="mb-0 weight-600 color-444">Arquivado:</span>
							@if($chamado->arquivado == 'N')
								<span class="text-success weight-700">Não</span>
							@else
								<span class="text-danger weight-700">Sim</span>
							@endif
	  					</li>
	  					<li class="list-group-item">
							<span class="mb-0 weight-600 color-444">Data de Cadastro:</span>
							@if($chamado->created_at)
								<span class="mb-0">{{ $chamado->created_at->format('d/m/Y H:i') ?? null }} ({{$chamado->created_at->diffForHumans()}}) </span>
	  						@else
	  							Não definido
	  						@endif
	  					</li>   
	  				</ul>

	      		</div>
	    	</div>
	  	</div>
	</div>

	@include('livewire.util.toast')

</div>

