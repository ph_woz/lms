<!doctype html>
<html lang="pt-br">
<head>

  <!-- Google tag (gtag.js) -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=G-MREB450HCT"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'G-MREB450HCT');
  </script>
  
   {!! $plataforma->scripts_start_head !!}

   <meta charset="utf-8">
   <meta name="viewport" content="width=device-width, initial-scale=1">

   <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KK94CHFLLe+nY2dmCWGMq91rCGa5gtU4mk92HdvYe+M/SXH301p5ILy+dN9+nJOZ" crossorigin="anonymous">
   <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/uikit@3.16.26/dist/css/uikit.min.css" />
   <link rel="stylesheet" href="{{ asset('sites-proprios/cefaep2/css/home.css') }}?v=2">
   <link rel="stylesheet" href="{{ asset('css/util.css') }}">

   <title>{{ $seo->seo_title ?? $plataforma->nome }}</title>
   <meta name="robots" content="index, follow">
   <meta name="description" content="{{ $seo->seo_description ?? null }}"/>
   <meta name="keywords" content="{{ $seo->seo_keywords ?? null }}"/>
   <link rel="canonical" href="{{ Request::url() }}" />
   <link rel="shortcut icon" href="{{ $plataforma->favicon }}">
   <meta property="og:locale" content="pt_BR" />
   <meta property="og:type" content="website" />
   <meta property="og:title" content="{{ $seo->seo_title ?? $plataforma->nome }}" />
   <meta property="og:description" content="{{ $seo->seo_description ?? null }}" />
   <meta property="og:url" content="{{ Request::url() }}" />
   <meta property="og:site_name" content="{{ $plataforma->nome }}" />
   <meta property="og:image" content="{{ $plataforma->logotipo }}" />
   <meta name='author' content='WOZCODE | Pedro Henrique' />

   {!! $plataforma->scripts_final_head !!}

</head>
<body id="inicio">

	@include('sites-proprios/cefaep2/componentes/header')

	<main>

		<section id="carouselExampleCaptions" class="carousel slide">
		  <div class="carousel-inner">
			@foreach($banners as $banner)
			    <div class="carousel-item @if($loop->first) active @endif">
			      <img src="{{ $banner->link }}" class="d-block w-100" alt="Banner">
			    </div>
		    @endforeach
		  </div>
		  <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="prev">
		    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
		    <span class="visually-hidden">Previous</span>
		  </button>
		  <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="next">
		    <span class="carousel-control-next-icon" aria-hidden="true"></span>
		    <span class="visually-hidden">Next</span>
		  </button>
		</section>

{{--       <section class="bg-f5 py-5" id="cursos">
         <div class="container-fluid px-xl-7 bg-lines">

          	<h3 class="mb-0 title m-quem-somos__title text-uppercase text-center">
            	Cursos
          	</h3>

          	<div class="mb-4-5 text-center">
	            <p class="title-lead title-lead--detail">
	              Conheça todos os nossos cursos para você
	            </p>
	        </div>

            <div class="mb-4-5 d-flex justify-content-center">

            <!--
              <button type="button" data-nivel="todos" class="btn-categoria bg-categoria-active px-3-5 py-2 font-raleway weight-600" style="border:3px solid #333; border-radius: 50px;">
                Todos
              </button>
			-->

              <button type="button" data-nivel="curso-nivel-curso-tecnico" class="btn-categoria bg-categoria-active px-3-5 py-2 font-raleway weight-600" style="border:3px solid #333; border-radius: 50px;">
                Curso Técnico
              </button>

              <button type="button" data-nivel="curso-nivel-pos-graduacao" class="mx-3 btn-categoria px-3-5 py-2 font-raleway weight-600" style="border:3px solid #333; border-radius: 50px;">
                Pós-Graduação
              </button>

              <button type="button" data-nivel="curso-nivel-graduacao" class="btn-categoria px-3-5 py-2 font-raleway weight-600" style="border:3px solid #333; border-radius: 50px;">
                Graduação
              </button>

            </div>

            <div class="row">

				@foreach($cursos as $curso)
				<div class="col-lg-3 mb-4 curso-slide curso-nivel-{{\Str::slug($curso->nivel)}}">
                    <a href="/curso/{{$curso->slug}}" class="mb-4 p-0">
                        <div class="card-servico box-shadow rounded">
                           <div class="card-header-servico px-3 rounded-top bg-info-c flex-center h-lg-80 py-4 py-lg-0">
                              <h1 class="mb-0 font-poppins weight-700 fs-16 text-light text-shadow-2 text-center">
                                 {{ $curso->nome }}
                              </h1>
                           </div>
                           <div class="card-body-servico">
                              <img src="{{ $curso->foto_miniatura ?? asset('images/no-image.jpeg') }}" alt="Curso" class="img-fluid object-fit-cover h-150 w-100 rounded-bottom">
                           </div>
                        </div>
                    </a>
				</div>
				@endforeach

            </div>

         </div>
      </section> --}}

		<section class="container-fluid m-quem-somos overflow-hidden pt-5">
      		<div class="container-fluid px-xl-7">
        		<div class="row g-0">

        			<div class="col-lg-7 pb-5">

			            <div class="m-quem-somos__header">
			              	<span class="title-lead title-lead--detail">
			              		Ensino à Distância
			              		<img src="{{ asset('images/utils/dots-yellow.webp') }}" alt="Yellow Dots">
			              	</span>
			              	<h1 class="title m-quem-somos__title text-uppercase">
			                	Com o <span class="color-orange">Certificado de conclusão</span> você pode:
			            	</h1>
			            </div>

						<div class="m-quem-somos__content">
							<ol class="ps-2">
								<li class="font-inter">Fazer Faculdade;</li>
								<li class="font-inter">Fazer Cursos Técnicos</li>
								<li class="font-inter">Ter melhores oportunidades no mercado de trabalho</li>
								<li class="font-inter">Realizar o Enem</li>
								<li class="font-inter">Participar de concursos públicos.</li>
							</ol>
						</div>


                  <a href="tel:+55{{preg_replace('/\D/', '', $plataforma->telefone)}}" class="btn btn-quero-me-matricular hover-d-none px-4 py-3-5">
                     <i class="bi bi-telephone"></i>                           
                     <span class="ms-2">Ligue agora </span> <span class="ms-1 d-xs-none">gratuitamente</span>
                     <span class="ms-2">{{ $plataforma->telefone ?? '(00) 0000-0000' }}</span>
                  </a>

			        </div>

			        <div class="col-lg-5 align-self-end d-xs-none">

			        	<img src="{{ asset('images/utils/woman-and-man.webp') }}" alt="Mulher e Homem">

			        </div>

			    </div>
			</div>
    	</section>

    	<section style="background-size: cover; background-repeat: no-repeat; background-image: url('{{ asset('images/utils/bg-green.webp') }}');">
    		<div class="container-fluid px-xl-7 overflow-hidden">
    			<div class="row align-items-center pt-5">

    				<div class="col-lg-6 pb-5">

						<div class="c-chamada__header">
				          	<span class="text-light title-lead title-lead--detail title-lead--detail-white">A CEFAEP ajuda você a</span>
				          	<img src="{{ asset('images/utils/dots-white.webp') }}" alt="Dots White" class="img-fluid">
				          	<h3 class="title title--big c-chamada__title color-white text-uppercase">
				            	Concluir essa etapa tão importante da sua vida.<br>
				            	Comece hoje!
				          	</h3>
				        </div>

						<div class="mt-4 position-relative">
				        	<a href="https://cefaep.com.br/f/formulario-de-matricula/23/t/54" target="_blank" class="mb-3 mb-lg-0 btn btn-white btn-white--big">Quero me matricular</a>
				        	<a href="/faq" id="btn-outline-white-a">Ainda tenho dúvidas</a>
				        </div>

    				</div>

    				<div class="col-lg-6 align-self-end d-xs-none">

    					<img style="width: 100%; height: 400px; object-fit: cover;" src="{{ asset('images/utils/two-woman.webp') }}" alt="Two womans">

    				</div>

    			</div>
    		</div>
    	</section>

    	<section class="py-5 overflow-hidden">
    		<div class="container-fluid px-xl-7 pt-5">

				<div class="c-slider-alunos-formados__header">
            	<span class="title-lead title-lead--detail">Conheça nossos </span>
            	<img src="{{ asset('images/utils/dots-yellow.webp') }}" alt="Yellow Dots">
            	<h2 class="title title--big m-como-funciona__title text-uppercase">Alunos Formados</h2>
            </div>

               <div uk-slider="autoplay: true; autoplay-interval: 3000;">

                  <div class="uk-position-relative z-1 uk-visible-toggle uk-light" tabindex="-1">
                       
                     <ul class="uk-slider-items uk-child-width-1-4@s">
                        @foreach($fotosAlunosFormados as $foto)
                        <li class="uk-width-1-2 uk-width-1-4@m">
                           <img src="{{ $foto }}" alt="Aluno Formado CEFAEP" class="px-2 px-lg-5 h-250 w-100 object-fit-cover" style="object-position: top">
                        </li>
                        @endforeach
                     </ul>

                     <a class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous uk-slider-item="previous"></a>
                     <a class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-next uk-slider-item="next"></a>

                  </div>

               </div>

				<div class="flex-center pt-4-5">
          		<a href="https://cefaep.com.br/f/formulario-de-matricula/23/t/54" target="_blank" class="btn btn-quero-me-matricular hover-d-none px-4-5 py-3">Quero me matricular também</a>
          	</div>

    		</div>
    	</section>

    	<section class="py-5">
    		<div class="container-fluid px-xl-7 overflow-hidden">
    			<div class="row align-items-center">

    				<div class="col-lg-6">

	              	<span class="title-lead title-lead--detail">
	              		AULAS 100% ONLINE
	              		<img src="{{ asset('images/utils/dots-yellow.webp') }}" alt="Yellow Dots">
	              	</span>

	              	<h1 class="title m-quem-somos__title text-uppercase">
	                	Como funciona o <span>SUPLETIVO?</span>
	            	</h1>

              		<p class="mb-0 font-inter">Através da Certificação por Competência você pode concluir todos os anos em que você perdeu na escola, realizando apenas 1 prova por matéria. Reconhecido pelo MEC e amparado pela *Lei-LDB.93.94/96.</p>

    				</div>

    				<div class="col-lg-6 d-xs-none">

    					<img src="{{ asset('images/utils/ilustration.webp') }}" alt="Ilustração">

    				</div>

    			</div>
    		</div>
    	</section>

    	<section class="py-5">
    		<div class="container-fluid px-xl-7 overflow-hidden">

           	<span class="title-lead title-lead--detail">
           		CONHEÇA AGORA OS
           		<img src="{{ asset('images/utils/dots-yellow.webp') }}" alt="Yellow Dots">
           	</span>

           	<h1 class="title m-quem-somos__title text-uppercase">
             	<span>MOTIVOS</span> PARA TERMINAR OS ESTUDOS
         	</h1>

				<div class="row mt-4-5">

               <div class="col-12 col-md-6 col-xl-5 offset-xl-1 c-motivos-para-terminar-col c-motivos-para-terminar-col--pros">
              		
              		<h3 class="weight-700 text-success">COM ENSINO MÉDIO COMPLETO</h3>
	              	<ul>
	                  <li class="mb-3 d-flex align-items-center">
		                <div>
		                  	<div class="icon-motivo motivo-success flex-center">
		                  		<i class="flex-center fs-25 bi bi-hand-thumbs-up"></i>
		                  	</div>
	                  	</div>
	                  	<span class="ms-2 font-inter weight-600 fs-16">Oportunidades no Mercado de Trabalho</span>
	                  </li>
	                  <li class="mb-3 d-flex align-items-center">
		                <div>
		                  	<div class="icon-motivo motivo-success flex-center">
		                  		<i class="flex-center fs-25 bi bi-hand-thumbs-up"></i>
		                  	</div>
	                  	</div>
	                  	<span class="ms-2 font-inter weight-600 fs-16">Iniciar uma Faculdade</span>
	                  </li>
	                  <li class="mb-3 d-flex align-items-center">
		                <div>
		                  	<div class="icon-motivo motivo-success flex-center">
		                  		<i class="flex-center fs-25 bi bi-hand-thumbs-up"></i>
		                  	</div>
	                  	</div>
	                  	<span class="ms-2 font-inter weight-600 fs-16">Iniciar um Curso Técnico</span>
	                  </li>
	                  <li class="mb-3 d-flex align-items-center">
		                <div>
		                  	<div class="icon-motivo motivo-success flex-center">
		                  		<i class="flex-center fs-25 bi bi-hand-thumbs-up"></i>
		                  	</div>
	                  	</div>
	                  	<span class="ms-2 font-inter weight-600 fs-16">Participar de concursos públicos</span>
	                  </li>
	                  <li class="mb-3 d-flex align-items-center">
		                <div>
		                  	<div class="icon-motivo motivo-success flex-center">
		                  		<i class="flex-center fs-25 bi bi-hand-thumbs-up"></i>
		                  	</div>
	                  	</div>
	                  	<span class="ms-2 font-inter weight-600 fs-16">Realizar o ENEM</span>
	                  </li>
	                  <li class="mb-3 d-flex align-items-center">
		                <div>
		                  	<div class="icon-motivo motivo-success flex-center">
		                  		<i class="flex-center fs-25 bi bi-hand-thumbs-up"></i>
		                  	</div>
	                  	</div>
	                  	<span class="ms-2 font-inter weight-600 fs-16">Crescer dentro de Empresas e melhora na qualidade de vida</span>
	                  </li>
	                  <li class="mb-3 d-flex align-items-center">
		                <div>
		                  	<div class="icon-motivo motivo-success flex-center">
		                  		<i class="flex-center fs-25 bi bi-hand-thumbs-up"></i>
		                  	</div>
	                  	</div>
	                  	<span class="ms-2 font-inter weight-600 fs-16">Oportunidades de Estudar no Exterior</span>
	                  </li>
	                  <li class="mb-3 d-flex align-items-center">
		                <div>
		                  	<div class="icon-motivo motivo-success flex-center">
		                  		<i class="flex-center fs-25 bi bi-hand-thumbs-up"></i>
		                  	</div>
	                  	</div>
	                  	<span class="ms-2 font-inter weight-600 fs-16">Conhecimento para toda a vida!</span>
	                  </li>
	                  <li class="mb-3 d-flex align-items-center">
		                <div>
		                  	<div class="icon-motivo motivo-success flex-center">
		                  		<i class="flex-center fs-25 bi bi-hand-thumbs-up"></i>
		                  	</div>
	                  	</div>
	                  	<span class="ms-2 font-inter weight-600 fs-16">E muito mais</span>
	                  </li>
	               </ul>
	            	
            	</div>
          
               <div class="mt-4 mt-lg-0 c-motivos-para-terminar-col col-12 col-md-6 col-xl-5 offset-xl-1">

              		<h3 class="weight-700 text-danger">SEM ENSINO MÉDIO COMPLETO</h3>
	              	<ul>
	                  <li class="mb-3 d-flex align-items-center">
		                  <div>
		                  	<div class="icon-motivo motivo-danger flex-center">
		                  		<i class="flex-center fs-25 bi bi-hand-thumbs-down"></i>
		                  	</div>
		                  </div>
	                  	<span class="ms-2 font-inter weight-600 fs-16">Sem Oportunidades no Mercado de Trabalho</span>
	                  </li>
	                  <li class="mb-3 d-flex align-items-center">
		                  <div>
		                  	<div class="icon-motivo motivo-danger flex-center">
		                  		<i class="flex-center fs-25 bi bi-hand-thumbs-down"></i>
		                  	</div>
		                  </div>
	                  	<span class="ms-2 font-inter weight-600 fs-16">Não consegue iniciar uma Faculdade</span>
	                  </li>
	                  <li class="mb-3 d-flex align-items-center">
		                  <div>
		                  	<div class="icon-motivo motivo-danger flex-center">
		                  		<i class="flex-center fs-25 bi bi-hand-thumbs-down"></i>
		                  	</div>
		                  </div>
	                  	<span class="ms-2 font-inter weight-600 fs-16">Não consegue iniciar um Curso Técnico</span>
	                  </li>
	                  <li class="mb-3 d-flex align-items-center">
		                  <div>
		                  	<div class="icon-motivo motivo-danger flex-center">
		                  		<i class="flex-center fs-25 bi bi-hand-thumbs-down"></i>
		                  	</div>
		                  </div>
	                  	<span class="ms-2 font-inter weight-600 fs-16">Não consegue participar de concursos públicos</span>
	                  </li>
	                  <li class="mb-3 d-flex align-items-center">
		                  <div>
		                  	<div class="icon-motivo motivo-danger flex-center">
		                  		<i class="flex-center fs-25 bi bi-hand-thumbs-down"></i>
		                  	</div>
		                  </div>
	                  	<span class="ms-2 font-inter weight-600 fs-16">Não pode fazer o ENEM</span>
	                  </li>
	                  <li class="mb-3 d-flex align-items-center">
		                  <div>
		                  	<div class="icon-motivo motivo-danger flex-center">
		                  		<i class="flex-center fs-25 bi bi-hand-thumbs-down"></i>
		                  	</div>
		                  </div>
	                  	<span class="ms-2 font-inter weight-600 fs-16">Sem projeção de crescimento dentro de Empresas</span>
	                  </li>
	                  <li class="mb-3 d-flex align-items-center">
		                  <div>
		                  	<div class="icon-motivo motivo-danger flex-center">
		                  		<i class="flex-center fs-25 bi bi-hand-thumbs-down"></i>
		                  	</div>
		                  </div>
	                  	<span class="ms-2 font-inter weight-600 fs-16">Sem Oportunidades de Estudar no Exterior</span>
	                  </li>
	                  <li class="mb-3 d-flex align-items-center">
		                  <div>
		                  	<div class="icon-motivo motivo-danger flex-center">
		                  		<i class="flex-center fs-25 bi bi-hand-thumbs-down"></i>
		                  	</div>
		                  </div>
	                  	<span class="ms-2 font-inter weight-600 fs-16">Falta de conhecimento</span>
	                  </li>
	                  <li class="mb-3 d-flex align-items-center">
		                  <div>
		                  	<div class="icon-motivo motivo-danger flex-center">
		                  		<i class="flex-center fs-25 bi bi-hand-thumbs-down"></i>
		                  	</div>
		                  </div>
	                  	<span class="ms-2 font-inter weight-600 fs-16">E muito outros fatores desvantajosos</span>
	                  </li>
	               </ul>

            	</div>

        		</div>

    		</div>
    	</section>

    	<section class="py-5">
    		<div class="container-fluid mb-5">

        		<div class="col-lg-8 offset-lg-2 text-center">
					<h2 class="title title--small c-motivos-para-terminar__title">Com a CEFAEP você se prepara para realizar a PROVA de qualquer lugar do Brasil.</h2>
				</div>

    		</div>
    	</section>

		<section class="w-100 bg-size-cover background-position-center py-5" style="background-image: url('{{ asset('sites-proprios/cefaep2/images/rodape.webp') }}')">
			<div class="container-fluid px-xl-7 h-100 py-5">

				<div class="flex-center">
					
					<div>

			         <span class="title-lead title-lead--detail text-light">
			         	Acabe com todas as suas dúvidas
			         	<img src="{{ asset('images/utils/dots-white.webp') }}" alt="White Dots" class="ms-2 img-fluid">
			         </span>
			         <h3 class="title c-chamada-faq__title text-uppercase text-light title--big">
			         	Ainda com dúvidas?
			         </h3>

						<div class="mt-3 c-form-banner">
							<h2 class="c-form-banner__header bg-primary text-shadow-2 text-light">
								Fale com um Especialista Educacional
							</h2>
							<div class="card-body p-3 flex-center">

								<div>
			                     	<a href="https://api.whatsapp.com/send?phone=55{{preg_replace('/\D/', '', $plataforma->whatsapp)}}&text=Olá, vim através do Site {{ $plataforma->nome }}" class="flex-center btn-custom px-4-5 py-3-5 fs-17 font-inter text-shadow-1 hover-d-none hover-white">
				                        <span class="me-3">Conversar no WhatsApp</span>
											<i class="bi bi-whatsapp"></i>
			                    	</a>
			                    	<div class="mt-2 flex-center">
										<i class="bi bi-lock"></i>
				                    	<p class="ms-1 color-ok mb-0 text-center weight-500 fs-13">
				                    		Seus dados estão seguros
				                    	</p>
									</div>
			                    </div>

							</div>
						</div>

			      </div>

		      </div>

			</div>
		</section>

	</main>

	@include('sites-proprios/cefaep2/componentes/footer')

<div class="toast-container position-fixed top-0 end-0 p-3 mt-lg-5">
  <div id="liveToast" class="toast notification-success" role="alert" aria-live="assertive" aria-atomic="true">
    <div class="toast-header">
      <strong class="me-auto" id="toast-header"> </strong>
    </div>
    <div class="toast-body font-inter weight-500 text-light" id="toast-body">

    </div>
  </div>
</div>

<script>

	var arrNotifications = [
	 	{
	 		header: "Pedro Henrique de Duque de Caxias - RJ", 
	 		body: "Matriculou-se no Supletivo EJA há 2 horas."
	 	},
	 	{
	 		header: "Lucas Silva de São Paulo - SP", 
	 		body: "Matriculou-se no Supletivo EJA há 30 minutos."
	 	},
	 	{
	 		header: "Lorena Sofia de Rio de Janeiro - RJ", 
	 		body: "Matriculou-se no Supletivo EJA há 15 minutos."
	 	},

	 	{
	 		header: "Lucas Santos de Rio Preto - SP", 
	 		body: "Matriculou-se no Supletivo EJA há 15 minutos."
	 	},
	 	{
	 		header: "Lucas Silva de São Paulo - SP", 
	 		body: "Matriculou-se no Supletivo EJA há alguns minutos."
	 	},
	 	{
	 		header: "Rafael Oliveira de Osasco - SP", 
	 		body: "Matriculou-se no Supletivo EJA há alguns minutos."
	 	},

	];


	let notifications = setInterval(function ()
	{
	  	notification = arrNotifications.shift();

    	if (notification === undefined)
    	{
     		clearInterval(notifications);
     		return;
  		}

		document.getElementById('toast-header').innerHTML = notification.header;
		document.getElementById('toast-body').innerHTML = notification.body;

		bootstrap.Toast.getOrCreateInstance(document.getElementById('liveToast')).show();

	},10000);

</script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/uikit@3.16.26/dist/js/uikit.min.js"></script>
<script>

    let countDesativados = 0;
    let countAtivos = 0;

    $(document).ready(function() {
    	$('.bg-categoria-active').click();
    });

    $('.btn-categoria').click(function() {

        $('.btn-categoria').removeClass('bg-categoria-active');
        $(this).addClass('bg-categoria-active');

        let nivel = $(this).attr('data-nivel');

        if(nivel != 'todos')
        {
           $('.curso-slide').hide();
           $('.'+nivel).show();
        
        } else {

           $('.curso-slide').show();
        }

        countAtivos = 0;
        countDesativados = parseInt($('.curso-slide:hidden').length);
        countSobra = countAtivos - countDesativados;

        if(countSobra <= 4)
        {
          UIkit.slider('#sliders').stopAutoplay();
        } else {
          UIkit.slider('#sliders').startAutoplay();
        }

    });

</script>

</body>
</html>