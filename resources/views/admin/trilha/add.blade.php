@extends('layouts.admin.master')

@section('css')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.14.0-beta2/dist/css/bootstrap-select.min.css">
@endsection

@section('content')

<div>

    <form method="POST" enctype="multipart/form-data" class="mb-4">
    @csrf

		<section class="mb-4-5">
			
			<a href="#boxInfoPrincipal" data-bs-toggle="collapse" aria-expanded="false" class="hover-d-none">
				<div class="section-title">
					Informações Principais
				</div>
			</a>
			<hr/>
			
			<div class="collapse show" id="boxInfoPrincipal">

		    	<div class="d-lg-flex mb-3">

			    	<div class="w-100">
			    		<label>Nome Comercial</label>
			    		<input type="text" name="nome" class="form-control p-form" placeholder="Nome que será exibido para o público e alunos" required value="{{ old('nome') }}">
			    	</div>
			    	<div class="w-100 mx-lg-3 my-3 my-lg-0">
			    		<label>Nome de Referência</label>
			    		<input type="text" name="referencia" class="form-control p-form" placeholder="Nome que será exibido somente para Colaboradores" required value="{{ old('referencia') }}">
			    	</div>
			    	<div class="w-lg-35">
			    		<label>Status</label>
			    		<select name="status" class="select-form">
			    			<option value="0">Ativo</option>
			    			<option value="1" @if(old('status') == 1) selected @endif>Desativado</option>
			    		</select>
			    	</div>

			    </div>

			    <div class="d-lg-flex mb-3">

					<div class="w-lg-80">
						<label>Certificado de Conclusão</label>
						<select name="certificado_id" class="selectpicker" title="Selecione" data-width="100%" data-live-search="true">
							<option value="">Selecione</option>
							@foreach($certificados as $certificado)
								<option value="{{ $certificado->id }}"  @if(old('certificado_id') == $certificado->id) selected @endif>
									{{ $certificado->referencia }}
								</option>
							@endforeach
						</select>
					</div>
					<div class="w-lg-80 ms-lg-3 mt-3 mt-lg-0">
						<label>Atestado de Matrícula</label>
						<select name="atestado_id" class="selectpicker" title="Selecione" data-width="100%" data-live-search="true">
							<option value="">Selecione</option>
							@foreach($certificados as $certificado)
								<option value="{{ $certificado->id }}"  @if(old('atestado_id') == $certificado->id) selected @endif>
									{{ $certificado->referencia }}
								</option>
							@endforeach
						</select>
					</div>
					<div class="w-100 mx-lg-3 my-3 my-lg-0">
						<label>Formulário de Inscrição de Matrícula</label>
						<select name="formulario_id" class="selectpicker" title="Selecione" data-width="100%" data-live-search="true">
							<option value="">Selecione</option>
							@foreach($formularios as $formulario)
								<option value="{{ $formulario->id }}" @if(old('formulario_id') == $formulario->id) selected @endif>
									{{ $formulario->referencia }}
								</option>
							@endforeach
						</select>
					</div>
					<div class="w-100">
						<label>Tipo de Inscrição de Matrícula</label>
						<select name="tipo_inscricao" class="select-form">
							<option value="requer-aprovacao">Requer aprovação</option>
							<option value="auto-inscricao" @if(old('tipo_inscricao') == 'auto-inscricao') selected @endif>Auto Inscrição</option>
						</select>
					</div>

			    </div>
			    
			    <div class="mb-3">
			    	<label>Turmas</label>
					<select name="turmas[]" class="selectpicker" multiple data-width="100%" data-live-search="true" title="Selecione">
						@foreach($turmas as $turma)
							<option value="{{ $turma->id }}">{{ $turma->nome }}</option>
						@endforeach
					</select>
			    </div>

				@if(\App\Models\Unidade::checkActive() === true)
				<div class="mb-3 boxUnidadeRemove">
					<label>Unidade</label>
					<select name="unidade_id" id="unidade_id" class="selectpicker" data-width="100%">
						<option value="">Selecione</option>
						@foreach($unidades as $unidade)
							<option value="{{ $unidade->id }}" @if(old('unidade_id') == $unidade->id) selected @endif>
								{{ $unidade->nome }}
							</option>
						@endforeach
					</select>
				</div>
				@endif

			    <div class="d-lg-flex mb-3">

					<div class="w-100 me-lg-3 mb-3 mb-lg-0">
						<label>Foto de Capa</label>
						<input type="file" name="foto" class="file-form">
					</div>
					<div class="w-100 me-lg-3 mb-3 mb-lg-0">
						<label>Foto de Miniatura</label>
						<input type="file" name="foto_min" class="file-form">
					</div>
					<div class="w-100 me-lg-3 mb-3 mb-lg-0">
						<label>Matriz Curricular</label>
						<input type="file" name="matriz" class="file-form">
					</div>
					<div class="w-lg-40">
						<label>Publicar</label>
						<select name="publicar" onchange="setPublicar(this.value)" class="select-form">
							<option value="N">Não</option>
							<option value="S" @if(old('publicar') == 'S') selected @endif>Sim</option>
						</select>
					</div>

			    </div>

			    <div id="boxCursoApresentacao" style="display: none;">

			    	<div class="d-lg-flex mb-3">
						<div class="w-100 me-lg-3 mt-3 mt-lg-0">
							<label>Slogan</label>
							<input type="text" name="slogan" class="form-control p-form" placeholder="Curta descrição" maxlength="275" value="{{ old('slogan') }}">
						</div>
						<div class="w-lg-60 me-lg-3 mt-3 mt-lg-0">
							<label>Vídeo de Apresentação</label>
							<input type="text" name="video" class="form-control p-form" placeholder="URL de vídeo" value="{{ old('video') }}">
						</div>
						<div class="w-lg-55 me-lg-3 mt-3 mt-lg-0">
							<label>Coordenador</label>
							<select name="coordenador_id" class="selectpicker" title="Selecione" data-width="100%" data-live-search="true">
								<option value="">Selecione</option>
								@foreach($colaboradores as $colaborador)
									<option value="{{ $colaborador->id }}" @if(old('coordenador_id') == $colaborador->id) selected @endif>
										{{ $colaborador->nome }}
									</option>
								@endforeach
							</select>
						</div>
						<div class="w-100">
							<label>Exibir Turmas como Conteúdo do Curso</label>
							<select name="exibir_turmas_como_cc" class="select-form">
								<option value="N">Não</option>
								<option value="S" @if(old('exibir_turmas_como_cc') == 'S') selected @endif>Sim</option>
							</select>
						</div>
					</div>

				    <div class="d-lg-flex mb-3">

						<div class="w-100 me-lg-3 mt-3 mt-lg-0">
							<label>Carga horária</label>
							<input type="text" name="carga_horaria" class="form-control p-form" placeholder="N° de horas" value="{{ old('carga_horaria') }}">
						</div>
						<div class="w-100 me-lg-3 mt-3 mt-lg-0">
							<label>Tipo de Formação</label>
							<input type="text" name="tipo_formacao" class="form-control p-form" value="{{ old('tipo_formacao') }}">
						</div>
						<div class="w-100">
							<label>Duração</label>
							<input type="text" name="duracao" class="form-control p-form" value="{{ old('duracao') }}">
						</div>
						<div class="w-100 mx-lg-3 my-3 my-lg-0">
							<label>Nível</label>
							<input type="text" name="nivel" class="form-control p-form" value="{{ old('nivel') }}">
						</div>
						<div class="w-100 me-lg-3 mt-3 mt-lg-0">
							<label>Modalidade</label>
							<input type="text" name="modalidade" class="form-control p-form" placeholder="Ex: EAD" value="{{ old('modalidade') }}">
						</div>
						<div class="w-100">
							<label>Área de Negócio</label>
							<input type="text" name="area_negocio" class="form-control p-form" placeholder="Ex: Saúde" value="{{ old('area_negocio') }}">
						</div>

					</div>

					<div class="mb-3">
						<label>Descrição</label>
						<textarea name="descricao" class="textarea-form" placeholder="Descrição">{{ old('descricao') }}</textarea>
					</div>

				</div>

			</div>

		</section>

		<section class="mb-4-5" wire:ignore>
			
			<a href="#boxCategorias" data-bs-toggle="collapse" aria-expanded="false" class="hover-d-none">
				<div class="section-title">
					Categorias 
				</div>
			</a>
			<hr/>

			<div class="collapse" id="boxCategorias">
				<select name="categorias[]" class="selectpicker" multiple data-width="100%" data-live-search="true" title="Selecione">
					@foreach($categorias as $categoria)
						<option value="{{ $categoria->id }}">{{ $categoria->nome }}</option>
					@endforeach
				</select>
			</div>

		</section>

		<section class="mb-4-5" id="boxSecValores">

			<a href="#boxValores" data-bs-toggle="collapse" aria-expanded="false" class="hover-d-none">
				<div class="section-title">
					Valores e Forma de Pagamento
				</div>
			</a>
			<hr/>

			<div class="collapse" id="boxValores">

				<div class="d-lg-flex mb-3">

					<div class="w-lg-75">
						<label>Tipo de Valor</label>
						<select name="tipo_valor" onchange="setTipoValor(this.value)" class="select-form">
							<option value="F">Fechado</option>
							<option value="M" @if(old('tipo_valor') == 'M') selected @endif>Mensalidade</option>
						</select>
					</div>

			    	<div class="boxValorFechado w-100 ms-lg-3 mt-3 mt-lg-0">
			    		<label>Valor a vista</label>
			    		<input type="text" name="valor_a_vista" class="valor form-control p-form" placeholder="R$" value="{{ old('valor_a_vista') }}">
			    	</div>
			    	<div class="boxValorFechado w-100 ms-lg-3 mt-3 mt-lg-0">
			    		<label>Valor a vista promocional</label>
			    		<input type="text" name="valor_promocional" class="valor form-control p-form" placeholder="R$" value="{{ old('valor_promocional') }}">
			    	</div>
			    	<div class="boxValorFechado w-100 ms-lg-3 mt-3 mt-lg-0">
			    		<label>Valor parcelado</label>
			    		<input type="text" name="valor_parcelado" class="form-control p-form" placeholder="Ex: 3x de R$ 97,99" value="{{ old('valor_parcelado') }}">
			    	</div>
			    	<div class="boxValorMensalidade w-100 ms-lg-3 mt-3 mt-lg-0" style="display: none;">
			    		<label>Valor da Mensalidade</label>
			    		<div class="d-flex w-100">
				    		<input type="text" name="valor_mensalidade" class="valor form-control p-form rounded-right-0" placeholder="R$" value="{{ old('valor_mensalidade') }}">
							<select name="tipo_mensalidade" onchange="setBoxProximosSemestre(this.value)" class="w-lg-80 select-form rounded-left-0">
								<option value="F">Fixo (até a conclusão do curso)</option>
								<option value="R" @if(old('tipo_mensalidade') == 'R') selected @endif>Primeiro Semestre</option>
							</select>
						</div>
			    	</div>
			    	<div class="w-100 ms-lg-3 mt-3 mt-lg-0">
			    		<label>% de desconto com Agilpass</label>
			    		<input type="text" name="desconto_agilpass" class="number form-control p-form" placeholder="Desconto em cada pagamento com Agilpass" value="{{ old('desconto_agilpass') }}">
			    	</div>

				</div>

				<hr/>

				<div class="w-100 boxValorMensalidade mb-3" id="boxProximosSemestres" style="display: none;">
					<div class="p-3 rounded" style="border:1.5px solid #dbdbdb;">
						<div class="mb-1 w-100">
							<p class="mb-0">Próximos Semestres</p>
							<hr class="mt-2">
						</div>
						<div class="boxLineParcela">
							<div class="mb-2 d-flex align-items-center">
								<input type="text" name="valores_por_semestres[]" style="width: 120px;" readonly class="form-control p-form text-center px-0 me-2" value="2° Semestre">
								<input type="text" name="semestres[]" class="valor form-control p-form" placeholder="R$ (valor da mensalidade deste semestre)">
								<a href="#" class="btnRemoveSemestre btn btn-danger py-2 ms-2">
									<i class="fa fa-trash"></i>
								</a>
							</div>
						</div>
						<button type="button" class="btn-append btn-append-semestre btn btn-primary py-1-5 fs-13">Adicionar +</button>
					</div>
				</div>

				<div class="boxValorFechado row mb-3">

					<div class="col-lg-4 mb-4">
						<div class="p-3 rounded" style="border:1.5px solid #dbdbdb;">
							<div class="mb-1 w-100">
								<p class="mb-0">Cartão de Crédito</p>
								<hr class="mt-2">
							</div>
							<div class="boxLineParcela">
								<div class="mb-2 d-flex align-items-center">
									<input type="text" name="cartao-de-credito[parcelas][]" class="number form-control p-form" placeholder="Parcela">
									<input type="text" style="width: 45px;" disabled readonly class="form-control p-form text-center px-0 mx-2" placeholder="de">
									<input type="text" name="cartao-de-credito[valores][]" class="valor form-control p-form" placeholder="R$">
									<a href="#" class="btnRemoveParcela btn btn-danger py-2 ms-2">
										<i class="fa fa-trash"></i>
									</a>
								</div>
							</div>
							<button type="button" class="btn-append btn-append-parcela-cc btn btn-primary py-1-5 fs-13">Adicionar +</button>
						</div>
					</div>
					<div class="col-lg-4 mb-4">
						<div class="p-3 rounded" style="border:1.5px solid #dbdbdb;">
							<div class="mb-1 w-100">
								<p class="mb-0">Cartão de Débito</p>
								<hr class="mt-2">
							</div>
							<div class="boxLineParcela">
								<div class="mb-2 d-flex align-items-center">
									<input type="text" name="cartao-de-debito[parcelas][]" class="number form-control p-form" placeholder="Parcela">
									<input type="text" style="width: 45px;" disabled readonly class="form-control p-form text-center px-0 mx-2" placeholder="de">
									<input type="text" name="cartao-de-debito[valores][]" class="valor form-control p-form" placeholder="R$">
									<a href="#" class="btnRemoveParcela btn btn-danger py-2 ms-2">
										<i class="fa fa-trash"></i>
									</a>
								</div>
							</div>
							<button type="button" class="btn-append btn-append-parcela-cd btn btn-primary py-1-5 fs-13">Adicionar +</button>
						</div>
					</div>
					<div class="col-lg-4 mb-4">
						<div class="p-3 rounded" style="border:1.5px solid #dbdbdb;">
							<div class="mb-1 w-100">
								<p class="mb-0">Boleto</p>
								<hr class="mt-2">
							</div>
							<div class="boxLineParcela">
								<div class="mb-2 d-flex align-items-center">
									<input type="text" name="boleto[parcelas][]" class="number form-control p-form" placeholder="Parcela">
									<input type="text" style="width: 45px;" disabled readonly class="form-control p-form text-center px-0 mx-2" placeholder="de">
									<input type="text" name="boleto[valores][]" class="valor form-control p-form" placeholder="R$">
									<a href="#" class="btnRemoveParcela btn btn-danger py-2 ms-2">
										<i class="fa fa-trash"></i>
									</a>
								</div>
							</div>
							<button type="button" class="btn-append btn-append-parcela-bo btn btn-primary py-1-5 fs-13">Adicionar +</button>
						</div>
					</div>

				</div>

			</div>

		</section>

		<section class="mb-4-5" id="boxCursoSEO" style="display: none;">

			<a href="#boxSeo" data-bs-toggle="collapse" aria-expanded="false" class="hover-d-none">
				<div class="section-title">
					Informações de SEO para resultados de buscas em Navegadores
				</div>
			</a>
			<hr/>
			
			<div class="collapse" id="boxSeo">

				<div class="d-lg-flex mb-3">

					<div class="w-100 me-lg-3 mb-3 mb-lg-0">
						<label>Título</label>
						<input type="text" name="seo_title" class="form-control p-form" placeholder="Title" value="{{ old('seo_title') }}">
					</div>

					<div class="w-100">
						<label>Palavras-chaves</label>
						<input type="text" name="seo_keywords" class="form-control p-form" placeholder="Keywords" value="{{ old('seo_keywords') }}">
					</div>

				</div>

				<div class="w-100 mb-3">
					<label>Descrição</label>
					<input type="text" name="seo_description" class="form-control p-form" placeholder="Description" value="{{ old('seo_description') }}">
				</div>
				
			</div>

		</section>

		<button type="submit" name="sent" value="ok" class="btn-add mt-3">Cadastrar</button>	

    </form>

</div>

@endsection

@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.14.0-beta2/dist/js/bootstrap-select.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.js"></script>
<script>

	@if(\Auth::user()->restringir_unidade == 'S' && \Auth::user()->unidade_id != null)
		jQuery("#unidade_id option:contains('Selecione')").remove();
	@endif

  	$(document).on("focus", ".valor", function() { 
    	$(this).mask('#.##0,00', {
          reverse: true
        });
  	});

  	$(document).on("focus", ".number", function() { 
    	$(this).mask("000");
  	});


    $(".btn-append-parcela-cc").click(function()
    {
    	$(this).prev().append('<div class="mb-2 d-flex align-items-center"> <input type="text" name="cartao-de-credito[parcelas][]" class="number form-control p-form" placeholder="Parcela"> <input type="text" style="width: 45px;" disabled readonly class="form-control p-form text-center px-0 mx-2" placeholder="de"> <input type="text" name="cartao-de-credito[valores][]" class="valor form-control p-form" placeholder="R$"> <a href="#" class="btnRemoveParcela btn btn-danger py-2 ms-2"> <i class="fa fa-trash"></i> </a> </div> </div>');
    });

    $(".btn-append-parcela-cd").click(function()
    {
    	$(this).prev().append('<div class="mb-2 d-flex align-items-center"> <input type="text" name="cartao-de-debito[parcelas][]" class="number form-control p-form" placeholder="Parcela"> <input type="text" style="width: 45px;" disabled readonly class="form-control p-form text-center px-0 mx-2" placeholder="de"> <input type="text" name="cartao-de-debito[valores][]" class="valor form-control p-form" placeholder="R$"> <a href="#" class="btnRemoveParcela btn btn-danger py-2 ms-2"> <i class="fa fa-trash"></i> </a> </div> </div>');
    });

    $(".btn-append-parcela-bo").click(function()
    {
    	$(this).prev().append('<div class="mb-2 d-flex align-items-center"> <input type="text" name="boleto[parcelas][]" class="number form-control p-form" placeholder="Parcela"> <input type="text" style="width: 45px;" disabled readonly class="form-control p-form text-center px-0 mx-2" placeholder="de"> <input type="text" name="boleto[valores][]" class="valor form-control p-form" placeholder="R$"> <a href="#" class="btnRemoveParcela btn btn-danger py-2 ms-2"> <i class="fa fa-trash"></i> </a> </div> </div>');
    });

	$(document).on('click', '.btnRemoveParcela', function(e)
	{
		e.preventDefault();
		$(this).parent().remove();
	});

    let iSemestre = 2;
    $(".btn-append-semestre").click(function()
    {
    	iSemestre++;
    	$(this).prev().append('<div class="boxLineSemestre"> <div class="mb-2 d-flex align-items-center"> <input type="text" name="semestres[]" style="width: 120px;" readonly class="form-control p-form text-center px-0 me-2" value="'+iSemestre+'° Semestre"> <input type="text" name="valores_por_semestres[]" class="valor form-control p-form" placeholder="R$ (valor da mensalidade deste semestre)" required> <a href="#" class="btnRemoveSemestre btn btn-danger py-2 ms-2"> <i class="fa fa-trash"></i> </a> </div> </div>');
    });

	$(document).on('click', '.btnRemoveSemestre', function(e)
	{
		e.preventDefault();
		$(this).parent().remove();
	});

	function setTipoValor(tipo_valor)
	{
		if(tipo_valor == 'M')
		{
			$('.boxValorFechado').hide();
			$('.boxValorMensalidade').show();
		
		} else {

			$('.boxValorMensalidade').hide();
			$('.boxValorFechado').show();
		}

		$("#boxProximosSemestres").hide();
	}

	function setBoxProximosSemestre(tipo_mensalidade)
	{
		if(tipo_mensalidade == 'R')
		{
			document.getElementById('boxProximosSemestres').style.display = 'block';
		
		} else {

			document.getElementById('boxProximosSemestres').style.display = 'none';
		}
	}

	function setPublicar(value)
	{
		if(value == 'S')
		{
			document.getElementById('boxCursoSEO').style.display = 'block';
			document.getElementById('boxCursoApresentacao').style.display = 'block';

		} else {

			document.getElementById('boxCursoSEO').style.display = 'none';
			document.getElementById('boxCursoApresentacao').style.display = 'none';
		}
	}

</script>
@endpush