@extends('layouts.admin.master')

@section('css')
   <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.14.0-beta2/dist/css/bootstrap-select.min.css">
   <style>
      .square-125 { width: 125px; height: 125px; }
      .object-fit-contain { object-fit: contain; }
      .border-profile { border:7px solid #ededed; }
   </style>
@endsection

@section('content')
    
    @livewire('admin.aluno.info', ['user_id' => $user_id])

@endsection

@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.14.0-beta2/dist/js/bootstrap-select.min.js"></script>
<script src="https://cdn.tiny.cloud/1/f50dygt4s4q2k3urd4bh4c2hkjb74m6exr798tjwbayaa0om/tinymce/5/tinymce.min.js"></script>
<script>

	tinymce.init({
		selector: '.descricao',
		language: 'pt_BR',
		height: 300,
		plugins: 'image lists link code',
		toolbar: "image | undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link | code"
	});

   @if(session('showModal_SendMailContrato'))
        
		var myModal = new bootstrap.Modal(document.getElementById("showModal_SendMailContrato"), {});
		document.onreadystatechange = function () {
			myModal.show();
		};

   @endif

</script>
<script>
	var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
	var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
	  return new bootstrap.Tooltip(tooltipTriggerEl)
	})
</script>

<script>
	window.addEventListener('dissmiss-modal', event => {
		$(".modal").modal('hide');
	});
</script>
@endpush	