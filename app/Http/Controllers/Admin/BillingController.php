<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Plataforma;
use App\Models\PlataformaPlano;
use App\Models\FluxoCaixa;
use App\Models\User;

class BillingController extends Controller
{
    public function info()
    {
        $getNomePaginaInterno = 'Meu Plano';

        $plataforma = Plataforma::plataforma()->first();
        $planoSelecionadoId = $plataforma->plano_id ?? 0;

        $planoAtual = PlataformaPlano::plataforma()->where('id', $planoSelecionadoId)->first();

        $countAlunosAtual = User::plataforma()->aluno()->ativo()->count();

        $planos = PlataformaPlano::plataforma()
            ->where('max_qnt_alunos', '>', $countAlunosAtual)
            ->where('id','<>', $planoAtual->id ?? 0)
            ->orderBy('max_qnt_alunos')
            ->get();

        $faturasInadimplentes = FluxoCaixa::select('data_vencimento','data_recebimento','valor_parcela','status')->billing()
        	->where('plataforma_id', 0)
        	->where('referencia', \Request::getHost())
        	->where('status', 0)
        	->whereDate('data_vencimento', '<', date('Y-m-d'))
        	->whereMonth('data_vencimento', '<>', date('m'))
        	->get();

        $faturasDoMesAtual = FluxoCaixa::select('data_vencimento','data_recebimento','valor_parcela','status')->billing()
        	->where('plataforma_id', 0)
        	->where('referencia', \Request::getHost())
        	->where('status', 0)
        	->whereMonth('data_vencimento', date('m'))
        	->get();

        $countTotal = count($faturasInadimplentes) + count($faturasDoMesAtual);


        if(request('contratar_novo_plano_id'))
        {
            $plataforma->update(['plano_id' => request('contratar_novo_plano_id')]);

            $novo_valor = PlataformaPlano::plataforma()->select('valor')->where('id', request('contratar_novo_plano_id'))->pluck('valor')[0] ?? null;

            if($novo_valor)
            {
                $faturasFuturas = FluxoCaixa::billing()
                    ->where('plataforma_id', 0)
                    ->where('referencia', \Request::getHost())
                    ->where('status', 0)
                    ->whereMonth('data_vencimento', '>', date('m'))
                    ->update(['valor_parcela' => $novo_valor]);
            }

            return back()->with('success', 'Novo plano contratado com êxito.');
        }

        return view('admin.billing.info', compact('getNomePaginaInterno','planoAtual','planos','faturasInadimplentes','faturasDoMesAtual','countTotal') );
    }
}