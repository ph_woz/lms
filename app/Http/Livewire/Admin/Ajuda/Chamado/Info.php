<?php

namespace App\Http\Livewire\Admin\Ajuda\Chamado;

use Livewire\Component;
use App\Models\User;
use App\Models\Chamado;
use App\Models\ChamadoComentario;
use Mail;
use App\Mail\EmailNotificaChamadoComentarioWozcode;
use Illuminate\Support\Facades\Http;

class Info extends Component
{
    public $chamado;
    public $cadastrante;
    public $comentario_descricao;

    public function mount($chamado_id)
    {
        $this->chamado = Chamado::plataforma()->find($chamado_id);

        $this->cadastrante = User::plataforma()->where('id', $this->chamado->cadastrante_id)->first();
    }

    public function render()
    {
        $comentarios = ChamadoComentario::plataforma()->where('chamado_id', $this->chamado->id)->orderBy('id','desc')->get();

        return view('livewire.admin.ajuda.chamado.info', ['comentarios' => $comentarios]);
    }

    public function addComentario()
    {
        $comentario = ChamadoComentario::create([
            'chamado_id' => $this->chamado->id,
            'descricao'  => $this->comentario_descricao,
            'tipo_autor' => 'C',
        ]);

        $this->comentario_descricao = null;

        session()->flash('success-livewire', 'Comentário adicionado com êxito.');
        $this->dispatchBrowserEvent('toast');

        Mail::to('pedrohenrique1207ph@gmail.com')->send(new EmailNotificaChamadoComentarioWozcode($this->chamado->assunto, $comentario));
    }

    public function setStatus($status)
    {
        $this->chamado->update(['status' => $status]);

        session()->flash('success-livewire', 'Status do Chamado atualizado com êxito.');
        $this->dispatchBrowserEvent('toast');
    }

    public function setArquivar($arquivado)
    {
        $this->chamado->update(['arquivado' => $arquivado]);

        if($arquivado == 'S')
            $flash = 'Arquivado com êxito.';
        else
            $flash = 'Desarquivado com êxito.';
         
        session()->flash('success-livewire', $flash);
        $this->dispatchBrowserEvent('toast');
    }

}
