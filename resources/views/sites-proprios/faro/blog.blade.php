<!doctype html>
<html lang="pt-br">
<head>

   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1">

   <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/uikit@3.5.9/dist/css/uikit.min.css" />
   <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
   <link rel="stylesheet" href="{{ asset('css/util.css') }}?v=3">
   <link rel="stylesheet" href="{{ asset('sites-proprios/faro/css/home.css') }}?v=3">

   <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
   
   <title>{{ $pagina->seo_title ?? $plataforma->nome }}</title>
   <meta name="robots" content="index, follow">
   <meta name="description" content="{{ $pagina->seo_description ?? null }}"/>
   <meta name="keywords" content="{{ $pagina->seo_keywords ?? null }}"/>
   <link rel="canonical" href="{{ Request::url() }}" />
   <link rel="shortcut icon" href="{{ $plataforma->favicon }}">
   <meta property="og:locale" content="pt_BR" />
   <meta property="og:type" content="website" />
   <meta property="og:title" content="{{ $pagina->seo_title ?? $plataforma->nome }}" />
   <meta property="og:description" content="{{ $pagina->seo_description ?? null }}" />
   <meta property="og:url" content="{{ Request::url() }}" />
   <meta property="og:site_name" content="{{ $plataforma->nome }}" />
   <meta property="og:image" content="{{ $plataforma->logotipo }}" />
   <meta name='author' content='WOZCODE | Pedro Henrique' />

</head>
<body class="bg-f4" id="inicio">

   @include('sites-proprios/faro/componentes/header')

   <main>

        <section id="blog_destaque" class="py-lg-5">
         <div class="container pt-lg-3">

            <h1 class="fs-md-40 font-montserrat weight-800 text-dark">NOTÍCIAS/<span class="color-faixa-verde">BLOG</span></h1>

            <div class="d-lg-none mb-5 mb-lg-0">

              <div class="uk-position-relative uk-visible-toggle" tabindex="-1" uk-slideshow="animation: push">

                  <ul class="uk-slideshow-items">
                      <li>
                        <a href="/blog/post/{{$postDestaque1->slug}}/{{$postDestaque1->id}}">
                           <div class="w-100 rounded-2">
                              <div class="bg-size-cover h-md-500 h-xs-250 rounded-2" style="background-image: linear-gradient(to bottom, rgba(0, 0, 0, .1), rgba(0, 0, 0, 1)), url('{{ $postDestaque1->foto_capa }}');"></div>
                              <div class="uk-position-bottom px-4 pb-4">
                                 <h5 class="text-light text-shadow-2 weight-700 fs-md-30 fs-xs-18 font-montserrat mb-0 p-0">
                                    {{ $postDestaque1->titulo }}
                                 </h5>
                                 <hr class="border" />
                                 <div class="d-flex justify-content-between align-items-center">
                                   <p class="mb-0 fs-12">
                                    @if($postDestaque1->autor)
                                     <span class="color-c9">por</span>
                                     <span class="color-c9 weight-600">{{ \App\Models\Util::getPrimeiroSegundoNome($postDestaque1->autor) }}</span>
                                    @endif
                                   </p>
                                  <p class="mb-0 font-nunito fs-12">
                                    <i class="fa fa-clock color-c9"></i>
                                    <span class="color-c9">{{ \Carbon\Carbon::createFromTimeStamp(strtotime($postDestaque1->created_at))->diffForHumans() }}</span>
                                  </p>
                                 </div>
                              </div>
                           </div>
                        </a>
                      </li>
                      <li>
                        <a href="/blog/post/{{$postDestaque2->slug}}/{{$postDestaque2->id}}">
                           <div class="w-100 rounded-2">
                              <div class="bg-size-cover h-md-500 h-xs-250 rounded-2" style="background-image: linear-gradient(to bottom, rgba(0, 0, 0, .1), rgba(0, 0, 0, 1)), url('{{ $postDestaque2->foto_capa }}');"></div>
                              <div class="uk-position-bottom px-4 pb-4">
                                 <h5 class="text-light text-shadow-2 weight-700 fs-md-25 fs-xs-18 font-montserrat mb-0 p-0">
                                    {{ $postDestaque2->titulo }}
                                 </h5>
                                 <hr class="border" />
                                 <div class="d-flex justify-content-between align-items-center">
                                   <p class="mb-0 fs-12">
                                    @if($postDestaque2->autor)
                                     <span class="color-c9">por</span>
                                     <span class="color-c9 weight-600">{{ \App\Models\Util::getPrimeiroSegundoNome($postDestaque2->autor) }}</span>
                                    @endif
                                   </p>
                                  <p class="mb-0 font-nunito fs-12">
                                    <i class="fa fa-clock color-c9"></i>
                                    <span class="color-c9">{{ \Carbon\Carbon::createFromTimeStamp(strtotime($postDestaque2->created_at))->diffForHumans() }}</span>
                                  </p>
                                 </div>
                              </div>
                           </div>
                        </a>
                      </li>
                      <li>
                       <a href="/blog/post/{{$postDestaque3->slug}}/{{$postDestaque3->id}}">
                          <div class="w-100 rounded-2">
                             <div class="bg-size-cover h-md-500 h-xs-250" style="background-image: linear-gradient(to bottom, rgba(0, 0, 0, .1), rgba(0, 0, 0, 1)), url('{{ $postDestaque3->foto_capa }}');"></div>
                             <div class="uk-position-bottom px-4 pb-4">
                                <h5 class="text-light text-shadow-2 weight-700 fs-18 font-montserrat mb-0 p-0">
                                   {{ $postDestaque3->titulo }}
                                </h5>
                                <hr class="border" />
                                <div class="d-flex justify-content-between align-items-center">
                                  <p class="mb-0 fs-12">
                                   @if($postDestaque3->autor)
                                    <span class="color-c9">por</span>
                                    <span class="color-c9 weight-600">{{ \App\Models\Util::getPrimeiroSegundoNome($postDestaque3->autor) }}</span>
                                   @endif
                                  </p>
                                 <p class="mb-0 font-nunito fs-12">
                                   <i class="fa fa-clock color-c9"></i>
                                   <span class="color-c9">{{ \Carbon\Carbon::createFromTimeStamp(strtotime($postDestaque3->created_at))->diffForHumans() }}</span>
                                 </p>
                                </div>
                             </div>
                          </div>
                       </a>
                      </li>
                      <li>
                         <a href="/blog/post/{{$postDestaque4->slug}}/{{$postDestaque4->id}}">
                            <div class="w-100 rounded-2">
                               <div class="bg-size-cover h-md-500 h-xs-250" style="background-image: linear-gradient(to bottom, rgba(0, 0, 0, .1), rgba(0, 0, 0, 1)), url('{{ $postDestaque4->foto_capa }}');"></div>
                               <div class="uk-position-bottom px-4 pb-4">
                                  <h5 class="text-light text-shadow-2 weight-700 fs-18 font-montserrat mb-0 p-0">
                                     {{ $postDestaque4->titulo }}
                                  </h5>
                                  <hr class="border" />
                                  <div class="d-flex justify-content-between align-items-center">
                                    <p class="mb-0 fs-12">
                                     @if($postDestaque4->autor)
                                      <span class="color-c9">por</span>
                                      <span class="color-c9 weight-600">{{ \App\Models\Util::getPrimeiroSegundoNome($postDestaque4->autor) }}</span>
                                     @endif
                                    </p>
                                   <p class="mb-0 font-nunito fs-12">
                                     <i class="fa fa-clock color-c9"></i>
                                     <span class="color-c9">{{ \Carbon\Carbon::createFromTimeStamp(strtotime($postDestaque4->created_at))->diffForHumans() }}</span>
                                   </p>
                                  </div>
                               </div>
                            </div>
                         </a>
                      </li>
                  </ul>

                  <div class="uk-light">
                      <a class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous uk-slideshow-item="previous"></a>
                      <a class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-next uk-slideshow-item="next"></a>
                  </div>

              </div>

            </div>

            <div class="d-md-none d-xs-none d-lg-block">

              <div class="row">

                 <div class="col-lg-6 px-lg-2">
                    
                    <a href="/blog/post/{{$postDestaque1->slug}}/{{$postDestaque1->id}}">
                       <div class="w-100 uk-inline-clip uk-transition-toggle box-convenios rounded-2">
                          <div class="uk-transition-scale-up uk-transition-opaque bg-size-cover h-lg-500 h-xs-250 rounded-2" style="background-image: linear-gradient(to bottom, rgba(0, 0, 0, .1), rgba(0, 0, 0, 1)), url('{{ $postDestaque1->foto_capa }}');"></div>
                          <div class="uk-position-bottom px-4 pb-4">
                             <h5 class="text-light text-shadow-2 weight-700 fs-md-30 fs-xs-18 font-montserrat mb-0 p-0">
                                {{ $postDestaque1->titulo }}
                             </h5>
                             <hr class="border" />
                             <div class="d-flex justify-content-between align-items-center">
                               <p class="mb-0 fs-12">
                                @if($postDestaque1->autor)
                                 <span class="color-c9">por</span>
                                 <span class="color-c9 weight-600">{{ \App\Models\Util::getPrimeiroSegundoNome($postDestaque1->autor) }}</span>
                                @endif
                               </p>
                              <p class="mb-0 font-nunito fs-12">
                                <i class="fa fa-clock color-c9"></i>
                                <span class="color-c9">{{ \Carbon\Carbon::createFromTimeStamp(strtotime($postDestaque1->created_at))->diffForHumans() }}</span>
                              </p>
                             </div>
                          </div>
                       </div>
                    </a>

                 </div>

                 <div class="col-lg-3 px-lg-2 py-3 py-lg-0">

                    <a href="/blog/post/{{$postDestaque2->slug}}/{{$postDestaque2->id}}">
                       <div class="w-100 uk-inline-clip uk-transition-toggle box-convenios rounded-2">
                          <div class="uk-transition-scale-up uk-transition-opaque bg-size-cover h-lg-500 h-xs-250 rounded-2" style="background-image: linear-gradient(to bottom, rgba(0, 0, 0, .1), rgba(0, 0, 0, 1)), url('{{ $postDestaque2->foto_capa }}');"></div>
                          <div class="uk-position-bottom px-4 pb-4">
                             <h5 class="text-light text-shadow-2 weight-700 fs-md-25 fs-xs-18 font-montserrat mb-0 p-0">
                                {{ $postDestaque2->titulo }}
                             </h5>
                             <hr class="border" />
                             <div class="d-flex justify-content-between align-items-center">
                               <p class="mb-0 fs-12">
                                @if($postDestaque2->autor)
                                 <span class="color-c9">por</span>
                                 <span class="color-c9 weight-600">{{ \App\Models\Util::getPrimeiroSegundoNome($postDestaque2->autor) }}</span>
                                @endif
                               </p>
                              <p class="mb-0 font-nunito fs-12">
                                <i class="fa fa-clock color-c9"></i>
                                <span class="color-c9">{{ \Carbon\Carbon::createFromTimeStamp(strtotime($postDestaque2->created_at))->diffForHumans() }}</span>
                              </p>
                             </div>
                          </div>
                       </div>
                    </a>

                 </div>

                 <div class="col-lg-3 px-lg-2">

                    <div>
                       <a href="/blog/post/{{$postDestaque3->slug}}/{{$postDestaque3->id}}">
                          <div class="w-100 uk-inline-clip uk-transition-toggle box-convenios rounded-2">
                             <div class="uk-transition-scale-up uk-transition-opaque bg-size-cover h-lg-240 h-xs-250" style="background-image: linear-gradient(to bottom, rgba(0, 0, 0, .1), rgba(0, 0, 0, 1)), url('{{ $postDestaque3->foto_capa }}');"></div>
                             <div class="uk-position-bottom px-4 pb-4">
                                <h5 class="text-light text-shadow-2 weight-700 fs-18 font-montserrat mb-0 p-0">
                                   {{ $postDestaque3->titulo }}
                                </h5>
                                <hr class="border" />
                                <div class="d-flex justify-content-between align-items-center">
                                  <p class="mb-0 fs-12">
                                   @if($postDestaque3->autor)
                                    <span class="color-c9">por</span>
                                    <span class="color-c9 weight-600">{{ \App\Models\Util::getPrimeiroSegundoNome($postDestaque3->autor) }}</span>
                                   @endif
                                  </p>
                                 <p class="mb-0 font-nunito fs-12">
                                   <i class="fa fa-clock color-c9"></i>
                                   <span class="color-c9">{{ \Carbon\Carbon::createFromTimeStamp(strtotime($postDestaque3->created_at))->diffForHumans() }}</span>
                                 </p>
                                </div>
                             </div>
                          </div>
                       </a>
                    </div>
                    <div class="py-2"></div>
                    <div>
                       <a href="/blog/post/{{$postDestaque4->slug}}/{{$postDestaque4->id}}">
                          <div class="w-100 uk-inline-clip uk-transition-toggle box-convenios rounded-2">
                             <div class="uk-transition-scale-up uk-transition-opaque bg-size-cover h-lg-240 h-xs-250" style="background-image: linear-gradient(to bottom, rgba(0, 0, 0, .1), rgba(0, 0, 0, 1)), url('{{ $postDestaque4->foto_capa }}');"></div>
                             <div class="uk-position-bottom px-4 pb-4">
                                <h5 class="text-light text-shadow-2 weight-700 fs-18 font-montserrat mb-0 p-0">
                                   {{ $postDestaque4->titulo }}
                                </h5>
                                <hr class="border" />
                                <div class="d-flex justify-content-between align-items-center">
                                  <p class="mb-0 fs-12">
                                   @if($postDestaque4->autor)
                                    <span class="color-c9">por</span>
                                    <span class="color-c9 weight-600">{{ \App\Models\Util::getPrimeiroSegundoNome($postDestaque4->autor) }}</span>
                                   @endif
                                  </p>
                                 <p class="mb-0 font-nunito fs-12">
                                   <i class="fa fa-clock color-c9"></i>
                                   <span class="color-c9">{{ \Carbon\Carbon::createFromTimeStamp(strtotime($postDestaque4->created_at))->diffForHumans() }}</span>
                                 </p>
                                </div>
                             </div>
                          </div>
                       </a>
                    </div>

                 </div>

              </div>

            </div>

         </div>
        </section>

    @foreach(\App\Models\Categoria::plataforma()->ativo()->publico()->tipo('blog')->orderBy('nome')->get() as $categoria)
      @php
        $postsIds = \App\Models\BlogPostCategoria::plataforma()->where('categoria_id', $categoria->id)->select('post_id')->get()->pluck('post_id')->toArray();
        $posts    = \App\Models\BlogPost::plataforma()->whereIn('id', $postsIds)->orderBy('id','desc')->get();
      @endphp
      <section class="pb-5 pt-3">
        <div class="container">

            <div class="d-flex align-items-center mb-3">
              <h1 class="title-blog my-0 text-shadow-1 fs-15 pe-4">
                <a href="/blog/categoria/{{\Str::slug($categoria->nome)}}/{{$categoria->id}}" class="color-faixa-verde text-shadow-1 hover-d-none"> 
                  {{ $categoria->nome }}
                </a>
              </h1>
              <hr class="uk-divider border w-100 my-0 py-0">
            </div>
            
            @if(count($posts) > 0)
            <div uk-slider="autoplay: true; autoplay-interval: 3000;">

                <div class="uk-position-relative uk-visible-toggle uk-light" tabindex="-1">

                    <ul class="uk-slider-items uk-child-width-1-4@s uk-grid">
                      @foreach($posts as $post)
                        <a href="/blog/post/{{$post->slug}}/{{$post->id}}">
                            <div class="uk-card uk-card-default box-shadow border-bottom">
                                <div class="uk-card-media-top">
                                    <img src="{{ $post->foto_capa }}" alt="{{ $post->titulo }}" class="h-lg-175 object-fit-cover w-100">
                                </div>
                                <div class="uk-card-body py-3 h-lg-100 h-xs-100 px-4a">
                                    <h3 class="uk-card-title fs-16">
                                      {{ \Str::limit($post->titulo, 55) }}
                                    </h3>
                                </div>
                            </div>
                        </a>
                      @endforeach
                    </ul>

                    <a class="uk-position-center-left uk-position-small uk-hidden-hover text-dark" href="#" uk-slidenav-previous uk-slider-item="previous"></a>
                    <a class="uk-position-center-right uk-position-small uk-hidden-hover text-dark" href="#" uk-slidenav-next uk-slider-item="next"></a>

                </div>

                <ul class="uk-slider-nav uk-dotnav uk-flex-center uk-margin"></ul>

            </div>
            @endif
            
        </div>
      </section>
    @endforeach

      <section id="newsletter" class="py-5 bg-gradient">
         <div class="container" id="contato">

            @if(session('success'))
               <div class='alert alert-success'><b>Sucesso!</b> {{ session('success') }}</div>
            @endif

            <div class="row justify-content-between align-items-center">

               <div class="col-lg-6">
                  <p class="mb-0 weight-700 fs-22 pe-lg-5 font-roboto">
                     <span class="color-2">Que tal receber nosso contato e ficar por dentro das nossas novidades?</span><span class="color-azul"> Cadastre-se!</span>
                  </p>
               </div>

               <form method="POST" class="col-lg-6 mt-3 mt-lg-0">
               @csrf

                  <input type="hidden" name="assunto" value="Newsletter">

                  <div class="d-lg-flex w-100">

                     <input type="text" name="nome" class="w-100 form-control lead-form rounded-left rounded-right-lg-0" placeholder="NOME" required>
                     
                     <div class="d-xs-none line-form"></div>                     

                     <input type="email" name="email" class="w-100 form-control lead-form rounded-left-lg-0 rounded-right-lg-0" placeholder="EMAIL" required>

                     <button type="submit" class="btn-news" name="sentContato" value="S">
                        Enviar
                     </button>

                  </div>

               </form>
            </div>

         </div>
      </section>

   </main>

   @include('sites-proprios/faro/componentes/footer')

   <a href="https://api.whatsapp.com/send?phone=55{{preg_replace('/\D/', '', $plataforma->whatsapp)}}&text=Olá, vim através do Site {{ $plataforma->nome }}" target="_blank" class="text-white fs-30">
       <div class="fixed-bottom right-0 me-3 mb-3 z-3">
           <div class="square-60 rounded-circle bg-wpp flex-center box-shadow-wpp">
               <i class="fab fa-whatsapp"></i>
           </div>
       </div>
   </a>

   <script src="https://cdn.jsdelivr.net/npm/uikit@3.5.9/dist/js/uikit.min.js"></script>
   <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-U1DAWAznBHeqEIlVSCgzq+c9gqGAJn5c/t99JyeKa9xxaYpSvHU5awsuZVVFIhvj" crossorigin="anonymous"></script>
   <script>

      window.onscroll = function() {myFunction()};

      var header = document.getElementById("menu");
      var sticky = header.offsetTop;

      function myFunction() {
        if (window.pageYOffset > sticky) {
          header.classList.add("fixed-top");
        } else {
          header.classList.remove("fixed-top");
        }
      }
   </script>

</body>
</html>