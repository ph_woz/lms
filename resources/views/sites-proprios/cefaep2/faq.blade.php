<!doctype html>
<html lang="pt-br">
<head>

  <!-- Google tag (gtag.js) -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=G-MREB450HCT"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'G-MREB450HCT');
  </script>
  
   {!! $plataforma->scripts_start_head !!}

   <meta charset="utf-8">
   <meta name="viewport" content="width=device-width, initial-scale=1">

   <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KK94CHFLLe+nY2dmCWGMq91rCGa5gtU4mk92HdvYe+M/SXH301p5ILy+dN9+nJOZ" crossorigin="anonymous">
   <link rel="stylesheet" href="{{ asset('sites-proprios/cefaep2/css/home.css') }}">
   <link rel="stylesheet" href="{{ asset('css/util.css') }}">

   <title>{{ $pagina->title ?? $plataforma->nome }}</title>
   <meta name="robots" content="index, follow">
   <meta name="description" content="{{ $pagina->description ?? null }}"/>
   <meta name="keywords" content="{{ $pagina->keywords ?? null }}"/>
   <link rel="canonical" href="{{ Request::url() }}" />
   <link rel="shortcut icon" href="{{ $plataforma->favicon }}">
   <meta property="og:locale" content="pt_BR" />
   <meta property="og:type" content="website" />
   <meta property="og:title" content="{{ $pagina->title ?? $plataforma->nome }}" />
   <meta property="og:description" content="{{ $pagina->description ?? null }}" />
   <meta property="og:url" content="{{ Request::url() }}" />
   <meta property="og:site_name" content="{{ $plataforma->nome }}" />
   <meta property="og:image" content="{{ $plataforma->logotipo }}" />
   <meta name='author' content='WOZCODE | Pedro Henrique' />

   {!! $plataforma->scripts_final_head !!}

</head>
<body id="inicio">

	@include('sites-proprios/cefaep2/componentes/header')

	<main>

      <section>
       <div class="w-100 h-200 background-size-cover background-position-center" style="background-image: url('{{ asset('sites-proprios/cefaep2/images/header-faq.webp') }}">
          <div class="position-relative h-100 px-xl-7 px-3">

            <!--
             <div class="row align-items-center h-100 position-relative">
                <div class="mt-2">
                   <h1 class="text-light font-saira-condensed weight-800 fs-xs-35 fs-md-50 text-shadow-3">
                      FAQ
                   </h1>
                </div>
             </div>
            -->
            
          </div>
       </div>
      </section>

      <section class="py-5">
      	<div class="container-fluid px-xl-7">

            <div class="mb-3">
               <a href="#" class="hover-d-none color-444" data-bs-toggle="collapse" data-bs-target="#secaoFAQ1" aria-expanded="false" aria-controls="collapseExample">
                  <div class="bg-white px-4 py-3 box-shadow border-top rounded-10">

                     <div class="color-verde-maduro d-flex align-items-center">

                        <i class="me-3 fs-20 bi bi-question-circle"></i>

                        <h1 class="color-verde-maduro py-0 my-0 weight-700 font-inter fs-17">                        
                           Como efetuarei o pagamento do curso à distância?
                        </h1>

                     </div>

                     <div class="collapse" id="secaoFAQ1">
                        <hr class="border border-secondary">
                        <p class="m-0 fs-15 color-444 weight-600 font-inter">
                           Você poderá efetuar o pagamento na própria unidade com cartão, ou pagar através de boleto bancário em qualquer banco ou lotérica.
                        </p>
                     </div>

                  </div>
               </a>
            </div>

            <div class="mb-3">
               <a href="#" class="hover-d-none color-444" data-bs-toggle="collapse" data-bs-target="#secaoFAQ2" aria-expanded="false" aria-controls="collapseExample">
                  <div class="bg-white px-4 py-3 box-shadow border-top rounded-10">

                     <div class="color-verde-maduro d-flex align-items-center">

                        <i class="me-3 fs-20 bi bi-question-circle"></i>

                        <h1 class="color-verde-maduro py-0 my-0 weight-700 font-inter fs-17">                        
                           Como receberei o material do meu curso?
                        </h1>

                     </div>

                     <div class="collapse" id="secaoFAQ2">
                        <hr class="border border-secondary">
                        <p class="m-0 fs-15 color-444 weight-600 font-inter">
                           O material para estudos estará disponível na sua Área do aluno, você também poderá baixar as apostilas e fazer a impressão para estudar.
                        </p>
                     </div>

                  </div>
               </a>
            </div>

            <div class="mb-3">
               <a href="#" class="hover-d-none color-444" data-bs-toggle="collapse" data-bs-target="#secaoFAQ3" aria-expanded="false" aria-controls="collapseExample">
                  <div class="bg-white px-4 py-3 box-shadow border-top rounded-10">

                     <div class="color-verde-maduro d-flex align-items-center">

                        <i class="me-3 fs-20 bi bi-question-circle"></i>

                        <h1 class="color-verde-maduro py-0 my-0 weight-700 font-inter fs-17">                        
                           E se eu acabar reprovando em alguma matéria?
                        </h1>

                     </div>

                     <div class="collapse" id="secaoFAQ3">
                        <hr class="border border-secondary">
                        <p class="m-0 fs-15 color-444 weight-600 font-inter">
                           Neste caso, após a correção da prova, você receberá outra para fazer novamente, sem nenhum custo adicional.
                        </p>
                     </div>

                  </div>
               </a>
            </div>

            <div class="mb-3">
               <a href="#" class="hover-d-none color-444" data-bs-toggle="collapse" data-bs-target="#secaoFAQ4" aria-expanded="false" aria-controls="collapseExample">
                  <div class="bg-white px-4 py-3 box-shadow border-top rounded-10">

                     <div class="color-verde-maduro d-flex align-items-center">

                        <i class="me-3 fs-20 bi bi-question-circle"></i>

                        <h1 class="color-verde-maduro py-0 my-0 weight-700 font-inter fs-17">                        
                           Eu tenho restrições em meu nome (SCPC / SERASA), posso fazer boleto bancário mesmo assim?
                        </h1>

                     </div>

                     <div class="collapse" id="secaoFAQ4">
                        <hr class="border border-secondary">
                        <p class="m-0 fs-15 color-444 weight-600 font-inter">
                           Sim, a CEFARP não consulta nome.
                        </p>
                     </div>

                  </div>
               </a>
            </div>

            <div class="mb-3">
               <a href="#" class="hover-d-none color-444" data-bs-toggle="collapse" data-bs-target="#secaoFAQ5" aria-expanded="false" aria-controls="collapseExample">
                  <div class="bg-white px-4 py-3 box-shadow border-top rounded-10">

                     <div class="color-verde-maduro d-flex align-items-center">

                        <i class="me-3 fs-20 bi bi-question-circle"></i>

                        <h1 class="color-verde-maduro py-0 my-0 weight-700 font-inter fs-17">                        
                           Já conclui o 1º ou 2º ano, como faço para continuar?
                        </h1>

                     </div>

                     <div class="collapse" id="secaoFAQ5">
                        <hr class="border border-secondary">
                        <p class="m-0 fs-15 color-444 weight-600 font-inter">
                           Neste caso, os dois anos que você já concluiu serão aceitos e as matérias eliminadas, porém o valor do curso não sofrerá alteração.
                        </p>
                     </div>

                  </div>
               </a>
            </div>

            <div class="mb-3">
               <a href="#" class="hover-d-none color-444" data-bs-toggle="collapse" data-bs-target="#secaoFAQ6" aria-expanded="false" aria-controls="collapseExample">
                  <div class="bg-white px-4 py-3 box-shadow border-top rounded-10">

                     <div class="color-verde-maduro d-flex align-items-center">

                        <i class="me-3 fs-20 bi bi-question-circle"></i>

                        <h1 class="color-verde-maduro py-0 my-0 weight-700 font-inter fs-17">                        
                           O certificado é reconhecido pelo MEC?
                        </h1>

                     </div>

                     <div class="collapse" id="secaoFAQ6">
                        <hr class="border border-secondary">
                        <p class="m-0 fs-15 color-444 weight-600 font-inter">
                           Sim, o certificado é oficial com validade nacional, autorizado pelo governo e o nome dos alunos após aprovação é devidamente publicado no GDAE e no Diário Oficial.
                        </p>
                     </div>

                  </div>
               </a>
            </div>

            <div class="mb-3">
               <a href="#" class="hover-d-none color-444" data-bs-toggle="collapse" data-bs-target="#secaoFAQ7" aria-expanded="false" aria-controls="collapseExample">
                  <div class="bg-white px-4 py-3 box-shadow border-top rounded-10">

                     <div class="color-verde-maduro d-flex align-items-center">

                        <i class="me-3 fs-20 bi bi-question-circle"></i>

                        <h1 class="color-verde-maduro py-0 my-0 weight-700 font-inter fs-17">                        
                           O curso à distância é reconhecido oficialmente?
                        </h1>

                     </div>

                     <div class="collapse" id="secaoFAQ7">
                        <hr class="border border-secondary">
                        <p class="m-0 fs-15 color-444 weight-600 font-inter">
                           Os cursos à distância são regulamentados pelos Conselhos Estaduais de Educação de cada estado e amparados pela Lei Federal 9394/96.
                        </p>
                     </div>

                  </div>
               </a>
            </div>

            <div class="mb-3">
               <a href="#" class="hover-d-none color-444" data-bs-toggle="collapse" data-bs-target="#secaoFAQ9" aria-expanded="false" aria-controls="collapseExample">
                  <div class="bg-white px-4 py-3 box-shadow border-top rounded-10">

                     <div class="color-verde-maduro d-flex align-items-center">

                        <i class="me-3 fs-20 bi bi-question-circle"></i>

                        <h1 class="color-verde-maduro py-0 my-0 weight-700 font-inter fs-17">                        
                           Qual a idade mínima para realizar o Supletivo EAD?
                        </h1>

                     </div>

                     <div class="collapse" id="secaoFAQ9">
                        <hr class="border border-secondary">
                        <p class="m-0 fs-15 color-444 weight-600 font-inter">
                           É necessário que você tenha 18 anos e 6 meses para cursar o Ensino Médio (antigo 2º grau) ou 16 anos para cursar o Ensino fundamental (antigo 1º grau). Você deverá comprovar a escolaridade anterior para continuar da série que você parou.
                        </p>
                     </div>

                  </div>
               </a>
            </div>

            <div class="mb-3">
               <a href="#" class="hover-d-none color-444" data-bs-toggle="collapse" data-bs-target="#secaoFAQ11" aria-expanded="false" aria-controls="collapseExample">
                  <div class="bg-white px-4 py-3 box-shadow border-top rounded-10">

                     <div class="color-verde-maduro d-flex align-items-center">

                        <i class="me-3 fs-20 bi bi-question-circle"></i>

                        <h1 class="color-verde-maduro py-0 my-0 weight-700 font-inter fs-17">
                           Qual é a duração do curso realizado à distância?
                        </h1>

                     </div>

                     <div class="collapse" id="secaoFAQ11">
                        <hr class="border border-secondary">
                        <p class="m-0 fs-15 color-444 weight-600 font-inter">
                           Cada cursista tem o seu ritmo próprio. Irá depender do seu desempenho e de seu tempo disponível.
                        </p>
                     </div>

                  </div>
               </a>
            </div>

            <div class="mb-3">
               <a href="#" class="hover-d-none color-444" data-bs-toggle="collapse" data-bs-target="#secaoFAQ10" aria-expanded="false" aria-controls="collapseExample">
                  <div class="bg-white px-4 py-3 box-shadow border-top rounded-10">

                     <div class="color-verde-maduro d-flex align-items-center">

                        <i class="me-3 fs-20 bi bi-question-circle"></i>

                        <h1 class="color-verde-maduro py-0 my-0 weight-700 font-inter fs-17">
                           Se faltar algum tipo de documento, posso realizar a matrícula mesmo assim?
                        </h1>

                     </div>

                     <div class="collapse" id="secaoFAQ10">
                        <hr class="border border-secondary">
                        <p class="m-0 fs-15 color-444 weight-600 font-inter">
                           Sim, o aluno poderá trazer o restante dos documentos no decorrer do curso.
                        </p>
                     </div>

                  </div>
               </a>
            </div>

            <div class="mt-4-5">
            
               <a href="tel:+55{{preg_replace('/\D/', '', $plataforma->telefone)}}" class="btn btn-quero-me-matricular px-4 py-3">
                  <i class="bi bi-telephone"></i>                           
                  <span class="ms-2">Ligue agora </span> <span class="ms-1 d-xs-none">gratuitamente</span>
                  <span class="ms-2">{{ $plataforma->telefone?? '(00) 0000-0000' }}</span>
               </a>
            
            </div>

      	</div>
      </section>

	</main>

	@include('sites-proprios/cefaep2/componentes/footer')

</body>
</html>