<?php

namespace App\Http\Livewire\Admin\Plano;

use Livewire\WithPagination;
use Livewire\Component;
use App\Models\Plano;

class Lista extends Component
{
    use WithPagination;

    protected $paginationTheme = 'bootstrap';

    public $search;
    public $status = 0;
    public $ordem;

    protected $queryString = ['search','status','ordem'];

    public function render()
    {
        $planos = Plano::plataforma();

        if($this->search)
        {
            $planos = $planos->where('nome', 'like', '%'.$this->search.'%');
        }

        $planos = $planos->where('status', $this->status);

        $countPlanos = $planos->count();


        if($this->ordem == 'desc' || $this->ordem == 'asc')
        {
            $planos = $planos->orderBy('id', $this->ordem);
        
        } else {

            $planos = $planos->orderBy('nome');
        }
        

        $planos = $planos->paginate(10);


        return view('livewire.admin.plano.lista', [
            'planos'      => $planos,
            'countPlanos' => $countPlanos,
        ]);
    }
}
