<?php

namespace App\Http\Controllers\SiteProprio;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Depoimento;
use App\Models\PaginaConteudo;

class CedetepEnfController extends Controller
{
    public function index()
    {
        $depoimentos = Depoimento::plataforma()->ativo()->orderBy('id','desc')->get();

        $seo = PaginaConteudo::plataforma()->where('url','/')->select('plataforma_id','seo_title','seo_keywords','seo_description')->first();

        if(request('sentContato'))
        {
            if(request('check'))
            {
                return back()->with('danger', 'SPAM Detected!');
            }

            \App\Models\Contato::create(request()->all() + [
                'plataforma_id' => $seo->plataforma_id,
                'tipo' => 'L',
                'assunto' => 'Solicitar Contato',
            ]);
            
            return redirect('/#pre-matricula')->with('success', 'Inscrição enviada com êxito.');
        }

    	return view('sites-proprios.cedetep.enfermagem', compact('depoimentos','seo') );
    }
}