<h1 style="font-size: 20px;font-weight: 500;">
	{{ strtok($nome, " ") }} adicionou um Comentário ao Chamado: {{$assunto}}.
</h1>

<div style="font-size: 16.5px; margin-bottom: 2rem;">
	<p>
		{!! nl2br($descricao) !!}
	</p>
</div>

<div>
	<a href="{{ $url }}" style="display: inline-block; color: #FFF; text-decoration: none; border-radius:2px; background:#1865d9; padding-left:20px; padding-right:20px; padding-top:10px; padding-bottom:10px;">
		Abrir Chamado
	</a>
</div>