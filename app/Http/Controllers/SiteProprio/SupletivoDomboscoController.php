<?php

namespace App\Http\Controllers\SiteProprio;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Contato;
use App\Models\Plataforma;

class SupletivoDomboscoController extends Controller
{

    public function index()
    {
        $plataforma = Plataforma::plataforma()->first();

        if(request('btnSend') || request('inscrever'))
        {
            $nome = request('nome');
            $firstname = $this->getPrimeiroSegundoNome($nome);
            $lastname = explode($firstname, $nome);
            $lastname = $lastname[1];
            
            if($lastname == '') {
                $fullname  = explode(' ', $firstname);
                $firstname = $fullname[0];

                if (array_key_exists(1, $fullname))
                    $lastname  = $fullname[1];
                else
                    $lastname = null;
            } 

            $mensagem = request('mensagem') ?? 'Dúvidas? Solicite em contato!';

            $arr = array(
                'properties' => array(
                    array(
                        'property' => 'email',
                        'value' => request('email')
                    ),
                    array(
                        'property' => 'firstname',
                        'value' => $firstname
                    ),
                    array(
                        'property' => 'lastname',
                        'value' => $lastname
                    ),
                    array(
                        'property' => 'phone',
                        'value' => request('celular')
                    ),
                    array(
                        'property' => 'website',
                        'value' => 'supletivodombosco.com.br'
                    ),
                    array(
                        'property' => 'message',
                        'value' => $mensagem
                    )
                )
            );
            $post_json = json_encode($arr);
            $endpoint = 'https://api.hubapi.com/contacts/v1/contact?hapikey=6db1dc59-c605-4d3a-942e-0aecf3ddb690';
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post_json);
            curl_setopt($ch, CURLOPT_URL, $endpoint);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $response = curl_exec($ch);
            $status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            $curl_errors = curl_error($ch);
            curl_close($ch);
            echo "curl Errors: " . $curl_errors;
            echo "\nStatus code: " . $status_code;
            echo "\nResponse: " . $response;
        }

        if(request('btnSend')) {

            if(strlen($_POST['spamVerify']) >= 1) 
                header('location:/#contato');

            $contato = Contato::create(request()->all() + ['plataforma_id' => $plataforma->id, 'site' => \Request::fullUrl(), 'tag' => 'Fale Conosco', 'assunto' => 'Fale Conosco']);

            if($plataforma->email_faleconosco != null)
                Mail::to($plataforma->email_faleconosco)->send(new FaleConosco($contato));

            return redirect('/#contato')->with('success','Email enviado com êxito.');
        }

        if(request('inscrever'))
        {
            $contato = Contato::create(request()->all() + ['plataforma_id' => $plataforma->id, 'site' => \Request::fullUrl(), 'tag' => 'Dúvidas', 'assunto' => 'Dúvidas']);

            if($plataforma->email_faleconosco != null)
                Mail::to($plataforma->email_faleconosco)->send(new FaleConosco($contato));

            return redirect('/#inicio')->with('success','Email enviado com êxito.');
        }

        return view('sites-proprios.supletivodombosco.home');
    }

    public function getPrimeiroSegundoNome($nome)
    {
        $names = explode(' ', $nome);
        $nome = (isset($names[1])) ? $names[0]. ' ' .$names[1] : $names[0];

        if (array_key_exists(2, $names))
            $names2 = $names[2];
        else
            $names2 = null;

        if( $this->endsWith($nome, 'de') || $this->endsWith($nome, 'dos') || $this->endsWith($nome, 'da') )
            $nome = $nome . ' ' . $names2 ?? null;

        return $nome;
    }

    public function endsWith( $haystack, $needle ) {
        return $needle === "" || (substr($haystack, -strlen($needle)) === $needle);
    }

}