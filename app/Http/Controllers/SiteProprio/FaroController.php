<?php

namespace App\Http\Controllers\SiteProprio;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Banner;
use App\Models\Documento;
use App\Models\PaginaConteudo;
use App\Models\SecaoDescritiva;
use App\Models\Curso;
use App\Models\Depoimento;
use App\Models\BlogPost;

class FaroController extends Controller
{
    public function index()
    {
        $plataforma_id = $this->getPlataformaId();

        $seo = PaginaConteudo::plataforma()->where('url','/')->select('plataforma_id','seo_title','seo_keywords','seo_description')->first();

        $banners = Banner::plataforma()->ativo()->orderBy('ordem')->get();
        $depoimentos = Depoimento::plataforma()->ativo()->orderBy('ordem')->get();

        $postsIdsBlog = [];

        $postDestaque1 = BlogPost::select('blog_posts.id','blog_posts.titulo','blog_posts.slug','blog_posts.foto_capa','blog_posts.created_at','autor.nome as autor')
            ->leftJoin('users as autor','blog_posts.autor_id','autor.id')
            ->where('blog_posts.plataforma_id', $plataforma_id)
            ->where('blog_posts.publicar', 'S')
            ->where('blog_posts.destaque_ordem', 1)
            ->first();

        $postDestaque2 = BlogPost::select('blog_posts.id','blog_posts.titulo','blog_posts.slug','blog_posts.foto_capa','blog_posts.created_at','autor.nome as autor')
            ->leftJoin('users as autor','blog_posts.autor_id','autor.id')
            ->where('blog_posts.plataforma_id', $plataforma_id)
            ->where('blog_posts.publicar', 'S')
            ->where('blog_posts.destaque_ordem', 2)
            ->first();

        $postDestaque3 = BlogPost::select('blog_posts.id','blog_posts.titulo','blog_posts.slug','blog_posts.foto_capa','blog_posts.created_at','autor.nome as autor')
            ->leftJoin('users as autor','blog_posts.autor_id','autor.id')
            ->where('blog_posts.plataforma_id', $plataforma_id)
            ->where('blog_posts.publicar', 'S')
            ->where('blog_posts.destaque_ordem', 3)
            ->first();

        $postDestaque4 = BlogPost::select('blog_posts.id','blog_posts.titulo','blog_posts.slug','blog_posts.foto_capa','blog_posts.created_at','autor.nome as autor')
            ->leftJoin('users as autor','blog_posts.autor_id','autor.id')
            ->where('blog_posts.plataforma_id', $plataforma_id)
            ->where('blog_posts.publicar', 'S')
            ->where('blog_posts.destaque_ordem', 4)
            ->first();


        if(isset($postDestaque1)) { $postsIdsBlog[] = $postDestaque1->id; }
        if(isset($postDestaque2)) { $postsIdsBlog[] = $postDestaque2->id; }
        if(isset($postDestaque3)) { $postsIdsBlog[] = $postDestaque3->id; }
        if(isset($postDestaque4)) { $postsIdsBlog[] = $postDestaque4->id; }

        if(request('sentContato'))
        {
            if(request('check'))
                return back()->with('danger', 'SPAM Detected!');

            \App\Models\Contato::create(request()->all() + [
                'plataforma_id' => $seo->plataforma_id,
                'tipo' => 'L',
            ]);
            
            return redirect('/#pre-matricula')->with('success_prematricula', 'Inscrição enviada com êxito.');
        }

        return view('sites-proprios.faro.home', compact('seo','banners','depoimentos','postDestaque1','postDestaque2','postDestaque3','postDestaque4','postsIdsBlog') );
    }

    public function curso($curoSlug)
    {
        $curso = Curso::plataforma()->publico()->where('slug', $curoSlug)->first();

        $coordenador = \App\Models\User::select('users.nome','users.foto_perfil','users.naturalidade','users.descricao','users.celular')->plataforma()->ativo()->where('id', $curso->coordenador_id)->first();

        $corpoDocente = \App\Models\CorpoDocente::plataforma()->curso()->where('registro_id', $curso->id)->get();

        $secoesDescritivas = SecaoDescritiva::plataforma()->curso()->where('registro_id', $curso->id)->orderBy('ordem')->get();

        return view('sites-proprios.faro.curso', compact('curso','coordenador','corpoDocente','secoesDescritivas') );
    }

    public function fies()
    {
        $seo = PaginaConteudo::plataforma()->where('url','/fies')->select('plataforma_id','seo_title','seo_keywords','seo_description')->first();

        $secoes = SecaoDescritiva::plataforma()->fies()->orderBy('titulo')->get();

        return view('sites-proprios.faro.fies', compact('secoes') );
    }

    public function prouni()
    {
        $seo = PaginaConteudo::plataforma()->where('url','/prouni')->select('plataforma_id','seo_title','seo_keywords','seo_description')->first();

        $secoes = SecaoDescritiva::plataforma()->prouni()->orderBy('titulo')->get();

        return view('sites-proprios.faro.prouni', compact('secoes','seo') );
    }

    public function enem()
    {
        $seo = PaginaConteudo::plataforma()->where('url','/enem')->select('plataforma_id','seo_title','seo_keywords','seo_description')->first();

        return view('sites-proprios.faro.enem', compact('seo') );
    }

    public function ouvidoria()
    {
        $seo = PaginaConteudo::plataforma()->where('url','/ouvidoria')->select('plataforma_id','seo_title','seo_keywords','seo_description')->first();

        return view('sites-proprios.faro.ouvidoria', compact('seo') );
    }

    public function cpa()
    {
        $seo = PaginaConteudo::plataforma()->where('url','/cpa')->select('plataforma_id','seo_title','seo_keywords','seo_description')->first();

        return view('sites-proprios.faro.cpa', compact('seo') );
    }

    public function transferencia()
    {
        $seo = PaginaConteudo::plataforma()->where('url','/transferencia')->select('plataforma_id','seo_title','seo_keywords','seo_description')->first();

        return view('sites-proprios.faro.transferencia', compact('seo') );
    }

    public function vestibular()
    {
        $seo = PaginaConteudo::plataforma()->where('url','/vestibular')->select('plataforma_id','seo_title','seo_keywords','seo_description')->first();

        return view('sites-proprios.faro.vestibular', compact('seo') );
    }

    public function consultarDiploma()
    {
        $seo = PaginaConteudo::plataforma()->where('url','/consultar-diploma')->select('plataforma_id','seo_title','seo_keywords','seo_description')->first();

        $trilhas = [];
        $turmas  = [];
        $aluno   = [];
        $tipo    = $_GET['tipo']  ?? null;
        $value   = $_GET['value'] ?? null;


        if(request('sent'))
        {
            if(request('tipo') == 'CPF')
            {
                $cpf = $this->formatCnpjCpf($value);

                $aluno = \App\Models\User::select('id','nome')->plataforma()->aluno()->where('cpf', $cpf)->first();

                if(isset($aluno))
                {
                    $trilhas = \App\Models\TrilhaAluno::select('trilhas.nome','trilhas_alunos.data_conclusao','trilhas_alunos.data_colacao_grau','trilhas_alunos.codigo_certificado')
                        ->join('trilhas','trilhas_alunos.trilha_id','trilhas.id')
                        ->where('trilhas_alunos.plataforma_id', $seo->plataforma_id)
                        ->where('trilhas.plataforma_id', $seo->plataforma_id)
                        ->where('trilhas_alunos.aluno_id', $aluno->id)
                        ->orderBy('data_conclusao')
                        ->get();
                }

            } else {

                $trilhas = \App\Models\TrilhaAluno::select('trilhas.nome','trilhas_alunos.data_conclusao','trilhas_alunos.aluno_id','trilhas_alunos.data_colacao_grau','trilhas_alunos.codigo_certificado')
                    ->join('trilhas','trilhas_alunos.trilha_id','trilhas.id')
                    ->where('trilhas_alunos.plataforma_id', $seo->plataforma_id)
                    ->where('trilhas.plataforma_id', $seo->plataforma_id)
                    ->where('codigo_certificado', $value)
                    ->whereNotNull('data_conclusao')
                    ->orderBy('data_conclusao')
                    ->get();

                $aluno_id = $trilhas[0]['aluno_id'] ?? $turmas[0]['aluno_id'] ?? null;

                $aluno = \App\Models\User::select('id','nome')->plataforma()->aluno()->where('id', $aluno_id)->first();
            }
        }

        return view('sites-proprios.faro.consultar-diploma', compact('seo','aluno','trilhas','tipo','value') );
    }

    public function blog()
    {
        $plataforma_id = $this->getPlataformaId();
        
        $pagina = PaginaConteudo::plataforma()->where('url','/blog')->first();

        $postsIdsBlog = [];
        
        $postDestaque1 = BlogPost::select('blog_posts.id','blog_posts.titulo','blog_posts.slug','blog_posts.foto_capa','blog_posts.created_at','autor.nome as autor')
            ->leftJoin('users as autor','blog_posts.autor_id','autor.id')
            ->where('blog_posts.plataforma_id', $plataforma_id)
            ->where('blog_posts.publicar', 'S')
            ->where('blog_posts.destaque_ordem', 1)
            ->first();

        $postDestaque2 = BlogPost::select('blog_posts.id','blog_posts.titulo','blog_posts.slug','blog_posts.foto_capa','blog_posts.created_at','autor.nome as autor')
            ->leftJoin('users as autor','blog_posts.autor_id','autor.id')
            ->where('blog_posts.plataforma_id', $plataforma_id)
            ->where('blog_posts.publicar', 'S')
            ->where('blog_posts.destaque_ordem', 2)
            ->first();

        $postDestaque3 = BlogPost::select('blog_posts.id','blog_posts.titulo','blog_posts.slug','blog_posts.foto_capa','blog_posts.created_at','autor.nome as autor')
            ->leftJoin('users as autor','blog_posts.autor_id','autor.id')
            ->where('blog_posts.plataforma_id', $plataforma_id)
            ->where('blog_posts.publicar', 'S')
            ->where('blog_posts.destaque_ordem', 3)
            ->first();

        $postDestaque4 = BlogPost::select('blog_posts.id','blog_posts.titulo','blog_posts.slug','blog_posts.foto_capa','blog_posts.created_at','autor.nome as autor')
            ->leftJoin('users as autor','blog_posts.autor_id','autor.id')
            ->where('blog_posts.plataforma_id', $plataforma_id)
            ->where('blog_posts.publicar', 'S')
            ->where('blog_posts.destaque_ordem', 4)
            ->first();


        if(isset($postDestaque1)) { $postsIdsBlog[] = $postDestaque1->id; }
        if(isset($postDestaque2)) { $postsIdsBlog[] = $postDestaque2->id; }
        if(isset($postDestaque3)) { $postsIdsBlog[] = $postDestaque3->id; }
        if(isset($postDestaque4)) { $postsIdsBlog[] = $postDestaque4->id; }


        if(request('sentContato'))
        {
            \App\Models\Contato::create(request()->all() + [
                'plataforma_id' => $plataforma_id,
                'tipo' => 'L',
            ]);
            
            return back()->with('success', 'Inscrição enviada com êxito.');
        }

        return view('sites-proprios.faro.blog', compact('pagina','postDestaque1','postDestaque2','postDestaque3','postDestaque4','postsIdsBlog') );
    }

    public function blogPost($id)
    {
        $post = BlogPost::plataforma()->where('id', $id)->first();

        if(isset($post) && $post->publicar != 'S')
        {
            return view('sites-proprios.faro.sem-acesso', ['mensagem' => 'Página não publicada.']);
        }

        $ultimos_posts = BlogPost::plataforma()->publico()->orderBy('id','desc')->limit(10)->get();

        if(request('sentContato'))
        {
            \App\Models\Contato::create(request()->all() + [
                'plataforma_id' => $post->plataforma_id,
                'tipo' => 'L',
            ]);
            
            return back()->with('success', 'Inscrição enviada com êxito.');
        }

        return view('sites-proprios.faro.blog-post', compact('post','ultimos_posts') );
    }

    public function sobreNos()
    {
        $pagina = PaginaConteudo::plataforma()->ativo()->where('url','/sobre-nos')->first();
        
        $depoimentos = Depoimento::plataforma()->ativo()->orderBy('ordem')->get();

        return view('sites-proprios.faro.sobre-nos', compact('pagina','depoimentos') );
    }

    public function politicaDePrivacidade()
    {
        $pagina = PaginaConteudo::plataforma()->ativo()->where('url','/politica-de-privacidade')->first();
        
        return view('sites-proprios.faro.politica-de-privacidade', compact('pagina') );
    }

    public function biblioteca()
    {
        $pagina = PaginaConteudo::plataforma()->ativo()->where('url','/biblioteca')->first();
        
        return view('sites-proprios.faro.biblioteca', compact('pagina') );
    }

    public function getPlataformaId()
    {
        return session('plataforma_id') ?? \App\Models\Plataforma::dominio()->pluck('id')[0] ?? null;
    }

    public function editais()
    {
        $pagina = PaginaConteudo::plataforma()->ativo()->where('url','/editais')->first();

        $search = request('search') ?? null;

        $documentos = Documento::select('nome','link','data_publicacao')->plataforma()->editais()->where('publicar', 'S')->where('arquivar', 'N');

        if($search)
        {
            $documentos = $documentos->where('nome', 'like', '%'.$search.'%');
        }

        $documentos = $documentos->orderBy('nome')->get();

        return view('sites-proprios.faro.editais', compact('pagina','documentos','search') );
    }

    public function dou()
    {
        $pagina = PaginaConteudo::plataforma()->ativo()->where('url','/dou')->first();

        $search = request('search') ?? null;

        $documentos = Documento::select('nome','link','data_publicacao')->plataforma()->dou();

        if($search)
        {
            $documentos = $documentos->where('nome', 'like', '%'.$search.'%');
        }

        $documentos = $documentos->orderBy('nome')->get();

        return view('sites-proprios.faro.dou', compact('pagina','documentos','search') );
    }

    public function categoria()
    {
        $plataforma_id = $this->getPlataformaId();

        $categoria = \App\Models\Categoria::select('nome','seo_title','seo_keywords','seo_description')->plataforma()->ativo()->publico()->tipo('curso')->where('slug', \Request::segment(1))->first();

        $cursos = Curso::select('nome','foto_miniatura','slug')
            ->where('plataforma_id', $plataforma_id)
            ->where('status', 0)
            ->where('publicar', 'S')
            ->where('nivel', $categoria->nome ?? 'N/D')
            ->orderBy('nome')
            ->get();

        return view('sites-proprios.faro.categoria', compact('categoria','cursos') );
    }

    public function polos()
    {
        $pagina = PaginaConteudo::plataforma()->ativo()->where('url','/polos')->first();

        $polos = \App\Models\Unidade::plataforma()->ativo()->publico();

        $search = request('search') ?? null;

        if($search)
        {
            $polos = $polos->where('nome', 'like', '%'.$search.'%');
        }

        $polos = $polos->orderBy('nome')->get();

        return view('sites-proprios.faro.polos', compact('pagina','polos','search') );
    }

    public function parceiros()
    {
        $pagina = PaginaConteudo::plataforma()->ativo()->where('url','/parceiros')->first();

        $parceiros = \App\Models\Parceiro::plataforma()->publico()->orderBy('id','desc')->get();

        return view('sites-proprios.faro.parceiros', compact('pagina','parceiros') );
    }

    public function formatCnpjCpf($value)
    {
      $CPF_LENGTH = 11;
      $cnpj_cpf = preg_replace("/\D/", '', $value);
      
      if (strlen($cnpj_cpf) === $CPF_LENGTH) {
        return preg_replace("/(\d{3})(\d{3})(\d{3})(\d{2})/", "\$1.\$2.\$3-\$4", $cnpj_cpf);
      } 
      
      return preg_replace("/(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/", "\$1.\$2.\$3/\$4-\$5", $cnpj_cpf);
    }

}