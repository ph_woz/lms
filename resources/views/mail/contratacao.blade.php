{!! $descricao !!}

@if($botaoRedefinirSenha == true)
<div>
	<a href="{{\Request::root()}}/redefinir-senha/token/{{ $token }}/email/{{ $email }}/id/{{ $id }}" style="display: inline-block; color: #FFF; text-decoration: none; border-radius:2px; background:#1865d9; padding-left:15px; padding-right:15px; padding-top:7px; padding-bottom:7px;">
		Alterar Senha e Entrar na plataforma
	</a>
</div>
@endif