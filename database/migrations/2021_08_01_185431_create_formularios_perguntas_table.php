<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFormulariosPerguntasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('formularios_perguntas', function (Blueprint $table) {
            $table->id();
            $table->integer('plataforma_id');
            $table->integer('formulario_id');
            $table->integer('ordem')->default(0)->nullable();
            $table->text('pergunta');
            $table->string('tipo_input')->default('text');
            $table->string('validation')->nullable();
            $table->text('options')->nullable();
            $table->string('obrigatorio', 1)->default('S');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('formularios_perguntas');
    }
}
