<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Models\Curso;
use App\Models\User;
use App\Models\CursoModulo;
use App\Models\CorpoDocente;
use App\Models\SecaoDescritiva;
use App\Models\CursoAula;
use App\Models\CursoCategoria;
use App\Models\Categoria;
use App\Models\CorpoDocenteCurso;
use App\Models\Certificado;

class CursoController extends Controller
{
    public function lista()
    {
        return view('admin.curso.lista');
    }
    
    public function info($curso_id)
    {
        return view('admin.curso.info', compact('curso_id') );
    }

    public function add()
    {
        $categorias    = Categoria::select('id','nome')->plataforma()->ativo()->tipo('curso')->orderBy('nome')->get();
        $certificados  = Certificado::select('id','referencia')->plataforma()->ativo()->orderBy('referencia')->get();
        $colaboradores = User::select('id','nome')->plataforma()->ativo()->orderBy('nome')->get();

        if(request('sent'))
        {
            $checkJaExist = Curso::plataforma()->where('referencia', request('referencia'))->exists();

            if($checkJaExist)
            {
                return back()->withInput()->with('danger','Já existe um curso cadastrado com esta referência.');
            }

            $curso = Curso::create(request()->all() + ['slug' => \Str::slug(request('nome'))]);

            $this->syncCategorias($curso->id);
            $this->syncFoto($curso, 'foto_capa', 'foto');
            $this->syncFoto($curso, 'foto_miniatura', 'foto_min');
            $this->syncMatrizCurricular($curso, 'matriz_curricular', 'matriz');

            return redirect()->route('admin.curso.info', ['id' => $curso->id])->with('success', 'curso criado com êxito.');
        }

    	return view('admin.curso.add', compact('categorias','certificados','colaboradores') );
    }

    public function editar($curso_id)
    {
        $curso = Curso::plataforma()->where('id', $curso_id)->first();

        $categorias = Categoria::select('id','nome')->plataforma()->ativo()->tipo('curso')->orderBy('nome')->get();
        $categoriasSelected = CursoCategoria::plataforma()->where('curso_id', $curso_id)->get()->pluck('categoria_id')->toArray();

        $certificados = Certificado::select('id','referencia')->plataforma()->ativo()->orderBy('referencia')->get();

        $colaboradores = User::select('id','nome')->plataforma()->ativo()->orderBy('nome')->get();

        $corpoDocente = CorpoDocenteCurso::select('user_id')->plataforma()->where('curso_id', $curso_id)->get()->pluck('user_id')->toArray();


        if(request('sent'))
        {
            $checkJaExist = Curso::plataforma()->where('id','<>', $curso_id)->where('referencia', request('referencia'))->exists();

            if($checkJaExist)
            {
                return back()->withInput()->with('danger','Já existe um curso cadastrado com esta referência.');
            }

            $curso->update(request()->all() + ['slug' => \Str::slug(request('nome'))]);

            $this->syncCategorias($curso->id);
            $this->syncFoto($curso, 'foto_capa', 'foto');
            $this->syncFoto($curso, 'foto_miniatura', 'foto_min');
            $this->syncMatrizCurricular($curso, 'matriz_curricular', 'matriz');

            return back()->with('success', 'curso atualizado com êxito.');
        }

        return view('admin.curso.editar', compact('curso','categorias','categoriasSelected','certificados','colaboradores','corpoDocente') );
    }

    public function listaModulos($curso_id)
    {
        if(request('editarModulo'))
        {
            CursoModulo::plataforma()->where('id', request('modulo_id'))->update(request()->only('nome','status','ordem'));

            return back()->with('success', 'Módulo atualizado com êxito.');
        }
        if(request('deletarModulo'))
        {
            $modulo = CursoModulo::plataforma()->where('id', request('modulo_id'))->first();

            $countAulas = CursoAula::plataforma()->where('curso_id', $curso_id)->where('modulo_id', $modulo->id)->count();

            if($countAulas > 0)
            {
                return back()->with('danger', 'Não é possível deletar um módulo com aulas cadastradas neste mesmo módulo.');
            
            } else {

                $modulo->delete();

                return back()->with('success', 'Módulo deletado com êxito.');
            }
        }


        $getNomePaginaInterno = 'Módulos';

        return view('admin.curso.modulo.lista', compact('getNomePaginaInterno','curso_id') );
    }

    public function listaAulas($curso_id, $modulo_id)
    {
        $getNomePaginaInterno = 'Aulas';

        return view('admin.curso.modulo.aula.lista', compact('curso_id','modulo_id','getNomePaginaInterno') );
    }

    public function addAula($curso_id, $modulo_id)
    {
        if(request('link') == null and request('arq') == null)
        {
            return back()->with('danger', 'Você não pode adicionar um conteúdo vazio.');
        }

        $aula = CursoAula::create(request()->all() + [
            'curso_id'  => $curso_id, 
            'modulo_id' => $modulo_id,
            'conteudo'  => request('link'),
        ]);

        $this->syncArquivo($aula, 'conteudo', 'arq');

        return back()->with('success', 'Aula cadastrada com êxito.');
    }

    public function editarAula($curso_id, $modulo_id)
    {   
        $aula = CursoAula::plataforma()
            ->where('curso_id', $curso_id)
            ->where('modulo_id', $modulo_id)
            ->where('id', request('aula_id'))
            ->first();

        $aula->update(request()->all() + ['conteudo' => request('link')]);

        $this->syncArquivo($aula, 'conteudo', 'arq');

        return redirect()->route('admin.curso.modulo.aula.lista', ['curso_id' => $curso_id, 'modulo_id' => $modulo_id])->with('success', 'Aula atualizada com êxito.');
    }

    public function deleteAula($curso_id, $modulo_id)
    {
        $aula = CursoAula::plataforma()
            ->where('curso_id', $curso_id)
            ->where('modulo_id', $modulo_id)
            ->where('id', request('aula_id'))
            ->first();

        $path = str_replace(\App\Models\Util::getLinkStorage(), '', $aula->conteudo);
        Storage::delete($path);

        $aula->delete();

        return redirect()->route('admin.curso.modulo.aula.lista', ['curso_id' => $curso_id, 'modulo_id' => $modulo_id])->with('success', 'Aula deletada com êxito.');
    }

    public function editarSecoesDescritivas($curso_id)
    {
        $curso = Curso::select('referencia')->plataforma()->where('id', $curso_id)->value('referencia');
        $secoes = SecaoDescritiva::plataforma()->where('registro_id', $curso_id)->orderBy('ordem')->get();

        if(request('add'))
        {
            SecaoDescritiva::create([
                'tipo'        => 'C',
                'registro_id' => $curso_id,
                'titulo'      => request('titulo'),
                'descricao'   => request('descricao'),
                'expandido'   => request('expandido'),
                'ordem'       => request('ordem'),
            ]);

            return back()->with('success', 'Seção cadastrada com êxito.');
        }

        if(request('editar'))
        {
            foreach(request('secao') as $secao)
            {
                SecaoDescritiva::plataforma()
                    ->where('registro_id', $curso_id)
                    ->where('id', $secao['id'])
                    ->update([
                        'titulo'    => $secao['titulo'],
                        'descricao' => $secao['descricao'],
                        'expandido' => $secao['expandido'],
                        'ordem'     => $secao['ordem'],
                    ]);
            }

            return back()->with('success', 'Seções atualizada com êxito.');
        }

        if(request('deletar'))
        {
            SecaoDescritiva::plataforma()
                ->where('registro_id', $curso_id)
                ->where('id', request('deletar_id'))
                ->delete();

            return back()->with('success', 'Seção deletada com êxito.');
        }

        return view('admin.curso.secoes', compact('curso','curso_id','secoes') );
    }

    public function editarCorpoDocente($curso_id)
    {
        $curso = Curso::select('referencia')->plataforma()->where('id', $curso_id)->value('referencia');
        $corpoDocente = CorpoDocente::plataforma()->curso()->where('registro_id', $curso_id)->get();

        $editarDocente = CorpoDocente::plataforma()->where('id', $_GET['editar_docente_id'] ?? 0)->first();

        if(request('add'))
        {
            $docente = CorpoDocente::create([
                'tipo'        => 'C',
                'registro_id' => $curso_id,
                'nome'        => request('nome'),
                'titulacao'   => request('titulacao'),
                'formacao'    => request('formacao'),
            ]);

            $this->syncFotoDocente($docente, 'foto_perfil', 'foto');

            return back()->with('success', 'Docente cadastrado com êxito.');
        }

        if(request('editar'))
        {
            $editarDocente->update([
                'nome'      => request('nome'),
                'titulacao' => request('titulacao'),
                'formacao'  => request('formacao'),
            ]);

            $this->syncFotoDocente($editarDocente, 'foto_perfil', 'foto');

            return back()->with('success', 'Docente atualizado com êxito.');
        }

        if(request('deletar'))
        {
            $docente = CorpoDocente::plataforma()->curso()
                ->where('registro_id', $curso_id)
                ->where('id', request('deletar_id'))
                ->first();

            $path = str_replace(\App\Models\Util::getLinkStorage(), '', $docente->foto_perfil);
            Storage::delete($path);

            $docente->delete();

            return back()->with('success', 'Docente deletado com êxito.');
        }

        return view('admin.curso.corpo-docente', compact('curso','curso_id','corpoDocente','editarDocente') );
    }

    public function syncArquivo($aula, $tipo, $inputFileName)
    {
        $file = request()->file($inputFileName);

        if($file)
        {
            $path = str_replace(\App\Models\Util::getLinkStorage(), '', $aula->$tipo);
            Storage::delete($path);

            $diretorio = 'plataformas/'.session('plataforma_id').'/cursos/'.$aula->curso_id.'/modulos/'.$aula->modulo_id.'/aulas/'.$aula->id;
            $filename = $aula->id.'.'.$file->getClientOriginalExtension();

            $path = Storage::putFileAs($diretorio, $file, $filename, 'public');

            $linkStorage = \App\Models\Util::getLinkStorage();
            $caminho = $linkStorage . $path;

            $aula->update([$tipo => $caminho]);
        }
    }

    public function syncFoto($curso, $tipo, $inputFileName)
    {
        $file = request()->file($inputFileName);
     
        if($file)
        {
            $path = str_replace(\App\Models\Util::getLinkStorage(), '', $curso->$tipo);
            Storage::delete($path);
            
            $diretorio = 'plataformas/'.session('plataforma_id').'/cursos/'.$curso->id;
            $filename = $tipo.'.'.$file->getClientOriginalExtension();

            $path = Storage::putFileAs($diretorio, $file, $filename, 'public');
            $linkStorage = \App\Models\Util::getLinkStorage();
            $caminho = $linkStorage . $path;

            $curso->update([$tipo => $caminho]);
        }
    }

    public function syncMatrizCurricular($curso, $tipo, $inputFileName)
    {
        $file = request()->file($inputFileName);
     
        if($file)
        {
            $path = str_replace(\App\Models\Util::getLinkStorage(), '', $curso->$tipo);
            Storage::delete($path);
            
            $diretorio = 'plataformas/'.session('plataforma_id').'/cursos/'.$curso->id;
            $filename = $tipo.'.'.$file->getClientOriginalExtension();

            $path = Storage::putFileAs($diretorio, $file, $filename, 'public');
            $linkStorage = \App\Models\Util::getLinkStorage();
            $caminho = $linkStorage . $path;

            $curso->update([$tipo => $caminho]);
        }
    }

    public function syncFotoDocente($docente, $tipo, $inputFileName)
    {
        $file = request()->file($inputFileName);

        if($file)
        {
            $path = str_replace(\App\Models\Util::getLinkStorage(), '', $docente->$tipo);
            Storage::delete($path);
            
            $diretorio = 'plataformas/'.session('plataforma_id').'/cursos/'.$docente->registro_id.'/corpo-docente';
            $filename = $docente->id.'.'.$file->getClientOriginalExtension();

            $path = Storage::putFileAs($diretorio, $file, $filename, 'public');
            $linkStorage = \App\Models\Util::getLinkStorage();
            $caminho = $linkStorage . $path;

            $docente->update([$tipo => $caminho]);
        }
    }

    public function syncCategorias($curso_id)
    {
        CursoCategoria::plataforma()->where('curso_id', $curso_id)->whereNotIn('categoria_id', request('categorias') ?? array())->delete();

        if(request('categorias') == null)
            return;

        foreach(request('categorias') as $categoria_id)
        {
            $countDuplicate = CursoCategoria::plataforma()->where('curso_id', $curso_id)->where('categoria_id', $categoria_id)->count();
            
            if($countDuplicate <= 0)
                CursoCategoria::create([
                    'plataforma_id' => session('plataforma_id'),
                    'curso_id'      => $curso_id, 
                    'categoria_id'  => $categoria_id,
                ]);
        }
    }

    public function syncCorpoDocente($curso_id)
    {
        $corpoDocente = request('corpoDocente');
       
        if($corpoDocente != null)
        {
            foreach($corpoDocente as $user_id)
            {
                CorpoDocenteCurso::create([
                    'plataforma_id' => session('plataforma_id'),
                    'curso_id'      => $curso_id, 
                    'user_id'       => $user_id,
                ]);
            }
        }
    }

}