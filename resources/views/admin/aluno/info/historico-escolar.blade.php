<!doctype html>
<html lang="pt-br">
<head>

  <!-- Google tag (gtag.js) -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=G-MREB450HCT"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'G-MREB450HCT');
  </script>
  
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="noindex, nofollow">
    <title>{{ $getNomePaginaInterno }} - {{ $plataforma->nome }}</title>
    <link href="{{ asset('css/util.css') }}?v=9" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">

</head>
<body class="bg-f9">

  	<section class="mb-4 mt-3">
    	<div class="container-fluid px-xl-5">
    		<div class="bg-white p-4-5 box-shadow rounded border">

	            <div class="d-flex align-items-center">

	            	<div class="me-3">
						<img src="{{ $plataforma->logotipo ?? asset('images/no-image.jpeg') }}" id="img-boletim-escolar-imprimir" class="img-fluid" width="200" height="200">
	            	</div>

	               	<div id="info-gerais-historico-escolar">

	               		{!! $conteudo->conteudo ?? null !!}

	               	</div>

	            </div>

	            <h1 class="text-center text-uppercase font-roboto">
	               HISTÓRICO ESCOLAR
	            </h1>

	            <div class="row pt-5">

	               <div class="col-lg-6 ml-lg-1">

	                  <p class="mb-1 font-roboto weight-600">
	                     <a href="/admin/aluno/info/{{$aluno_id}}" target="_blank" class="text-d-none">
	                        <span class="color-444">MATRÍCULA DO ALUNO:</span>
	                     	<span class="text-dark">{{ $aluno_id }}</span>
	                     </a>
	                  </p>

	                  <p class="mb-1 font-roboto weight-600">
	                     <a href="/admin/aluno/info/{{$aluno_id}}" target="_blank" class="text-d-none">
	                        <span class="color-444">NOME DO ALUNO:</span>
	                     	<span class="text-dark">{{ $aluno->nome }}</span>
	                     </a>
	                  </p>

	                  <p class="mb-1 font-roboto weight-600">
	                     <span class="color-444">
	                        CPF:
	                     </span>
	                     {{ $aluno->cpf ?? '000.000.00-00' }}
	                  </p> 

	                  <p class="mb-1 font-roboto weight-600">
	                     <span class="color-444">
	                        DATA DE NASCIMENTO:
	                     </span>
	                     {{ \App\Models\Util::replaceDatePt($aluno->data_nascimento) ?? 'Não definido' }}
	                  </p> 

	                  <p class="mb-1 font-roboto weight-600">
	                     <span class="color-444">
	                        DATA DE EMISSÃO:
	                     </span>
	                    {{ \App\Models\Util::replaceDatePt(date('Y-m-d')) }}
	                  </p> 

	               </div>

	            </div>

	        </div>
	    </div>
  	</section>

  	@foreach($trilhas as $trilha)

  	@php

  		$trilha_id = $trilha->id;

  		if($trilha->data_conclusao != null)
  		{
  			$progresso = 100;
  		
  		} else {
		
			$turmasIdsParaConcluir = \App\Models\TrilhaTurma::select('turma_id')->plataforma()->where('trilha_id', $trilha_id)->get()->pluck('turma_id')->toArray();

	        if(count($turmasIdsParaConcluir) == 0)
	        {
	        	$progresso = 0;

	        } else {

				$turmasConcluidas = \App\Models\TurmaAluno::select('turma_id')->plataforma()->where('aluno_id', $aluno_id)->whereNotNull('data_conclusao_curso')->whereIn('turma_id', $turmasIdsParaConcluir)->count();
		        $progresso = round(($turmasConcluidas*100/count($turmasIdsParaConcluir)), 1);
	        }
	    }


        $lines = \DB::select( \DB::raw("

            SELECT
                turmas.id as turma_id,
                turmas.nome as turma,
                avaliacoes.referencia as avaliacao,
                avaliacoes.id as avaliacao_id,
                avaliacoes.n_tentativas,
                avaliacoes_alunos.n_tentativa AS n_tentativa_atual,
                avaliacoes_alunos.status,
                turmas.nota_minima_aprovacao,
                avaliacoes_alunos.nota,
                turmas_alunos.data_primeiro_acesso,
                turmas_alunos.data_ultimo_acesso,
                turmas_alunos.data_conclusao_conteudo,
                turmas_alunos.data_conclusao_curso

            FROM
                trilhas_alunos

            JOIN trilhas_turmas ON trilhas_alunos.trilha_id = trilhas_turmas.trilha_id
            JOIN turmas_alunos ON trilhas_turmas.turma_id = turmas_alunos.turma_id AND turmas_alunos.aluno_id = ".$aluno_id."

            JOIN turmas ON turmas_alunos.turma_id = turmas.id
            LEFT JOIN avaliacoes ON turmas.id = avaliacoes.turma_id
            LEFT JOIN avaliacoes_alunos ON avaliacoes.id = avaliacoes_alunos.avaliacao_id AND avaliacoes_alunos.aluno_id = ".$aluno_id."
            
            WHERE
                trilhas_alunos.plataforma_id = ".$plataforma_id."
                AND trilhas_turmas.trilha_id = ".$trilha_id."
                AND trilhas_alunos.trilha_id = ".$trilha_id."
                AND trilhas_alunos.aluno_id = ".$aluno_id."

            ORDER BY avaliacoes.referencia;

        "));

  	@endphp

  	<section class="mb-4">
    	<div class="container-fluid px-xl-5">
    		<div class="bg-white p-4-5 box-shadow rounded border">

    			<div class="d-lg-flex justify-content-between align-items-end">
		         	
		         	<div class="w-100">
			         	<h1 class="pb-0 mb-lg-0 mb-2 font-roboto fs-20">
			         		<a href="/admin/trilha/info/{{$trilha_id}}" class="text-d-none text-dark" target="_blank">{{ $trilha->nome }}</a>
			         	</h1>
			        </div>

		         	<div class="w-100">
						<div class="progress mt-3" data-bs-toggle="tooltip" data-bs-placement="top" data-html="true" title="Progresso do conteúdo do curso">
							<div class="progress-bar progress-bar-striped" role="progressbar" style="width: {{$progresso}}%;" aria-valuenow="{{$progresso}}" aria-valuemin="0" aria-valuemax="100">{{$progresso}}%</div>
						</div>
					</div>

					<div class="w-100" align="right"> 
			         	@if($trilha->data_conclusao !== null)
				         	<span class="bg-wpp text-light px-2-5 py-1-5 fs-14 rounded-2 weight-600">
				         		Concluído em {{ \App\Models\Util::replaceDateTimePt($trilha->data_conclusao) }}
				         	</span>
			         	@else
				         	<span class="bg-secondary text-light px-2-5 py-1-5 fs-14 rounded-2 weight-600">
				         		Ainda não concluiu
				         	</span>
			         	@endif
			        </div>

		        </div>

	         	<hr class="border border-secondary">

	            <div class="table-responsive">
	               <table class="table table-bordered">
	                 <thead>
	                   <tr>
	                     <th scope="col">TURMA</th>
	                     <th scope="col">AVALIAÇÃO</th>
	                     <th scope="col">NOTA</th>
	                     <th scope="col">TENTATIVAS</th>
	                     <th scope="col">CONTEÚDO</th>
	                     <th scope="col">SITUAÇÃO</th>
	                     <th scope="col">PRIMEIRO ACESSO</th>
	                     <th scope="col">ÚLTIMO ACESSO</th>
	                   </tr>
	                 </thead>
	                 <tbody>
	                 	@foreach($lines as $line)
	                    <tr>
	                       	<td>
	                       		<span class="weight-600">
	                       			<a href="/admin/turma/info/{{$line->turma_id}}" target="_blank" class="text-dark text-d-none"> 
	                       				{{ $line->turma }}
	                       			</a>
	                       		</span>
	                       	</td>
	                       	<td>
	                       		<span class="weight-600">
		                       		@if($line->avaliacao)
		                       			<a href="/admin/avaliacao/info/{{$line->avaliacao_id}}" target="_blank" class="text-dark text-d-none"> 
		                       				{{ $line->avaliacao }}
		                       			</a>
		                       		@else
		                       			<i>Não há avaliação</i>
		                       		@endif
		                       	</span>
	                       	</td>
	                       	<td>
	                       		@if($line->status == 'F')
	                       			@if(floatval($line->nota) >= floatval($line->nota_minima_aprovacao))
		                       			<span class="text-primary weight-600">
				                       		{{ number_format($line->nota, 2, ',', '.') }}
				                       		de nota mínima {!! $line->nota_minima_aprovacao ?? '<i>Não definido</i>' !!}
		                       			</span>
		                       		@else
		                       			<span class="text-danger weight-600">
				                       		{{ number_format($line->nota, 2, ',', '.') }}
				                       		de nota mínima {!! $line->nota_minima_aprovacao ?? '<i>Não definido</i>' !!}
		                       			</span>
		                       		@endif
		                       	@else
		                       		@if($line->avaliacao)
		                       			<span class="text-danger weight-600"><i>Ainda não fez</i></span>
		                       		@else
		                       			<span class="weight-600"><i>Não há avaliação</i></span>
		                       		@endif
		                       	@endif
	                       	</td>
	                       	<td>
	                       		@if($line->avaliacao)
		                       		@if(intval($line->n_tentativa_atual) >= intval($line->n_tentativas))
		                       			<span class="text-danger weight-600">{{ $line->n_tentativa_atual ?? '0' }} tentativas feitas de {{ $line->n_tentativas ?? '0' }}.</span>
		                       		@else
		                       			<span class="text-primary weight-600">{{ $line->n_tentativa_atual ?? '0' }} tentativas feitas de {{ $line->n_tentativas ?? '0' }}.</span>
		                       		@endif
		                       	@else
		                       		<span class="weight-600"><i>Não há avaliação</i></span>
		                       	@endif
	                       	</td>
	                       	<td>
	                       		@if($line->data_conclusao_conteudo)
	                       			<span class="text-primary weight-600">Concluiu em {{ \App\Models\Util::replaceDateTimePt($line->data_conclusao_conteudo) }}</span>
	                       		@else
	                       			<span class="text-danger weight-600"><i>Ainda não concluiu</i></span>
	                       		@endif
	                       	</td>
	                       	<td>
	                       		@if($line->data_conclusao_curso)
	                       			<span class="text-success weight-600">Concluiu em {{ \App\Models\Util::replaceDateTimePt($line->data_conclusao_curso) }}</span>
	                       		@else
	                       			<span class="text-danger weight-600">Ainda não concluiu</span>
	                       		@endif
	                       	</td>
	                       	<td>
	                       		@if($line->data_primeiro_acesso)
	                       			<span class="text-dark weight-600">{{ \App\Models\Util::replaceDateTimePt($line->data_primeiro_acesso) }}</span>
	                       		@else
	                       			<span class="text-danger weight-600">Ainda não acessou</span>
	                       		@endif
	                       	</td>
	                       	<td>
	                       		@if($line->data_ultimo_acesso)
	                       			<span class="text-dark weight-600">{{ \App\Models\Util::replaceDateTimePt($line->data_ultimo_acesso) }}</span>
	                       		@else
	                       			<span class="text-danger weight-600">Ainda não acessou</span>
	                       		@endif
	                       	</td>
	                    </tr>
	                    @endforeach
	                 </tbody>
	               </table>
	            </div>

	        </div>
     	</div>
  	</section>
  	@endforeach

  	<section class="pb-5 pt-5" id="assinatura-do-diretor">
     	<div class="mt-5 container-fluid text-center">

           	<div align="center">
           		<div id="assinatura-diretor-historico-escolar">
            	<img src="{{ $conteudo->seo_keywords ?? null }}" class="img-fluid" id="historico-escolar-foto-assinatura" width="{{ $conteudo->seo_title ?? 200 }}">
            	<hr class="border border-dark" style="width: 300px;" />
              	<p class="mb-0 font-roboto">Assinatura do Diretor</p>
           	</div>

     	</div>
  	</section>

  	{!! $plataforma->scripts_final_body !!}

</body>
</html>