<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use HubSpot\Factory;
use HubSpot\Client\Crm\Contacts\ApiException;
use HubSpot\Client\Crm\Contacts\Model\SimplePublicObjectInput;

class FormularioIntegracao extends Model
{
    use HasFactory;

    protected $table = 'formularios_integracoes';

    public $timestamps = false;

    protected $fillable = [
        'plataforma_id',
        'formulario_id',
        'ref',
        'value',
        'status',
    ];

    public static  function boot()
    {
        parent::boot(); 
        static::creating(function ($model) 
        {
            if(\Auth::check())
            {
                $model->plataforma_id  = session('plataforma_id') ?? Auth()->user()->plataforma_id ?? Plataforma::dominio()->pluck('id')[0] ?? null;
            }
        });
    }

    public function scopeAtivo($query)
    {
        return $query->where('status', 1);
    }

    public function scopePlataforma($query)
    {
        return $query->where('plataforma_id', session('plataforma_id') ?? Auth()->user()->plataforma_id ?? Plataforma::dominio()->pluck('id')[0] ?? null);
    }

    public static function verificaSeIntegrado($ref)
    {
        $check = FormularioIntegracao::plataforma()->ativo()->where('ref', $ref)->whereNotNull('value')->exists();

        return $check;
    } 

    public static function sendToEmail($contato)
    {
        $email = FormularioIntegracao::plataforma()->where('formulario_id', $contato->formulario_id)->where('ref','email')->pluck('value')[0] ?? null;

        if($email && $contato)
        {
            \Mail::to($email)->send(new \App\Mail\EmailFormulario($contato));
        }
    }

    public static function sendToContatosHubspot($contato)
    {
        $chaveAPI = FormularioIntegracao::plataforma()->where('formulario_id', $contato->formulario_id)->where('ref','hubspot')->pluck('value')[0] ?? null;

        if(is_null($chaveAPI))
        {
            return; // CHAVE API não setada.
        }

        $nome      = request('nome');
        $firstname = FormularioIntegracao::getPrimeiroSegundoNome($nome);
        $lastname  = FormularioIntegracao::getLastName($firstname, $nome);

        $client = Factory::createWithAccessToken($chaveAPI);

        $arr['firstname'] = $firstname;
        $arr['lastname'] = $lastname;

        $properties = FormularioHubspotProperty::plataforma()->where('formulario_id', $contato->formulario_id)->whereNotNull('property')->whereNotNull('input')->where('property','<>','nome')->where('property','<>','firstname')->get();
        foreach($properties as $property)
        {
            $input = $property->input;
            $arr[$property->property] = $contato->$input ?? $input;
        }

        $simplePublicObjectInputForCreate = new SimplePublicObjectInput([
            'properties'   => $arr,
            'associations' => null,
        ]);

        try {

            $apiResponse = $client->crm()->contacts()->basicApi()->create($simplePublicObjectInputForCreate);

        } catch (ApiException $e) {

            echo "Exception when calling basic_api->create: ", $e->getMessage();

            dd($arr);
        }
    }

    public static function getPrimeiroSegundoNome($nome)
    {
        $names = explode(' ', $nome);
        $nome = (isset($names[1])) ? $names[0]. ' ' .$names[1] : $names[0];

        if (array_key_exists(2, $names))
            $names2 = $names[2];
        else
            $names2 = null;

        if( FormularioIntegracao::endsWith($nome, 'de') || FormularioIntegracao::endsWith($nome, 'dos') || FormularioIntegracao::endsWith($nome, 'da') )
            $nome = $nome . ' ' . $names2 ?? null;

        return $nome;
    }

    public static function getLastName($firstname, $nome)
    {
        $lastname  = explode($firstname, $nome);
        $lastname  = $lastname[1];
        
        if($lastname == '')
        {
            $fullname  = explode(' ', $firstname);
            $firstname = $fullname[0];

            if (array_key_exists(1, $fullname))
                $lastname  = $fullname[1];
            else
                $lastname = ' ';
        }

        return $lastname;
    }
    
    public static function endsWith( $haystack, $needle ) {
        return $needle === "" || (substr($haystack, -strlen($needle)) === $needle);
    }

}

