<!doctype html>
<html lang="pt-br">
<head>

   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1">

   <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/uikit@3.5.9/dist/css/uikit.min.css" />
   <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
   <link rel="stylesheet" href="{{ asset('css/util.css') }}?v=3">
   <link rel="stylesheet" href="{{ asset('sites-proprios/faro/css/home.css') }}?v=3">

   <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
   
   <title>{{ $post->seo_title ?? $plataforma->nome }}</title>
   <meta name="robots" content="index, follow">
   <meta name="description" content="{{ $post->seo_description ?? null }}"/>
   <meta name="keywords" content="{{ $post->seo_keywords ?? null }}"/>
   <link rel="canonical" href="{{ Request::url() }}" />
   <link rel="shortcut icon" href="{{ $plataforma->favicon }}">
   <meta property="og:locale" content="pt_BR" />
   <meta property="og:type" content="website" />
   <meta property="og:title" content="{{ $post->seo_title ?? $plataforma->nome }}" />
   <meta property="og:description" content="{{ $post->seo_description ?? null }}" />
   <meta property="og:url" content="{{ Request::url() }}" />
   <meta property="og:site_name" content="{{ $plataforma->nome }}" />
   <meta property="og:image" content="{{ $plataforma->logotipo }}" />
   <meta name='author' content='WOZCODE | Pedro Henrique' />

</head>
<body class="bg-f4" id="inicio">

   @include('sites-proprios/faro/componentes/header')

   <main class="pt-3" id="mainPostBlog">

      <section class="container py-5">
         <div class="row">
            
            <div class="col-lg-8">

               <div class="uk-position-relative uk-visible-toggle uk-light" tabindex="-1" uk-slideshow="animation: fade; max-height: 385; max-height: 385;">

                  <ul class="uk-slideshow-items">
                      <li>
                          <img src="{{ $post->foto_capa }}" alt="Foto {{ $post->titulo }} - {{ $plataforma->nome }}" uk-cover>
                          <div class="uk-overlay uk-overlay-primary uk-position-bottom uk-text-center uk-transition-slide-bottom">
                              <h3 class="uk-margin-remove fs-xs-17">{{ $post->titulo }}</h3>
                          </div>
                          <div class="px-2 d-flex justify-content-between align-items-center uk-position-bottom text-left mb-0">
                              <h6 class="color-c9 fs-9">
                                @if($post->autor_id)
                                  por {{ \App\Models\Util::getPrimeiroSegundoNome(\App\Models\User::plataforma()->where('id', $post->autor_id)->pluck('nome')[0] ?? null) }}
                                @endif
                              </h6>
                              <h6 class="color-c9 fs-9">
                                <i class="fa fa-clock"></i>
                                {{ $post->created_at->diffForHumans() }}
                              </h6>
                          </div>
                      </li>
                  </ul>

                  <a class="p-lg-5 uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous uk-slideshow-item="previous"></a>
                  <a class="p-lg-5 uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-next uk-slideshow-item="next"></a>

               </div>

               <div class="py-5">
                  <div class="container">
                     {!! nl2br($post->descricao) !!}
                  </div>
               </div>

            </div>
         
            <div class="col-lg-4">
               
              <div class="d-flex align-items-center mb-3">
                <h1 class="title-blog my-0 color-red-dark text-shadow-1 fs-15 w-lg-75 w-xs-100" id="title-ultimas-noticias">ÚLTIMAS NOTÍCIAS</h1>
                <hr class="uk-divider border w-100 my-0 py-0">
              </div>

               @foreach($ultimos_posts as $post)
                  <a href="/blog/post/{{$post->slug}}/{{$post->id}}" class="d-flex align-items-center mb-2 hover-d-none color-51 color-faixa-verde">
                     <div>
                        <div class="uk-inline-clip uk-transition-toggle" style="width: 125px;height: 80px;" tabindex="0">
                           <img src="{{ $post->foto_capa }}" style="width: 125px;height: 80px;" class="uk-transition-scale-up uk-transition-opaque rounded-2 object-fit-cover" alt="{{ $post->nome }} | {{ $plataforma->nome }}">
                        </div>
                     </div>
                     <p class="font-roboto ms-2 mb-0 fs-15">
                        {{ $post->titulo }}
                     </p>
                  </a>
               @endforeach

            </div>

         </div>
      </section>

      <section id="newsletter" class="py-5 bg-gradient">
         <div class="container" id="contato">

            @if(session('success'))
               <div class='alert alert-success'><b>Sucesso!</b> {{ session('success') }}</div>
            @endif

            <div class="row justify-content-between align-items-center">

               <div class="col-lg-6">
                  <p class="mb-0 weight-700 fs-22 pe-lg-5 font-roboto">
                     <span class="color-2">Que tal receber nosso contato e ficar por dentro das nossas novidades?</span><span class="color-azul"> Cadastre-se!</span>
                  </p>
               </div>

               <form method="POST" class="col-lg-6 mt-3 mt-lg-0">
               @csrf

                  <input type="hidden" name="assunto" value="Newsletter">

                  <div class="d-lg-flex w-100">

                     <input type="text" name="nome" class="w-100 form-control lead-form rounded-left rounded-right-lg-0" placeholder="NOME" required>
                     
                     <div class="d-xs-none line-form"></div>                     

                     <input type="email" name="email" class="w-100 form-control lead-form rounded-left-lg-0 rounded-right-lg-0" placeholder="EMAIL" required>

                     <button type="submit" class="btn-news" name="sentContato" value="S">
                        Enviar
                     </button>

                  </div>

               </form>
            </div>

         </div>
      </section>

   </main>

   @include('sites-proprios/faro/componentes/footer')

   <a href="https://api.whatsapp.com/send?phone=55{{preg_replace('/\D/', '', $plataforma->whatsapp)}}&text=Olá, vim através do Site {{ $plataforma->nome }}" target="_blank" class="text-white fs-30">
       <div class="fixed-bottom right-0 me-3 mb-3 z-3">
           <div class="square-60 rounded-circle bg-wpp flex-center box-shadow-wpp">
               <i class="fab fa-whatsapp"></i>
           </div>
       </div>
   </a>

   <script src="https://cdn.jsdelivr.net/npm/uikit@3.5.9/dist/js/uikit.min.js"></script>
   <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-U1DAWAznBHeqEIlVSCgzq+c9gqGAJn5c/t99JyeKa9xxaYpSvHU5awsuZVVFIhvj" crossorigin="anonymous"></script>
   <script>

      window.onscroll = function() {myFunction()};

      var header = document.getElementById("menu");
      var sticky = header.offsetTop;

      function myFunction() {
        if (window.pageYOffset > sticky) {
          header.classList.add("fixed-top");
        } else {
          header.classList.remove("fixed-top");
        }
      }
   </script>

</body>
</html>