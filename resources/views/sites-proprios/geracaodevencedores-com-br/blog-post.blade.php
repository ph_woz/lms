<!doctype html>
<html lang="pt-br">
<head>

  <!-- Google tag (gtag.js) -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=G-MREB450HCT"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'G-MREB450HCT');
  </script>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/uikit@3.5.5/dist/css/uikit.min.css" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/5.0.0-alpha1/css/bootstrap.min.css" integrity="sha384-r4NyP46KrjDleawBgD5tp8Y7UzmLA05oM1iAEQ17CSuDqnUK2+k9luXQOfXJCJ4I" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('sites-proprios/geracaodevencedores-com-br/css/home.css') }}?v=8">
    <link rel="stylesheet" href="{{ asset('css/util.css') }}?v=2">

    <link rel="shortcut icon" href="{{ $plataforma->favicon ?? asset('sites-proprios/geracaodevencedores-com-br/images/favicon.ico') }}">
    <link href="{{ Request::url() }}" rel="canonical">

    <meta name="robots" content="index, follow">
    <title>{{ $seo->title ?? $plataforma->nome }}</title>
    <meta name="description" content="{{ $seo->description ?? null }}">
    <meta name="keywords" content="{{ $seo->keywords ?? null }}">

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <meta name='author' content='WOZCODE | Pedro Henrique' />

    <meta property="og:locale" content="pt_BR">
    <meta property="og:type" content="website">
    <meta property="og:image" content="{{ $plataforma->logotipoMenu ?? asset('sites-proprios/geracaodevencedores-com-br/images/logotipo.png') }}">
    <meta property="og:url" content="{{ Request::url() }}">
    <meta property="og:description" content="{{ $seo->description ?? null }}">
    <meta property="og:title" content="{{ $seo->title ?? $plataforma->nome }}">

    <style>
    
      #menuPlataforma nav { background: #355a7f; }
      .menu-link, .menu-link-color { color: #F5F5F5;; }
      .menu-link:hover { text-decoration: none; background: #1e3b57; color: #FFF!important; }
      .navbar-brand span, .activeNavLink { text-decoration: none; background: #1e3b57; color: #FFF!important; }

      #menu {
          width: 100%;
          box-sizing: border-box;
          padding: 9px 10px!important;
          position: fixed;
          z-index: 200;
          box-shadow: 0px -1px 8px 0px rgba(0, 0, 0, 0.60);
          background: transparent;
          transition: background-color 0.3s ease-in-out;
      }
      @media (min-width: 769px) 
      {
          #menu { height: 75px; }

          .menu-link { 
            margin: 0 10px;
            padding-top: 27.2px!important;
            padding-bottom: 27.2px!important;
            padding-left: 10px!important; 
            padding-right: 10px!important; 
          }
      }
      .menu-link {
          font-family: 'Open Sans',sans-serif;
          font-size: 13.5px;
          text-transform: uppercase;
          text-decoration: none;
          font-weight: 700;
          color: #F5F5F5!important;
          cursor: pointer;
          transition: all .2s;
          text-shadow: 1px 1px rgba(0, 0, 0, .1);
      }

      .btn-entrar { 
        background: #f5f5f5!important; 
        color: #355a7f!important; 
        font-family: 'Poppins', sans-serif; 
        font-weight: 600!important; 
        border-radius: 3px!important;
        padding: 12px 24px!important;
      }
          
      #footer { position: absolute!important; }
    
</style>

</head>
<body id="inicio">

   @include('sites-proprios/geracaodevencedores-com-br/componentes/header')

   <main class="py-5" id="mainPostBlog">
        
      <section class="container py-5">
         <div class="row pt-5">
            
            <div class="col-lg-8">

               <div class="uk-position-relative uk-visible-toggle uk-light" tabindex="-1" uk-slideshow="animation: fade; max-height: 385; max-height: 385;">

                  <ul class="uk-slideshow-items">
                      <li>
                          <img src="{{ $post->foto_capa }}" alt="Foto {{ $post->titulo }} - {{ $plataforma->nome }}" uk-cover>
                          <div class="uk-overlay uk-overlay-primary uk-position-bottom uk-text-center uk-transition-slide-bottom">
                              <h3 class="uk-margin-remove fs-xs-17">{{ $post->titulo }}</h3>
                          </div>
                          <div class="px-2 d-flex justify-content-between align-items-center uk-position-bottom text-left mb-0">
                              <h6 class="color-c9 fs-9">
                                @if($post->autor_id)
                                  por {{ \App\Models\Util::getPrimeiroSegundoNome(\App\Models\User::plataforma()->where('id', $post->autor_id)->pluck('nome')[0] ?? null) }}
                                @endif
                              </h6>
                              <h6 class="color-c9 fs-9">
                                <i class="fa fa-clock"></i>
                                {{ $post->created_at->diffForHumans() }}
                              </h6>
                          </div>
                      </li>
                  </ul>

                  <a class="p-lg-5 uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous uk-slideshow-item="previous"></a>
                  <a class="p-lg-5 uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-next uk-slideshow-item="next"></a>

               </div>

               <div class="py-5">
                  <div class="container">
                     {!! nl2br($post->descricao) !!}
                  </div>
               </div>

            </div>
         
            <div class="col-lg-4">
               
              <div class="d-flex align-items-center mb-3">
                <h1 class="title-blog my-0 color-red-dark text-shadow-1 fs-15 w-lg-75 w-xs-100" id="title-ultimas-noticias">ÚLTIMAS NOTÍCIAS</h1>
                <hr class="uk-divider border w-100 my-0 py-0">
              </div>

               @foreach($ultimos_posts as $post)
                  <a href="/blog/post/{{$post->slug}}/{{$post->id}}" class="d-flex align-items-center mb-2 hover-d-none color-51 color-faixa-verde">
                     <div>
                        <div class="uk-inline-clip uk-transition-toggle" style="width: 125px;height: 80px;" tabindex="0">
                           <img src="{{ $post->foto_capa }}" style="width: 125px;height: 80px;" class="uk-transition-scale-up uk-transition-opaque rounded-2 object-fit-cover" alt="{{ $post->nome }} | {{ $plataforma->nome }}">
                        </div>
                     </div>
                     <p class="font-roboto ms-2 mb-0 fs-15">
                        {{ $post->titulo }}
                     </p>
                  </a>
               @endforeach

            </div>

         </div>
      </section>

   </main>

   @include('sites-proprios/geracaodevencedores-com-br/componentes/footer')

   <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
   <script src="https://cdn.jsdelivr.net/npm/uikit@3.5.5/dist/js/uikit.min.js"></script>
   <script src="https://kit.fontawesome.com/2f91eaaef4.js" crossorigin="anonymous"></script>  
   <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>    
   <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
   <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

   <script>

      window.onscroll = function() {myFunction()};

      var header = document.getElementById("menu");
      var sticky = header.offsetTop;

      function myFunction() {
        if (window.pageYOffset > sticky) {
          header.classList.add("fixed-top");
          header.classList.add("mudaCor");
          $(".nav-link").addClass("nav-link-after-hover");
        } else {
          header.classList.remove("fixed-top");
          header.classList.remove("mudaCor");
          $(".nav-link").removeClass("nav-link-after-hover");
        }
      }

   </script>
</body>
</html>