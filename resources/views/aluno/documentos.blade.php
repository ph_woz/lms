@extends('layouts.aluno')
@section('css')
<style>
	label { color: #F5F5F5; }
	.color-anexo { color: #2e98e3; }
</style>
@endsection
@section('content')

@section('seo_title', 'Acadêmico')
<div class="container-fluid px-xl-5 mt-5">

	<div class="mb-4 p-5 rounded-10 box-shadow bg-14">

		<form method="POST" enctype="multipart/form-data">
		@csrf

			@if($formulario->texto)
				<div id="formDescricao">
					{!! nl2br($formulario->texto) !!}
				</div>

				<hr/>
			@endif

		    <p class="font-nunito weight-700 fs-17 text-shadow-1">
		        @if($countJaRespondidas >= count($perguntas))
		            <span class="text-success">
		                <i class="fas fa-check-circle"></i>
		                Documentação Recebida
		            </span>
		        @else
		            <span class="text-warning">
		                Atenção <i class="fas fa-exclamation"></i><br>Há documentação pendente de envio.
		            </span>
		        @endif
		    </p>

	        <div class="row mt-4">

	            @foreach($perguntas as $pergunta)
	                
		            <div class="mb-4 colIndex{{$loop->index}} col-lg-6">
		                @php
		                    $anexo = \App\Models\Documento::plataforma()->where('user_id', \Auth::id())->where('nome', $pergunta->pergunta)->pluck('link')[0] ?? null;
		                @endphp
		                <label class="d-block color-alunoLight-black mb-0">
		                    {{ $pergunta->pergunta }}
		                </label>
		                <input type="file" name="files[{{$pergunta->pergunta}}]" class="file-form" @if($pergunta->obrigatorio == 'S' && $anexo == null) required @endif>
		                @if($anexo)
		                	<div class="mt-1">
			                    <a href="{{ $anexo }}" class="font-nunito fs-15 weight-600 color-anexo" target="_blank">
			                        <i class="fas fa-file"></i>
			                        Visualizar arquivo já em anexo
		                	    </a>
		                	</div>
		                @else
		                    <label class="text-warning mt-1">
		                        Ainda não enviado
		                    </label>
		                @endif
		            </div>
	                                        
	            @endforeach

	        </div>

	        <button type="submit" name="sent" value="ok" id="formBtnEnviar" class="mt-4 btn-suporte-enviar px-4 py-2">
	        	Enviar
		    </button>

		</form>

	</div>

</div>
@endsection