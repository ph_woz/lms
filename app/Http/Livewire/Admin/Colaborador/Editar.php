<?php

namespace App\Http\Livewire\Admin\Colaborador;

use Livewire\Component;
use App\Models\User;
use App\Models\PlataformaModulo;
use App\Models\AclPlataformaModuloUser;
use App\Models\UserEndereco;
use App\Models\UserCategoria;
use App\Models\Categoria;
use App\Models\Plataforma;
use App\Models\Unidade;
use App\Models\FluxoCaixa;
use DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Http;

class Editar extends Component
{
    public $user;
    public $endereco;

    public $nome;
    public $email;
    public $new_password;
    public $status = 0;
    public $unidade_id;
    public $restringir_unidade = 'N';
    public $cpf;
    public $rg;
    public $matricula;
    public $data_nascimento;
    public $celular;
    public $telefone;
    public $genero;
    public $profissao;
    public $naturalidade;
    public $obs;
    public $descricao;

    public $endereco_cep;
    public $endereco_rua;
    public $endereco_numero;
    public $endereco_bairro;
    public $endereco_cidade;
    public $endereco_estado;
    public $endereco_complemento;
    public $endereco_ponto_referencia;

    public $categoriasSelected = [];

    public $modulos = [];
    public $acl = [];


    public function mount($user_id)
    {
        $this->user = User::plataforma()->find($user_id);
        $this->endereco = UserEndereco::plataforma()->where('user_id', $this->user->id)->first();
        $this->categoriasSelected = UserCategoria::plataforma()->where('user_id', $this->user->id)->get()->pluck('categoria_id')->toArray();


        $responsavel_id = Plataforma::plataforma()->pluck('responsavel_id')[0] ?? null;

        $this->modulos = \DB::table('plataformas_modulos')
            ->join('acl_plataformas_modulos_users', 'plataformas_modulos.id', 'acl_plataformas_modulos_users.modulo_id')
            ->where('plataformas_modulos.status', 0)
            ->where('plataformas_modulos.plataforma_id', session('plataforma_id'))
            ->where('acl_plataformas_modulos_users.user_id', \Auth::id());

        if($responsavel_id != \Auth::id())
            $this->modulos = $this->modulos->where('acl_plataformas_modulos_users.nivel', '>', 0);

        $this->modulos = $this->modulos->select('plataformas_modulos.id','plataformas_modulos.url','plataformas_modulos.texto')->get();


        foreach($this->modulos as $modulo)
        {
            $nivelAtual = AclPlataformaModuloUser::plataforma()->where('user_id', $this->user->id)->where('modulo_id', $modulo->id)->pluck('nivel')[0] ?? 0;
            $this->acl[$modulo->id] = $nivelAtual;
        }

        $this->nome                      = $this->user->nome;
        $this->email                     = $this->user->email;
        $this->status                    = $this->user->status;
        $this->unidade_id                = $this->user->unidade_id;
        $this->restringir_unidade        = $this->user->restringir_unidade;
        $this->cpf                       = $this->user->cpf;
        $this->rg                        = $this->user->rg;
        $this->matricula                 = $this->user->matricula;
        $this->data_nascimento           = $this->user->data_nascimento;
        $this->celular                   = $this->user->celular;
        $this->telefone                  = $this->user->telefone;
        $this->genero                    = $this->user->genero;
        $this->profissao                 = $this->user->profissao;
        $this->naturalidade              = $this->user->naturalidade;
        $this->obs                       = $this->user->obs;
        $this->descricao                 = $this->user->descricao;

        $this->endereco_cep              = $this->endereco->cep              ?? null;
        $this->endereco_rua              = $this->endereco->rua              ?? null;
        $this->endereco_numero           = $this->endereco->numero           ?? null;
        $this->endereco_bairro           = $this->endereco->bairro           ?? null;
        $this->endereco_cidade           = $this->endereco->cidade           ?? null;
        $this->endereco_estado           = $this->endereco->estado           ?? null;
        $this->endereco_complemento      = $this->endereco->complemento      ?? null;
        $this->endereco_ponto_referencia = $this->endereco->ponto_referencia ?? null;

        if(\Auth::user()->restringir_unidade == 'S')
        {
            $this->restringir_unidade = 'S';
        }
    }

    public function editar()
    {
        $checkEmailExists = User::plataforma()->where('email', $this->email)->where('id','<>', $this->user->id)->exists();
        
        if($checkEmailExists == true)
        {
            session()->flash('danger-livewire', 'Já existe um colaborador cadastrado com este Email');
            $this->dispatchBrowserEvent('toast');
            return;
        }

        if($this->matricula)
        {
            $checkMatriculaExists = User::plataforma()->where('matricula', $this->matricula)->where('id','<>', $this->user->id)->exists();

            if($checkMatriculaExists == true)
            {
                session()->flash('danger-livewire', 'Já existe um colaborador cadastrado com esta Matrícula');
                $this->dispatchBrowserEvent('toast');
                return;
            }
        }

        if($this->cpf)
        {            
            $checkCPFExists = User::plataforma()->where('cpf', $this->cpf)->where('id','<>', $this->user->id)->exists();

            if($checkCPFExists == true)
            {
                session()->flash('danger-livewire', 'Já existe um colaborador cadastrado com este CPF');
                $this->dispatchBrowserEvent('toast');
                return;
            }
        }

        if($this->user->nome != $this->nome)
        {
            if(FluxoCaixa::checkActive())
            {
                FluxoCaixa::plataforma()->where('cadastrante_id', $this->user->id)->update(['cadastrante_nome' => $this->nome]);
                // FluxoCaixa::plataforma()->where('editor_id', $this->user->id)->update(['editor_nome' => $this->nome]);
            }
        }

        $this->user->update([
            'plataforma_id'      => $this->user->plataforma_id,
            'nome'               => $this->nome,
            'email'              => $this->email,
            'status'             => $this->status,
            'unidade_id'         => (int)$this->unidade_id,
            'restringir_unidade' => $this->restringir_unidade,
            'cpf'                => $this->cpf,
            'rg'                 => $this->rg,
            'matricula'          => $this->matricula,
            'data_nascimento'    => $this->data_nascimento,
            'celular'            => $this->celular,
            'telefone'           => $this->telefone,
            'genero'             => $this->genero,
            'profissao'          => $this->profissao,
            'naturalidade'       => $this->naturalidade,
            'obs'                => $this->obs,
            'descricao'          => $this->descricao,
        ]);


        if($this->new_password)
        {
            $this->user->update(['password' => Hash::make($this->new_password)]);
        }
        
        $endereco = UserEndereco::plataforma()->where('user_id', $this->user->id)->first();

        if(is_null($endereco))
        {
            if($this->endereco_cep != null || $this->endereco_rua != null || $this->endereco_numero != null || $this->endereco_bairro != null || $this->endereco_cidade != null || $this->endereco_estado != null || $this->endereco_complemento != null || $this->endereco_ponto_referencia != null)
                UserEndereco::create([
                    'user_id'          => $this->user->id,
                    'cep'              => $this->endereco_cep,
                    'rua'              => $this->endereco_rua,
                    'numero'           => $this->endereco_numero,
                    'bairro'           => $this->endereco_bairro,
                    'cidade'           => $this->endereco_cidade,
                    'estado'           => $this->endereco_estado,
                    'complemento'      => $this->endereco_complemento,
                    'ponto_referencia' => $this->endereco_ponto_referencia,
                ]);

        } else {

            $endereco->update([
                'user_id'          => $this->user->id,
                'cep'              => $this->endereco_cep,
                'rua'              => $this->endereco_rua,
                'numero'           => $this->endereco_numero,
                'bairro'           => $this->endereco_bairro,
                'cidade'           => $this->endereco_cidade,
                'estado'           => $this->endereco_estado,
                'complemento'      => $this->endereco_complemento,
                'ponto_referencia' => $this->endereco_ponto_referencia,
            ]);
        }


        if($this->user->id == \Auth::id())
        {
            if($this->user->restringir_unidade == 'S' && $this->user->unidade_id != null)
            {
                $nome = \App\Models\Unidade::plataforma()->where('id', $this->user->unidade_id)->select('nome')->pluck('nome')[0] ?? 'Não identificado';
                session(['nomeUnidade' => 'Polo: ' . $nome ]);
            
            } else {
            
                session(['nomeUnidade' => null]);
            }
        }

        $this->syncACL();
        $this->syncCategorias($this->user->id);

        session()->flash('success-livewire', 'Colaborador atualizado com êxito.');

        $this->dispatchBrowserEvent('toast');
    }

    public function syncACL()
    {
        AclPlataformaModuloUser::plataforma()->where('user_id', $this->user->id)->delete();

        foreach($this->acl as $modulo_id => $nivel)
        {
            AclPlataformaModuloUser::create([
                'user_id'   => $this->user->id,
                'modulo_id' => $modulo_id,
                'nivel'     => $nivel,
            ]);
        }
    }

    public function syncCategorias($user_id)
    {
        UserCategoria::plataforma()->where('user_id', $user_id)->delete();

        if($this->categoriasSelected == null)
            return;

        foreach($this->categoriasSelected as $categoria_id)
        {
            $countDuplicate = UserCategoria::plataforma()->where('user_id', $user_id)->where('categoria_id', $categoria_id)->count();
            
            if($countDuplicate <= 0)
                UserCategoria::create([
                    'plataforma_id' => session('plataforma_id'),
                    'user_id'       => $user_id, 
                    'categoria_id'  => $categoria_id,
                ]);
        }
    }

    public function setEndereco()
    {
        // Retorna endereço do ViaCEP.
        $response = Http::get("http://viacep.com.br/ws/{$this->endereco_cep}/json/");
        $viacep = $response->json();
        $this->endereco_rua    = $viacep['logradouro'] ?? null;
        $this->endereco_bairro = $viacep['bairro'] ?? null;
        $this->endereco_cidade = $viacep['localidade'] ?? null;
        $this->endereco_estado = $viacep['uf'] ?? null;
    }

    public function render()
    {
        $categorias = Categoria::plataforma()->ativo()->tipo('colaborador')->orderBy('nome')->get();
        $unidades   = Unidade::plataforma()->ativo()->unidade()->orderBy('nome')->get();

        return view('livewire.admin.colaborador.editar', ['categorias' => $categorias, 'unidades' => $unidades]);
    }
}
