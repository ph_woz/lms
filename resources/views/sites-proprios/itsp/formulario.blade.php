<!doctype html>
<html lang="pt-br">
<head>

  <!-- Google tag (gtag.js) -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=G-MREB450HCT"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'G-MREB450HCT');
  </script>
  
   <meta charset="utf-8">
   <meta name="viewport" content="width=device-width, initial-scale=1">
   
   <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/uikit@3.5.9/dist/css/uikit.min.css" />
   <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
   <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
   <link rel="stylesheet" href="{{ asset('css/util.css') }}?v=3">
   <link rel="stylesheet" href="{{ asset('sites-proprios/cedetep/css/home.css') }}">

   <title>{{ $seo->seo_title ?? $plataforma->nome }}</title>
   <meta name="robots" content="index, follow">
   <meta name="description" content="{{ $seo->seo_description ?? null }}"/>
   <meta name="keywords" content="{{ $seo->seo_keywords ?? null }}"/>
   <link rel="canonical" href="{{ Request::url() }}" />
   <link rel="shortcut icon" href="{{ $plataforma->favicon }}">
   <meta property="og:locale" content="pt_BR" />
   <meta property="og:type" content="website" />
   <meta property="og:title" content="{{ $seo->seo_title ?? $plataforma->nome }}" />
   <meta property="og:description" content="{{ $seo->seo_description ?? null }}" />
   <meta property="og:url" content="{{ Request::url() }}" />
   <meta property="og:site_name" content="{{ $plataforma->nome }}" />
   <meta property="og:image" content="{{ $plataforma->logotipo }}" />
   <meta name='author' content='WOZCODE | Pedro Henrique' />

</head>
<body id="inicio" style="background-image: url({{ asset('images/bg-login.png') }})">

   <header>
      
      <div class="sub-header container-fluid px-xl-7 py-2-3">
         <div class="row align-items-center">
            <div class="col-6 col-lg-4">

               <a href="https://api.whatsapp.com/send?phone=55{{preg_replace('/\D/', '', $plataforma->whatsapp)}}&text=Olá, vim através do Site {{ $plataforma->nome }}." target="_blank" class="btn-submenu">
                  <i class="fab fa-whatsapp fs-17"></i>
                  <span class="d-xs-none"> 
                    Matricule-se pelo WhatsApp
                  </span>
                  <span class="d-lg-none ml-1">
                    {{ $plataforma->whatsapp }}
                  </span>
               </a>

               <a href="tel:+55{{preg_replace('/\D/', '', $plataforma->telefone)}}" class="d-xs-none btn-submenu">
                  <i class="fa fa-phone fs-17"></i>
                  {{ $plataforma->telefone }}
               </a>

            </div>

            <div class="col-6 col-lg-8" align="right">
               
               <a href="/sobre-nos" class="btn-submenu d-xs-none">
                  Institucional
               </a>

               <a href="https://itspensino.com.br/f/trabalhe-conosco/49/t/652" target="_blank" class="btn-submenu d-xs-none">
                  Trabalhe Conosco
               </a>

               <a href="/login" class="btn-submenu btnPortalAluno">
                  Portal do Aluno
               </a>

            </div>

         </div>
      </div>

      <nav class="navbar navbar-expand-lg bg-white box-shadow" id="menu">
         <div class="container-fluid px-xl-7">
             <a class="navbar-brand" href="/#inicio">
               <img src="{{ $plataforma->logotipo }}" width="75" alt="Logotipo {{ $plataforma->nome }}">
             </a>
             <button class="navbar-toggler outline-0 border-0 text-dark" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
               <span class="fa fa-bars"></span>
             </button>
             <div class="collapse navbar-collapse" id="navbarSupportedContent">
                  <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
                     <li class="nav-item">
                        <a class="nav-link" href="/">HOME</a>
                     </li>
                     <li class="nav-item">
                        <a class="nav-link" href="/#cursos">CURSOS</a>
                     </li>
                     <li class="nav-item">
                        <a class="nav-link" href="/#contato">
                           CONTATO
                        </a>
                     </li>
                     <li class="nav-item ms-lg-4 mt-3 mt-lg-0">
                       <a class="nav-link btnAreaAluno animation-grow d-inline-block" href="https://itspensino.com.br/f/formulario-de-matricula/47/t/650" target="_blank">
                          <i class="fas fa-graduation-cap mr-1"></i>
                          MATRICULE-SE
                       </a>
                     </li>
                 </ul>
             </div>
         </div>
      </nav>

   </header>

   <main>

      <form method="POST" class="container-fluid px-xl-5 pb-5 pt-3" id="formContainer">
      @csrf

        <section class="bg-white p-lg-5 p-4 my-5 rounded-2 shadow">

          <div id="formDescricao">
            {!! nl2br($formulario->texto) !!}
          </div>

          <hr/>

              <div class="row mt-4">

                  @foreach($perguntas as $pergunta)
                      
                      @if($pergunta->tipo_input == 'select')
                          <div class="mb-4 col-lg-3">
                              <label>{{ $pergunta->pergunta }}</label>
                              <select name="{{ $pergunta->validation ?? 'respostas['.$pergunta->id.']' }}" class="{{ $pergunta->validation ?? null }} select-form" @if($pergunta->obrigatorio == 'S') required @endif>
                                  <option value="" selected disabled>Selecione</option>
                                  @foreach(unserialize($pergunta->options) as $option)
                                      <option value="{{ $option }}" @if(old($pergunta->validation) == $option) selected @endif>{{ $option }}</option>
                                  @endforeach
                              </select> 
                          </div>
                      @endif
                      
                      @if($pergunta->tipo_input == 'text')
                      <div class="mb-4 col-lg-3">
                          <label>{{ $pergunta->pergunta }}</label>
                          <input type="text" name="{{ $pergunta->validation ?? 'respostas['.$pergunta->id.']' }}" class="{{ $pergunta->validation ?? null }} form-control p-form" @if($pergunta->obrigatorio == 'S') required @endif>
                      </div>
                      @endif
                      
                      @if($pergunta->tipo_input == 'radio')
                      <div class="mb-4 col-12">
                          <label class="d-block mb-1">{{ $pergunta->pergunta }}</label>
                          @foreach(unserialize($pergunta->options) as $option)
                              <label for="opt{{$loop->index}}p{{$pergunta->id}}" class="mb-1 d-flex align-items-center">
                                  <input type="radio" class="square-15" id="opt{{$loop->index}}p{{$pergunta->id}}" name="{{ $pergunta->validation ?? 'respostas['.$pergunta->id.']' }}" value="{{ $option }}" @if($pergunta->obrigatorio == 'S') required @endif>
                                  <span class="ms-2">{!! $option !!}</span>
                              </label>
                          @endforeach
                      </div>
                      @endif

                      @if($pergunta->tipo_input == 'checkbox')
                      <div class="mb-4 col-12">
                          <label class="d-block mb-1">{{ $pergunta->pergunta }}</label>
                          @foreach(unserialize($pergunta->options) as $option)
                              <label for="opt{{$loop->index}}p{{$pergunta->id}}" class="mb-1 d-flex align-items-center">
                                  <input type="checkbox" class="square-15" id="opt{{$loop->index}}p{{$pergunta->id}}" name="{{ $pergunta->validation ?? 'respostas['.$pergunta->id.']' }}" value="{{ $option }}">
                                  <span class="ms-2">{!! $option !!}</span>
                              </label>
                          @endforeach
                      </div>
                      @endif

                  @endforeach

              </div>

              <input type="text" name="check" class="h-0 border-0 outline-0 float-end">

              <button type="submit" name="sent" value="ok" id="formBtnEnviar" class="btnAreaAluno weight-600 fs-17 ls-05 d-inline-block rounded-3 box-shadow">
                Enviar
              </button>

        </section>

      </form>

      <div class="modal fade" id="modalCallback" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalCenterTitle">Inscrição enviada com sucesso!</h5>
            </div>
            <div class="modal-body">
              
                  {!! $formulario->texto_retorno !!}

            </div>
            <div class="modal-footer">
              <a href="{{ $formulario->link_retorno }}" class="btn btn-primary weight-600 shadow px-4 py-2">
                  {{ $formulario->texto_botao_retorno ?? 'Retornar' }}
              </a>
            </div>
          </div>
        </div>
      </div>


   </main>

   <footer id="contato" style="background-image: url('{{ asset('sites-proprios/cedetep/images/footer.jpg') }}'); background-repeat: no-repeat; background-size: cover; background-position: center;">
      <div class="container-fluid px-xl-8 pt-5 ps-lg-5" style="margin-top: -50px;">

         <div class="row align-items-end py-lg-4 px-3 px-md-0">

            <div class="col-lg-9">

               <h1 class="fs-35 font-montserrat text-light weight-800 text-uppercase">FALE CONOSCO</h1>
               <div class="my-4 w-80px h-2px bg-line rounded"></div>

               <img src="{{ $plataforma->logotipo }}" class="img-fluid" width="100">

               <div class="mt-2">                 

                  <div class="mb-3">
                     <span class="text-light font-poppins fs-14">
                        <i class="color-d4 fa fa-envelope"></i> 
                        {{ $plataforma->email ?? 'meuemail@exemlo.com' }}
                     </span>
                  </div>
                  <div class="mb-3">
                     <a href="https://api.whatsapp.com/send?phone=55{{preg_replace('/\D/', '', $plataforma->whatsapp)}}&text=Olá, vim através do Site {{ $plataforma->nome }}" class="text-light font-poppins fs-14">
                        <i class="color-d4 fab fa-whatsapp"></i> 
                        {{ $plataforma->whatsapp ?? '(00) 00000-0000' }}
                     </a>
                  </div>
                  <div class="mb-3">
                     <span class="text-light font-poppins fs-14">
                        <i class="color-d4 fa fa-clock"></i> 
                        Horário de Funcionamento: {{ $plataforma->horario ?? 'Segunda à sexta-feira das 09:00 às 18:00' }}
                     </span>
                  </div> 
                  <div class="mb-3">
                    <address>
                      <a href="https://www.google.com/maps/place/CEDETEP+Escola+T%C3%A9cnica+-+Pindamonhangaba/@-22.924224,-45.468257,14z/data=!4m5!3m4!1s0x0:0x70581a8d7cfa2dad!8m2!3d-22.9242244!4d-45.4682567?hl=pt-BR" target="_blank" class="btnEnderecoSiteCedetep text-light font-poppins fs-14">
                           <i class="color-d4 fas fa-map-marker-alt"></i>
                           {{ $plataforma->endereco ?? 'Pintangueiras - RJ. Rua Nossa Senhora das Graças, 16. 21932-230.' }}
                      </a>
                     </address>
                  </div>

                  <div class="mt-4 d-flex">

                     <a href="{{ $plataforma->facebook }}" target="_blank" class="me-2 hover-d-none">
                        <div class="square-40 rounded-circle social-icon social-icon-facebook flex-center">
                           <i class="text-white fab fa-facebook fs-20"></i>
                        </div>
                     </a>
                     <a href="{{ $plataforma->instagram }}" target="_blank" class="hover-d-none">
                        <div class="square-40 rounded-circle social-icon social-icon-instagram flex-center">
                           <i class="text-white fab fa-instagram fs-20"></i>
                        </div>
                     </a>

                  </div>

               </div>

            </div>

            <div class="col-lg-3 mt-5 mt-lg-0">

               <div class="mb-3">
                  <a href="https://itspensino.com.br/f/formulario-de-matricula/47/t/650" target="_blank" class="btn btn-primary weight-600 w-100 py-2-5 font-poppins hover-d-none">
                     MATRICULE-SE JÁ!
                  </a>
               </div>
               <div class="mb-3">
                  <a href="/login" class="btn btn-warning weight-600 w-100 py-2-5 font-poppins hover-d-none">
                     ÁREA DO ALUNO
                  </a>
               </div>
               <div class="mb-3">
                  <a href="https://itspensino.com.br/f/torne-se-parceiro/48/t/651" target="_blank" class="btn btn-light weight-600 w-100 py-2-5 font-poppins hover-d-none">
                     TORNE-SE PARCEIRO
                  </a>
               </div>
               <div class="mb-3">
                  <a href="https://api.whatsapp.com/send?phone=55{{preg_replace('/\D/', '', $plataforma->whatsapp)}}&text=Olá, vim através do Site {{ $plataforma->nome }}" class="btn btn-success bg-wpp text-shadow-1 weight-600 w-100 py-2-5 font-poppins hover-d-none">
                     ENTRAR EM CONTATO
                  </a>
               </div>

            </div>

         </div>

         <hr class="mt-4 border border-light" />

         <div class="pb-3 d-lg-flex justify-content-between align-items-center">

            <p class="text-xs-center mb-1 mb-lg-0 p-0 text-light font-poppins fs-11 ls-05">
               {{ $plataforma->nome }} © 2022 • Todos os direitos reservados • CNPJ: {{ $plataforma->cnpj }}
            </p>

            <p class="text-xs-center m-0 p-0 text-light font-poppins fs-11 ls-05">
               Desenvolvido por <a href="https://wozcode.com" target="_blank" class="text-light text-d-none fs-11 weight-600 text-shadow-1">WOZCODE</a>
            </p>

         </div>

      </div>
   </footer>

   <a href="https://api.whatsapp.com/send?phone=55{{preg_replace('/\D/', '', $plataforma->whatsapp)}}&text=Olá, vim através do Site {{ $plataforma->nome }}" target="_blank" class="text-white fs-30">
       <div class="fixed-bottom right-0 me-3 mb-3 z-3">
           <div class="square-60 rounded-circle bg-wpp flex-center box-shadow-wpp">
               <i class="fab fa-whatsapp"></i>
           </div>
       </div>
   </a>

   <script src="https://kit.fontawesome.com/2f91eaaef4.js" crossorigin="anonymous"></script>  
   <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-U1DAWAznBHeqEIlVSCgzq+c9gqGAJn5c/t99JyeKa9xxaYpSvHU5awsuZVVFIhvj" crossorigin="anonymous"></script>


    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.js"></script>

    @if(session('success') && $formulario->texto_retorno != null)
    <script>
        $(document).ready(function() {
            $("#modalCallback").modal('show');
        });
    </script>
    @endif

    <script>

        $(document).ready(function() {

            function limpa_formulário_cep() {

                $(".logradouro").val("");
                $(".bairro").val("");
                $(".cidade").val("");
                $(".estado").val("");
            }
            

            $(".cep").keyup(function() {

                var cep = $(this).val().replace(/\D/g, '');

                if (cep != "" && cep.length >= 8) {

                    var validacep = /^[0-9]{8}$/;

                    if(validacep.test(cep)) {

                        $(".logradouro").val("...");
                        $(".bairro").val("...");
                        $(".cidade").val("...");
                        $(".estado").val("...");

                        $.getJSON("https://viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {

                            if (!("erro" in dados)) {
                                $(".logradouro").val(dados.logradouro);
                                $(".bairro").val(dados.bairro);
                                $(".cidade").val(dados.localidade);
                                $(".estado").val(dados.uf);
                            } 
                            else {
                               
                                limpa_formulário_cep();
                            }
                        });
                    } 
                    else {
                        
                        limpa_formulário_cep();
                        
                    }
                }
                else {
                  
                    limpa_formulário_cep();
                }
            });
        });

    </script>

    <script>
      $(".email").attr('type','email');
      $(".password").attr('type','password');
        $(".cpf").mask('000.000.000-00').attr('minlength','14');
        $(".data_nascimento").mask('00/00/0000');
        $(".cep").mask('00000-000');
        $(".estado, .uf").attr('minlength','2').attr('maxlength','2');
        $(".numero").mask('000000000');
        $(".password").val('');

        $(document).on("focus", ".celular, .telefone", function() { 
          jQuery(this)
              .mask("(99) 9999-99999")
              .change(function (event) {  
                  target = (event.currentTarget) ? event.currentTarget : event.srcElement;  
                  phone = target.value.replace(/\D/g, '');
                  element = $(target);  
                  element.unmask();  
                  if(phone.length > 10) {  
                      element.mask("(99) 99999-9999");  
                  } else {  
                      element.mask("(99) 9999-9999?9");  
                  }  
          });
        });

        $(document).ready(function() {

            function limpa_formulário_cep() {

                $(".logradouro").val("");
                $(".bairro").val("");
                $(".cidade").val("");
                $(".estado").val("");
            }
            

            $(".cep").keyup(function() {

                var cep = $(this).val().replace(/\D/g, '');

                if (cep != "" && cep.length >= 8) {

                    var validacep = /^[0-9]{8}$/;

                    if(validacep.test(cep)) {

                        $(".logradouro").val("...");
                        $(".bairro").val("...");
                        $(".cidade").val("...");
                        $(".estado").val("...");

                        $.getJSON("https://viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {

                            if (!("erro" in dados)) {
                                $(".logradouro").val(dados.logradouro);
                                $(".bairro").val(dados.bairro);
                                $(".cidade").val(dados.localidade);
                                $(".estado").val(dados.uf);
                            } 
                            else {
                               
                                limpa_formulário_cep();
                            }
                        });
                    } 
                    else {
                        
                        limpa_formulário_cep();
                        
                    }
                }
                else {
                  
                    limpa_formulário_cep();
                }
            });
        });

        $('.estado, .uf').keyup(function(){
            $(this).val($(this).val().toUpperCase());
        });

    </script>

    @if(session()->has('success'))
        <div id="box-toast-success" class="boxToast">
            <div id="alertSuccess" class="toast-success">
                <p class="mt-2 text-white font-inter fs-14">
                    <span class="weight-800">Sucesso!&nbsp;</span>{{ session('success') }}
                </p>
            </div>
        </div>
        <script>
            setTimeout(function() {

                document.getElementById('box-toast-success').style.display = 'none';

            }, 2500);
        </script>
    @endif

    @if(session()->has('danger'))
        <div id="box-toast-danger" class="boxToast">
            <div id="alertDanger" class="toast-danger">
                <p class="mt-2 text-white font-inter fs-14">
                    <span class="weight-800">Ops!&nbsp;</span>{{ session('danger') }}
                </p>
            </div>
        </div>
        <script>
            setTimeout(function() {

                document.getElementById('box-toast-danger').style.display  = 'none';

            }, 2500);
        </script>
    @endif

</body>
</html>