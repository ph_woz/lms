<?php

namespace App\Http\Livewire\Admin\Documento;

use Livewire\WithPagination;
use Livewire\Component;
use App\Models\Documento;
use App\Models\Categoria;
use App\Models\DocumentoCategoria;

class Lista extends Component
{
    use WithPagination;

    protected $paginationTheme = 'bootstrap';

    public $search;
    public $arquivado = 'N';
    public $publicado = 'S';
    public $ordem;
    public $categoriasSelected;

    protected $queryString = ['search','arquivado','publicado','ordem','categoriasSelected'];

    public function render()
    {
        $documentos = Documento::plataforma()->whereNull('user_id');

        if($this->categoriasSelected != null)
        {
            $documentosIds = DocumentoCategoria::plataforma()->whereIn('categoria_id', $this->categoriasSelected ?? array())->get()->pluck('documento_id')->toArray();
            $documentos = $documentos->whereIn('id', $documentosIds);
        }

        if($this->search)
        {
            $documentos->where(function ($query)
            {
                $query
                    ->orWhere('nome', 'like', '%'.$this->search.'%')
                    ->orWhere('referencia', 'like', '%'.$this->search.'%');
            });
        }

        $documentos = $documentos->where('arquivado', $this->arquivado);
        $documentos = $documentos->where('publicar', $this->publicado);


        $countDocumentos = $documentos->count();


        if($this->ordem == 'desc' || $this->ordem == 'asc')
        {
            $documentos = $documentos->orderBy('id', $this->ordem);

        } else {

            $documentos = $documentos->orderBy('nome');
        }
        

        $documentos = $documentos->paginate(10);


        $categorias = Categoria::plataforma()->ativo()->tipo('documento')->orderBy('nome')->get();

        return view('livewire.admin.documento.lista', [
            'documentos'      => $documentos,
            'countDocumentos' => $countDocumentos,
            'categorias'      => $categorias,
        ]);
    }
}
