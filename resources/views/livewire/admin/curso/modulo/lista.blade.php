<div>

    <nav aria-label="breadcrumb" class="mb-4-5 p-3 bg-db rounded ashadow border">
        <ol class="breadcrumb p-0 m-0">
            <li class="breadcrumb-item"><a href="{{ route('admin.curso.lista') }}" class="color-blue-info-link">Cursos</a></li>
            <li class="breadcrumb-item"><a href="{{ route('admin.curso.info', ['id' => $curso->id]) }}" class="color-blue-info-link">{{ $curso->referencia }}</a></li>
            <li class="breadcrumb-item text-dark" aria-current="page">Módulos</li>
        </ol>
    </nav>

    <form>

        <div class="d-lg-flex mb-3">

            <input wire:model="search" type="search" class="form-control p-form" placeholder="Buscar">

            <select wire:model="status" class="ms-lg-3 mt-3 mt-lg-0 select-form w-lg-40">
                <option value="0">Ativos</option>
                <option value="1">Desativados</option>
            </select>

        </div>

    </form>

    <div class="mt-5 d-flex justify-content-between">

        <div class="align-self-end mb-2">
            <span class="resultados-encontrados">
                {{ count($modulos) }} resultados de {{ $countModulos }}.
            </span>
        </div>

        <div class="mb-3">
            <a href="#ModalCadastrarModulo" data-bs-toggle="modal" class="btn btn-primary aclNivel2">
                + Cadastar novo Módulo
            </a>
        </div>

    </div>

    <div class="row">
        <div class="col-12">

            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col">Nome</th>
                                    <th scope="col">
                                        <span class="float-end">
                                            Ações
                                        </span>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($modulos as $modulo)
                                    <tr>
                                        <td>{{ $modulo->nome }}</td>
                                        <td>
                                            <span class="float-end">

                                                <a href="{{ route('admin.curso.modulo.aula.lista', ['curso_id' => $curso_id, 'modulo_id' => $modulo->id]) }}" class="btn btn-primary">
                                                    Gerenciar Aulas
                                                </a>
                                                <a href="#" onclick="document.getElementById('editar_modulo_id').value='{{ $modulo->id }}';document.getElementById('editar_modulo_nome').value='{{ $modulo->nome }}';;document.getElementById('editar_modulo_ordem').value='{{ $modulo->ordem }}';document.getElementById('editar_modulo_status').value='{{ $modulo->status }}';" data-bs-target="#ModalEditarModulo" data-bs-toggle="modal" class="aclNivel2 btn btn-warning text-dark">
                                                    <i class="fa fa-edit"></i>
                                                </a>
                                                <a href="#" onclick="document.getElementById('deletar_modulo_id').value='{{ $modulo->id }}';document.getElementById('deletar_modulo_nome').innerHTML='{{ $modulo->nome }}';" data-bs-target="#ModalDeletarModulo" data-bs-toggle="modal" class="aclNivel3 btn btn-danger">
                                                    <i class="fa fa-trash"></i>
                                                </a>

                                            </span>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            {{ $modulos->links() }}

        </div>
    </div>

    <form wire:submit.prevent="cadastrarModulo">
        <div wire:ignore class="modal fade" id="ModalCadastrarModulo" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3 class="modal-title" id="exampleModalLabel">Cadastrar Módulo</h3>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body d-lg-flex">

                        <input type="text" wire:model="add_nome" class="form-control p-form" placeholder="Nome" required>
                        
                        <select wire:model="add_status" class="mx-lg-3 my-3 my-lg-0 w-lg-40 select-form">
                            <option value="0">Ativo</option>
                            <option value="1">Desativado</option>
                        </select>

                        <input type="number" wire:model="add_ordem" class="w-lg-30 form-control p-form" placeholder="Ordem">

                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary weight-600 py-2">Confirmar</button>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <form method="POST" action="{{ route('admin.curso.modulo.lista', ['id' => $curso->id]) }}">
    @csrf
        <div wire:ignore class="modal fade" id="ModalEditarModulo" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3 class="modal-title" id="exampleModalLabel">Editar Módulo</h3>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body d-lg-flex">

                        <input type="hidden" name="modulo_id" id="editar_modulo_id">

                        <input type="text" name="nome" id="editar_modulo_nome" class="me-lg-3 mb-3 mb-lg-0 form-control p-form" placeholder="Modulo">

                        <select name="status" id="editar_modulo_status" class="mx-lg-3 my-3 my-lg-0 w-lg-40 select-form">
                            <option value="0">Ativo</option>
                            <option value="1">Desativado</option>
                        </select>

                        <input type="number" name="ordem" id="editar_modulo_ordem" class="w-lg-30 form-control p-form" placeholder="Ordem">

                    </div>
                    <div class="modal-footer">
                        <button type="submit" name="editarModulo" value="ok" class="btn btn-primary weight-600 py-2">Confirmar</button>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <form method="POST">
    @csrf
        <div wire:ignore class="modal fade" id="ModalDeletarModulo" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3 class="modal-title" id="exampleModalLabel">Deletar Módulo</h3>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body text-center py-5">

                        <input type="hidden" name="modulo_id" id="deletar_modulo_id">

                        <p id="deletar_modulo_nome" class="m-0 fs-15 weight-600">
                            
                        </p>

                        <hr/>

                        <p class="m-0 fs-15">
                            Você tem certeza que deseja deletar esta modulo?
                            <br>
                            Todas as informações serão perdidas definitivamente.
                        </p>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" name="deletarModulo" value="ok" class="btn btn-danger weight-600">Confirmar</button>
                    </div>
                </div>
            </div>
        </div>
    </form>

    @include('livewire.util.toast')

</div>