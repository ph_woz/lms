<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserContratoCampo extends Model
{
    use HasFactory;

    protected $table = 'users_contratos_campos';

    public $timestamps = false;

    protected $fillable = [
        'plataforma_id',
        'contrato_id',
        'user_contrato_id',
        'user_id',
        'campo_id',
        'value',
    ];

    public static  function boot()
    {
        parent::boot(); 
        static::creating(function ($model)
        {
            $model->plataforma_id  = session('plataforma_id') ?? \Auth()->user()->plataforma_id ?? Plataforma::dominio()->pluck('id')[0] ?? null;
        });
    }
    
    public function scopePlataforma($query)
    {
        return $query->where('plataforma_id', session('plataforma_id') ?? Plataforma::dominio()->pluck('id')[0] ?? null);
    }
}
