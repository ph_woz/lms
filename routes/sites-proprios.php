<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SiteProprio\BrasEduController;
use App\Http\Controllers\SiteProprio\CedetepEnfController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
*/


Route::group(['prefix' => 'brasedu'], function() {

	Route::get('/cursos-by-categoria', [BrasEduController::class, 'getCursosByCategoriaId'])->name('brasedu.cursos.by.categoria.id');

});


Route::get('/enfermagem-cedetep', [CedetepEnfController::class, 'index'])->name('cedetep.curso.enfermagem');