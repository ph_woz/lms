<!doctype html>
<html lang="pt-br">
<head>

   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1">

   <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/uikit@3.5.9/dist/css/uikit.min.css" />
   <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
   <link rel="stylesheet" href="{{ asset('css/util.css') }}?v=3">
   <link rel="stylesheet" href="{{ asset('sites-proprios/faro/css/home.css') }}?v=3">

   <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">

   <title>{{ $pagina->seo_title ?? $plataforma->nome }}</title>
   <meta name="robots" content="index, follow">
   <meta name="description" content="{{ $pagina->seo_description ?? null }}"/>
   <meta name="keywords" content="{{ $pagina->seo_keywords ?? null }}"/>
   <link rel="canonical" href="{{ Request::url() }}" />
   <link rel="shortcut icon" href="{{ $plataforma->favicon }}">
   <meta property="og:locale" content="pt_BR" />
   <meta property="og:type" content="website" />
   <meta property="og:title" content="{{ $pagina->seo_title ?? $plataforma->nome }}" />
   <meta property="og:description" content="{{ $pagina->seo_description ?? null }}" />
   <meta property="og:url" content="{{ Request::url() }}" />
   <meta property="og:site_name" content="{{ $plataforma->nome }}" />
   <meta property="og:image" content="{{ $plataforma->logotipo }}" />
   <meta name='author' content='WOZCODE | Pedro Henrique' />

   <style>
      .border-th { border-color: #e3e3e3!important; }
   </style>

</head>
<body class="bg-f4" id="inicio">

   @include('sites-proprios/faro/componentes/header')

   <main>

      <section>
           <div class="w-100 h-200" style="background-size: cover; background-position: center; background-image: url('{{ $plataforma->jumbotron ?? 'https://yata.s3-object.locaweb.com.br/8fd17de7588f66611d70c6df80b8e0dbefbf907add82ccb385b7b3bf642c3662' }}');">
              <div class="position-relative h-100 px-xl-7 px-3">
              <div class="background-overlay opacity-08 h-100" style="background: rgba(0,0,0,1);"></div>

                 <div class="row container align-items-center h-100 position-relative">
                    <div class="mt-2">
                       <h1 class="text-light font-poppins weight-800 fs-xs-35 fs-md-50 text-shadow-3" id="tituloPolos">
                          Polos
                       </h1>
                    </div>
                 </div>
              </div>
           </div>
      </section>

      <section class="py-5">
         <div class="container">
         
            <div>

                  <div class="bg-white p-3 box-shadow rounded">

                     <form>

                        <div class="d-flex mb-3">

                           <div class="w-100 me-3">
                              <input name="search" type="search" class="filtro-search-curso rounded" placeholder="Buscar" value="{{ $search ?? null }}">
                           </div>

                           <button type="submit" class="bg-gradient bg-hover-gradient d-block text-center px-4 rounded text-dark weight-700 font-poppins hover-d-none border-0">
                              <i class="fa fa-search"></i>
                           </button>

                        </div>

                     </form>

                     <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col" class="border-th font-poppins weight-600 color-titulo fs-15">Nome</th>
                                    <th scope="col" class="border-th font-poppins weight-600 color-titulo fs-15">Celular</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($polos as $polo)
                                    <tr>
                                        <td class="w-lg-75 font-poppins fs-14">{{ $polo->nome }}</td>
                                        <td class="w-lg-20 font-poppins fs-14">{!! $polo->celular ?? '<i>Não definido</i>' !!}</td>
                                        <td>
                                          <a href="/polo/{{\Str::slug($polo->nome)}}/{{$polo->id}}" target="_blank" class="btn-azul px-3 py-2 fs-11 weight-700 font-poppins ls-05 hover-d-none">
                                              ABRIR
                                          </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                     </div>

                  </div>

            </div>

         </div>
      </section>

   </main>

   @include('sites-proprios/faro/componentes/footer')

   <a href="https://api.whatsapp.com/send?phone=55{{preg_replace('/\D/', '', $plataforma->whatsapp)}}&text=Olá, vim através do Site {{ $plataforma->nome }}" target="_blank" class="text-white fs-30">
       <div class="fixed-bottom right-0 me-3 mb-3 z-3">
           <div class="square-60 rounded-circle bg-wpp flex-center box-shadow-wpp">
               <i class="fab fa-whatsapp"></i>
           </div>
       </div>
   </a>

   <script src="https://cdn.jsdelivr.net/npm/uikit@3.5.9/dist/js/uikit.min.js"></script>
   <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-U1DAWAznBHeqEIlVSCgzq+c9gqGAJn5c/t99JyeKa9xxaYpSvHU5awsuZVVFIhvj" crossorigin="anonymous"></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>
   <script>

      window.onscroll = function() {myFunction()};

      var header = document.getElementById("menu");
      var sticky = header.offsetTop;

      function myFunction() {
        if (window.pageYOffset > sticky) {
          header.classList.add("fixed-top");
        } else {
          header.classList.remove("fixed-top");
        }
      }
   </script>

</body>
</html>