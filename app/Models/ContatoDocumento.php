<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ContatoDocumento extends Model
{
    use HasFactory;

    public $timestamps = true;

    protected $table = 'contatos_documentos';

    protected $fillable = [
        'plataforma_id',
        'cadastrante_id',
    	'contato_id',
    	'nome',
        'link',
    ];

    public static  function boot()
    {
        parent::boot(); 
        static::creating(function ($model) {
            if(\Auth::check())
            {
                $model->plataforma_id = Auth()->user()->plataforma_id;
                $model->cadastrante_id = Auth()->user()->id;
            }
        });
    }

    public function scopePlataforma($query)
    {
        $plataforma_id = Plataforma::dominio()->pluck('id')[0];

        return $query->where('plataforma_id', $plataforma_id);
    }

    public function scopeContato($query, $id)
    {
        return $query->where('contato_id', $id);
    }

    public function cadastrante($contato_id)
    {
        $nome = Contato::plataforma()->where('id', $contato_id)->pluck('nome')[0] ?? null;

        return $nome;
    }
}
