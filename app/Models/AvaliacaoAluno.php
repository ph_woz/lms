<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AvaliacaoAluno extends Model
{
    use HasFactory;

    protected $table = 'avaliacoes_alunos';

    public $timestamps = true;

    public $fillable = [
        'plataforma_id',
        'curso_id',
        'turma_id',
        'avaliacao_id',
        'aluno_id',
        'nota',
        'status',
        'n_tentativa',
        'questoesIds',
        'data_finalizada',
    ];

    public static  function boot()
    {
        parent::boot(); 
        static::creating(function ($model)
        {
            $model->plataforma_id = session('plataforma_id') ?? \Auth()->user()->plataforma_id ?? Plataforma::dominio()->pluck('id')[0] ?? null;
        });
    }

    public function scopePlataforma($query)
    {
        return $query->where('plataforma_id', session('plataforma_id') ?? Plataforma::dominio()->pluck('id')[0] ?? null);
    }

    public function scopeFinalizada($query)
    {
        return $query->where('status', 'F');
    }
    
    public function getNota($notas)
    {   
        $valoresParaSoma = [];

        foreach($notas as $nota)
        {
            $nota = floatval(str_replace(',','.', $nota));

            $valoresParaSoma[] = $nota;
        }
        
        $nota = array_sum($valoresParaSoma);

        return $nota;
    }

    public static function staticSomaNotas($notas)
    {   
        $valoresParaSoma = [];

        foreach($notas as $nota)
        {
            $nota = floatval(str_replace(',','.', $nota));

            $valoresParaSoma[] = $nota;
        }
        
        $nota = array_sum($valoresParaSoma);

        return $nota;
    }

}
