<?php

namespace App\Http\Controllers\SiteProprio;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\PaginaConteudo;
use App\Models\Curso;

class ColegioComercialController extends Controller
{
    public function index()
    {
        $seo = PaginaConteudo::plataforma()->where('url','/')->select('plataforma_id','seo_title','seo_keywords','seo_description')->first();

        $cursos = Curso::plataforma()->ativo()->publico()->orderBy('nome')->get();

        if(request('sentContato'))
        {
            if(request('check'))
                return back()->with('danger', 'SPAM Detected!');

            \App\Models\Contato::create(request()->all() + [
                'plataforma_id' => $seo->plataforma_id,
                'tipo' => 'L',
            ]);
            
            return redirect('/#contato')->with('success', 'Inscrição enviada com êxito.');
        }

    	return view('sites-proprios.colegio-comercial.home', compact('seo','cursos') );
    }
}