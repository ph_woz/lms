<footer class="bg-footer pt-5">
   <div class="container-fluid ps-xl-6 pe-xl-6">
 
      <div class="row py-lg-4 px-3 px-md-0">

         <div class="col-lg-4">

            <h1 class="fs-20 font-montserrat text-light weight-800 text-uppercase">CONTATO MATRIZ</h1>
            <div class="my-4 w-80px h-2px bg-line rounded"></div>

            <div class="mt-2 pe-xl-6">                 

               <div class="mb-4" id="boxContatoHorarioAtendimento">

                  <div class="d-flex align-items-center">

                     <div class="square-40 pe-lg-5 pe-3">
                        <i class="far fa-clock fs-30 color-amarelo"></i>
                     </div>

                     <div>
                        <p class="mb-0 font-poppins weight-600 text-white d-flex align-items-center fs-15">
                           <span>HORÁRIO DE ATENDIMENTO</span>
                        </p>
                        <p class="mt-1 mt-lg-0 mb-0 font-poppins weight-500 fs-14 text-light">
                           Segunda a Sexta das 08:00 as 17:00hs
                        </p>
                     </div>

                  </div>

               </div>

               <div class="mb-4" id="boxContatoEmail">

                  <div class="d-flex align-items-center">

                     <div class="square-40 pe-lg-5 pe-3">
                        <i class="fa fa-envelope fs-30 color-amarelo"></i>
                     </div>

                     <div>
                        <p class="mb-0 font-poppins weight-600 text-white d-flex align-items-center fs-15">
                           <span>EMAIL</span>
                        </p>
                        <p class="mt-1 mt-lg-0 mb-0 font-poppins weight-500 fs-14 text-light">
                           {{ $plataforma->email ?? 'meuemail@exemplo.com' }}
                        </p>
                     </div>

                  </div>

               </div>

               <div class="mb-4" id="boxContatoWhatsAp">

                  <div class="d-flex align-items-center">

                     <div class="square-40 pe-lg-5 pe-3">
                        <i class="fab fa-whatsapp fs-30 color-amarelo"></i>
                     </div>

                     <div>
                        <p class="mb-0 font-poppins weight-600 text-white d-flex align-items-center fs-15">
                           <span>CONTATO</span>
                        </p>
                         <p class="mb-0 font-poppins weight-500 fs-14 text-light">
                           Matrícule-se:
                           <a href="https://api.whatsapp.com/send?phone=55{{preg_replace('/\D/', '', $plataforma->whatsapp)}}&text=Olá, vim através do Site {{ $plataforma->nome }}" target="_blank" class="text-d-none text-light hover-white">{{ $plataforma->whatsapp }}</a>
                         </p>
                     </div>

                  </div>

               </div>

               <div class="mb-4" id="boxContatoTelefone">

                  <div class="d-flex align-items-center">

                     <div class="square-40 pe-lg-5 pe-3">
                        <i class="fa fa-phone fs-30 color-amarelo"></i>
                     </div>

                     <div>
                        <p class="mb-0 font-poppins weight-600 text-white d-flex align-items-center fs-15">
                           <span>TELEFONE</span>
                        </p>
                         <p class="mb-0 font-poppins weight-500 fs-14 text-light">
                           Administrativo:
                           <a href="tel:+55{{preg_replace('/\D/', '', $plataforma->telefone)}}" target="_blank" class="text-d-none text-light hover-white">{{ $plataforma->telefone }}</a>
                         </p>
                     </div>

                  </div>

               </div>

               <div class="mb-3" id="boxContatoEndereco">

                  <div class="d-flex align-items-center">

                     <div class="square-40 pe-lg-5 pe-3">
                        <i class="fa fa-map-marker-alt fs-30 color-amarelo"></i>
                     </div>

                     <div>
                        <p class="mb-0 font-poppins weight-600 text-white d-flex align-items-center fs-15">
                           <span>ENDEREÇO</span>
                        </p>
                       <address class="p-0 m-0 font-poppins weight-500 fs-14 text-light">
                         Avenida América, 349. Vila Diniz - São José do Rio Preto, SP. 15013-310
                       </address>
                     </div>

                  </div>

               </div>

               <div id="boxContatoRedesSociais" class="pt-4">
                  <div class="d-flex">
                     <a href="{{ $plataforma->facebook }}" target="_blank" class="d-block me-2 hover-d-none social-icon text-light font-poppins rounded box-shadow fs-14 square-50 d-flex align-items-center justify-content-center">
                        <i class="text-white fab fa-facebook-f fs-20"></i>
                     </a>
                     <a href="{{ $plataforma->instagram }}" target="_blank" class="d-block me-2 hover-d-none social-icon text-light font-poppins rounded box-shadow fs-14 square-50 d-flex align-items-center justify-content-center">
                       <i class="text-white fab fa-instagram fs-20"></i>
                     </a>
                  </div>
               </div>

            </div>

         </div>

         <div class="col-lg-4 my-5 my-lg-0">

            <h1 class="fs-20 font-montserrat text-light weight-800 text-uppercase">POLOS</h1>
            <div class="my-4 w-80px h-2px bg-line rounded"></div>

            <div class="w-100 pe-lg-5">
               @php
                  $unidades = \App\Models\Unidade::plataforma()->ativo()->publico()->orderBy('nome')->get();
               @endphp
               @foreach($unidades as $unidade)
                  <div class="w-100">
                     <p class="mb-0 font-poppins weight-600 text-white d-flex align-items-center fs-15">{{ $unidade->nome }}</p>
                     <div class="w-100">
                        <div>
                           <span class="font-poppins weight-500 fs-14 text-light">
                              <i class="color-amarelo fa fa-envelope"></i> 
                              {{ $unidade->email }}
                           </span>
                        </div>
                        <div class="my-1">
                           <a href="https://api.whatsapp.com/send?phone=55{{preg_replace('/\D/', '', $unidade->celular)}}&text=Olá, vim através do Site {{ $plataforma->nome }}." target="_blank" class="font-poppins weight-500 fs-14 text-light">
                              <i class="color-amarelo fab fa-whatsapp"></i> 
                              {{ $unidade->celular }}
                           </a>
                        </div>
                        <div>
                          <address class="p-0 m-0 font-poppins weight-500 fs-14 text-light">
                           <i class="color-amarelo fa fa-map-marker-alt"></i>
                            {{ $unidade->endereco }}
                          </address>
                        </div>
                     </div>
                  </div>
                  @if(!$loop->last)
                     <hr class="border border-light">
                  @endif
               @endforeach
            </div>

         </div>

         <div class="col-lg-4 mt-4 mt-lg-0">

            <h1 class="fs-20 font-montserrat text-light weight-800 text-uppercase">LINKS</h1>
            <div class="my-4 w-80px h-2px bg-line rounded"></div>

            <div class="pe-lg-5">
               <div class="my-3">
                  <a href="https://braseducursos.com.br/f/ficha-de-matricula/37/t/627" target="_blank" class="bg-gradient fs-14 d-block text-center text-shadow-2 px-4 rounded box-shadow text-light weight-600 w-100 py-2-5 font-poppins hover-d-none">
                     MATRICULE-SE JÁ!
                  </a>
               </div>
               <div class="mb-3">
                  <a href="/login" class="bg-gradient fs-14 d-block text-center text-shadow-2 px-4 rounded box-shadow text-light weight-600 w-100 py-2-5 font-poppins hover-d-none">
                     ÁREA DO ALUNO
                  </a>
               </div>
               <div class="mb-3">
                  <a href="https://api.whatsapp.com/send?phone=55{{preg_replace('/\D/', '', $plataforma->whatsapp)}}&text=Olá, vim através do Site {{ $plataforma->nome }}" class="bg-gradient fs-14 d-block text-center text-shadow-2 px-4 rounded box-shadow text-light weight-600 w-100 py-2-5 font-poppins hover-d-none">
                     ENTRAR EM CONTATO
                  </a>
               </div>
               <div class="mb-3">
                  <a href="https://braseducursos.com.br/f/trabalhe-conosco/41/t/628" target="_blank" class="bg-gradient fs-14 d-block text-center text-shadow-2 px-4 rounded box-shadow text-light weight-600 w-100 py-2-5 font-poppins hover-d-none">
                     SEJA UM CONSULTOR
                  </a>
               </div>
               <div class="mb-3">
                  <a href="https://blog.braseducursos.com.br/" target="_blank" class="bg-gradient fs-14 d-block text-center text-shadow-2 px-4 rounded box-shadow text-light weight-600 w-100 py-2-5 font-poppins hover-d-none">
                     BLOG
                  </a>
               </div>
               <div class="mb-3">
                  <a href="/sobre-nos" target="_blank" class="bg-gradient fs-14 d-block text-center text-shadow-2 px-4 rounded box-shadow text-light weight-600 w-100 py-2-5 font-poppins hover-d-none">
                     QUEM SOMOS
                  </a>
               </div>
            </div>

         </div>

      </div>

      <hr class="mt-4 border border-light" />

      <div class="pb-3 d-lg-flex justify-content-between align-items-center">

         <p class="text-xs-center mb-1 mb-lg-0 p-0 text-light font-poppins fs-11 ls-05">
            {{ $plataforma->nome }} © 2022 • Todos os direitos reservados • <span class="d-xs-block mt-1">CNPJ: {{ $plataforma->cnpj ?? '00.080.000/0000-00' }}</span>
         </p>

         <p class="text-xs-center m-0 p-0 text-light font-poppins fs-11 ls-05">
            Desenvolvido por <a href="https://wozcode.com" target="_blank" class="text-light text-d-none fs-11 weight-600 text-shadow-1">WOZCODE</a>
         </p>

      </div>

   </div>
</footer>

<script src="https://cdn.jsdelivr.net/npm/uikit@3.5.9/dist/js/uikit.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-u1OknCvxWvY5kfmNBILK2hRnQC3Pr17a+RTT6rIHI7NnikvbZlHgTPOOmMi466C8" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.1/jquery.min.js" integrity="sha512-aVKKRRi/Q/YV+4mjoKBsE4x3H+BkegoM/em46NNlCqNTmUYADjBbeNefNxYV7giUp0VxICtqdrbqU7iVaeZNXA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>