<?php

namespace App\Http\Controllers\SiteProprio;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\PaginaConteudo;

class FunespController extends Controller
{
    public function index()
    {
        $seo = PaginaConteudo::plataforma()->where('url','/')->select('seo_title','seo_keywords','seo_description')->first();

        if(request('sentContato'))
        {
            if(request('check'))
            {
                return back()->with('danger', 'SPAM Detected!');
            }

            \App\Models\Contato::create(request()->all() + ['tipo' => 'L']);
            
            if(request('assunto') == 'Fale Conosco')
            {
                return redirect('/#contato')->with('success', 'Inscrição enviada com êxito.');
            
            } else {
            
                return redirect('/#pre-matricula')->with('success_prematricula', 'Inscrição enviada com êxito.');
            }
        }

    	return view('sites-proprios.funesp.home', compact('seo') );
    }
}