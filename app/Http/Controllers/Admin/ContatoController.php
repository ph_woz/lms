<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class ContatoController extends Controller
{
    public function lista()
    {
        return view('admin.contato.lista');
    }
    
    public function info($contato_id)
    {
        return view('admin.contato.info', compact('contato_id') );
    }


    public function matricularAluno($contato_id)
    {
    	$contato = \App\Models\Contato::plataforma()->where('id', $contato_id)->first();

    	$aluno = $this->cadastrarAluno($contato);

    	return redirect()->route('admin.aluno.info', ['id' => $aluno->id])->with('success', 'Aluno cadastrado com êxito.');
    }

    public function cadastrarAluno($contato)
    {
        $aluno = \App\Models\User::plataforma()->where('email', $contato->email)->first();

        if(is_null($aluno))
        {
            $contato['password'] = $contato->password ?? Hash::make($contato->cpf ?? \App\Models\Util::generateHash());
            $contato['tipo'] = 'aluno';
            $contato['status'] = 0;

            $aluno = \App\Models\User::create($contato->toArray());
        }

        $categoriasIds = \App\Models\ContatoCategoria::plataforma()->where('contato_id', $contato->id)->get()->pluck('categoria_id')->toArray();

        foreach($categoriasIds as $categoria_id)
        {
            $countDuplicate = \App\Models\UserCategoria::plataforma()->where('user_id', $aluno->id)->where('categoria_id', $categoria_id)->count();
            
            if($countDuplicate == 0)
                \App\Models\UserCategoria::create([
                    'plataforma_id' => $contato->plataforma_id,
                    'categoria_id'  => $categoria_id,
                    'user_id'       => $aluno->id,
                ]);
        }

        $endereco['cep']              = $contato->cep;
        $endereco['rua']              = $contato->rua;
        $endereco['numero']           = $contato->numero;
        $endereco['bairro']           = $contato->bairro;
        $endereco['cidade']           = $contato->cidade;
        $endereco['estado']           = $contato->estado;
        $endereco['complemento']      = $contato->complemento;
        $endereco['ponto_referencia'] = $contato->ponto_referencia;

        if(array_filter($endereco))
        {
            $userEndereco = \App\Models\UserEndereco::where('user_id', $aluno->id)->first();

            if(is_null($userEndereco))
                \App\Models\UserEndereco::create($endereco + ['plataforma_id' => $contato->plataforma_id, 'user_id' => $aluno->id]);
            else
                \App\Models\UserEndereco::where('user_id', $aluno->id)->update($endereco);
        }

        if($contato->turma_id) // Matricular Aluno na Turma
        {
            $turmaAluno = \App\Models\TurmaAluno::create([
		        'plataforma_id' => $contato->plataforma_id,
		        'turma_id'      => $contato->turma_id,
		        'curso_id'      => $contato->curso_id,
		        'aluno_id'      => $aluno->id,
            ]);

            if(request('matricular_aluno_turma_enviar_email') && request('email_contratacao') != null)
            {
                \Mail::to($aluno->email)->send(new \App\Mail\EmailContratacaoTurma($aluno, $turmaAluno));
            }
        }

        if($contato->trilha_id) // Matricular Aluno na Trilha
        {
            \App\Models\TrilhaAluno::create([
                'plataforma_id' => $contato->plataforma_id,
                'trilha_id'     => $contato->trilha_id,
                'aluno_id'      => $aluno->id,
            ]);

            $turmasIds = \App\Models\TrilhaTurma::plataforma()->where('trilha_id', $contato->trilha_id)->get()->pluck('turma_id')->toArray();
            $turmas = \App\Models\Turma::plataforma()->whereIn('id', $turmasIds)->select('id','curso_id')->get();

            foreach($turmas as $turma)
            {
                \App\Models\TurmaAluno::create([
                    'plataforma_id' => $contato->plataforma_id,
                    'turma_id'      => $turma->id,
                    'curso_id'      => $turma->curso_id,
                    'aluno_id'      => $aluno->id,
                ]);
            }

            if(request('matricular_aluno_trilha_enviar_email') && request('email_contratacao') != null)
            {
                $trilha = \App\Models\Trilha::plataforma()->where('id', $contato->trilha_id)->first();
                
                \Mail::to($aluno->email)->send(new \App\Mail\EmailContratacaoTrilha($aluno, $trilha));
            }
        }

        return $aluno;
    }

}