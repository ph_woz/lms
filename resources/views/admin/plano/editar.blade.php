@extends('layouts.admin.master')

@section('css')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.14.0-beta2/dist/css/bootstrap-select.min.css">
@endsection

@section('content')
    
<div>

    <div class="mb-3 d-flex justify-content-end">
	    <a href="{{ route('admin.plano.info', ['id' => $plano->id ?? null]) }}" class="btn bg-base text-white">
	        <i class="fa fa-eye"></i>
	    </a>
	  </div>

    <form method="POST" class="mb-4">
    @csrf

		<section class="mb-4-5">
			
			<a href="#boxInfoPrincipal" data-bs-toggle="collapse" aria-expanded="false" class="hover-d-none">
				<div class="section-title">
					Informações Principais
				</div>
			</a>
			<hr/>
			
			<div class="collapse show" id="boxInfoPrincipal">

		    	<div class="d-lg-flex mb-3">

			    	<div class="w-100 me-lg-3 mb-3 mb-lg-0">
			    		<label>Nome</label>
			    		<input type="text" name="nome" class="form-control p-form" placeholder="Referência" required value="{{ old('nome') ?? $plano->nome }}">
			    	</div>

			    	<div class="w-lg-35 me-lg-3 mb-3 mb-lg-0">
			    		<label>Publicar</label>
			    		<select name="publicar" class="select-form">
			    			<option value="S">Sim</option>
			    			<option value="N" @if(old('publicar') == 'N' || $plano->publicar == 'N') selected @endif>Não</option>
			    		</select>
			    	</div>

			    	<div class="w-lg-35 me-lg-3 mb-3 mb-lg-0">
			    		<label>Ordem</label>
				    	<input type="text" name="ordem" class="mask_number form-control p-form" placeholder="N° de Exibição" value="{{ old('ordem') ?? $plano->ordem }}">
			    	</div>

			    	<div class="w-lg-40">
			    		<label>Status</label>
			    		<select name="status" class="select-form">
			    			<option value="0">Ativo</option>
			    			<option value="1" @if(old('status') == '1' || $plano->status == 1) selected @endif>Desativado</option>
			    		</select>
			    	</div>

			    </div>

			    <div class="w-100 d-lg-flex mb-3">

			    	<div class="w-lg-75 me-lg-3 mb-3 mb-lg-0">
			    		<label>Preço</label>
				    	<input type="text" name="valor" class="mask_valor form-control p-form" placeholder="R$" value="{{ old('valor') ?? $plano->valor }}">
			    	</div>

			    	<div class="w-lg-75 me-lg-3 mb-3 mb-lg-0">
			    		<label>Preço Promocional</label>
				    	<input type="text" name="valor_promocional" class="mask_valor form-control p-form" placeholder="R$" value="{{ old('valor_promocional') ?? $plano->valor_promocional }}">
			    	</div>

			    	<div class="w-lg-75 me-lg-3 mb-3 mb-lg-0">
			    		<label>Renovação automática</label>
			    		<select name="renovacao_automatica" class="select-form">
			    			<option value="N">Não</option>
			    			<option value="S" @if(old('renovacao_automatica') == 'S' || $plano->renovacao_automatica == 'S') selected @endif>Sim</option>
			    		</select>
			    	</div>

					@if(\App\Models\Unidade::checkActive() === true)
					<div class="w-100 boxUnidadeRemove">
						<label>Unidade</label>
						<select name="unidade_id" id="unidade_id" class="selectpicker" data-width="100%">
							<option value="">Selecione</option>
							@foreach($unidades as $unidade)
								<option value="{{ $unidade->id }}" @if(old('unidade_id') == $unidade->id || $plano->unidade_id == $unidade->id) selected @endif>
									{{ $unidade->nome }}
								</option>
							@endforeach
						</select>
					</div>
					@endif

				</div>

			</div>

		</section>

		<section class="mb-4-5">
			
			<a href="#boxBeneficiosDoPlano" data-bs-toggle="collapse" aria-expanded="false" class="hover-d-none">
				<div class="section-title">
					Benefícios/Diferenciais do Plano
				</div>
			</a>
			<hr/>
			
			<div class="collapse show" id="boxBeneficiosDoPlano">

				@if(count($beneficios) > 0)
					@foreach($beneficios as $beneficio)
						<div class="mb-2 d-flex align-items-center">
							<input type="tezt" name="beneficios[]" class="form-control p-form" placeholder="Exemplo: Horário Livre" value="{{ $beneficio }}">
							<a href="#" class="btn btn-danger py-2-3 ms-2">
								<i class="fa fa-trash"></i>
							</a>
						</div>
					@endforeach
				@else
					<div class="mb-2 d-flex align-items-center">
						<input type="tezt" name="beneficios[]" class="form-control p-form" placeholder="Exemplo: Horário Livre">
						<a href="#" class="btn btn-danger py-2-3 ms-2">
							<i class="fa fa-trash"></i>
						</a>
					</div>
				@endif

			</div>

			<div id="append"></div>

			<div>
				<a href="#" class="btn-append btn btn-primary weight-600 fs-12">
					+ Adicionar outro
				</a>
			</div>

		</section>

		<section class="mb-4-5">
			
			<a href="#boxCategorias" data-bs-toggle="collapse" aria-expanded="false" class="hover-d-none">
				<div class="section-title">
					Categorias 
				</div>
			</a>
			<hr/>

			<div class="collapse" id="boxCategorias">
				<select name="categorias[]" class="selectpicker" multiple data-width="100%" data-live-search="true" title="Selecione">
					@foreach($categorias as $categoria)
						<option value="{{ $categoria->id }}" @if(in_array($categoria->id, $categoriasSelected)) selected @endif>{{ $categoria->nome }}</option>
					@endforeach
				</select>
			</div>

		</section>

		<button type="submit" name="sent" value="ok" class="btn-add mt-3">Salvar Alterações</button>	

    </form>

</div>


@endsection

@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.14.0-beta2/dist/js/bootstrap-select.min.js"></script>
<script>

	@if(\Auth::user()->restringir_unidade == 'S' && \Auth::user()->unidade_id != null)
		jQuery("#unidade_id option:contains('Selecione')").remove();
	@endif

	$('.mask_number').mask('00000');

    $(".mask_valor").mask('#.##0,00', {
      reverse: true
    });


	$('.btn-append').click(function(e) {

		e.preventDefault();

		$("#append").append('<div class="mb-2 d-flex align-items-center"> <input type="tezt" name="beneficios[]" class="form-control p-form" placeholder="Exemplo: Horário Livre"> <a href="" class="btn btn-danger py-2-3 ms-2"> <i class="fa fa-trash"></i> </a> </div>');

	});

	$(document).on('click', '.btn-danger', function(e)
	{
		e.preventDefault();

		$(this).parent().remove();
	});

</script>
@endpush