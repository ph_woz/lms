@extends('layouts.admin.master')

@section('css')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.14.0-beta2/dist/css/bootstrap-select.min.css">
@endsection

@section('content')
    
<div>

    <div class="mb-3 d-flex justify-content-end">
	    <a href="{{ route('admin.produto.info', ['id' => $produto->id ?? null]) }}" class="btn bg-base text-white">
	        <i class="fa fa-eye"></i>
	    </a>
	  </div>

    <form method="POST" class="mb-4">
    @csrf

		<section class="mb-4-5">
			
			<a href="#boxInfoPrincipal" data-bs-toggle="collapse" aria-expanded="false" class="hover-d-none">
				<div class="section-title">
					Informações Principais
				</div>
			</a>
			<hr/>
			
			<div class="collapse show" id="boxInfoPrincipal">

		    	<div class="d-lg-flex mb-3">

			    	<div class="w-100 me-lg-3 mb-3 mb-lg-0">
			    		<label>Nome</label>
			    		<input type="text" name="nome" class="form-control p-form" placeholder="Referência" required value="{{ old('nome') ?? $produto->nome }}">
			    	</div>

			    	<div class="w-lg-60 me-lg-3 mb-3 mb-lg-0">
			    		<label>Marca</label>
			    		<input type="text" name="marca" class="form-control p-form" placeholder="Marca" value="{{ old('marca') ?? $produto->marca }}">
			    	</div>

			    	<div class="w-lg-60 me-lg-3 mb-3 mb-lg-0">
			    		<label>Modelo</label>
			    		<input type="text" name="modelo" class="form-control p-form" placeholder="Modelo" value="{{ old('modelo') ?? $produto->modelo }}">
			    	</div>

			    	<div class="w-lg-35 me-lg-3 mb-3 mb-lg-0">
			    		<label>Publicar</label>
			    		<select name="publicar" class="select-form">
			    			<option value="S">Sim</option>
			    			<option value="N" @if(old('publicar') == 'N' || $produto->publicar == 'N') selected @endif>Não</option>
			    		</select>
			    	</div>

			    	<div class="w-lg-40">
			    		<label>Status</label>
			    		<select name="status" class="select-form">
			    			<option value="0">Ativo</option>
			    			<option value="1" @if(old('status') == '1' || $produto->status == '1') selected @endif>Desativado</option>
			    		</select>
			    	</div>

			    </div>


			    <div class="d-lg-flex mb-3">

			    	<div class="w-100 me-lg-3 mb-3 mb-lg-0">
			    		<label>Tamanho</label>
			    		<input type="text" name="tamanho" class="form-control p-form" placeholder="Exemplo: GG ou 39" value="{{ old('tamanho') ?? $produto->tamanho }}">
			    	</div>

			    	<div class="w-100 me-lg-3 mb-3 mb-lg-0">
			    		<label>Cor</label>
			    		<input type="text" name="cor" class="form-control p-form" placeholder="Cor" value="{{ old('cor') ?? $produto->cor }}">
			    	</div>

			    	<div class="w-100 me-lg-3 mb-3 mb-lg-0">
			    		<label>Estoque</label>
			    		<div class="d-flex w-100">
				    		<input type="text" name="estoque" class="mask_number form-control p-form" placeholder="N° em quantidade" value="{{ old('estoque') ?? $produto->estoque }}">
						</div>
			    	</div>

			    	<div class="w-100 me-lg-3 mb-3 mb-lg-0">
			    		<label>Visibilidade</label>
			    		<select name="visibilidade" class="select-form">
			    			<option value="0">Listado</option>
			    			<option value="1" @if(old('visibilidade') == '1' || $produto->visibilidade == '1') selected @endif>Não listado</option>
			    		</select>
			    	</div>

			    	<div class="w-100 me-lg-3 mb-3 mb-lg-0">
			    		<label>Frete Grátis</label>
			    		<select name="frete_gratis" class="select-form">
			    			<option value="S">Sim</option>
			    			<option value="N" @if(old('frete_gratis') == 'N' || $produto->frete_gratis == 'N') selected @endif>Não</option>
			    		</select>
			    	</div>

			    	<div class="w-100">
			    		<label>Disponível</label>
			    		<select name="disponivel" class="select-form">
			    			<option value="S">Sim</option>
			    			<option value="N" @if(old('disponivel') == 'N' || $produto->disponivel == 'N') selected @endif>Não, esgotado</option>
			    		</select>
			    	</div>

			    </div>

			    <div class="d-lg-flex mb-3">

					@if(\App\Models\Unidade::checkActive() === true)
					<div class="w-100 mb-3 me-lg-3 mb-lg-0 boxUnidadeRemove">
						<label>Unidade</label>
						<select name="unidade_id" id="unidade_id" class="selectpicker" data-width="100%">
							<option value="">Selecione</option>
							@foreach($unidades as $unidade)
								<option value="{{ $unidade->id }}" @if(old('unidade_id') == $unidade->id || $unidade->id == $produto->unidade_id) selected @endif>
									{{ $unidade->nome }}
								</option>
							@endforeach
						</select>
					</div>
					@endif

			    	<div class="w-100">
			    		<label>Link do vídeo do seu produto no Youtube ou Vimeo</label>
			    		<input type="text" name="video" class="form-control p-form" placeholder="URL do Vídeo" value="{{ old('video') ?? $produto->video }}">
			    	</div>

			    </div>

			    <div class="d-lg-flex mb-3">

			    	<div class="w-100">
			    		<label>Descrição</label>
			    		<textarea name="descricao" class="textarea-form" placeholder="Descreva o produto">{{ old('descricao') ?? $produto->descricao }}</textarea>
			    	</div>

			    </div>

			</div>

		</section>

		<section class="mb-4-5">
			
			<a href="#boxPesosDimensoes" data-bs-toggle="collapse" aria-expanded="false" class="hover-d-none">
				<div class="section-title">
					Pesos e Dimensões 
				</div>
			</a>
			<hr/>

			<div class="collapse" id="boxPesosDimensoes">

			    <div class="d-lg-flex mb-3">

			    	<div class="w-100 me-lg-3 mb-3 mb-lg-0">
			    		<label>Peso</label>
			    		<input type="text" name="peso" class="form-control p-form" placeholder="Ex: 0,150kg" value="{{ old('peso') ?? $produto->peso }}">
			    	</div>

			    	<div class="w-100 me-lg-3 mb-3 mb-lg-0">
			    		<label>Comprimento</label>
			    		<input type="text" name="comprimento" class="form-control p-form" placeholder="Ex: 20 cm" value="{{ old('comprimento') ?? $produto->comprimento }}">
			    	</div>

			    	<div class="w-100 me-lg-3 mb-3 mb-lg-0">
			    		<label>Largura</label>
			    		<input type="text" name="largura" class="form-control p-form" placeholder="Ex: 30 cm" value="{{ old('largura') ?? $produto->largura }}">
			    	</div>

			    	<div class="w-100">
			    		<label>Altura</label>
			    		<input type="text" name="altura" class="form-control p-form" placeholder="Ex: 5 cm" value="{{ old('altura') ?? $produto->altura }}">
			    	</div>

			    </div>

			</div>

		</section>

		<section class="mb-4-5" id="boxSecValores">

			<a href="#boxValores" data-bs-toggle="collapse" aria-expanded="false" class="hover-d-none">
				<div class="section-title">
					Valores e Forma de Pagamento
				</div>
			</a>
			<hr/>

			<div class="collapse" id="boxValores">

				<div class="d-lg-flex mb-3">

			    	<div class="w-100 me-lg-3 mb-3 mb-lg-0">
			    		<label>Exibir Preço</label>
			    		<select name="exibir_preco" class="select-form">
			    			<option value="S">Sim</option>
			    			<option value="N" @if(old('exibir_preco') == 'N' || $produto->exibir_preco == 'N') selected @endif>Não, é Sob Consulta</option>
			    		</select>
			    	</div>

			    	<div class="w-100 me-lg-3 mb-3 mb-lg-0">
			    		<label>Valor a vista</label>
			    		<input type="text" name="valor" class="valor form-control p-form" placeholder="R$" value="{{ old('valor') ?? $produto->valor }}">
			    	</div>
			    	<div class="w-100 me-lg-3 mb-3 mb-lg-0">
			    		<label>Valor a vista promocional</label>
			    		<input type="text" name="valor_promocional" class="valor form-control p-form" placeholder="R$" value="{{ old('valor_promocional') ?? $produto->valor_promocional }}">
			    	</div>
			    	<div class="w-100">
			    		<label>Valor parcelado</label>
			    		<input type="text" name="valor_parcelado" class="form-control p-form" placeholder="Ex: 3x de R$ 97,99" value="{{ old('valor_parcelado') ?? $produto->valor_parcelado }}">
			    	</div>

				</div>

				<hr/>

				<div class="row mb-3">

					<div class="col-lg-4 mb-4">
						<div class="p-3 rounded" style="border:1.5px solid #dbdbdb;">
							<div class="mb-1 w-100">
								<p class="mb-0">Cartão de Crédito</p>
								<hr class="mt-2">
							</div>
							<div class="boxLineParcela">
								@foreach($formasPagamento->where('tipo','CC') as $formaPagamento)
								<div class="mb-2 d-flex align-items-center">
									<input type="text" name="cartao-de-credito[parcelas][]" class="number form-control p-form" placeholder="Parcela" value="{{  $formaPagamento->parcela }}">
									<input type="text" style="width: 45px;" disabled readonly class="form-control p-form text-center px-0 mx-2" placeholder="de">
									<input type="text" name="cartao-de-credito[valores][]" class="valor form-control p-form" placeholder="R$" value="{{  $formaPagamento->valor }}">
									<a href="#" class="btnRemoveParcela btn btn-danger py-2 ms-2">
										<i class="fa fa-trash"></i>
									</a>
								</div>
								@endforeach
								<div class="mb-2 d-flex align-items-center">
									<input type="text" name="cartao-de-credito[parcelas][]" class="number form-control p-form" placeholder="Parcela">
									<input type="text" style="width: 45px;" disabled readonly class="form-control p-form text-center px-0 mx-2" placeholder="de">
									<input type="text" name="cartao-de-credito[valores][]" class="valor form-control p-form" placeholder="R$">
									<a href="#" class="btnRemoveParcela btn btn-danger py-2 ms-2">
										<i class="fa fa-trash"></i>
									</a>
								</div>
							</div>
							<button type="button" class="btn-append btn-append-parcela-cc btn btn-primary py-1-5 fs-13">Adicionar +</button>
						</div>
					</div>
					<div class="col-lg-4 mb-4">
						<div class="p-3 rounded" style="border:1.5px solid #dbdbdb;">
							<div class="mb-1 w-100">
								<p class="mb-0">Cartão de Débito</p>
								<hr class="mt-2">
							</div>
							<div class="boxLineParcela">
								@foreach($formasPagamento->where('tipo','CD') as $formaPagamento)
								<div class="mb-2 d-flex align-items-center">
									<input type="text" name="cartao-de-debito[parcelas][]" class="number form-control p-form" placeholder="Parcela" value="{{  $formaPagamento->parcela }}">
									<input type="text" style="width: 45px;" disabled readonly class="form-control p-form text-center px-0 mx-2" placeholder="de">
									<input type="text" name="cartao-de-debito[valores][]" class="valor form-control p-form" placeholder="R$" value="{{  $formaPagamento->valor }}">
									<a href="#" class="btnRemoveParcela btn btn-danger py-2 ms-2">
										<i class="fa fa-trash"></i>
									</a>
								</div>
								@endforeach
								<div class="mb-2 d-flex align-items-center">
									<input type="text" name="cartao-de-debito[parcelas][]" class="number form-control p-form" placeholder="Parcela">
									<input type="text" style="width: 45px;" disabled readonly class="form-control p-form text-center px-0 mx-2" placeholder="de">
									<input type="text" name="cartao-de-debito[valores][]" class="valor form-control p-form" placeholder="R$">
									<a href="#" class="btnRemoveParcela btn btn-danger py-2 ms-2">
										<i class="fa fa-trash"></i>
									</a>
								</div>
							</div>
							<button type="button" class="btn-append btn-append-parcela-cd btn btn-primary py-1-5 fs-13">Adicionar +</button>
						</div>
					</div>
					<div class="col-lg-4 mb-4">
						<div class="p-3 rounded" style="border:1.5px solid #dbdbdb;">
							<div class="mb-1 w-100">
								<p class="mb-0">Boleto</p>
								<hr class="mt-2">
							</div>
							<div class="boxLineParcela">
								@foreach($formasPagamento->where('tipo','BO') as $formaPagamento)
								<div class="mb-2 d-flex align-items-center">
									<input type="text" name="boleto[parcelas][]" class="number form-control p-form" placeholder="Parcela" value="{{  $formaPagamento->parcela }}">
									<input type="text" style="width: 45px;" disabled readonly class="form-control p-form text-center px-0 mx-2" placeholder="de">
									<input type="text" name="boleto[valores][]" class="valor form-control p-form" placeholder="R$" value="{{  $formaPagamento->valor }}">
									<a href="#" class="btnRemoveParcela btn btn-danger py-2 ms-2">
										<i class="fa fa-trash"></i>
									</a>
								</div>
								@endforeach
								<div class="mb-2 d-flex align-items-center">
									<input type="text" name="boleto[parcelas][]" class="number form-control p-form" placeholder="Parcela">
									<input type="text" style="width: 45px;" disabled readonly class="form-control p-form text-center px-0 mx-2" placeholder="de">
									<input type="text" name="boleto[valores][]" class="valor form-control p-form" placeholder="R$">
									<a href="#" class="btnRemoveParcela btn btn-danger py-2 ms-2">
										<i class="fa fa-trash"></i>
									</a>
								</div>
							</div>
							<button type="button" class="btn-append btn-append-parcela-bo btn btn-primary py-1-5 fs-13">Adicionar +</button>
						</div>
					</div>

				</div>

			</div>

		</section>

		<section class="mb-4-5">
			
			<a href="#boxCategorias" data-bs-toggle="collapse" aria-expanded="false" class="hover-d-none">
				<div class="section-title">
					Categorias 
				</div>
			</a>
			<hr/>

			<div class="collapse" id="boxCategorias">
				<select name="categorias[]" class="selectpicker" multiple data-width="100%" data-live-search="true" title="Selecione">
					@foreach($categorias as $categoria)
						<option value="{{ $categoria->id }}" @if(in_array($categoria->id, $categoriasSelected)) selected @endif>{{ $categoria->nome }}</option>
					@endforeach
				</select>
			</div>

		</section>

		<button type="submit" name="sent" value="ok" class="btn-add mt-3">Salvar Alterações</button>	

    </form>

</div>


@endsection

@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.14.0-beta2/dist/js/bootstrap-select.min.js"></script>
<script>

	@if(\Auth::user()->restringir_unidade == 'S' && \Auth::user()->unidade_id != null)
		jQuery("#unidade_id option:contains('Selecione')").remove();
	@endif
	
	$('.mask_number').mask('00000');

    $(".mask_valor").mask('#.##0,00', {
      reverse: true
    });

    $(".btn-append-parcela-cc").click(function()
    {
    	$(this).prev().append('<div class="mb-2 d-flex align-items-center"> <input type="text" name="cartao-de-credito[parcelas][]" class="number form-control p-form" placeholder="Parcela"> <input type="text" style="width: 45px;" disabled readonly class="form-control p-form text-center px-0 mx-2" placeholder="de"> <input type="text" name="cartao-de-credito[valores][]" class="valor form-control p-form" placeholder="R$"> <a href="#" class="btnRemoveParcela btn btn-danger py-2 ms-2"> <i class="fa fa-trash"></i> </a> </div> </div>');
    });

    $(".btn-append-parcela-cd").click(function()
    {
    	$(this).prev().append('<div class="mb-2 d-flex align-items-center"> <input type="text" name="cartao-de-debito[parcelas][]" class="number form-control p-form" placeholder="Parcela"> <input type="text" style="width: 45px;" disabled readonly class="form-control p-form text-center px-0 mx-2" placeholder="de"> <input type="text" name="cartao-de-debito[valores][]" class="valor form-control p-form" placeholder="R$"> <a href="#" class="btnRemoveParcela btn btn-danger py-2 ms-2"> <i class="fa fa-trash"></i> </a> </div> </div>');
    });

    $(".btn-append-parcela-bo").click(function()
    {
    	$(this).prev().append('<div class="mb-2 d-flex align-items-center"> <input type="text" name="boleto[parcelas][]" class="number form-control p-form" placeholder="Parcela"> <input type="text" style="width: 45px;" disabled readonly class="form-control p-form text-center px-0 mx-2" placeholder="de"> <input type="text" name="boleto[valores][]" class="valor form-control p-form" placeholder="R$"> <a href="#" class="btnRemoveParcela btn btn-danger py-2 ms-2"> <i class="fa fa-trash"></i> </a> </div> </div>');
    });

	$(document).on('click', '.btnRemoveParcela', function(e)
	{
		e.preventDefault();
		$(this).parent().remove();
	});
	
</script>
@endpush

