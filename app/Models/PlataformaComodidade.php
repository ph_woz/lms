<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PlataformaComodidade extends Model
{
    protected $table = 'plataformas_comodidades';

    public $timestamps = false;

    protected $fillable = ['plataforma_id','comodidade_id'];

    public function scopePlataforma($query)
    {
        return $query->where('plataforma_id', session('plataforma_id') ?? \Auth::user()->plataforma_id);
    }

    public static  function boot()
    {
        parent::boot(); 
        static::creating(function ($model) {
            $model->plataforma_id  = session('plataforma_id') ?? Auth()->user()->plataforma_id ?? Plataforma::dominio()->pluck('id')[0] ?? null;
        });
    }
}
