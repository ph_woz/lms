<!DOCTYPE html>
<html lang="pt-br">

<head>

  <!-- Google tag (gtag.js) -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=G-MREB450HCT"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'G-MREB450HCT');
  </script>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="robots" content="noindex, nofollow">
    <meta name="author" content="Pedro Henrique | WOZCODE">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link rel="shortcut icon" href="{{ $plataforma->favicon }}" />
    <title>{{ $getNomePaginaInterno ?? $nomePaginaAtual }} - {{ $nomePlataforma }} | Área Administrativa</title>
    <link rel="stylesheet" href="{{ asset('css/util.css') }}">
    <link rel="stylesheet" href="{{ asset('main/css/app.css') }}">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
    @livewireStyles
    @yield('css')

    <style>

        .sidebar-item { display: none; }

        @foreach($meusModulosSidebar as $modulo)
            #url-{{$modulo->url}} { display: block; }
            #url-{{$modulo->url}} span:after { content: '{{$modulo->texto}}'; }
        @endforeach

        #url-{{Request::segment(2)}} .side-color { color: rgba(255,255,255,0.8); }

    </style>

    @if($getAtualAcl)

        @if($getAtualAcl->nivel == 1)
        <style>

            .aclNivel2 { display: none; }
            .aclNivel3 { display: none; }

        </style>
        @endif

        @if($getAtualAcl->nivel == 2)
        <style>

            .aclNivel3 { display: none; }

        </style>
        @endif
        
    @endif

</head>


<body>
    <div class="wrapper">
        <nav id="sidebar" class="sidebar js-sidebar">
            <div class="sidebar-content js-simplebar pt-3">
                @include('layouts.admin.sidebar')
            </div>
        </nav>

        <div class="main">
            <nav class="navbar navbar-expand navbar-light navbar-bg box-shadow">
                <a class="sidebar-toggle js-sidebar-toggle">
                    <i class="hamburger align-self-center"></i>
                </a>

                <h1 class="h4 m-0 p-0" id="nomePagina">
                    {{ $getNomePaginaInterno ?? $nomePaginaAtual ?? null }}
                </h1>

                <div class="navbar-collapse collapse">
                    <ul class="navbar-nav navbar-align">
                        @include('layouts.admin.topbar')
                    </ul>
                </div>
            </nav>

            <main class="content">
                <div class="container-fluid p-0">

                    @if(!$forbidden['errors'])

                        @yield('content')

                    @else
                        
                        <section class="pt-5">
                            
                            <div class="border col-md-8 offset-md-2 px-4 py-5 text-center bg-white rounded-2">

                                <h1 class="mb-3 display-5">Permissão Negada</h1>
                                <p class="mb-0 fs-20 ls-05 color-444">
                                    {{ $forbidden['label'] }}
                                </p>
                            
                            </div>

                        </section>

                    @endif
                    
                </div>
            </main>

            @include('layouts.admin.footer')
        </div>
    </div>
    <script src="{{ asset('main/js/app.js') }}"></script>

    @if(session()->has('success'))
        <div id="box-toast-success" class="boxToast">
            <div id="alertSuccess" class="toast-success">
                <p class="mt-2 text-white">
                    <span class="weight-800">Sucesso!&nbsp;</span>{{ session('success') }}
                </p>
            </div>
        </div>
        <script>
            setTimeout(function() {

                document.getElementById('box-toast-success').style.display = 'none';

            }, 2500);
        </script>
    @endif

    @if(session()->has('danger'))
        <div id="box-toast-danger" class="boxToast">
            <div id="alertDanger" class="toast-danger">
                <p class="mt-2 text-white">
                    <span class="weight-800">Ops!&nbsp;</span>{{ session('danger') }}
                </p>
            </div>
        </div>
        <script>
            setTimeout(function() {

                document.getElementById('box-toast-danger').style.display  = 'none';

            }, 2500);
        </script>
    @endif

    @livewireScripts
    @yield('js')
    @stack('scripts')
</body>

</html>
