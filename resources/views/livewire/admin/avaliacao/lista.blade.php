<div>

    <form>

        <div class="d-lg-flex mb-3">

            <input wire:model="search" type="search" class="form-control p-form" placeholder="Buscar">

            <select wire:model="status" class="mx-lg-3 my-3 my-lg-0 select-form w-lg-40">
                <option value="0">Ativos</option>
                <option value="1">Desativados</option>
            </select>

            <select wire:model="ordem" class="select-form w-lg-40">
                <option value="nome">Alfabética</option>
                <option value="desc">Mais novos</option>
                <option value="asc">Mais antigos</option>
            </select>

        </div>

        <div class="d-lg-flex mb-3">
            <div class="w-100 me-lg-3 mb-lg-0" wire:ignore>
                <select wire:model="curso_id" class="selectpicker" data-width="100%" data-live-search="true" title="Curso">                
                    @foreach($cursos as $curso)
                        <option value="{{ $curso->id }}">{{ $curso->referencia }}</option>
                    @endforeach
                </select>
            </div>
            <div class="w-100">
                <select wire:model="turma_id" class="select-form">
                    <option value="">Turma</option>
                    @foreach($turmas as $turma)
                        <option value="{{ $turma->id }}">{{ $turma->nome }}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="w-100" wire:ignore>
            <select wire:model="categoriasSelected" class="selectpicker" multiple data-width="100%" data-live-search="true" title="Categorias">
                @foreach($categorias as $categoria)
                    <option value="{{ $categoria->id }}">{{ $categoria->nome }}</option>
                @endforeach
            </select>
        </div>
            
    </form>

    <div class="mt-5 d-flex justify-content-between">

        <div class="align-self-end mb-2">
            <span class="resultados-encontrados">
                {{ count($avaliacoes) }} resultados de {{ $countAvaliacoes }}.
            </span>
        </div>

        <div class="mb-3">
            <a href="{{ route('admin.avaliacao.add') }}" class="btn-add aclNivel2">
                + Adicionar
            </a>
        </div>

    </div>

    <div class="row">
        <div class="col-12">

            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col">Referência</th>
                                    <th scope="col">
                                        <span class="float-end">
                                            Ações
                                        </span>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($avaliacoes as $avaliacao)
                                    <tr>
                                        <td>{{ $avaliacao->referencia }}</td>
                                        <td>
                                            <span class="float-end">

                                                <a href="{{ route('admin.avaliacao.info', ['id' => $avaliacao->id]) }}" class="btn bg-base text-white">
                                                    <i class="fa fa-eye"></i>
                                                </a>

                                            </span>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            {{ $avaliacoes->links() }}

        </div>
    </div>

</div>