<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Http\Request;
use App\Models\Plataforma;
use App\Models\User;
use App\Models\UserContrato;

class EmailContrato extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $userContrato;

    public function __construct(User $user, UserContrato $userContrato)
    {
        $this->user = $user;
        $this->userContrato = $userContrato;
    }

    public function build()
    {
        $plataforma = Plataforma::plataforma()->first();

        $assunto = $this->userContrato->referencia . ' | '. $plataforma->nome;

        $token = $this->generateToken();
        $this->user->update(['token' => $token]);

        $linkBotaoAssinatura = \Request::root() . '/solicitar-assinatura-contrato-digital/token/'.$token.'/email/'.$this->user->email.'/id/'.$this->user->id.'/'.$this->userContrato->id;

        return $this->view('mail.contrato')
            ->subject($assunto)
            ->from($plataforma->email, $plataforma->nome)
            ->replyTo($plataforma->email, $plataforma->nome)
            ->attach($this->userContrato->pdf)
            ->with([
                'nome' => \App\Models\Util::replacePrimeiroNome($this->user->nome),
                'linkBotaoAssinatura' => $linkBotaoAssinatura,
            ]);
    }

    public function generateToken()
    {
        return md5(rand(1, 10) . microtime());
    }

}
