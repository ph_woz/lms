<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Plataforma;
use App\Models\Util;

class RecuperarSenha extends Mailable
{
    use Queueable, SerializesModels;

    public function generateToken()
    {
        return md5(rand(1, 10) . microtime());
    }

    public function build()
    {
        $plataforma = Plataforma::plataforma()->first();

        $user = User::plataforma()->where('email', request('email'))->first();

        $nome    =  Util::getPrimeiroSegundoNome($user->nome);
        $email   =  $user->email;
        $id      =  $user->id;
        $token   =  $this->generateToken();
        $assunto =  'Recuperar Senha | ' . $plataforma->nome;

        $user->update(['token' => $token]);

        return $this->view('mail.recuperar-senha')
                ->subject($assunto)
                ->from($plataforma->email, $plataforma->nome)
                ->replyTo($plataforma->email, $plataforma->nome)
                ->with([
                    'nome'           => $nome,
                    'email'          => $email,
                    'id'             => $id,
                    'token'          => $token,
                    'plataformaNome' => $plataforma->nome,
                ]);
    }
}
