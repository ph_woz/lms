<div>

    @if($diaBloqueio)
    <div class="alert alert-warning mb-4 px-4 py-3 shadow d-flex align-items-center" role="alert">
        <i class="fas fa-exclamation-triangle"></i>
        <span class="weight-600 ms-3">
            Atenção! Regularize o pagamento da plataforma para que a mesma não seja desativada.
        </span>
    </div>
    @endif

    @if(\App\Models\Unidade::checkActive() === true && \Auth::user()->restringir_unidade == 'N' && count($unidades) > 0)
    <div class="mb-4 w-100" wire:ignore>
        <select wire:model="unidade_id" class="select-form w-100">
            <option value="">Unidades</option>
            @foreach($unidades as $unidade)
                <option value="{{ $unidade->id }}">{{ $unidade->nome }}</option>
            @endforeach
        </select>  
    </div>
    @endif

	<div class="d-xl-flex justify-content-between">
		
		@foreach($meses as $numero => $nome)
			<div class="d-inline-block mb-3 w-100 text-center me-xl-1" id="n_mes_{{$numero}}">
				<a href="?mes={{ $numero }}&ano={{ $ano }}@if(isset($unidade_id))&unidade_id={{ $unidade_id }}@endif" class="d-block w-100 btn-add fs-13 rounded">
					{{ $nome }}
				</a>
			</div>
		@endforeach

	</div>

	<hr/>

	<div class="row">

		<div class="col-lg-3 mb-4">
            <a href="{{ route('admin.aluno.lista', ['ano' => $ano, 'mes' => $mes, 'data_field' => 'users.created_at']) }}" class="hover-d-none">
                <div class="text-white bg-primary rounded-2 shadow p-3" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Novos alunos ativos">
                    <div class="d-flex justify-content-between align-items-center mb-2">
                        <span class="d-block fs-20 weight-800 text-shadow-2">
                        	{{ $countAlunos }}
                        </span>
                        <i class="fa fa-users fs-25"></i>
                    </div>
                    <span class="d-block fs-17 weight-600 text-shadow-2">Alunos</span>
                </div>
            </a>
		</div>
		<div class="col-lg-3 mb-4">
            <a href="{{ route('admin.contato.lista', ['tipo' => 'L', 'ano' => $ano, 'mes' => $mes]) }}" class="hover-d-none">
                <div class="text-white bg-primary rounded-2 shadow p-3" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Leads capturados">
                    <div class="d-flex justify-content-between align-items-center mb-2">
                        <span class="d-block fs-20 weight-800 text-shadow-2">
                        	{{ $countLeads }}
                        </span>
                        <i class="fas fa-envelope fs-25"></i>
                    </div>
                    <div class="d-lg-flex justify-content-between align-items-center">
                        <span class="d-block fs-17 weight-600 text-shadow-2">Leads</span>
                        <span class="d-block fs-13 weight-600 text-shadow-2">{{ $countLeadsPendentes }} Pendentes</span>
                    </div>
                </div>
            </a>
		</div>
		<div class="col-lg-3 mb-4">
            <a href="{{ route('admin.contato.lista', ['tipo' => 'S', 'ano' => $ano, 'mes' => $mes]) }}" class="hover-d-none">
                <div class="text-white bg-primary rounded-2 shadow p-3" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Alunos que acionaram a área de Suporte">
                    <div class="d-flex justify-content-between align-items-center mb-2">
                        <span class="d-block fs-20 weight-800 text-shadow-2">
                        	{{ $countSuportes }}
                        </span>
                        <i class="fas fa-headset fs-25"></i>
                    </div>
                    <div class="d-lg-flex justify-content-between align-items-center">
                        <span class="d-block fs-17 weight-600 text-shadow-2">Suporte</span>
                        <span class="d-block fs-13 weight-600 text-shadow-2">{{ $countSuportesPendentes }} Pendentes</span>
                    </div>
                </div>
            </a>
		</div>
		<div class="col-lg-3 mb-4">
            <a href="{{ route('admin.aluno.lista', ['ano' => $ano, 'mes' => $mes, 'trilha_tipo' => 'concluiram']) }}" class="hover-d-none">
                <div class="text-white bg-primary rounded-2 shadow p-3" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Alunos que concluíram alguma trilha">
                    <div class="d-flex justify-content-between align-items-center mb-2">
                        <span class="d-block fs-20 weight-800 text-shadow-2">
                        	{{ $countConclusaoTrilha }}
                        </span>
                        <i class="fas fa-certificate fs-25"></i>
                    </div>
                    <span class="d-block fs-17 weight-600 text-shadow-2">Conclusão de Trilha</span>
                </div>
            </a>
		</div>
	
	</div>

    <div class="modal fade" id="ModalInadimplencia" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header bg-warning">
                    <h3 class="modal-title text-dark weight-600" id="exampleModalLabel">Atenção! Assinatura não foi renovada</h3>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body py-4 px-4">
                    <p class="fs-17 mb-0">
                        Ainda não identificamos o pagamento da sua assinatura para esta plataforma.
                        Para evitar a suspensão dos serviços para todos os usuários em {{ $diaBloqueio }}, regularize o pagamento agora mesmo.
                    </p>
                </div>
                <div class="modal-footer">
                    <a href="{{ route('admin.billing') }}" class="btn btn-success fs-16 weight-600 py-2 text-shadow-1 box-shadow">
                        Regularizar o pagamento agora
                    </a>
                </div>
            </div>
        </div>
    </div>
    
</div>