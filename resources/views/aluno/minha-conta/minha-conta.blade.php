@extends('layouts.aluno')

@section('css')
   <style>

      .square-125 { width: 125px; height: 125px; }
      .object-fit-contain { object-fit: contain; }
      .border-profile { border:3px solid #bfbdbd; }

      #url-informacoes-gerais { background: #222e3c!important; }

   </style>
@endsection

@section('content')

@section('seo_title', 'Acadêmico')
<div class="container-fluid px-xl-5 mt-5">

	<div class="row">

        <div class="col-lg-4">
        
            <div class="mb-4 p-5 rounded-10 box-shadow bg-14">

                @include('aluno.minha-conta.componentes.navbar')

            </div>

        </div>

		<div class="col-lg-8">
		
			<div class="mb-4 p-5 rounded-10 box-shadow bg-14">

                <form method="POST" enctype="multipart/form-data">
                @csrf

                    <input type="file" name="foto" style="display:none" id="input-file-foto" onchange="loadFile(event)" />

                    <div>
                        <img src="{{ $user->foto_perfil ?? asset('images/camera.JPG') }}" id="fotoPreview" class=" @if($user->foto_perfil == null) p-3 @endif square-100 border-profile rounded-circle object-fit-cover cursor-pointer" onclick="setFotoPerfil()">
                    </div>

                    <div class="mt-4-5 mb-3">

                        <label class="text-light">Profissão</label>
                        <input type="text" name="profissao" class="mb-2 form-control p-form bg-444 border-0 text-white py-3 color-placeholder-light" placeholder="Sua profissão" value="{{ $user->profissao }}">

                    </div>

                    <div class="w-100">
                        <label class="text-light">Descrição</label>
                        <textarea name="descricao" class="form-control p-form bg-444 border-0 text-white py-3 color-placeholder-light h-min-125" placeholder="Você pode falar o que quiser aqui. Exemplo: Um pouco sobre você, o que você gosta de fazer, o que te motiva e os seus objetivos." required>{{ $user->descricao }}</textarea>
                    </div>

                    <button type="submit" name="sent" value="S" class="btn-suporte-enviar mt-4-5 px-4-5">
                        Salvar
                    </button>

                </form>

			</div>

		</div>

	</div>

</div>
@endsection

@push('scripts')
<script>
    
    function setFotoPerfil()
    {
        document.getElementById('input-file-foto').click();
    }

    var loadFile = function(event) {
    var reader = new FileReader();
    reader.onload = function(){
      var output = document.getElementById('fotoPreview');
      output.src = reader.result;
      output.classList.remove('p-3');
    };
    reader.readAsDataURL(event.target.files[0]);
    };

</script>
@endpush