<?php

namespace App\Http\Controllers\Aluno;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Experiencia;
use App\Models\Formacao;

class MinhaContaController extends Controller
{
    public function minhaConta()
    {
        $getNomePage = 'Minha Conta';

        $user = User::plataforma()->where('id', \Auth::id())->first();

        if(request('sent'))
        {
            $user->update(['profissao' => request('profissao'), 'descricao' => request('descricao')]);

            $this->setFotoPerfil($user->id);

            return back()->with('success', 'Informações atualizada no seu perfil com êxito.');
        }

        return view('aluno.minha-conta.minha-conta', compact('getNomePage','user') );
    }

    public function setFotoPerfil($user_id)
    {
        $file = request()->file('foto');

        if($file)
        {
            $diretorio = 'plataformas/'.session('plataforma_id').'/users/'.$user_id;
            $filename = 'foto_perfil.'.$file->getClientOriginalExtension();

            $path = Storage::putFileAs($diretorio, $file, $filename, 'public');
            $linkStorage = \App\Models\Util::getLinkStorage();
            $caminho = $linkStorage . $path;

            User::plataforma()->where('id', $user_id)->update(['foto_perfil' => $caminho]);
        }
    }

    public function experienciaAdd()
    {
        $getNomePage = 'Adicionar Experiência Profissional';

        if(request('sent'))
        {
            Experiencia::create(request()->all() + ['user_id' => \Auth::id()]);

            return back()->with('success', 'Experiência adicionada ao seu perfil com êxito.');
        }

        return view('aluno.minha-conta.experiencia-add', compact('getNomePage') );
    }

    public function experienciaEditar($experiencia_id)
    {
        $getNomePage = 'Editar Experiência Profissional';

        $experiencia = Experiencia::plataforma()->where('id', $experiencia_id)->where('user_id', \Auth::id())->first();

        if(request('sent'))
        {
            $experiencia->update(request()->all() + ['user_id' => \Auth::id()]);

            return back()->with('success', 'Experiência atualizada ao seu perfil com êxito.');
        }

        return view('aluno.minha-conta.experiencia-editar', compact('getNomePage','experiencia') );
    }

    public function experienciaLista()
    {
        $getNomePage = 'Experiência Profissionais';

        $experiencias = Experiencia::plataforma()->where('user_id', \Auth::id())->orderBy('data_inicio')->get();

        if(request('deletar'))
        {
            Experiencia::plataforma()->where('user_id', \Auth::id())->where('id', request('registro_id'))->delete();

            return back()->with('success', 'Experiência deletada do seu perfil com êxito.');
        }

        return view('aluno.minha-conta.experiencia-lista', compact('getNomePage','experiencias') );
    }


    public function formacaoAdd()
    {
        $getNomePage = 'Adicionar Formação Acadêmica';

        if(request('sent'))
        {
            Formacao::create(request()->all() + ['user_id' => \Auth::id()]);

            return back()->with('success', 'Formação adicionada ao seu perfil com êxito.');
        }

        return view('aluno.minha-conta.formacao-add', compact('getNomePage') );
    }

    public function formacaoEditar($formacao_id)
    {
        $getNomePage = 'Editar Formação Acadêmica';

        $formacao = Formacao::plataforma()->where('id', $formacao_id)->where('user_id', \Auth::id())->first();

        if(request('sent'))
        {
            $formacao->update(request()->all() + ['user_id' => \Auth::id()]);

            return back()->with('success', 'Formação atualizada ao seu perfil com êxito.');
        }

        return view('aluno.minha-conta.formacao-editar', compact('getNomePage','formacao') );
    }

    public function formacaoLista()
    {
        $getNomePage = 'Formações Acadêmicas';

        $formacoes = Formacao::plataforma()->where('user_id', \Auth::id())->orderBy('data_inicio')->get();

        if(request('deletar'))
        {
            Formacao::plataforma()->where('user_id', \Auth::id())->where('id', request('registro_id'))->delete();

            return back()->with('success', 'Formação deletada do seu perfil com êxito.');
        }

        return view('aluno.minha-conta.formacao-lista', compact('getNomePage','formacoes') );
    }

    public function alterarSenha()
    {
        $getNomePage = 'Alterar Senha';

        if(request('sent'))
        {
            if(\Auth::attempt(['id' => \Auth::id(), 'password' => request('atual_password'), 'status' => 0]))
            {
                $password = Hash::make(request('new_password'));

                User::plataforma()->where('id', \Auth::id())->update(['password' => $password]);
            
                return back()->with('success','Sua Senha foi atualizada com êxito.');
            
            } else {

                return back()->with('danger','Sua Senha atual não confere.');
            }
        }

        return view('aluno.minha-conta.alterar-senha', compact('getNomePage') );
    }

}