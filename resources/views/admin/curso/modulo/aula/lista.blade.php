@extends('layouts.admin.master')

@section('content')
    
    @livewire('admin.curso.modulo.aula.lista', ['curso_id' => $curso_id, 'modulo_id' => $modulo_id])

@endsection

@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>
<script src="https://cdn.tiny.cloud/1/f50dygt4s4q2k3urd4bh4c2hkjb74m6exr798tjwbayaa0om/tinymce/5/tinymce.min.js"></script>
<script>
	tinymce.init({
		selector: '.descricao',
		language: 'pt_BR',
		height: 300,
		plugins: 'image lists link code',
		toolbar: "image | undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link | code"
	});
</script>
<script>
	window.addEventListener('dissmiss-modal', event => {
		$(".modal").modal('hide');
	});

	var myModal = new bootstrap.Modal(document.getElementById("ModalEditarAula"), {});
	document.onreadystatechange = function () {
	  myModal.show();
	};
</script>
@endpush