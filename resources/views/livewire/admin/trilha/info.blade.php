<div>

	<section>

		<div class="d-flex justify-content-end mb-3">

			<a href="{{ route('admin.aluno.lista', ['add' => 'S', 'add_alunos_para_trilha_id' => $trilha->id]) }}" class="btn btn-primary fs-13 py-1-5 mb-1">
				Matricular alunos em massa
			</a>

			<a href="{{ route('admin.trilha.editar-secoes', ['id' => $trilha->id]) }}" class="btn btn-primary fs-13 py-1-5 ms-2 mb-1">
				Seções descritivas
			</a>

		</div>

		<div class="section-title rounded-bottom-0 d-flex justify-content-between align-items-center">
			{{ $trilha->referencia }}
			<div>

				<a href="#" data-bs-toggle="modal" data-bs-target="#ModalMetaDados" class="btn btn-info me-2 fs-14 mb-1 py-1 px-2-3">
					<i class="fa fa-align-left"></i>
				</a>
				<a href="/admin/trilha/editar/{{$trilha->id}}" class="aclNivel2 btn btn-warning text-dark fs-14 mb-1 py-1 px-2">
					<i class="fa fa-edit"></i>
				</a>
				<a href="#" data-bs-toggle="modal" data-bs-target="#ModalDeletarTrilha" class="aclNivel3 btn btn-danger mb-1 fs-15 py-1 px-2 ms-2">
					<i class="fa fa-trash"></i>
				</a>
			</div>
		</div>

		<div class="bg-white border p-4 d-lg-flex justify-content-between align-items-center">

			<div class="mb-2 mb-lg-0">
				<p class="mb-0 weight-600 color-444">Publicado</p>
				<div>
					@if($trilha->publicar == 'S')
                     <a href="/curso/{{ $trilha->slug }}" target="_blank" class="hover-d-none">
                        <div class="bg-primary text-white rounded h-40px d-flex align-items-center justify-content-center w-150px">
                           <div class="weight-500 fs-13">
                              <span>
                                 Sim
                              </span>
                           </div>
                           <div>
                              <div class="mx-2-3" style="width: 2.5px;height: 40px; background: #000; opacity: .2;"></div>
                           </div>
                           <div class="weight-500 fs-13"> 
                              <span class="me-1">Visualizar</span>
                              <i class="fas fa-external-link-alt"></i>
                           </div>
                        </div>
                     </a>
                     @else
                        <div class="bg-secondary text-white px-3 rounded h-40px d-flex align-items-center justify-content-center">
                           <div class="weight-500 fs-13">
                              <span>
                                 Não
                              </span>
                           </div>
                        </div>
                     @endif
				</div>
			</div>

			<div class="mb-2 mb-lg-0">
				<p class="mb-0 weight-600 color-444">Nome Comercial</p>
				<p class="mb-0">
					{{ $trilha->nome }}
				</p>
			</div>

			<div class="mb-2 mb-lg-0">
				<p class="mb-0 weight-600 color-444">Certificado de Conclusão</p>
				@if($certificado)
					<a href="{{ route('admin.certificado.info', ['id' => $certificado->id]) }}" class="color-blue-info-link">
						{{ $certificado->referencia }}
					</a>
				@else
					<p class="mb-0">
						Não definido
					</p>
				@endif
			</div>

			@if($atestado)
			<div class="mb-2 mb-lg-0">
				<p class="mb-0 weight-600 color-444">Atestado de Matrícula</p>
				<a href="{{ route('admin.certificado.info', ['id' => $atestado->id]) }}" class="color-blue-info-link">
					{{ $atestado->referencia }}
				</a>
			</div>
			@endif

			@if($unidade)
			<div class="mb-2 mb-lg-0">
				<p class="mb-0 weight-600 color-444">Unidade</p>
				<a href="{{ route('admin.unidade.info', ['id' => $unidade->id]) }}" class="color-blue-info-link">
					{{ $unidade->nome }}
				</a>
			</div>
			@endif

		</div>

		@if(isset($formulario))
		<div class="bg-white border p-4">

			<div class="row">
				<div class="col-lg-6 mb-2 mb-lg-0">
					<p class="mb-0 weight-600 color-444">
						Formulário de Inscrição
						@if($formulario)
							<a href="/f/{{$formulario->slug}}/{{$formulario->id}}/t/{{$trilha->id}}" target="_blank" class="color-blue-info-link hover-d-none">
								- Abrir Link <i class="fas fa-link"></i>
							</a>
						@endif
					</p>
					@if($formulario)
						<a href="{{ route('admin.formulario.info', ['id' => $formulario->id]) }}" class="color-blue-info-link">
							{{ $formulario->referencia }}
						</a>
					@else
						<p class="mb-0">
							Não definido
						</p>
					@endif
				</div>

				<div class="col-lg-6 mb-2 mb-lg-0">
					<p class="mb-0 weight-600 color-444">Tipo de Inscrição de Matrícula</p>
					<p class="mb-0">
						@if($trilha->tipo_inscricao == 'requer-aprovacao')
							Requer aprovação
						@else
							Auto inscrição
						@endif
					</p>
				</div>
			</div>
			
		</div>
		@endif

		<div class="bg-white border p-4 d-lg-flex justify-content-between">

			<div class="mb-2 mb-lg-0">
				<p class="mb-0 weight-600 color-444">Turmas ({{ count($turmas) }})</p>
				@if(count($turmas) > 0)
					<ul class="list-group list-group-flush">
				  		@foreach($turmas as $turma)
				  			<a href="{{ route('admin.turma.info', ['id' => $turma->id]) }}" class="list-group-item">
				  				{{ $turma->nome }}
				  			</a>
				  		@endforeach
				  	</ul>
				@else
					<p class="mb-0 font-nunito">Não definido</p>
				@endif
			</div>

		</div>

		<div class="bg-white border p-4 d-lg-flex justify-content-between">

			<div class="mb-2 mb-lg-0">
				<p class="mb-0 weight-600 color-444">Categorias</p>
				@if(count($categorias) > 0)
					<ul class="list-group list-group-flush">
				  		@foreach($categorias as $categoria)
				  			<li class="list-group-item">{{ $categoria }}</li>
				  		@endforeach
				  	</ul>
				@else
					<p class="mb-0 font-nunito">Não definido</p>
				@endif
			</div>

		</div>
		
	</section>

	<div class="modal fade" id="ModalMetaDados" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	  	<div class="modal-dialog modal-dialog-centered" role="document">
	    	<div class="modal-content">
	      		<div class="modal-header">
	        		<h3 class="modal-title">Metadados</h3>
	    			<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
	      		</div>
	      		<div class="modal-body">

					<ul class="list-group list-group-flush">
	  					<li class="list-group-item">
							<span class="mb-0 weight-600 color-444">Cadastrante:</span>
							@if($cadastrante)
								<a href="/admin/colaborador/info/{{$cadastrante->id}}" class="color-blue-info-link">
									{{ $cadastrante->nome }}
								</a>
							@else
								Não definido
							@endif
	  					</li>   
	  					<li class="list-group-item">
							<span class="mb-0 weight-600 color-444">Status:</span>
							@if($trilha->status == 0)
								<span class="text-success weight-700">Ativo</span>
							@else
								<span class="text-danger weight-700">Desativado</span>
							@endif
	  					</li>
	  					<li class="list-group-item">
							<span class="mb-0 weight-600 color-444">Data de Cadastro:</span>
							@if($trilha->created_at)
								<span class="mb-0">{{ $trilha->created_at->format('d/m/Y H:i') ?? null }} ({{$trilha->created_at->diffForHumans()}}) </span>
	  						@else
	  							Não definido
	  						@endif
	  					</li>   
	  				</ul>

	      		</div>
	    	</div>
	  	</div>
	</div>

	<form wire:submit.prevent="delete" method="POST">
		<div wire:ignore class="modal fade" id="ModalDeletarTrilha" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  		<div class="modal-dialog modal-dialog-centered">
	    		<div class="modal-content">
	      			<div class="modal-header">
	        			<h3 class="modal-title" id="exampleModalLabel">Deletar Trilha</h3>
	        			<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
	      			</div>
	      			<div class="modal-body text-center py-5">
	        			<p class="m-0 fs-16">
	        				Você tem certeza que deseja deletar esta trilha?
	        				<br>
	        				Todas as informações serão perdidas definitivamente.
	        			</p>
	      			</div>
	      			<div class="modal-footer">
	      				<div class="d-flex w-100">
		      				<input type="password" wire:model="password_to_delete" class="w-100 form-control p-form me-3" placeholder="Digite sua senha" required>
		        			<button type="submit" class="btn btn-danger weight-600">Confirmar</button>
	    				</div>
	      			</div>
	    		</div>
	  		</div>
		</div>
	</form>

	@include('livewire.util.toast')

</div>

