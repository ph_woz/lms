<div>

	<section class="mb-4-5">

		<div class="section-title rounded-bottom-0 d-flex justify-content-between align-items-center">
			{{ $comunicado->assunto }}
			<div>

				<a href="#" data-bs-toggle="modal" data-bs-target="#ModalMetaDados" class="btn btn-info me-2 fs-14 mb-1 py-1 px-2-3">
					<i class="fa fa-align-left"></i>
				</a>
				<a href="/admin/comunicado/editar/{{$comunicado->id}}" class="aclNivel2 btn btn-warning text-dark fs-14 mb-1 py-1 px-2">
					<i class="fa fa-edit"></i>
				</a>
				<a href="#ModalDeletarComunicado" data-bs-toggle="modal" class="aclNivel3 btn btn-danger mb-1 fs-15 py-1 px-2 ms-2">
					<i class="fa fa-trash"></i>
				</a>
			</div>
		</div>

		<div class="bg-white border p-4 d-lg-flex justify-content-between">

			<div class="mb-2 mb-lg-0">
				<p class="mb-0 weight-600 color-444">Descrição</p>
				<div>{!! nl2br($comunicado->descricao) !!}</div>
			</div>

		</div>


	</section>

	<section>

		<div class="section-title rounded-bottom-0 d-flex justify-content-between align-items-center">
			Alunos destinatários ({{ count($alunos) }})
			<div>
				<a href="{{ route('admin.aluno.lista') }}?add=S&add_alunos_para_comunicado_id={{$comunicado->id}}" class="btn btn-primary fs-13 py-2">
					+ Adicionar Alunos
				</a>
			</div>
		</div>

		<div class="bg-white border p-4">

            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">Nome</th>
                            <th scope="col">Email</th>
                            <th scope="col">Último Login</th>
                            <th scope="col">
                            	<span class="float-end">
                            		Confirmação de Leitura
                            	</span>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($alunos as $aluno)
                            <tr>
                                <td>
                                	<a href="{{ route('admin.aluno.info', ['id' => $aluno->id]) }}" class="text-dark"> 
                                		{{ $aluno->nome }}
                                	</a>
                                </td>
                                <td>{{ $aluno->email }}</td>
						      	<td>
							      	@if($aluno->data_ultimo_acesso)
							      		<span class="cursor-pointer" title="{{ \App\Models\Util::replaceDateTimePt($aluno->data_ultimo_acesso) }}">
							      			{{ \Carbon\Carbon::createFromTimeStamp(strtotime($aluno->data_ultimo_acesso))->diffForHumans() }}
							      		</span>
							      	@else
										Ainda não acessou
							      	@endif
						      	</td>
                                <td>
                                	<span class="float-end me-1">
								      	@if($aluno->data_visualizacao)
								      		<span class="cursor-pointer" title="{{ \App\Models\Util::replaceDateTimePt($aluno->data_visualizacao) }}">
								      			{{ \Carbon\Carbon::createFromTimeStamp(strtotime($aluno->data_visualizacao))->diffForHumans() }}
								      		</span>
								      	@else
											Ainda não visualizou
								      	@endif
                                	</span>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>

		</div>


	</section>

	<div class="modal fade" id="ModalMetaDados" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	  	<div class="modal-dialog modal-dialog-centered" role="document">
	    	<div class="modal-content">
	      		<div class="modal-header">
	        		<h3 class="modal-title">Metadados</h3>
	    			<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
	      		</div>
	      		<div class="modal-body">

					<ul class="list-group list-group-flush">
	  					<li class="list-group-item">
							<span class="mb-0 weight-600 color-444">Cadastrante:</span>
							@if($cadastrante)
								<a href="/admin/colaborador/info/{{$cadastrante->id}}" class="color-blue-info-link">
									{{ $cadastrante->nome }}
								</a>
							@else
								Não definido
							@endif
	  					</li>   
	  					<li class="list-group-item">
							<span class="mb-0 weight-600 color-444">Arquivado:</span>
							@if($comunicado->arquivar == 'N')
								<span class="text-success weight-700">Não</span>
							@else
								<span class="text-danger weight-700">Sim</span>
							@endif
	  					</li>
	  					<li class="list-group-item">
							<span class="mb-0 weight-600 color-444">Data de Cadastro:</span>
							@if($comunicado->created_at)
								<span class="mb-0">{{ $comunicado->created_at->format('d/m/Y H:i') ?? null }} ({{$comunicado->created_at->diffForHumans()}}) </span>
	  						@else
	  							Não definido
	  						@endif
	  					</li>   
	  				</ul>

	      		</div>
	    	</div>
	  	</div>
	</div>

	<form wire:submit.prevent="delete" method="POST">
		<div wire:ignore class="modal fade" id="ModalDeletarComunicado" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  		<div class="modal-dialog modal-dialog-centered">
	    		<div class="modal-content">
	      			<div class="modal-header">
	        			<h3 class="modal-title" id="exampleModalLabel">Deletar Comunicado</h3>
	        			<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
	      			</div>
	      			<div class="modal-body text-center py-5">
	        			<p class="m-0 fs-16">
	        				Você tem certeza que deseja deletar esta comunicado?
	        				<br>
	        				Todas as informações serão perdidas definitivamente.
	        			</p>
	      			</div>
	      			<div class="modal-footer">
	      				<div class="d-flex w-100">
		      				<input type="password" wire:model="password_to_delete" class="w-100 form-control p-form me-3" placeholder="Digite sua senha" required>
		        			<button type="submit" class="btn btn-danger weight-600">Confirmar</button>
	    				</div>
	      			</div>
	    		</div>
	  		</div>
		</div>
	</form>

	@include('livewire.util.toast')

</div>

