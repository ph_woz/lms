<?php

namespace App\Http\Controllers\SiteProprio;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\PaginaConteudo;
use App\Models\Depoimento;
use App\Models\Banner;
use App\Models\TrilhaTurma;
use App\Models\SecaoDescritiva;
use App\Models\Trilha;

class Cefaep2Controller extends Controller
{
    public function index()
    {
        $seo = PaginaConteudo::plataforma()->where('url','/')->select('plataforma_id','seo_title','seo_keywords','seo_description')->first();

        $banners = Banner::plataforma()->ativo()->orderBy('ordem')->get();
        
        // $cursos = Trilha::select('nome','slug','nivel','foto_miniatura')->plataforma()->ativo()->publico()->orderBy('nome')->get();

        $fotosAlunosFormados = Depoimento::select('foto')->plataforma()->ativo()->orderBy('id','desc')->get()->pluck('foto')->toArray();

    	return view('sites-proprios.cefaep2.home', compact('seo','banners','fotosAlunosFormados') );
    }

    public function sobreNos()
    {
        $pagina = PaginaConteudo::select('conteudo','seo_title','seo_keywords','seo_description')
            ->plataforma()
            ->ativo()
            ->where('url','/sobre-nos')
            ->first();

        return view('sites-proprios.cefaep2.sobre-nos', compact('pagina') );
    }

    public function comoFunciona()
    {
        $pagina = PaginaConteudo::select('seo_title','seo_keywords','seo_description')
            ->plataforma()
            ->ativo()
            ->where('url','/como-funciona')
            ->first();

        return view('sites-proprios.cefaep2.como-funciona', compact('pagina') );
    }

    public function faq()
    {
        $pagina = PaginaConteudo::select('seo_title','seo_keywords','seo_description')
            ->plataforma()
            ->ativo()
            ->where('url','/faq')
            ->first();

        return view('sites-proprios.cefaep2.faq', compact('pagina') );
    }

    public function contato()
    {
        $pagina = PaginaConteudo::select('plataforma_id','seo_title','seo_keywords','seo_description')
            ->plataforma()
            ->ativo()
            ->where('url','/contato')
            ->first();

        if(request('sentContato'))
        {
            if(request('check'))
            {
                return back()->with('danger', 'SPAM Detected!');
            }

            \App\Models\Contato::create(request()->all() + [
                'plataforma_id' => $pagina->plataforma_id,
                'assunto'       => 'Fale Conosco',
                'tipo'          => 'L',
            ]);

            return back()->with('success', 'Inscrição enviada com êxito.');
        }

        return view('sites-proprios.cefaep2.contato', compact('pagina') );
    }


    public function curso($curoSlug)
    {
        $curso = Trilha::plataforma()->ativo()->publico()->where('slug', $curoSlug)->first();

        $secoesDescritivas = SecaoDescritiva::plataforma()->trilha()->where('registro_id', $curso->id)->orderBy('ordem')->get();

        $corpoDocente = \App\Models\CorpoDocente::plataforma()->trilha()->where('registro_id', $curso->id)->get();

        $coordenador = \App\Models\User::select('users.nome','users.foto_perfil','users.naturalidade','users.descricao','users.celular')->plataforma()->ativo()->where('id', $curso->coordenador_id)->first();

        if($curso->exibir_turmas_como_cc == 'S')
        {
            $turmasIds = \App\Models\TrilhaTurma::select('turma_id')->plataforma()->where('trilha_id', $curso->id)->get()->pluck('turma_id')->toArray();

            $turmas = \App\Models\Turma::select('nome')->plataforma()->ativo()->whereIn('id', $turmasIds)->orderBy('nome')->get();
        
        } else {

            $turmas = [];
        }

        return view('sites-proprios.cefaep2.curso', compact('curso','turmas','secoesDescritivas','coordenador','corpoDocente') );
    }

}