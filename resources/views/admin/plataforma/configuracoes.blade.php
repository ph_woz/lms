@extends('layouts.admin.master')

@section('css')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.14.0-beta2/dist/css/bootstrap-select.min.css">
@endsection

@section('content')
    
<div>

    <form method="POST" enctype="multipart/form-data" class="mb-4">
    @csrf

    <section class="mb-4-5">
      
      <a href="#boxInfoPrincipal" data-bs-toggle="collapse" aria-expanded="false" class="hover-d-none">
        <div class="section-title">
          Informações Gerais
        </div>
      </a>
      <hr/>
      
      <div class="collapse show" id="boxInfoPrincipal">

          <div class="d-lg-flex mb-3">

            <div class="w-100 me-lg-3 mb-3 mb-lg-0">
              <label>Logotipo</label>
              @if($plataforma->logotipo)
                <a href="{{ $plataforma->logotipo }}" class="weight-600 fs-11" target="_blank">
                  visualizar anexo
                </a>
              @endif
              <input type="file" name="logo" id="logo" class="file-form">
            </div>

            <div class="w-100 me-lg-3 mb-3 mb-lg-0">
              <label>Favicon</label>
              @if($plataforma->favicon)
                <a href="{{ $plataforma->favicon }}" class="weight-600 fs-11" target="_blank">
                  visualizar anexo
                </a>
              @endif
              <input type="file" name="fav" class="file-form">
            </div>

            <div class="w-100 me-lg-3 mb-3 mb-lg-0">
              <label>Fundo de Login</label>
              @if($plataforma->fundo_login)
                <a href="{{ $plataforma->fundo_login }}" class="weight-600 fs-11" target="_blank">
                  visualizar anexo
                </a>
              @endif
              <input type="file" name="fundoLogin" class="file-form">
            </div>

            <div class="w-100">
              <label>Jumbotron</label>
              @if($plataforma->jumbotron)
                <a href="{{ $plataforma->jumbotron }}" class="weight-600 fs-11" target="_blank">
                  visualizar anexo
                </a>
              @endif
              <input type="file" name="jumb" class="file-form">
            </div>

          </div>

          <div class="d-lg-flex mb-3">

            <div class="w-100 me-lg-3 mb-3 mb-lg-0">
              <label>Nome da Empresa</label>
              <input type="text" name="nome" class="form-control p-form" placeholder="Nome" required value="{{ $plataforma->nome }}">
            </div>

            <div class="w-100 me-lg-3 mb-3 mb-lg-0">
              <label>CNPJ</label>
              <input type="text" name="cnpj" class="cnpj form-control p-form" placeholder="CNPJ" value="{{ $plataforma->cnpj }}">
            </div>

            <div class="w-100 me-lg-3 mb-3 mb-lg-0">
              <label>Responsável da Plataforma</label>
              <select name="responsavel_id" class="select-form" required>
                @foreach($colaboradores as $user)
                  <option value="{{ $user->id }}" @if($plataforma->responsavel_id == $user->id) selected @endif>{{ $user->nome }}</option>
                @endforeach
              </select>
            </div>

            <div class="w-100">
              <label>API KEY da Plataforma</label>
              <input type="text" class="form-control p-form" value="{{ $plataforma->api_key }}" readonly>
            </div>

          </div>

          <div class="d-lg-flex mb-3">

            <div class="w-100 me-lg-3 mb-3 mb-lg-0">
              <label>WhatsApp</label>
              <input type="tel" name="whatsapp" id="whatsapp" class="phone form-control p-form" placeholder="(00) 00000-0000" minlength="15" value="{{ $plataforma->whatsapp }}">
            </div>
            <div class="w-100 me-lg-3 mb-3 mb-lg-0">
              <label>Telefone</label>
              <input type="tel" name="telefone" class="form-control p-form" placeholder="(00) 0000-0000" value="{{ $plataforma->telefone }}">
            </div>

            <div class="w-100 me-lg-3 mb-3 mb-lg-0">
              <label>Horário de Funcionamento</label>
              <input type="text" name="horario" class="form-control p-form" placeholder="Ex: Segunda à Sexta-Feira: dàs 07:00h às 18:00h" value="{{ $plataforma->horario }}">
            </div>

            <div class="w-100">
              <label>Endereço local</label>
              <input type="text" name="endereco" class="form-control p-form" placeholder="Ex: Av. RJ..." value="{{ $plataforma->endereco }}">
            </div>

          </div>

          <div class="d-lg-flex mb-3">

            <div class="w-100 me-lg-3 mb-3 mb-lg-0">
              <label>Email</label>
              <input type="email" name="email" class="form-control p-form" placeholder="ex: meuemail@dominio.com" value="{{ $plataforma->email }}">
            </div>

            <div class="w-100">
              <label>Facebook</label>
              <input type="text" name="facebook" class="form-control p-form" placeholder="Link" value="{{ $plataforma->facebook }}">
            </div>
            <div class="w-100 mx-lg-3 my-3 my-lg-0">
              <label>Instagram</label>
              <input type="text" name="instagram" class="form-control p-form" placeholder="Link" value="{{ $plataforma->instagram }}">
            </div>
            <div class="w-100">
              <label>YouTube</label>
              <input type="text" name="youtube" class="form-control p-form" placeholder="Link" value="{{ $plataforma->youtube }}">
            </div>

          </div>

      </div>

    </section>

    <section class="mb-4-5">
      
      <a href="#boxOutros" data-bs-toggle="collapse" aria-expanded="false" class="hover-d-none">
        <div class="section-title">
          Outros
        </div>
      </a>
      <hr/>
      
      <div class="collapse" id="boxOutros">
        <a href="{{ route('admin.plataforma.editar.configuracoes.categorias') }}" target="_blank" class="btn btn-primary fs-13 weight-600">
          Categorias
        </a>
        <a href="{{ route('admin.plataforma.editar.configuracoes.logs') }}" target="_blank" class="btn btn-primary fs-13 weight-600">
          Log
        </a>
        <a href="{{ route('admin.plataforma.editar.configuracoes.modelo-de-emails') }}" target="_blank" class="btn btn-primary fs-13 weight-600">
          Modelo de Emails
        </a>
      </div>

    </section>

    <section class="mb-4-5">
      
      <a href="#boxModulos" data-bs-toggle="collapse" aria-expanded="false" class="hover-d-none">
        <div class="section-title">
          Módulos
        </div>
      </a>
      <hr/>
      
      <div class="collapse show" id="boxModulos">

        <div class="row">
          @foreach($modulos as $modulo)
            <div class="col-lg-3 mb-1">
              <label for="moduloId{{$modulo->id}}" class="w-100">
                <div class="d-flex align-items-center">
                  
                  <input type="checkbox" id="moduloId{{$modulo->id}}" name="modulos[]" value="{{$modulo->url}}" @if($modulo->status != 1) checked @endif>                    
                  <input type="text" name="modulosTextos[{{$modulo->id}}]" class="ms-1 w-lg-80 w-xs-100 rounded-left-0 rounded-right px-2 border outline-0" value="{{ $modulo->texto }}">
            
                </div>
              </label>
            </div>
          @endforeach
        </div>

      </div>

    </section>

    <section class="mb-4-5">
      
      <a href="#boxScriptsPersonalizado" data-bs-toggle="collapse" aria-expanded="false" class="hover-d-none">
        <div class="section-title">
          Scripts personalizado
        </div>
      </a>
      <hr/>
      
      <div class="collapse show" id="boxScriptsPersonalizado">

        <div class="weight-600 bg-db fs-12 text-dark rounded-1 mb-3 p-3">
          <i class="fas fa-info-circle color-666"></i>
          Aqui você pode adicionar blocos de códigos para integrações como Google Analytics, Facebook Pixel e entre outras.<br>Você também pode criar blocos de scripts de JavaScript e até mesmo CSS.
        </div>

        <div class="d-lg-flex mb-3">

          <div class="w-100 mb-3 mb-lg-0 me-lg-3">
            <label>Campo para adicionar Scripts ao início da tag &lt;HEAD&gt;</label>
            <textarea name="scripts_start_head" class="textarea-form">{{ $plataforma->scripts_start_head }}</textarea>
          </div>
          <div class="w-100">
            <label>Campo para adicionar Scripts ao fim da tag &lt;/HEAD&gt;</label>
            <textarea name="scripts_final_head" class="textarea-form">{{ $plataforma->scripts_final_head }}</textarea>
          </div>

        </div>
        <div class="d-lg-flex mb-3">

          <div class="w-100 mb-3 mb-lg-0 me-lg-3">
            <label>Campo para adicionar Scripts ao início da tag &lt;BODY&gt;</label>
            <textarea name="scripts_start_body" class="textarea-form">{{ $plataforma->scripts_start_body }}</textarea>
          </div>
          <div class="w-100">
            <label>Campo para adicionar Scripts ao fim da tag &lt;/BODY&gt;</label>
            <textarea name="scripts_final_body" class="textarea-form">{{ $plataforma->scripts_final_body }}</textarea>
          </div>

        </div>

      </div>

    </section>

    <section class="mb-4-5">
      
      <a href="#boxTema" data-bs-toggle="collapse" aria-expanded="false" class="hover-d-none">
        <div class="section-title">
          Tema da Ára do Aluno
        </div>
      </a>
      <hr/>
      
      <div class="collapse show" id="boxTema">

        <select name="tema" class="select-form">
            <option value="D" @if($plataforma->tema == 'D') selected @endif>Dark</option>
            <option value="L" @if($plataforma->tema == 'L') selected @endif>Light</option>
        </select>

      </div>

   </section>

    <section class="mb-4-5">
      
      <a href="#boxFormaLogin" data-bs-toggle="collapse" aria-expanded="false" class="hover-d-none">
        <div class="section-title">
          Forma de Login
        </div>
      </a>
      <hr/>
      
      <div class="collapse show" id="boxFormaLogin">

        <select name="forma_login" class="select-form">
            <option value="email" @if($plataforma->forma_login == 'email') selected @endif>Email</option>
            <option value="cpf" @if($plataforma->forma_login == 'cpf') selected @endif>CPF</option>
        </select>

      </div>

   </section>

    <button type="submit" name="sent" value="ok" class="btn-add mt-3">Salvar Alterações</button> 

    </form>

</div>

@endsection

@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.14.0-beta2/dist/js/bootstrap-select.min.js"></script>
<script>

	$(".cnpj").mask('99.999.999/9999-99');
	$(".telefone").mask('(00) 0000-0000');

    $(document).on("focus", ".phone", function() { 
      jQuery(this)
          .mask("(99) 9999-99999")
          .change(function (event) {  
              target = (event.currentTarget) ? event.currentTarget : event.srcElement;  
              phone = target.value.replace(/\D/g, '');
              element = $(target);  
              element.unmask();  
              if(phone.length > 10) {  
                  element.mask("(99) 99999-9999");  
              } else {  
                  element.mask("(99) 9999-9999?9");  
              }  
      });
    });

</script>
@endpush