<?php

namespace App\Http\Livewire\Admin\Trilha;

use Livewire\Component;
use App\Models\Trilha;
use App\Models\TrilhaTurma;
use App\Models\TrilhaAluno;
use App\Models\Turma;
use App\Models\User;
use App\Models\Formulario;
use App\Models\Certificado;
use App\Models\Unidade;
use App\Models\Categoria;
use App\Models\TrilhaCategoria;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Http;

class Info extends Component
{
    public $trilha;
    public $turmas;
    public $cadastrante;
    public $formulario;
    public $certificado;
    public $atestado;
    public $unidade;
    public $password_to_delete;

    public function mount($trilha_id)
    {
        $this->trilha       = Trilha::plataforma()->find($trilha_id);
        $this->cadastrante  = User::plataforma()->where('id', $this->trilha->cadastrante_id)->first();
        $this->formulario   = Formulario::plataforma()->find($this->trilha->formulario_id);
        $this->certificado  = Certificado::plataforma()->find($this->trilha->certificado_id);
        $this->atestado     = Certificado::plataforma()->find($this->trilha->atestado_id);
        $this->unidade      = Unidade::plataforma()->find($this->trilha->unidade_id);
    
        $turmasIds    = TrilhaTurma::plataforma()->where('trilha_id', $this->trilha->id)->get()->pluck('turma_id')->toArray();
        $this->turmas = Turma::plataforma()->whereIn('id', $turmasIds)->orderBy('nome')->get();
    
        $categoriasIds    = TrilhaCategoria::plataforma()->where('trilha_id', $this->trilha->id)->get()->pluck('categoria_id')->toArray();
        $this->categorias = Categoria::plataforma()->ativo()->whereIn('id', $categoriasIds)->select('nome')->orderBy('nome')->get()->pluck('nome')->toArray();
    }

    public function render()
    {
        return view('livewire.admin.trilha.info');
    }

    public function delete()
    {
        if(\Auth::attempt(['id' => \Auth::id(), 'password' => $this->password_to_delete, 'status' => 0]))
        {
            TrilhaTurma::plataforma()->where('trilha_id', $this->trilha->id)->delete();
            $deleteAndReturnCountDeleteds = TrilhaAluno::plataforma()->where('trilha_id', $this->trilha->id)->delete();
            
            $trilha = Trilha::plataforma()->where('id', $this->trilha->id)->first();

            \App\Models\Log::create([
                'tipo' => 'trilha',
                'ref'  => 'Trilha: ' . $trilha->referencia . ' foi deletada. Quantidade de Alunos vinculados removidos da trilha: ' . $deleteAndReturnCountDeleteds,
            ]);

            $path = str_replace(\App\Models\Util::getLinkStorage(), '', $trilha->foto_capa);
            Storage::delete($path);

            $trilha->delete();

            session()->flash('success', 'Trilha deletada com êxito.');
            
            return redirect(route('admin.trilha.lista'));

        } else {

            session()->flash('danger-livewire', 'Senha incorreta.');

            $this->dispatchBrowserEvent('toast');
        }
    }
}
