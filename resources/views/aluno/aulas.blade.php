@extends('layouts.aluno')
@section('content')
<section class="container-fluid px-xl-4 z-3 position-relative">

<div class="pt-5">

	@section('seo_title', 'Curso ' . $curso->nome)

	<style>
		#getNomePage::after { content: '{{ $curso->nome }}'; }
		.checkAula { color: #229754!important; }
	</style>

	<div class="row">

		<div class="col-lg-8">

			<div id="boxCarregando">
				<div class="flex-center my-5">
					<div class="mt-5 spinner-grow text-primary" role="status">
						<span class="visually-hidden">Loading...</span>
					</div>
				</div>
			</div>

			<div id="boxConteudo">

				<iframe src="" id="iframe-conteudo" height="400" class="w-100" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

				<div style="display: none;" id="boxConteudoLinkExterno">
					<div style="height: 400px;" class="w-100 d-flex justify-content-center align-items-center">
						<a href="" target="_blank" id="btnLinkExterno" class="btn btn-primary weight-600 fs-17 font-nunito">
							<span id="linkExternoAtualAulaNome"></span>
							<i class="ms-2 fas fa-external-link-square-alt"></i>
						</a>
					</div>
				</div>

			</div>

			<h1 class="color-b7 fs-20 mt-4" id="AULA_ATUAL_TITULO">
				
			</h1>

			<span class="d-block color-b7" id="AULA_ATUAL_DESCRICAO">
				
			</span>

			<hr/>

			@if(isset($turmaAluno))
			<div class="d-flex justify-content-end align-items-center my-4">
				
				<button onclick="setMarkAula(this)" id="btnMarcarComoConcluido" class="btnMarcarComoConcluido px-3 py-2-5">
					<i class="far fa-check-circle"></i>&nbsp;
					Marcar como concluído
				</button>

				@if($curso->certificado_id && $turmaAluno->data_conclusao_curso != null)
				<button onclick="window.open('/aluno/gerar-certificado/{{$curso->certificado_id}}/turma/{{$turmaAluno->turma_id}}');" class="d-block ms-2 btnMarcarComoConcluido px-3 py-2-5">
					<i class="far fa-file-pdf"></i>&nbsp;
					Baixar Certificado
				</button>
				@endif
			</div>
			@endif
			
		</div>

		<div class="col-lg-4" style="height: 400px; overflow-y: auto;">

			<input type="hidden" id="aula_id">
			<input type="hidden" id="modulo_id">

			@foreach($modulos as $modulo)

				<div>

					<a href="#" data-bs-toggle="collapse" data-bs-target="#aulas{{$modulo->id}}" aria-expanded="false" class="text-decoration-none">
						<div class="d-flex justify-content-between p-4 bg-modulo">
							<p class="m-0 color-modulo">
								{{ $modulo->nome }}
							</p>
							<i class="fas fa-caret-down color-b7"></i>
						</div>
					</a>

					@php
						$aulas = \App\Models\CursoAula::select('id','nome')->plataforma()->ativo()->where('modulo_id', $modulo->id)->orderBy('ordem')->get();
					@endphp

					<ul id="aulas{{$modulo->id}}" class="list-group">
						@foreach($aulas as $aula)
							<a onclick="aulaActive({{$aula->id}}, {{$modulo->id}}, this)" class="clickAula side-color list-group-item color-modulo">
								<div class="d-flex justify-content-between align-items-center"> 
									<span>{{ $aula->nome }}</span>
									<i id="checkAulaId{{$aula->id}}" class="far fa-check-circle ms-4 color-666"></i>
								</div>
							</a>

							<hr class="p-0 m-0" />

						@endforeach
					</ul>
				</div>

			@endforeach

		</div>

	</div>


	<script>

		document.addEventListener("DOMContentLoaded", function(event) { 
		 	document.querySelector('.clickAula').click();
		});

		const aulasConcluidasIds = [];
	
	</script>

	@if(isset($turmaAluno))
		@foreach($aulasConcluidas as $aula_id)
			<script>
				document.getElementById('checkAulaId'+{{$aula_id}}).classList.add('checkAula');
				aulasConcluidasIds.push({{$aula_id}});
			</script>
		@endforeach
	@endif

	<script>

		function aulaActive(aula_id, modulo_id, el)
		{
      		document.getElementById('aula_id').value = aula_id;
      		document.getElementById('modulo_id').value = modulo_id;

      		document.getElementById('boxCarregando').style.display = 'block';

			var els = document.querySelectorAll('.side-color')

			for (var i = 0; i < els.length; i++)
			{
				els[i].classList.remove('bg-selected-aula')
			}

			el.classList.add('bg-selected-aula');

			getAulaAtualById(aula_id);
		}

      	function getAulaAtualById(aula_id)
      	{
      		document.getElementById('iframe-conteudo').style.display = 'none';
      		document.getElementById('iframe-conteudo').src = '';
      		document.getElementById('boxConteudoLinkExterno').style.display = 'none';

			document.getElementById('AULA_ATUAL_TITULO').innerHTML = null;
			document.getElementById('AULA_ATUAL_DESCRICAO').innerHTML = null;

			document.querySelectorAll('.btnMarcarComoConcluido').forEach(obj=>obj.classList.remove('aula-concluida'));

         	fetch('{{ route('api.aula-by-id') }}?aula_id='+aula_id)
            	.then(response => response.text())
            	.then(res => { 
               		try {
                  
                		data = JSON.parse(res);

                		let tipo_objeto = data.aula.tipo_objeto;
                		let conteudo    = data.aula.conteudo;
						let aluno_id    = '{{\Auth::id()}}';
						let params      = '?aluno_id='+aluno_id;

                		if(tipo_objeto == 'Link externo')
                		{
                			document.getElementById('boxConteudoLinkExterno').style.display = 'block';
                			document.getElementById('btnLinkExterno').href = conteudo+params;
                			document.getElementById('linkExternoAtualAulaNome').innerHTML = data.aula.nome;
							document.getElementById('btnMarcarComoConcluido').style.display = 'none';
							
                		} else {
							
							document.getElementById('btnMarcarComoConcluido').style.display = 'block';
                			document.getElementById('iframe-conteudo').style.display = 'block';

	                		if(tipo_objeto == 'Vídeo')
	                		{	
						        if(conteudo.indexOf('youtube') > 0 || conteudo.indexOf('youtu.be') > 0)
						        {
						            tipo = 'Y'; // Youtube
						        } else if (conteudo.indexOf('vimeo') > 0) {
						            tipo = 'V'; // Vimeo
						        } else {
						            tipo = 'U'; // Unknown
						        }

	                			if(tipo == 'Y')
	                			{
		                			conteudo = 'https://www.youtube.com/embed/' + youtube_parser(conteudo);

		                			document.getElementById('iframe-conteudo').src = conteudo;
	                			
	                			} else {

	                				document.getElementById('iframe-conteudo').src = conteudo;
	                			}

	                		} else {

		                		document.getElementById('iframe-conteudo').src = conteudo;
	                		}

                		}

                		document.getElementById('AULA_ATUAL_TITULO').innerHTML = data.aula.nome;
                		document.getElementById('AULA_ATUAL_DESCRICAO').innerHTML = data.aula.descricao;

						if(document.getElementById('checkAulaId'+aula_id).classList.contains('checkAula'))
						{
							document.getElementById('btnMarcarComoConcluido').classList.add('aula-concluida');

						} else {

							document.getElementById('btnMarcarComoConcluido').classList.remove('aula-concluida');
						}

               		} catch (error) {
                  
                  console.log(error);
               	}   
          	
				document.getElementById('boxCarregando').style.display = 'none';

         	});
      	}

		function youtube_parser(url)
		{
		    var regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#&?]*).*/;
		    var match = url.match(regExp);
		    return (match&&match[7].length==11)? match[7] : false;
		}

		function setMarkAula(el)
		{
			let aula_id = parseInt(document.getElementById('aula_id').value);

			if(document.getElementById('btnMarcarComoConcluido').classList.contains('aula-concluida'))
			{	
				document.getElementById('btnMarcarComoConcluido').classList.remove('aula-concluida');
				document.getElementById('checkAulaId'+aula_id).classList.remove('checkAula');

				desmarcarComoConcluida();
			
			} else {

				document.getElementById('btnMarcarComoConcluido').classList.add('aula-concluida');
				document.getElementById('checkAulaId'+aula_id).classList.add('checkAula');

				marcarComoConcluida()				
			}
		}

		function marcarComoConcluida()
		{
			let curso_id  = {{ $curso->id }};
			let modulo_id = document.getElementById('modulo_id').value;
			let aula_id   = document.getElementById('aula_id').value;

	        const data = {
	          	method: 'PUT',
				headers: {
				 	'Content-type': 'application/json; charset=UTF-8',
				 	'X-CSRF-TOKEN': '{{ csrf_token() }}'
				},
				body: JSON.stringify({ aluno_id: {{\Auth::id()}}, curso_id:curso_id, modulo_id:modulo_id, aula_id:aula_id })
	        };

         	fetch('{{ route('api.marcar-como-conluida') }}', data)
            	.then(response => response.text())
            	.then(res => { 
               		try {
                  		
               		} catch (error) {
                  
                  console.log(error);
               	}
         	});

		}

		function desmarcarComoConcluida()
		{
			let curso_id  = {{ $curso->id }};
			let modulo_id = document.getElementById('modulo_id').value;
			let aula_id   = document.getElementById('aula_id').value;

	        const data = {
	          	method: 'PUT',
				headers: {
				 	'Content-type': 'application/json; charset=UTF-8',
				 	'X-CSRF-TOKEN': '{{ csrf_token() }}'
				},
				body: JSON.stringify({ aluno_id: {{\Auth::id()}}, curso_id:curso_id, modulo_id:modulo_id, aula_id:aula_id })
	        };

         	fetch('{{ route('api.desmarcar-como-conluida') }}', data)
            	.then(response => response.text())
            	.then(res => { 
               		try {
                  		
               		} catch (error) {
                  
                  console.log(error);
               	}
         	});

		}

	</script>

</div>

</section>
@endsection