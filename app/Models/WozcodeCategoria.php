<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WozcodeCategoria extends Model
{
	public $timestamps = false;
	
	protected $table = 'wozcode_categorias';

	protected $fillable = [
		'nome',
		'slug'
	];

}
