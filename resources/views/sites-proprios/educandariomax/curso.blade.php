<!doctype html>
<html lang="pt-br">
<head>

   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1">

   <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/uikit@3.5.9/dist/css/uikit.min.css" />
   <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
   <link rel="stylesheet" href="{{ asset('css/util.css') }}">
   <link rel="stylesheet" href="{{ asset('sites-proprios/educandariomax/css/home.css') }}">
   <link rel="stylesheet" href="{{ asset('css/pages.css') }}">
   
   <title>{{ $curso->seo_title ?? strip_tags($curso->nome . ' - ' . $nomePlataforma) }}</title>
   <meta name="robots" content="index, follow">
   <meta name="description" content="{{ $curso->seo_description ?? null }}"/>
   <meta name="keywords" content="{{ $curso->seo_keywords ?? null }}"/>
   <link rel="canonical" href="{{ Request::url() }}" />
   <link rel="shortcut icon" href="{{ $plataforma->favicon }}">
   <meta property="og:locale" content="pt_BR" />
   <meta property="og:type" content="website" />
   <meta property="og:title" content="{{ $curso->seo_title ?? $curso->nome . ' - ' . $nomePlataforma }}" />
   <meta property="og:description" content="{{ $curso->seo_description ?? null }}" />
   <meta property="og:url" content="{{ Request::url() }}" />
   <meta property="og:site_name" content="{{ $plataforma->nome }}" />
   <meta property="og:image" content="{{ $plataforma->logotipo }}" />
   <meta name='author' content='WOZCODE | Pedro Henrique' />

</head>
<body class="bg-f4" id="inicio">

   <header>
      
      <div class="sub-header container-fluid px-xl-7 py-2-3">
         <div class="d-flex justify-content-between align-items-center">

            <div>

               <span class="d-xs-none font-poppins text-light text-shadow-2 fs-12">
                  <i class="fa fa-envelope"></i>
                  {{ $plataforma->email ?? 'meuemail@exemlo.com' }}
               </span>

               <a href="https://api.whatsapp.com/send?phone=55{{preg_replace('/\D/', '', $plataforma->whatsapp)}}&text=Olá, vim através do Site {{ $plataforma->nome }}" target="_blank" class="mx-3 text-shadow-2 text-d-none font-poppins text-light hover-d-none fs-12">
                  <i class="fab fa-whatsapp"></i>
                  {{ $plataforma->whatsapp ?? '(00) 00000-0000' }}
               </a>

               <a href="tel:+55{{preg_replace('/\D/', '', $plataforma->telefone)}}" class="d-xs-none text-shadow-2 text-d-none font-poppins text-light hover-d-none fs-12">
                  <i class="fa fa-phone"></i>
                  {{ $plataforma->telefone ?? '(00) 0000-0000' }}
               </a>

            </div>

            <div class="d-flex justify-content-between align-items-end">

               <div>
                  <a href="{{ $plataforma->facebook }}" target="_blank" class="fs-17 text-d-none text-light text-shadow-2">
                       <i class="fab fa-facebook-f flex-center"></i>
                  </a>
                  <a href="{{ $plataforma->instagram }}" target="_blank" class="ms-2 fs-17 text-d-none text-light text-shadow-2">
                     <i class="fab fa-instagram flex-center"></i>
                  </a>
               </div>

            </div>

         </div>
      </div>

      <nav class="navbar navbar-expand-lg bg-white box-shadow py-0 z-3" id="menu">
         <div class="container-fluid px-xl-7">
             <a class="navbar-brand" href="/">
               <img src="{{ $plataforma->logotipo }}" class="d-xs-none" width="90" alt="Logotipo Educandario Max">
               <img src="{{ $plataforma->logotipo }}" class="d-md-none" id="logoMenuMobile" width="75" alt="Logotipo Educandario Max">
             </a>
             <button class="navbar-toggler outline-0 border-0 text-dark" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
               <span class="fa fa-bars"></span>
             </button>
             <div class="collapse navbar-collapse" id="navbarSupportedContent">
                  <ul id="ulLinks" class="navbar-nav ms-auto mb-2 mb-lg-0 align-items-lg-center p-3 p-lg-auto">
                     <li class="nav-item">
                        <a class="nav-link" href="/#cursos">
                          Nossos Cursos
                        </a>
                     </li>
                     <li class="nav-item">
                        <a class="nav-link" href="/#quem-somos">
                           Quem somos
                        </a>
                     </li>
                     <li class="nav-item">
                        <a class="nav-link" href="/#contato">
                           Fale Conosco
                        </a>
                     </li>
                     <li class="nav-item">
                        <a class="nav-link" href="/login">
                           <i class="fas fa-graduation-cap"></i>
                           Área do Aluno
                        </a>
                     </li>
                     <li class="nav-item ms-lg-5 mt-lg-1 mt-4">
                       <a class="nav-link btnInscrever animation-grow d-inline-block" href="/#pre-matricula">
                          Matricule-se
                       </a>
                     </li>
                 </ul>
             </div>
         </div>
      </nav>

   </header>

   <main>

      <section>
         <div class="w-100 h-lg-350 h-xs-100vh" style="background-size: cover; background-position: center; background-image: url('{{ $curso->foto_capa ?? asset('images/bg-login.png') }}');">
            <div class="uk-position-relative h-100 px-xl-7 px-3">
            <div class="background-overlay h-100" style="background: rgba(1, 10, 20, 1);opacity: .9;"></div>

               <div class="row align-items-center h-100 uk-position-relative">
                  <div>
                     <h1 class="mb-4-5 text-light font-poppins weight-800 fs-xs-35 fs-md-50 text-shadow-3">
                        <span id="cursoTiulo">{!! $curso->nome !!}</span>
                     </h1>
                     <a id="btnCursoInscrevaSe" class="bg-faixa px-4 py-3 rounded font-poppins text-shadow-2 box-shadow hover-d-none text-white weight-700" href="#turmas" uk-scroll="offset: 175;">
                        INSCREVA-SE AGORA!
                     </a>
                  </div>
               </div>
            </div>
         </div>
      </section>

      <section class="pb-lg-3">
         <div class="row container-fluid px-3 gx-0 gx-lg-4 px-xl-5 mt--15px position-relative pb-5">
            <div class="col-lg-8">
               <div class="bg-white px-lg-5 px-4 pt-4-5 pb-4-5 box-shadow border-top rounded-20">

                  <p class="mb-0 pb-0 font-poppins weight-600 fs-14 color-666">
                     Sobre o curso
                  </p>

                  <hr class="border border-secondary">

                  @if($curso->descricao == null)
                     <p class="mb-0 pb-0 font-poppins weight-600 fs-13 color-666">
                        <i class="fas fa-exclamation-circle"></i> Descrição do Curso não definida.
                     </p>
                  @endif

                  <p id="cursoDescricao">
                     {!! $curso->descricao !!}
                  </p>

               </div>
            </div>
            <div class="col-lg-4 mt-4 mt-lg-0">
               <div class="bg-white px-4-5 pt-4-5 pb-4-5 box-shadow border-top rounded-20">

                  @if($curso->video)
                     <iframe width="100%" height="225" class="rounded mb-4" src="{{ $curso->video }}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                  @endif

                      @if($curso->nivel)
                      <div>                                
                          <p class="fs-15 mb-0 weight-600 color-555">NÍVEL:</p>
                          <p class="fs-14 mb-0 weight-600 font-poppins color-444">{{ $curso->nivel }}</p>
                          <hr/>
                      </div>
                      @endif

                      @if($curso->tipo_formacao)
                      <div>                                
                          <p class="fs-15 mb-0 weight-600 color-555">TIPO DE FORMAÇÃO:</p>
                          <p class="fs-14 mb-0 weight-600 font-poppins color-444">{{ $curso->tipo_formacao }}</p>
                          <hr/>
                      </div>
                      @endif
                      
                      @if($curso->carga_horaria)
                      <div>                                
                          <p class="fs-15 mb-0 weight-600 color-555">CARGA HORÁRIA:</p>
                          <p class="fs-14 mb-0 weight-600 font-poppins color-444">{{ $curso->carga_horaria }} horas</p>
                      </div>
                      @endif

               </div>
            </div>
         </div>
      </section>

      <section class="pb-5" id="turmas">
         <div class="row container-fluid px-3 gx-0 gx-lg-4 px-xl-5 mt--15px position-relative pb-5">
            <div class="col-lg-8">
               <div class="bg-white px-lg-5 px-4 pt-4-5 pb-4-5 box-shadow border-top rounded-20">

                  <p class="font-poppins weight-600 fs-14 color-666">
                     Turmas
                  </p>

                  @if(count($turmas) == 0)
                     <p class="mb-0 pb-0 font-poppins weight-600 fs-13 color-666">
                        <i class="fas fa-exclamation-circle"></i> Não há turmas publicadas disponíveis para inscrição.
                     </p>
                  @endif

                  @foreach($turmas as $turma)
                     <div class="mb-4 p-4 border rounded-20">
                        <div class="d-flex align-items-end justify-content-between">
                           <h1 class="m-0 p-0 fs-17 font-poppins color-faixa-verde">
                              {{ $turma->nome }}
                           </h1>
                           @if($turma->modalidade)
                              <span class="modalidade bg-faixa">
                                 {{ $turma->modalidade }}
                              </span>
                           @endif
                        </div>
                        <hr/>
                        <div>
                           <div class="d-lg-flex justify-content-between">
                              
                              @if($turma->data_inicio)
                              <div class="mb-3 mb-lg-0 d-flex align-items-center">
                                 <span class="me-3">
                                    <i class="fs-25 far fa-calendar-alt"></i>
                                 </span>
                                 <p class="m-0">
                                    <span class="d-block font-poppins fs-13 color-666">Data:</span>
                                    <span class="d-block fs-15 weight-600 color-555">
                                       Início: {{ \App\Models\Util::replaceDatePt($turma->data_inicio) }}
                                    </span>
                                    <span class="d-block fs-15 weight-600 color-555">
                                       Término: {{ \App\Models\Util::replaceDatePt($turma->data_termino) }}
                                    </span>
                                 </p>
                              </div>
                              @endif

                              @if($turma->horario)
                              <div class="mb-3 mb-lg-0 d-flex align-items-center pe-lg-2">
                                 <span class="me-3">
                                    <i class="fs-25 far fa-clock"></i>
                                 </span>
                                 <p class="m-0">
                                    <span class="d-block font-poppins fs-13 color-666">Dias e Horário:</span>
                                    <span class="d-block fs-15 weight-600 color-555">
                                       {{ $turma->frequencia }}
                                    </span>
                                    <span class="d-block fs-15 weight-600 color-555">
                                       {{ $turma->horario }}
                                    </span>
                                 </p>
                              </div>
                              @endif

                              @if($turma->duracao)
                              <div class="mb-3 mb-lg-0 d-flex align-items-center pe-lg-2">
                                 <span class="me-3">
                                    <i class="fs-25 far fa-clock"></i>
                                 </span>
                                 <p class="m-0">
                                    <span class="d-block font-poppins fs-13 color-666">Duração:</span>
                                    <span class="d-block fs-15 weight-600 color-555">
                                       {{ $turma->duracao }}
                                    </span>
                                 </p>
                              </div>
                              @endif

                           </div>

                           @if($turma->data_inicio || $turma->horario || $turma->duracao)
                              <hr/>
                           @endif

                           <div class="d-lg-flex justify-content-between align-items-center">

                              <div class="ps-lg-1">
                                 @if($turma->valor_a_vista)
                                 <p class="d-flex align-items-center m-0 p-0">
                                    <span class="me-2 weight-600 color-555">
                                       por:
                                    </span>
                                    <span class="fs-20 font-poppins color-faixa-verde text-shadow-1 weight-700">
                                       R$ {{ $turma->valor_a_vista }}
                                    </span>
                                 </p>
                                 @endif
                                 @if($turma->valor_parcelado)
                                 <p class="mb-0 me-2 weight-600 color-555">
                                    <span class="me-2 weight-600 color-555">
                                       ou: 
                                    </span>
                                    <span class="fs-20 font-poppins color-faixa-verde text-shadow-1 weight-700">
                                       R$ {{ $turma->valor_parcelado }}
                                    </span>
                                 </p>
                                 @endif
                              </div>

                              <div class="mt-3 mt-lg-0">
                                 <a href="/f/{{$turma->formulario_id ?? 0}}/c/{{$turma->id}}" target="_blank" id="btnCursoMatricular" class="btn-comprar">
                                    QUERO ME MATRICULAR!
                                 </a>
                              </div>
                           </div>

                        </div>
                     </div>
                  @endforeach

               </div>
            </div>
         </div>
      </section>

   </main>

   <footer id="contato" style="background-image: url('{{ asset('sites-proprios/educandariomax/images/footer.jpg') }}'); background-repeat: no-repeat; background-size: cover; background-position: center;">
      <div class="container-fluid px-xl-7 pt-5" style="margin-top: -50px;">

         <div class="row align-items-end py-lg-4 px-3 px-md-0">

            <div class="col-lg-9">

               <h1 class="fs-35 font-montserrat color-faixa weight-800 text-uppercase">FALE CONOSCO</h1>
               <div class="my-4 w-80px h-2px bg-line rounded"></div>

               <img src="{{ $plataforma->logotipo }}" class="img-fluid" width="100">

               <div class="mt-2">                 

                  <div class="mb-3">
                     <span class="text-light font-poppins fs-14">
                        <i class="color-d4 fa fa-envelope"></i> 
                        {{ $plataforma->email ?? 'meuemail@exemlo.com' }}
                     </span>
                  </div>
                  <div class="mb-3">
                     <a href="https://api.whatsapp.com/send?phone=55{{preg_replace('/\D/', '', $plataforma->whatsapp)}}&text=Olá, vim através do Site {{ $plataforma->nome }}" class="text-light font-poppins fs-14">
                        <i class="color-d4 fab fa-whatsapp"></i> 
                        {{ $plataforma->whatsapp ?? '(00) 00000-0000' }}
                     </a>
                  </div>
                  <div class="mb-3">
                     <a href="tel:+55{{preg_replace('/\D/', '', $plataforma->telefone)}}" class="text-light font-poppins fs-14">
                        <i class="color-d4 fa fa-phone"></i> 
                        {{ $plataforma->telefone ?? '(00) 0000-0000' }}
                     </a>
                  </div>
                  <div class="mb-3">
                     <span class="text-light font-poppins fs-14">
                        <i class="color-d4 fa fa-clock"></i> 
                        Horário de Funcionamento: {{ $plataforma->horario ?? 'Segunda à sexta-feira das 09:00 às 18:00' }}
                     </span>
                  </div> 
                  <div class="mb-3">
                    <address>
                      <a href="https://www.google.com/maps/place/R.+Nossa+Sra.+das+Gra%C3%A7as,+16+-+Pitangueiras,+Rio+de+Janeiro+-+RJ,+21932-160/@-22.816727,-43.1837951,3a,90y,309.68h,81.41t/data=!3m6!1e1!3m4!1sEH9YpWezCMa2EgcnRZTlDQ!2e0!7i13312!8i6656!4m5!3m4!1s0x9978053a115757:0x6ce6de971070dcf7!8m2!3d-22.8167475!4d-43.1838824" target="_blank" class="text-light font-poppins fs-14">
                           <i class="color-d4 fas fa-map-marker-alt"></i>
                           {{ $plataforma->endereco ?? 'Pintangueiras - RJ. Rua Nossa Senhora das Graças, 16. 21932-230.' }}
                      </a>
                     </address>
                  </div>

                  <div class="mt-4 d-flex">

                     <a href="{{ $plataforma->facebook }}" target="_blank" class="me-2 hover-d-none">
                        <div class="square-40 rounded-circle social-icon-facebook flex-center">
                           <i class="text-white fab fa-facebook fs-20"></i>
                        </div>
                     </a>
                     <a href="{{ $plataforma->instagram }}" target="_blank" class="hover-d-none">
                        <div class="square-40 rounded-circle social-icon-instagram flex-center">
                           <i class="text-white fab fa-instagram fs-20"></i>
                        </div>
                     </a>

                  </div>

               </div>

            </div>

            <div class="col-lg-3 mt-5 mt-lg-0">

               <div class="mb-3">
                  <a href="/#pre-matricula" class="btn btn-primary weight-600 w-100 py-2-5 font-poppins hover-d-none">
                     MATRICULE-SE JÁ!
                  </a>
               </div>
               <div class="mb-3">
                  <a href="/login" class="btn btn-warning weight-600 w-100 py-2-5 font-poppins hover-d-none">
                     ÁREA DO ALUNO
                  </a>
               </div>
               <div class="mb-3">
                  <a href="/f/torne-se-parceiro/10/t/25" target="_blank" class="btn btn-light weight-600 w-100 py-2-5 font-poppins hover-d-none">
                     TORNE-SE PARCEIRO
                  </a>
               </div>
               <div class="mb-3">
                  <a href="https://api.whatsapp.com/send?phone=55{{preg_replace('/\D/', '', $plataforma->whatsapp)}}&text=Olá, vim através do Site {{ $plataforma->nome }}" class="btn btn-success bg-wpp text-shadow-1 weight-600 w-100 py-2-5 font-poppins hover-d-none">
                     ENTRAR EM CONTATO
                  </a>
               </div>

            </div>

         </div>

         <hr class="mt-4 border border-light" />

         <div class="pb-3 d-lg-flex justify-content-between align-items-center">

            <p class="text-xs-center mb-1 mb-lg-0 p-0 color-faixa font-poppins fs-11 ls-05">
               Educandário Max © 2021 • Todos os direitos reservados • CNPJ: {{ $plataforma->cnpj }}
            </p>

            <p class="text-xs-center m-0 p-0 color-faixa font-poppins fs-11 ls-05">
               Desenvolvido por <a href="https://wozcode.com" target="_blank" class="text-danger text-d-none fs-11 weight-600 text-shadow-1">WOZCODE</a>
            </p>

         </div>

      </div>
   </footer>

   <a href="https://api.whatsapp.com/send?phone=55{{preg_replace('/\D/', '', $plataforma->whatsapp)}}&text=Olá, vim através do Site {{ $plataforma->nome }}" target="_blank" class="text-white fs-30">
       <div class="fixed-bottom right-0 me-3 mb-3 z-3">
           <div class="square-60 rounded-circle bg-wpp flex-center box-shadow-wpp">
               <i class="fab fa-whatsapp"></i>
           </div>
       </div>
   </a>

   <script src="https://kit.fontawesome.com/2f91eaaef4.js" crossorigin="anonymous"></script>  
   <script src="https://cdn.jsdelivr.net/npm/uikit@3.5.9/dist/js/uikit.min.js"></script>
   <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-U1DAWAznBHeqEIlVSCgzq+c9gqGAJn5c/t99JyeKa9xxaYpSvHU5awsuZVVFIhvj" crossorigin="anonymous"></script>
   <script>
      window.onscroll = function() {myFunction()};

      var header = document.getElementById("menu");
      var sticky = header.offsetTop;

      function myFunction() {
        if (window.pageYOffset > sticky) {
          header.classList.add("fixed-top");
        } else {
          header.classList.remove("fixed-top");
        }
      }
   </script>

</body>
</html>