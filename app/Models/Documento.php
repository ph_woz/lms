<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Documento extends Model
{
    use HasFactory;

    public $timestamps = true;

    protected $table = 'documentos';

    protected $fillable = [
        'plataforma_id',
        'cadastrante_id',
    	'user_id',
    	'nome',
        'referencia',
        'link',
        'publicar',
        'arquivado',
        'data_publicacao',
    ];

    public static  function boot()
    {
        parent::boot(); 
        static::creating(function ($model)
        {
            $model->plataforma_id  = session('plataforma_id') ?? \Auth()->user()->plataforma_id ?? Plataforma::dominio()->pluck('id')[0] ?? null;
            $model->cadastrante_id = \Auth()->user()->id;
        });
    }

    public function scopePlataforma($query)
    {
        return $query->where('plataforma_id', session('plataforma_id') ?? Plataforma::dominio()->pluck('id')[0] ?? null);
    }

    public function scopePublico($query)
    {
        return $query->where('publicar', 'S');
    }

    public function scopeDou($query)
    {
        return $query->where('referencia', 'DOU');
    }

    public function scopeEditais($query)
    {
        return $query->where('referencia', 'Editais');
    }

}
