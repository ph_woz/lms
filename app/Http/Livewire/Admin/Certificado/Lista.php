<?php

namespace App\Http\Livewire\Admin\Certificado;

use Livewire\WithPagination;
use Livewire\Component;
use App\Models\Certificado;

class Lista extends Component
{
    use WithPagination;

    protected $paginationTheme = 'bootstrap';

    public $search;
    public $status = 0;
    public $ordem;

    protected $queryString = ['search','status','ordem'];

    public function render()
    {
        $certificados = Certificado::select('id','referencia')->plataforma()->where('status', $this->status);

        if($this->search)
        {
            $certificados = $certificados->where('referencia', 'like', '%'.$this->search.'%');
        }

        $countCertificados = $certificados->count();


        if($this->ordem == 'desc' || $this->ordem == 'asc')
        {
            $certificados = $certificados->orderBy('id', $this->ordem);
        
        } else {

            $certificados = $certificados->orderBy('referencia');
        }


        $certificados = $certificados->paginate(10);


        return view('livewire.admin.certificado.lista', [
            'certificados'      => $certificados,
            'countCertificados' => $countCertificados,
        ]);
    }
}
