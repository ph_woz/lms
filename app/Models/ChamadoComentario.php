<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ChamadoComentario extends Model
{
    use HasFactory;

    public $timestamps = true;

    protected $table = 'chamados_comentarios';

    public $fillable = [
    	'plataforma_id',
        'chamado_id',
        'cadastrante_id',
        'tipo_autor', // C = Cliente LMS ou W = Equipe WOZCODE.
        'descricao',
    ];

    public static  function boot()
    {
        parent::boot(); 
        static::creating(function ($model)
        {
            $model->plataforma_id = session('plataforma_id') ?? \Auth()->user()->plataforma_id ?? Plataforma::dominio()->pluck('id')[0] ?? null;
            $model->cadastrante_id = \Auth()->user()->id ?? null;
        });
    }
    
    public function scopePlataforma($query)
    {
        return $query->where('plataforma_id', session('plataforma_id') ?? Plataforma::dominio()->pluck('id')[0] ?? null);
    }

}
