<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContatosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contatos', function (Blueprint $table) {
            $table->id();

            $table->integer('plataforma_id');
            $table->integer('formulario_id')->nullable();
            $table->integer('curso_id')->nullable();
            $table->integer('turma_id')->nullable();
            $table->integer('trilha_id')->nullable();
            $table->integer('unidade_id')->nullable();
            $table->string('nome')->nullable();
            $table->string('email')->nullable();
            $table->string('tipo', 1);
            $table->string('password')->nullable();
            $table->string('cpf')->nullable();
            $table->string('rg')->nullable();
            $table->string('naturalidade')->nullable();
            $table->string('valor_contratado')->nullable();
            $table->string('assunto');
            $table->text('mensagem')->nullable();
            $table->string('celular')->nullable();
            $table->string('telefone')->nullable();
            $table->string('genero', 1)->nullable();
            $table->date('data_nascimento')->nullable();

            $table->string('cep')->nullable();
            $table->string('rua')->nullable();
            $table->string('numero')->nullable();
            $table->string('bairro')->nullable();
            $table->string('cidade')->nullable();
            $table->string('estado')->nullable();
            $table->string('complemento')->nullable();
            $table->string('ponto_referencia')->nullable();

            $table->text('anotacao')->nullable();
            $table->integer('status')->default(0);
            $table->string('arquivado', 1)->default('N');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contatos');
    }
}
