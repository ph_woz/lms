<footer class="bg-faixa pt-5">
   <div class="container">

      <div class="row py-lg-4">

         <div class="col-lg-4">

            <h1 class="fs-20 font-montserrat text-light weight-800 text-uppercase">CONTATO</h1>
            <div class="my-4 w-80px h-2px bg-line rounded"></div>

            <div class="mt-2">                 

               <div class="mb-4" id="boxContatoHorarioAtendimento">

                  <div class="d-flex align-items-center">

                     <div class="square-40 pe-lg-5 pe-3">
                        <i class="far fa-clock fs-30 color-icon-footer"></i>
                     </div>

                     <div>
                        <p class="mb-0 font-poppins weight-600 text-white d-flex align-items-center fs-15">
                           <span>HORÁRIO DE ATENDIMENTO</span>
                        </p>
                        <p class="mt-1 mt-lg-0 mb-0 font-poppins weight-500 fs-14 text-light" id="horario">
                           {{ $plataforma->horario ?? 'Segunda à Sexta: 08:00 - 23:00' }}
                        </p>
                     </div>

                  </div>

               </div>

               <div class="mb-4" id="boxContatoEmail">

                  <div class="d-flex align-items-center">

                     <div class="square-40 pe-lg-5 pe-3">
                        <i class="fa fa-envelope fs-30 color-icon-footer"></i>
                     </div>

                     <div>
                        <p class="mb-0 font-poppins weight-600 text-white d-flex align-items-center fs-15">
                           <span>EMAIL</span>
                        </p>
                        <p class="mt-1 mt-lg-0 mb-0 font-poppins weight-500 fs-14 text-light">
                           {{ $plataforma->email ?? 'contato@faroroseira.edu.br' }}
                        </p>
                     </div>

                  </div>

               </div>

               <div class="mb-4" id="boxContatoWhatsAp">

                  <div class="d-flex align-items-center">

                     <div class="square-40 pe-lg-5 pe-3">
                        <i class="fab fa-whatsapp fs-30 color-icon-footer"></i>
                     </div>

                     <div>
                        <p class="mb-0 font-poppins weight-600 text-white d-flex align-items-center fs-15">
                           <span>CONTATO</span>
                        </p>
                         <p class="mb-0 font-poppins weight-500 fs-14 text-light">
                           Matrícule-se:
                           <a href="https://api.whatsapp.com/send?phone=55{{preg_replace('/\D/', '', $plataforma->whatsapp ?? '551236462071')}}&text=Olá, vim através do Site {{ $plataforma->nome }}" target="_blank" class="text-d-none text-light hover-white">{{ $plataforma->whatsapp }}</a>
                         </p>
                     </div>

                  </div>

               </div>

               <div class="mb-4" id="boxContatoTelefone">

                  <div class="d-flex align-items-center">

                     <div class="square-40 pe-lg-5 pe-3">
                        <i class="fa fa-phone fs-30 color-icon-footer"></i>
                     </div>

                     <div>
                        <p class="mb-0 font-poppins weight-600 text-white d-flex align-items-center fs-15">
                           <span>TELEFONE</span>
                        </p>
                         <p class="mb-0 font-poppins weight-500 fs-14 text-light">
                           Administrativo:
                           <a href="tel:+55{{preg_replace('/\D/', '', $plataforma->telefone ?? '551236462071')}}" target="_blank" class="text-d-none text-light hover-white">{{ $plataforma->telefone ?? '(12) 3646-2071' }}</a>
                         </p>
                         <p class="mb-0 font-poppins weight-500 fs-14 text-light">
                           Administrativo:
                           <a href="tel:+55{{preg_replace('/\D/', '', $plataforma->telefone ?? '551236462441')}}" target="_blank" class="text-d-none text-light hover-white">{{ $plataforma->telefone ?? '(12) 3646-2441' }}</a>
                         </p>
                     </div>

                  </div>

               </div>

               <div class="mb-3" id="boxContatoEndereco">

                  <div class="d-flex align-items-center">

                     <div class="square-40 pe-lg-5 pe-3">
                        <i class="fa fa-map-marker-alt fs-30 color-icon-footer"></i>
                     </div>

                     <div>
                        <p class="mb-0 font-poppins weight-600 text-white d-flex align-items-center fs-15">
                           <span>ENDEREÇO</span>
                        </p>
                       <address class="p-0 m-0 font-poppins weight-500 fs-14 text-light" id="endereco">
                         {{ $plataforma->endereco ?? 'Rod. Pres. Dutra KM 77 - Roseira - SP' }}
                       </address>
                     </div>

                  </div>

               </div>

               <div class="d-flex align-items-center pt-lg-3 pt-2 mb-4">
                  <a href="{{ $plataforma->facebook }}" target="_blank" class="d-block text-center bg-facebook hover-d-none social-icon text-light p-3 font-poppins rounded box-shadow fs-14">
                    <i class="text-white fab fa-facebook fs-20"></i>
                  </a>
                  <a href="{{ $plataforma->instagram }}" target="_blank" class="my-2 my-lg-0 d-block text-center bg-instagram mx-2 hover-d-none social-icon text-light p-3 font-poppins rounded box-shadow fs-14">
                    <i class="text-white fab fa-instagram fs-20"></i>
                  </a>
                  <a href="{{ $plataforma->instagram }}" target="_blank" class="d-block text-center bg-linkedin hover-d-none social-icon text-light p-3 font-poppins rounded box-shadow fs-14">
                    <i class="text-white fab fa-linkedin fs-20"></i>
                  </a>
               </div>

            </div>

         </div>

         <div class="col-lg-3 mt-4 mt-lg-0">

            <h1 class="fs-20 font-montserrat text-light weight-800 text-uppercase">LINKS</h1>
            <div class="my-4 w-80px h-2px bg-line rounded"></div>

            <div>
               <div class="my-3">
                  <a href="/f/formulario-de-matricula/45/t/631" target="_blank" class="bg-gradient bg-hover-gradient fs-14 d-block text-center px-4 rounded box-shadow text-dark weight-700 w-100 py-2-5 font-poppins hover-d-none">
                     MATRICULE-SE JÁ!
                  </a>
               </div>
               <div class="mb-3">
                  <a href="/login" class="bg-gradient bg-hover-gradient fs-14 d-block text-center px-4 rounded box-shadow text-dark weight-700 w-100 py-2-5 font-poppins hover-d-none">
                     ÁREA DO ALUNO
                  </a>
               </div>
               <div class="mb-3">
                  <a href="https://api.whatsapp.com/send?phone=55{{preg_replace('/\D/', '', $plataforma->whatsapp)}}&text=Olá, vim através do Site {{ $plataforma->nome }}" class="bg-gradient bg-hover-gradient fs-14 d-block text-center px-4 rounded box-shadow text-dark weight-700 w-100 py-2-5 font-poppins hover-d-none">
                     ENTRAR EM CONTATO
                  </a>
               </div>
               <div class="mb-3">
                  <a href="/f/seja-um-polo/46/t/632" target="_blank" class="bg-gradient bg-hover-gradient fs-14 d-block text-center px-4 rounded box-shadow text-dark weight-700 w-100 py-2-5 font-poppins hover-d-none">
                     SEJA UM POLO
                  </a>
               </div>
            </div>

         </div>

         <div class="col-lg-4 offset-lg-1 my-5 my-lg-0">

            <h1 class="fs-20 font-montserrat text-light weight-800 text-uppercase">A FARO</h1>
            <div class="my-4 w-80px h-2px bg-line rounded"></div>

             <div class="w-100">
                <a href="/fies" class="d-block mb-2 color-icon-footer font-nunito fs-14">FIES</a>
                <a href="/prouni" class="d-block mb-2 color-icon-footer font-nunito fs-14">ProUni</a>
                <a href="/enem" class="d-block mb-2 color-icon-footer font-nunito fs-14">ENEM</a>
                <a href="/transferencia" class="d-block mb-2 color-icon-footer font-nunito fs-14">Transferência</a>
                <a href="/vestibular" class="d-block mb-2 color-icon-footer font-nunito fs-14">Vestibular</a>
                <a href="/editais" class="d-block mb-2 color-icon-footer font-nunito fs-14">Editais</a>
                <a href="/dou" class="d-block mb-2 color-icon-footer font-nunito fs-14">DOU</a>
                <a href="https://modular.mentorweb.ws/modularSecurityG5/?pcaes=1aeeca054c75260d78afb1450ccb248e2d7758e322540603e0f1addb26be6f0ed544599cf791fbfa1f1de5eb830b0d09f3eb85b4a65fb48bd0a0fc03ae5f4da43b99ce3ea6f8e53e8b14541dd742e486ce7b57858fe68e31c3e18b4005c436e5ef000e3da32d24ca832e8d7564eb0dc29540d32f0065bf322e078856b6a6b5606205ee8a76cc31447754a6bd32de4299ee40b492bfd4fdf36f85edc85321c9806d36bc64eda65a01cf37334ad50266b94b383c9f32d1e145fffbc9df2faac600406d40ef02e86af9df4a21dfa5098164e9553a8da6a2f08e21e2f41d361f9f68bf2e04adcb418ae9747e5509fd4589dc" target="_blank" class="d-block mb-2 color-icon-footer font-nunito fs-14">Mentor Web</a>
                <a href="https://jbh.faroroseira.edu.br/index.php/jbh" target="_blank" class="d-block mb-2 color-icon-footer font-nunito fs-14">Revista Científica</a>
                <a href="/blog" class="d-block mb-2 color-icon-footer font-nunito fs-14">Blog</a>
                <a href="/polos" class="d-block mb-2 color-icon-footer font-nunito fs-14">Polos</a>
                <a href="/parceiros" class="d-block mb-2 color-icon-footer font-nunito fs-14">Parceiros</a>
                <a href="/biblioteca" class="d-block mb-2 color-icon-footer font-nunito fs-14">Biblioteca</a>
                <a href="/sobre-nos" class="d-block mb-2 color-icon-footer font-nunito fs-14">Conheça-nos</a>
                <a href="/ouvidoria" class="d-block mb-2 color-icon-footer font-nunito fs-14">Ouvidoria</a>
                <a href="/cpa" class="d-block mb-2 color-icon-footer font-nunito fs-14">CPA</a>
                <a href="https://zeno.fm/radio/faro-roseira/" target="_blank" class="d-block mb-2 color-icon-footer font-nunito fs-14">Rádio FARO</a>
                <a href="https://volpecom.net/faro/" target="_blank" class="d-block mb-2 color-icon-footer font-nunito fs-14">Inloco</a>
                <a href="/consultar-diploma" class="d-block mb-2 color-icon-footer font-nunito fs-14">Consultar Diploma</a>
             </div>

         </div>
         
      </div>

      <hr class="mt-4 border border-light" />

      <div class="row align-items-center justify-content-between">
         
         <div class="col-lg-7 push-lg-9">

            <div class="mt-3 mb-3">
               <img src="{{ asset('sites-proprios/faro/images/faro-branco.png') }}" width="100" alt="Logotipo {{ $plataforma->nome }}">

               <p class="font-poppins weight-400 fs-13 ls-05 text-light pe-lg-5 mt-2 mb-0">
                  Há +16 anos conectando mentes para juntos construirmos um futuro melhor. Nos acompanhe nessa missão por meio de nossas redes sociais!
               </p>
            </div>

         </div>
         
         <div class="col-lg-2">
            <img src="{{ asset('sites-proprios/faro/images/mec.jpeg') }}" alt="MEC - Faro" class="img-fluid w-xs-100">
         </div>

      </div>

      <hr class="mt-4 border border-light" />

      <div class="pb-3 d-lg-flex justify-content-between align-items-center">

         <p class="text-xs-center mb-1 mb-lg-0 p-0 text-light font-poppins fs-10 ls-05">
            {{ $plataforma->nome }} © 2007 • Todos os direitos reservados • <span class="d-xs-block mt-1">CNPJ: {{ $plataforma->cnpj ?? '32.295.888/0001-23' }}</span>
         </p>

         <p class="text-xs-center">
           <a href="/politica-de-privacidade" target="_blank" class="text-light text-d-none fs-10 weight-600 text-shadow-1">
            Política de Privacidade
           </a>
         </p>

         <p class="text-xs-center m-0 p-0 text-light font-poppins fs-10 ls-05">
            Desenvolvido por <a href="https://wozcode.com" target="_blank" class="text-light text-d-none fs-10 weight-600 text-shadow-1">WOZCODE</a>
         </p>

      </div>

   </div>
</footer>