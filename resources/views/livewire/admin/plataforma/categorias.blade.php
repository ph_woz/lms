<div>

    <form>

        <div class="d-lg-flex mb-3">

            <input wire:model="search" type="search" class="w-lg-90 form-control p-form" placeholder="Buscar">

            <select wire:model="tipo" class="mx-lg-3 my-3 my-lg-0 select-form w-lg-40">
                <option value="">Tipo</option>
                <option value="produto">Produtos</option>
                <option value="plano">Planos</option>
                <option value="servico">Serviços</option>
                <option value="aluno">Alunos</option>
                <option value="avaliacao">Avaliações</option>
                <option value="curso">Cursos</option>
                <option value="trilha">Trilhas</option>
                <option value="questao">Questões</option>
                <option value="formulario">Formulários</option>
                <option value="contato">Contatos</option>
                <option value="blog">Blog</option>
                <option value="documento">Gestão de Documentos</option>
                <option value="controle financeiro">Controle Financeiro</option>
            </select>

            <select wire:model="status" class="select-form w-lg-40">
                <option value="0">Ativos</option>
                <option value="1">Desativados</option>
            </select>

            <select wire:model="publicados" class="ms-lg-3 mt-3 mt-lg-0 select-form w-lg-90">
                <option value="">Publicados e não publicados</option>
                <option value="S">Publicados, apenas</option>
                <option value="N">Não publicados, apenas</option>
            </select>

            <select wire:model="ordem" class="ms-lg-3 mt-3 mt-lg-0 select-form w-lg-40">
                <option value="nome">Alfabética</option>
                <option value="desc">Mais novos</option>
                <option value="asc">Mais antigos</option>
            </select>

        </div>

    </form>

    <div class="mt-5 d-flex justify-content-between">

        <div class="align-self-end mb-2">
            <span class="resultados-encontrados">
                {{ count($categorias) }} resultados de {{ $countCategorias }}.
            </span>
        </div>

        <div class="mb-3">
            <a href="#ModalAddCategoria" data-bs-toggle="modal" class="btn-add aclNivel2">
                + Adicionar
            </a>
        </div>

    </div>

    <div class="row">
        <div class="col-12">

            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col">Nome</th>
                                    <th scope="col">Referência</th>
                                    <th scope="col">Tipo</th>
                                    <th scope="col">
                                        <span class="float-end">
                                            Ações
                                        </span>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($categorias as $categoria)
                                    <tr>
                                        <td>{{ $categoria->nome }}</td>
                                        <td>{!! $categoria->referencia ?? '<i>Não definido</i>' !!}</td>
                                        <td>{{ $categoria->tipo }}</td>
                                        <td>
                                            <span class="float-end">

                                                <a href="#" onclick="document.getElementById('editar_categoria_id').value='{{ $categoria->id }}';document.getElementById('editar_categoria_referencia').value='{{ $categoria->referencia }}';document.getElementById('editar_categoria_nome').value='{{ $categoria->nome }}';document.getElementById('editar_categoria_tipo').value='{{ $categoria->tipo }}';document.getElementById('editar_categoria_status').value='{{ $categoria->status }}';document.getElementById('editar_publicar').value='{{ $categoria->publicar }}'; checkTipoEditar('{{ $categoria->tipo }}'); setEditarDisplayBoxSEO('{{ $categoria->publicar }}'); document.getElementById('editar_seo_title').value='{{ $categoria->seo_title }}';document.getElementById('editar_seo_description').value='{{ $categoria->seo_description }}';document.getElementById('editar_seo_keywords').value='{{ $categoria->seo_keywords }}';" data-bs-target="#ModalEditarCategoria" data-bs-toggle="modal" class="aclNivel2 btn btn-warning text-dark">
                                                    <i class="fa fa-edit"></i>
                                                </a>
                                                <a href="#" onclick="document.getElementById('deletar_categoria_id').value='{{ $categoria->id }}';document.getElementById('deletar_categoria_nome').innerHTML='{{ $categoria->nome }}';" data-bs-target="#ModalDeletarCategoria" data-bs-toggle="modal" class="aclNivel3 btn btn-danger">
                                                    <i class="fa fa-trash"></i>
                                                </a>

                                            </span>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            {{ $categorias->links() }}

        </div>
    </div>

    <form method="POST" action="{{ route('admin.plataforma.editar.configuracoes.categorias') }}">
    @csrf
        <div wire:ignore class="modal fade" id="ModalAddCategoria" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3 class="modal-title" id="exampleModalLabel">Adicionar Categoria</h3>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">

                        <div class="d-lg-flex">

                            <div class="w-lg-65 me-lg-3 mb-3 mb-lg-0">
                                <label>Tipo</label>
                                <select name="tipo" onchange="checkTipoAdd(this.value)" class="select-form" required>
                                    <option value="">Selecione</option>
                                    <option value="produto">Produtos</option>
                                    <option value="plano">Planos</option>
                                    <option value="servico">Serviços</option>
                                    <option value="aluno">Alunos</option>
                                    <option value="avaliacao">Avaliações</option>
                                    <option value="curso">Cursos</option>
                                    <option value="trilha">Trilhas</option>
                                    <option value="questao">Questões</option>
                                    <option value="formulario">Formulários</option>
                                    <option value="contato">Contatos</option>
                                    <option value="blog">Blog</option>
                                    <option value="documento">Gestão de Documentos</option>
                                    <option value="controle financeiro">Controle Financeiro</option>
                                </select>
                            </div>

                            <div class="w-100 me-lg-3 mb-3 mb-lg-0">
                                <label>Categoria</label>
                                <input type="text" name="nome" class="form-control p-form" placeholder="Exemplo: Saúde" required>
                            </div>

                            <div class="w-100 me-lg-3 mb-3 mb-lg-0">
                                <label>Referência</label>
                                <input type="text" name="referencia" class="form-control p-form" placeholder="Exemplo: Áreas de Interesse">
                            </div>

                            <div class="w-lg-50 me-lg-3 mb-3 mb-lg-0" id="boxAddPublicar" style="display: none;">
                                <label>Publicar</label>
                                <select name="publicar" class="select-form" id="add_publicar" onchange="setAddDisplayBoxSEO(this.value)">
                                    <option value="N">Não</option>
                                    <option value="S">Sim</option>
                                </select>
                            </div>

                            <div class="w-lg-50">
                                <label>Status</label>
                                <select name="status" class="select-form">
                                    <option value="0">Ativo</option>
                                    <option value="1">Desativado</option>
                                </select>
                            </div>

                        </div>

                        <section class="mt-4-5" id="boxAddSEO" style="display: none;">

                            <p class="mb-0">
                                Informações de SEO para resultados de buscas em Navegadores
                            </p>
                            <hr class="mt-2" />
                            
                            <div class="collapse show">

                                <div class="d-lg-flex mb-3">

                                    <div class="w-100 me-lg-3 mb-3 mb-lg-0">
                                        <label>Título</label>
                                        <input type="text" name="seo_title" class="form-control p-form" placeholder="Title">
                                    </div>

                                    <div class="w-100">
                                        <label>Palavras-chaves</label>
                                        <input type="text" name="seo_keywords" class="form-control p-form" placeholder="Keywords">
                                    </div>

                                </div>

                                <div class="w-100 mb-3">
                                    <label>Descrição</label>
                                    <input type="text" name="seo_description" class="form-control p-form" placeholder="Description">
                                </div>
                                
                            </div>

                        </section>

                    </div>
                    <div class="modal-footer">
                        <button type="submit" name="addCategoria" value="ok" class="btn btn-primary weight-600 py-2">Confirmar</button>
                    </div>
                </div>
            </div>
        </div>
    </form>


    <form method="POST" action="{{ route('admin.plataforma.editar.configuracoes.categorias') }}">
    @csrf
        <div wire:ignore class="modal fade" id="ModalEditarCategoria" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3 class="modal-title" id="exampleModalLabel">Editar Categoria</h3>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">

                        <input type="hidden" name="categoria_id" id="editar_categoria_id">

                        <div class="d-lg-flex">

                            <div class="w-lg-65 me-lg-3 mb-3 mb-lg-0">
                                <label>Tipo</label>
                                <select name="tipo" onchange="checkTipoEditar(this.value)" id="editar_categoria_tipo" class="select-form" required>
                                    <option value="">Selecione o Tipo</option>
                                    <option value="produto">Produtos</option>
                                    <option value="plano">Planos</option>
                                    <option value="servico">Serviços</option>
                                    <option value="aluno">Alunos</option>
                                    <option value="avaliacao">Avaliações</option>
                                    <option value="curso">Cursos</option>
                                    <option value="trilha">Trilhas</option>
                                    <option value="questao">Questões</option>
                                    <option value="formulario">Formulários</option>
                                    <option value="contato">Contatos</option>
                                    <option value="blog">Blog</option>
                                    <option value="documento">Gestão de Documentos</option>
                                    <option value="controle financeiro">Controle Financeiro</option>
                                </select>
                            </div>

                            <div class="w-100 me-lg-3 mb-3 mb-lg-0">
                                <label>Nome</label>
                                <input type="text" name="nome" id="editar_categoria_nome" class="form-control p-form" placeholder="Exemplo: Saúde">
                            </div>

                            <div class="w-100 me-lg-3 mb-3 mb-lg-0">
                                <label>Referência</label>
                                <input type="text" name="referencia" id="editar_categoria_referencia" class="form-control p-form" placeholder="Exemplo: Áreas de Interesse">
                            </div>

                            <div class="w-lg-35 me-lg-3 mb-3 mb-lg-0" id="boxEditarPublicar" style="display: none;">
                                <label>Publicar</label>
                                <select name="publicar" id="editar_publicar" class="select-form" onchange="setEditarDisplayBoxSEO(this.value)">
                                    <option value="N">Não</option>
                                    <option value="S">Sim</option>
                                </select>
                            </div>

                            <div class="w-lg-30">
                                <label>Status</label>
                                <select name="status" id="editar_categoria_status" class="select-form">
                                    <option value="0">Ativo</option>
                                    <option value="1">Desativado</option>
                                </select>
                            </div>

                        </div>

                        <section class="mt-4-5" id="boxEditarSEO" style="display: none;">

                            <p class="mb-0">
                                Informações de SEO para resultados de buscas em Navegadores
                            </p>
                            <hr class="mt-2" />
                            
                            <div class="collapse show">

                                <div class="d-lg-flex mb-3">

                                    <div class="w-100 me-lg-3 mb-3 mb-lg-0">
                                        <label>Título</label>
                                        <input type="text" name="seo_title" id="editar_seo_title" class="form-control p-form" placeholder="Title">
                                    </div>

                                    <div class="w-100">
                                        <label>Palavras-chaves</label>
                                        <input type="text" name="seo_keywords" id="editar_seo_keywords" class="form-control p-form" placeholder="Keywords">
                                    </div>

                                </div>

                                <div class="w-100 mb-3">
                                    <label>Descrição</label>
                                    <input type="text" name="seo_description" id="editar_seo_description" class="form-control p-form" placeholder="Description">
                                </div>
                                
                            </div>

                        </section>

                    </div>
                    <div class="modal-footer">
                        <button type="submit" name="editarCategoria" value="ok" class="btn btn-primary weight-600 py-2">Confirmar</button>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <form method="POST" action="{{ route('admin.plataforma.editar.configuracoes.categorias') }}">
    @csrf
        <div wire:ignore class="modal fade" id="ModalDeletarCategoria" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3 class="modal-title" id="exampleModalLabel">Deletar Categoria</h3>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body text-center py-5">

                        <input type="hidden" name="categoria_id" id="deletar_categoria_id">

                        <p id="deletar_categoria_nome" class="m-0 fs-15 weight-600">
                            
                        </p>

                        <hr/>

                        <p class="m-0 fs-15">
                            Você tem certeza que deseja deletar esta categoria?
                            <br>
                            Todas as informações serão perdidas definitivamente.
                        </p>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" name="deletarCategoria" value="ok" class="btn btn-danger weight-600">Confirmar</button>
                    </div>
                </div>
            </div>
        </div>
    </form>

</div>

<script>

    function setAddDisplayBoxSEO(value)
    {
        if(value == 'S')
        {
            document.getElementById('boxAddSEO').style.display = 'block';

        } else {

            document.getElementById('boxAddSEO').style.display = 'none';
        }
    }

    function setEditarDisplayBoxSEO(value)
    {
        if(value == 'S')
        {
            document.getElementById('boxEditarSEO').style.display = 'block';

        } else {

            document.getElementById('boxEditarSEO').style.display = 'none';
        }
    }

    function checkTipoAdd(value)
    {
        if(value == 'curso' || value == 'trilha' || 'blog' || 'produto' || 'plano' || 'servico')
        {
            document.getElementById('boxAddPublicar').style.display = 'block';

        } else {
            
            document.getElementById('add_publicar').value = 'N';
            document.getElementById('boxAddPublicar').style.display = 'none';
        }
    }

    function checkTipoEditar(value)
    {
        if(value == 'curso' || value == 'trilha' || 'blog' || 'produto' || 'plano' || 'servico')
        {
            document.getElementById('boxEditarPublicar').style.display = 'block';

        } else {
            
            document.getElementById('editar_publicar').value = 'N';
            document.getElementById('boxEditarPublicar').style.display = 'none';
        }
    }

</script>