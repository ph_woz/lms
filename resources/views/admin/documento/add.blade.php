@extends('layouts.admin.master')

@section('css')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.14.0-beta2/dist/css/bootstrap-select.min.css">
@endsection

@section('content')
    
<div>

    <form method="POST" enctype="multipart/form-data" class="mb-4">
    @csrf

		<section class="mb-4-5">
			
			<a href="#boxInfoPrincipal" data-bs-toggle="collapse" aria-expanded="false" class="hover-d-none">
				<div class="section-title">
					Informações Principais
				</div>
			</a>
			<hr/>
			
			<div class="collapse show" id="boxInfoPrincipal">

		    	<div class="d-lg-flex mb-3">

			    	<div class="w-100 me-lg-3 mb-3 mb-lg-0">
			    		<label>Nome</label>
			    		<input type="text" name="nome" class="form-control p-form" placeholder="Nome" required>
			    	</div>

			    	<div class="w-100">
			    		<label>Referência</label>
			    		<input type="text" name="referencia" list="opts" class="form-control p-form" placeholder="Referência" required>
			    		<datalist id="opts">
			    			<option value="Editais">Editais</option>
			    			<option value="DOU">DOU</option>
			    		</datalist>
			    	</div>

					<div class="w-lg-60 mx-lg-3 my-3 my-lg-0">
						<label>Arquivo</label>
						<input type="file" name="arquivo" class="file-form" required>
					</div>

					<div class="w-lg-60 me-lg-3 mb-3 mb-lg-0">
						<label>Data de Publicação</label>
						<input type="date" name="data_publicacao" class="form-control p-form" required>
					</div>

			    	<div class="w-lg-35 me-lg-3 mb-3 mb-lg-0">
			    		<label>Publicar</label>
			    		<select name="publicar" class="select-form">
			    			<option value="N">Não</option>
			    			<option value="S">Sim</option>
			    		</select>
			    	</div>

			    	<div class="w-lg-35">
			    		<label>Arquivado</label>
			    		<select name="arquivado" class="select-form">
			    			<option value="N">Não</option>
			    			<option value="S">Sim</option>
			    		</select>
			    	</div>

			    </div>

			</div>

		</section>

		<section class="mb-4-5">
			
			<a href="#boxCategorias" data-bs-toggle="collapse" aria-expanded="false" class="hover-d-none">
				<div class="section-title">
					Categorias 
				</div>
			</a>
			<hr/>

			<div class="collapse" id="boxCategorias">
				<select name="categorias[]" class="selectpicker" multiple data-width="100%" data-live-search="true" title="Selecione">
					@foreach($categorias as $categoria)
						<option value="{{ $categoria->id }}">{{ $categoria->nome }}</option>
					@endforeach
				</select>
			</div>

		</section>

		<button type="submit" name="sent" value="ok" class="btn-add mt-3">Cadastrar</button>	

    </form>

</div>


@endsection

@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.14.0-beta2/dist/js/bootstrap-select.min.js"></script>
@endpush
