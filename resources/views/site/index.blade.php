@extends('layouts.site')

@section('seo_title', $pagina->seo_title ?? $nomePlataforma)
@section('seo_description', $pagina->seo_description ?? null)
@section('seo_keywords', $pagina->seo_keywords ?? null)

@section('css')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/uikit@3.7.2/dist/css/uikit.min.css" />
@endsection

@section('content')

<section id="slides">
    <div class="uk-position-relative uk-visible-toggle uk-light" tabindex="-1" uk-slideshow="animation: fade; autoplay: true; autoplay-interval: 5000; pause-on-hover: false; min-height: 550; max-height: 550">

        <ul class="uk-slideshow-items">

           @if(count($banners) == 0)
             <li>
                <div class="uk-background-cover" uk-parallax="bgy: -100" style="background-image: url('{{ asset('images/banner.jpg') }}');" uk-cover>
                </div>
             </li>
           @endif
           @foreach($banners as $banner)
           <li>
              <div class="uk-background-cover" uk-parallax="bgy: -100" style="background-image: url('{{ $banner->link }}');" uk-cover>
                 @if($banner->overlay == 'S')
                    <div class="background-overlay" style="background: {{ $banner->overlay_color ?? 'unset' }};opacity: {{ $banner->overlay_opacity ?? '0' }};"></div>
                 @endif
                 <div class="uk-position-left uk-flex uk-flex-middle uk-position-small uk-text-left uk-light px-xl-8">
                    <div class="pt-lg-5">
                       @if($banner->titulo)
                       <h2 uk-slideshow-parallax="x: 300,0,-100" class="font-poppins fs-md-50 fs-xs-35 weight-700 text-shadow-2">
                          {!! $banner->titulo !!}
                       </h2>
                       @endif
                       @if($banner->subtitulo)
                       <p uk-slideshow-parallax="x: 300,0,-100" class="mt-3 mb-4-5 font-poppins fs-20 text-light">
                          {!! $banner->subtitulo !!}
                       </p>
                       @endif
                       @if($banner->botao_texto)
                       <div uk-slideshow-parallax="x: 300,0,-100">
                          <a href="{{ $banner->botao_link }}" class="btn btn-primary weight-600 shadow px-4 py-2-5 fs-16 font-poppins hover-d-none">
                             {{ $banner->botao_texto }}
                          </a>
                       </div>
                       @endif
                    </div>
                 </div>
              </div>
           </li>
           @endforeach
        </ul>

        <div class="uk-light">
           <a class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous uk-slideshow-item="previous"></a>
           <a class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-next uk-slideshow-item="next"></a>
        </div>

    </div>
</section>

<section id="cursos" class="py-5 overflow-hidden">
   <div class="container-fluid px-xl-7 pt-3">

      <div class="row align-items-center mb-5">
         <div class="col-lg-6">
            <h1 class="p-0 m-0 font-poppins weight-700 text-dark">
               CURSOS
            </h1> 
         </div>
         <form action="/#cursos" class="col-lg-6 d-flex mt-1 mt-lg-0">
            <input type="search" name="buscar" class="form-control p-form rounded-right-0 border" placeholder="Qual curso está procurando?" value="{{ $_GET['buscar'] ?? null }}">
            <button type="submit" class="btn btn-primary weight-600 rounded-left-0">
               Buscar
            </button>
         </form>
      </div>

      @foreach($categorias as $categoria)

        @php
          $cursosIds = \App\Models\CursoCategoria::plataforma()->where('categoria_id', $categoria->id)->get()->pluck('curso_id')->toArray();
          $cursos    = \App\Models\Curso::plataforma()->ativo()->publico();

          if(isset($_GET['buscar']))
            $cursos = $cursos->where('nome', 'like', '%'.$_GET['buscar'].'%');

          $cursos = $cursos->whereIn('id', $cursosIds)->orderBy('nome')->get();  
        @endphp

        @if(count($cursos) > 0)
          <article class="mb-4">

             <p class="mb-0 pb-0 font-poppins fs-17">
                <a href="" class="text-dark hover-d-none"> 
                   {{ $categoria->nome }}
                </a>
             </p>

             <hr class="border border-secondary mt-1" />

             <div class="uk-slider-container-offset" uk-slider="autoplay: true;">

               <div class="uk-position-relative uk-visible-toggle uk-light" tabindex="-1">

                   <ul class="uk-slider-items uk-child-width-1-5@s uk-grid">
                       @foreach($cursos as $curso)
                       <li>
                         <a href="/curso/{{$curso->slug}}" class="hover-d-none">
                           <div class="mb-5 uk-card uk-card-default hoverBox">
                               <div class="uk-card-media-top">
                                   <img src="{{ $curso->foto_capa ?? asset('images/no-image.jpeg') }}" class="card-img-top h-lg-200 object-fit-cover">
                               </div>
                                <div class="card-body h-lg-90 flex-center">
                                   <p class="mb-0 text-dark font-poppins weight-600 text-center"> 
                                      {{ $curso->nome }}
                                   </p>
                                </div>
                               <div class="card-footer bg-primary font-poppins weight-600 text-white text-center text-shadow-1 ls-05">
                                  Matricule-se
                               </div>
                           </div>
                         </a>
                       </li>
                       @endforeach
                   </ul>

                   <a class="uk-position-center-left uk-position-small uk-hidden-hover text-dark" href="#" uk-slidenav-previous uk-slider-item="previous"></a>
                   <a class="uk-position-center-right uk-position-small uk-hidden-hover text-dark" href="#" uk-slidenav-next uk-slider-item="next"></a>

               </div>

             </div>

          </article>
        @endif

      @endforeach

   </div>
</section>

@endsection

@push('scripts')

<div class="box-cookies d-none fixed-bottom bg-1 py-3 px-4">
  <div class="d-flex align-items-center justify-content-between px-xl-3">
     <p class="msg-cookies mb-0 text-light font-inter">
        Este site usa cookies para garantir que você obtenha a melhor experiência. 
        <a href="/politica-de-privacidade" target="_blank" class="text-white text-decoration-underline">
          Política de Privacidade
        </a>
     </p>
     <button class="btn-cookies btn btn-primary weight-600 px-3 rounded-2">Ok!</button>
  </div>
</div>

<script src="https://cdn.jsdelivr.net/npm/uikit@3.7.2/dist/js/uikit.min.js"></script>

<script>
(() => {
  if (!localStorage.getItem('accept')) {
    document.querySelector(".box-cookies").classList.remove('d-none');
  }
  
  const acceptCookies = () => {
    document.querySelector(".box-cookies").classList.add('d-none');
    localStorage.setItem('accept', 'S');
  };
  
  const btnCookies = document.querySelector(".btn-cookies");

  btnCookies.addEventListener('click', acceptCookies);
})();
</script>

@endpush