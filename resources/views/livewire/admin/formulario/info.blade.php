<div>

	<div class="d-flex justify-content-end mb-4">
		<a href="?editar_pergunta_id=0" class="me-lg-2 btn btn-primary fs-13 py-2">
			+ Adicionar Pergunta
		</a>

		<a href="#" data-bs-toggle="modal" data-bs-target="#ModalObterLink" class="btn btn-primary fs-13 py-2">
			Obter Link
			<i class="fas fa-link"></i>
		</a>
	</div>

	<section class="mb-4-5">


		<div class="section-title rounded-bottom-0 d-flex justify-content-between align-items-center">
			{{ $formulario->referencia }}
			<div>
				<a href="#" data-bs-toggle="modal" data-bs-target="#ModalMetaDados" class="btn btn-info me-2 fs-14 mb-1 py-1 px-2-3">
					<i class="fa fa-align-left"></i>
				</a>
				<a href="/admin/formulario/editar/{{$formulario->id}}" class="aclNivel2 btn btn-warning text-dark fs-14 mb-1 py-1 px-2">
					<i class="fa fa-edit"></i>
				</a>
				<a href="#" data-bs-toggle="modal" data-bs-target="#ModalDeletarFormulario" class="aclNivel3 btn btn-danger mb-1 fs-15 py-1 px-2 ms-2">
					<i class="fa fa-trash"></i>
				</a>
			</div>
		</div>

		<div class="bg-white border p-4">

			@if(count($perguntas) > 0)
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">Perguntas</th>
                            <th scope="col">
                                <span class="float-end">
                                    Ações
                                </span>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($perguntas as $pergunta)
                            <tr>
                                <td>{{ $pergunta->pergunta }}</td>
                                <td>
                                    <span class="float-end">

                                        <a href="?editar_pergunta_id={{$pergunta->id}}" class="btn btn-warning text-dark">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                        <a href="#ModalDeletarPergunta" wire:click="setDeletePerguntaId({{$pergunta->id}})" onclick="document.getElementById('remover_pergunta').innerHTML='{{ $pergunta->pergunta }}';" data-bs-toggle="modal" class="btn btn-danger">
                                            <i class="fa fa-trash"></i>
                                        </a>

                                    </span>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
			@else
				<p class="mb-0">Não há perguntas cadastradas.</p>
			@endif

		</div>

	</section>


	@if(isset($editar_pergunta_id) && $editar_pergunta_id == 0)
	<form wire:submit.prevent="addPergunta" method="POST">
		<div wire:ignore.self class="modal fade" id="ModalPerguntaAdd" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  		<div class="modal-dialog modal-lg modal-dialog-centered">
	    		<div class="modal-content">
	      			<div class="modal-header">
	        			<h3 class="modal-title" id="exampleModalLabel">Cadastrar Pergunta</h3>
	        			<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
	      			</div>
	      			<div class="modal-body">

	      				<div class="d-lg-flex">

		      				<div class="me-lg-3 mb-3 mb-lg-0 w-100">
		      					<label>Pergunta</label>
		      					<input type="text" wire:model="pergunta" class="form-control p-form" placeholder="Pergunta" required>
							</div>

							<div class="w-lg-75">
								<label>Tipo do campo</label>
								<select wire:model="tipo_input" class="select-form">
									<option value="text">Dissertativa</option>
									<option value="radio">Múltipla Escolha</option>
									<option value="checkbox">Caixas de Seleção</option>
									<option value="select">Lista</option>
									<option value="file">Arquivo</option>
								</select>
							</div>

							<div class="mx-lg-3 my-3 my-lg-0 w-lg-35">
								<label>Ordem</label>
								<input type="text" wire:model="ordem" class="number form-control p-form" placeholder="N°">
							</div>

							<div class="w-lg-40">
								<label>Obrigatoriedade</label>
								<select wire:model="obrigatorio" class="select-form">
									<option value="S">Obrigatório</option>
									<option value="N">Opcional</option>
								</select>
							</div>

						</div>

						@if($check_integracao_hubspot == true)
						<div class="my-4">
							<label>Propriedade do Hubspot</label>
							<input type="text" wire:model="hubspot_propriedade" class="form-control p-form" placeholder="Propriedade definida no Hubspot">
						</div>
						@endif

						<div class="my-4">

							<label class="mb-1">
								Validação de preenchimento do campo
							</label>
							<select wire:model="validation" class="select-form">
								<option value="">Nenhum</option>
								<optgroup label="Informações Gerais">
									<option value="nome">Nome</option>
									<option value="email">Email</option>
									<option value="password">Senha</option>
									<option value="cpf">CPF</option>
									<option value="rg">RG</option>
									<option value="naturalidade">Naturalidade</option>
									<option value="telefone">Telefone</option>
									<option value="celular">Celular</option>
									<option value="data_nascimento">Data de Nascimento</option>
									<option value="genero">Gênero</option>
									{{-- <option value="curso_categoria_id">Categoria de Curso</option> --}}
									{{-- <option value="curso_id">Curso</option> --}}
								</optgroup>
								<optgroup label="Informações de Endereço">
									<option value="cep">CEP</option>
								    <option value="rua">Logradouro</option>
								    <option value="numero">Número da residência</option>
								    <option value="bairro">Bairro</option>
								    <option value="cidade">Cidade</option>
								    <option value="estado"> Estado</option>
								    <option value="complemento"> Complemento</option>
								    <option value="ponto_referencia"> Ponto de Referência</option>
								</optgroup>
							</select>

						</div>

						<label class="mb-1 d-block">Opções</label>
						@foreach($options as $value => $key)
							<div class="mb-1 d-flex options">
								<input type="text" wire:model="options.{{$value}}" class="form-control p-form" placeholder="Nova Opção">
								<button class="btn btn-danger ms-1" wire:click.prevent="removeOptions({{$value}})"><i class="fa fa-trash"></i></button>
							</div>
						@endforeach

                    	<button class="btn btn-primary fs-13" wire:click.prevent="addOptions({{$i}})">Adicionar outro</button>

	      			</div>
	      			<div class="modal-footer">
	        			<button type="submit" class="btn btn-primary weight-600 py-2">Cadastrar</button>
	      			</div>
	    		</div>
	  		</div>
		</div>
	</form>
	@endif


	@if($perguntaEditar)
	<form wire:submit.prevent="editarPergunta" method="POST">
		<div wire:ignore.self class="modal fade" id="ModalPerguntaEditar" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  		<div class="modal-dialog modal-lg modal-dialog-centered">
	    		<div class="modal-content">
	      			<div class="modal-header">
	        			<h3 class="modal-title" id="exampleModalLabel">Editar Pergunta</h3>
	        			<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
	      			</div>
	      			<div class="modal-body">

	      				<div class="d-lg-flex">

	      					<div class="me-lg-3 mb-3 mb-lg-0 w-100">
	      						<label>Pergunta</label>
		      					<input type="text" wire:model="pergunta" class="form-control p-form" placeholder="Pergunta" required>
							</div>

							<div class="w-lg-75">
								<label>Tipo de Campo</label>
								<select wire:model="tipo_input" class="select-form">
									<option value="text">Dissertativa</option>
									<option value="radio">Múltipla Escolha</option>
									<option value="checkbox">Caixas de Seleção</option>
									<option value="select">Lista</option>
									<option value="file">Arquivo</option>
								</select>
							</div>

							<div class="mx-lg-3 my-3 my-lg-0 w-lg-35">
								<label>Ordem</label>
								<input type="text" wire:model="ordem" class="number form-control p-form" placeholder="N° de Ordem">
							</div>

							<div class="w-lg-40">
								<label>Obrigatoriedade</label>
								<select wire:model="obrigatorio" class="select-form">
									<option value="S">Obrigatório</option>
									<option value="N">Opcional</option>
								</select>
							</div>

						</div>

						@if($check_integracao_hubspot == true)
						<div class="my-4">
							<label>Propriedade do Hubspot</label>
							<input type="text" wire:model="hubspot_propriedade" class="form-control p-form" placeholder="Propriedade definida no Hubspot">
						</div>
						@endif
						
						<div class="my-4">

							<label class="mb-1">
								Validação de preenchimento do campo
							</label>
							<select wire:model="validation" class="select-form">
								<option value="">Nenhum</option>
								<optgroup label="Informações Gerais">
									<option value="nome">Nome</option>
									<option value="email">Email</option>
									<option value="password">Senha</option>
									<option value="cpf">CPF</option>
									<option value="rg">RG</option>
									<option value="naturalidade">Naturalidade</option>
									<option value="telefone">Telefone</option>
									<option value="celular">Celular</option>
									<option value="data_nascimento">Data de Nascimento</option>
									<option value="genero">Gênero</option>
								</optgroup>
								<optgroup label="Informações de Endereço">
									<option value="cep">CEP</option>
								    <option value="rua">Logradouro</option>
								    <option value="numero">Número da residência</option>
								    <option value="bairro">Bairro</option>
								    <option value="cidade">Cidade</option>
								    <option value="estado"> Estado</option>
								    <option value="complemento"> Complemento</option>
								    <option value="ponto_referencia"> Ponto de Referência</option>
								</optgroup>
							</select>

						</div>

						<label class="mb-1 d-block">Opções</label>
						@foreach($options as $value => $key)
							<div class="mb-1 d-flex options">
								<input type="text" wire:model="options.{{$value}}" class="form-control p-form" placeholder="Nova Opção">
								<button class="btn btn-danger ms-1" wire:click.prevent="removeOptions({{$value}})"><i class="fa fa-trash"></i></button>
							</div>
						@endforeach

                    	<button class="btn btn-primary fs-13" wire:click.prevent="addOptions({{$i}})">Adicionar outro</button>

	      			</div>
	      			<div class="modal-footer">
	        			<button type="submit" class="btn btn-primary weight-600 py-2">Salvar Alterações</button>
	      			</div>
	    		</div>
	  		</div>
		</div>
	</form>
	@endif

	<div wire:ignore class="modal fade" id="ModalObterLink" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  		<div class="modal-dialog modal-lg modal-dialog-centered">
    		<div class="modal-content">
      			<div class="modal-header">
        			<h3 class="modal-title" id="exampleModalLabel">Obter Link</h3>
        			<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      			</div>
      			<div class="modal-body">

					@if(count($linksTrilhas) > 0 || count($linksTurmas) > 0)
		            <div class="table-responsive mb-4">
		                <table class="table">
		                    <thead>
		                        <tr>
		                            <th scope="col">Trilha/Turma</th>
		                            <th scope="col">
		                                <span class="float-end">
		                                    Link
		                                </span>
		                            </th>
		                        </tr>
		                    </thead>
		                    <tbody>
		                        @foreach ($linksTrilhas as $trilha)
		                            <tr>
		                                <td>
		                                	<a href="{{ route('admin.trilha.info', ['id' => $trilha->id]) }}" class="color-blue-info-link"> 
		                                		{{ $trilha->referencia }}
		                                	</a>
		                                </td>
		                                <td>
		                                    <span class="float-end">

		                                        <a href="/f/{{$formulario->slug}}/{{$formulario->id}}/t/{{$trilha->id}}" target="_blank" class="btn btn-primary fs-13">
		                                            Abrir
		                                            <i class="fas fa-external-link-alt"></i>
		                                        </a>

		                                    </span>
		                                </td>
		                            </tr>
		                        @endforeach
		                        @foreach ($linksTurmas as $turma)
		                            <tr>
		                                <td>
		                                	<a href="{{ route('admin.turma.info', ['id' => $turma->id]) }}" class="color-blue-info-link"> 
		                                		{{ $turma->nome }}
		                                	</a>
		                                </td>
		                                <td>
		                                    <span class="float-end">

		                                        <a href="/f/{{$formulario->slug}}/{{$formulario->id}}/c/{{$turma->id}}" target="_blank" class="btn btn-primary fs-13">
		                                            Abrir
		                                            <i class="fas fa-external-link-alt"></i>
		                                        </a>

		                                    </span>
		                                </td>
		                            </tr>
		                        @endforeach
		                    </tbody>
		                </table>
		            </div>
					@endif


					@if(count($linksTurmas) == 0 && count($linksTrilhas) == 0)
						<p class="mb-0">Não há Trilhas e Turmas vinculadas a esse Formulário.</p>
					@endif

      			</div>
    		</div>
  		</div>
	</div>

	<div class="modal fade" id="ModalMetaDados" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	  	<div class="modal-dialog modal-dialog-centered" role="document">
	    	<div class="modal-content">
	      		<div class="modal-header">
	        		<h3 class="modal-title">Metadados</h3>
	    			<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
	      		</div>
	      		<div class="modal-body">

					<ul class="list-group list-group-flush">
	  					<li class="list-group-item">
							<span class="mb-0 weight-600 color-444">Cadastrante:</span>
							@if($cadastrante)
								<a href="/admin/colaborador/info/{{$cadastrante->id}}" class="color-blue-info-link">
									{{ $cadastrante->nome }}
								</a>
							@else
								Não definido
							@endif
	  					</li>   
	  					<li class="list-group-item">
							<span class="mb-0 weight-600 color-444">Status:</span>
							@if($formulario->status == 0)
								<span class="text-success weight-700">Ativo</span>
							@else
								<span class="text-danger weight-700">Desativado</span>
							@endif
	  					</li>
	  					<li class="list-group-item">
							<span class="mb-0 weight-600 color-444">Data de Cadastro:</span>
							@if($formulario->created_at)
								<span class="mb-0">{{ $formulario->created_at->format('d/m/Y H:i') ?? null }} ({{$formulario->created_at->diffForHumans()}}) </span>
	  						@else
	  							Não definido
	  						@endif
	  					</li>   
	  				</ul>

	      		</div>
	    	</div>
	  	</div>
	</div>

	<form wire:submit.prevent="deletePergunta" method="POST">
		<div wire:ignore class="modal fade" id="ModalDeletarPergunta" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  		<div class="modal-dialog modal-dialog-centered">
	    		<div class="modal-content">
	      			<div class="modal-header">
	        			<h3 class="modal-title" id="exampleModalLabel">Deletar Pergunta</h3>
	        			<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
	      			</div>
	      			<div class="modal-body text-center py-5">

	        			<p id="remover_pergunta" class="m-0 fs-15 weight-600">
	        				
	        			</p>

	        			<hr/>

	        			<p class="m-0 fs-16">
	        				Você tem certeza que deseja deletar esta pergunta?
	        			</p>
	      			</div>
	      			<div class="modal-footer">
		        		<button type="submit" class="btn btn-danger weight-600">Confirmar</button>
	      			</div>
	    		</div>
	  		</div>
		</div>
	</form>

	<form wire:submit.prevent="delete" method="POST">
		<div wire:ignore class="modal fade" id="ModalDeletarFormulario" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  		<div class="modal-dialog modal-dialog-centered">
	    		<div class="modal-content">
	      			<div class="modal-header">
	        			<h3 class="modal-title" id="exampleModalLabel">Deletar Formulario</h3>
	        			<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
	      			</div>
	      			<div class="modal-body text-center py-5">
	        			<p class="m-0 fs-16">
	        				Você tem certeza que deseja deletar este formulário?
	        				<br>
	        				Todas as informações serão perdidas definitivamente.
	        			</p>
	      			</div>
	      			<div class="modal-footer">
	      				<div class="d-flex w-100">
		      				<input type="password" wire:model="password_to_delete" class="w-100 form-control p-form me-3" placeholder="Digite sua senha" required>
		        			<button type="submit" class="btn btn-danger weight-600">Confirmar</button>
	    				</div>
	      			</div>
	    		</div>
	  		</div>
		</div>
	</form>

	@include('livewire.util.toast')

</div>

