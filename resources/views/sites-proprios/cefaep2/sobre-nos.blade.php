<!doctype html>
<html lang="pt-br">
<head>

  <!-- Google tag (gtag.js) -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=G-MREB450HCT"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'G-MREB450HCT');
  </script>
  
   {!! $plataforma->scripts_start_head !!}

   <meta charset="utf-8">
   <meta name="viewport" content="width=device-width, initial-scale=1">

   <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KK94CHFLLe+nY2dmCWGMq91rCGa5gtU4mk92HdvYe+M/SXH301p5ILy+dN9+nJOZ" crossorigin="anonymous">
   <link rel="stylesheet" href="{{ asset('sites-proprios/cefaep2/css/home.css') }}">
   <link rel="stylesheet" href="{{ asset('css/util.css') }}">

   <title>{{ $pagina->title ?? $plataforma->nome }}</title>
   <meta name="robots" content="index, follow">
   <meta name="description" content="{{ $pagina->description ?? null }}"/>
   <meta name="keywords" content="{{ $pagina->keywords ?? null }}"/>
   <link rel="canonical" href="{{ Request::url() }}" />
   <link rel="shortcut icon" href="{{ $plataforma->favicon }}">
   <meta property="og:locale" content="pt_BR" />
   <meta property="og:type" content="website" />
   <meta property="og:title" content="{{ $pagina->title ?? $plataforma->nome }}" />
   <meta property="og:description" content="{{ $pagina->description ?? null }}" />
   <meta property="og:url" content="{{ Request::url() }}" />
   <meta property="og:site_name" content="{{ $plataforma->nome }}" />
   <meta property="og:image" content="{{ $plataforma->logotipo }}" />
   <meta name='author' content='WOZCODE | Pedro Henrique' />

   {!! $plataforma->scripts_final_head !!}

</head>
<body id="inicio">

	@include('sites-proprios/cefaep2/componentes/header')

	<main>

      <section>
       <div class="w-100 h-200 background-size-cover background-position-center" style="background-image: url('{{ asset('sites-proprios/cefaep2/images/header-quem-somos.webp') }}">
          <div class="position-relative h-100 px-xl-7 px-3">

            <!--
             <div class="row align-items-center h-100 position-relative">
                <div class="mt-2">
                   <h1 class="text-light font-saira-condensed weight-800 fs-xs-35 fs-md-50 text-shadow-3">
                      COMO FUNCIONA
                   </h1>
                </div>
             </div>
            -->
            
          </div>
       </div>
      </section>

      <section class="py-5">
      	<div class="container-fluid px-xl-7">
      			
      		<h1 class="mb-4 title">
      			A <span>CEFAEP</span> veio para ajudar você a realizar seu sonho de concluir seus estudos e dar novos passos em sua evolução profissional e pessoal. 
      		</h1>

      		<div class="row align-items-center">
      			
      			<div class="col-lg-6">

	      			<p class="font-opensans weight-600 color-555 fs-16">
	      				Somos uma Assessoria Pedagógica, com polo EaD de várias Instituição de Ensino que atua em todo o território nacional, tornando possível o término dos estudos em mais de 1612 cidades, nos 26 Estados Brasileiros e no Distrito Federal.
	      			</p>

	      			<p class="font-opensans weight-600 color-555 fs-16">
	      				Nossa missão vai muito além de possibilitar a volta aos estudos de nossos alunos, nós torcemos para que eles concluam essa etapa e sigam para sonhos e planos ainda maiores. 
	      			</p>

	      			<p class="font-opensans weight-600 color-555 fs-16">
	      				Atuamos de acordo com as principais Legislação Educacional Brasileira – LDB de acordo com a normativa para certificação do Ensino Fundamental e Ensino Médio vigentes no Brasil para oferecer o que há de mais acessível para nossos alunos.
	      			</p>

                  <div class="mt-4">
                  
                     <a href="https://cefaep.com.br/f/formulario-de-matricula/23/t/54" target="_blank" class="me-2 btn btn-quero-me-matricular px-4-5 py-3">
                        Quero me matricular
                     </a>

                     <a href="/faq" class="mt-3 mt-lg-0 btn btn-duvidas px-4-5 py-3">
                        Ainda tenho dúvidas
                     </a>
                  
                  </div>

	      		</div>

	      		<div class="col-lg-6">

	      			<img class="img-fluid mt-3 mt-lg-0" src="{{ asset('images/utils/estudantes-formados.webp') }}" alt="Estudantes">

	      		</div>

            </div>
            
      	</div>
      </section>

	</main>

	@include('sites-proprios/cefaep2/componentes/footer')

</body>
</html>