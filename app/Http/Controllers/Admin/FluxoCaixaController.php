<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Util;
use App\Models\User;
use App\Models\Trilha;
use App\Models\Turma;
use App\Models\Unidade;
use App\Models\FluxoCaixa;
use App\Models\FluxoCaixaCategoria;
use App\Models\Categoria;
use App\Models\Log;
use App\Models\Produto;
use App\Models\Plano;
use App\Models\Servico;

class FluxoCaixaController extends Controller
{

    public function lista()
    {
        $getNomePaginaInterno = 'Controle Financeiro';

        $meses    = Util::getMeses();
        $ano      = $_GET['ano'] ?? date('Y');
        $mes      = $_GET['mes'] ?? date('m');

        $clientes = User::select('id','nome')->plataforma()->ativo()->orderBy('nome')->get();

        $anos = FluxoCaixa::distinct()->selectRaw('YEAR(data_vencimento) as data_vencimento ')->plataforma()->get()->pluck('data_vencimento')->toArray();

        $cliente_id = $_GET['cliente_id'] ?? null;
        $clienteSelectedLabel = null;
        $add_entrada_cliente_nome = null;

        if($cliente_id != null)
        {
            $clienteSelected = User::select('nome','email','cpf','celular')->plataforma()->aluno()->where('id', $cliente_id)->first();

            if(isset($clienteSelected))
            {
                $add_entrada_cliente_nome = $clienteSelected->nome;

                $clienteSelectedLabel = $clienteSelected->nome . ' / ' . $clienteSelected->email;

                if(isset($clienteSelected->cpf))
                {
                    $clienteSelectedLabel = $clienteSelectedLabel . ' / ' . $clienteSelected->cpf;
                }
                if(isset($clienteSelected->celular))
                {
                    $clienteSelectedLabel = $clienteSelectedLabel . ' / ' . $clienteSelected->celular;
                }
            }
        }


        $trilhas    = Trilha::select('id','referencia','valor_a_vista')->plataforma()->ativo()->orderBy('referencia')->get();
        $turmas     = Turma::select('id','nome','valor_a_vista')->plataforma()->ativo()->orderBy('nome')->get();

        $unidade_id = null;

        if(\Auth::user()->restringir_unidade == 'S' && \Auth::user()->unidade_id != null)
        {
            $unidade_id = \Auth::user()->unidade_id;

            $unidades = Unidade::select('id','nome')->plataforma()->ativo()->where('id', $unidade_id)->get();

            $vendedores = User::select('id','nome')->plataforma()->colaborador()->where('unidade_id', $unidade_id)->ativo()->orderBy('nome')->get();
            //$trilhas    = Trilha::select('id','referencia','valor_a_vista')->plataforma()->ativo()->where('unidade_id', $unidade_id)->orderBy('nome')->get();
            //$turmas     = Turma::select('id','nome','valor_a_vista')->plataforma()->ativo()->where('unidade_id', $unidade_id)->orderBy('nome')->get();
            
            $planos   = Plano::select('id','nome','valor','valor_promocional')->plataforma()->ativo()->where('unidade_id', $unidade_id)->orderBy('nome')->get();
            $produtos = Produto::select('id','nome','valor','valor_promocional')->plataforma()->ativo()->where('unidade_id', $unidade_id)->orderBy('nome')->get();
            $servicos = Servico::select('id','nome','valor')->plataforma()->ativo()->where('unidade_id', $unidade_id)->orderBy('nome')->get();

        } else {

            $unidades = Unidade::select('id','nome')->plataforma()->ativo()->orderBy('nome')->get();

            $vendedores = User::select('id','nome')->plataforma()->colaborador()->ativo()->orderBy('nome')->get();

            $planos   = Plano::select('id','nome','valor','valor_promocional')->plataforma()->ativo()->orderBy('nome')->get();
            $produtos = Produto::select('id','nome','valor','valor_promocional')->plataforma()->ativo()->orderBy('nome')->get();
            $servicos = Servico::select('id','nome','valor')->plataforma()->ativo()->orderBy('nome')->get();

        }


        $entradas = FluxoCaixa::select(
            'id',
            'registro_id',
            'produto_nome',
            'cliente_nome',
            'valor_parcela',
            'valor_total',
            'n_parcelas',
            'n_parcela',
            'status',
            'data_vencimento'
        )->plataforma()->entrada();

        $saidas = FluxoCaixa::select(
            'id',
            'registro_id',
            'referencia',
            'valor_parcela',
            'valor_total',
            'n_parcelas',
            'n_parcela',
            'status',
            'data_vencimento'
        )->plataforma()->saida();


        if($unidade_id != null)
        {
            $entradas->where('unidade_id', $unidade_id);
            $saidas->where('unidade_id', $unidade_id);
        }

        if(request('produto_id'))
        {
            $produto = $this->getProdutoIdAndTipoProduto(request('produto_id'));

            $entradas->where('produto_id', $produto['produto_id'])->where('tipo_produto', $produto['tipo_produto']);
            $saidas->where('produto_id', $produto['produto_id'])->where('tipo_produto', $produto['tipo_produto']);
        }

        if(request('vendedor_id'))
        {
            $entradas->where('vendedor_id', request('vendedor_id'));
        }
        
        if(request('categoria_id'))
        {
            $fluxoCaixaIds = FluxoCaixaCategoria::where('categoria_id', request('categoria_id'))->get()->pluck('fluxo_caixa_id')->toArray();

            $entradas->whereIn('id', $fluxoCaixaIds);
            $saidas->whereIn('id', $fluxoCaixaIds);
        }

        if(request('buscar'))
        {
            $entradas->where(function ($query)
            {
                $query->orWhere('referencia', 'like', '%'.request('buscar').'%')
                      ->orWhere('cliente_nome', 'like', '%'.request('buscar').'%')
                      ->orWhere('produto_nome', 'like', '%'.request('buscar').'%');
            });

            $saidas->where(function ($query)
            {
                $query->orWhere('referencia', 'like', '%'.request('buscar').'%')
                      ->orWhere('cliente_nome', 'like', '%'.request('buscar').'%')
                      ->orWhere('produto_nome', 'like', '%'.request('buscar').'%');
            });
        }

        $entradas = $entradas->whereYear('fluxo_caixa.data_vencimento', $ano)->whereMonth('fluxo_caixa.data_vencimento', $mes);
        $saidas = $saidas->whereYear('fluxo_caixa.data_vencimento', $ano)->whereMonth('fluxo_caixa.data_vencimento', $mes);
        
        $status = request('status');

        if($status)
        {
            if($status == 'a-receber')
            {
                $entradas = $entradas->where('fluxo_caixa.status', 0)->whereDate('fluxo_caixa.data_vencimento','>=', date('Y-m-d'));
            }

            if($status == 'recebidos-e-pagos')
            {
                $entradas = $entradas->where('fluxo_caixa.status', 1);
                $saidas = $saidas->where('fluxo_caixa.status', 1);
            }

            if($status == 'vencidos')
            {
                $entradas = $entradas->where('fluxo_caixa.status', 0)->whereDate('fluxo_caixa.data_vencimento','<', date('Y-m-d'));
                $saidas = $saidas->where('fluxo_caixa.status', 0)->whereDate('fluxo_caixa.data_vencimento','<', date('Y-m-d'));
            }

            if($status == 'estornados')
            {
                $entradas = $entradas->where('fluxo_caixa.status', 2);
                $saidas = $saidas->where('fluxo_caixa.status', 2);
            }
        }


        $vencidos = clone $entradas;
        $vencidos = $vencidos->where('fluxo_caixa.status', 0)->whereDate('fluxo_caixa.data_vencimento','<', date('Y-m-d'))->select('valor_parcela')->get()->pluck('valor_parcela')->toArray();

        $despesasEmAtraso = clone $saidas;
        $countDespesasEmAtraso = $despesasEmAtraso->where('fluxo_caixa.status', 0)->whereDate('fluxo_caixa.data_vencimento','<', date('Y-m-d'))->count();


        $entradas = $entradas->orderBy('data_vencimento')->get();
        $saidas   = $saidas->orderBy('data_vencimento')->get();

        $categorias = Categoria::ativo()->tipo('Controle Financeiro')->orderBy('nome')->get();


        return view('admin.controle-financeiro.fluxo-caixa.lista-2', 
            compact(
                'getNomePaginaInterno',
                'trilhas',
                'turmas',
                'vendedores',
                'unidades',
                'entradas',
                'saidas',
                'meses',
                'ano',
                'vencidos',
                'countDespesasEmAtraso',
                'clientes',
                'categorias',
                'anos',
                'cliente_id',
                'clienteSelectedLabel',
                'add_entrada_cliente_nome',
                'planos',
                'produtos',
                'servicos',
                'unidade_id'
            ) 
        );
    }


    public function info($produto_id)
    {
        $getNomePaginaInterno = 'Produto';

        return view('admin.produto.info', compact('produto_id','getNomePaginaInterno') );
    }
    
    public function addEntrada()
    {
        $produtoIdAndTipoProduto = $this->getProdutoIdAndTipoProduto(request('add_entrada_produto_id'));

        $entrada = FluxoCaixa::create([
            'vendedor_id'      => request('add_entrada_vendedor_id'),
            'cliente_id'       => request('add_entrada_cliente_id'),
            'cliente_nome'     => request('add_entrada_cliente_nome'),
            'produto_nome'     => request('add_entrada_produto_nome'),
            'produto_id'       => $produtoIdAndTipoProduto['produto_id'],
            'unidade_nome'     => request('add_entrada_unidade_nome'),
            'vendedor_nome'    => request('add_entrada_vendedor_nome'),
            'unidade_id'       => request('add_entrada_unidade_id'),
            'tipo_transacao'   => 'E',
            'tipo_produto'     => $produtoIdAndTipoProduto['tipo_produto'],
            'valor_parcela'    => request('add_entrada_valor'),
            'forma_pagamento'  => request('add_entrada_forma_pagamento'),
            'n_parcela'        => 1,
            'n_parcelas'       => request('add_entrada_n_parcelas'),
            'link'             => request('add_entrada_link'),
            'data_vencimento'  => request('add_entrada_data_vencimento'),
            'obs'              => request('add_entrada_obs'),
        ]);
    

        $lineParcelas = array_map(function ($add_entrada_n_parcela, $add_entrada_data_vencimento, $add_entrada_valor, $add_entrada_forma_pagamento, $add_entrada_link)
        {
          return array_combine(
            ['add_entrada_n_parcela','add_entrada_data_vencimento','add_entrada_valor', 'add_entrada_forma_pagamento', 'add_entrada_link'],
            [$add_entrada_n_parcela, $add_entrada_data_vencimento, $add_entrada_valor, $add_entrada_forma_pagamento, $add_entrada_link]
          );
        }, request('add_entrada_n_parcela') ?? array(), request('add_entrada_datas_vencimento') ?? array(), request('add_entrada_valores') ?? array(), request('add_entrada_formas_pagamento') ?? array(), request('add_entrada_links') ?? array());

        $valores = [];
        $valores[] = request('add_entrada_valor');

        foreach($lineParcelas as $parcela)
        {
            $valores[] = $parcela['add_entrada_valor'];

            FluxoCaixa::create([
                'vendedor_id'      => request('add_entrada_vendedor_id'),
                'cliente_id'       => request('add_entrada_cliente_id'),
                'cliente_nome'     => request('add_entrada_cliente_nome'),
                'produto_nome'     => request('add_entrada_produto_nome'),
                'produto_id'       => $produtoIdAndTipoProduto['produto_id'],
                'unidade_nome'     => request('add_entrada_unidade_nome'),
                'vendedor_nome'    => request('add_entrada_vendedor_nome'),
                'unidade_id'       => request('add_entrada_unidade_id'),
                'registro_id'      => $entrada->id,
                'tipo_transacao'   => 'E',
                'tipo_produto'     => $produtoIdAndTipoProduto['tipo_produto'],
                'n_parcelas'       => request('add_entrada_n_parcelas'),
                'n_parcela'        => $parcela['add_entrada_n_parcela'],
                'valor_parcela'    => $parcela['add_entrada_valor'],
                'forma_pagamento'  => $parcela['add_entrada_forma_pagamento'],
                'link'             => $parcela['add_entrada_link'],
                'data_vencimento'  => $parcela['add_entrada_data_vencimento'],
            ]);
        }

        $valor_total = FluxoCaixa::getValorRealInArray($valores);
        
        $entrada->update(['valor_total' => $valor_total]);

        FluxoCaixa::plataforma()->where('registro_id', $entrada->id)->update(['valor_total' => $valor_total]);

        $this->syncCategorias($entrada->id);


        return redirect($this->routeToReturn(request('add_entrada_data_vencimento')))->with('success', 'Entrada adicionada com êxito.');        
    }

    public function deletarEntrada()
    {
        if(\Auth::attempt(['id' => \Auth::id(), 'password' => request('password_to_delete'), 'status' => 0]))
        {
            $entrada = FluxoCaixa::plataforma()->entrada()->where('id', request('deletar_entrada_id'))->first();

            $deleteAndReturnCountDeleteds = 0;

            if($entrada->registro_id == null)
            {
                $deleteAndReturnCountDeleteds = FluxoCaixa::plataforma()->entrada()->where('registro_id', $entrada->id)->delete();
            }

            Log::create([
                'tipo' => 'fluxo-caixa',
                'ref'  => 'Entrada do Fluxo de Caixa deletada. | ' . $entrada->cliente_nome . ' | ' . $entrada->produto_nome . ' | ' . $entrada->valor_parcela . ' | ' . $deleteAndReturnCountDeleteds . ' parcelas futuras afetadas.',
            ]);

            $entrada->delete();
            
            return back()->with('success', 'Entrada deletada com êxito.');

        } else {

            return back()->with('danger', 'Senha incorreta.');
        }
    }


    public function addSaida()
    {
        $saida = FluxoCaixa::create([
            'referencia'       => request('add_saida_referencia'),
            'unidade_nome'     => request('add_saida_unidade_nome'),
            'unidade_id'       => request('add_saida_unidade_id'),
            'tipo_transacao'   => 'S',
            'valor_parcela'    => request('add_saida_valor'),
            'forma_pagamento'  => request('add_saida_forma_pagamento'),
            'n_parcela'        => 1,
            'n_parcelas'       => request('add_saida_n_parcelas'),
            'link'             => request('add_saida_link'),
            'data_vencimento'  => request('add_saida_data_vencimento'),
            'obs'              => request('add_saida_obs'),
        ]);
    

        $lineParcelas = array_map(function ($add_saida_n_parcela, $add_saida_data_vencimento, $add_saida_valor, $add_saida_forma_pagamento, $add_saida_link)
        {
          return array_combine(
            ['add_saida_n_parcela','add_saida_data_vencimento','add_saida_valor', 'add_saida_forma_pagamento', 'add_saida_link'],
            [$add_saida_n_parcela, $add_saida_data_vencimento, $add_saida_valor, $add_saida_forma_pagamento, $add_saida_link]
          );
        }, request('add_saida_n_parcela') ?? array(), request('add_saida_datas_vencimento') ?? array(), request('add_saida_valores') ?? array(), request('add_saida_formas_pagamento') ?? array(), request('add_saida_links') ?? array());

        $valores = [];
        $valores[] = request('add_saida_valor');

        foreach($lineParcelas as $parcela)
        {
            $valores[] = $parcela['add_saida_valor'];

            FluxoCaixa::create([
                'referencia'       => request('add_saida_referencia'),
                'unidade_nome'     => request('add_saida_unidade_nome'),
                'unidade_id'       => request('add_saida_unidade_id'),
                'registro_id'      => $saida->id,
                'tipo_transacao'   => 'S',
                'n_parcelas'       => request('add_saida_n_parcelas'),
                'n_parcela'        => $parcela['add_saida_n_parcela'],
                'valor_parcela'    => $parcela['add_saida_valor'],
                'forma_pagamento'  => $parcela['add_saida_forma_pagamento'],
                'link'             => $parcela['add_saida_link'],
                'data_vencimento'  => $parcela['add_saida_data_vencimento'],
            ]);
        }

        $valor_total = FluxoCaixa::getValorRealInArray($valores);
        
        $saida->update(['valor_total' => $valor_total]);

        FluxoCaixa::plataforma()->where('registro_id', $saida->id)->update(['valor_total' => $valor_total]);

        $this->syncCategorias($saida->id);


        return redirect($this->routeToReturn(request('add_saida_data_vencimento')))->with('success', 'Saída adicionada com êxito.');        
    }

    public function deletarSaida()
    {
        if(\Auth::attempt(['id' => \Auth::id(), 'password' => request('password_to_delete'), 'status' => 0]))
        {
            $saida = FluxoCaixa::plataforma()->where('id', request('deletar_saida_id'))->first();

            $deleteAndReturnCountDeleteds = 0;

            if($saida->registro_id == null)
            {
                $deleteAndReturnCountDeleteds = FluxoCaixa::plataforma()->saida()->where('registro_id', $saida->id)->delete();
            }

            Log::create([
                'tipo' => 'fluxo-caixa',
                'ref'  => 'Saída do Fluxo de Caixa deletada. | ' . $saida->valor_parcela . ' | ' . $deleteAndReturnCountDeleteds . ' parcelas futuras afetadas.',
            ]);

            $saida->delete();
            
            return back()->with('success', 'Saída deletada com êxito.');

        } else {

            return back()->with('danger', 'Senha incorreta.');
        }
    }

    public function routeToReturn($data)
    {
        $mes = date('m', strtotime($data));
        $ano = date('Y', strtotime($data));

        $routeToReturn = '/admin/controle-financeiro/lista/fluxo-caixa?mes='.$mes.'&ano='.$ano;

        return $routeToReturn;
    }

    public function getProdutoIdAndTipoProduto($add_entrada_produto_id)
    {
        $add_entrada_produto_id = explode('-', $add_entrada_produto_id);

        $tipo_produto = $add_entrada_produto_id[0]; // C ou T

        $produto_id = $add_entrada_produto_id[1];

        $data = ['tipo_produto' => $tipo_produto, 'produto_id' => $produto_id];

        return $data;
    }


    public function syncCategorias($fluxo_caixa_id)
    {
        FluxoCaixaCategoria::where('fluxo_caixa_id', $fluxo_caixa_id)->delete();

        if(request('categoriasSelected') == null)
        {
            return;
        }

        foreach(request('categoriasSelected') as $categoria_id)
        {
            FluxoCaixaCategoria::create([
                'fluxo_caixa_id' => $fluxo_caixa_id, 
                'categoria_id'   => $categoria_id,
            ]);
        }
    }

    public function getDadosEntradaById()
    {
        $entrada_id = $_GET['entrada_id'];

        $entrada = FluxoCaixa::entrada()->where('id', $entrada_id)->first();

        if(is_null($entrada))
        {
            return response()->json(['message' => 'Entrada não encontrada.']);
        }

        if($entrada->status == 0)
        {
            if(date('Y-m-d') > $entrada->data_vencimento)
            {
                $status = '<span class="weight-600 py-2 px-3 text-light rounded bg-danger">Em atraso</span>';
            }

            if(date('Y-m-d') <= $entrada->data_vencimento)
            {
                $status = '<span class="weight-600 py-2 px-3 text-light rounded bg-primary">A receber</span>';
            }

        } elseif ($entrada->status == 1) {
            
            $status = '<span class="weight-600 py-2 px-3 text-light rounded bg-success">Recebido</span>';

        } elseif ($entrada->status == 2) {

            $status = '<span class="weight-600 py-2 px-3 text-light rounded bg-inadimplente">Estornado</span>';
        }

        $entrada->status_label = $status;

        return response()->json($entrada);
    }

    public function getDadosSaidaById()
    {
        $saida_id = $_GET['saida_id'];

        $saida = FluxoCaixa::saida()->where('id', $saida_id)->first();

        if(is_null($saida))
        {
            return response()->json(['message' => 'Saída não encontrada.']);
        }

        if($saida->status == 0)
        {
            if(date('Y-m-d') > $saida->data_vencimento)
            {
                $status = '<span class="weight-600 py-2 px-3 text-light rounded bg-danger">Em atraso</span>';
            }

            if(date('Y-m-d') <= $saida->data_vencimento)
            {
                $status = '<span class="weight-600 py-2 px-3 text-light rounded bg-primary">A pagar</span>';
            }

        } elseif ($saida->status == 1) {
            
            $status = '<span class="weight-600 py-2 px-3 text-light rounded bg-success">Pago</span>';

        } elseif ($saida->status == 2) {

            $status = '<span class="weight-600 py-2 px-3 text-light rounded bg-inadimplente">Estornado</span>';
        }

        $saida->status_label = $status;

        return response()->json($saida);
    }

    public function editarValor()
    {
        $status = FluxoCaixa::plataforma()->where('id', request('registro_id'))->update(['valor_parcela' => request('valor')]);
        
        return response()->json(['status' => $status]);
    }

    public function editarReferencia()
    {
        $status = FluxoCaixa::plataforma()->where('id', request('registro_id'))->update(['referencia' => request('referencia')]);
        
        return response()->json(['status' => $status]);
    }
    
    public function editarLink()
    {
        $status = FluxoCaixa::plataforma()->where('id', request('registro_id'))->update(['link' => request('link_pagamento')]);
        
        return response()->json(['status' => $status]);
    }

    public function editarObs()
    {
        $status = FluxoCaixa::plataforma()->where('id', request('registro_id'))->update(['obs' => request('obs')]);
        
        return response()->json(['status' => $status]);
    }

    public function editarMotivoEstorno()
    {
        $status = FluxoCaixa::plataforma()->where('id', request('registro_id'))->update(['motivo_estorno' => request('motivo_estorno')]);
        
        return response()->json(['status' => $status]);
    }

    public function setStatus()
    {
        $tipo = request('tipo_acao');

        if($tipo == 'concluir_pagamento')
        {
            FluxoCaixa::plataforma()->where('id', request('entrada_id'))->update(['data_recebimento' => request('data_recebimento'), 'status' => 1]);
            
            return back()->with('success', 'Pagamento concluído com êxito.');
        }
        if($tipo == 'concluir_estorno')
        {
            FluxoCaixa::plataforma()->where('id', request('entrada_id'))->update(['data_estorno' => request('data_estorno'), 'motivo_estorno' => request('motivo_estorno'), 'status' => 2]);
            
            return back()->with('success', 'Pagamento concluído com êxito.');
        }
    }

    public function editarDataVencimento()
    {
        $status = FluxoCaixa::plataforma()->where('id', request('registro_id'))->update(['data_vencimento' => request('data_vencimento')]);
        
        return response()->json(['status' => $status]);
    }

    public function editarDataRecebimento()
    {
        $status = FluxoCaixa::plataforma()->where('id', request('registro_id'))->update(['data_recebimento' => request('data_recebimento')]);
        
        return response()->json(['status' => $status]);
    }

    public function editarDataEstorno()
    {
        $status = FluxoCaixa::plataforma()->where('id', request('registro_id'))->update(['data_estorno' => request('data_estorno')]);
        
        return response()->json(['status' => $status]);
    }

    public function editarDataLancamento()
    {
        $status = FluxoCaixa::plataforma()->where('id', request('registro_id'))->update(['data_lancamento' => request('data_lancamento')]);
        
        return response()->json(['status' => $status]);
    }

    public function editarFormaPagamento()
    {
        $status = FluxoCaixa::plataforma()->where('id', request('registro_id'))->update(['forma_pagamento' => request('forma_pagamento')]);
        
        return response()->json(['status' => $status]);
    }

    public function editarVendedor()
    {
        $status = FluxoCaixa::plataforma()->where('id', request('registro_id'))->update(['vendedor_id' => request('vendedor_id'), 'vendedor_nome' => request('vendedor_nome')]);
        
        return response()->json(['status' => $status]);        
    }

    public function editarUnidade()
    {
        $status = FluxoCaixa::plataforma()->where('id', request('registro_id'))->update(['unidade_id' => request('unidade_id'), 'unidade_nome' => request('unidade_nome')]);
        
        return response()->json(['status' => $status]);
    }
    
    public function conversar($aluno_id)
    {
        $celular = User::select('celular')->plataforma()->aluno()->where('id', $aluno_id)->pluck('celular')[0] ?? null;

        if(is_null($celular))
        {
            return back()->with('danger', 'Número de Celular do Aluno não foi definido.');
        }

        $link = 'https://api.whatsapp.com/send?phone=55'.preg_replace('/\D/', '', $celular);

        return redirect($link)->send();
    }

}