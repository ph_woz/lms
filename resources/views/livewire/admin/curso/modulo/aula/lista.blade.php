<div>

    <nav aria-label="breadcrumb" class="mb-4-5 p-3 bg-db rounded ashadow border">
        <ol class="breadcrumb p-0 m-0">
            <li class="breadcrumb-item"><a href="{{ route('admin.curso.lista') }}" class="color-blue-info-link">Cursos</a></li>
            <li class="breadcrumb-item"><a href="{{ route('admin.curso.info', ['id' => $curso->id]) }}" class="color-blue-info-link">{{ $curso->referencia }}</a></li>
            <li class="breadcrumb-item"><a href="{{ route('admin.curso.modulo.lista', ['id' => $curso->id]) }}" class="color-blue-info-link">Módulos</a></li>
            <li class="breadcrumb-item text-dark" aria-current="page">{{ $modulo->nome }}</li>
        </ol>
    </nav>

    <form>

        <div class="d-lg-flex mb-3">

            <input wire:model="search" type="search" class="form-control p-form" placeholder="Buscar">

            <select wire:model="status" class="ms-lg-3 mt-3 mt-lg-0 select-form w-lg-40">
                <option value="0">Ativos</option>
                <option value="1">Desativados</option>
            </select>

        </div>

    </form>

    <div class="mt-5 d-flex justify-content-between">

        <div class="align-self-end mb-2">
            <span class="resultados-encontrados">
                {{ count($aulas) }} resultados de {{ $countAulas }}.
            </span>
        </div>

        <div class="mb-3">
            <a href="{{ route('aluno.aulas', ['slug' => $curso->slug, 'curso_id' => $curso->id]) }}" target="_blank" class="btn btn-primary aclNivel2 me-1">
                Visualizar aulas
            </a>
            <a href="#ModalCadastrarAula" data-bs-toggle="modal" class="btn btn-primary aclNivel2">
                + Cadastar nova Aula
            </a>
        </div>

    </div>

    <div class="row">
        <div class="col-12">

            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col">Nome</th>
                                    <th scope="col">Conteúdo</th>
                                    <th scope="col">
                                        <span class="float-end">
                                            Ações
                                        </span>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($aulas as $aula)
                                    <tr>
                                        <td>{{ $aula->nome }}</td>
                                        <td>{{ $aula->tipo_objeto }}</td>
                                        <td>
                                            <span class="float-end">

                                                <a href="?editar_aula_id={{$aula->id}}" class="btn btn-warning text-dark aclNivel2">
                                                    <i class="fa fa-edit"></i>
                                                </a>
                                                <a href="#ModalDeletarAula" onclick="document.getElementById('deletar_aula_id').value='{{ $aula->id }}';document.getElementById('deletar_aula_nome').innerHTML='{{ $aula->nome }}';" data-bs-toggle="modal" class="btn btn-danger aclNivel3">
                                                    <i class="fa fa-trash"></i>
                                                </a>

                                            </span>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            {{ $aulas->links() }}

        </div>
    </div>

    <form action="{{ route('admin.curso.modulo.aula.add', ['curso_id' => $curso_id, 'modulo_id' => $modulo_id]) }}" method="POST" enctype="multipart/form-data">
    @csrf
        <div wire:ignore class="modal fade" id="ModalCadastrarAula" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3 class="modal-title" id="exampleModalLabel">Cadastrar Aula</h3>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">

                        <div class="d-lg-flex mb-3">

                            <div class="w-100">
                                <label>Nome</label>
                                <input type="text" name="nome" class="form-control p-form" placeholder="Referência" required>
                            </div>
                            <div class="w-lg-75 ms-lg-3 mt-3 mt-lg-0">
                                <label>Tipo de Conteúdo</label>
                                <select name="tipo_objeto" onchange="addTipoConteudo(this)" class="select-form" required>
                                    <option value="" data-fieldupload="disabled">Selecione</option>
                                    <option value="Vídeo" data-fieldupload="disabled">Vídeo</option>
                                    <option value="Áudio" data-fieldupload="enabled">Áudio</option>
                                    <option value="Arquivo" data-fieldupload="enabled">Arquivo</option>
                                    <option value="Imagem" data-fieldupload="enabled">Imagem</option>
                                    <option value="Incorporar site (EMBED)" data-fieldupload="disabled">Incorporar site</option>
                                    <option value="SlideShare" data-fieldupload="disabled">SlideShare</option>
                                    <option value="Link externo" data-fieldupload="disabled">Link externo</option>
                                </select>
                            </div>
                            <div class="mx-lg-3 my-3 my-lg-0 w-lg-40">
                                Status
                                <select name="status" class="select-form">
                                    <option value="0">Ativo</option>
                                    <option value="1">Desativado</option>
                                </select>
                            </div>
                            <div class="w-lg-30">
                                Ordem
                                <input type="number" name="ordem" class="form-control p-form" placeholder="N°">
                            </div>

                        </div>

                        <div class="mb-4 d-lg-flex align-items-center">
                            <div class="w-100" id="boxAddLink">
                                <label>Link</label>
                                <input type="text" name="link" placeholder="Link" class="form-control p-form">
                            </div>
                            <label class="mx-3 mt-4" id="labelAddOU" style="display: none;">OU</label>
                            <div class="w-100" id="boxAddCarregar" style="display: none;">
                                <label>Carregar</label>
                                <input type="file" name="arq" class="file-form">
                            </div>
                        </div>

                        <div>
                            <label>Descrição</label>
                            <textarea name="descricao" class="descricao"></textarea>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="submit" name="cadastrarAula" value="ok" class="btn btn-primary weight-600 py-2">Confirmar</button>
                    </div>
                </div>
            </div>
        </div>
    </form>

    @if(isset($_GET['editar_aula_id']))
        @php
            $editar = \App\Models\CursoAula::plataforma()->where('id', $_GET['editar_aula_id'])->first();
        @endphp
        @if($editar)
            <form action="{{ route('admin.curso.modulo.aula.editar', ['curso_id' => $curso_id, 'modulo_id' => $modulo_id]) }}" method="POST" enctype="multipart/form-data">
            @csrf
                <input type="hidden" name="aula_id" value="{{ $editar->id }}">
                <div wire:ignore class="modal fade" id="ModalEditarAula" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg modal-dialog-centered">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h3 class="modal-title" id="exampleModalLabel">Editar Aula</h3>
                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div class="modal-body">

                                <div class="d-lg-flex mb-3">

                                    <div class="w-100">
                                        Nome
                                        <input type="text" name="nome" class="form-control p-form" placeholder="Referência" required value="{{ $editar->nome }}">
                                    </div>
                                    <div class="w-lg-75 ms-lg-3 mt-3 mt-lg-0">
                                        <label>Tipo de Conteúdo</label>
                                        <select name="tipo_objeto" id="editarTipoObjeto" onchange="editarTipoConteudo(this)" class="select-form" required>
                                            <option value="" data-fieldupload="disabled">Selecione</option>
                                            <option value="Vídeo" data-fieldupload="disabled">Vídeo</option>
                                            <option value="Áudio" data-fieldupload="enabled">Áudio</option>
                                            <option value="Arquivo" data-fieldupload="enabled">Arquivo</option>
                                            <option value="Imagem" data-fieldupload="enabled">Imagem</option>
                                            <option value="Incorporar site (EMBED)" data-fieldupload="disabled">Incorporar site</option>
                                            <option value="SlideShare" data-fieldupload="disabled">SlideShare</option>
                                            <option value="Link externo" data-fieldupload="disabled">Link externo</option>
                                        </select>
                                        <script>
                                            document.getElementById('editarTipoObjeto').value = '{{ $editar->tipo_objeto }}';
                                        </script>
                                    </div>
                                    <div class="mx-lg-3 my-3 my-lg-0 w-lg-40">
                                        Status
                                        <select name="status" class="select-form">
                                            <option value="0">Ativo</option>
                                            <option value="1" @if($editar->status == 1) selected @endif>Desativado</option>
                                        </select>
                                    </div>
                                    <div class="w-lg-30">
                                        Ordem
                                        <input type="number" name="ordem" class="form-control p-form" placeholder="N°" value="{{ $editar->ordem }}">
                                    </div>

                                </div>

                                <div class="mb-4 d-lg-flex align-items-center">
                                    <div class="w-100" id="boxEditarLink">
                                        <label>Link</label>
                                        <input type="text" name="link" placeholder="Link" class="form-control p-form" value="{{ $editar->conteudo }}">
                                    </div>
                                    <label class="mx-3 mt-4" id="labelEditarOU" style="display: none;">OU</label>
                                    <div class="w-100" id="boxEditarCarregar" style="display: none;">
                                        <label>Carregar</label>
                                        <input type="file" name="arq" class="file-form">
                                    </div>
                                </div>

                                <div>
                                    <label>Descrição</label>
                                    <textarea name="descricao" class="descricao">{{ $editar->descricao }}</textarea>
                                </div>

                            </div>
                            <div class="modal-footer">
                                <button type="submit" name="editarAula" value="ok" class="btn btn-primary weight-600 py-2">Salvar Alterações</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <script>
                let obj = document.getElementById('editarTipoObjeto');
                let data = obj.options[obj.selectedIndex].getAttribute('data-fieldupload');
                
                if(data == 'enabled')
                {
                    document.getElementById('boxEditarCarregar').style.display = 'block';
                    document.getElementById('labelEditarOU').style.display = 'block';
                }
            </script>
        @endif
    @endif

    <form action="{{ route('admin.curso.modulo.aula.delete', ['curso_id' => $curso_id, 'modulo_id' => $modulo_id]) }}" method="POST">
    @csrf
        <div wire:ignore class="modal fade" id="ModalDeletarAula" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3 class="modal-title" id="exampleModalLabel">Deletar Aula</h3>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body text-center py-5">

                        <input type="hidden" name="aula_id" id="deletar_aula_id">

                        <p id="deletar_aula_nome" class="m-0 fs-15 weight-600">
                            
                        </p>

                        <hr/>

                        <p class="m-0 fs-15">
                            Você tem certeza que deseja deletar esta aula?
                            <br>
                            Todas as informações serão perdidas definitivamente.
                        </p>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-danger weight-600">Confirmar</button>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <script>

        function addTipoConteudo(obj)
        {
            let data = obj.options[obj.selectedIndex].getAttribute('data-fieldupload');
            
            if(data == 'disabled')
            {
                document.getElementById('boxAddCarregar').style.display = 'none';
                document.getElementById('labelAddOU').style.display = 'none';
            
            } else {

                document.getElementById('boxAddCarregar').style.display = 'block';
                document.getElementById('labelAddOU').style.display = 'block';
            }
        }

        function editarTipoConteudo(obj)
        {
            let data = obj.options[obj.selectedIndex].getAttribute('data-fieldupload');
            
            if(data == 'disabled')
            {
                document.getElementById('boxEditarCarregar').style.display = 'none';
                document.getElementById('labelEditarOU').style.display = 'none';
            
            } else {

                document.getElementById('boxEditarCarregar').style.display = 'block';
                document.getElementById('labelEditarOU').style.display = 'block';
            }
        }
        
    </script>

</div>