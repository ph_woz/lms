<!doctype html>
<html lang="pt-br">
<head>

  <!-- Google tag (gtag.js) -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=G-MREB450HCT"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'G-MREB450HCT');
  </script>
  
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1">

   <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/uikit@3.5.9/dist/css/uikit.min.css" />
   <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
   <link rel="stylesheet" href="{{ asset('css/util.css') }}">
   <link rel="stylesheet" href="{{ asset('sites-proprios/sd-saude/css/home.css') }}">
   <link rel="stylesheet" href="{{ asset('css/pages.css') }}">

   <title>{{ $curso->seo_title ?? $curso->nome . ' - ' . $nomePlataforma }}</title>
   <meta name="robots" content="index, follow">
   <meta name="description" content="{{ $curso->seo_description ?? null }}"/>
   <meta name="keywords" content="{{ $curso->seo_keywords ?? null }}"/>
   <link rel="canonical" href="{{ Request::url() }}" />
   <link rel="shortcut icon" href="{{ $plataforma->favicon }}">
   <meta property="og:locale" content="pt_BR" />
   <meta property="og:type" content="website" />
   <meta property="og:title" content="{{ $curso->seo_title ?? $curso->nome . ' - ' . $nomePlataforma }}" />
   <meta property="og:description" content="{{ $curso->seo_description ?? null }}" />
   <meta property="og:url" content="{{ Request::url() }}" />
   <meta property="og:site_name" content="{{ $plataforma->nome }}" />
   <meta property="og:image" content="{{ $plataforma->logotipo }}" />
   <meta name='author' content='WOZCODE | Pedro Henrique' />

</head>
<body class="bg-f4" id="inicio">

   <header>
      
      <div class="sub-header container-fluid px-xl-7 py-2-3">
         <div class="d-flex justify-content-between align-items-center">

            <div>

               <span class="d-xs-none font-poppins text-light text-shadow-2 fs-13 weight-600">
                  <i class="fa fa-envelope"></i>
                  {{ $plataforma->email ?? 'email@exemplo.com' }}
               </span>

               <a href="https://api.whatsapp.com/send?phone=55{{preg_replace('/\D/', '', $plataforma->whatsapp)}}&text=Olá, vim através do Site SD SAÚDE." target="_blank" class="mx-3 text-shadow-2 text-d-none font-poppins text-light hover-d-none fs-13 weight-600">
                  <i class="fab fa-whatsapp"></i>
                  {{ $plataforma->whatsapp ?? '(00) 00000-0000' }}
               </a>

               <a href="tel:+55{{preg_replace('/\D/', '', $plataforma->telefoen)}}" class="d-xs-none text-shadow-2 text-d-none font-poppins text-light hover-d-none fs-13 weight-600">
                  <i class="fa fa-phone"></i>
                  {{ $plataforma->telefone ?? '(00) 0000-0000' }}
               </a>

            </div>

            <div class="d-flex justify-content-between align-items-end">

               <div>
                  <a href="{{ $plataforma->facebook }}" target="_blank" class="fs-17 text-d-none text-light text-shadow-2">
                       <i class="fab fa-facebook-f flex-center"></i>
                  </a>
                  <a href="{{ $plataforma->instagram }}" target="_blank" class="ms-2 fs-17 text-d-none text-light text-shadow-2">
                     <i class="fab fa-instagram flex-center"></i>
                  </a>
               </div>

            </div>

         </div>
      </div>

      <nav class="navbar navbar-expand-lg bg-white box-shadow py-0 z-3" id="menu">
         <div class="container-fluid px-xl-7">
             <a class="navbar-brand" href="/">
               <img src="{{ $plataforma->logotipo }}" class="d-xs-none" width="100" alt="Logotipo SD Saúde">
               <img src="{{ $plataforma->logotipo }}" class="d-md-none" id="logoMenuMobile" width="75" alt="Logotipo SD Saúde">
             </a>
             <button class="navbar-toggler outline-0 border-0 text-dark" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
               <span class="fa fa-bars"></span>
             </button>
             <div class="collapse navbar-collapse" id="navbarSupportedContent">
                  <ul id="ulLinks" class="navbar-nav ms-auto mb-2 mb-lg-0 align-items-lg-center p-3 p-lg-auto">
                     <li class="nav-item">
                        <a class="nav-link" href="/#cursos">
                           <span class="d-block weight-500">
                              Nossos
                           </span>
                           <span class="weight-700">
                              CURSOS
                           </span>
                        </a>
                     </li>
                     <li class="nav-item">
                        <a class="nav-link" href="/#quem-somos">
                           <span class="d-block weight-500">
                              Sobre a
                           </span>
                           <span class="weight-700">
                              SD SAÚDE
                           </span>
                        </a>
                     </li>
                     <li class="nav-item">
                        <a class="nav-link" href="#inicio">
                           <span class="d-block weight-500">
                              Como
                           </span>
                           <span class="weight-700">
                              INGRESSAR
                           </span>
                        </a>
                     </li>
                     <li class="nav-item">
                        <a class="nav-link" href="#unidades">
                           <span class="d-block weight-500">
                              Nossas
                           </span>
                           <span class="weight-700">
                              UNIDADES
                           </span>
                        </a>
                     </li>
                     <li class="nav-item">
                        <a class="nav-link" href="#contato" uk-scroll="offset: 100;">
                           <span class="d-block weight-500">
                              Central de 
                           </span>
                           <span class="weight-700">
                              ATENDIMENTO
                           </span>
                        </a>
                     </li>
                     <li class="nav-item ms-lg-5 mt-lg-1 mt-4">
                       <a class="nav-link btnInscrever animation-grow d-inline-block" href="#">
                          Inscreva-se
                       </a>
                     </li>
                 </ul>
             </div>
         </div>
      </nav>

   </header>

   <main>

      <section>
         <div class="w-100 h-lg-350 h-xs-100vh" style="background-size: cover; background-position: center; background-image: url('{{ $curso->foto_capa ?? asset('images/bg-login.png') }}');">
            <div class="uk-position-relative h-100 px-xl-7 px-3">
            <div class="background-overlay h-100" style="background: rgba(0,0,0,1);"></div>

               <div class="row align-items-center h-100 uk-position-relative">
                  <div>
                     <h1 class="mb-4-5 text-light font-poppins weight-800 fs-xs-35 fs-md-50 text-shadow-3">
                        <span id="cursoTiulo">{{ $curso->nome }}</span>
                     </h1>
                     <a id="btnCursoInscrevaSe" class="bg-faixa px-4 py-3 rounded font-poppins text-shadow-2 box-shadow hover-d-none text-white weight-700" href="#turmas" uk-scroll="offset: 175;">
                        INSCREVA-SE AGORA!
                     </a>
                  </div>
               </div>
            </div>
         </div>
      </section>

      <section class="pb-3">
         <div class="row container-fluid px-xl-5 mt--15px position-relative pb-5">
            <div class="col-lg-8">
               <div class="bg-white px-lg-5 px-4 pt-4-5 pb-4-5 box-shadow border-top rounded-20">

                  <p class="mb-0 pb-0 font-poppins weight-600 fs-14 color-666">
                     Sobre o curso
                  </p>

                  <hr class="border border-secondary">

                  @if($curso->descricao == null)
                     <p class="mb-0 pb-0 font-poppins weight-600 fs-13 color-666">
                        <i class="fas fa-exclamation-circle"></i> Descrição do Curso não definida.
                     </p>
                  @endif

                  <p id="cursoDescricao">
                     {!! nl2br($curso->descricao) !!}
                  </p>

               </div>
            </div>
            <div class="col-lg-4 mt-4 mt-lg-0">
               <div class="bg-white px-4-5 pt-4-5 pb-4-5 box-shadow border-top rounded-20">

                  @if($curso->video)
                     <iframe width="100%" height="225" class="rounded mb-4" src="{{ $curso->video }}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                  @endif

                      @if($curso->nivel)
                      <div>                                
                          <p class="fs-15 mb-0 weight-600 color-555">NÍVEL:</p>
                          <p class="fs-14 mb-0 weight-600 font-poppins color-444">{{ $curso->nivel }}</p>
                          <hr/>
                      </div>
                      @endif

                      @if($curso->tipo_formacao)
                      <div>                                
                          <p class="fs-15 mb-0 weight-600 color-555">TIPO DE FORMAÇÃO:</p>
                          <p class="fs-14 mb-0 weight-600 font-poppins color-444">{{ $curso->tipo_formacao }}</p>
                          <hr/>
                      </div>
                      @endif
                      
                      @if($curso->carga_horaria)
                      <div>                                
                          <p class="fs-15 mb-0 weight-600 color-555">CARGA HORÁRIA:</p>
                          <p class="fs-14 mb-0 weight-600 font-poppins color-444">{{ $curso->carga_horaria }} horas</p>
                      </div>
                      @endif

               </div>
            </div>
         </div>
      </section>

      <section class="pb-5" id="turmas">
         <div class="row container-fluid px-xl-5 mt--15px position-relative pb-5">
            <div class="col-lg-8">
               <div class="bg-white px-lg-5 px-4 pt-4-5 pb-4-5 box-shadow border-top rounded-20">

                  <p class="font-poppins weight-600 fs-14 color-666">
                     Turmas
                  </p>

                  @if(count($turmas) == 0)
                     <p class="mb-0 pb-0 font-poppins weight-600 fs-13 color-666">
                        <i class="fas fa-exclamation-circle"></i> Não há turmas publicadas disponíveis para inscrição.
                     </p>
                  @endif

                  @foreach($turmas as $turma)
                     <div class="mb-4 p-4 border rounded-20">
                        <div class="d-flex align-items-end justify-content-between">
                           <h1 class="m-0 p-0 fs-17 font-poppins color-faixa-verde">
                              {{ $turma->nome }}
                           </h1>
                           @if($turma->modalidade)
                              <span class="modalidade bg-faixa">
                                 {{ $turma->modalidade }}
                              </span>
                           @endif
                        </div>
                        <hr/>
                        <div>
                           <div class="d-lg-flex justify-content-between">
                              
                              @if($turma->data_inicio)
                              <div class="mb-3 mb-lg-0 d-flex align-items-center">
                                 <span class="me-3">
                                    <i class="fs-25 far fa-calendar-alt"></i>
                                 </span>
                                 <p class="m-0">
                                    <span class="d-block font-poppins fs-13 color-666">Data:</span>
                                    <span class="d-block fs-15 weight-600 color-555">
                                       Início: {{ \App\Models\Util::replaceDatePt($turma->data_inicio) }}
                                    </span>
                                    <span class="d-block fs-15 weight-600 color-555">
                                       Término: {{ \App\Models\Util::replaceDatePt($turma->data_termino) }}
                                    </span>
                                 </p>
                              </div>
                              @endif

                              @if($turma->horario)
                              <div class="mb-3 mb-lg-0 d-flex align-items-center pe-lg-2">
                                 <span class="me-3">
                                    <i class="fs-25 far fa-clock"></i>
                                 </span>
                                 <p class="m-0">
                                    <span class="d-block font-poppins fs-13 color-666">Dias e Horário:</span>
                                    <span class="d-block fs-15 weight-600 color-555">
                                       {{ $turma->frequencia }}
                                    </span>
                                    <span class="d-block fs-15 weight-600 color-555">
                                       {{ $turma->horario }}
                                    </span>
                                 </p>
                              </div>
                              @endif

                              @if($turma->duracao)
                              <div class="mb-3 mb-lg-0 d-flex align-items-center pe-lg-2">
                                 <span class="me-3">
                                    <i class="fs-25 far fa-clock"></i>
                                 </span>
                                 <p class="m-0">
                                    <span class="d-block font-poppins fs-13 color-666">Duração:</span>
                                    <span class="d-block fs-15 weight-600 color-555">
                                       {{ $turma->duracao }}
                                    </span>
                                 </p>
                              </div>
                              @endif

                           </div>

                           @if($turma->data_inicio || $turma->horario || $turma->duracao)
                              <hr/>
                           @endif

                           <div class="d-lg-flex justify-content-between align-items-center">

                              <div class="ps-lg-1">
                                 @if($turma->valor_a_vista)
                                 <p class="d-flex align-items-center m-0 p-0">
                                    <span class="me-2 weight-600 color-555">
                                       por:
                                    </span>
                                    <span class="fs-20 font-poppins color-faixa-verde text-shadow-1 weight-700">
                                       R$ {{ $turma->valor_a_vista }}
                                    </span>
                                 </p>
                                 @endif
                                 @if($turma->valor_parcelado)
                                 <p class="mb-0 me-2 weight-600 color-555">
                                    <span class="me-2 weight-600 color-555">
                                       ou: 
                                    </span>
                                    <span class="fs-20 font-poppins color-faixa-verde text-shadow-1 weight-700">
                                       R$ {{ $turma->valor_parcelado }}
                                    </span>
                                 </p>
                                 @endif
                              </div>

                              <div class="mt-3 mt-lg-0">
                                 <a href="/f/{{$turma->formulario_id ?? 0}}/c/{{$turma->id}}" target="_blank" id="btnCursoMatricular" class="btn-comprar">
                                    QUERO ME MATRICULAR!
                                 </a>
                              </div>
                           </div>

                        </div>
                     </div>
                  @endforeach

               </div>
            </div>
         </div>
      </section>

   </main>

   <footer id="contato" style="background-image: url('{{ asset('sites-proprios/sd-saude/images/bg-rodape.jpg') }}'); background-repeat: no-repeat; background-size: cover; background-position: center;">
      <div class="container-fluid px-xl-7 pt-5">

         <h1 class="fs-40 text-center font-montserrat text-light weight-800">FALE CONOSCO</h1>
         <div class="flex-center mt-4-5 mb-4-5"><div class="w-80px h-2px bg-base-amarelo rounded"></div></div>

         <div class="row align-items-center py-lg-4 px-3 px-md-0">

            <div class="col-lg-6 mt-4 mb-5 mb-lg-0">      

               <div class="mb-4-5">
                 <p class="mb-0 font-opensans weight-600 text-white d-flex align-items-center">
                   <i class="fa fa-clock fs-35 color-base-amarelo"></i>
                   <span class="ms-3">HORÁRIO DE ATENDIMENTO</span>
                 </p>
                 <p class="mt-1 mt-lg-0 mb-0 font-opensans weight-600 fs-15 color-faixa ms-lg-5">
                   &nbsp;Segunda à Sexta-Feira: dàs 07:00h às 18:00h
                 </p>
               </div>

               <div class="mb-4-5">
                 <p class="mb-0 font-opensans weight-600 text-white d-flex align-items-center">
                   <i class="fa fa-envelope fs-35 color-base-amarelo"></i>
                   <span class="ms-3">EMAIL </span>
                 </p>
                 <p class="mt-1 mt-lg-0 mb-0 font-opensans weight-600 fs-15 color-faixa ms-lg-5">
                   &nbsp;{{ $plataforma->email ?? 'email@exemplo.com' }}
                 </p>
               </div>

               <div class="mb-4-5">
                 <p class="mb-0 font-opensans weight-600 text-white d-flex align-items-center">
                   <i class="fab fa-whatsapp fs-40 color-base-amarelo"></i>
                   <span class="ms-3">CONTATO</span>
                 </p>
                 <div class="mt-1 mt-lg-0 mb-0 font-opensans weight-600 fs-15 color-faixa ms-lg-5">
                   <p class="mb-0">
                     Unidade Campos do Jordão:
                     <a href="https://api.whatsapp.com/send?phone=5512992609279" target="_blank" class="text-d-none color-faixa hover-white">(12) 99260-9279</a>
                   </p>
                   <p class="mb-0">
                     Unidade Tremembé:
                     <a href="https://api.whatsapp.com/send?phone=5512992480753" target="_blank" class="text-d-none color-faixa hover-white">(12) 99248-0753</a>
                   </p>
                   <p class="mb-0">
                     Unidade Pinda:
                     <a href="https://api.whatsapp.com/send?phone=5512991511952" target="_blank" class="text-d-none color-faixa hover-white">(12) 99151-1952</a>
                   </p>
                 </div>
               </div>

               <div class="d-flex mt-4">

                  <a href="{{ $plataforma->facebook }}" target="_blank" class="me-4 fs-40 color-base-amarelo color-hover-amarelo">
                       <i class="fab fa-facebook-square"></i>
                  </a>
                  <a href="{{ $plataforma->instagram }}" target="_blank" class="fs-40 color-base-amarelo color-hover-amarelo">
                     <i class="fab fa-instagram flex-center"></i>
                  </a>

               </div>

            </div>

            <div class="col-lg-6">
             <form method="POST" action="/">
             @csrf

               @if(session('success'))
                 <div class='alert alert-success'><b>Sucesso!</b> {{ session('success') }}</div>
               @endif
                
               <input type="hidden" name="assunto" value="Fale Conosco">
               <input type="text" name="nome" class="form-contact mb-2" placeholder="Nome" required>
               <input type="tel" name="celular" class="form-contact mb-2" placeholder="Telefone" required>
               <input type="email" name="email" class="form-contact mb-2" placeholder="Email" required>
               <textarea name="mensagem" placeholder="Mensagem" class="form-contact h-min-100 mb-2 p-3 h-min-100"></textarea>
               
              <div class="my-3">
                <label class="cursor-pointer font-poppins fs-14 text-white">
                   <input type="checkbox" class="" name="consentimento" value="1" required> 
                   Concordo que os dados pessoais fornecidos acima serão utilizados para envio de conteúdo informativo, analítico e publicitário sobre produtos, serviços e assuntos gerais, nos termos da Lei Geral de Proteção de Dados.
                </label>
               </div>

               <input type="text" name="check" class="invisible h-0px d-block" placeholder="Não preencha este campo.">
               <input type="submit" value="Enviar" name="sentContato" class="btnEntrarEmContato">

             </form>
            </div>
            
         </div>

         <div id="unidades">

            <hr class="my-5 border border-light" />

            <h1 class="fs-35 font-montserrat text-light weight-800">UNIDADES</h1>
            <div class="mb-4-5 mt-4 w-80px h-2px bg-base-amarelo rounded"></div>

            <div class="row">

               <div class="mb-4 col-lg-3">
                  <div class="p-4 rounded-2 border-unidade h-lg-325">
                     <p class="mb-0 font-opensans weight-600 color-faixa d-flex align-items-center">
                        PINDA - SP
                     </p>
                     <hr class="border border-faixa" />
                     <p class="font-opensans weight-600 fs-15 color-faixa">
                        <i class="fa fa-envelope color-base-amarelo"></i> 
                        &nbsp;{{ $plataforma->email ?? 'email@exemplo.com' }}
                     </p>
                     <p>
                        <a href="" target="_blank" class="text-d-none hover-white font-opensans weight-600 fs-15 color-faixa">
                           <i class="fab fa-whatsapp color-base-amarelo"></i>
                           &nbsp;(12) 99151-1952
                        </a>
                     </p>
                     <address class="font-opensans weight-600 fs-15 color-faixa">
                        <i class="fa fa-map-marker-alt color-base-amarelo"></i>
                        Pindamonhangaba. CEP 12400131 n 611end. Rua Bicudo Leme bairro Jardim irmãos Braga. Cidade Pindamonhangaba.
                     </address>
                  </div>
               </div>

               <div class="mb-4 col-lg-3">
                  <div class="p-4 rounded-2 border-unidade h-lg-325">
                     <p class="mb-0 font-opensans weight-600 color-faixa d-flex align-items-center">
                        CAMPOS DO JORDÃO - SP
                     </p>
                     <hr class="border border-faixa" />
                     <p class="font-opensans weight-600 fs-15 color-faixa">
                        <i class="fa fa-envelope color-base-amarelo"></i> 
                        &nbsp;{{ $plataforma->email ?? 'email@exemplo.com' }}
                     </p>
                     <p>
                        <a href="https://api.whatsapp.com/send?phone=5512992609279" target="_blank" class="text-d-none hover-white font-opensans weight-600 fs-15 color-faixa">
                           <i class="fab fa-whatsapp color-base-amarelo"></i>
                           &nbsp;(12) 99260-9279
                        </a>
                     </p>
                     <address class="font-opensans weight-600 fs-15 color-faixa">
                        <i class="fa fa-map-marker-alt color-base-amarelo"></i>
                        Campos dos Jordão. CEP 1246000 bairro Vila Telma n 110 end rua Américo richieri cidade Campos dos Jordão.
                     </address>
                  </div>
               </div>

               <div class="mb-4 col-lg-3">
                  <div class="p-4 rounded-2 border-unidade h-lg-325">
                     <p class="mb-0 font-opensans weight-600 color-faixa d-flex align-items-center">
                        TREMEMBÉ - SP
                     </p>
                     <hr class="border border-faixa" />
                     <p class="font-opensans weight-600 fs-15 color-faixa">
                        <i class="fa fa-envelope color-base-amarelo"></i> 
                        &nbsp;{{ $plataforma->email ?? 'email@exemplo.com' }}
                     </p>
                     <p>
                        <a href="https://api.whatsapp.com/send?phone=5512992480753" target="_blank" class="text-d-none hover-white font-opensans weight-600 fs-15 color-faixa">
                           <i class="fab fa-whatsapp color-base-amarelo"></i>
                           &nbsp;(12) 99248-0753
                        </a>
                     </p>
                     <address class="font-opensans weight-600 fs-15 color-faixa">
                        <i class="fa fa-map-marker-alt color-base-amarelo"></i>
                        Tremembé. CEP 12120000 bairro Santana n110 end rua Arcanjo Banhara cidade Tremembé.
                     </address>
                  </div>
               </div>

               <div class="mb-4 col-lg-3">
                  <div class="p-4 rounded-2 border-unidade h-lg-325">
                     <p class="mb-0 font-opensans weight-600 color-faixa d-flex align-items-center">
                        CARAGUATATUBA - SP
                     </p>
                     <hr class="border border-faixa" />
                     <p class="font-opensans weight-600 fs-15 color-faixa">
                        <i class="fa fa-envelope color-base-amarelo"></i> 
                        &nbsp;{{ $plataforma->email ?? 'email@exemplo.com' }}
                     </p>
                     <p>
                        <a href="" target="_blank" class="text-d-none hover-white font-opensans weight-600 fs-15 color-faixa">
                           <i class="fab fa-whatsapp color-base-amarelo"></i>
                           &nbsp;(00) 00000-0000
                        </a>
                     </p>
                     <address class="font-opensans weight-600 fs-15 color-faixa">
                        <i class="fa fa-map-marker-alt color-base-amarelo"></i>
                        Caraguatatuba. CEP 11661140 bairro Sumaré end rua Lorena n 75 cidade Caraguatatuba. 
                      </address>
                  </div>
               </div>

            </div>

         </div>

         <hr class="mt-5 border border-light" />

         <div class="pb-3 d-lg-flex justify-content-between align-items-center">

            <p class="text-xs-center mb-1 mb-lg-0 p-0 color-faixa font-poppins fs-11 ls-05">
               SD SAÚDE © 2021 - Todos os direitos reservados.
            </p>

            <p class="text-xs-center m-0 p-0 color-faixa font-poppins fs-11 ls-05">
               Desenvolvido por <a href="https://wozcode.com" target="_blank" class="text-danger text-d-none fs-11 weight-600 text-shadow-1">WOZCODE</a>
            </p>

         </div>

      </div>
   </footer>

   <a href="https://api.whatsapp.com/send?phone=55{{preg_replace('/\D/', '', $plataforma->whatsapp)}}&text=Olá, vim através do Site SD SAÚDE." target="_blank" class="text-white fs-30">
       <div class="fixed-bottom right-0 me-3 mb-3 z-3">
           <div class="square-60 rounded-circle bg-wpp flex-center box-shadow-wpp">
               <i class="fab fa-whatsapp"></i>
           </div>
       </div>
   </a>

   <script src="https://kit.fontawesome.com/2f91eaaef4.js" crossorigin="anonymous"></script>  
   <script src="https://cdn.jsdelivr.net/npm/uikit@3.5.9/dist/js/uikit.min.js"></script>
   <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-U1DAWAznBHeqEIlVSCgzq+c9gqGAJn5c/t99JyeKa9xxaYpSvHU5awsuZVVFIhvj" crossorigin="anonymous"></script>
   <script>

      var isMobile = {
          Android: function() {
              return navigator.userAgent.match(/Android/i);
          },
          BlackBerry: function() {
              return navigator.userAgent.match(/BlackBerry/i);
          },
          iOS: function() {
              return navigator.userAgent.match(/iPhone|iPad|iPod/i);
          },
          Opera: function() {
              return navigator.userAgent.match(/Opera Mini/i);
          },
          Windows: function() {
              return navigator.userAgent.match(/IEMobile/i) || navigator.userAgent.match(/WPDesktop/i);
          },
          any: function() {
              return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
          }
      };

      if(isMobile.any() != null)
      {
        const navLinks = document.querySelectorAll('.nav-item');
        const menuToggle = document.getElementById('navbarSupportedContent');
        navLinks.forEach((l) => {
            l.addEventListener('click', () => { new bootstrap.Collapse(menuToggle) })
        })
      }

      window.onscroll = function() {myFunction()};

      var header = document.getElementById("menu");
      var sticky = header.offsetTop;

      function myFunction() {
        if (window.pageYOffset > sticky) {
          header.classList.add("fixed-top");
        } else {
          header.classList.remove("fixed-top");
        }
      }
   </script>

</body>
</html>