<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PlataformaSubcategoria extends Model
{
    protected $table = 'plataformas_subcategorias';

	public $timestamps = false;

    public $fillable = [
        'plataforma_id',
        'subcategoria_id',
    ];

    public function scopePlataforma($query)
    {
        return $query->where('plataforma_id', session('plataforma_id') ?? \Auth::user()->plataforma_id);
    }

    public static  function boot()
    {
        parent::boot(); 
        static::creating(function ($model) {
            $model->plataforma_id  = session('plataforma_id') ?? Auth()->user()->plataforma_id ?? Plataforma::dominio()->pluck('id')[0] ?? null;
        });
    }

}
