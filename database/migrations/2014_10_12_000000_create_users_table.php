<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->integer('plataforma_id');
            $table->string('nome');
            $table->string('email');
            $table->string('password');
            $table->string('tipo');    // 'colaborador' ou 'aluno'
            $table->integer('status')->default(0);
            $table->integer('unidade_id')->nullable();
            $table->string('restringir_unidade', 1)->default('N');
            $table->dateTime('data_ultimo_acesso')->nullable();
            $table->dateTime('data_docs_recebidas')->nullable();

            $table->integer('cadastrante_id')->nullable(); 
            $table->string('cpf', 14)->nullable();
            $table->string('rg', 12)->nullable();
            $table->string('naturalidade')->nullable();
            $table->date('data_nascimento')->nullable();
            $table->string('celular')->nullable();
            $table->string('telefone')->nullable();
            $table->string('genero', 1)->nullable();
            $table->string('profissao')->nullable();
            $table->text('foto_perfil')->nullable();
            $table->text('obs')->nullable();
            $table->text('descricao')->nullable();
            $table->string('aceita_termos', 1)->default('N')->nullable();
            $table->text('token')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
