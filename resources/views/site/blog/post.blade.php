@extends('layouts.site')

@section('seo_title', $pagina->seo_title ?? $nomePlataforma)
@section('seo_description', $pagina->seo_description ?? null)
@section('seo_keywords', $pagina->seo_keywords ?? null)

@section('content')
<section class="py-5 mb-5">
	<div class="container-fluid px-xl-7 py-5 mt-2">

		<div class="px-3 py-5">
			{!! nl2br($blog->conteudo ?? null) !!}
		</div>

	</div>
</section>
@endsection