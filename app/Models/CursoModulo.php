<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CursoModulo extends Model
{
    use HasFactory;

    protected $table = 'cursos_modulos';

    public $timestamps = false;

    public $fillable = [

    	'plataforma_id',
    	'cadastrante_id',
    	'curso_id',
    	'nome',
    	'ordem',
    	'status',
    ];

    public static  function boot()
    {
        parent::boot(); 
        static::creating(function ($model)
        {
            $model->plataforma_id = session('plataforma_id') ?? \Auth()->user()->plataforma_id ?? Plataforma::dominio()->pluck('id')[0] ?? null;
            $model->cadastrante_id = \Auth()->user()->id ?? null;
        });
    }

    public function scopePlataforma($query)
    {
        return $query->where('plataforma_id', session('plataforma_id') ?? Plataforma::dominio()->pluck('id')[0] ?? null);
    }

    public function scopeAtivo($query)
    {
        return $query->where('status', 0);
    }

    public function countAulasAtivas($modulo_id)
    {
        $total = CursoAula::plataforma()->ativo()->where('modulo_id', $modulo_id)->count();
        return $total;
    }
}
