<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Models\User;
use App\Models\Plataforma;
use App\Models\PlataformaModulo;
use Illuminate\Support\Facades\Cache;

class PlataformaController extends Controller
{
    public function configuracoes()
    {
    	$plataforma = Plataforma::plataforma()->first();

    	$modulos = PlataformaModulo::plataforma()->whereIn('status', [0,1])->get();

    	$colaboradores = User::plataforma()->colaborador()->ativo()->orderBy('nome')->get();

    	if($plataforma->api_key == null)
        {
            $plataforma->update(['api_key' => $plataforma->id.date('Ymdhi').\App\Models\Util::generateHash()]);
        }

    	if(request('sent'))
    	{
    		$plataforma->update(request()->all());

            $this->syncFoto($plataforma, 'logotipo',  'logo');
            $this->syncFoto($plataforma, 'favicon',   'fav');
            $this->syncFoto($plataforma, 'jumbotron', 'jumb');
            $this->syncFoto($plataforma, 'fundo_login', 'fundoLogin');

    		$this->syncModulos();

            Cache::forget('plataforma-dominio-'.$plataforma->dominio);

    		return back()->with('success', 'Plataforma atualizada com êxito.');
    	}

        return view('admin.plataforma.configuracoes', compact('modulos','colaboradores') );
    }

    public function syncSubcategorias()
    {
        PlataformaSubcategoria::plataforma()->delete();

        foreach(request('subcategorias') ?? array() as $subcategoria_id)
        {
            PlataformaSubcategoria::create(['subcategoria_id' => $subcategoria_id]);
        }
    }

    public function syncComodidades()
    {
        PlataformaComodidade::plataforma()->delete();

        foreach(request('comodidades') ?? array() as $comodidade_id)
        {
            PlataformaComodidade::create(['comodidade_id' => $comodidade_id]);
        }
    }

    public function syncHorarioFuncionamento()
    {

        $dias     = ['Segunda-feira', 'Terça-feira', 'Quarta-feira', 'Quinta-feira', 'Sexta-feira', 'Sábado', 'Domingo'];
        $horasde  = request('horasde') ?? array();
        $horasate = request('horasate') ?? array();

        $horarios = array_map(function ($dia, $hora_de, $hora_ate)
        {
            return array_combine(
                ['dia','hora_de', 'hora_ate'],
                [$dia, $hora_de, $hora_ate]
            );
        }, $dias, $horasde, $horasate);

        foreach($horarios as $horario)
        {
            if(isset($horario['hora_de']) && isset($horario['ate']))
            {
                $fechado = 'N';

                if($horario['hora_de'] == null || $horario['hora_ate'] == null)
                {
                    $fechado = 'S';
                }

                HorarioFuncionamento::plataforma()->where('dia', $horario['dia'])
                    ->update([
                        'de'      => $horario['hora_de'],
                        'ate'     => $horario['hora_ate'],
                        'fechado' => $fechado,
                     ]);
            }
        }
    }

    public function modeloDeEmails()
    {   
        $modeloDeEmails = Plataforma::select('id','email_contratacao_trilha','email_contratacao_turma')->plataforma()->first();
        $getNomePaginaInterno = 'Editar Modelos de Emails';

        if(request('sent'))
        {
            $modeloDeEmails->update(request()->all());

            return back()->with('success', 'Modelos de Emails atualizado com êxito.');
        }

        return view('admin.plataforma.modelos-de-emails', compact('modeloDeEmails','getNomePaginaInterno') );
    }

    public function syncFoto($plataforma, $tipo, $inputFileName)
    {
       	$file = request()->file($inputFileName);

        if($file)
        {
            $path = str_replace(\App\Models\Util::getLinkStorage(), '', $plataforma->$tipo);
            Storage::delete($path);

            $diretorio = 'plataformas/'.session('plataforma_id');
            $filename = $tipo.'.'.$file->getClientOriginalExtension();

            $path = Storage::putFileAs($diretorio, $file, $filename, 'public');
            $linkStorage = \App\Models\Util::getLinkStorage();
            $caminho = $linkStorage . $path;

            $plataforma->update([$tipo => $caminho]);
        }
    }

    public function syncModulos()
    {
        PlataformaModulo::plataforma()->whereNotIn('url', request('modulos'))->update(['status' => 1]);
        PlataformaModulo::plataforma()->whereIn('url', request('modulos'))->update(['status' => 0]);
        PlataformaModulo::plataforma()->where('url', 'plataforma')->update(['status' => 0]);

        foreach (request('modulosTextos') as $moduloId => $texto) 
            PlataformaModulo::where('id', $moduloId)->update(['texto' => $texto]);       
    }


    public function categorias()
    {
        if(request('addCategoria'))
        {
            \App\Models\Categoria::create(request()->all() + ['slug' => \Str::slug(request('nome'))] );

            return back()->with('success', 'Categoria adicionda com êxito.');
        }

        if(request('editarCategoria'))
        {
            \App\Models\Categoria::where('id', request('categoria_id'))->update(request()->only('nome','referencia','tipo','publicar','status','seo_title','seo_description','seo_keywords') + ['slug' => \Str::slug(request('nome'))] );

            return back()->with('success', 'Categoria atualizada com êxito.');
        }

        if(request('deletarCategoria'))
        {
            $categoria = \App\Models\Categoria::plataforma()->where('id', request('categoria_id'))->first();
            
            if($categoria->tipo == 'aluno')
                $table = 'users_categorias';
            
            if($categoria->tipo == 'curso')
                $table = 'cursos_categorias';
            
            if($categoria->tipo == 'questao')
                $table = 'questoes_categorias';
            
            if($categoria->tipo == 'formulario')
                $table = 'formularios_categorias';
            
            if($categoria->tipo == 'contato')
                $table = 'contatos_categorias';

            \DB::table($table)->where('categoria_id', $categoria->id)->delete();

            \App\Models\Log::create([
                'tipo' => 'categoria',
                'ref'  => 'Categoria ('.$categoria->nome.') deletada com êxito.',
            ]);
            
            $categoria->delete();

            return back()->with('success', 'Categoria deletada com êxito.');
        }


        $getNomePaginaInterno = 'Categorias';

        return view('admin.plataforma.categorias', compact('getNomePaginaInterno'));
    }

    public function logs()
    {
        $getNomePaginaInterno = 'Log';

        return view('admin.plataforma.logs', compact('getNomePaginaInterno') );
    }
}