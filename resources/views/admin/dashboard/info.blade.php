@extends('layouts.admin.master')

@section('css')
<style>
	#n_mes_12 { margin-right: 0px!important; }
	#n_mes_{{ $_GET['mes'] ?? date('m') }} .btn-add { background-color: #3b7ddd!important; }
</style>
@endsection

@section('content')
    
    @livewire('admin.dashboard.info', ['diaBloqueio' => $diaBloqueio])

@endsection

@push('scripts')
<script>

	var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
	var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
	  return new bootstrap.Tooltip(tooltipTriggerEl)
	});

	@if($diaBloqueio)
		var myModal = new bootstrap.Modal(document.getElementById("ModalInadimplencia"), {});
		document.onreadystatechange = function () {
		  myModal.show();
		};
	@endif

</script>
@endpush