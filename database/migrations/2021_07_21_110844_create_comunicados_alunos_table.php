<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateComunicadosAlunosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comunicados_alunos', function (Blueprint $table) {
            $table->id();
            $table->integer('plataforma_id');
            $table->integer('comunicado_id');
            $table->integer('aluno_id');
            $table->dateTime('data_visualizacao')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comunicados_alunos');
    }
}
