<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Curso extends Model
{
    use HasFactory;

    protected $table = 'cursos';

	public $timestamps = true;

    public $fillable = [

        'plataforma_id',
        'cadastrante_id',
        'certificado_id',
        'coordenador_id',

        'nome', // nome comercial
        'referencia', // nome interno para ADM
        'slug',

        'slogan',        // curta descrição
        'carga_horaria', // ex: 1.500 horas
        'duracao',       // ex: 5 anos
        'tipo_formacao', // ex: Bacharelado
        'nivel',         // ex: Graduação

        'descricao',
        'foto_capa',
        'foto_miniatura',
        'matriz_curricular',
        'video',
        'exibir_cc',

        'status',
        'publicar',

        // SEO
        'seo_title',
        'seo_keywords',
        'seo_description',

    ];

    public static  function boot()
    {
        parent::boot(); 
        static::creating(function ($model)
        {
            $model->plataforma_id  = session('plataforma_id') ?? \Auth()->user()->plataforma_id ?? Plataforma::dominio()->pluck('id')[0] ?? null;
            $model->cadastrante_id = \Auth()->user()->id ?? null;
        });
    }
    
    public function scopePlataforma($query)
    {
        return $query->where('plataforma_id', session('plataforma_id') ?? Plataforma::dominio()->pluck('id')[0] ?? null);
    }

    public function scopeAtivo($query)
    {
        return $query->where('status', 0);
    }

    public function scopePublico($query)
    {
        return $query->where('publicar', 'S');
    }


    public function scopeGraduacao($query)
    {
        return $query->where('nivel', 'Graduação');
    }

    public function scopePosGraduacao($query)
    {
        return $query->where('nivel', 'Pós-Graduação');
    }

    public function scopeCursosExtensao($query)
    {
        return $query->where('nivel', 'Cursos de Extensão');
    }

}
