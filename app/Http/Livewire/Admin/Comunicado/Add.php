<?php

namespace App\Http\Livewire\Admin\Comunicado;

use Livewire\Component;
use App\Models\Comunicado;
use Illuminate\Support\Facades\Http;

class Add extends Component
{

    public $comunicado;
    public $assunto;
    public $descricao;
    public $arquivar = 'N';

    protected $rules = [
        'assunto'   => 'required',
        'descricao' => 'required',
    ];

    protected $messages = [
        'assunto.required'   => 'O Assunto não pode estar vazio.',
        'descricao.required' => 'A Descrição não pode estar vazio.',
    ];

    public function add()
    {
        $this->validate();

        $this->comunicado = Comunicado::create([
            'assunto'   => $this->assunto,
            'descricao' => $this->descricao,
            'arquivar'  => $this->arquivar,
        ]);

        session()->flash('success-livewire', 'Comunicado criado com êxito.');
        
        $this->resetFields();

        $this->dispatchBrowserEvent('toast');
    }

    public function resetFields()
    {
        $this->assunto   = null;
        $this->descricao = null;
        $this->arquivar  = null;
    }

    public function render()
    {
        return view('livewire.admin.comunicado.add');
    }
}
