@extends('layouts.admin.master')

@section('css')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.14.0-beta2/dist/css/bootstrap-select.min.css">
<style>

	.bg-sell-green { background: #2aa385; }

	.btnNavTab {
	    text-align: center;
	    width: 100%;
	    display: flex; 
	    align-items: center;
	    justify-content: center;
	    height: 50px;
	    color: #666;
	    font-family: 'Nunito', sans-serif;
	    font-weight: 600;
        background: #FFF;
	}


	.btnNavTab:hover { 
	    text-decoration: none; 
	    background: #222e3c; 
	    color: #F9F9F9;
	}

	.lineTab { height: 45px; border:1px solid #eee; }

	.btnNavTab[aria-expanded="true"] { background: #222e3c; }

	#sidebar ul li.active>a,
	.btnNavTab[aria-expanded="true"] {
	    color: #fff;
	}

	.btnNavTab[data-bs-toggle="collapse"] {
	    position: relative;
	}

    #n_mes_12 { margin-right: 0px!important; }
    #n_mes_{{ $_GET['mes'] ?? date('m') }} .btn-add { background-color: #3b7ddd!important; }
    
    .h-50px { height: 50px; }

    .bg-inadimplente { background: #c45210; }
    .text-inadimplente { color: #c45210; }

</style>
@endsection

@section('content')
    
    @livewire('admin.controle-financeiro.fluxo-caixa.lista')

@endsection

@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/gh/farhadmammadli/bootstrap-select@main/js/bootstrap-select.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.js"></script>
<script>

    window.addEventListener('dissmiss-modal', event => {
        $(".modal").modal('hide');
    });
    
    $(function()
    {
        $(document).bind('DOMSubtreeModified', function () {
            $(".mask_valor").mask('#.##0,00', {
              reverse: true
            });
        });
    });

    @if(isset($_GET['add_entrada']) && !isset($_GET['entrada_id']))
        
        var myModal = new bootstrap.Modal(document.getElementById("ModalAddEntrada"), {});
        document.onreadystatechange = function () {
          myModal.show();
        };

    @endif

    @if(isset($_GET['entrada_id']))
        
        var myModal = new bootstrap.Modal(document.getElementById("ModalAcoesEntrada"), {});
        document.onreadystatechange = function () {
          myModal.show();
        };

    @endif

    @if(isset($_GET['saida_id']))
        
        var myModal = new bootstrap.Modal(document.getElementById("ModalAcoesSaida"), {});
        document.onreadystatechange = function () {
          myModal.show();
        };

    @endif

    @if(isset($_GET['editar_entrada_id']))
        
        var myModal = new bootstrap.Modal(document.getElementById("ModalEditarEntrada"), {});
        document.onreadystatechange = function () {
          myModal.show();
        };

    @endif

    @if(isset($_GET['editar_saida_id']))
        
        var myModal = new bootstrap.Modal(document.getElementById("ModalEditarSaida"), {});
        document.onreadystatechange = function () {
          myModal.show();
        };

    @endif

	@if(\Auth::user()->restringir_unidade == 'S' && \Auth::user()->unidade_id != null)
		jQuery("#unidade_id option:contains('Unidades')").remove();
		jQuery("#unidade_id").attr('disabled','true');

        jQuery("#add_unidade_id option:contains('Selecione')").remove();
        jQuery("#add_unidade_id").attr('disabled','true');

	@endif

	var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
	var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
	  return new bootstrap.Tooltip(tooltipTriggerEl)
	});

    $('.btnTabEntrada').click(function(e) {

        e.preventDefault();

        $('#tabSaidas').removeClass('show');
        $('.btnTabSaida').attr('aria-expanded', false);

    });

    $('.btnTabSaida').click(function(e) {

        e.preventDefault();

        $('#tabEntradas').removeClass('show');
        $('.btnTabEntrada').attr('aria-expanded', false);

    });


    $(".number").mask('000');

</script>
@endpush