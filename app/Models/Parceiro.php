<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Parceiro extends Model
{

    protected $table = 'parceiros';

    public $timestamps = false;

    public $fillable = [
        'plataforma_id',
        'nome',
        'logotipo',
        'logotipoWidth',
        'site',
        'publicar',
        'ordem',
    ];

    public static  function boot()
    {
        parent::boot(); 
        static::creating(function ($model)
        {
            $model->plataforma_id  = \Auth()->user()->plataforma_id;
        });
    }

    public function scopePlataforma($query)
    {
        return $query->where('plataforma_id', session('plataforma_id') ?? Plataforma::dominio()->pluck('id')[0] ?? null);
    }

    public function scopePublico($query)
    {
        return $query->where('publicar', 'S');
    }
   
}
