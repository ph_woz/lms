<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Models\Documento;
use App\Models\DocumentoCategoria;
use App\Models\Categoria;

class DocumentoController extends Controller
{
    public function lista()
    {
        return view('admin.documento.lista');
    }
    
    public function info($documento_id)
    {
        return view('admin.documento.info', compact('documento_id') );
    }

    public function add()
    {
        $categorias = Categoria::plataforma()->ativo()->tipo('documento')->orderBy('nome')->get();

        $getNomePaginaInterno = 'Adicionar Documento';

        if(request('sent'))
        {
            $documento = Documento::create(request()->all());

            $this->syncCategorias($documento->id);
            $this->syncDocumento($documento, 'link', 'arquivo');

            return redirect()->route('admin.documento.info', ['id' => $documento->id])->with('success', 'Documento cadastrado com êxito.');
        }

    	return view('admin.documento.add', compact('categorias','getNomePaginaInterno') );
    }

    public function editar($id)
    {
        $getNomePaginaInterno = 'Editar Documento';

        $documento = Documento::plataforma()->where('id', $id)->first();

        $categorias = Categoria::plataforma()->ativo()->tipo('documento')->orderBy('nome')->get();
        
        $categoriasSelected = DocumentoCategoria::select('categoria_id')->plataforma()->where('documento_id', $id)->get()->pluck('categoria_id')->toArray();

        if(request('sent'))
        {
            $documento->update(request()->all());

            $this->syncCategorias($documento->id);
            $this->syncDocumento($documento, 'link', 'arquivo');

            return redirect()->route('admin.documento.info', ['id' => $documento->id])->with('success', 'Documento cadastrado com êxito.');
        }

        return view('admin.documento.editar', compact('categorias','categoriasSelected','getNomePaginaInterno','documento') );
    }

    public function syncDocumento($documento, $tipo, $inputFileName)
    {
        $file = request()->file($inputFileName);
     
        if($file)
        {
            $path = str_replace(\App\Models\Util::getLinkStorage(), '', $documento->$tipo);
            Storage::delete($path);

            $diretorio = 'plataformas/'.\Auth::user()->plataforma_id.'/documentos';
            $filename = $documento->id.'.'.$file->getClientOriginalExtension();

            $path = Storage::putFileAs($diretorio, $file, $filename, 'public');
            $linkStorage = \App\Models\Util::getLinkStorage();
            $caminho = $linkStorage . $path;

            $documento->update([$tipo => $caminho]);
        }
    }

    public function syncCategorias($documento_id)
    {
        if(request('categorias') == null)
        {
            return;
        }

        foreach(request('categorias') as $categoria_id)
        {
            DocumentoCategoria::create([
                'plataforma_id' => session('plataforma_id'),
                'documento_id'  => $documento_id, 
                'categoria_id'  => $categoria_id,
            ]);
        }
    }

}