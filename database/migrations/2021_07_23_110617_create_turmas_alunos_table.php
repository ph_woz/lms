<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTurmasAlunosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('turmas_alunos', function (Blueprint $table) {
            $table->id();
            $table->integer('plataforma_id');
            $table->integer('turma_id');
            $table->integer('curso_id');
            $table->integer('aluno_id');
            $table->dateTime('data_primeiro_acesso')->nullable();
            $table->dateTime('data_ultimo_acesso')->nullable();
            $table->dateTime('data_conclusao_curso')->nullable();
            $table->date('data_colacao_grau')->nullable();
            $table->dateTime('data_conclusao_conteudo')->nullable();
            $table->dateTime('data_expiracao')->nullable();
            $table->integer('status')->default(0);
            $table->string('codigo_certificado')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('turmas_alunos');
    }
}
