<?php

namespace App\Http\Livewire\Admin\Avaliacao;

use Livewire\Component;
use App\Models\Avaliacao;
use App\Models\Curso;
use App\Models\Turma;
use App\Models\AvaliacaoConfigNota;
use App\Models\AvaliacaoCategoria;
use App\Models\Categoria;
use Illuminate\Support\Facades\Http;

class Add extends Component
{
    public $avaliacao;
    public $cursos;
    public $curso_id;
    public $turmas = [];
    public $turma_id;
    public $referencia;
    public $condicao_fazer_avaliacao;
    public $n_tentativas;
    public $liberar_gabarito = 'S';
    public $instrucoes;
    public $status = 0;

    public $i = 0;
    public $questoesQuantidadesMinimas = [];
    public $questoesNotas = [];

    public $categoriasSelected = [];

    protected $rules = [
        'referencia' => 'required',
        'curso_id'   => 'required',
        'turma_id'   => 'required',
    ];

    protected $messages = [
        'referencia.required' => 'O Nome não pode estar vazio.',
        'curso_id.required' => 'O Curso não pode estar vazio.',
        'turma_id.required' => 'A Turma não pode estar vazio.',
    ];

    public function updated()
    {
        if($this->curso_id)
        {
            $this->turmas = Turma::plataforma()->ativo()->where('curso_id', $this->curso_id)->select('id','nome')->orderBy('nome')->get();
        }
    }

    public function mount()
    {
        $this->cursos = Curso::plataforma()->ativo()->orderBy('referencia')->get();

        $this->addCampoNota($this->i);
    }

    public function add()
    {
        $this->validate();

        $this->avaliacao = Avaliacao::create([
            'curso_id'                 => $this->curso_id,
            'turma_id'                 => $this->turma_id,
            'referencia'               => $this->referencia,
            'condicao_fazer_avaliacao' => $this->condicao_fazer_avaliacao,
            'n_tentativas'             => $this->n_tentativas,
            'liberar_gabarito'         => $this->liberar_gabarito,
            'instrucoes'               => $this->instrucoes,
            'status'                   => $this->status,
        ]);

        $notas = array_combine($this->questoesQuantidadesMinimas, $this->questoesNotas);

        foreach ($notas as $n_questoes => $valor_nota)
        {
            if($n_questoes !== null && $valor_nota !== null)
            {
                $countDuplicate = AvaliacaoConfigNota::plataforma()->where('avaliacao_id', $this->avaliacao->id)->where('valor_nota', $valor_nota)->count();

                if($countDuplicate == 0)
                {
                    AvaliacaoConfigNota::create([
                        'avaliacao_id' => $this->avaliacao->id,
                        'n_questoes'   => $n_questoes,
                        'valor_nota'   => $valor_nota,
                    ]);
                }
            }
        }

        $this->syncCategorias($this->avaliacao->id);

        session()->flash('success-livewire', 'Avaliação criada com êxito.');
        
        $this->resetFields();

        $this->dispatchBrowserEvent('toast');
    }

    public function syncCategorias($avaliacao_id)
    {
        if($this->categoriasSelected == null)
        {
            return;
        }

        foreach($this->categoriasSelected as $categoria_id)
        {            
            AvaliacaoCategoria::create([
                'plataforma_id' => session('plataforma_id'),
                'avaliacao_id'  => $avaliacao_id, 
                'categoria_id'  => $categoria_id,
            ]);
        }
    }

    public function addCampoNota($i)
    {
        $this->i = $i+1;
        $this->questoesQuantidadesMinimas[$this->i] = null;
    }

    public function removeCampoNota($i)
    {
        unset($this->questoesQuantidadesMinimas[$i]);
    }

    public function resetFields()
    {
        $avaliacao                = null;
        $curso_id                 = null;
        $turma_id                 = null;
        $referencia               = null;
        $condicao_fazer_avaliacao = null;
        $n_tentativas             = null;
        $liberar_gabarito         = null;
        $instrucoes               = null;
        $status                   = 0;
    }

    public function render()
    {
        $listaNotas = Avaliacao::listaNotas();
        $categorias = Categoria::plataforma()->ativo()->tipo('avaliacao')->orderBy('nome')->get();

        return view('livewire.admin.avaliacao.add', ['listaNotas' => $listaNotas, 'categorias' => $categorias] );
    }

}
