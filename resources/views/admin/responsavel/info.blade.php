@extends('layouts.admin.master')

@section('css')
   <style>
         
      .square-125 { width: 125px; height: 125px; }
      .object-fit-contain { object-fit: contain; }
      .border-profile { border:7px solid #ededed; }
      .color-777 { color: #777; }
      .color-555 { color: #555; }
      .color-333 { color: #333; }

      .fs-13 { font-size: 13px; }
      .fs-16 { font-size: 16px; }
      .fs-17 { font-size: 17px; }
      .fs-19 { font-size: 19px; }
         
      @media (min-width: 769px) {
         .fs-md-75 { font-size: 75px; }
      }
      @media (max-width: 768px) {
         .fs-xs-50 { font-size: 50px; }
      }

      .rounded-bottom-0 { border-bottom-right-radius: 0px!important; border-bottom-left-radius: 0px!important; }

   </style>
@endsection

@section('content')
    
    @livewire('admin.responsavel.info', ['responsavel_id' => $responsavel_id])

@endsection

@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>
<script>
   window.addEventListener('dissmiss-modal', event => {
      $(".modal").modal('hide');
   });
</script>
@endpush