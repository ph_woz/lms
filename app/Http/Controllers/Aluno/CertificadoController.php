<?php

namespace App\Http\Controllers\Aluno;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Codedge\Fpdf\Fpdf\Fpdf;
use App\Models\Certificado;
use App\Models\TurmaAluno;
use App\Models\TrilhaTurma;
use App\Models\TrilhaAluno;

class CertificadoController extends Controller
{
    public function gerarCertificado($certificado_id, $turma_id)
    {
        $checkConclusao = TurmaAluno::plataforma()->where('turma_id', $turma_id)->where('aluno_id', \Auth::id())->whereNotNull('data_conclusao_curso')->first();
        
        if(is_null($checkConclusao))
        {
            return back()->with('danger', 'Você não pode baixar esse certificado.');
        }

        $certificado = Certificado::plataforma()->find($certificado_id);

        $user_id = \Auth::id();
        $texto1  = $certificado->replaceVariaveis($certificado->texto1, $user_id, $turma_id, null);
        $texto2  = $certificado->replaceVariaveis($certificado->texto2, $user_id, $turma_id, null);

        $title  = $certificado->referencia;
        $codigo = $checkConclusao->codigo_certificado;


        $posicaoNomeX   = $certificado->nome_posicao_x   ?? 20;
        $posicaoNomeY   = $certificado->nome_posicao_y   ?? 85;
        $fontSizeNome   = $certificado->nome_fontSize    ?? 30;

        $posicaoTexto1X = $certificado->texto1_posicao_x ?? 20;
        $posicaoTexto1Y = $certificado->texto1_posicao_y ?? 117;
        $fontSizeTexto1 = $certificado->texto1_fontSize  ?? 15;

        $posicaoTexto2X = $certificado->texto2_posicao_x ?? 20;
        $posicaoTexto2Y = $certificado->texto2_posicao_y ?? 150;
        $fontSizeTexto2 = $certificado->texto2_fontSize  ?? 15;

        $codigoPosicaoX = $certificado->codigo_posicao_x ?? 233;
        $codigoPosicaoY = $certificado->codigo_posicao_y ?? 179;
        $fontSizeCodigo = $certificado->codigo_fontSize  ?? 15;


        $pdf = new Fpdf();
        $pdf->SetTitle(utf8_decode($title));
        $pdf->AddPage('L');
        $pdf->SetLineWidth(1.5);
        $pdf->Image($certificado->modelo, 0, 0, $certificado->posicao_imagem ?? 300);

        // Nome
        $pdf->SetFont('Arial', '', $fontSizeNome);
        $pdf->SetXY($posicaoNomeX, $posicaoNomeY); 
        $pdf->MultiCell(265, 6, utf8_decode(\Auth::user()->nome), '', 'C', 0); 

        // Texto 1
        $pdf->SetFont('Arial', '', $fontSizeTexto1); 
        $pdf->SetXY($posicaoTexto1X, $posicaoTexto1Y);
        $pdf->MultiCell(265, 9, utf8_decode($texto1), '', 'C', 0); 

        // Texto 2
        $pdf->SetFont('Arial', '', $fontSizeTexto2); 
        $pdf->SetXY($posicaoTexto2X, $posicaoTexto2Y); 
        $pdf->MultiCell(165, 9, utf8_decode($texto2), '', 'L', 0);

        // Código de Validação
        $pdf->SetFont('Arial', '', $fontSizeCodigo); 
        $pdf->SetXY($codigoPosicaoX, $codigoPosicaoY);
        $pdf->MultiCell(165, 9, utf8_decode($codigo), '', 'L', 0);


        $pdf->Output();
    }

    public function gerarCertificadoTrilha($certificado_id, $trilha_id)
    {
        $checkConclusao = TrilhaAluno::plataforma()->where('trilha_id', $trilha_id)->where('aluno_id', \Auth::id())->whereNotNull('data_conclusao')->first();
        
        if(is_null($checkConclusao))
        {
            return back()->with('danger', 'Você não pode baixar esse certificado.');
        }

        $certificado = Certificado::plataforma()->find($certificado_id);

        $user_id = \Auth::id();
            
        $texto1 = null;
        $texto2 = null;

        if($certificado->texto1 != null)
        {
            $texto1 = $certificado->replaceVariaveis($certificado->texto1, $user_id, null, $trilha_id);
        }

        if($certificado->texto2 != null)
        {
            $texto2 = $certificado->replaceVariaveis($certificado->texto2, $user_id, null, $trilha_id);
        }


        $title  = $certificado->referencia;
        $codigo = $checkConclusao->codigo_certificado;


        $posicaoNomeX   = $certificado->nome_posicao_x   ?? 20;
        $posicaoNomeY   = $certificado->nome_posicao_y   ?? 85;
        $fontSizeNome   = $certificado->nome_fontSize    ?? 30;

        $posicaoTexto1X = $certificado->texto1_posicao_x ?? 20;
        $posicaoTexto1Y = $certificado->texto1_posicao_y ?? 117;
        $fontSizeTexto1 = $certificado->texto1_fontSize  ?? 15;

        $posicaoTexto2X = $certificado->texto2_posicao_x ?? 20;
        $posicaoTexto2Y = $certificado->texto2_posicao_y ?? 150;
        $fontSizeTexto2 = $certificado->texto2_fontSize  ?? 15;

        $codigoPosicaoX = $certificado->codigo_posicao_x ?? 233;
        $codigoPosicaoY = $certificado->codigo_posicao_y ?? 179;
        $fontSizeCodigo = $certificado->codigo_fontSize  ?? 15;


        $pdf = new Fpdf();
        $pdf->SetTitle(utf8_decode($title));
        $pdf->AddPage('L');
        $pdf->SetLineWidth(1.5);
        $pdf->Image($certificado->modelo, 0, 0, $certificado->posicao_imagem ?? 300);

        // Nome
        $pdf->SetFont('Arial', '', $fontSizeNome);
        $pdf->SetXY($posicaoNomeX, $posicaoNomeY); 
        $pdf->MultiCell(265, 6, utf8_decode(\Auth::user()->nome), '', 'C', 0); 

        // Texto 1
        $pdf->SetFont('Arial', '', $fontSizeTexto1); 
        $pdf->SetXY($posicaoTexto1X, $posicaoTexto1Y);
        $pdf->MultiCell(265, 9, utf8_decode($texto1), '', 'C', 0); 

        // Texto 2
        $pdf->SetFont('Arial', '', $fontSizeTexto2); 
        $pdf->SetXY($posicaoTexto2X, $posicaoTexto2Y); 
        $pdf->MultiCell(165, 9, utf8_decode($texto2), '', 'L', 0);

        // Código de Validação
        $pdf->SetFont('Arial', '', $fontSizeCodigo); 
        $pdf->SetXY($codigoPosicaoX, $codigoPosicaoY);
        $pdf->MultiCell(165, 9, utf8_decode($codigo), '', 'L', 0);


        if(isset($trilha_id) && $trilha_id != null)
        {
            $disciplinas = TrilhaTurma::select('turmas.nome')
                ->join('turmas','trilhas_turmas.turma_id','turmas.id')
                ->where('trilhas_turmas.plataforma_id', $certificado->plataforma_id)
                ->where('turmas.status', 0)
                ->where('trilhas_turmas.trilha_id', $trilha_id)
                ->get()
                ->pluck('nome')
                ->toArray();
        
            if(count($disciplinas) > 0)
            {
                $pdf->SetFont('Arial', 'B', 15); 
                $pdf->SetXY(10, 200);
                $pdf->MultiCell(165, 9, utf8_decode('DISCIPLINAS APLICADAS'), '', 'L', 0);

                $pdf->SetFont('Arial', '', 15); 
                foreach($disciplinas as $disciplina)
                {
                    $pdf->MultiCell(165, 9, utf8_decode($disciplina), '', 'L', 0);
                }
            }
        }


        $pdf->Output();
    }


    public function gerarAtestadoTrilha($certificado_id, $trilha_id)
    {
        $checkPermissao = TrilhaAluno::plataforma()->where('trilha_id', $trilha_id)->where('aluno_id', \Auth::id())->exists();
        
        if($checkPermissao == false)
        {
            return back()->with('danger', 'Você não tem permissão para baixar este atestado de matrícula.');
        }


        $certificado = Certificado::plataforma()->find($certificado_id);

        $user_id = \Auth::id();
        $texto1  = $certificado->replaceVariaveis($certificado->texto1, $user_id, null, $trilha_id);
        $texto2  = $certificado->replaceVariaveis($certificado->texto2, $user_id, null, $trilha_id);

        $title  = $certificado->referencia;

        $posicaoNomeX   = $certificado->nome_posicao_x   ?? 20;
        $posicaoNomeY   = $certificado->nome_posicao_y   ?? 85;
        $fontSizeNome   = $certificado->nome_fontSize    ?? 30;

        $posicaoTexto1X = $certificado->texto1_posicao_x ?? 20;
        $posicaoTexto1Y = $certificado->texto1_posicao_y ?? 117;
        $fontSizeTexto1 = $certificado->texto1_fontSize  ?? 15;

        $posicaoTexto2X = $certificado->texto2_posicao_x ?? 20;
        $posicaoTexto2Y = $certificado->texto2_posicao_y ?? 150;
        $fontSizeTexto2 = $certificado->texto2_fontSize  ?? 15;


        $pdf = new Fpdf();
        $pdf->SetTitle(utf8_decode($title));
        $pdf->AddPage('L');
        $pdf->SetLineWidth(1.5);
        $pdf->Image($certificado->modelo, 0, 0, $certificado->posicao_imagem ?? 300);

        // Nome
        $pdf->SetFont('Arial', '', $fontSizeNome);
        $pdf->SetXY($posicaoNomeX, $posicaoNomeY); 
        $pdf->MultiCell(265, 6, utf8_decode(\Auth::user()->nome), '', 'C', 0); 

        // Texto 1
        $pdf->SetFont('Arial', '', $fontSizeTexto1); 
        $pdf->SetXY($posicaoTexto1X, $posicaoTexto1Y);
        $pdf->MultiCell(265, 9, utf8_decode($texto1), '', 'C', 0); 

        // Texto 2
        $pdf->SetFont('Arial', '', $fontSizeTexto2); 
        $pdf->SetXY($posicaoTexto2X, $posicaoTexto2Y); 
        $pdf->MultiCell(165, 9, utf8_decode($texto2), '', 'L', 0);


        $pdf->Output();
    }

    public function gerarAtestadoTurma($certificado_id, $turma_id)
    {
        $checkPermissao = TurmaAluno::plataforma()->where('turma_id', $turma_id)->where('aluno_id', \Auth::id())->exists();
        
        if($checkPermissao == false)
            return back()->with('danger', 'Você não tem permissão para baixar este atestado de matrícula.');


        $certificado = Certificado::plataforma()->find($certificado_id);

        $user_id = \Auth::id();
        $texto1  = $certificado->replaceVariaveis($certificado->texto1, $user_id, $turma_id, null);
        $texto2  = $certificado->replaceVariaveis($certificado->texto2, $user_id, $turma_id, null);

        $title  = $certificado->referencia;

        $posicaoNomeX   = $certificado->nome_posicao_x   ?? 20;
        $posicaoNomeY   = $certificado->nome_posicao_y   ?? 85;
        $fontSizeNome   = $certificado->nome_fontSize    ?? 30;

        $posicaoTexto1X = $certificado->texto1_posicao_x ?? 20;
        $posicaoTexto1Y = $certificado->texto1_posicao_y ?? 117;
        $fontSizeTexto1 = $certificado->texto1_fontSize  ?? 15;

        $posicaoTexto2X = $certificado->texto2_posicao_x ?? 20;
        $posicaoTexto2Y = $certificado->texto2_posicao_y ?? 150;
        $fontSizeTexto2 = $certificado->texto2_fontSize  ?? 15;


        $pdf = new Fpdf();
        $pdf->SetTitle(utf8_decode($title));
        $pdf->AddPage('L');
        $pdf->SetLineWidth(1.5);
        $pdf->Image($certificado->modelo, 0, 0, $certificado->posicao_imagem ?? 300);

        // Nome
        $pdf->SetFont('Arial', '', $fontSizeNome);
        $pdf->SetXY($posicaoNomeX, $posicaoNomeY); 
        $pdf->MultiCell(265, 6, utf8_decode(\Auth::user()->nome), '', 'C', 0); 

        // Texto 1
        $pdf->SetFont('Arial', '', $fontSizeTexto1); 
        $pdf->SetXY($posicaoTexto1X, $posicaoTexto1Y);
        $pdf->MultiCell(265, 9, utf8_decode($texto1), '', 'C', 0); 

        // Texto 2
        $pdf->SetFont('Arial', '', $fontSizeTexto2); 
        $pdf->SetXY($posicaoTexto2X, $posicaoTexto2Y); 
        $pdf->MultiCell(165, 9, utf8_decode($texto2), '', 'L', 0);


        $pdf->Output();
    }

}