<?php

namespace App\Http\Livewire\Admin\Ajuda\Chamado;

use Livewire\WithPagination;
use Livewire\Component;
use App\Models\Chamado;

class Lista extends Component
{
    use WithPagination;

    protected $paginationTheme = 'bootstrap';

    public $status;
    public $arquivado = 'N';
    public $ordem = 'desc';

    protected $queryString = [
        'status',
        'arquivado',
        'ordem',
    ];


    public function render()
    {
        $chamados = Chamado::plataforma();

        if($this->status != null)
        {
            $chamados = $chamados->where('status', $this->status);
        }

        if($this->arquivado)
        {
            $chamados = $chamados->where('arquivado', $this->arquivado);
        }

        $countChamados = $chamados->count();

        $chamados = $chamados->orderBy('id', $this->ordem)->paginate(10);

        return view('livewire.admin.ajuda.chamado.lista', [
            'chamados'      => $chamados,
            'countChamados' => $countChamados,
        ]);
    }
}
