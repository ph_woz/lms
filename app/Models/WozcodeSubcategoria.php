<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WozcodeSubcategoria extends Model
{
	public $timestamps = false;

	protected $table = 'wozcode_subcategorias';

	protected $fillable = [
		'categoria_id',
		'nome',
		'slug',
	];

}
