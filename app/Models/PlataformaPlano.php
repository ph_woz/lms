<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PlataformaPlano extends Model
{
    use HasFactory;

    protected $table = 'plataformas_planos';

	public $timestamps = false;

    public $fillable = [
        'plataforma_id',
        'plano',
        'max_qnt_alunos',
    	'valor',
    ];
    
    public function scopePlataforma($query)
    {
        return $query->where('plataforma_id', session('plataforma_id') ?? Plataforma::dominio()->pluck('id')[0] ?? null);
    }
}
