<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Models\Produto;
use App\Models\ProdutoFoto;
use App\Models\ProdutoCategoria;
use App\Models\Unidade;
use App\Models\Categoria;
use App\Models\FormaPagamento;

class ProdutoController extends Controller
{
    public function lista()
    {
        return view('admin.produto.lista');
    }

    public function info($produto_id)
    {
        return view('admin.produto.info', compact('produto_id') );
    }

    public function add()
    {
        if(request()->has('sent'))
        {
            $produto = Produto::create(request()->all());

            $this->syncCategorias($produto->id);
            $this->syncFormasPagamento($produto->id);

            return redirect()->route('admin.produto.info', ['id' => $produto->id])->with('success', 'Produto cadastrado com êxito.')->with('produto_id', $produto->id)->with('produto_nome', $produto->nome);
        }

        $unidades   = Unidade::select('id','nome')->plataforma()->ativo()->orderBy('nome')->get();
        $categorias = Categoria::select('id','nome')->plataforma()->ativo()->tipo('Produtos')->orderBy('nome')->get();

        return view('admin.produto.add', compact('unidades','categorias') );
    }

    public function editar($id)
    {
        $produto = Produto::plataforma()->find($id);

        if(is_null($produto))
        {
            return view('admin.page-whoops', ['erro' => 'Produto não encontrado.']);
        }

        if(request()->has('sent'))
        {
            $produto->update(request()->all());

            $this->syncCategorias($produto->id);
            $this->syncFormasPagamento($produto->id);

            return back()->with('success', 'Produto atualizado com êxito.');
        }


        $unidades   = Unidade::select('id','nome')->plataforma()->ativo()->orderBy('nome')->get();
        $categorias = Categoria::select('id','nome')->plataforma()->ativo()->tipo('Produtos')->orderBy('nome')->get();
        $categoriasSelected = ProdutoCategoria::select('categoria_id')->plataforma()->where('produto_id', $produto->id)->get()->pluck('categoria_id')->toArray();
        $formasPagamento = FormaPagamento::plataforma()->produto()->where('registro_id', $produto->id)->get();

        return view('admin.produto.editar', compact('produto','unidades','categorias','formasPagamento','categoriasSelected') );
    }

    public function syncFormasPagamento($produto_id)
    {
        FormaPagamento::plataforma()->produto()->where('registro_id', $produto_id)->delete();

        $formaCartaoDeCredito = array_combine(request('cartao-de-credito')['parcelas'], request('cartao-de-credito')['valores']);
        foreach($formaCartaoDeCredito as $parcela => $valor)
        {
            if($parcela && $valor)
            {
                FormaPagamento::create([
                    'registro_id'   => $produto_id,
                    'tipo_cadastro' => 'Produto',
                    'tipo'          => 'CC',
                    'parcela'       => $parcela,
                    'valor'         => $valor,
                ]);
            }
        }

        $formaCartaoDeDebito = array_combine(request('cartao-de-debito')['parcelas'], request('cartao-de-debito')['valores']);
        foreach($formaCartaoDeDebito as $parcela => $valor)
        {
            if($parcela && $valor)
            {
                FormaPagamento::create([
                    'registro_id'   => $produto_id,
                    'tipo_cadastro' => 'Produto',
                    'tipo'          => 'CD',
                    'parcela'       => $parcela,
                    'valor'         => $valor,
                ]);
            }
        }

        $formaBoleto = array_combine(request('boleto')['parcelas'], request('boleto')['valores']);
        foreach($formaBoleto as $parcela => $valor)
        {
            if($parcela && $valor)
            {
                FormaPagamento::create([
                    'registro_id'   => $produto_id,
                    'tipo_cadastro' => 'Produto',
                    'trilha_id'     => $trilha_id,
                    'tipo'          => 'BO',
                    'parcela'       => $parcela,
                    'valor'         => $valor,
                ]);
            }
        }
    }

    public function syncFotos($produto_id)
    {
        $fotosToDeleteIds = explode(',', trim(request('delete_fotos_ids'), ','));
        $fotosToDelete = ProdutoFoto::plataforma()->whereIn('id', $fotosToDeleteIds)->get();

        foreach($fotosToDelete as $fotoToDelete)
        {
            $path = str_replace(\App\Models\Util::getLinkStorage(), '', $fotoToDelete->foto);
            Storage::delete($path);

            $fotoToDelete->delete();
        }

        $i = 0;
        foreach(request('fotos') ?? array() as $foto)
        {
            $i++;

            $diretorio = 'plataformas/'.\Auth::user()->plataforma_id.'/produtos/'.$produto_id;

            $filename = $i.'.'.$foto->getClientOriginalExtension();

            $path = Storage::putFileAs($diretorio, $foto, $filename, 'public');
            $linkStorage = \App\Models\Util::getLinkStorage();
            $caminho = $linkStorage . $path;

            ProdutoFoto::create([
                'produto_id' => $produto_id, 
                'foto' => $caminho,
            ]);
        }


        // $allFotos = ProdutoFoto::select('id')->plataforma()->where('produto_id', $produto_id)->orderBy('ordem')->get()->pluck('id')->toArray();
        // $fotosToSortIds = explode(',', trim(request('ordem'), ','));

        // $i = 0;

        // foreach($fotosToSortIds as $ordem)
        // {
        //     $i++;

        //     ProdutoFoto::plataforma()->where('produto_id', $produto_id)->where('id', $i)->orderBy('ordem')->update(['ordem' => $ordem]);
        // }


        $fotosToSortIds = explode(',', trim(request('ordem'), ','));
        
        foreach(request('fotosIds') as $foto_id)
        {
            // dd($foto_id);
        }

        // dd($fotosOrdens);


    }

    public function syncCategorias($produto_id)
    {
        ProdutoCategoria::plataforma()->where('produto_id', $produto_id)->delete();

        foreach(request('categorias') ?? array() as $categoria_id)
        {
            ProdutoCategoria::create([
                'produto_id'    => $produto_id, 
                'categoria_id'  => $categoria_id,
            ]);
        }
    }
}