@extends('layouts.admin.master')

@section('css')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.14.0-beta2/dist/css/bootstrap-select.min.css">
<style>

    .calendar-icon-white::-webkit-calendar-picker-indicator {
      filter: invert(1);
    }

    .bg-sell-green { background: #2aa385; }

    .btnNavTab {
        text-align: center;
        width: 100%;
        display: flex; 
        align-items: center;
        justify-content: center;
        height: 50px;
        color: #666;
        font-family: 'Nunito', sans-serif;
        font-weight: 600;
        background: #FFF;
    }


    .btnNavTab:hover { 
        text-decoration: none; 
        background: #222e3c; 
        color: #F9F9F9;
    }

    .lineTab { height: 45px; border:1px solid #eee; }

    .btnNavTab[aria-expanded="true"] { background: #222e3c; }

    #sidebar ul li.active>a,
    .btnNavTab[aria-expanded="true"] {
        color: #fff;
    }

    .btnNavTab[data-bs-toggle="collapse"] {
        position: relative;
    }

    #n_mes_12 { margin-right: 0px!important; }
    #n_mes_{{ $_GET['mes'] ?? date('m') }} .btn-add { background-color: #3b7ddd!important; }
    #n_ano_{{ $_GET['ano'] ?? date('Y') }} .btn-add { background-color: #3b7ddd!important; }
    
    .h-50px { height: 50px; }

    .bg-estorno { background: #c45210!important; }
    .bg-inadimplente { background: #c45210; }
    .text-inadimplente { color: #c45210; }

</style>
@endsection

@section('content')

<div>

    <div class="d-flex align-items-center p-3 bg-white box-shadow rounded-1" style="border: 2px solid #dee2e6!important;">
        <a href="{{ route('admin.aluno.lista', ['add' => 'S', 'add_aluno_para_fluxo_caixa' => 'S']) }}" class="w-100 text-center btn-add py-2-4 fs-15 font-nunito weight-600 border-0">
            + Adicionar Entrada
        </a>

        <div class="mx-3" style="height: 45px; width:4.3px;background: #dee2e6;"></div>

        <a href="#" data-bs-target="#ModalAddSaida" data-bs-toggle="modal" class="w-100 text-center btn-add py-2-4 fs-15 font-nunito weight-600 border-0">
            - Adicionar Saída
        </a>
    </div>

    <div>
        <form class="py-3 mb-3 border-top border-bottom border-muted my-3">

            <input type="hidden" name="ano" value="{{ $_GET['ano'] ?? date('Y') }}">
            <input type="hidden" name="mes" value="{{ $_GET['mes'] ?? date('m') }}">

            <div class="mt-4 d-xl-flex justify-content-between">

                <div class="d-inline-block mb-3 w-100 text-center me-xl-1" id="n_ano_{{ $_GET['ano'] ?? date('Y') }}">
                    <div class="dropdown">
                        <button class="btn-add rounded w-100 fs-11 px-2" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                            {{ $ano ?? date('Y') }}&nbsp;
                            <i class="fas fa-caret-down"></i>
                        </button>
                        <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                            @foreach($anos as $ano)
                                <li><a class="dropdown-item" href="?mes={{$_GET['mes'] ?? date('m')}}&ano={{ $ano }}">{{ $ano }}</a></li>
                            @endforeach
                        </ul>
                    </div>
                </div>

                @foreach($meses as $numero => $nome)
                    <div class="d-inline-block mb-3 w-100 text-center me-xl-1" id="n_mes_{{$numero}}">
                        <a href="?mes={{ $numero }}&ano={{ $_GET['ano'] ?? date('Y') }}" class="d-block w-100 btn-add fs-11 px-2 rounded">
                            {{ $nome }}
                        </a>
                    </div>
                @endforeach

            </div>

            <div class="mb-3 d-lg-flex align-items-center">

                <input type="search" name="buscar" onchange="this.form.submit()" id="buscar" class="me-lg-3 mb-3 mb-lg-0 form-control p-form" placeholder="Buscar" value="{{ $_GET['buscar'] ?? null }}">

                @if(\App\Models\Unidade::checkActive() === true && count($unidades) > 0)
                <select name="unidade_id" id="unidade_id" onchange="this.form.submit()" class="me-lg-3 mb-3 mb-lg-0 selectpicker" data-width="100%" data-live-search="true">
                    <option value="">Unidades</option>
                    @foreach($unidades as $unidade)
                        <option value="{{ $unidade->id }}">{{ $unidade->nome }}</option>
                    @endforeach
                </select>
                @endif

                <select name="vendedor_id" id="vendedor_id" onchange="this.form.submit()" class="selectpicker me-lg-3 mb-3 mb-lg-0" data-width="100%" data-live-search="true">
                    <option value="">Vendedores</option>
                    @foreach($vendedores as $vendedor)
                        <option value="{{ $vendedor->id }}">{{ $vendedor->nome }}</option>
                    @endforeach
                </select>  

                <select name="categoria_id" id="categoria_id" onchange="this.form.submit()" class="selectpicker me-lg-3 mb-3 mb-lg-0" data-width="100%" data-live-search="true">
                    <option value="">Categorias</option>
                    @foreach($categorias as $categoria)
                        <option value="{{ $categoria->id }}">{{ $categoria->nome }}</option>
                    @endforeach
                </select>

                <select name="produto_id" id="produto_id" onchange="this.form.submit()" class="me-lg-3 mb-3 mb-lg-0 selectpicker" data-width="100%" data-live-search="true" required>
                    <option value="">Produtos</option>
                    <option value="">Selecione</option>
                    <optgroup label="Trilhas">
                        @foreach($trilhas as $trilha)
                            <option value="T-{{ $trilha->id }}" data-valor="{{ $trilha->valor_a_vista }}">
                                {{ $trilha->referencia }}
                            </option>
                        @endforeach
                    </optgroup>
                    <optgroup label="Turmas">
                        @foreach($turmas as $turma)
                            <option value="C-{{ $turma->id }}" data-valor="{{ $turma->valor_a_vista }}">
                                {{ $turma->nome }}
                            </option>
                        @endforeach
                    </optgroup>
                </select>

                <select name="status" id="status" onchange="this.form.submit();" class="select-form w-lg-60">
                    <option value="">Status de Pagamento</option>
                    <option value="a-receber">A receber</option>
                    <option value="recebidos-e-pagos">Recebidos/Pagos</option>
                    <option value="vencidos">Vencidos</option>
                    <option value="estornados">Estornados</option>
                </select>

            </div>

        </form>
    </div>

    <div class="row mb-lg-4">

        <div class="col-lg-4">
            <div class="card m-0">
                <div class="card-header text-white bg-sell-green">
                    <div class="d-flex justify-content-between align-items-center">
                        <span class="d-block fs-25 weight-500">
                            R$ {{ \App\Models\FluxoCaixa::getValorRealDisponivel(\App\Models\FluxoCaixa::getValorRealInArray($entradas->pluck('valor_parcela')->toArray()), \App\Models\FluxoCaixa::getValorRealInArray($saidas->pluck('valor_parcela')->toArray())) }}
                        </span>
                        <span class="fa fa-sign-out-alt fs-25 text-white-50"></span>
                    </div>
                    <span class="d-block font-nunito fs-16 weight-600">Previsto total de Disponível</span>
                </div>
            </div>
        </div>

        <div class="col-lg-4">
            <div class="card m-0">
                <div class="card-header text-white bg-info">
                    <div class="d-flex justify-content-between align-items-center">
                        <span class="d-block fs-25 weight-500">
                            R$ {{ \App\Models\FluxoCaixa::getValorRealInArray($entradas->pluck('valor_parcela')->toArray()) }}
                        </span>
                        <span class="fa fa-money-check-alt fs-25 text-white-50"></span>
                    </div>
                    <span class="d-block font-nunito fs-16 weight-600">Previsto total de Faturamento</span>
                </div>
            </div>
        </div>

        <div class="col-lg-4">
            <div class="card m-0">
                <div class="card-header text-white bg-danger">
                    <div class="d-flex justify-content-between align-items-center">
                        <span class="d-block fs-25 weight-500">
                            R$ {{ \App\Models\FluxoCaixa::getValorRealInArray($saidas->pluck('valor_parcela')->toArray()) }}
                        </span>
                        <span class="fa fa-sign-out-alt fs-25 text-white-50"></span>
                    </div>
                    <span class="d-block font-nunito fs-16 weight-600">Previsto total de Pagamentos</span>
                </div>
            </div>
        </div>
        
    </div>

    <div class="row mb-lg-4">

        <div class="col-lg-4">
            <div class="card m-0">
                <div class="card-header text-white bg-sell-green">
                    <div class="d-flex justify-content-between align-items-center">
                        <span class="d-block fs-25 weight-500">
                            R$ {{ \App\Models\FluxoCaixa::getValorRealDisponivel(\App\Models\FluxoCaixa::getValorRealInArray($entradas->where('status', 1)->pluck('valor_parcela')->toArray()), \App\Models\FluxoCaixa::getValorRealInArray($saidas->where('status', 1)->pluck('valor_parcela')->toArray())) }}
                        </span>
                        <span class="fa fa-money-check-alt fs-25 text-white-50"></span>
                    </div>
                    <span class="d-block font-nunito fs-16 weight-600">Disponível atual</span>
                </div>
            </div>
        </div>


        <div class="col-lg-4">
            <div class="card m-0">
                <div class="card-header text-white bg-info">
                    <div class="d-flex justify-content-between align-items-center">
                        <span class="d-block fs-25 weight-500">
                            R$ {{ \App\Models\FluxoCaixa::getValorRealInArray($entradas->where('status', 1)->pluck('valor_parcela')->toArray()) }}
                        </span>
                        <span class="fa fa-money-check-alt fs-25 text-white-50"></span>
                    </div>
                    <span class="d-block font-nunito fs-16 weight-600">Faturamento atual</span>
                </div>
            </div>
        </div>

        <div class="col-lg-4">
            <div class="card m-0">
                <div class="card-header text-white bg-danger">
                    <div class="d-flex justify-content-between align-items-center">
                        <span class="d-block fs-25 weight-500">
                            R$ {{ \App\Models\FluxoCaixa::getValorRealInArray($saidas->where('status', 1)->pluck('valor_parcela')->toArray()) }}
                        </span>
                        <span class="fa fa-sign-out-alt fs-25 text-white-50"></span>
                    </div>
                    <span class="d-block font-nunito fs-16 weight-600">Despesas já pagas</span>
                </div>
            </div>
        </div>

    </div>

    <div class="row mb-4-5">

        <div class="col-lg-4">
            <div class="card m-0">
                <div class="card-header text-white bg-danger">
                    <div class="d-flex justify-content-between align-items-center">
                        <span class="d-block fs-25 weight-500">
                            R$ {{ \App\Models\FluxoCaixa::getValorRealInArray($vencidos) }}
                        </span>
                        <span class="fa fa-money-check-alt fs-25 text-white-50"></span>
                    </div>
                    <span class="d-block font-nunito fs-16 weight-600">
                        Vencidos <span class="fs-15">({{ count($vencidos) }} clientes)</span>
                    </span>
                </div>
            </div>
        </div>

        <div class="col-lg-4">
            <div class="card m-0">
                <div class="card-header text-white bg-info">
                    <div class="d-flex justify-content-between align-items-center">
                        <span class="d-block fs-25 weight-500">
                            R$ {{ \App\Models\FluxoCaixa::getValorRealInArray($entradas->where('status', 0)->pluck('valor_parcela')->toArray()) }}
                        </span>
                        <span class="fa fa-money-check-alt fs-25 text-white-50"></span>
                    </div>
                    <span class="d-block font-nunito fs-16 weight-600">Pendentes a receber</span>
                </div>
            </div>
        </div>

        <div class="col-lg-4">
            <div class="card m-0">
                <div class="card-header text-white bg-danger">
                    <div class="d-flex justify-content-between align-items-center">
                        <span class="d-block fs-25 weight-500">
                            R$ {{ \App\Models\FluxoCaixa::getValorRealInArray($saidas->where('status', 0)->pluck('valor_parcela')->toArray()) }}
                        </span>
                        <span class="fa fa-sign-out-alt fs-25 text-white-50"></span>
                    </div>
                    <span class="d-block font-nunito fs-16 weight-600">
                        Pendentes a pagar
                        @if($countDespesasEmAtraso > 0)
                            ({{ $countDespesasEmAtraso }} em atraso)
                        @endif
                    </span>
                </div>
            </div>
        </div>

    </div>

    <div class="border rounded-1 h-50px d-flex align-items-center">

        <a href="#tabEntradas" class="btnNavTab btnTabEntrada" data-bs-toggle="collapse" aria-expanded="true">
            Entrada ({{ count($entradas) }})
        </a>
        
        <div class="lineTab"></div>
        
        <a href="#tabSaidas" class="btnNavTab btnTabSaida" data-bs-toggle="collapse" aria-expanded="false">
            Saída ({{ count($saidas) }})
        </a>

    </div>

    <div class="bg-white border border border-bottom-0">
        <div class="table-responsive collapse show" id="tabEntradas">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th scope="col">Cliente</th>
                  <th scope="col">Produto</th>
                  <th scope="col">Valor</th>
                  <th scope="col">Status</th>
                  <th scope="col">Data de Vencimento</th>
                  <th scope="col">
                    <span class="float-end">
                        Ações
                    </span>
                  </th>
                </tr>
              </thead>
              <tbody>
                @foreach($entradas as $entrada)
                <tr>
                    <td>
                        {{ \App\Models\Util::getPrimeiroSegundoNome($entrada->cliente_nome) }}
                    </td>
                    <td>
                        {{ $entrada->produto_nome ?? 'Não definido' }}
                    </td>
                    <td>
                        @if($entrada->n_parcelas != $entrada->n_parcela || $entrada->registro_id != null)
                            {{ $entrada->n_parcela }}/{{$entrada->n_parcelas}} de
                        @endif
                        R$ {{ $entrada->valor_parcela }}
                    </td>
                    <td>
                        @switch($entrada->status)
                            @case(0)
                                @if(date('Y-m-d') > $entrada->data_vencimento)
                                    <span class="py-1-5 px-2 text-light rounded bg-danger">
                                        Em atraso
                                    </span>
                                @endif
                                @if(date('Y-m-d') <= $entrada->data_vencimento)
                                    <span class="py-1-5 px-2 text-light rounded bg-primary">
                                        A receber
                                    </span>
                                @endif
                            @break
                            @case(1)
                                <span class="py-1-5 px-2 text-light rounded bg-success">
                                    Recebido
                                </span>
                            @break
                            @case(2)
                                <span class="py-1-5 px-2 text-light rounded bg-inadimplente">
                                    Estornado
                                </span>
                            @break
                        @endswitch
                    </td>
                    <td>
                        {{ date('d/m/Y', strtotime($entrada->data_vencimento)) }}
                    </td>
                    <td>
                        <span class="float-end">
                            <button type="button" data-bs-target="#ModalDadosEAcoesEntrada" onclick="getDadosEntradaById('{{ $entrada->id }}')" data-bs-toggle="modal" class="btn-add py-2 px-2-5">
                                <i class="fa fa-eye"></i>
                            </button>
                        </span>
                    </td>
                </tr>
                @endforeach
              </tbody>
            </table>
        </div>
        <div class="table-responsive collapse" id="tabSaidas">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th scope="col">Referência</th>
                  <th scope="col">Valor</th>
                  <th scope="col">Status</th>
                  <th scope="col">Data de Vencimento</th>
                  <th scope="col">
                    <span class="float-end">
                        Ações
                    </span>
                  </th>
                </tr>
              </thead>
              <tbody>
                @foreach($saidas as $saida)
                <tr>
                    <td>
                        {{ $saida->referencia }}
                    </td>
                    <td>
                        @if($saida->n_parcelas != $saida->n_parcela || $saida->registro_id != null)
                            {{ $saida->n_parcela }}/{{$saida->n_parcelas}} de
                        @endif
                        R$ {{ $saida->valor_parcela }}
                    </td>
                    <td>
                        @switch($saida->status)
                            @case(0)
                                @if(date('Y-m-d') > $saida->data_vencimento)
                                    <span class="py-1-5 px-2 text-light rounded bg-danger">
                                        Em atraso
                                    </span>
                                @endif
                                @if(date('Y-m-d') <= $saida->data_vencimento)
                                    <span class="py-1-5 px-2 text-light rounded bg-secondary">
                                        A pagar
                                    </span>
                                @endif
                            @break
                            @case(1)
                                <span class="py-1-5 px-2 text-light rounded bg-success">
                                    Pago
                                </span>
                            @break
                        @endswitch
                    </td>
                    <td>
                        {{ date('d/m/Y', strtotime($saida->data_vencimento)) }}
                    </td>
                    <td>
                        <span class="float-end">
                            <button type="button" data-bs-target="#ModalDadosEAcoesSaida" onclick="getDadosSaidaById('{{ $saida->id }}')" data-bs-toggle="modal" class="btn-add py-2 px-2-5">
                                <i class="fa fa-eye"></i>
                            </button>
                        </span>
                    </td>
                </tr>
                @endforeach
              </tbody>
            </table>
        </div>
    </div>

    <form action="{{ route('admin.controle-financeiro.lista.fluxo-caixa.add-entrada') }}" method="POST">
        @csrf

        <div class="modal fade" id="ModalAddEntrada" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3 class="modal-title" id="exampleModalLabel">+ Adicionar Entrada</h3>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">

                        <input type="hidden" name="add_entrada_produto_nome"  id="add_entrada_produto_nome">
                        <input type="hidden" name="add_entrada_cliente_nome"  id="add_entrada_cliente_nome" value="{{ $add_entrada_cliente_nome }}">
                        <input type="hidden" name="add_entrada_cliente_id"    id="add_entrada_cliente_id" value="{{ $cliente_id }}">
                        <input type="hidden" name="add_entrada_unidade_nome"  id="add_entrada_unidade_nome">
                        <input type="hidden" name="add_entrada_vendedor_nome" id="add_entrada_vendedor_nome" value="{{ \Auth::user()->nome }}">

                        <div class="w-100 mb-3">
                            <label>Cliente</label>
                            <a href="{{ route('admin.aluno.info', ['id' => $cliente_id ?? 0]) }}" target="_blank" class="hover-d-none">
                                <input type="text" name="referencia" class="form-control p-form cursor-pointer bg-secondary border-0 text-light" disabled value="{{ $clienteSelectedLabel }}">
                            </a>
                        </div>

                        <hr/>

                        <div class="d-lg-flex mb-3">

                            <div class="w-100 me-lg-3 mb-3 mb-lg-0">
                                <label>Vendedor</label>
                                <select name="add_entrada_vendedor_id" id="add_entrada_vendedor_id" onchange="setAddEntradaVendedorNome(this)" class="selectpicker" data-width="100%" data-live-search="true" required>
                                    <option value="">Selecione</option>
                                    @foreach($vendedores as $vendedor)
                                        <option value="{{ $vendedor->id }}" @if(\Auth::id() == $vendedor->id) selected @endif>
                                            {{ \App\Models\Util::getPrimeiroSegundoNome($vendedor->nome) }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>

                            <!--
                            <div class="w-100 me-lg-3 mb-3 mb-lg-0">
                                <label>Cliente</label>
                                <select name="add_entrada_cliente_id" id="add_entrada_cliente_id" onchange="setAddEntradaClienteNome(this)" class="selectpicker" data-width="100%" data-live-search="true" required>
                                    <option value="">Selecione</option>
                                    @foreach($clientes as $cliente)
                                        <option value="{{ $cliente->id }}">
                                            {{ $cliente->nome }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                            -->

                            <div class="w-100">
                                <label>Produto</label>
                                <select name="add_entrada_produto_id" onchange="setAddEntradaProdutoNomeAndValorAndReferencia(this)" class="selectpicker" data-width="100%" data-live-search="true" required>
                                    <option value="">Selecione</option>

                                    @if(\App\Models\Trilha::checkActive() === true && count($trilhas) > 0)
                                    <optgroup label="Trilhas">
                                        @foreach($trilhas as $trilha)
                                            <option value="T-{{ $trilha->id }}" data-valor="{{ $trilha->valor_a_vista }}">
                                                {{ $trilha->referencia }}
                                            </option>
                                        @endforeach
                                    </optgroup>
                                    @endif

                                    @if(\App\Models\Turma::checkActive() === true && count($turmas) > 0)
                                    <optgroup label="Turmas">
                                        @foreach($turmas as $turma)
                                            <option value="C-{{ $turma->id }}" data-valor="{{ $turma->valor_a_vista }}">
                                                {{ $turma->nome }}
                                            </option>
                                        @endforeach
                                    </optgroup>
                                    @endif

                                    @if(count($planos) > 0)
                                    <optgroup label="Planos">
                                        @foreach($planos as $plano)
                                            <option value="A-{{ $plano->id }}" data-valor="{{ $plano->valor_promocional ?? $plano->valor }}">
                                                {{ $plano->nome }}
                                            </option>
                                        @endforeach
                                    </optgroup>
                                    @endif

                                    @if(count($servicos) > 0)
                                    <optgroup label="Serviços">
                                        @foreach($servicos as $servico)
                                            <option value="S-{{ $servico->id }}" data-valor="{{ $servico->valor }}">
                                                {{ $servico->nome }}
                                            </option>
                                        @endforeach
                                    </optgroup>
                                    @endif

                                    @if(count($produtos) > 0)
                                    <optgroup label="Produtos">
                                        @foreach($produtos as $produto)
                                            <option value="P-{{ $produto->id }}" data-valor="{{ $produto->valor_promocional ?? $produto->valor }}">
                                                {{ $produto->nome }}
                                            </option>
                                        @endforeach
                                    </optgroup>
                                    @endif
                                    
                                </select>
                                

                            </div>

                            @if(\App\Models\Unidade::checkActive() === true && count($unidades) > 0)
                            <div class="w-100 ms-lg-3 mt-3 mt-lg-0">
                                <label>Unidade</label>
                                <select name="add_entrada_unidade_id" id="add_entrada_unidade_id"  onchange="setAddEntradaUnidadeNome(this)" class="selectpicker" data-width="100%" data-live-search="true">
                                    
                                    @if(is_null($unidade_id))
                                        <option value="">Selecione</option>
                                    @endif

                                    @foreach($unidades as $unidade)
                                        <option value="{{ $unidade->id }}" @if(isset($unidade_id) && $unidade_id == $unidade->id) selected @endif>{{ $unidade->nome }}</option>
                                    @endforeach
                                </select>
                            </div>
                            @endif

                        </div>

                        <div class="d-lg-flex mb-3">

                            <div class="w-lg-50 me-lg-3 mb-3 mb-lg-0">
                                <label>Data de Vencimento</label>
                                <input type="date" name="add_entrada_data_vencimento" id="add_entrada_data_vencimento" class="form-control p-form" required>
                            </div>

                            <div class="w-lg-50 me-lg-3 mb-3 mb-lg-0">
                                <label>Valor</label>
                                <input type="text" name="add_entrada_valor" id="add_entrada_valor" class="mask_valor form-control p-form" placeholder="R$" required>
                            </div>

                            <div class="w-lg-50 me-lg-3 mb-3 mb-lg-0">
                                <label>Forma de Pagamento</label>
                                <select name="add_entrada_forma_pagamento" id="add_entrada_forma_pagamento" class="select-form">
                                    <option value="">Selecione</option>
                                    <option value="Dinheiro">Dinheiro</option>
                                    <option value="Cartão de débito">Cartão de débito</option>
                                    <option value="Cartão de crédito">Cartão de crédito</option>
                                    <option value="Boleto bancário">Boleto bancário</option>
                                    <option value="Pix">Pix</option>
                                    <option value="TED">TED</option>
                                    <option value="DOC">DOC</option>
                                </select>
                            </div>

                            <div class="w-lg-30">
                                <label>Parcelas</label>
                                <input type="text" id="add_entrada_n_parcelas" name="add_entrada_n_parcelas" onchange="gerarParcelasAddEntrada(this.value)" class="number form-control p-form" placeholder="N°" required value="1">
                            </div>

                        </div>

                        <div class="mb-3 w-100">
                            <label>Link de Pagamento</label>
                            <input type="text" name="link" class="form-control p-form" placeholder="Link">
                        </div>

                        <div id="boxParcelasAddEntrada" style="display:none;" class="mb-3">

                            <label class="mb-0 d-block">Parcelas</label>
                            <hr class="mt-2">

                            <div class="mb-2 d-flex align-items-center parcelas">

                                <div class="me-2 border bg-light rounded px-3 text-center flex-center w-lg-20" readonly style="height: 42.5px;">
                                    N°
                                </div>

                                <div class="me-2 border bg-light rounded px-3 d-flex align-items-center w-100" readonly style="height: 42.5px;">
                                    Data de Vencimento
                                </div>

                                <div class="me-2 border bg-light rounded px-3 d-flex align-items-center w-100" readonly style="height: 42.5px;">
                                    Valor
                                </div>

                                <div class="me-2 border bg-light rounded px-3 d-flex align-items-center w-100" readonly style="height: 42.5px;">
                                    Link de Pagamento
                                </div>

                                <div class="border bg-light rounded px-3 d-flex align-items-center w-100" readonly style="height: 42.5px;">
                                    Forma de Pagamento
                                </div>

                            </div>
                            
                            <div id="appendParcelaAddEntrada" class="w-100">
                                

                            </div>

                        </div>

                        <div class="mb-3">
                            <label>Observações internas</label>
                            <textarea name="add_entrada_obs" class="textarea-form" placeholder="Descreva observações..."></textarea>
                        </div>

                        <div class="w-100">
                            <label>Categorias</label>
                            <select name="categoriasSelected[]" class="selectpicker" multiple data-width="100%" data-live-search="true" title="Selecione">
                                <option value="">Selecione</option>
                                @foreach($categorias as $categoria)
                                    <option value="{{ $categoria->id }}">
                                        {{ $categoria->nome }}
                                    </option>
                                @endforeach
                            </select>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary weight-600 py-2">Confirmar</button>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <form class="w-100" action="{{ route('admin.controle-financeiro.lista.editar-setstatus') }}">
    @csrf
        <div class="modal fade" id="ModalDadosEAcoesEntrada" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content" style="border-radius: 10px;">
                    <div class="modal-header w-100">
                        <div class="w-100 d-flex justify-content-between align-items-center">
                            <h3 class="modal-title" id="exampleModalLabel">Dados de Entrada</h3>
                                <div class="me-1" id="boxLoadingEntrada">
                                    <div class="spinner-grow text-primary" role="status">
                                      <span class="visually-hidden">Loading...</span>
                                    </div>
                                </div>
                            <div>
                                <span class="weight-600" id="entrada_status">

                                </span>
                                <a href="" target="_blank" id="btn-conversar" class="py-2 px-3 hover-d-none rounded bg-sell-green text-light weight-600 text-shadow-1">
                                    <i class="fab fa-whatsapp"></i>
                                    Conversar
                                </a>
                            </div>
                            <div data-bs-toggle="tooltip" data-bs-placement="top" data-bs-title="Deletar" class="me-3">
                                <a href="#ModalDeletarEntrada" onclick="setCamposToDeleteEntrada()" data-bs-toggle="modal" data-bs-dismiss="modal" class="text-danger fs-17" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-title="Deletar">
                                    <i class="fas fa-trash-alt"></i>
                                </a>
                            </div>
                        </div>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-title="Fechar" aria-label="Close"></button>
                    </div>
                    <div class="modal-body" style="border:5px solid #eee;">

                        <input type="hidden" name="entrada_id" id="dados_e_acoes_entrada_id">
                        <input type="hidden" id="entrada_cliente_id">

                        <ul class="list-group list-group-flush">
                            <li class="list-group-item d-flex align-items-center justify-content-between pe-0">
                                <span class="weight-600 color-444 nameProduto">Data de Vencimento:</span>
                                <div class="d-flex align-items-center">
                                    <span class="weight-600" id="entrada_data_vencimento">

                                    </span>
                                    <input type="date" id="input_editar_entrada_data_vencimento" class="form-control" style="display:none;">
                                    <a href="#" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-title="Editar" class="ms-3 text-primary" id="btn_editar_entrada_datavencimento">
                                        <i class="fa fa-edit fs-15"></i>
                                    </a>
                                    <a href="#" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-title="Salvar" class="btnSaveEditEntrada text-success mx-2" data-field="data_vencimento" style="display:none;">
                                        <i class="fas fa-check-circle fs-17"></i>
                                    </a>
                                    <a href="#" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-title="Cancelar" class="btnResetEditEntrada text-secondary" style="display:none;">
                                        <i class="fas fa-minus-circle fs-17"></i>
                                    </a>
                                </div>
                            </li>
                            <li class="list-group-item d-flex align-items-center justify-content-between pe-0" id="boxEntradaDataRecebimento">
                                <span class="weight-600 color-444 nameProduto">Data de Recebimento:</span>
                                <div class="d-flex align-items-center">
                                    <span class="weight-600" id="entrada_data_recebimento">

                                    </span>
                                    <input type="date" id="input_editar_entrada_data_recebimento" class="form-control" style="display:none;">
                                    <a href="#" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-title="Editar" class="ms-3 text-primary" id="btn_editar_entrada_datarecebimento">
                                        <i class="fa fa-edit fs-15"></i>
                                    </a>
                                    <a href="#" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-title="Salvar" class="btnSaveEditEntrada text-success mx-2" data-field="data_recebimento" style="display:none;">
                                        <i class="fas fa-check-circle fs-17"></i>
                                    </a>
                                    <a href="#" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-title="Cancelar" class="btnResetEditEntrada text-secondary" style="display:none;">
                                        <i class="fas fa-minus-circle fs-17"></i>
                                    </a>
                                </div>
                            </li>
                            <li class="list-group-item d-flex align-items-center justify-content-between pe-0" id="boxEntradaDataEstorno">
                                <span class="weight-600 color-444 nameProduto">Data de Estorno:</span>
                                <div class="d-flex align-items-center">
                                    <span class="weight-600" id="entrada_data_estorno">

                                    </span>
                                    <input type="date" id="input_editar_entrada_data_estorno" class="form-control" style="display:none;">
                                    <a href="#" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-title="Editar" class="ms-3 text-primary" id="btn_editar_entrada_dataestorno">
                                        <i class="fa fa-edit fs-15"></i>
                                    </a>
                                    <a href="#" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-title="Salvar" class="btnSaveEditEntrada text-success mx-2" data-field="data_estorno" style="display:none;">
                                        <i class="fas fa-check-circle fs-17"></i>
                                    </a>
                                    <a href="#" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-title="Cancelar" class="btnResetEditEntrada text-secondary" style="display:none;">
                                        <i class="fas fa-minus-circle fs-17"></i>
                                    </a>
                                </div>
                            </li>
                            <li class="list-group-item d-flex align-items-center justify-content-between pe-0" id="boxEntradaMotivoEstorno">
                                <span class="weight-600 color-444">Motivo de Estorno:</span>
                                <div class="d-flex justify-content-between align-items-center">
                                    <span class="w-100 weight-600 value-list" id="entrada_motivo_estorno">
                                    </span>
                                    <textarea id="input_editar_entrada_motivo_estorno" class="ms-3 textarea-form h-min-50" placeholder="Motivo do Estorno" style="display:none;"></textarea>
                                    <a href="#" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-title="Editar" class="ms-3 text-primary" id="btn_editar_entrada_motivo_estorno">
                                        <i class="fa fa-edit fs-15"></i>
                                    </a>
                                    <a href="#" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-title="Salvar" class="btnSaveEditEntrada text-success mx-2" data-field="motivo_estorno" style="display:none;">
                                        <i class="fas fa-check-circle fs-17"></i>
                                    </a>
                                    <a href="#" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-title="Cancelar" class="btnResetEditEntrada btnEntradaResetEditMotivoEstorno text-secondary" style="display:none;">
                                        <i class="fas fa-minus-circle fs-17"></i>
                                    </a>
                                </div>
                            </li>
                            <li class="list-group-item d-flex justify-content-between pe-0">
                                <span class="weight-600 color-444 nameProduto">Cliente:</span>
                                <div>
                                    <a href="#" class="weight-600 color-blue-info-link" id="entrada_cliente_nome">
                                        
                                    </a>
                                    <a href="" class="ms-3 text-primary invisible">
                                        <i class="fa fa-edit fs-15"></i>
                                    </a>
                                </div>
                            </li> 
                            <li class="list-group-item d-flex justify-content-between pe-0">
                                <span class="weight-600 color-444 nameProduto">Produto:</span>
                                <div>
                                    <a href="" class="weight-600 color-blue-info-link" id="entrada_produto_nome">

                                    </a>
                                    <a href="" class="ms-3 text-primary invisible">
                                        <i class="fa fa-edit fs-15"></i>
                                    </a>
                                </div>
                            </li>
                            <li class="list-group-item d-flex align-items-center justify-content-between pe-0">
                                <span class="weight-600 color-444">Valor:</span>
                                <div class="d-flex align-items-center">
                                    <span class="weight-600 value-list" id="entrada_valor">
                                    </span>
                                    <input type="text" id="input_editar_entrada_valor" class="mask_valor form-control" placeholder="R$" style="display:none;">
                                    <a href="#" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-title="Editar" class="ms-3 text-primary" id="btn_editar_entrada_valor">
                                        <i class="fa fa-edit fs-15"></i>
                                    </a>
                                    <a href="#" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-title="Salvar" class="btnSaveEditEntrada text-success mx-2" data-field="valor_parcela" style="display:none;">
                                        <i class="fas fa-check-circle fs-17"></i>
                                    </a>
                                    <a href="#" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-title="Cancelar" class="btnResetEditEntrada text-secondary" style="display:none;">
                                        <i class="fas fa-minus-circle fs-17"></i>
                                    </a>
                                </div>
                            </li>

                            <li class="list-group-item d-flex align-items-center justify-content-between pe-0">
                                <span class="weight-600 color-444">Vendedor:</span>
                                <div class="d-flex align-items-center">
                                    <a href="#" data-vendedor="vendedor_id" class="weight-600 color-blue-info-link" id="entrada_vendedor_nome">

                                    </a>

                                    <select id="input_editar_entrada_vendedor" class="select-form" style="display:none;">
                                        <option value="">Selecione</option>
                                        @foreach($vendedores as $vendedor)
                                            <option value="{{ $vendedor->id }}">
                                                {{ \App\Models\Util::getPrimeiroSegundoNome($vendedor->nome) }}
                                            </option>
                                        @endforeach
                                    </select>

                                    <a href="#" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-title="Editar" class="ms-3 text-primary" id="btn_editar_entrada_vendedor">
                                        <i class="fa fa-edit fs-15"></i>
                                    </a>
                                    <a href="#" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-title="Salvar" class="btnSaveEditEntrada text-success mx-2" data-field="vendedor" style="display:none;">
                                        <i class="fas fa-check-circle fs-17"></i>
                                    </a>
                                    <a href="#" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-title="Cancelar" class="btnResetEditEntrada text-secondary" style="display:none;">
                                        <i class="fas fa-minus-circle fs-17"></i>
                                    </a>
                                </div>
                            </li>

                            <li class="list-group-item d-flex align-items-center justify-content-between pe-0" id="boxEntradaUnidade">
                                <span class="weight-600 color-444">Unidade:</span>
                                <div class="d-flex align-items-center">
                                    <a href="#" data-unidade="unidade_id" class="weight-600 color-blue-info-link" id="entrada_unidade_nome">

                                    </a>

                                    <select id="input_editar_entrada_unidade" class="select-form" style="display:none;">
                                        
                                        @if(is_null($unidade_id))
                                            <option value="">Selecione</option>
                                        @endif

                                        @foreach($unidades as $unidade)
                                            <option value="{{ $unidade->id }}" @if(isset($unidade_id) && $unidade_id == $unidade->id) selected @endif>
                                                {{ $unidade->nome }}
                                            </option>
                                        @endforeach
                                    </select>

                                    <a href="#" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-title="Editar" class="ms-3 text-primary" id="btn_editar_entrada_unidade">
                                        <i class="fa fa-edit fs-15"></i>
                                    </a>
                                    <a href="#" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-title="Salvar" class="btnSaveEditEntrada text-success mx-2" data-field="unidade" style="display:none;">
                                        <i class="fas fa-check-circle fs-17"></i>
                                    </a>
                                    <a href="#" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-title="Cancelar" class="btnResetEditEntrada text-secondary" style="display:none;">
                                        <i class="fas fa-minus-circle fs-17"></i>
                                    </a>
                                </div>
                            </li>

                            <li class="list-group-item d-flex align-items-center justify-content-between pe-0">
                                <span class="weight-600 color-444">Forma de Pagamento:</span>
                                <div class="d-flex align-items-center">
                                    <span class="weight-600" id="entrada_forma_pagamento">
                                        
                                    </span>

                                    <select id="input_editar_entrada_formapagamento" style="display:none;" class="select-form">
                                        <option value="" selected>Selecione</option>
                                        <option value="Dinheiro">Dinheiro</option>
                                        <option value="Cartão de débito">Cartão de débito</option>
                                        <option value="Cartão de crédito">Cartão de crédito</option>
                                        <option value="Boleto bancário">Boleto bancário</option>
                                        <option value="Pix">Pix</option>
                                        <option value="TED">TED</option>
                                        <option value="DOC">DOC</option>
                                    </select>

                                    <a href="#" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-title="Editar" class="ms-3 text-primary" id="btn_editar_entrada_formapagamento">
                                        <i class="fa fa-edit fs-15"></i>
                                    </a>
                                    <a href="#" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-title="Salvar" class="btnSaveEditEntrada text-success mx-2" data-field="forma_pagamento" style="display:none;">
                                        <i class="fas fa-check-circle fs-17"></i>
                                    </a>
                                    <a href="#" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-title="Cancelar" class="btnResetEditEntrada text-secondary" style="display:none;">
                                        <i class="fas fa-minus-circle fs-17"></i>
                                    </a>
                                </div>
                            </li>

                            <li class="list-group-item d-flex align-items-center justify-content-between pe-0">
                                <span class="weight-600 color-444">Observações:</span>
                                <div class="d-flex justify-content-between align-items-center">
                                    <span class="w-100 weight-600 value-list" id="entrada_obs">
                                    </span>
                                    <textarea id="input_editar_entrada_obs" class="ms-3 textarea-form h-min-50" placeholder="Observações" style="display:none;"></textarea>
                                    <a href="#" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-title="Editar" class="ms-3 text-primary" id="btn_editar_entrada_obs">
                                        <i class="fa fa-edit fs-15"></i>
                                    </a>
                                    <a href="#" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-title="Salvar" class="btnSaveEditEntrada text-success mx-2" data-field="obs" style="display:none;">
                                        <i class="fas fa-check-circle fs-17"></i>
                                    </a>
                                    <a href="#" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-title="Cancelar" class="btnResetEditEntrada btnEntradaResetEditObs text-secondary" style="display:none;">
                                        <i class="fas fa-minus-circle fs-17"></i>
                                    </a>
                                </div>
                            </li>


                            <li class="list-group-item d-flex align-items-center justify-content-between pe-0">
                                <span class="weight-600 color-444">Link de Pagamento:</span>
                                <div class="d-flex align-items-center">
                                    <a href="#" target="_blank" class="weight-600 color-blue-info-link" id="entrada_link_pagamento">
                                        Abrir para ver
                                    </a>
                                    <input type="text" id="input_editar_entrada_link_pagamento" class="form-control" placeholder="Link" style="display:none;">
                                    <a href="#" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-title="Editar" class="ms-3 text-primary" id="btn_editar_entrada_link_pagamento">
                                        <i class="fa fa-edit fs-15"></i>
                                    </a>
                                    <a href="#" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-title="Salvar" class="btnSaveEditEntrada text-success mx-2" data-field="link_pagamento" style="display:none;">
                                        <i class="fas fa-check-circle fs-17"></i>
                                    </a>
                                    <a href="#" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-title="Cancelar" class="btnResetEditEntrada text-secondary" style="display:none;">
                                        <i class="fas fa-minus-circle fs-17"></i>
                                    </a>
                                </div>
                            </li>

                        </ul>

                    </div>
                    <div class="modal-footer w-100" id="boxModalFooterAcoesEntrada">
                        <div class="w-100 aclNivel2" id="boxEntradaConcluirPagamento">
                            <div class="d-flex align-items-end w-100">
                                <div class="w-lg-23 me-3">
                                    <label>Recebido em</label>
                                    <input type="date" name="data_recebimento" class="bg-primary calendar-icon-white text-light border-0 py-2-5 form-control p-form" value="{{ date('Y-m-d') }}">
                                </div>
                                <div class="w-100">
                                    <button type="submit" name="tipo_acao" value="concluir_pagamento" class="btn btn-primary weight-600 py-2-5 w-100">
                                        <i class="far fa-check-circle"></i>
                                        <span class="ms-1">Concluir Pagamento</span>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="w-100 aclNivel3" id="boxEntradaEstornarPagamento">

                            <div class="mb-3 w-100">
                                <label>Motivo de Estorno</label> 
                                <input type="text" name="motivo_estorno" class="form-control p-form" placeholder="Descreva o motivo desta compra precisar ser estornada...">
                            </div>

                            <div class="d-flex align-items-end w-100">
                                <div class="w-lg-23 me-3">
                                    <label>Estornado em</label>
                                    <input type="date" name="data_estorno" class="bg-estorno calendar-icon-white text-light border-0 py-2-5 form-control p-form" value="{{ date('Y-m-d') }}">
                                </div>
                                <div class="w-100">
                                    <button type="submit" name="tipo_acao" value="concluir_estorno" class="btn bg-estorno text-light weight-600 py-2-5 w-100">
                                        <i class="far fa-check-circle"></i>
                                        <span class="ms-1">Concluir Estorno</span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <form class="w-100" action="{{ route('admin.controle-financeiro.lista.editar-setstatus') }}">
    @csrf
        <div class="modal fade" id="ModalDadosEAcoesSaida" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content" style="border-radius: 10px;">
                    <div class="modal-header w-100">
                        <div class="w-100 d-flex justify-content-between align-items-center">
                            <h3 class="modal-title" id="exampleModalLabel">Dados de Saída</h3>
                                <div class="me-1" id="boxLoadingSaida">
                                    <div class="spinner-grow text-primary" role="status">
                                      <span class="visually-hidden">Loading...</span>
                                    </div>
                                </div>
                            <div>
                                <span class="weight-600" id="saida_status">

                                </span>
                            </div>
                            <div data-bs-toggle="tooltip" data-bs-placement="top" data-bs-title="Deletar" class="me-3">
                                <a href="#ModalDeletarSaida" onclick="setCamposToDeleteSaida()" data-bs-toggle="modal" data-bs-dismiss="modal" class="text-danger fs-17" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-title="Deletar">
                                    <i class="fas fa-trash-alt"></i>
                                </a>
                            </div>
                        </div>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-title="Fechar" aria-label="Close"></button>
                    </div>
                    <div class="modal-body" style="border:5px solid #eee;">

                        <input type="hidden" name="saida_id" id="dados_e_acoes_saida_id">
                        <input type="hidden" id="saida_cliente_id">

                        <ul class="list-group list-group-flush">
                            <li class="list-group-item d-flex align-items-center justify-content-between pe-0">
                                <span class="weight-600 color-444 nameProduto">Data de Vencimento:</span>
                                <div class="d-flex align-items-center">
                                    <span class="weight-600" id="saida_data_vencimento">

                                    </span>
                                    <input type="date" id="input_editar_saida_data_vencimento" class="form-control" style="display:none;">
                                    <a href="#" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-title="Editar" class="ms-3 text-primary" id="btn_editar_saida_datavencimento">
                                        <i class="fa fa-edit fs-15"></i>
                                    </a>
                                    <a href="#" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-title="Salvar" class="btnSaveEditSaida text-success mx-2" data-field="data_vencimento" style="display:none;">
                                        <i class="fas fa-check-circle fs-17"></i>
                                    </a>
                                    <a href="#" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-title="Cancelar" class="btnResetEditSaida text-secondary" style="display:none;">
                                        <i class="fas fa-minus-circle fs-17"></i>
                                    </a>
                                </div>
                            </li>
                            <li class="list-group-item d-flex align-items-center justify-content-between pe-0" id="boxSaidaDataRecebimento">
                                <span class="weight-600 color-444 nameProduto">Data de Recebimento:</span>
                                <div class="d-flex align-items-center">
                                    <span class="weight-600" id="saida_data_recebimento">

                                    </span>
                                    <input type="date" id="input_editar_saida_data_recebimento" class="form-control" style="display:none;">
                                    <a href="#" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-title="Editar" class="ms-3 text-primary" id="btn_editar_saida_datarecebimento">
                                        <i class="fa fa-edit fs-15"></i>
                                    </a>
                                    <a href="#" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-title="Salvar" class="btnSaveEditSaida text-success mx-2" data-field="data_recebimento" style="display:none;">
                                        <i class="fas fa-check-circle fs-17"></i>
                                    </a>
                                    <a href="#" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-title="Cancelar" class="btnResetEditSaida text-secondary" style="display:none;">
                                        <i class="fas fa-minus-circle fs-17"></i>
                                    </a>
                                </div>
                            </li>
                            <li class="list-group-item d-flex align-items-center justify-content-between pe-0" id="boxSaidaDataEstorno">
                                <span class="weight-600 color-444 nameProduto">Data de Estorno:</span>
                                <div class="d-flex align-items-center">
                                    <span class="weight-600" id="saida_data_estorno">

                                    </span>
                                    <input type="date" id="input_editar_saida_data_estorno" class="form-control" style="display:none;">
                                    <a href="#" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-title="Editar" class="ms-3 text-primary" id="btn_editar_saida_dataestorno">
                                        <i class="fa fa-edit fs-15"></i>
                                    </a>
                                    <a href="#" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-title="Salvar" class="btnSaveEditSaida text-success mx-2" data-field="data_estorno" style="display:none;">
                                        <i class="fas fa-check-circle fs-17"></i>
                                    </a>
                                    <a href="#" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-title="Cancelar" class="btnResetEditSaida text-secondary" style="display:none;">
                                        <i class="fas fa-minus-circle fs-17"></i>
                                    </a>
                                </div>
                            </li>
                            <li class="list-group-item d-flex align-items-center justify-content-between pe-0" id="boxSaidaMotivoEstorno">
                                <span class="weight-600 color-444">Motivo de Estorno:</span>
                                <div class="d-flex justify-content-between align-items-center">
                                    <span class="w-100 weight-600 value-list" id="saida_motivo_estorno">
                                    </span>
                                    <textarea id="input_editar_saida_motivo_estorno" class="ms-3 textarea-form h-min-50" placeholder="Motivo do Estorno" style="display:none;"></textarea>
                                    <a href="#" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-title="Editar" class="ms-3 text-primary" id="btn_editar_saida_motivo_estorno">
                                        <i class="fa fa-edit fs-15"></i>
                                    </a>
                                    <a href="#" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-title="Salvar" class="btnSaveEditSaida text-success mx-2" data-field="motivo_estorno" style="display:none;">
                                        <i class="fas fa-check-circle fs-17"></i>
                                    </a>
                                    <a href="#" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-title="Cancelar" class="btnResetEditSaida btnSaidaResetEditMotivoEstorno text-secondary" style="display:none;">
                                        <i class="fas fa-minus-circle fs-17"></i>
                                    </a>
                                </div>
                            </li>
                            <li class="list-group-item d-flex align-items-center justify-content-between pe-0">
                                <span class="weight-600 color-444">Referência:</span>
                                <div class="d-flex align-items-center">
                                    <span class="weight-600 value-list" id="saida_referencia">
                                    </span>
                                    <input type="text" id="input_editar_saida_referencia" class="form-control" placeholder="Referência" style="display:none;">
                                    <a href="#" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-title="Editar" class="ms-3 text-primary" id="btn_editar_saida_referencia">
                                        <i class="fa fa-edit fs-15"></i>
                                    </a>
                                    <a href="#" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-title="Salvar" class="btnSaveEditSaida text-success mx-2" data-field="referencia" style="display:none;">
                                        <i class="fas fa-check-circle fs-17"></i>
                                    </a>
                                    <a href="#" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-title="Cancelar" class="btnResetEditSaida text-secondary" style="display:none;">
                                        <i class="fas fa-minus-circle fs-17"></i>
                                    </a>
                                </div>
                            </li>
                            <li class="list-group-item d-flex align-items-center justify-content-between pe-0">
                                <span class="weight-600 color-444">Valor:</span>
                                <div class="d-flex align-items-center">
                                    <span class="weight-600 value-list" id="saida_valor">
                                    </span>
                                    <input type="text" id="input_editar_saida_valor" class="mask_valor form-control" placeholder="R$" style="display:none;">
                                    <a href="#" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-title="Editar" class="ms-3 text-primary" id="btn_editar_saida_valor">
                                        <i class="fa fa-edit fs-15"></i>
                                    </a>
                                    <a href="#" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-title="Salvar" class="btnSaveEditSaida text-success mx-2" data-field="valor_parcela" style="display:none;">
                                        <i class="fas fa-check-circle fs-17"></i>
                                    </a>
                                    <a href="#" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-title="Cancelar" class="btnResetEditSaida text-secondary" style="display:none;">
                                        <i class="fas fa-minus-circle fs-17"></i>
                                    </a>
                                </div>
                            </li>

                            <li class="list-group-item d-flex align-items-center justify-content-between pe-0" id="boxSaidaUnidade">
                                <span class="weight-600 color-444">Unidade:</span>
                                <div class="d-flex align-items-center">
                                    <a href="#" data-unidade="unidade_id" class="weight-600 color-blue-info-link" id="saida_unidade_nome">

                                    </a>

                                    <select id="input_editar_saida_unidade" class="select-form" style="display:none;">
                                        
                                        @if(is_null($unidade_id))
                                            <option value="">Selecione</option>
                                        @endif

                                        @foreach($unidades as $unidade)
                                            <option value="{{ $unidade->id }}" @if(isset($unidade_id) && $unidade_id == $unidade->id) selected @endif>
                                                {{ $unidade->nome }}
                                            </option>
                                        @endforeach
                                    </select>

                                    <a href="#" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-title="Editar" class="ms-3 text-primary" id="btn_editar_saida_unidade">
                                        <i class="fa fa-edit fs-15"></i>
                                    </a>
                                    <a href="#" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-title="Salvar" class="btnSaveEditSaida text-success mx-2" data-field="unidade" style="display:none;">
                                        <i class="fas fa-check-circle fs-17"></i>
                                    </a>
                                    <a href="#" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-title="Cancelar" class="btnResetEditSaida text-secondary" style="display:none;">
                                        <i class="fas fa-minus-circle fs-17"></i>
                                    </a>
                                </div>
                            </li>

                            <li class="list-group-item d-flex align-items-center justify-content-between pe-0">
                                <span class="weight-600 color-444">Forma de Pagamento:</span>
                                <div class="d-flex align-items-center">
                                    <span class="weight-600" id="saida_forma_pagamento">
                                        
                                    </span>

                                    <select id="input_editar_saida_formapagamento" style="display:none;" class="select-form">
                                        <option value="" selected>Selecione</option>
                                        <option value="Dinheiro">Dinheiro</option>
                                        <option value="Cartão de débito">Cartão de débito</option>
                                        <option value="Cartão de crédito">Cartão de crédito</option>
                                        <option value="Boleto bancário">Boleto bancário</option>
                                        <option value="Pix">Pix</option>
                                        <option value="TED">TED</option>
                                        <option value="DOC">DOC</option>
                                    </select>

                                    <a href="#" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-title="Editar" class="ms-3 text-primary" id="btn_editar_saida_formapagamento">
                                        <i class="fa fa-edit fs-15"></i>
                                    </a>
                                    <a href="#" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-title="Salvar" class="btnSaveEditSaida text-success mx-2" data-field="forma_pagamento" style="display:none;">
                                        <i class="fas fa-check-circle fs-17"></i>
                                    </a>
                                    <a href="#" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-title="Cancelar" class="btnResetEditSaida text-secondary" style="display:none;">
                                        <i class="fas fa-minus-circle fs-17"></i>
                                    </a>
                                </div>
                            </li>

                            <li class="list-group-item d-flex align-items-center justify-content-between pe-0">
                                <span class="weight-600 color-444">Observações:</span>
                                <div class="d-flex justify-content-between align-items-center">
                                    <span class="w-100 weight-600 value-list" id="saida_obs">
                                    </span>
                                    <textarea id="input_editar_saida_obs" class="ms-3 textarea-form h-min-50" placeholder="Observações" style="display:none;"></textarea>
                                    <a href="#" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-title="Editar" class="ms-3 text-primary" id="btn_editar_saida_obs">
                                        <i class="fa fa-edit fs-15"></i>
                                    </a>
                                    <a href="#" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-title="Salvar" class="btnSaveEditSaida text-success mx-2" data-field="obs" style="display:none;">
                                        <i class="fas fa-check-circle fs-17"></i>
                                    </a>
                                    <a href="#" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-title="Cancelar" class="btnResetEditSaida btnSaidaResetEditObs text-secondary" style="display:none;">
                                        <i class="fas fa-minus-circle fs-17"></i>
                                    </a>
                                </div>
                            </li>


                            <li class="list-group-item d-flex align-items-center justify-content-between pe-0">
                                <span class="weight-600 color-444">Link de Pagamento:</span>
                                <div class="d-flex align-items-center">
                                    <a href="#" target="_blank" class="weight-600 color-blue-info-link" id="saida_link_pagamento">
                                        Abrir para ver
                                    </a>
                                    <input type="text" id="input_editar_saida_link_pagamento" class="form-control" placeholder="Link" style="display:none;">
                                    <a href="#" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-title="Editar" class="ms-3 text-primary" id="btn_editar_saida_link_pagamento">
                                        <i class="fa fa-edit fs-15"></i>
                                    </a>
                                    <a href="#" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-title="Salvar" class="btnSaveEditSaida text-success mx-2" data-field="link_pagamento" style="display:none;">
                                        <i class="fas fa-check-circle fs-17"></i>
                                    </a>
                                    <a href="#" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-title="Cancelar" class="btnResetEditSaida text-secondary" style="display:none;">
                                        <i class="fas fa-minus-circle fs-17"></i>
                                    </a>
                                </div>
                            </li>

                        </ul>

                    </div>
                    <div class="modal-footer w-100" id="boxModalFooterAcoesSaida">
                        <div class="w-100 aclNivel2" id="boxSaidaConcluirPagamento">
                            <div class="d-flex align-items-end w-100">
                                <div class="w-lg-23 me-3">
                                    <label>Recebido em</label>
                                    <input type="date" name="data_recebimento" class="bg-primary calendar-icon-white text-light border-0 py-2-5 form-control p-form" value="{{ date('Y-m-d') }}">
                                </div>
                                <div class="w-100">
                                    <button type="submit" name="tipo_acao" value="concluir_pagamento" class="btn btn-primary weight-600 py-2-5 w-100">
                                        <i class="far fa-check-circle"></i>
                                        <span class="ms-1">Concluir Pagamento</span>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="w-100 aclNivel3" id="boxSaidaEstornarPagamento">

                            <div class="mb-3 w-100">
                                <label>Motivo de Estorno</label> 
                                <input type="text" name="motivo_estorno" class="form-control p-form" placeholder="Descreva o motivo desta compra precisar ser estornada...">
                            </div>

                            <div class="d-flex align-items-end w-100">
                                <div class="w-lg-23 me-3">
                                    <label>Estornado em</label>
                                    <input type="date" name="data_estorno" class="bg-estorno calendar-icon-white text-light border-0 py-2-5 form-control p-form" value="{{ date('Y-m-d') }}">
                                </div>
                                <div class="w-100">
                                    <button type="submit" name="tipo_acao" value="concluir_estorno" class="btn bg-estorno text-light weight-600 py-2-5 w-100">
                                        <i class="far fa-check-circle"></i>
                                        <span class="ms-1">Concluir Estorno</span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <form action="/admin/controle-financeiro/lista/fluxo-caixa/deletar-entrada" method="POST">
    @csrf
        <div class="modal fade" id="ModalDeletarEntrada" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3 class="modal-title" id="exampleModalLabel">Deletar Entrada</h3>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body py-5">

                        <input type="hidden" name="deletar_entrada_id" id="deletar_entrada_id">

                        <div class="row align-items-center px-4">

                            <div class="col-6">
                                <p class="m-0 fs-15">
                                    <span class="d-block">Valor</span>
                                    <span class="weight-600" id="deletar_entrada_valor">

                                    </span>
                                </p>
                            </div>
                            <div class="col-6">
                                <p class="m-0 fs-15">
                                    <span class="d-block">Nome</span>
                                    <span class="weight-600" id="deletar_entrada_nome">

                                    </span>
                                </p>
                            </div>

                        </div>

                        <hr/>

                        <p class="text-center m-0 fs-15">
                            Você tem certeza que deseja deletar esta venda?
                            <br>
                            Todas as informações também serão deletadas definitivamente.
                            <span class="m-0 d-block mt-2 weight-600 text-danger" id="deletar_entrada_others_parcelas">
                                
                            </span>
                        </p>

                    </div>
                    <div class="modal-footer">
                        <div class="d-flex w-100">
                            <input type="password" name="password_to_delete" class="w-100 form-control p-form me-3" placeholder="Digite sua senha" required>
                            <button type="submit" class="btn btn-danger weight-600">Confirmar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <form action="/admin/controle-financeiro/lista/fluxo-caixa/deletar-saida" method="POST">
    @csrf
        <div class="modal fade" id="ModalDeletarSaida" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3 class="modal-title" id="exampleModalLabel">Deletar Saída</h3>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body py-5">

                        <input type="hidden" name="deletar_saida_id" id="deletar_saida_id">

                        <div class="row align-items-center px-4">

                            <div class="col-6">
                                <p class="m-0 fs-15">
                                    <span class="d-block">Valor</span>
                                    <span class="weight-600" id="deletar_saida_valor">

                                    </span>
                                </p>
                            </div>
                            <div class="col-6">
                                <p class="m-0 fs-15">
                                    <span class="d-block">Nome</span>
                                    <span class="weight-600" id="deletar_saida_nome">

                                    </span>
                                </p>
                            </div>

                        </div>

                        <hr/>

                        <p class="text-center m-0 fs-15">
                            Você tem certeza que deseja deletar esta saída?
                            <br>
                            Todas as informações também serão deletadas definitivamente.
                            <span class="m-0 d-block mt-2 weight-600 text-danger" id="deletar_saida_others_parcelas">
                                
                            </span>
                        </p>

                    </div>
                    <div class="modal-footer">
                        <div class="d-flex w-100">
                            <input type="password" name="password_to_delete" class="w-100 form-control p-form me-3" placeholder="Digite sua senha" required>
                            <button type="submit" class="btn btn-danger weight-600">Confirmar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <form action="{{ route('admin.controle-financeiro.lista.fluxo-caixa.add-saida') }}" method="POST">
        @csrf

        <div class="modal fade" id="ModalAddSaida" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3 class="modal-title" id="exampleModalLabel">+ Adicionar Saída</h3>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">

                        <input type="hidden" name="add_saida_unidade_nome"  id="add_saida_unidade_nome">
                        <input type="hidden" name="add_saida_produto_nome"  id="add_saida_produto_nome">

                        <div class="d-lg-flex mb-3">

                            <div class="w-100 me-lg-3 mb-3 mb-lg-0">
                                <label>Produto</label>
                                <select name="add_saida_produto_id" onchange="setAddSaidaProdutoNomeAndValorAndReferencia(this)" class="selectpicker" data-width="100%" data-live-search="true" required>
                                    <option value="">Selecione</option>

                                    @if(\App\Models\Plano::checkActive() === true && count($planos) > 0)
                                    <optgroup label="Planos">
                                        @foreach($planos as $plano)
                                            <option value="A-{{ $plano->id }}" data-valor="{{ $plano->valor_promocional ?? $plano->valor }}">
                                                {{ $plano->nome }}
                                            </option>
                                        @endforeach
                                    </optgroup>
                                    @endif

                                    @if(\App\Models\Turma::checkActive() === true && count($servicos) > 0)
                                    <optgroup label="Serviços">
                                        @foreach($servicos as $servico)
                                            <option value="S-{{ $servico->id }}" data-valor="{{ $servico->valor }}">
                                                {{ $servico->nome }}
                                            </option>
                                        @endforeach
                                    </optgroup>
                                    @endif

                                    @if(\App\Models\Produto::checkActive() === true && count($produtos) > 0)
                                    <optgroup label="Produtos">
                                        @foreach($produtos as $produto)
                                            <option value="P-{{ $produto->id }}" data-valor="{{ $produto->valor_promocional ?? $produto->valor }}">
                                                {{ $produto->nome }}
                                            </option>
                                        @endforeach
                                    </optgroup>
                                    @endif
                                    
                                </select>
                            </div>

                            <div class="w-100">
                                <label>Referência</label>
                                <input type="text" name="add_saida_referencia" id="add_saida_referencia" class="form-control p-form" placeholder="Referência">
                            </div>

                            @if(\App\Models\Unidade::checkActive() === true && count($unidades) > 0)
                            <div class="w-100 ms-lg-3 mt-3 mt-lg-0">
                                <label>Unidade</label>
                                <select name="add_saida_unidade_id" id="add_saida_unidade_id"  onchange="setAddSaidaUnidadeNome(this)" class="selectpicker" data-width="100%" data-live-search="true">
                                    <option value="">Selecione</option>
                                    @foreach($unidades as $unidade)
                                        <option value="{{ $unidade->id }}">{{ $unidade->nome }}</option>
                                    @endforeach
                                </select>
                            </div>
                            @endif

                        </div>

                        <div class="d-lg-flex mb-3">

                            <div class="w-lg-50 me-lg-3 mb-3 mb-lg-0">
                                <label>Data de Vencimento</label>
                                <input type="date" name="add_saida_data_vencimento" id="add_saida_data_vencimento" class="form-control p-form" required>
                            </div>

                            <div class="w-lg-50 me-lg-3 mb-3 mb-lg-0">
                                <label>Valor</label>
                                <input type="text" name="add_saida_valor" id="add_saida_valor" class="mask_valor form-control p-form" placeholder="R$" required>
                            </div>

                            <div class="w-lg-50 me-lg-3 mb-3 mb-lg-0">
                                <label>Forma de Pagamento</label>
                                <select name="add_saida_forma_pagamento" id="add_saida_forma_pagamento" class="select-form">
                                    <option value="">Selecione</option>
                                    <option value="Dinheiro">Dinheiro</option>
                                    <option value="Cartão de débito">Cartão de débito</option>
                                    <option value="Cartão de crédito">Cartão de crédito</option>
                                    <option value="Boleto bancário">Boleto bancário</option>
                                    <option value="Pix">Pix</option>
                                    <option value="TED">TED</option>
                                    <option value="DOC">DOC</option>
                                </select>
                            </div>

                            <div class="w-lg-30">
                                <label>Parcelas</label>
                                <input type="text" id="add_saida_n_parcelas" name="add_saida_n_parcelas" onchange="gerarParcelasAddSaida(this.value)" class="number form-control p-form" placeholder="N°" required value="1">
                            </div>

                        </div>

                        <div class="mb-3 w-100">
                            <label>Link de Pagamento</label>
                            <input type="text" name="link" class="form-control p-form" placeholder="Link">
                        </div>

                        <div id="boxParcelasAddSaida" style="display:none;" class="mb-3">

                            <label class="mb-0 d-block">Parcelas</label>
                            <hr class="mt-2">

                            <div class="mb-2 d-flex align-items-center parcelas">

                                <div class="me-2 border bg-light rounded px-3 text-center flex-center w-lg-20" readonly style="height: 42.5px;">
                                    N°
                                </div>

                                <div class="me-2 border bg-light rounded px-3 d-flex align-items-center w-100" readonly style="height: 42.5px;">
                                    Data de Vencimento
                                </div>

                                <div class="me-2 border bg-light rounded px-3 d-flex align-items-center w-100" readonly style="height: 42.5px;">
                                    Valor
                                </div>

                                <div class="me-2 border bg-light rounded px-3 d-flex align-items-center w-100" readonly style="height: 42.5px;">
                                    Link de Pagamento
                                </div>

                                <div class="border bg-light rounded px-3 d-flex align-items-center w-100" readonly style="height: 42.5px;">
                                    Forma de Pagamento
                                </div>

                            </div>
                            
                            <div id="appendParcelaAddSaida" class="w-100">
                                

                            </div>

                        </div>

                        <div class="mb-3">
                            <label>Observações internas</label>
                            <textarea name="add_saida_obs" class="textarea-form" placeholder="Descreva observações..."></textarea>
                        </div>

                        <div class="w-100">
                            <label>Categorias</label>
                            <select name="categoriasSelected[]" class="selectpicker" multiple data-width="100%" data-live-search="true" title="Selecione">
                                <option value="">Selecione</option>
                                @foreach($categorias as $categoria)
                                    <option value="{{ $categoria->id }}">
                                        {{ $categoria->nome }}
                                    </option>
                                @endforeach
                            </select>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary weight-600 py-2">Confirmar</button>
                    </div>
                </div>
            </div>
        </div>
    </form>

</div>

@endsection

@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/gh/farhadmammadli/bootstrap-select@main/js/bootstrap-select.min.js"></script>
<script src="{{ asset('js/moment.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.js"></script>
<script src="{{ asset('js/moment.min.js') }}"></script>
<script>

    var textoParcelaToDeleteEntrada = null;;
    var textoParcelaToDeleteSaida = null;;

    $(document).ready(function()
    {
        $("#unidade_id").val("{{$_GET['unidade_id'] ?? null}}");
        $("#produto_id").val("{{$_GET['produto_id'] ?? null}}");
        $("#vendedor_id").val("{{$_GET['vendedor_id'] ?? null}}");
        $("#categoria_id").val("{{$_GET['categoria_id'] ?? null}}");
        $(".selectpicker").selectpicker("refresh");
        $("#status").val('{{$_GET['status'] ?? null}}');

        $("#add_entrada_desconto").mask('#.##0,00', {
          reverse: true
        });
    });


    $("#btn_editar_entrada_vendedor").click(function(e)
    {   
        e.preventDefault();

        vendedor_id = $("#entrada_vendedor_nome").data('vendedor');

        $("#entrada_vendedor_nome").hide();

        $("#input_editar_entrada_vendedor").show().focus();

        if(vendedor_id != '' && vendedor_id != 'Não definido')
        {
            $("#input_editar_entrada_vendedor").val(vendedor_id);
        }
        
        $(this).hide();
        $(this).next().show();
        $(this).next().next().show();

    });

    $("#btn_editar_entrada_unidade").click(function(e)
    {   
        e.preventDefault();

        unidade_id = $("#entrada_unidade_nome").data('unidade');

        $("#entrada_unidade_nome").hide();

        $("#input_editar_entrada_unidade").show().focus();

        if(unidade_id != '' && unidade_id != 'Não definido')
        {
            $("#input_editar_entrada_unidade").val(unidade_id);
        }
        
        $(this).hide();
        $(this).next().show();
        $(this).next().next().show();

    });

    $("#btn_editar_entrada_formapagamento").click(function(e)
    {   
        e.preventDefault();

        forma_pagamento = $("#entrada_forma_pagamento").text();

        $("#entrada_forma_pagamento").hide();

        $("#input_editar_entrada_formapagamento").show().focus();

        if(forma_pagamento != '' && forma_pagamento != 'Não definido')
        {
            $("#input_editar_entrada_formapagamento").val(forma_pagamento);
        }
        
        $(this).hide();
        $(this).next().show();
        $(this).next().next().show();

    });


    $("#btn_editar_entrada_datarecebimento").click(function(e)
    {   
        e.preventDefault();

        data_recebimento = $("#entrada_data_recebimento").text();

        let data_recebimento_en = data_recebimento.split("/").reverse().join("-");

        $("#entrada_data_recebimento").hide();

        $("#input_editar_entrada_data_recebimento").show().focus().val(data_recebimento_en);
        
        $(this).hide();
        $(this).next().show();
        $(this).next().next().show();

    });

    $("#btn_editar_entrada_dataestorno").click(function(e)
    {   
        e.preventDefault();

        data_estorno = $("#entrada_data_estorno").text();

        let data_estorno_en = data_estorno.split("/").reverse().join("-");

        $("#entrada_data_estorno").hide();

        $("#input_editar_entrada_data_estorno").show().focus().val(data_estorno_en);
        
        $(this).hide();
        $(this).next().show();
        $(this).next().next().show();

    });

    $("#btn_editar_entrada_datavencimento").click(function(e)
    {   
        e.preventDefault();

        data_vencimento = $("#entrada_data_vencimento").text();

        let data_vencimento_en = data_vencimento.split("/").reverse().join("-");

        $("#entrada_data_vencimento").hide();

        $("#input_editar_entrada_data_vencimento").show().focus().val(data_vencimento_en);
        
        $(this).hide();
        $(this).next().show();
        $(this).next().next().show();

    });

    $("#btn_editar_entrada_obs").click(function(e)
    {   
        e.preventDefault();

        obs = $("#entrada_obs").text();

        if(obs == 'Não definido')
        {
            obs = null;
        }

        $("#entrada_obs").hide();

        $("#input_editar_entrada_obs").show().focus().val(obs).addClass('w-100');
        
        $("#input_editar_entrada_obs").parent().addClass('w-100').parent().addClass('w-100');

        $(this).hide();
        $(this).next().show();
        $(this).next().next().show();

    });

    $("#btn_editar_entrada_motivo_estorno").click(function(e)
    {   
        e.preventDefault();

        motivo_estorno = $("#entrada_motivo_estorno").text();

        if(motivo_estorno == 'Não definido')
        {
            motivo_estorno = null;
        }

        $("#entrada_motivo_estorno").hide();

        $("#input_editar_entrada_motivo_estorno").show().focus().val(motivo_estorno).addClass('w-100');
        
        $("#input_editar_entrada_motivo_estorno").parent().addClass('w-100').parent().addClass('w-100');

        $(this).hide();
        $(this).next().show();
        $(this).next().next().show();

    });

    $("#btn_editar_entrada_valor").click(function(e)
    {   
        e.preventDefault();

        valor = $("#entrada_valor").text();

        $("#entrada_valor").hide();

        $("#input_editar_entrada_valor").show().focus().val(valor);
        
        $(this).hide();
        $(this).next().show();
        $(this).next().next().show();

    });

    $("#btn_editar_entrada_link_pagamento").click(function(e)
    {   
        e.preventDefault();

        link = $("#entrada_link_pagamento").attr('href');
        link = link.trim();

        $("#entrada_link_pagamento").hide();

        $("#input_editar_entrada_link_pagamento").show().focus();
        
        if(link != '' && link != 'Não definido' && link != 'Abrir para ver')
        {
            $("#input_editar_entrada_link_pagamento").val(link);
        }

        $(this).hide();
        $(this).next().show();
        $(this).next().next().show();

    });


    $("#btn_editar_saida_unidade").click(function(e)
    {   
        e.preventDefault();

        unidade_id = $("#saida_unidade_nome").data('unidade');

        $("#saida_unidade_nome").hide();

        $("#input_editar_saida_unidade").show().focus();

        if(unidade_id != '' && unidade_id != 'Não definido')
        {
            $("#input_editar_saida_unidade").val(unidade_id);
        }
        
        $(this).hide();
        $(this).next().show();
        $(this).next().next().show();

    });

    $("#btn_editar_saida_formapagamento").click(function(e)
    {   
        e.preventDefault();

        forma_pagamento = $("#saida_forma_pagamento").text();

        $("#saida_forma_pagamento").hide();

        $("#input_editar_saida_formapagamento").show().focus();

        if(forma_pagamento != '' && forma_pagamento != 'Não definido')
        {
            $("#input_editar_saida_formapagamento").val(forma_pagamento);
        }
        
        $(this).hide();
        $(this).next().show();
        $(this).next().next().show();

    });


    $("#btn_editar_saida_datarecebimento").click(function(e)
    {   
        e.preventDefault();

        data_recebimento = $("#saida_data_recebimento").text();

        let data_recebimento_en = data_recebimento.split("/").reverse().join("-");

        $("#saida_data_recebimento").hide();

        $("#input_editar_saida_data_recebimento").show().focus().val(data_recebimento_en);
        
        $(this).hide();
        $(this).next().show();
        $(this).next().next().show();

    });

    $("#btn_editar_saida_dataestorno").click(function(e)
    {   
        e.preventDefault();

        data_estorno = $("#saida_data_estorno").text();

        let data_estorno_en = data_estorno.split("/").reverse().join("-");

        $("#saida_data_estorno").hide();

        $("#input_editar_saida_data_estorno").show().focus().val(data_estorno_en);
        
        $(this).hide();
        $(this).next().show();
        $(this).next().next().show();

    });

    $("#btn_editar_saida_datavencimento").click(function(e)
    {   
        e.preventDefault();

        data_vencimento = $("#saida_data_vencimento").text();

        let data_vencimento_en = data_vencimento.split("/").reverse().join("-");

        $("#saida_data_vencimento").hide();

        $("#input_editar_saida_data_vencimento").show().focus().val(data_vencimento_en);
        
        $(this).hide();
        $(this).next().show();
        $(this).next().next().show();

    });

    $("#btn_editar_saida_obs").click(function(e)
    {   
        e.preventDefault();

        obs = $("#saida_obs").text();

        if(obs == 'Não definido')
        {
            obs = null;
        }

        $("#saida_obs").hide();

        $("#input_editar_saida_obs").show().focus().val(obs).addClass('w-100');
        
        $("#input_editar_saida_obs").parent().addClass('w-100').parent().addClass('w-100');

        $(this).hide();
        $(this).next().show();
        $(this).next().next().show();

    });

    $("#btn_editar_saida_motivo_estorno").click(function(e)
    {   
        e.preventDefault();

        motivo_estorno = $("#saida_motivo_estorno").text();

        if(motivo_estorno == 'Não definido')
        {
            motivo_estorno = null;
        }

        $("#saida_motivo_estorno").hide();

        $("#input_editar_saida_motivo_estorno").show().focus().val(motivo_estorno).addClass('w-100');
        
        $("#input_editar_saida_motivo_estorno").parent().addClass('w-100').parent().addClass('w-100');

        $(this).hide();
        $(this).next().show();
        $(this).next().next().show();

    });

    $("#btn_editar_saida_referencia").click(function(e)
    {   
        e.preventDefault();

        referencia = $("#saida_referencia").text();

        $("#saida_referencia").hide();

        $("#input_editar_saida_referencia").show().focus().val(referencia);
        
        $(this).hide();
        $(this).next().show();
        $(this).next().next().show();

    });

    $("#btn_editar_saida_valor").click(function(e)
    {   
        e.preventDefault();

        valor = $("#saida_valor").text();

        $("#saida_valor").hide();

        $("#input_editar_saida_valor").show().focus().val(valor);
        
        $(this).hide();
        $(this).next().show();
        $(this).next().next().show();

    });

    $("#btn_editar_saida_link_pagamento").click(function(e)
    {   
        e.preventDefault();

        link = $("#saida_link_pagamento").attr('href');
        link = link.trim();

        $("#saida_link_pagamento").hide();

        $("#input_editar_saida_link_pagamento").show().focus();
        
        if(link != '' && link != 'Não definido' && link != 'Abrir para ver')
        {
            $("#input_editar_saida_link_pagamento").val(link);
        }

        $(this).hide();
        $(this).next().show();
        $(this).next().next().show();

    });

    $(".btnEntradaResetEditObs").click(function(e) {

        e.preventDefault();

        $("#input_editar_entrada_obs").parent().removeClass('w-100').parent().removeClass('w-100');

    });

    $(".btnEntradaResetEditMotivoEstorno").click(function(e) {

        e.preventDefault();

        $("#input_editar_entrada_motivo_estorno").parent().removeClass('w-100').parent().removeClass('w-100');

    });

    $(".btnSaidaResetEditObs").click(function(e) {

        e.preventDefault();

        $("#input_editar_saida_obs").parent().removeClass('w-100').parent().removeClass('w-100');

    });

    $(".btnSaidaResetEditMotivoEstorno").click(function(e) {

        e.preventDefault();

        $("#input_editar_saida_motivo_estorno").parent().removeClass('w-100').parent().removeClass('w-100');

    });

    $(".btnSaveEditEntrada").click(function(e)
    {
        e.preventDefault();

        let registro_id = document.getElementById('dados_e_acoes_entrada_id').value;

        let field = $(this).data('field');

        checkValidate = null;

        if(field == 'valor_parcela')
        {
            checkValidate = updateEntradaValor(registro_id);
        }

        if(field == 'link_pagamento')
        {
            checkValidate = updateEntradaLinkPagamento(registro_id);
        }

        if(field == 'forma_pagamento')
        {
            checkValidate = updateEntradaFormaPagamento(registro_id);
        }

        if(field == 'vendedor')
        {
            checkValidate = updateEntradaVendedor(registro_id);
        }

        if(field == 'unidade')
        {
            checkValidate = updateEntradaUnidade(registro_id);
        }

        if(field == 'obs')
        {
            checkValidate = updateEntradaObs(registro_id);
        }

        if(field == 'data_vencimento')
        {
            checkValidate = updateEntradaDataVencimento(registro_id);
        }

        if(field == 'data_recebimento')
        {
            checkValidate = updateEntradaDataRecebimento(registro_id);
        }

        if(field == 'data_estorno')
        {
            checkValidate = updateEntradaDataEstorno(registro_id);
        }

        if(field == 'motivo_estorno')
        {
            checkValidate = updateEntradaMotivoEstorno(registro_id);
        }

        if(checkValidate == true)
        {
            $(this).prev().prev().hide();
            $(this).hide();
            $(this).next().hide();
            $(this).prev().show();
            $(this).prev().prev().prev().show();
        }
    });

    function updateEntradaValor(registro_id)
    {
        let valor = $("#input_editar_entrada_valor").val();

        if(valor == '')
        {
            $("#input_editar_entrada_valor").addClass('border border-danger').attr('placeholder','Ops! Este campo é obrigatório.');
            return false;
        
        } else {

            $("#input_editar_entrada_valor").removeClass('border border-danger').attr('placeholder','R$');
        }

        const data = {
          _method: 'PUT',
          _token: '{{ csrf_token() }}',
          registro_id: registro_id,
          valor: valor
        }
        $.post(`{{ route('admin.controle-financeiro.lista.editar-valor') }}`, data, function(res)
        {
            if(res['status'] == 1)
            {
                var url = new URL(window.location.href);
                url.searchParams.set('entrada_id', registro_id);
                url.searchParams.set('saida_id', '');
                window.location.href = url.href;

                $("#entrada_valor").text('Salvando e recalculando...');

            } else {

                alert('Ops! Houve algum problema.');
            }
        });

        return true;
    }

    function updateEntradaLinkPagamento(registro_id)
    {
        let link_pagamento = $("#input_editar_entrada_link_pagamento").val();

        const data = {
          _method: 'PUT',
          _token: '{{ csrf_token() }}',
          registro_id: registro_id,
          link_pagamento: link_pagamento
        }
        $.post(`{{ route('admin.controle-financeiro.lista.editar-link') }}`, data, function(res)
        {
            link_atualizado = $("#input_editar_entrada_link_pagamento").val();

            if(res['status'] == 1)
            {
                $("#entrada_link_pagamento").text('Abrir para ver').attr('href', link_atualizado);
                
            } else {

                alert('Ops! Houve algum problema.');
            }
        });

        return true;
    }

    function updateEntradaDataVencimento(registro_id)
    {
        let data_vencimento = $("#input_editar_entrada_data_vencimento").val();

        if(data_vencimento == '')
        {
            $("#input_editar_entrada_data_vencimento").addClass('border border-danger').attr('placeholder','Ops! Este campo é obrigatório.');
            return false;
        
        } else {

            $("#input_editar_entrada_data_vencimento").removeClass('border border-danger').attr('placeholder','R$');
        }

        const data = {
          _method: 'PUT',
          _token: '{{ csrf_token() }}',
          registro_id: registro_id,
          data_vencimento: data_vencimento
        }
        $.post(`{{ route('admin.controle-financeiro.lista.editar-datavencimento') }}`, data, function(res)
        {
            if(res['status'] == 1)
            {

                data_vencimento = data_vencimento.split("-");
                setAno = data_vencimento[0];
                setMes = data_vencimento[1];

                var url = new URL(window.location.href);
                url.searchParams.set('entrada_id', registro_id);
                url.searchParams.set('saida_id', '');
                url.searchParams.set('ano', setAno);
                url.searchParams.set('mes', setMes);
                window.location.href = url.href;

                $("#entrada_data_vencimento").text('Salvando e recalculando...');

            } else {

                alert('Ops! Houve algum problema.');
            }
        });

        return true;
    }

    function updateEntradaDataRecebimento(registro_id)
    {
        let data_recebimento = $("#input_editar_entrada_data_recebimento").val();

        const data = {
          _method: 'PUT',
          _token: '{{ csrf_token() }}',
          registro_id: registro_id,
          data_recebimento: data_recebimento
        }
        $.post(`{{ route('admin.controle-financeiro.lista.editar-datarecebimento') }}`, data, function(res)
        {
            if(res['status'] == 1)
            {
                data_recebimento_atualizada = $("#input_editar_entrada_data_recebimento").val();

                data_recebimento_atualizada = data_recebimento_atualizada.split("-").reverse().join("/");

                $("#entrada_data_recebimento").text(data_recebimento_atualizada);

            } else {

                alert('Ops! Houve algum problema.');
            }
        });

        return true;
    }

    function updateEntradaDataEstorno(registro_id)
    {
        let data_estorno = $("#input_editar_entrada_data_estorno").val();

        const data = {
          _method: 'PUT',
          _token: '{{ csrf_token() }}',
          registro_id: registro_id,
          data_estorno: data_estorno
        }
        $.post(`{{ route('admin.controle-financeiro.lista.editar-dataestorno') }}`, data, function(res)
        {
            if(res['status'] == 1)
            {
                data_estorno_atualizada = $("#input_editar_entrada_data_estorno").val();

                data_estorno_atualizada = data_estorno_atualizada.split("-").reverse().join("/");

                $("#entrada_data_estorno").text(data_estorno_atualizada);

            } else {

                alert('Ops! Houve algum problema.');
            }
        });

        return true;
    }

    function updateEntradaObs(registro_id)
    {
        let obs = $("#input_editar_entrada_obs").val();

        const data = {
          _method: 'PUT',
          _token: '{{ csrf_token() }}',
          registro_id: registro_id,
          obs: obs
        }
        $.post(`{{ route('admin.controle-financeiro.lista.editar-obs') }}`, data, function(res)
        {
            obs_atualizado = $("#input_editar_entrada_obs").val();

            if(res['status'] == 1)
            {
                $("#entrada_obs").text(obs_atualizado);
                
                $("#input_editar_entrada_obs").parent().removeClass('w-100').parent().removeClass('w-100');

            } else {

                alert('Ops! Houve algum problema.');
            }
        });

        return true;
    }

    function updateEntradaMotivoEstorno(registro_id)
    {
        let motivo_estorno = $("#input_editar_entrada_motivo_estorno").val();

        const data = {
          _method: 'PUT',
          _token: '{{ csrf_token() }}',
          registro_id: registro_id,
          motivo_estorno: motivo_estorno
        }
        $.post(`{{ route('admin.controle-financeiro.lista.editar-motivoestorno') }}`, data, function(res)
        {
            motivo_estorno_atualizado = $("#input_editar_entrada_motivo_estorno").val();

            if(res['status'] == 1)
            {
                $("#entrada_motivo_estorno").text(motivo_estorno_atualizado);
                
                $("#input_editar_entrada_motivo_estorno").parent().removeClass('w-100').parent().removeClass('w-100');

            } else {

                alert('Ops! Houve algum problema.');
            }
        });

        return true;
    }

    function updateEntradaVendedor(registro_id)
    {
        let vendedor_nome = $("#entrada_vendedor_nome").text();
        let vendedor_nome_atualizado = $("#input_editar_entrada_vendedor").find('option:selected').text();
        let vendedor_id   = $("#input_editar_entrada_vendedor").val();

        const data = {

            _method: 'PUT',
            _token: '{{ csrf_token() }}',
            registro_id: registro_id,
            vendedor_id: vendedor_id,
            vendedor_nome: vendedor_nome_atualizado,
        }

        $.post(`{{ route('admin.controle-financeiro.lista.editar-vendedor') }}`, data, function(res)
        {
            if(res['status'] == 1)
            {
                $("#entrada_vendedor_nome").text(vendedor_nome_atualizado);
                
            } else {

                alert('Ops! Houve algum problema.');
            }
        });

        return true;
    }

    function updateEntradaUnidade(registro_id)
    {
        let unidade_nome = $("#entrada_unidade_nome").text();
        let unidade_nome_atualizado = $("#input_editar_entrada_unidade").find('option:selected').text();
        let unidade_id   = $("#input_editar_entrada_unidade").val();

        const data = {

            _method: 'PUT',
            _token: '{{ csrf_token() }}',
            registro_id: registro_id,
            unidade_id: unidade_id,
            unidade_nome: unidade_nome_atualizado,
        }

        $.post(`{{ route('admin.controle-financeiro.lista.editar-unidade') }}`, data, function(res)
        {
            if(res['status'] == 1)
            {
                $("#entrada_unidade_nome").text(unidade_nome_atualizado);
                
            } else {

                alert('Ops! Houve algum problema.');
            }
        });

        return true;
    }

    function updateEntradaFormaPagamento(registro_id)
    {
        let forma_pagamento = $("#input_editar_entrada_formapagamento").val();

        const data = {
          _method: 'PUT',
          _token: '{{ csrf_token() }}',
          registro_id: registro_id,
          forma_pagamento: forma_pagamento
        }
        $.post(`{{ route('admin.controle-financeiro.lista.editar-formapagamento') }}`, data, function(res)
        {
            if(res['status'] == 1)
            {
                $("#entrada_forma_pagamento").text($("#input_editar_entrada_formapagamento").val());
                
            } else {

                alert('Ops! Houve algum problema.');
            }
        });

        return true;
    }

    $(".btnResetEditEntrada").click(function(e)
    {
        e.preventDefault();

        $(this).prev().hide();
        $(this).prev().prev().prev().hide();
        $(this).hide();
        $(this).prev().next().hide();
        $(this).prev().prev().show();
        $(this).prev().prev().prev().prev().show();
    });

    $(".btnSaveEditSaida").click(function(e)
    {
        e.preventDefault();

        let registro_id = document.getElementById('dados_e_acoes_saida_id').value;

        let field = $(this).data('field');

        checkValidate = null;

        if(field == 'valor_parcela')
        {
            checkValidate = updateSaidaValor(registro_id);
        }

        if(field == 'referencia')
        {
            checkValidate = updateSaidaReferencia(registro_id);
        }

        if(field == 'link_pagamento')
        {
            checkValidate = updateSaidaLinkPagamento(registro_id);
        }

        if(field == 'forma_pagamento')
        {
            checkValidate = updateSaidaFormaPagamento(registro_id);
        }

        if(field == 'vendedor')
        {
            checkValidate = updateSaidaVendedor(registro_id);
        }

        if(field == 'unidade')
        {
            checkValidate = updateSaidaUnidade(registro_id);
        }

        if(field == 'obs')
        {
            checkValidate = updateSaidaObs(registro_id);
        }

        if(field == 'data_vencimento')
        {
            checkValidate = updateSaidaDataVencimento(registro_id);
        }

        if(field == 'data_recebimento')
        {
            checkValidate = updateSaidaDataRecebimento(registro_id);
        }

        if(field == 'data_estorno')
        {
            checkValidate = updateSaidaDataEstorno(registro_id);
        }

        if(field == 'motivo_estorno')
        {
            checkValidate = updateSaidaMotivoEstorno(registro_id);
        }

        if(checkValidate == true)
        {
            $(this).prev().prev().hide();
            $(this).hide();
            $(this).next().hide();
            $(this).prev().show();
            $(this).prev().prev().prev().show();
        }
    });

    function updateSaidaValor(registro_id)
    {
        let valor = $("#input_editar_saida_valor").val();

        if(valor == '')
        {
            $("#input_editar_saida_valor").addClass('border border-danger').attr('placeholder','Ops! Este campo é obrigatório.');
            return false;
        
        } else {

            $("#input_editar_saida_valor").removeClass('border border-danger').attr('placeholder','R$');
        }

        const data = {
          _method: 'PUT',
          _token: '{{ csrf_token() }}',
          registro_id: registro_id,
          valor: valor
        }
        $.post(`{{ route('admin.controle-financeiro.lista.editar-valor') }}`, data, function(res)
        {
            if(res['status'] == 1)
            {
                var url = new URL(window.location.href);
                url.searchParams.set('saida_id', registro_id);
                url.searchParams.set('entrada_id', '');
                window.location.href = url.href;

                $("#saida_valor").text('Salvando e recalculando...');

            } else {

                alert('Ops! Houve algum problema.');
            }
        });

        return true;
    }

    function updateSaidaLinkPagamento(registro_id)
    {
        let link_pagamento = $("#input_editar_saida_link_pagamento").val();

        const data = {
          _method: 'PUT',
          _token: '{{ csrf_token() }}',
          registro_id: registro_id,
          link_pagamento: link_pagamento
        }
        $.post(`{{ route('admin.controle-financeiro.lista.editar-link') }}`, data, function(res)
        {
            link_atualizado = $("#input_editar_saida_link_pagamento").val();

            if(res['status'] == 1)
            {
                $("#saida_link_pagamento").text('Abrir para ver').attr('href', link_atualizado);
                
            } else {

                alert('Ops! Houve algum problema.');
            }
        });

        return true;
    }

    function updateSaidaDataVencimento(registro_id)
    {
        let data_vencimento = $("#input_editar_saida_data_vencimento").val();

        if(data_vencimento == '')
        {
            $("#input_editar_saida_data_vencimento").addClass('border border-danger').attr('placeholder','Ops! Este campo é obrigatório.');
            return false;
        
        } else {

            $("#input_editar_saida_data_vencimento").removeClass('border border-danger').attr('placeholder','R$');
        }

        const data = {
          _method: 'PUT',
          _token: '{{ csrf_token() }}',
          registro_id: registro_id,
          data_vencimento: data_vencimento
        }
        $.post(`{{ route('admin.controle-financeiro.lista.editar-datavencimento') }}`, data, function(res)
        {
            if(res['status'] == 1)
            {

                data_vencimento = data_vencimento.split("-");
                setAno = data_vencimento[0];
                setMes = data_vencimento[1];

                var url = new URL(window.location.href);
                url.searchParams.set('saida_id', registro_id);
                url.searchParams.set('entrada_id', '');
                url.searchParams.set('ano', setAno);
                url.searchParams.set('mes', setMes);
                window.location.href = url.href;

                $("#saida_data_vencimento").text('Salvando e recalculando...');

            } else {

                alert('Ops! Houve algum problema.');
            }
        });

        return true;
    }

    function updateSaidaDataRecebimento(registro_id)
    {
        let data_recebimento = $("#input_editar_saida_data_recebimento").val();

        const data = {
          _method: 'PUT',
          _token: '{{ csrf_token() }}',
          registro_id: registro_id,
          data_recebimento: data_recebimento
        }
        $.post(`{{ route('admin.controle-financeiro.lista.editar-datarecebimento') }}`, data, function(res)
        {
            if(res['status'] == 1)
            {
                data_recebimento_atualizada = $("#input_editar_saida_data_recebimento").val();

                data_recebimento_atualizada = data_recebimento_atualizada.split("-").reverse().join("/");

                $("#saida_data_recebimento").text(data_recebimento_atualizada);

            } else {

                alert('Ops! Houve algum problema.');
            }
        });

        return true;
    }

    function updateSaidaDataEstorno(registro_id)
    {
        let data_estorno = $("#input_editar_saida_data_estorno").val();

        const data = {
          _method: 'PUT',
          _token: '{{ csrf_token() }}',
          registro_id: registro_id,
          data_estorno: data_estorno
        }
        $.post(`{{ route('admin.controle-financeiro.lista.editar-dataestorno') }}`, data, function(res)
        {
            if(res['status'] == 1)
            {
                data_estorno_atualizada = $("#input_editar_saida_data_estorno").val();

                data_estorno_atualizada = data_estorno_atualizada.split("-").reverse().join("/");

                $("#saida_data_estorno").text(data_estorno_atualizada);

            } else {

                alert('Ops! Houve algum problema.');
            }
        });

        return true;
    }

    function updateSaidaReferencia(registro_id)
    {
        let referencia = $("#input_editar_saida_referencia").val();

        if(referencia == '')
        {
            $("#input_editar_saida_referencia").addClass('border border-danger').attr('placeholder','Ops! Este campo é obrigatório.');
            return false;
        
        } else {

            $("#input_editar_saida_referencia").removeClass('border border-danger').attr('placeholder','R$');
        }

        const data = {
          _method: 'PUT',
          _token: '{{ csrf_token() }}',
          registro_id: registro_id,
          referencia: referencia
        }
        $.post(`{{ route('admin.controle-financeiro.lista.editar-referencia') }}`, data, function(res)
        {
            if(res['status'] == 1)
            {
                $("#saida_referencia").text('okok' + referencia);

            } else {

                alert('Ops! Houve algum problema.');
            }
        });

        return true;
    }
    
    function updateSaidaObs(registro_id)
    {
        let obs = $("#input_editar_saida_obs").val();

        const data = {
          _method: 'PUT',
          _token: '{{ csrf_token() }}',
          registro_id: registro_id,
          obs: obs
        }
        $.post(`{{ route('admin.controle-financeiro.lista.editar-obs') }}`, data, function(res)
        {
            obs_atualizado = $("#input_editar_saida_obs").val();

            if(res['status'] == 1)
            {
                $("#saida_obs").text(obs_atualizado);
                
                $("#input_editar_saida_obs").parent().removeClass('w-100').parent().removeClass('w-100');

            } else {

                alert('Ops! Houve algum problema.');
            }
        });

        return true;
    }

    function updateSaidaMotivoEstorno(registro_id)
    {
        let motivo_estorno = $("#input_editar_saida_motivo_estorno").val();

        const data = {
          _method: 'PUT',
          _token: '{{ csrf_token() }}',
          registro_id: registro_id,
          motivo_estorno: motivo_estorno
        }
        $.post(`{{ route('admin.controle-financeiro.lista.editar-motivoestorno') }}`, data, function(res)
        {
            motivo_estorno_atualizado = $("#input_editar_saida_motivo_estorno").val();

            if(res['status'] == 1)
            {
                $("#saida_motivo_estorno").text(motivo_estorno_atualizado);
                
                $("#input_editar_saida_motivo_estorno").parent().removeClass('w-100').parent().removeClass('w-100');

            } else {

                alert('Ops! Houve algum problema.');
            }
        });

        return true;
    }

    function updateSaidaVendedor(registro_id)
    {
        let vendedor_nome = $("#saida_vendedor_nome").text();
        let vendedor_nome_atualizado = $("#input_editar_saida_vendedor").find('option:selected').text();
        let vendedor_id   = $("#input_editar_saida_vendedor").val();

        const data = {

            _method: 'PUT',
            _token: '{{ csrf_token() }}',
            registro_id: registro_id,
            vendedor_id: vendedor_id,
            vendedor_nome: vendedor_nome_atualizado,
        }

        $.post(`{{ route('admin.controle-financeiro.lista.editar-vendedor') }}`, data, function(res)
        {
            if(res['status'] == 1)
            {
                $("#saida_vendedor_nome").text(vendedor_nome_atualizado);
                
            } else {

                alert('Ops! Houve algum problema.');
            }
        });

        return true;
    }

    function updateSaidaUnidade(registro_id)
    {
        let unidade_nome = $("#saida_unidade_nome").text();
        let unidade_nome_atualizado = $("#input_editar_saida_unidade").find('option:selected').text();
        let unidade_id   = $("#input_editar_saida_unidade").val();

        const data = {

            _method: 'PUT',
            _token: '{{ csrf_token() }}',
            registro_id: registro_id,
            unidade_id: unidade_id,
            unidade_nome: unidade_nome_atualizado,
        }

        $.post(`{{ route('admin.controle-financeiro.lista.editar-unidade') }}`, data, function(res)
        {
            if(res['status'] == 1)
            {
                $("#saida_unidade_nome").text(unidade_nome_atualizado);
                
            } else {

                alert('Ops! Houve algum problema.');
            }
        });

        return true;
    }

    function updateSaidaFormaPagamento(registro_id)
    {
        let forma_pagamento = $("#input_editar_saida_formapagamento").val();

        const data = {
          _method: 'PUT',
          _token: '{{ csrf_token() }}',
          registro_id: registro_id,
          forma_pagamento: forma_pagamento
        }
        $.post(`{{ route('admin.controle-financeiro.lista.editar-formapagamento') }}`, data, function(res)
        {
            if(res['status'] == 1)
            {
                $("#saida_forma_pagamento").text($("#input_editar_saida_formapagamento").val());
                
            } else {

                alert('Ops! Houve algum problema.');
            }
        });

        return true;
    }

    $(".btnResetEditSaida").click(function(e)
    {
        e.preventDefault();

        $(this).prev().hide();
        $(this).prev().prev().prev().hide();
        $(this).hide();
        $(this).prev().next().hide();
        $(this).prev().prev().show();
        $(this).prev().prev().prev().prev().show();
    });

    function setRegraMaskDesconto(value)
    {
        tipo = document.getElementById('add_entrada_tipo_desconto').value;
        
        if(tipo == '%')
        {
            valor_desconto = document.getElementById('add_entrada_desconto').value;

            if(parseInt(valor_desconto) > 100)
            {
                $("#add_entrada_desconto").val('100');
            }
        }
    }

    function setMaskTipoDesconto(value)
    {   
        $("#add_entrada_desconto").unmask();

        if(value == '%')
        {
            $("#add_entrada_desconto").mask('000');
            
            valor_desconto = document.getElementById('add_entrada_desconto').value;
            
            if(parseInt(valor_desconto) > 100)
            {
                $("#add_entrada_desconto").val('100');
            }

        } else {

            $("#add_entrada_desconto").mask('#.##0,00', {
              reverse: true
            });
        }
    }

    function setCamposToDeleteEntrada()
    {
        document.getElementById('deletar_entrada_id').value = document.getElementById('dados_e_acoes_entrada_id').value;
        document.getElementById('deletar_entrada_nome').innerHTML = document.getElementById('entrada_cliente_nome').innerHTML;
        document.getElementById('deletar_entrada_valor').innerHTML = document.getElementById('entrada_valor').innerHTML;
        document.getElementById('deletar_entrada_others_parcelas').innerHTML = textoParcelaToDeleteEntrada;
    }

    function setCamposToDeleteSaida()
    {
        document.getElementById('deletar_saida_id').value = document.getElementById('dados_e_acoes_saida_id').value;
        document.getElementById('deletar_saida_nome').innerHTML = document.getElementById('saida_referencia').innerHTML;
        document.getElementById('deletar_saida_valor').innerHTML = document.getElementById('saida_valor').innerHTML;
        document.getElementById('deletar_saida_others_parcelas').innerHTML = textoParcelaToDeleteSaida;
    }

    function getDadosEntradaById(entrada_id)
    {
        $("#boxLoadingEntrada").show();
        $("#boxEntradaEstornarPagamento").hide();
        $("#boxEntradaConcluirPagamento").hide();
        $("#boxEntradaDataRecebimento").hide().removeClass('d-flex');
        $("#boxEntradaDataEstorno").hide().removeClass('d-flex');
        $("#btn-conversar").hide();
        $("#entrada_cliente_id").val('');
        $("#entrada_cliente_nome").html('');
        $("#entrada_produto_nome").html('');
        $("#entrada_valor").html('');
        $("#entrada_forma_pagamento").html('');
        $("#entrada_link_pagamento").html('');
        $("#entrada_vendedor_nome").html('');
        $("#entrada_unidade_nome").html('');
        $("#entrada_obs").html('');
        $("#boxEntradaMotivoEstorno").hide().removeClass('d-flex');
        $("#entrada_motivo_estorno").html('');
        $("#entrada_status").html('');
        $("#entrada_data_vencimento").html('');
        $("#entrada_data_recebimento").html('');
        $("#entrada_data_estorno").html('');

        @if(\App\Models\Unidade::checkActive() === false || count($unidades) == 0)
            $("#boxEntradaUnidade").hide().removeClass('d-flex');
        @endif

        textoParcelaToDeleteEntrada = null;

        var url = "{{ route('admin.controle-financeiro.lista.fluxo-caixa.getDadosEntradaById') }}";

        document.getElementById('dados_e_acoes_entrada_id').value = entrada_id;

        var dadosGET = { entrada_id: entrada_id }

        $.ajax({

            type: 'GET',
            dataType: "json",
            contentType: 'application/json',
            url: url,
            data: dadosGET,

            success: function(data)
            {
                console.log(data);

                if(data.registro_id == null && data.n_parcelas > 1)
                {
                    textoParcelaToDeleteEntrada = parseInt(data.n_parcelas) - 1 + ' outras parcelas futuras também serão deletadas.';
                }

                if(data.tipo_produto == 'C')
                {
                    linkProduto = '/admin/turma/info/' + data.produto_id;

                }
                
                if(data.tipo_produto == 'T')
                {
                    linkProduto = '/admin/trilha/info/' + data.produto_id;
                }

                if(data.tipo_produto == 'P')
                {
                    linkProduto = '/admin/produto/info/' + data.produto_id;
                }

                if(data.tipo_produto == 'A')
                {
                    linkProduto = '/admin/plano/info/' + data.produto_id;
                }

                if(data.tipo_produto == 'S')
                {
                    linkProduto = '/admin/servico/info/' + data.produto_id;
                }

                $("#entrada_cliente_id").val(data.cliente_id);
                $("#entrada_cliente_nome").html(data.cliente_nome).attr('href','/admin/aluno/info/' + data.cliente_id);
                $("#entrada_produto_nome").html(data.produto_nome).attr('href',linkProduto);
                $("#entrada_valor").html(data.valor_parcela);
                $("#entrada_forma_pagamento").html(data.forma_pagamento ?? '<span class="text-secondary">Não definido</span>');
                
                if(data.link)
                {
                    $("#entrada_link_pagamento").attr('href', data.link);
                
                } else {

                    $("#entrada_link_pagamento").html('<span class="text-secondary">Não definido</span>');
                }


                data_vencimento = data.data_vencimento;
                data_vencimento = data_vencimento.split("-").reverse().join("/");

                $("#entrada_data_vencimento").html(data_vencimento ?? '<span class="text-secondary">Não definido</span>');

                $("#entrada_vendedor_nome").html(data.vendedor_nome ?? '<span class="text-secondary">Não definido</span>').attr('href','/admin/colaborador/info/' + data.vendedor_id);
                $("#entrada_vendedor_nome").attr('data-vendedor', data.vendedor_id ?? 0);

                $("#entrada_unidade_nome").html(data.unidade_nome ?? '<span class="text-secondary">Não definido</span>').attr('href','/admin/unidade/info/' + data.unidade_id);
                $("#entrada_unidade_nome").attr('data-unidade', data.unidade_id ?? 0);

                $("#entrada_obs").html(data.obs ?? '<span class="text-secondary">Não definido</span>');

                $("#btn-conversar").show();
                $("#btn-conversar").attr('href','/admin/controle-financeiro/lista/fluxo-caixa/conversar/'+data.cliente_id);
                $("#entrada_status").html(data.status_label);

                if(data.status == 0)
                {
                    $("#boxModalFooterAcoesEntrada").show();
                    $("#boxEntradaEstornarPagamento").hide();
                    $("#boxEntradaConcluirPagamento").show();
                    $("#boxEntradaDataRecebimento").hide().removeClass('d-flex');
                    $("#boxEntradaDataEstorno").hide().removeClass('d-flex');
                    $("#boxEntradaMotivoEstorno").hide().removeClass('d-flex');
                }

                if(data.status == 1)
                {
                    $("#boxModalFooterAcoesEntrada").show();
                    $("#boxEntradaEstornarPagamento").show();
                    $("#boxEntradaConcluirPagamento").hide();
                    $("#boxEntradaDataRecebimento").show().addClass('d-flex');
                    $("#boxEntradaDataEstorno").hide().removeClass('d-flex');

                    data_recebimento = data.data_recebimento;
                    data_recebimento = data_recebimento.split("-").reverse().join("/");

                    $("#entrada_data_recebimento").html(data_recebimento);
                    $("#boxEntradaMotivoEstorno").hide().removeClass('d-flex');;
                }

                if(data.status == 2)
                {
                    $("#boxModalFooterAcoesEntrada").hide();
                    $("#boxEntradaDataRecebimento").show().addClass('d-flex');
                    $("#boxEntradaDataEstorno").show().addClass('d-flex');

                    if(data.data_estorno)
                    {
                        data_estorno = data.data_estorno;
                        data_estorno = data_estorno.split('-').reverse().join('/');
                        $("#entrada_data_estorno").html(data_estorno);
                    
                    } else {

                        $("#entrada_data_estorno").html('<span class="text-secondary">Não definido</span>');
                    }

                    $("#boxEntradaDataEstorno").show();


                    data_recebimento = data.data_recebimento;
                    data_recebimento = data_recebimento.split("-").reverse().join("/");

                    $("#entrada_data_recebimento").html(data_recebimento);

                    $("#boxEntradaMotivoEstorno").show().addClass('d-flex');;
                    $("#entrada_motivo_estorno").html(data.motivo_estorno ?? '<span class="text-secondary">Não definido</span>');
                }

                $("#boxLoadingEntrada").hide();
            },

            error: function(data) {
                data = [{ "tipo": "Sem Resultados", "total": 0 }];
            }
        });
    }

    function getDadosSaidaById(saida_id)
    {
        $("#boxLoadingSaida").show();
        $("#boxSaidaEstornarPagamento").hide();
        $("#boxSaidaConcluirPagamento").hide();
        $("#boxSaidaDataRecebimento").hide().removeClass('d-flex');
        $("#boxSaidaDataEstorno").hide().removeClass('d-flex');
        $("#saida_valor").html('');
        $("#saida_referencia").html('');
        $("#saida_forma_pagamento").html('');
        $("#saida_link_pagamento").html('');
        $("#saida_unidade_nome").html('');
        $("#saida_obs").html('');
        $("#boxSaidaMotivoEstorno").hide().removeClass('d-flex');
        $("#saida_motivo_estorno").html('');
        $("#saida_status").html('');
        $("#saida_data_vencimento").html('');
        $("#saida_data_recebimento").html('');
        $("#saida_data_estorno").html('');

        @if(\App\Models\Unidade::checkActive() === false || count($unidades) == 0)
            $("#boxSaidaUnidade").hide().removeClass('d-flex');
        @endif

        textoParcelaToDeleteSaida = null;

        var url = "{{ route('admin.controle-financeiro.lista.fluxo-caixa.getDadosSaidaById') }}";

        document.getElementById('dados_e_acoes_saida_id').value = saida_id;

        var dadosGET = { saida_id: saida_id }

        $.ajax({

            type: 'GET',
            dataType: "json",
            contentType: 'application/json',
            url: url,
            data: dadosGET,

            success: function(data)
            {
                console.log(data);

                if(data.registro_id == null && data.n_parcelas > 1)
                {
                    textoParcelaToDeleteSaida = parseInt(data.n_parcelas) - 1 + ' outras parcelas futuras também serão deletadas.';
                }

                if(data.tipo_produto == 'C')
                {
                    linkProduto = '/admin/turma/info/' + data.produto_id;

                } else {

                    linkProduto = '/admin/trilha/info/' + data.produto_id;
                }

                $("#saida_valor").html(data.valor_parcela);
                $("#saida_referencia").html(data.referencia);
                $("#saida_forma_pagamento").html(data.forma_pagamento ?? '<span class="text-secondary">Não definido</span>');
                
                if(data.link)
                {
                    $("#saida_link_pagamento").attr('href', data.link);
                
                } else {

                    $("#saida_link_pagamento").html('<span class="text-secondary">Não definido</span>');
                }


                data_vencimento = data.data_vencimento;
                data_vencimento = data_vencimento.split("-").reverse().join("/");

                $("#saida_data_vencimento").html(data_vencimento ?? '<span class="text-secondary">Não definido</span>');

                $("#saida_unidade_nome").html(data.unidade_nome ?? '<span class="text-secondary">Não definido</span>').attr('href','/admin/unidade/info/' + data.unidade_id);
                $("#saida_unidade_nome").attr('data-unidade', data.unidade_id ?? 0);

                $("#saida_obs").html(data.obs ?? '<span class="text-secondary">Não definido</span>');

                $("#saida_status").html(data.status_label);

                if(data.status == 0)
                {
                    $("#boxModalFooterAcoesSaida").show();
                    $("#boxSaidaEstornarPagamento").hide();
                    $("#boxSaidaConcluirPagamento").show();
                    $("#boxSaidaDataRecebimento").hide().removeClass('d-flex');
                    $("#boxSaidaDataEstorno").hide().removeClass('d-flex');
                    $("#boxSaidaMotivoEstorno").hide().removeClass('d-flex');
                }

                if(data.status == 1)
                {
                    $("#boxModalFooterAcoesSaida").show();
                    $("#boxSaidaEstornarPagamento").show();
                    $("#boxSaidaConcluirPagamento").hide();
                    $("#boxSaidaDataRecebimento").show().addClass('d-flex');
                    $("#boxSaidaDataEstorno").hide().removeClass('d-flex');

                    data_recebimento = data.data_recebimento;
                    data_recebimento = data_recebimento.split("-").reverse().join("/");

                    $("#saida_data_recebimento").html(data_recebimento);
                    $("#boxSaidaMotivoEstorno").hide().removeClass('d-flex');;
                }

                if(data.status == 2)
                {
                    $("#boxModalFooterAcoesSaida").hide();
                    $("#boxSaidaDataRecebimento").show().addClass('d-flex');
                    $("#boxSaidaDataEstorno").show().addClass('d-flex');

                    data_estorno = data.data_estorno;
                    data_estorno = data_estorno.split('-').reverse().join('/');
                    
                    $("#boxSaidaDataEstorno").show();

                    $("#saida_data_estorno").html(data_estorno);

                    data_recebimento = data.data_recebimento;
                    data_recebimento = data_recebimento.split("-").reverse().join("/");

                    $("#saida_data_recebimento").html(data_recebimento);

                    $("#boxSaidaMotivoEstorno").show().addClass('d-flex');;
                    $("#saida_motivo_estorno").html(data.motivo_estorno ?? '<span class="text-secondary">Não definido</span>');
                }

                $("#boxLoadingSaida").hide();
            },

            error: function(data) {
                data = [{ "tipo": "Sem Resultados", "total": 0 }];
            }
        });
    }

    function setAddEntradaClienteNome(el)
    {
        nome  = $(el).find('option:selected').text();

        document.getElementById('add_entrada_cliente_nome').value = nome.trim();
    }

    function setAddEntradaVendedorNome(el)
    {
        nome  = $(el).find('option:selected').text();
        nome  = nome.trim();

        if(nome == 'Selecione')
        {
            nome = '';
        }

        document.getElementById('add_entrada_vendedor_nome').value = nome;
    }

    function setAddEntradaUnidadeNome(el)
    {
        nome = $(el).find('option:selected').text();

        document.getElementById('add_entrada_unidade_nome').value = nome.trim();
    }

    function setAddEntradaProdutoNomeAndValorAndReferencia(el)
    {
        valor = $(el).find('option:selected').data("valor");

        produto_nome  = $(el).find('option:selected').text();
        produto_nome  = produto_nome.trim();

        cliente_nome  = document.getElementById('add_entrada_cliente_nome').value;

        document.getElementById('add_entrada_produto_nome').value = produto_nome;
        document.getElementById('add_entrada_valor').value = valor.trim();
    }

    function rowParcelaEntrada(data)
    {
        let row = `
            <div class="mb-2 d-flex align-items-center parcelas w-100">

                <div class="me-2 border bg-white rounded px-3 text-center flex-center w-lg-20" style="height: 42.5px;">
                    ${data.n_parcela}
                </div>

                <input type="hidden" name="add_entrada_n_parcela[]" value="${data.n_parcela}">

                <input type="date" name="add_entrada_datas_vencimento[]" class="w-100 me-2 form-control p-form" required value="${data.data_vencimento}">
                
                <input type="text" name="add_entrada_valores[]" class="w-100 me-2 mask_valor form-control p-form" placeholder="R$" required value="${data.valor_parcela}">
                
                <input type="text" name="add_entrada_links[]" class="w-100 me-2 form-control p-form" placeholder="Link de Pagamento">
                
                <select name="add_entrada_formas_pagamento[]" class="w-100 select-form" id="appended_forma_pagamento_add_entrada_${data.n_parcela}">
                    <option value="">Selecione</option>
                    <option value="Dinheiro">Dinheiro</option>
                    <option value="Cartão de débito">Cartão de débito</option>
                    <option value="Cartão de crédito">Cartão de crédito</option>
                    <option value="Boleto bancário">Boleto bancário</option>
                    <option value="Pix">Pix</option>
                    <option value="TED">TED</option>
                    <option value="DOC">DOC</option>
                </select>

            </div>
        `;

        $("#appendParcelaAddEntrada").append(row);

        document.getElementById('appended_forma_pagamento_add_entrada_'+data.n_parcela).value = data.forma_pagamento;
    }

    function gerarParcelasAddEntrada(n_parcelas)
    {
        $("#appendParcelaAddEntrada").empty();

        if(n_parcelas[0] === '0')
        {
            document.getElementById('add_entrada_n_parcelas').value = '1';
        }

        if(n_parcelas > 1)
        {          
            document.getElementById('boxParcelasAddEntrada').style.display = 'block';

            let data = [];
            let data_vencimento = document.getElementById('add_entrada_data_vencimento').value;
            let valor_total     = document.getElementById('add_entrada_valor').value;
            let forma_pagamento = document.getElementById('add_entrada_forma_pagamento').value;

            valor_total = valor_total.split('.').join('').split(',').join('.');
    
            valor_parcela = valor_total / parseInt(n_parcelas);
            valor_parcela = valor_parcela.toLocaleString(undefined, {minimumFractionDigits: 2});

            document.getElementById('add_entrada_valor').value = valor_parcela;

            data['valor_parcela']   = valor_parcela;
            data['forma_pagamento'] = forma_pagamento;

            var now = moment(data_vencimento, 'YYYY-MM-DD', true);

            let n_parcela = 1;

            for (var i = n_parcelas - 1; i > 0; i--)
            {
                n_parcela++;

                data['data_vencimento'] = now.add(1, 'M').format('YYYY-MM-DD');
                data['n_parcela'] = n_parcela;

                rowParcelaEntrada(data);
            }

        } else {

            document.getElementById('boxParcelasAddEntrada').style.display = 'none';

            if(n_parcelas <= 0)
            {
                document.getElementById('add_entrada_n_parcelas').value = '1';
            }
        }
    }



    function setAddSaidaUnidadeNome(el)
    {
        nome = $(el).find('option:selected').text();

        document.getElementById('add_saida_unidade_nome').value = nome.trim();
    }

    function setAddSaidaProdutoNomeAndValorAndReferencia(el)
    {
        valor = $(el).find('option:selected').data("valor");

        produto_nome  = $(el).find('option:selected').text();
        produto_nome  = produto_nome.trim();

        document.getElementById('add_saida_produto_nome').value = produto_nome;
        document.getElementById('add_saida_referencia').value = produto_nome;
        document.getElementById('add_saida_valor').value = valor.trim();
    }

    function rowParcelaSaida(data)
    {
        let row = `
            <div class="mb-2 d-flex align-items-center parcelas w-100">

                <div class="me-2 border bg-white rounded px-3 text-center flex-center w-lg-20" style="height: 42.5px;">
                    ${data.n_parcela}
                </div>

                <input type="hidden" name="add_saida_n_parcela[]" value="${data.n_parcela}">

                <input type="date" name="add_saida_datas_vencimento[]" class="w-100 me-2 form-control p-form" required value="${data.data_vencimento}">
                
                <input type="text" name="add_saida_valores[]" class="w-100 me-2 mask_valor form-control p-form" placeholder="R$" required value="${data.valor_parcela}">
                
                <input type="text" name="add_saida_links[]" class="w-100 me-2 form-control p-form" placeholder="Link de Pagamento">
                
                <select name="add_saida_formas_pagamento[]" class="w-100 select-form" id="appended_forma_pagamento_add_saida_${data.n_parcela}">
                    <option value="">Selecione</option>
                    <option value="Dinheiro">Dinheiro</option>
                    <option value="Cartão de débito">Cartão de débito</option>
                    <option value="Cartão de crédito">Cartão de crédito</option>
                    <option value="Boleto bancário">Boleto bancário</option>
                    <option value="Pix">Pix</option>
                    <option value="TED">TED</option>
                    <option value="DOC">DOC</option>
                </select>

            </div>
        `;

        $("#appendParcelaAddSaida").append(row);

        document.getElementById('appended_forma_pagamento_add_saida_'+data.n_parcela).value = data.forma_pagamento;
    }

    function gerarParcelasAddSaida(n_parcelas)
    {
        $("#appendParcelaAddSaida").empty();

        if(n_parcelas[0] === '0')
        {
            document.getElementById('add_saida_n_parcelas').value = '1';
        }

        if(n_parcelas > 1)
        {          
            document.getElementById('boxParcelasAddSaida').style.display = 'block';

            let data = [];
            let data_vencimento = document.getElementById('add_saida_data_vencimento').value;
            let valor_total     = document.getElementById('add_saida_valor').value;
            let forma_pagamento = document.getElementById('add_saida_forma_pagamento').value;

            valor_total = valor_total.split('.').join('').split(',').join('.');
    
            valor_parcela = valor_total / parseInt(n_parcelas);
            valor_parcela = valor_parcela.toLocaleString(undefined, {minimumFractionDigits: 2});

            document.getElementById('add_saida_valor').value = valor_parcela;

            data['valor_parcela']   = valor_parcela;
            data['forma_pagamento'] = forma_pagamento;

            var now = moment(data_vencimento, 'YYYY-MM-DD', true);

            let n_parcela = 1;

            for (var i = n_parcelas - 1; i > 0; i--)
            {
                n_parcela++;

                data['data_vencimento'] = now.add(1, 'M').format('YYYY-MM-DD');
                data['n_parcela'] = n_parcela;

                rowParcelaSaida(data);
            }

        } else {

            document.getElementById('boxParcelasAddSaida').style.display = 'none';

            if(n_parcelas <= 0)
            {
                document.getElementById('add_saida_n_parcelas').value = '1';
            }
        }
    }

    $(function()
    {
        $(".mask_valor").mask('#.##0,00', {
          reverse: true
        });
    });

    $(function()
    {
        $(".mask_porcentagem").mask('#.##0,00', {
          reverse: true
        });
    });

    $('.btnTabEntrada').click(function(e) {

        e.preventDefault();

        $('#tabSaidas').removeClass('show');
        $('.btnTabSaida').attr('aria-expanded', false);

    });

    $('.btnTabSaida').click(function(e) {

        e.preventDefault();

        $('#tabEntradas').removeClass('show');
        $('.btnTabEntrada').attr('aria-expanded', false);

    });


    $(".number").mask('000');


</script>

<script>

    var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
    var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
      return new bootstrap.Tooltip(tooltipTriggerEl)
    });

</script>

@if(isset($_GET['add_entrada']) && !isset($_GET['entrada_id']))
<script> 
    var myModal = new bootstrap.Modal(document.getElementById("ModalAddEntrada"), {});
    document.onreadystatechange = function () {
      myModal.show();
    };
</script>
@endif

@if(isset($_GET['entrada_id']) && $_GET['entrada_id'] != '' && $_GET['entrada_id'] != NULL)
<script> 
    
    var myModal = new bootstrap.Modal(document.getElementById("ModalDadosEAcoesEntrada"), {});
    document.onreadystatechange = function () {
      myModal.show();
    };

    getDadosEntradaById({{$_GET['entrada_id']}});

</script>
@endif

@if(isset($_GET['saida_id']) && $_GET['saida_id'] != '' && $_GET['saida_id'] != NULL)
<script> 
    
    var myModal = new bootstrap.Modal(document.getElementById("ModalDadosEAcoesSaida"), {});
    document.onreadystatechange = function () {
      myModal.show();
    };

    getDadosSaidaById({{$_GET['saida_id']}});

</script>
@endif

@endpush