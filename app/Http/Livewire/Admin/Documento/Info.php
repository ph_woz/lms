<?php

namespace App\Http\Livewire\Admin\Documento;

use Livewire\Component;
use App\Models\User;
use App\Models\Documento;
use App\Models\Categoria;
use App\Models\DocumentoCategoria;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Http;

class Info extends Component
{
    public $documento;
    public $cadastrante;
    public $categorias;
    public $password_to_delete;

    public function mount($documento_id)
    {
        $this->documento = Documento::plataforma()->find($documento_id);

        $this->cadastrante = User::plataforma()->where('id', $this->documento->cadastrante_id)->first();

        $categoriasIds = DocumentoCategoria::plataforma()->where('documento_id', $this->documento->id)->get()->pluck('categoria_id')->toArray();
        $this->categorias = Categoria::plataforma()->ativo()->whereIn('id', $categoriasIds)->select('nome')->orderBy('nome')->get()->pluck('nome')->toArray();
    }

    public function render()
    {
        return view('livewire.admin.documento.info');
    }

    public function delete()
    {
        if(\Auth::attempt(['id' => \Auth::id(), 'password' => $this->password_to_delete, 'status' => 0]))
        {
            DocumentoCategoria::plataforma()->where('documento_id', $this->documento->id)->delete();

            $path = str_replace(\App\Models\Util::getLinkStorage(), '', $this->documento->link);
            Storage::delete($path);

            $this->documento->delete();

            session()->flash('success', 'Documento deletado com êxito.');
            
            return redirect(route('admin.documento.lista'));

        } else {

            session()->flash('danger-livewire', 'Senha incorreta.');

            $this->dispatchBrowserEvent('toast');
        }
    }
}
