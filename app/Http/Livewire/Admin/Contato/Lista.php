<?php

namespace App\Http\Livewire\Admin\Contato;

use Livewire\WithPagination;
use Livewire\Component;
use App\Models\Util;
use App\Models\Formulario;
use App\Models\Curso;
use App\Models\Turma;
use App\Models\Trilha;
use App\Models\Contato;
use App\Models\Categoria;
use App\Models\ContatoCategoria;
use App\Models\Unidade;

class Lista extends Component
{
    use WithPagination;

    protected $paginationTheme = 'bootstrap';

    public $search;
    public $ano;
    public $mes;
    public $tipo;
    public $status;
    public $arquivado = 'N';
    public $ordem = 'desc';

    public $formulario_id;
    public $curso_id;
    public $turma_id;
    public $trilha_id;
    public $unidade_id;

    protected $queryString = [
        'search',
        'ano',
        'mes',
        'tipo',
        'status',
        'arquivado',
        'ordem',
        'formulario_id',
        'curso_id',
        'turma_id',
        'trilha_id',
        'unidade_id',
        'categoriasSelected'
    ];

    public $turmas = [];
    public $categoriasSelected = [];
    

    public function updated()
    {
        if($this->curso_id)
        {
            $this->turmas = Turma::plataforma()->ativo()->where('curso_id', $this->curso_id)->select('id','nome')->orderBy('nome')->get();
        }
    }

    public function render()
    {
        $contatos = Contato::select('id','assunto','nome','email','created_at')->plataforma();

        if($this->search)
        {
            $contatos->where(function ($query)
            {
                $query
                    ->orWhere('nome', 'like', '%'.$this->search.'%')
                    ->orWhere('email', 'like', '%'.$this->search.'%')
                    ->orWhere('cpf', 'like', '%'.$this->search.'%')
                    ->orWhere('assunto', 'like', '%'.$this->search.'%');
            });
        }

        if($this->ano)
        {
            $contatos = $contatos->whereYear('created_at', $this->ano);
        }

        if($this->mes)
        {
            $contatos = $contatos->whereMonth('created_at', $this->mes);
        }

        if($this->tipo)
        {
            $contatos = $contatos->where('tipo', $this->tipo);
        }

        if($this->curso_id)
        {
            $contatos = $contatos->where('curso_id', $this->curso_id);
        }

        if($this->turma_id)
        {
            $contatos = $contatos->where('turma_id', $this->turma_id);
        }

        if($this->trilha_id)
        {
            $contatos = $contatos->where('trilha_id', $this->trilha_id);
        }

        if($this->formulario_id)
        {
            $contatos = $contatos->where('formulario_id', $this->formulario_id);
        }

        if($this->arquivado)
        {
            $contatos = $contatos->where('arquivado', $this->arquivado);
        }

        if(\Auth::user()->restringir_unidade == 'S' && \Auth::user()->unidade_id != null)
        {
            $contatos = $contatos->where('unidade_id', \Auth::user()->unidade_id);
        }

        if($this->unidade_id)
        {
            $contatos = $contatos->where('unidade_id', $this->unidade_id);
        }


        if($this->categoriasSelected != null)
        {
            $contatosIds = ContatoCategoria::plataforma()->whereIn('categoria_id', $this->categoriasSelected ?? array())->get()->pluck('contato_id')->toArray();
            $contatos = $contatos->whereIn('id', $contatosIds);
        }


        $countPendentes   = clone $contatos;
        $countEmAnalise   = clone $contatos;
        $countAtendidas   = clone $contatos;
        $countDesativados = clone $contatos;
        
        $countPendentes   = $countPendentes->where('status', 0)->count();
        $countEmAnalise   = $countEmAnalise->where('status', 1)->count();
        $countAtendidas   = $countAtendidas->where('status', 2)->count();
        $countDesativados = $countDesativados->where('status', 3)->count();

        $countContatos = $contatos->count();

        if($this->status !== null)
        {
            $contatos = $contatos->where('status', $this->status);
        }

        $contatos = $contatos->orderBy('id', $this->ordem)->paginate(10);


        $formularios = Formulario::plataforma()->ativo()->orderBy('referencia')->get();
        $categorias  = Categoria::plataforma()->ativo()->orderBy('nome')->get();
        $cursos      = Curso::plataforma()->ativo()->orderBy('nome')->get();
        $trilhas     = Trilha::plataforma()->ativo()->orderBy('referencia')->get();
        $unidades    = Unidade::plataforma()->ativo()->orderBy('nome')->get();
        $meses       = Util::getMeses();

        return view('livewire.admin.contato.lista', [
            'contatos'         => $contatos,
            'countContatos'    => $countContatos,
            'formularios'      => $formularios,
            'categorias'       => $categorias,
            'cursos'           => $cursos,
            'turmas'           => $this->turmas,
            'trilhas'          => $trilhas,
            'unidades'         => $unidades,
            'meses'            => $meses,
            'countPendentes'   => $countPendentes,
            'countEmAnalise'   => $countEmAnalise,
            'countAtendidas'   => $countAtendidas,
            'countDesativados' => $countDesativados,
        ]);
    }
}
