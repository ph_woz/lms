<?php

namespace App\Http\Livewire\Admin\Ajuda\Chamado;

use Livewire\Component;
use App\Models\Chamado;
use Illuminate\Support\Facades\Http;
use Mail;
use App\Mail\EmailNotificaChamadoWozcode;

class Add extends Component
{

    public $chamado;
    public $assunto;
    public $descricao;

    protected $rules = [
        'assunto'   => 'required',
        'descricao' => 'required',
    ];

    protected $messages = [
        'assunto.required'   => 'O Assunto não pode estar vazio.',
        'descricao.required' => 'A Descrição não pode estar vazio.',
    ];

    public function add()
    {
        $this->validate();

        $this->chamado = Chamado::create([
            'assunto'    => $this->assunto,
            'descricao'  => $this->descricao,
        ]);

        session()->flash('success-livewire', 'Chamado criado com êxito. Responderemos em breve.');
        
        $this->resetFields();

        $this->dispatchBrowserEvent('toast');

        Mail::to('pedrohenrique1207ph@gmail.com')->send(new EmailNotificaChamadoWozcode($this->chamado, \Auth::user()->nome));
    }

    public function resetFields()
    {
        $this->assunto   = null;
        $this->descricao = null;
    }

    public function render()
    {
        return view('livewire.admin.ajuda.chamado.add');
    }
}
