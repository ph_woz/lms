<?php

namespace App\Http\Controllers\SiteProprio;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Banner;
use App\Models\PaginaConteudo;
use App\Models\Curso;

class EducandarioMaxController extends Controller
{
    public function index()
    {
        $seo = PaginaConteudo::plataforma()->where('url','/')->select('plataforma_id','seo_title','seo_keywords','seo_description')->first();

        $banners = Banner::plataforma()->ativo()->orderBy('ordem')->get();
        $cursos = Curso::plataforma()->ativo()->publico()->orderBy('nome')->get();

        if(request('sentContato'))
        {
            if(request('check'))
                return back()->with('danger', 'SPAM Detected!');

            \App\Models\Contato::create(request()->all() + [
                'plataforma_id' => $seo->plataforma_id,
                'tipo' => 'L',
            ]);
            
            return redirect('/#pre-matricula')->with('success_prematricula', 'Inscrição enviada com êxito.');
        }

    	return view('sites-proprios.educandariomax.home', compact('seo','banners','cursos') );
    }

    public function curso($slug)
    {
        $curso = \App\Models\Curso::plataforma()->publico()->where('slug', $slug)->firstOrFail();

        $turmas = \App\Models\Turma::plataforma()->ativo()->publico()->where('curso_id', $curso->id)->get();

        return view('sites-proprios.educandariomax.curso', compact('curso','turmas') );
    }

}