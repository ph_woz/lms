<?php

namespace App\Http\Livewire\Admin\Responsavel;

use Livewire\WithPagination;
use Livewire\Component;
use App\Models\Responsavel;
use App\Models\ResponsavelAluno;
use App\Models\User;

class Lista extends Component
{
    use WithPagination;

    protected $paginationTheme = 'bootstrap';

    public $search;
    public $status = 0;
    public $ordem;

    protected $queryString = ['search','status','ordem'];

    public $add_responsaveis_para_aluno_id;
    public $add_responsavel_id;
    public $add_responsavel_grau_parentesco;


    public function mount()
    {
        $this->add_responsaveis_para_aluno_id = $_GET['add_responsaveis_para_aluno_id'] ?? null;
    }

    public function render()
    {
        $responsaveis = Responsavel::plataforma();

        if($this->search)
            $responsaveis = $responsaveis->where('nome', 'like', '%'.$this->search.'%');

        if($this->status)
            $responsaveis = $responsaveis->where('status', $this->status);


        if($this->add_responsaveis_para_aluno_id)
        {
            $responsaveisIdsJaAdicionados = ResponsavelAluno::plataforma()->where('aluno_id', $this->add_responsaveis_para_aluno_id)->select('responsavel_id')->get()->pluck('responsavel_id')->toArray();
            $responsaveis = $responsaveis->whereNotIn('id', $responsaveisIdsJaAdicionados);
        }

        $countResponsaveis = $responsaveis->count();


        if($this->ordem == 'desc' || $this->ordem == 'asc')
            $responsaveis = $responsaveis->orderBy('id', $this->ordem);
        else
            $responsaveis = $responsaveis->orderBy('nome');
        

        $responsaveis = $responsaveis->paginate(10);


        $aluno = User::plataforma()->ativo()->aluno()->where('id', $this->add_responsaveis_para_aluno_id)->first();


        return view('livewire.admin.responsavel.lista', [
            'responsaveis'      => $responsaveis,
            'countResponsaveis' => $countResponsaveis,
            'aluno'             => $aluno,
        ]);
    }


    public function setResponsavelId($responsavel_id)
    {
        $this->add_responsavel_id = $responsavel_id;
    }

    public function addResponsavelParaAluno()
    {
        ResponsavelAluno::create([
            'aluno_id'        => $this->add_responsaveis_para_aluno_id, 
            'responsavel_id'  => $this->add_responsavel_id,
            'grau_parentesco' => $this->add_responsavel_grau_parentesco,
        ]);
        
        session()->flash('success-livewire', 'Responsável vinculado ao aluno com êxito.');

        $this->dispatchBrowserEvent('toast');
        $this->dispatchBrowserEvent('dissmiss-modal');
    }

}
