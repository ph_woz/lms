<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Trilha extends Model
{
    use HasFactory;

    protected $table = 'trilhas';

    public $timestamps = true;

    protected $fillable = [
        'plataforma_id',
        'formulario_id',
        'certificado_id',
        'atestado_id',
        'unidade_id',
        'coordenador_id',
        'nome',
        'referencia',
        'slug',
        'tipo_inscricao',
        'foto_capa',
        'foto_miniatura',
        'matriz_curricular',
        'publicar',
        'carga_horaria',
        'tipo_formacao',
        'duracao',
        'nivel',
        'modalidade',
        'area_negocio',
        'slogan',
        'video',
        'exibir_turmas_como_cc',
        'descricao',
        'tipo_valor',
        'tipo_mensalidade',
        'desconto_agilpass',
        'valor_parcelado',
        'valor_a_vista',
        'valor_mensalidade',
        'valor_promocional',
        'seo_title',
        'seo_keywords',
        'seo_description',
        'status',
    ];

    public static  function boot()
    {
        parent::boot(); 
        static::creating(function ($model)
        {
            $model->plataforma_id = session('plataforma_id') ?? Auth()->user()->plataforma_id ?? Plataforma::dominio()->pluck('id')[0] ?? null;
            $model->cadastrante_id = Auth()->user()->id ?? null;
        });
    }

    public function scopeAtivo($query)
    {
        return $query->where('status', 0);
    }

    public function scopePublico($query)
    {
        return $query->where('publicar', 'S');
    }
    
    public function scopePlataforma($query)
    {
        return $query->where('plataforma_id', session('plataforma_id') ?? Plataforma::dominio()->pluck('id')[0] ?? null);
    }

    public static function checkActive()
    {
        $check = PlataformaModulo::plataforma()->ativo()->where('url','trilha')->exists();
        
        return $check;
    }

}
