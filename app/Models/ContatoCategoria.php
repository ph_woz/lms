<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ContatoCategoria extends Model
{
    use HasFactory;

    protected $table = 'contatos_categorias';

    public $timestamps = false;

    protected $fillable = [ 'plataforma_id', 'categoria_id', 'contato_id', ];

    public function scopePlataforma($query)
    {
        return $query->where('plataforma_id', session('plataforma_id') ?? Plataforma::dominio()->pluck('id')[0] ?? null);
    }
}
