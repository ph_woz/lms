<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AvaliacaoController extends Controller
{
    public function lista()
    {
        return view('admin.avaliacao.lista');
    }
    
    public function info($avaliacao_id)
    {
        return view('admin.avaliacao.info', compact('avaliacao_id') );
    }

    public function finalizaram($avaliacao_id)
    {
        return view('admin.avaliacao.finalizaram', compact('avaliacao_id') );
    }

    public function naoFinalizaram($avaliacao_id)
    {
        return view('admin.avaliacao.nao-finalizaram', compact('avaliacao_id') );
    }

    public function add()
    {
    	return view('admin.avaliacao.add');
    }

    public function editar($avaliacao_id)
    {
        $curso_id = \App\Models\Avaliacao::plataforma()->where('id', $avaliacao_id)->pluck('curso_id')[0] ?? null;

        return view('admin.avaliacao.editar', compact('avaliacao_id','curso_id') );
    }

}