<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CursoAula extends Model
{
    use HasFactory;
    protected $table = 'cursos_aulas';

    public $timestamps = false;

    public $fillable = [

    	'plataforma_id',
    	'cadastrante_id',
    	'curso_id',
    	'modulo_id',
    	'nome',
        'descricao',
        'tipo_objeto',
        'conteudo',
    	'ordem',
    	'status',
    ];

    public static  function boot()
    {
        parent::boot(); 
        static::creating(function ($model)
        {
            $model->plataforma_id = session('plataforma_id') ?? \Auth()->user()->plataforma_id ?? Plataforma::dominio()->pluck('id')[0] ?? null;
            $model->cadastrante_id = \Auth()->user()->id ?? null;
        });
    }

    public function getPlataforma()
    {
        return $this->belongsTo(Plataforma::class, 'plataforma_id');
    }

    public function scopePlataforma($query)
    {
        return $query->where('plataforma_id', session('plataforma_id') ?? Plataforma::dominio()->pluck('id')[0] ?? null);
    }
    
    public function scopeAtivo($query)
    {
        return $query->where('status', 0);
    }


    public static function getVideoYoutubeEmbed($link)
    {
        $tipo = null;

        if(strpos($link, 'embed') > 0)
        {
            return $link;
        }

        if (strpos($link, 'youtube') > 0 || strpos($link, 'youtu.be') > 0) {
            $tipo = 'youtube';
        } elseif (strpos($link, 'vimeo') > 0) {
            $tipo = 'vimeo';
        } else {
            $tipo = 'unknown';
        }

        if($tipo == 'youtube')
        {
            if (strpos($link, 'youtube') > 0) {

                $video_id = explode("?v=", $link); 
                if (empty($video_id[1]))
                    $video_id = explode("/v/", $link);

                if (empty($video_id[1]))
                    $video_id = explode("/v/", $link); 

                if(isset($video_id[1]))
                    $video_id = explode("&", $video_id[1]);

                $video_id = $video_id[0];

            } else {

                $video_id = explode("https://youtu.be/", $link);
            
                $video_id = $video_id[1];
            }

            $link = 'https://www.youtube.com/embed/'.$video_id;
        }

        return $link;
    }

}
