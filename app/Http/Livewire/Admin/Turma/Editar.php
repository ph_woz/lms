<?php

namespace App\Http\Livewire\Admin\Turma;

use Livewire\Component;
use App\Models\Turma;
use App\Models\Curso;
use App\Models\Formulario;
use App\Models\Unidade;
use App\Models\FluxoCaixa;
use Illuminate\Support\Facades\Http;

class Editar extends Component
{
    public $turma;
    public $curso_id;
    public $formulario_id;
    public $unidade_id;
    public $nome;
    public $status = 0;
    public $tipo_inscricao;
    public $publicar;
    public $modalidade;
    public $data_inicio;
    public $data_termino;
    public $frequencia = [];
    public $horario;
    public $duracao;
    public $valor_parcelado;
    public $valor_a_vista;
    public $nota_minima_aprovacao;

    public $controlBoxApresentacao = 'none';
    public $controlBoxPresencial   = 'none';

    public function mount($turma_id)
    {
        $this->turma = Turma::plataforma()->find($turma_id);

        $this->frequencia = explode(', ', $this->turma->frequencia);

        if($this->turma->modalidade != 'EaD' && $this->turma->modalidade != null)
            $this->controlBoxPresencial = 'block';
        else
            $this->controlBoxPresencial = 'none';

        if($this->turma->publicar == 'S')
            $this->controlBoxApresentacao = 'block';

        $this->curso_id              = $this->turma->curso_id;
        $this->formulario_id         = $this->turma->formulario_id;
        $this->unidade_id            = $this->turma->unidade_id;
        $this->nome                  = $this->turma->nome;
        $this->status                = $this->turma->status;
        $this->tipo_inscricao        = $this->turma->tipo_inscricao;
        $this->publicar              = $this->turma->publicar;
        $this->modalidade            = $this->turma->modalidade;
        $this->data_inicio           = $this->turma->data_inicio;
        $this->data_termino          = $this->turma->data_termino;
        $this->horario               = $this->turma->horario;
        $this->duracao               = $this->turma->duracao;
        $this->valor_parcelado       = $this->turma->valor_parcelado;
        $this->valor_a_vista         = $this->turma->valor_a_vista;
        $this->nota_minima_aprovacao = $this->turma->nota_minima_aprovacao;
    }

    public function editar()
    {
        $frequencia = null;

        if($this->frequencia)
        {
            $frequencia = implode(', ', $this->frequencia);
        }

        if($this->turma->nome != $this->nome)
        {
            if(FluxoCaixa::checkActive())
            {
                FluxoCaixa::plataforma()->where('turma_id', $this->turma->id)->whereNull('trilha_id')->update(['produto_nome' => $this->nome]);
            }
        }
        
        $this->turma->update([
            'curso_id'              => $this->curso_id,
            'formulario_id'         => $this->formulario_id  ? $this->formulario_id  : null,
            'unidade_id'            => $this->unidade_id     ? $this->unidade_id     : null,
            'nome'                  => $this->nome,
            'status'                => $this->status,
            'tipo_inscricao'        => $this->tipo_inscricao,
            'publicar'              => $this->publicar,
            'modalidade'            => $this->modalidade,
            'data_inicio'           => $this->data_inicio,
            'data_termino'          => $this->data_termino,
            'frequencia'            => $frequencia,
            'horario'               => $this->horario,
            'duracao'               => $this->duracao,
            'valor_parcelado'       => $this->valor_parcelado,
            'valor_a_vista'         => $this->valor_a_vista,
            'nota_minima_aprovacao' => $this->nota_minima_aprovacao,
        ]);

        session()->flash('success-livewire', 'Turma atualizada com êxito.');

        $this->dispatchBrowserEvent('toast');
    }

    public function render()
    {
        $cursos       = Curso::plataforma()->ativo()->orderBy('referencia')->get();
        $formularios  = Formulario::plataforma()->ativo()->orderBy('referencia')->get();
        $unidades     = Unidade::plataforma()->ativo()->orderBy('nome')->get();

        return view('livewire.admin.turma.editar', 
            [
                'cursos'       => $cursos,
                'formularios'  => $formularios,
                'unidades'     => $unidades,
            ] 
        );
    }

    public function changeEventPublicar($value)
    {
        if($value == 'S')
            $this->controlBoxApresentacao = 'block';
        else
            $this->controlBoxApresentacao = 'none';
    }

    public function changeEventModalidade($value)
    {
        if($value == 'EaD' || $value == '')
            $this->controlBoxPresencial = 'none';
        else
            $this->controlBoxPresencial = 'block';
    }
}
