<?php

namespace App\Http\Livewire\Admin\Dashboard;

use Livewire\Component;
use Illuminate\Support\Facades\Http;
use App\Models\Util;
use App\Models\User;
use App\Models\Contato;
use App\Models\TrilhaAluno;
use App\Models\Unidade;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class Info extends Component
{
    public $ano;
    public $mes;

    public $unidade_id;

    protected $queryString = ['unidade_id','mes','ano'];

    public $diaBloqueio;

    public function mount()
    {
        $this->ano = date('Y');
        $this->mes = date('m');

        if(Auth::user()->restringir_unidade == 'S' && Auth::user()->unidade_id != null)
        {
            $this->unidade_id = Auth::user()->unidade_id;
        }
    }

    public function render()
    {
        $meses = Util::getMeses();

        $this->mes = $_GET['mes'] ?? date('m');
        $this->ano = $_GET['ano'] ?? date('Y');

        $unidades = Unidade::select('id','nome')->plataforma()->unidade()->ativo()->orderBy('nome')->get();

        if($this->unidade_id)
        {
            $countAlunos = User::select('id')->plataforma()->where('unidade_id', $this->unidade_id)->aluno()->ativo()->whereYear('created_at', $this->ano)->whereMonth('created_at', $this->mes)->count();
            
            $countLeads             = Contato::select('id')->plataforma()->where('unidade_id', $this->unidade_id)->lead()->whereYear('created_at', $this->ano)->whereMonth('created_at', $this->mes)->count();
            $countLeadsPendentes    = Contato::select('id')->plataforma()->where('unidade_id', $this->unidade_id)->pendente()->lead()->whereYear('created_at', $this->ano)->whereMonth('created_at', $this->mes)->count();
            
            $countSuportes          = Contato::select('id')->plataforma()->where('unidade_id', $this->unidade_id)->suporte()->whereYear('created_at', $this->ano)->whereMonth('created_at', $this->mes)->count();
            $countSuportesPendentes = Contato::select('id')->plataforma()->where('unidade_id', $this->unidade_id)->pendente()->suporte()->whereYear('created_at', $this->ano)->whereMonth('created_at', $this->mes)->count();

            $countConclusaoTrilha   = DB::table('users')
                    ->select('trilhas_alunos.aluno_id')
                    ->join('trilhas_alunos','users.id','trilhas_alunos.aluno_id')
                    ->where('users.tipo','aluno')
                    ->where('users.unidade_id', $this->unidade_id)
                    ->whereNotNull('trilhas_alunos.data_conclusao')
                    ->where('users.plataforma_id', session('plataforma_id') ?? Auth::user()->plataforma_id)
                    ->where('trilhas_alunos.plataforma_id', session('plataforma_id') ?? Auth::user()->plataforma_id)
                    ->whereYear('trilhas_alunos.data_conclusao', $this->ano)
                    ->whereMonth('trilhas_alunos.data_conclusao', $this->mes)
                    ->count();
        }
        else
        {
            $countAlunos = User::select('id')->plataforma()->aluno()->ativo()->whereYear('created_at', $this->ano)->whereMonth('created_at', $this->mes)->count();

            $countLeads             = Contato::select('id')->plataforma()->lead()->whereYear('created_at', $this->ano)->whereMonth('created_at', $this->mes)->count();
            $countLeadsPendentes    = Contato::select('id')->plataforma()->pendente()->lead()->whereYear('created_at', $this->ano)->whereMonth('created_at', $this->mes)->count();
            
            $countSuportes          = Contato::select('id')->plataforma()->suporte()->whereYear('created_at', $this->ano)->whereMonth('created_at', $this->mes)->count();
            $countSuportesPendentes = Contato::select('id')->plataforma()->pendente()->suporte()->whereYear('created_at', $this->ano)->whereMonth('created_at', $this->mes)->count();
            
            $countConclusaoTrilha   = TrilhaAluno::select('id')->plataforma()->whereNotNull('data_conclusao')->whereYear('data_conclusao', $this->ano)->whereMonth('data_conclusao', $this->mes)->count();
        }

        return view('livewire.admin.dashboard.info', 
            [
                'meses'                  => $meses,
                'countAlunos'            => $countAlunos,
                'countLeads'             => $countLeads,
                'countLeadsPendentes'    => $countLeadsPendentes,
                'countSuportes'          => $countSuportes,
                'countSuportesPendentes' => $countSuportesPendentes,
                'countConclusaoTrilha'   => $countConclusaoTrilha,
                'unidades'               => $unidades,
            ]
        );
    }

}
