@extends('layouts.aluno')
@section('seo_title', 'Avaliações')
@section('content')
<section class="container-fluid px-xl-5 z-3 mt-5 position-relative">

<div class="mb-4 px-4-5 py-4 rounded-10 box-shadow bg-14">

	<h5 class="mb-3 title fs-17">
		Provas não disponíveis no momento. Página em Manutenção temporariamente.
	</h5>

</section>
@endsection