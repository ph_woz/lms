<?php

namespace App\Http\Controllers\Aluno;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use App\Models\ComunicadoAluno;

class ComunicadoController extends Controller
{
    public function comunicados()
    {
        $getNomePage = 'Comunicados';

        ComunicadoAluno::plataforma()->where('aluno_id', \Auth::id())->whereNull('data_visualizacao')->update(['data_visualizacao' => date('Y-m-d H:i')]);

        $comunicados = DB::table('comunicados')
            ->join('comunicados_alunos','comunicados.id','comunicados_alunos.comunicado_id')
            ->leftJoin('users','comunicados.cadastrante_id','users.id')
            ->where('comunicados_alunos.plataforma_id', session('plataforma_id') ?? \Auth::user()->plataforma_id)
            ->where('comunicados_alunos.aluno_id', \Auth::id())
            ->orderBy('comunicados.id','desc')
            ->select(
                'users.nome as cadastrante',
                'comunicados.id',
                'comunicados.assunto',
                'comunicados.descricao',
                'comunicados.created_at',
                'comunicados_alunos.data_visualizacao'
            )->get();

        return view('aluno.comunicados', compact('getNomePage','comunicados') );
    }

}