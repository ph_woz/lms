<h1>Recuperação de Conta | {{ $plataformaNome }}</h1>

<p style="font-size:17px;">Olá, {{ $nome }}.</p>

<p style="font-size: 17px;">Para redefinir a senha de acesso da sua conta, clique no botão abaixo e redefina sua senha.</p>
<p style="font-size: 17px; margin-bottom: 2rem;">Se você não solicitou uma nova senha, desconsidere essa mensagem.</p>

<div>
	<a href="{{\Request::root()}}/redefinir-senha/token/{{ $token }}/email/{{ $email }}/id/{{ $id }}" style="display: inline-block; color: #FFF; text-decoration: none; border-radius:2px; background:#1865d9; padding-left:20px; padding-right:20px; padding-top:10px; padding-bottom:10px;">
		Redefinir Senha
	</a>
</div>