@extends('layouts.admin.master')

@section('css')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.14.0-beta2/dist/css/bootstrap-select.min.css">
@endsection

@section('content')
    
<div>    

    <div class="mb-3 d-flex justify-content-end">
	    <a href="{{ route('admin.questao.info', ['id' => $questao->id ?? null]) }}" class="btn bg-base text-white">
	        <i class="fa fa-eye"></i>
	    </a>
	</div>
	
	<form method="POST" autocomplete="off" enctype="multipart/form-data">
	@csrf
		
		<section class="mb-4-5">
			
			<a href="#boxInfoPrincipal" data-bs-toggle="collapse" aria-expanded="false" class="hover-d-none">
				<div class="section-title">
					Informações Principais
				</div>
			</a>
			<hr/>
			
			<div class="collapse show" id="boxInfoPrincipal">

				<div class="d-lg-flex mb-3">
					
					<div class="w-100">
						<label>Assunto</label>
						<input type="text" name="assunto" class="form-control p-form" placeholder="Assunto" required value="{{ $questao->assunto }}">
					</div>

					<div class="w-100 mx-lg-3 my-3 my-lg-0">
						<label>Modelo de Questão</label>
						<select name="modelo" onchange="modeloQuestao(this.value)" class="select-form" required>
							<option value="D">Discursiva</option>
							<option value="O" @if($questao->modelo == 'O') selected @endif>Objetiva</option>
						</select>
					</div>

					<div class="w-lg-45">
						<label>Status</label>
						<select name="status" class="select-form">
							<option value="0">Ativo</option>
							<option value="1" @if($questao->status == 1) selected @endif>Desativado</option>
						</select>
					</div>

				</div>

				<div class="mb-3">
					<label>Enunciado</label>
					<textarea name="enunciado" class="textarea-form">{{ $questao->enunciado }}</textarea>

					@if($questao->arquivo)
						<div class="d-flex align-items-center my-1" id="boxArquivo">
							<a href="{{ $questao->arquivo }}" class="font-nunito weight-600 fs-12 color-blue" target="_blank">
								Visualizar Anexo
							</a>
							<button title="Remover"
							    type="button" 
								onclick="document.getElementById('remover_arquivo').value = 'true';
								         document.getElementById('boxArquivo').remove();" 
								class="btn btn-danger ms-2 fs-11 px-2 py-1 flex-center">
								<i class="fa fa-times-circle fs-11"></i>
							</button>
						</div>
						<input type="hidden" name="remover_arquivo" id="remover_arquivo" value="">
					@endif

					<input type="file" name="arq" class="file-form">
				</div>
				<div class="mb-3">
					<label>Gabarito</label>
					<textarea name="gabarito" class="textarea-form" placeholder="Explicação de resposta">{{ $questao->gabarito }}</textarea>
				</div>

				<div class="mb-4" id="boxAlternativas" @if($questao->modelo == 'Discursiva' || $questao->modelo == 'Anexo de Arquivo') style="display:none;" @endif>
					
					<label>Alternativas</label>
					
					@foreach($alternativas as $alternativa)

						<div class="d-flex align-items-center mb-1">
							<input type="text" name="alternativas[]" class="form-control p-form me-2" placeholder="Alternativa" value="{{ $alternativa->alternativa }}">
							<select name="resolucoes[]" class="selectCorreta select-form me-2 w-20">
								<option value="N">Falsa</option>
								<option value="S" @if($alternativa->correta == 'S') selected @endif>Correta</option>
							</select>
							<a href="#" class="btn btn-danger btnRemove py-2-3"> <i class="fa fa-trash"></i> </a>
						</div>
					
					@endforeach

					@if(count($alternativas) <= 0)
						<div class="d-flex align-items-center mb-1">
							<input type="text" name="alternativas[]" class="form-control p-form me-2" placeholder="Alternativa">
							<select name="resolucoes[]" class="selectCorreta select-form me-2 w-20">
								<option value="N">Falsa</option>
								<option value="S">Correta</option>
							</select>
							<a href="#" class="btn btn-danger btnRemove py-2-3"> <i class="fa fa-trash"></i> </a>
						</div>
					@endif

					<div id="append"></div>

					<button type="button" class="mt-1 btn-append btn btn-primary fs-13">
						Adicionar outro
					</button>

				</div>

			</div>

		</section>

		<section class="mb-4-5">
			
			<a href="#boxCategorias" data-bs-toggle="collapse" aria-expanded="false" class="hover-d-none">
				<div class="section-title">
					Categorias 
				</div>
			</a>
			<hr/>

			<div class="collapse" id="boxCategorias">
				<div class="w-100">
					<label>Categorias</label>
					<select name="categorias[]" class="selectpicker" multiple data-width="100%" data-live-search="true" title="Selecione">
						@foreach($categorias as $categoria)
							<option value="{{ $categoria->id }}" @if(in_array($categoria->id, $categoriasSelected)) selected @endif>
								{{ $categoria->nome }}
							</option>
						@endforeach
					</select>
				</div>
			</div>

		</section>

		<button type="submit" class="btn-add mt-3">
			Salvar Alterações
		</button>

	</form>

</div>

@endsection

@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.14.0-beta2/dist/js/bootstrap-select.min.js"></script>
<script>

	$(document).on('change', '.selectCorreta', function(e) {
		
		e.preventDefault();

		$('.selectCorreta').val('N');
		$(this).val('S');
	
	});
	
	function modeloQuestao(value)
	{
		if(value == 'O')
			document.getElementById('boxAlternativas').style.display = 'block';
		else
			document.getElementById('boxAlternativas').style.display = 'none';
	}

	var index = 0;

	$('.btn-append').click(function(e) {

		e.preventDefault();

		$("#append").append('<div class="d-flex align-items-center mb-1"> <input type="text" name="alternativas[]" class="form-control p-form me-2" placeholder="Alternativa" required> <select name="resolucoes[]" class="selectCorreta select-form me-2 w-20"> <option value="N">Falsa</option> <option value="S">Correta</option> </select> <a href="#" class="btn btn-danger btnRemove py-2-3"> <i class="fa fa-trash"></i> </a> </div>');
	});

	$(document).on('click', '.btnRemove', function(e) {

		e.preventDefault();

		$(this).parent().remove();

	});

	$(document).on('change', '.radioCorreta', function(e) {

		e.preventDefault();
		$('.radioCorreta').prop('checked', false);
		$(this).prop('checked', true);

	});

</script>
@endsection
