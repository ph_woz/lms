<?php

namespace App\Http\Livewire\Admin\Colaborador;

use Livewire\WithPagination;
use Livewire\Component;
use App\Models\User;
use App\Models\Categoria;
use App\Models\UserCategoria;
use App\Models\Unidade;

class Lista extends Component
{
    use WithPagination;

    protected $paginationTheme = 'bootstrap';

    public $search;
    public $status = 0;
    public $ordem;
    public $unidade_id;
    public $categoriasSelected;

    protected $queryString = ['search','unidade_id','status','ordem'];

    public function render()
    {
        $colaboradores = User::select('id','nome','email')->plataforma()->colaborador()->where('status', $this->status);

        if($this->categoriasSelected != null)
        {
            $usersIds = UserCategoria::plataforma()->whereIn('categoria_id', $this->categoriasSelected ?? array())->get()->pluck('user_id')->toArray();
            $colaboradores = $colaboradores->whereIn('id', $usersIds);
        }

        if($this->search)
        {
            $colaboradores->where(function ($query)
            {
                $query
                    ->orWhere('nome', 'like', '%'.$this->search.'%')
                    ->orWhere('email', 'like', '%'.$this->search.'%')
                    ->orWhere('cpf', 'like', '%'.$this->search.'%');
            });
        }

        if(\Auth::user()->restringir_unidade == 'S' && \Auth::user()->unidade_id != null)
        {
            $colaboradores = $colaboradores->where('unidade_id', \Auth::user()->unidade_id);
        }

        if($this->unidade_id)
        {
            $colaboradores = $colaboradores->where('unidade_id', $this->unidade_id);
        }

        $countColaboradores = $colaboradores->count();


        if($this->ordem == 'desc' || $this->ordem == 'asc')
        {
            $colaboradores = $colaboradores->orderBy('id', $this->ordem);
        
        } else {

            if($this->ordem == 'data_ultimo_acesso')
            {
                $colaboradores = $colaboradores->orderBy('data_ultimo_acesso', 'desc');
            
            } else {

                $colaboradores = $colaboradores->orderBy('nome');
            }
        }

        $colaboradores = $colaboradores->orderBy('nome')->paginate(10);

        $categorias = Categoria::plataforma()->ativo()->tipo('aluno')->orderBy('nome')->get();
        $unidades   = Unidade::plataforma()->ativo()->orderBy('nome')->get();

        return view('livewire.admin.colaborador.lista', [
            'colaboradores'      => $colaboradores,
            'countColaboradores' => $countColaboradores,
            'categorias'         => $categorias,
            'unidades'           => $unidades,
        ]);
    }
}
