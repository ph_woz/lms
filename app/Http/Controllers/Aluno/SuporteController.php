<?php

namespace App\Http\Controllers\Aluno;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Contato;

class SuporteController extends Controller
{
    public function suporte()
    {
        $getNomePage = 'Suporte';

        if(request('sentContato'))
        {
            Contato::create(request()->all() + ['plataforma_id' => \Auth::user()->plataforma_id, 'tipo' => 'S']);

            return back()->with('success', 'Cadastro enviado com êxito.');
        }

        return view('aluno.suporte', compact('getNomePage') );
    }

}