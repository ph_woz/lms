<div>

	<form>

		<div class="d-lg-flex">

            <select wire:model="status" class="select-form w-lg-40">
            	<option value="">Status</option>
				<option value="0">em Pendente</option>
                <option value="1">em Andamento</option>
				<option value="2">em Validação</option>
				<option value="3">para Revisão</option>
                <option value="4">em Finalizada</option>
				<option value="5">em Desativada</option>
            </select>

            <select wire:model="arquivado" class="mx-lg-3 my-3 my-lg-0 select-form w-lg-45">
                <option value="N">Não arquivados</option>
                <option value="S">Arquivados</option>
            </select>

            <select wire:model="ordem" class="select-form w-lg-40">
                <option value="desc">Mais novos</option>
                <option value="asc">Mais antigos</option>
            </select>
            
		</div>

	</form>

	<ul class="mt-5 mb-3 list-group list-group-horizontal-md text-center fs-14 p-0">
	
		<li class="w-100 weight-600 list-group-item text-danger">
			{{ $chamados->where('status', 0)->count() }} em Pendente
		</li>
		<li class="w-100 weight-600 list-group-item text-secondary">
			{{ $chamados->where('status', 1)->count() }} em Andamento
		</li>
		<li class="w-100 weight-600 list-group-item text-primary">
			{{ $chamados->where('status', 2)->count() }} em Validação
		</li>
		<li class="w-100 weight-600 list-group-item text-warning text-shadow-1">
			{{ $chamados->where('status', 3)->count() }} para Revisão
		</li>
		<li class="w-100 weight-600 list-group-item text-success">
			{{ $chamados->where('status', 4)->count() }} em Finalizada
		</li>
		<li class="w-100 weight-600 list-group-item text-dark">
			{{ $chamados->where('status', 5)->count() }} em Desativada
		</li>

	</ul>

    <div class="mt-4-5 d-flex justify-content-between">

        <div class="align-self-end mb-2">
            <span class="resultados-encontrados">
                {{ count($chamados) }} resultados de {{ $countChamados }}.
            </span>
        </div>

        <div class="mb-3">
            <a href="{{ route('admin.ajuda.chamado.add') }}" class="btn-add aclNivel2 default-show">
                + Adicionar
            </a>
        </div>

    </div>

    <div class="row">
        <div class="col-12">

            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col">Assunto</th>
                                    <th scope="col">Status</th>
                                    <th scope="col">
                                        <span class="float-end">
                                            Ações
                                        </span>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($chamados as $chamado)
                                    <tr>
                                        <td>{{ $chamado->assunto }}</td>
                                        <td>
                                            @switch($chamado->status)
                                                @case(0)
                                                    <span class="text-danger font-nunito weight-700">Pendente</span>
                                                @break
                                                @case(1)
                                                    <span class="text-secondary font-nunito weight-700">em Andamento</span>
                                                @break
                                                @case(2)
                                                    <span class="text-primary font-nunito weight-700">em Validação</span>
                                                @break
                                                @case(3)
                                                    <span class="text-warning text-shadow-1 font-nunito weight-700">para Revisão</span>
                                                @break
                                                @case(4)
                                                    <span class="text-success font-nunito weight-700">Finalizada</span>
                                                @break
                                                @case(5)
                                                    <span class="text-dark font-nunito weight-700">Desativada</span>
                                                @break
                                            @endswitch
                                        </td>
                                        <td>
                                            <span class="float-end">

                                                <a href="{{ route('admin.ajuda.chamado.info', ['id' => $chamado->id]) }}" class="btn bg-base text-white">
                                                    <i class="fa fa-eye"></i>
                                                </a>

                                            </span>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            {{ $chamados->links() }}

        </div>
    </div>

</div>