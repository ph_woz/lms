<div>

	<section>

		<div class="d-lg-flex justify-content-end mb-4">

			<a href="#" data-bs-toggle="modal" data-bs-target="#ModalGerarCertificado" class="btn btn-primary fs-13 py-2" target="_blank">
				Gerar Certificado
			</a>

		</div>

		<div class="section-title rounded-bottom-0 d-flex justify-content-between align-items-center">
			{{ $certificado->referencia }}
			<div>

				<a href="#" data-bs-toggle="modal" data-bs-target="#ModalMetaDados" class="btn btn-info me-2 fs-14 mb-1 py-1 px-2-3">
					<i class="fa fa-align-left"></i>
				</a>
				<a href="/admin/certificado/editar/{{$certificado->id}}" class="aclNivel2 btn btn-warning text-dark fs-14 mb-1 py-1 px-2">
					<i class="fa fa-edit"></i>
				</a>
				<a href="#" data-bs-toggle="modal" data-bs-target="#ModalDeletarCertificado" class="aclNivel3 btn btn-danger mb-1 fs-15 py-1 px-2 ms-2">
					<i class="fa fa-trash"></i>
				</a>
			</div>
		</div>

		<div class="bg-white border p-4 d-lg-flex justify-content-between">

			<div class="mb-2 mb-lg-0">
				<p class="mb-0 weight-600 color-444">Texto 1</p>
				<p class="mb-0">
					{{ $certificado->texto1 ?? 'Não definido' }}
				</p>
			</div>

		</div>

		<div class="bg-white border p-4 d-lg-flex justify-content-between">

			<div class="mb-2 mb-lg-0">
				<p class="mb-0 weight-600 color-444">Texto 2</p>
				<p class="mb-0">
					{{ $certificado->texto2 ?? 'Não definido' }}
				</p>
			</div>

		</div>

	</section>

	<div class="modal fade" id="ModalMetaDados" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	  	<div class="modal-dialog modal-dialog-centered" role="document">
	    	<div class="modal-content">
	      		<div class="modal-header">
	        		<h3 class="modal-title">Metadados</h3>
	    			<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
	      		</div>
	      		<div class="modal-body">

					<ul class="list-group list-group-flush">
	  					<li class="list-group-item">
							<span class="mb-0 weight-600 color-444">Cadastrante:</span>
							@if($cadastrante)
								<a href="/admin/colaborador/info/{{$cadastrante->id}}" class="color-blue-info-link">
									{{ $cadastrante->nome }}
								</a>
							@else
								Não definido
							@endif
	  					</li>   
	  					<li class="list-group-item">
							<span class="mb-0 weight-600 color-444">Status:</span>
							@if($certificado->status == 0)
								<span class="text-success weight-700">Ativo</span>
							@else
								<span class="text-danger weight-700">Desativado</span>
							@endif
	  					</li>
	  					<li class="list-group-item">
							<span class="mb-0 weight-600 color-444">Data de Cadastro:</span>
							@if($certificado->created_at)
								<span class="mb-0">{{ $certificado->created_at->format('d/m/Y H:i') ?? null }} ({{$certificado->created_at->diffForHumans()}}) </span>
	  						@else
	  							Não definido
	  						@endif
	  					</li>   
	  				</ul>

	      		</div>
	    	</div>
	  	</div>
	</div>

	<form wire:submit.prevent="delete" method="POST">
		<div wire:ignore class="modal fade" id="ModalDeletarCertificado" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  		<div class="modal-dialog modal-dialog-centered">
	    		<div class="modal-content">
	      			<div class="modal-header">
	        			<h3 class="modal-title" id="exampleModalLabel">Deletar Certificado</h3>
	        			<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
	      			</div>
	      			<div class="modal-body text-center py-5">
	        			<p class="m-0 fs-16">
	        				Você tem certeza que deseja deletar este certificado?
	        				<br>
	        				Todas as informações serão perdidas definitivamente.
	        			</p>
	      			</div>
	      			<div class="modal-footer">
	      				<div class="d-flex w-100">
		      				<input type="password" wire:model="password_to_delete" class="w-100 form-control p-form me-3" placeholder="Digite sua senha" required>
		        			<button type="submit" class="btn btn-danger weight-600">Confirmar</button>
	    				</div>
	      			</div>
	    		</div>
	  		</div>
		</div>
	</form>

	<div wire:ignore.self class="modal fade" id="ModalGerarCertificado" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  		<div class="modal-dialog modal-lg modal-dialog-centered">
    		<div class="modal-content">
      			<div class="modal-header">
        			<h3 class="modal-title" id="exampleModalLabel">Gerar Certificado</h3>
        			<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      			</div>
      			<div class="modal-body p-lg-4">

      				<div>
      					<label>Email do Aluno</label>
      					<input type="text" wire:model="gerar_certificado_aluno_email" class="form-control p-form" placeholder="Digite o Email do Aluno" required>
      				</div>

      				@if(count($alunosGerarCertificado) > 0)
                    <div class="table-responsive mt-3">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col">Nome</th>
                                    <th scope="col">Email</th>
                                    <th scope="col">
                                        <span class="float-end me-2">
                                            Ações
                                        </span>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($alunosGerarCertificado as $aluno)
                                    <tr>
                                        <td>{{ $aluno->nome }}</td>
                                        <td>{{ $aluno->email }}</td>
                                        <td>
                                            <span class="float-end">
												<a href="?gerar_certificado_aluno_id={{ $aluno->id }}" class="btn btn-primary weight-600 py-2">Continuar</a>
                                            </span>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    @endif

      			</div>
    		</div>
  		</div>
	</div>

	@if($alunoEscolhidoGerarCertificado)
	<form wire:submit.prevent="redirectGerarCerfificado">
		<div wire:ignore.self class="modal fade" id="ModalAlunoEscolhidoGerarCertificado" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  		<div class="modal-dialog modal-dialog-centered">
	    		<div class="modal-content">
	      			<div class="modal-header">
	        			<h3 class="modal-title" id="exampleModalLabel">
	        				Gerar Certificado<br>
	        				<span class="fs-14 d-block mt-3">
	        					{{ $alunoEscolhidoGerarCertificado->nome }}
	        				</span>
	        				<span class="fs-14">
	        					{{ $alunoEscolhidoGerarCertificado->email }}
	        				</span>
	        			</h3>
	        			<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
	      			</div>
	      			<div class="modal-body">

	      				<div class="mb-3 d-lg-flex align-items-center">

					        <div class="w-100" wire:ignore>
					            <select wire:model="gerar_certificado_turma_id" wire:change="changeEventTurma()" class="selectpicker" data-width="100%" data-live-search="true" title="Selecione uma Turma">
					                @foreach($turmas as $turma)
					                    <option value="{{ $turma->id }}" @if($this->gerar_certificado_turma_id == $turma->id) selected @endif>{{ $turma->nome }}</option>
					                @endforeach
					            </select>
					        </div>

					        <div class="d-xs-none mx-2">
					        	OU
					        </div>

					        <div class="w-100" wire:ignore>
					            <select wire:model="gerar_certificado_trilha_id" wire:change="changeEventTrilha()" class="selectpicker" data-width="100%" data-live-search="true" title="Selecione uma Trilha">
					                @foreach($trilhas as $trilha)
					                    <option value="{{ $trilha->id }}" @if($this->gerar_certificado_trilha_id == $trilha->id) selected @endif>{{ $trilha->nome }}</option>
					                @endforeach
					            </select>
					        </div>

					    </div>

					    <div class="w-100 mb-3">
					    	<textarea wire:model="gerar_certificado_texto1" class="textarea-form h-min-150" placeholder="Texto"></textarea>
					    </div>

					    <div class="w-100 mb-3">
					    	<textarea wire:model="gerar_certificado_texto2" class="textarea-form h-min-150" placeholder="Texto"></textarea>
					    </div>

	      			</div>
	      			<div class="modal-footer">
		        		<button type="submit" class="btn btn-primary weight-600 py-2">Gerar agora!</button>
	      			</div>
	    		</div>
	  		</div>
		</div>
	</form>
	@endif

	@include('livewire.util.toast')

</div>