<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WozcodeComodidade extends Model
{
    protected $table = 'wozcode_comodidades';

    public $timestamps = false;

    protected $fillable = ['texto','icon'];
}
