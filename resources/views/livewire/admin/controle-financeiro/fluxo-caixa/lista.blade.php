<div>

    <div class="d-flex align-items-center p-3 bg-white box-shadow rounded-1" style="border: 2px solid #dee2e6!important;">
        <a href="{{ route('admin.aluno.lista', ['add' => 'S', 'add_aluno_para_fluxo_caixa' => 'S']) }}" class="w-100 text-center btn-add py-2-4 fs-15 font-nunito weight-600 border-0">
            + Adicionar Entrada
        </a>

        <div class="mx-3" style="height: 45px; width:4.3px;background: #dee2e6;"></div>

        <a href="#" data-bs-target="#ModalAddSaida" data-bs-toggle="modal" class="w-100 text-center btn-add py-2-4 fs-15 font-nunito weight-600 border-0">
            - Adicionar Saída
        </a>
    </div>

    <div wire:ignore>
        <form class="py-3 mb-3 border-top border-bottom border-muted my-3">
            
            <div class="mb-3 d-lg-flex align-items-center">

                @if(\App\Models\Unidade::checkActive() === true && count($unidades) > 0)
                <select wire:model="unidade_id" id="unidade_id" class="me-lg-3 mb-3 mb-lg-0 selectpicker" data-width="100%" data-live-search="true">
                    <option value="">Unidades</option>
                    @foreach($unidades as $unidade)
                        <option value="{{ $unidade->id }}">{{ $unidade->nome }}</option>
                    @endforeach
                </select>
                @endif

                <input type="search" wire:model="buscar" class="me-lg-3 mb-3 mb-lg-0 form-control p-form" placeholder="Buscar">

                <select wire:model="vendedor_id" class="me-lg-3 mb-3 mb-lg-0 selectpicker" data-width="100%" data-live-search="true" required>
                    <option value="">Vendedor</option>
                    @foreach($vendedores as $vendedor)
                        <option value="{{ $vendedor->id }}">
                            {{ $vendedor->nome }}
                        </option>
                    @endforeach
                </select>

                <select wire:model="produto_id" class="me-lg-3 mb-3 mb-lg-0 selectpicker" data-width="100%" data-live-search="true" required>
                    <option value="">Produto</option>
                    <optgroup label="Trilhas">
                        @foreach($trilhas as $nome => $turma_id)
                            <option value="trilha_id{{ $turma_id }}">
                                {{ $nome }}
                            </option>
                        @endforeach
                    </optgroup>
                    <optgroup label="Turmas">
                        @foreach($turmas as $nome => $turma_id)
                            <option value="turma_id{{ $turma_id }}">
                                {{ $nome }}
                            </option>
                        @endforeach
                    </optgroup>
                </select>

                <select wire:model="status" class="select-form w-lg-50">
                    <option value="">Status de Pagamento</option>
                    <option value="a-receber">A receber</option>
                    <option value="recebidos-e-pagos">Recebidos/Pagos</option>
                    <option value="inadimplentes">Inadimplentes</option>
                    <option value="estornados">Estornados</option>
                </select>

            </div>

            <div class="mt-4 d-xl-flex justify-content-between">

                @foreach($meses as $numero => $nome)
                    <div class="d-inline-block mb-3 w-100 text-center me-xl-1" id="n_mes_{{$numero}}">
                        <a href="?mes={{ $numero }}&ano={{ $ano }}" class="d-block w-100 btn-add fs-13 rounded">
                            {{ $nome }}
                        </a>
                    </div>
                @endforeach

            </div>

        </form>
    </div>

    <div class="row mb-lg-4">

        <div class="col-lg-4">
            <div class="card m-0">
                <div class="card-header text-white bg-sell-green">
                    <div class="d-flex justify-content-between align-items-center">
                        <span class="d-block fs-25 weight-500">
                            R$ {{ \App\Models\FluxoCaixa::getValorRealDisponivel(\App\Models\FluxoCaixa::getValorRealInArray($entradas->pluck('valor')->toArray()), \App\Models\FluxoCaixa::getValorRealInArray($saidas->pluck('valor')->toArray())) }}
                        </span>
                        <span class="fa fa-sign-out-alt fs-25 text-white-50"></span>
                    </div>
                    <span class="d-block font-nunito fs-16 weight-600">Previsto total de Disponível</span>
                </div>
            </div>
        </div>

        <div class="col-lg-4">
            <div class="card m-0">
                <div class="card-header text-white bg-info">
                    <div class="d-flex justify-content-between align-items-center">
                        <span class="d-block fs-25 weight-500">
                            R$ {{ \App\Models\FluxoCaixa::getValorRealInArray($entradas->pluck('valor')->toArray()) }}
                        </span>
                        <span class="fa fa-money-check-alt fs-25 text-white-50"></span>
                    </div>
                    <span class="d-block font-nunito fs-16 weight-600">Previsto total de Faturamento</span>
                </div>
            </div>
        </div>

        <div class="col-lg-4">
            <div class="card m-0">
                <div class="card-header text-white bg-danger">
                    <div class="d-flex justify-content-between align-items-center">
                        <span class="d-block fs-25 weight-500">
                            R$ {{ \App\Models\FluxoCaixa::getValorRealInArray($saidas->pluck('valor')->toArray()) }}
                        </span>
                        <span class="fa fa-sign-out-alt fs-25 text-white-50"></span>
                    </div>
                    <span class="d-block font-nunito fs-16 weight-600">Previsto total de Pagamentos</span>
                </div>
            </div>
        </div>
        
    </div>

    <div class="row mb-lg-4">

        <div class="col-lg-4">
            <div class="card m-0">
                <div class="card-header text-white bg-sell-green">
                    <div class="d-flex justify-content-between align-items-center">
                        <span class="d-block fs-25 weight-500">
                            R$ {{ \App\Models\FluxoCaixa::getValorRealDisponivel(\App\Models\FluxoCaixa::getValorRealInArray($entradas->where('status', 1)->pluck('valor')->toArray()), \App\Models\FluxoCaixa::getValorRealInArray($saidas->where('status', 1)->pluck('valor')->toArray())) }}
                        </span>
                        <span class="fa fa-money-check-alt fs-25 text-white-50"></span>
                    </div>
                    <span class="d-block font-nunito fs-16 weight-600">Disponível atual</span>
                </div>
            </div>
        </div>


        <div class="col-lg-4">
            <div class="card m-0">
                <div class="card-header text-white bg-info">
                    <div class="d-flex justify-content-between align-items-center">
                        <span class="d-block fs-25 weight-500">
                            R$ {{ \App\Models\FluxoCaixa::getValorRealInArray($entradas->where('status', 1)->pluck('valor')->toArray()) }}
                        </span>
                        <span class="fa fa-money-check-alt fs-25 text-white-50"></span>
                    </div>
                    <span class="d-block font-nunito fs-16 weight-600">Faturamento atual</span>
                </div>
            </div>
        </div>

        <div class="col-lg-4">
            <div class="card m-0">
                <div class="card-header text-white bg-danger">
                    <div class="d-flex justify-content-between align-items-center">
                        <span class="d-block fs-25 weight-500">
                            R$ {{ \App\Models\FluxoCaixa::getValorRealInArray($saidas->where('status', 1)->pluck('valor')->toArray()) }}
                        </span>
                        <span class="fa fa-sign-out-alt fs-25 text-white-50"></span>
                    </div>
                    <span class="d-block font-nunito fs-16 weight-600">Despesas já pagas</span>
                </div>
            </div>
        </div>

    </div>

    <div class="row mb-4-5">

        <div class="col-lg-4">
            <div class="card m-0">
                <div class="card-header text-white bg-danger">
                    <div class="d-flex justify-content-between align-items-center">
                        <span class="d-block fs-25 weight-500">
                            R$ {{ \App\Models\FluxoCaixa::getValorRealInArray($inadimplentes) }}
                        </span>
                        <span class="fa fa-money-check-alt fs-25 text-white-50"></span>
                    </div>
                    <span class="d-block font-nunito fs-16 weight-600">
                        Inadimplentes <span class="fs-15">({{ count($inadimplentes) }} clientes)</span>
                    </span>
                </div>
            </div>
        </div>

        <div class="col-lg-4">
            <div class="card m-0">
                <div class="card-header text-white bg-info">
                    <div class="d-flex justify-content-between align-items-center">
                        <span class="d-block fs-25 weight-500">
                            R$ {{ \App\Models\FluxoCaixa::getValorRealInArray($entradas->where('status', 0)->pluck('valor')->toArray()) }}
                        </span>
                        <span class="fa fa-money-check-alt fs-25 text-white-50"></span>
                    </div>
                    <span class="d-block font-nunito fs-16 weight-600">Pendentes a receber</span>
                </div>
            </div>
        </div>

        <div class="col-lg-4">
            <div class="card m-0">
                <div class="card-header text-white bg-danger">
                    <div class="d-flex justify-content-between align-items-center">
                        <span class="d-block fs-25 weight-500">
                            R$ {{ \App\Models\FluxoCaixa::getValorRealInArray($saidas->where('status', 0)->pluck('valor')->toArray()) }}
                        </span>
                        <span class="fa fa-sign-out-alt fs-25 text-white-50"></span>
                    </div>
                    <span class="d-block font-nunito fs-16 weight-600">
                        Pendentes a pagar
                        @if($countDespesasEmAtraso > 0)
                            ({{ $countDespesasEmAtraso }} em atraso)
                        @endif
                    </span>
                </div>
            </div>
        </div>

    </div>

    <div class="border rounded-1 h-50px d-flex align-items-center">

        <a href="#tabEntradas" class="btnNavTab btnTabEntrada" data-bs-toggle="collapse" aria-expanded="true">
            Entrada ({{ count($entradas) }})
        </a>
        
        <div class="lineTab"></div>
        
        <a href="#tabSaidas" class="btnNavTab btnTabSaida" data-bs-toggle="collapse" aria-expanded="false">
            Saída ({{ count($saidas) }})
        </a>

    </div>

    <div class="bg-white border border border-bottom-0">
        <div class="table-responsive collapse show" id="tabEntradas">
            <table class="table">
              <thead>
                <tr>
                  <th scope="col">Cliente</th>
                  <th scope="col">Produto</th>
                  <th scope="col">Valor</th>
                  <th scope="col">Status</th>
                  <th scope="col">Data de Vencimento</th>
                  <th scope="col">
                    <span class="float-end">
                        Ações
                    </span>
                  </th>
                </tr>
              </thead>
              <tbody>
                @foreach($entradas as $entrada)
                <tr>
                    <td>
                        <a href="{{ route('admin.aluno.info', ['id' => $entrada->cliente_id]) }}" class="color-blue-info-link">
                            {{ \App\Models\Util::getPrimeiroSegundoNome($entrada->cliente_nome) }}
                        </a>
                    </td>
                    <td>
                        @if($entrada->trilha_id !== null)
                            <a href="{{ route('admin.trilha.info', ['id' => $entrada->trilha_id]) }}" title="Trilha" class="color-blue-info-link">
                                {{ $entrada->produto_nome }}
                            </a>
                        @else
                            <a href="{{ route('admin.turma.info', ['id' => $entrada->turma_id ?? 0]) }}" title="Turma" class="color-blue-info-link">
                                {{ $entrada->produto_nome }}
                            </a>
                        @endif
                    </td>
                    <td>
                        @if($entrada->n_parcelas != $entrada->n_parcela || $entrada->registro_id != null)
                            {{ $entrada->n_parcela }}/{{$entrada->n_parcelas}} de
                        @endif
                        R$ {{ $entrada->valor }}
                    </td>
                    <td>
                        @switch($entrada->status)
                            @case(0)
                                @if(date('Y-m-d') > $entrada->data_vencimento)
                                    <span class="py-1-5 px-2 text-light rounded bg-danger">
                                        Inadimplente
                                    </span>
                                @endif
                                @if(date('Y-m-d') <= $entrada->data_vencimento)
                                    <span class="py-1-5 px-2 text-light rounded bg-info">
                                        A receber
                                    </span>
                                @endif
                            @break
                            @case(1)
                                <span class="py-1-5 px-2 text-light rounded bg-success">
                                    Recebido
                                </span>
                            @break
                            @case(2)
                                <span class="py-1-5 px-2 text-light rounded bg-inadimplente">
                                    Estornado
                                </span>
                            @break
                        @endswitch
                    </td>
                    <td>
                        {{ date('d/m/Y', strtotime($entrada->data_vencimento)) }}
                    </td>
                    <td>
                        <span class="float-end" wire:ignore>
                            <button type="button" data-bs-target="#ModalAcoesEntrada" wire:click="infoEntrada({{$entrada->id}},{{$mes}},{{$ano}})" data-bs-toggle="modal" class="btn-add py-2 px-2-5">
                                <i class="fa fa-eye"></i>
                            </button>
                        </span>
                    </td>
                </tr>
                @endforeach
              </tbody>
            </table>
        </div>
        <div class="table-responsive collapse" id="tabSaidas">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th scope="col">Referência</th>
                  <th scope="col">Valor</th>
                  <th scope="col">Status</th>
                  <th scope="col">Data</th>
                  <th scope="col">
                    <span class="float-end">
                        Ações
                    </span>
                  </th>
                </tr>
              </thead>
              <tbody>
                @foreach($saidas as $saida)
                <tr>
                    <td>
                        {{ $saida->ref_saida }}
                    </td>
                    <td>
                        @if($saida->n_parcelas != $saida->n_parcela || $saida->registro_id != null)
                            {{ $saida->n_parcela }}/{{$saida->n_parcelas}} de
                        @endif
                        R$ {{ $saida->valor }}
                    </td>
                    <td>
                        @switch($saida->status)
                            @case(0)
                                @if(date('Y-m-d') > $saida->data_vencimento)
                                    <span class="py-1-5 px-2 text-light rounded bg-danger">
                                        Em atraso
                                    </span>
                                @endif
                                @if(date('Y-m-d') <= $saida->data_vencimento)
                                    <span class="py-1-5 px-2 text-light rounded bg-secondary">
                                        A pagar
                                    </span>
                                @endif
                            @break
                            @case(1)
                                <span class="py-1-5 px-2 text-light rounded bg-success">
                                    Pago
                                </span>
                            @break
                        @endswitch
                    </td>
                    <td>
                        {{ date('d/m/Y', strtotime($saida->data_vencimento)) }}
                    </td>
                    <td>
                        <span class="float-end" wire:ignore>
                            <button type="button" data-bs-target="#ModalAcoesSaida" wire:click="infoSaida({{$saida->id}},{{$mes}},{{$ano}})" data-bs-toggle="modal" class="btn-add py-2 px-2-5">
                                <i class="fa fa-eye"></i>
                            </button>
                        </span>
                    </td>
                </tr>
                @endforeach
              </tbody>
            </table>
        </div>
    </div>

    <form wire:submit.prevent="addEntrada" method="POST">
        <div wire:ignore.self class="modal fade" id="ModalAddEntrada" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3 class="modal-title" id="exampleModalLabel">+ Adicionar Entrada</h3>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">

                        <div class="w-100 mb-3" wire:ignore>
                            <label>Cliente</label>
                            <a href="{{ route('admin.aluno.info', ['id' => $cliente_id ?? 0]) }}" target="_blank" class="hover-d-none">
                                <input type="text" wire:model="add_entrada_cliente_selected_nome" class="form-control p-form cursor-pointer bg-secondary border-0 text-light" readonly disabled>
                            </a>
                        </div>

                        <hr/>

                        <div class="d-lg-flex mb-3">

                            <div class="w-100" wire:ignore>
                                <label>Vendedor</label>
                                <select wire:model="add_entrada_vendedor_id" wire:change="setAddEntradaVendedorNome($event.target.value)" class="selectpicker" data-width="100%" data-live-search="true" required>
                                    <option value="">Selecione</option>
                                    @foreach($vendedores as $vendedor)
                                        <option value="{{ $vendedor->id }}">
                                            {{ $vendedor->nome }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="w-100 ms-lg-3 mt-3 mt-lg-0" wire:ignore>
                                <label>Produto</label>
                                <select wire:model="add_entrada_produto_id" class="selectpicker" wire:change="setAddEntradaProdutoNome($event.target.value)" title="Selecione" data-width="100%" data-live-search="true" required>
                                    <option value="">Selecione</option>
                                    <optgroup label="Trilhas">
                                        @foreach($trilhas as $nome => $turma_id)
                                            <option value="trilha_id{{ $turma_id }}">
                                                {{ $nome }}
                                            </option>
                                        @endforeach
                                    </optgroup>
                                    <optgroup label="Turmas">
                                        @foreach($turmas as $nome => $turma_id)
                                            <option value="turma_id{{ $turma_id }}">
                                                {{ $nome }}
                                            </option>
                                        @endforeach
                                    </optgroup>
                                </select>
                            </div>

                            @if(\App\Models\Unidade::checkActive() === true && count($unidades) > 0)
                            <div class="w-100 ms-lg-3 mt-3 mt-lg-0" wire:ignore>
                                <label>Unidade</label>
                                <select wire:model="add_entrada_unidade_id" id="add_entrada_unidade_id"  wire:change="setAddEntradaUnidadeNome($event.target.value)" class="selectpicker" data-width="100%" data-live-search="true">
                                    <option value="">Selecione</option>
                                    @foreach($unidades as $unidade)
                                        <option value="{{ $unidade->id }}">{{ $unidade->nome }}</option>
                                    @endforeach
                                </select>
                            </div>
                            @endif

                        </div>

                        <div class="d-lg-flex mb-3">

                            <div class="w-lg-50 me-lg-3 mb-3 mb-lg-0">
                                <label>Data de Vencimento</label>
                                <input type="date" wire:model="add_entrada_data_vencimento" class="form-control p-form" required>
                            </div>

                            <div class="w-lg-50">
                                <label>Valor</label>
                                <input type="text" wire:model="add_entrada_valor" id="add_entrada_valor" class="mask_valor form-control p-form" placeholder="R$" required>
                            </div>

                            <div class="w-lg-50 mx-lg-3 my-3 my-lg-0">
                                <label>Forma de Pagamento</label>
                                <select wire:model="add_entrada_forma_pagamento" class="select-form">
                                    <option value="">Selecione</option>
                                    <option value="Dinheiro">Dinheiro</option>
                                    <option value="Cartão de débito">Cartão de débito</option>
                                    <option value="Cartão de crédito">Cartão de crédito</option>
                                    <option value="Boleto bancário">Boleto bancário</option>
                                    <option value="Pix">Pix</option>
                                    <option value="TED">TED</option>
                                    <option value="DOC">DOC</option>
                                </select>
                            </div>

                            <div class="w-lg-30">
                                <label>Parcelas</label>
                                <input type="text" wire:model="add_entrada_n_parcelas" wire:change="gerarParcelasAddEntrada($event.target.value)" class="number form-control p-form" placeholder="N°" required>
                            </div>

                        </div>

                        <div class="mb-3 w-100">
                            <label>Link de Pagamento</label>
                            <input type="text" wire:model="add_entrada_link" class="form-control p-form" placeholder="Link de Pagamento">
                        </div>

                        <div id="boxParcelas" style="display: {{$add_entrada_valueDisplayParcelas}}">
                            
                            <label class="mb-0 d-block">Parcelas</label>
                            <hr class="mt-2" />

                            <div class="mb-2 d-flex align-items-center parcelas">

                                <div class="border bg-light rounded-left-2 flex-center w-lg-20" readonly style="height: 42.5px;">
                                    N°
                                </div>

                                <input type="text" class="mx-lg-2 my-2 my-lg-0 form-control p-form" value="Data de Vencimento" readonly>
                                
                                <input type="text" class="form-control p-form" value="Valor" readonly>
                                
                                <input type="text" class="ms-lg-2 mt-2 mt-lg-0 form-control p-form" value="Forma de Pagamento" readonly>

                            </div>

                            @foreach($add_entrada_parcelas as $value => $key)
                                <div class="mb-2 d-flex align-items-center parcelas">

                                    <div class="border bg-white rounded-left-2 flex-center w-lg-20" style="height: 42.5px;">
                                        {{ $loop->iteration + 1 }}
                                    </div>

                                    <input type="hidden" wire:model="add_entrada_n_parcela.{{$value}}">

                                    <input type="date" wire:model="add_entrada_datas_vencimento.{{$value}}" class="mx-lg-2 my-2 my-lg-0 form-control p-form" required>
                                    
                                    <input type="text" wire:model="add_entrada_valores.{{$value}}" class="mask_valor form-control p-form" placeholder="R$" required>
                                    
                                    <input type="text" wire:model="add_entrada_links.{{$value}}" class="ms-lg-2 mt-2 mt-lg-0 form-control p-form" placeholder="Link de Pagamento">
                                    
                                    <select wire:model="add_entrada_formas_pagamento.{{$value}}" class="ms-lg-2 mt-2 mt-lg-0 select-form">
                                        <option value="">Selecione</option>
                                        <option value="Dinheiro">Dinheiro</option>
                                        <option value="Cartão de débito">Cartão de débito</option>
                                        <option value="Cartão de crédito">Cartão de crédito</option>
                                        <option value="Boleto bancário">Boleto bancário</option>
                                        <option value="Pix">Pix</option>
                                        <option value="TED">TED</option>
                                        <option value="DOC">DOC</option>
                                    </select> 

                                </div>
                            @endforeach

                            <hr>

                        </div>

                        <div class="mb-3">
                            <label>Observações internas</label>
                            <textarea wire:model="add_entrada_obs" class="textarea-form" placeholder="Descreva observações..."></textarea>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary weight-600 py-2">Confirmar</button>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <form wire:submit.prevent="editarEntrada" method="POST">
        <div wire:ignore.self class="modal fade" id="ModalEditarEntrada" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3 class="modal-title" id="exampleModalLabel">+ Editar Entrada</h3>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">

                        <div class="w-100 mb-3" wire:ignore>
                            <label>Cliente</label>
                            <a href="{{ route('admin.aluno.info', ['id' => $cliente_id ?? 0]) }}" target="_blank" class="hover-d-none">
                                <input type="text" wire:model="editar_entrada_cliente_selected_nome" class="form-control p-form cursor-pointer bg-secondary border-0 text-light" readonly disabled>
                            </a>
                        </div>

                        <hr/>

                        <div class="d-lg-flex mb-3">

                            <div class="w-100" wire:ignore>
                                <label>Vendedor</label>
                                <select wire:model="editar_entrada_vendedor_id" wire:change="setEditarEntradaVendedorNome($event.target.value)" class="selectpicker" data-width="100%" data-live-search="true" required>
                                    <option value="">Selecione</option>
                                    @foreach($vendedores as $vendedor)
                                        <option value="{{ $vendedor->id }}">
                                            {{ $vendedor->nome }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="w-100 ms-lg-3 mt-3 mt-lg-0" wire:ignore>
                                <label>Produto</label>
                                <select wire:model="editar_entrada_produto_id" class="selectpicker" wire:change="setEditarEntradaProdutoNome($event.target.value)" title="Selecione" data-width="100%" data-live-search="true" required>
                                    <option value="">Selecione</option>
                                    <optgroup label="Trilhas">
                                        @foreach($trilhas as $nome => $turma_id)
                                            <option value="trilha_id{{ $turma_id }}">
                                                {{ $nome }}
                                            </option>
                                        @endforeach
                                    </optgroup>
                                    <optgroup label="Turmas">
                                        @foreach($turmas as $nome => $turma_id)
                                            <option value="turma_id{{ $turma_id }}">
                                                {{ $nome }}
                                            </option>
                                        @endforeach
                                    </optgroup>
                                </select>
                            </div>

                            @if(\App\Models\Unidade::checkActive() === true && count($unidades) > 0)
                            <div class="w-100 ms-lg-3 mt-3 mt-lg-0" wire:ignore>
                                <label>Unidade</label>
                                <select wire:model="editar_entrada_unidade_id" id="editar_entrada_unidade_id"  wire:change="setEditarEntradaUnidadeNome($event.target.value)" class="selectpicker" data-width="100%" data-live-search="true">
                                    <option value="">Selecione</option>
                                    @foreach($unidades as $unidade)
                                        <option value="{{ $unidade->id }}">{{ $unidade->nome }}</option>
                                    @endforeach
                                </select>
                            </div>
                            @endif

                        </div>

                        @if(isset($editar_entrada) && $editar_entrada !== false && $editar_entrada->registro_id == null)

                            <div class="d-lg-flex mb-3">

                                <div class="w-lg-50 me-lg-3 mb-3 mb-lg-0">
                                    <label>Data de Vencimento</label>
                                    <input type="date" wire:model="editar_entrada_data_vencimento" class="form-control p-form" required>
                                </div>

                                <div class="w-lg-50">
                                    <label>Valor</label>
                                    <input type="text" wire:model="editar_entrada_valor" id="editar_entrada_valor" class="mask_valor form-control p-form" placeholder="R$" required>
                                </div>

                                <div class="w-lg-50 mx-lg-3 my-3 my-lg-0">
                                    <label>Forma de Pagamento</label>
                                    <select wire:model="editar_entrada_forma_pagamento" class="select-form">
                                        <option value="">Selecione</option>
                                        <option value="Dinheiro">Dinheiro</option>
                                        <option value="Cartão de débito">Cartão de débito</option>
                                        <option value="Cartão de crédito">Cartão de crédito</option>
                                        <option value="Boleto bancário">Boleto bancário</option>
                                        <option value="Pix">Pix</option>
                                        <option value="TED">TED</option>
                                        <option value="DOC">DOC</option>
                                    </select>
                                </div>

                                <div class="w-lg-30">
                                    <label>Parcelas</label>
                                    <input type="text" wire:model="editar_entrada_n_parcelas" wire:change="gerarParcelasEditarEntrada($event.target.value)" class="number form-control p-form" placeholder="N°" required @if(isset($editar_entrada) && $editar_entrada->registro_id != null) disabled @endif>
                                </div>

                            </div>

                            <div class="mb-3 w-100">
                                <label>Link de Pagamento</label>
                                <input type="text" wire:model="editar_entrada_link" id="editar_entrada_link" class="form-control p-form" placeholder="Link de Pagamento">
                            </div>

                        @endif

                        <div id="boxParcelas" style="display: {{$editar_entrada_valueDisplayParcelas}}">
                            
                            <label class="mb-0 d-block">Parcelas</label>
                            <hr class="mt-2" />

                            <div class="mb-2 d-flex align-items-center parcelas">

                                <div class="border bg-light rounded-left-2 flex-center w-lg-20" readonly style="height: 42.5px;">
                                    N°
                                </div>

                                <input type="text" class="mx-lg-2 my-2 my-lg-0 form-control p-form" value="Data de Vencimento" readonly>
                                
                                <input type="text" class="form-control p-form" value="Valor" readonly>
                                
                                <input type="text" class="ms-lg-2 mt-2 mt-lg-0 form-control p-form" value="Forma de Pagamento" readonly>

                            </div>

                            @foreach($editar_entrada_parcelas as $value => $key)
                                <div class="mb-2 d-flex align-items-center parcelas">

                                    <input type="hidden" wire:model="editar_entrada_ids.{{$value}}">
                                    <input type="hidden" wire:model="editar_entrada_registro_ids.{{$value}}">

                                    <input type="text" wire:model="editar_entrada_n_parcela.{{$value}}" class="border @if(isset($key['data_vencimento']) && $key['data_vencimento'] == $editar_entrada->data_vencimento) bg-primary text-light @else bg-white @endif rounded-left-2 flex-center w-lg-20 text-center" readonly style="height: 42.5px;">

                                    <input type="date" wire:model="editar_entrada_datas_vencimento.{{$value}}" class="mx-lg-2 my-2 my-lg-0 form-control p-form" required>
                                    
                                    <input type="text" wire:model="editar_entrada_valores.{{$value}}" class="mask_valor form-control p-form" placeholder="R$" required>
                                    
                                    <input type="text" wire:model="editar_entrada_links.{{$value}}" class="ms-lg-2 mt-2 mt-lg-0 form-control p-form" placeholder="Link de Pagamento">
                                    
                                    <select wire:model="editar_entrada_formas_pagamento.{{$value}}" class="ms-lg-2 mt-2 mt-lg-0 select-form">
                                        <option value="">Selecione</option>
                                        <option value="Dinheiro">Dinheiro</option>
                                        <option value="Cartão de débito">Cartão de débito</option>
                                        <option value="Cartão de crédito">Cartão de crédito</option>
                                        <option value="Boleto bancário">Boleto bancário</option>
                                        <option value="Pix">Pix</option>
                                        <option value="TED">TED</option>
                                        <option value="DOC">DOC</option>
                                    </select> 

                                </div>
                            @endforeach

                            <hr>

                        </div>

                        <div class="mb-3">
                            <label>Observações internas</label>
                            <textarea wire:model="editar_entrada_obs" class="textarea-form" placeholder="Descreva observações..."></textarea>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary weight-600 py-2">Salvar Alterações</button>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <form wire:submit.prevent="addSaida" method="POST">
        <div wire:ignore.self class="modal fade" id="ModalAddSaida" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3 class="modal-title" id="exampleModalLabel">- Adicionar Saída</h3>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">

                        <div class="d-lg-flex mb-3">

                            <div class="w-100" wire:ignore>
                                <label>Referência</label>
                                <input type="text" wire:model="add_saida_ref_saida" class="form-control p-form" placeholder="Referência" required>
                            </div>

                            @if(\App\Models\Unidade::checkActive() === true && count($unidades) > 0)
                            <div class="w-100 ms-lg-3 mt-3 mt-lg-0" wire:ignore>
                                <label>Unidade</label>
                                <select wire:model="add_saida_unidade_id" id="add_saida_unidade_id" wire:change="setAddSaidaUnidadeNome($event.target.value)" class="selectpicker" data-width="100%" data-live-search="true">
                                    <option value="">Selecione</option>
                                    @foreach($unidades as $unidade)
                                        <option value="{{ $unidade->id }}">{{ $unidade->nome }}</option>
                                    @endforeach
                                </select>
                            </div>
                            @endif

                        </div>

                        <div class="d-lg-flex mb-3">

                            <div class="w-lg-50 me-lg-3 mb-3 mb-lg-0">
                                <label>Data de Vencimento</label>
                                <input type="date" wire:model="add_saida_data_vencimento" class="form-control p-form" required>
                            </div>

                            <div class="w-lg-50">
                                <label>Valor</label>
                                <input type="text" wire:model="add_saida_valor" id="add_saida_valor" class="mask_valor form-control p-form" placeholder="R$" required>
                            </div>

                            <div class="w-lg-50 mx-lg-3 my-3 my-lg-0">
                                <label>Forma de Pagamento</label>
                                <select wire:model="add_saida_forma_pagamento" class="select-form">
                                    <option value="">Selecione</option>
                                    <option value="Dinheiro">Dinheiro</option>
                                    <option value="Cartão de débito">Cartão de débito</option>
                                    <option value="Cartão de crédito">Cartão de crédito</option>
                                    <option value="Boleto bancário">Boleto bancário</option>
                                    <option value="Pix">Pix</option>
                                    <option value="TED">TED</option>
                                    <option value="DOC">DOC</option>
                                </select>
                            </div>

                            <div class="w-lg-30">
                                <label>Parcelas</label>
                                <input type="text" wire:model="add_saida_n_parcelas" wire:change="gerarParcelasAddSaida($event.target.value)" class="number form-control p-form" placeholder="N°" required>
                            </div>

                        </div>

                        <div class="mb-3 w-100">
                            <label>Link de Pagamento</label>
                            <input type="text" wire:model="add_saida_link" id="add_saida_link" class="form-control p-form" placeholder="Link de Pagamento">
                        </div>

                        <div id="boxParcelas" style="display: {{$add_saida_valueDisplayParcelas}}">
                            
                            <label class="mb-0 d-block">Parcelas</label>
                            <hr class="mt-2" />

                            <div class="mb-2 d-flex align-items-center parcelas">

                                <div class="border bg-light rounded-left-2 flex-center w-lg-20" readonly style="height: 42.5px;">
                                    N°
                                </div>

                                <input type="text" class="mx-lg-2 my-2 my-lg-0 form-control p-form" value="Data de Vencimento" readonly>
                                
                                <input type="text" class="form-control p-form" value="Valor" readonly>
                                
                                <input type="text" class="ms-lg-2 mt-2 mt-lg-0 form-control p-form" value="Forma de Pagamento" readonly>

                            </div>

                            @foreach($add_saida_parcelas as $value => $key)
                                <div class="mb-2 d-flex align-items-center parcelas">

                                    <div class="border bg-white rounded-left-2 flex-center w-lg-20" style="height: 42.5px;">
                                        {{ $loop->iteration + 1 }}
                                    </div>

                                    <input type="hidden" wire:model="add_saida_n_parcela.{{$value}}">

                                    <input type="date" wire:model="add_saida_datas_vencimento.{{$value}}" class="mx-lg-2 my-2 my-lg-0 form-control p-form" required>
                                    
                                    <input type="text" wire:model="add_saida_valores.{{$value}}" class="mask_valor form-control p-form" placeholder="R$" required>
                                    
                                    <input type="text" wire:model="add_saida_links.{{$value}}" class="ms-lg-2 mt-2 mt-lg-0 form-control p-form" placeholder="Link de Pagamento">
                                    
                                    <select wire:model="add_saida_formas_pagamento.{{$value}}" class="ms-lg-2 mt-2 mt-lg-0 select-form">
                                        <option value="">Selecione</option>
                                        <option value="Dinheiro">Dinheiro</option>
                                        <option value="Cartão de débito">Cartão de débito</option>
                                        <option value="Cartão de crédito">Cartão de crédito</option>
                                        <option value="Boleto bancário">Boleto bancário</option>
                                        <option value="Pix">Pix</option>
                                        <option value="TED">TED</option>
                                        <option value="DOC">DOC</option>
                                    </select> 

                                </div>
                            @endforeach

                            <hr>

                        </div>

                        <div class="mb-3">
                            <label>Observações internas</label>
                            <textarea wire:model="add_saida_obs" class="textarea-form" placeholder="Descreva observações..."></textarea>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-danger weight-600 py-2">Confirmar</button>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <form wire:submit.prevent="editarSaida" method="POST">
        <div wire:ignore.self class="modal fade" id="ModalEditarSaida" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3 class="modal-title" id="exampleModalLabel">- Editar Saída</h3>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">

                        <div class="d-lg-flex mb-3">

                            <div class="w-100" wire:ignore>
                                <label>Referência</label>
                                <input type="text" wire:model="editar_saida_ref_saida" class="form-control p-form" placeholder="Referência" required>
                            </div>

                            @if(\App\Models\Unidade::checkActive() === true && count($unidades) > 0)
                            <div class="w-100 ms-lg-3 mt-3 mt-lg-0" wire:ignore>
                                <label>Unidade</label>
                                <select wire:model="editar_saida_unidade_id" id="editar_saida_unidade_id" wire:change="setEditarSaidaUnidadeNome($event.target.value)" class="selectpicker" data-width="100%" data-live-search="true">
                                    <option value="">Selecione</option>
                                    @foreach($unidades as $unidade)
                                        <option value="{{ $unidade->id }}">{{ $unidade->nome }}</option>
                                    @endforeach
                                </select>
                            </div>
                            @endif

                        </div>

                        @if(isset($editar_saida) && $editar_saida !== false && $editar_saida->registro_id == null)

                            <div class="d-lg-flex mb-3">

                                <div class="w-lg-50 me-lg-3 mb-3 mb-lg-0">
                                    <label>Data de Vencimento</label>
                                    <input type="date" wire:model="editar_saida_data_vencimento" class="form-control p-form" required>
                                </div>

                                <div class="w-lg-50">
                                    <label>Valor</label>
                                    <input type="text" wire:model="editar_saida_valor" id="editar_saida_valor" class="mask_valor form-control p-form" placeholder="R$" required>
                                </div>

                                <div class="w-lg-50 mx-lg-3 my-3 my-lg-0">
                                    <label>Forma de Pagamento</label>
                                    <select wire:model="editar_saida_forma_pagamento" class="select-form">
                                        <option value="">Selecione</option>
                                        <option value="Dinheiro">Dinheiro</option>
                                        <option value="Cartão de débito">Cartão de débito</option>
                                        <option value="Cartão de crédito">Cartão de crédito</option>
                                        <option value="Boleto bancário">Boleto bancário</option>
                                        <option value="Pix">Pix</option>
                                        <option value="TED">TED</option>
                                        <option value="DOC">DOC</option>
                                    </select>
                                </div>

                                <div class="w-lg-30">
                                    <label>Parcelas</label>
                                    <input type="text" wire:model="editar_saida_n_parcelas" wire:change="gerarParcelasEditarSaida($event.target.value)" class="number form-control p-form" placeholder="N°" required @if(isset($editar_saida) && $editar_saida->registro_id != null) disabled @endif>
                                </div>

                            </div>

                            <div class="mb-3 w-100">
                                <label>Link de Pagamento</label>
                                <input type="text" wire:model="editar_saida_link" id="editar_saida_link" class="form-control p-form" placeholder="Link de Pagamento">
                            </div>

                        @endif

                        <div id="boxParcelas" style="display: {{$editar_saida_valueDisplayParcelas}}">
                            
                            <label class="mb-0 d-block">Parcelas</label>
                            <hr class="mt-2" />

                            <div class="mb-2 d-flex align-items-center parcelas">

                                <div class="border bg-light rounded-left-2 flex-center w-lg-20" readonly style="height: 42.5px;">
                                    N°
                                </div>

                                <input type="text" class="mx-lg-2 my-2 my-lg-0 form-control p-form" value="Data de Vencimento" readonly>
                                
                                <input type="text" class="form-control p-form" value="Valor" readonly>
                                
                                <input type="text" class="ms-lg-2 mt-2 mt-lg-0 form-control p-form" value="Forma de Pagamento" readonly>

                            </div>

                            @foreach($editar_saida_parcelas as $value => $key)
                                <div class="mb-2 d-flex align-items-center parcelas">

                                    <input type="hidden" wire:model="editar_saida_ids.{{$value}}">
                                    <input type="hidden" wire:model="editar_saida_registro_ids.{{$value}}">

                                    <input type="text" wire:model="editar_saida_n_parcela.{{$value}}" class="border @if(isset($key['data_vencimento']) && $key['data_vencimento'] == $editar_saida->data_vencimento) bg-primary text-light @else bg-white @endif rounded-left-2 flex-center w-lg-20 text-center" readonly style="height: 42.5px;">

                                    <input type="date" wire:model="editar_saida_datas_vencimento.{{$value}}" class="mx-lg-2 my-2 my-lg-0 form-control p-form" required>
                                    
                                    <input type="text" wire:model="editar_saida_valores.{{$value}}" class="mask_valor form-control p-form" placeholder="R$" required>
                                    
                                    <input type="text" wire:model="editar_saida_links.{{$value}}" class="ms-lg-2 mt-2 mt-lg-0 form-control p-form" placeholder="Link de Pagamento">
                                    
                                    <select wire:model="editar_saida_formas_pagamento.{{$value}}" class="ms-lg-2 mt-2 mt-lg-0 select-form">
                                        <option value="">Selecione</option>
                                        <option value="Dinheiro">Dinheiro</option>
                                        <option value="Cartão de débito">Cartão de débito</option>
                                        <option value="Cartão de crédito">Cartão de crédito</option>
                                        <option value="Boleto bancário">Boleto bancário</option>
                                        <option value="Pix">Pix</option>
                                        <option value="TED">TED</option>
                                        <option value="DOC">DOC</option>
                                    </select> 

                                </div>
                            @endforeach

                            <hr>

                        </div>

                        <div class="mb-3">
                            <label>Observações internas</label>
                            <textarea wire:model="editar_saida_obs" class="textarea-form" placeholder="Descreva observações..."></textarea>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary weight-600 py-2">Salvar Alterações</button>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <div wire:ignore.self class="modal fade" id="ModalAcoesEntrada" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            @if($view_entrada === true)
                <div class="modal-content">
                    <div class="modal-header">
                        <div>
                            <h3 class="modal-title" id="exampleModalLabel">Dados e Ações</h3>
                        </div>
                        <div>
                           <button type="button" class="btn btn-secondary weight-600 fs-14" data-bs-dismiss="modal" aria-label="Close">
                               X
                           </button>
                        </div>
                    </div>
                    <div class="modal-body">
                        <p class="text-center m-0 weight-600 fs-16 color-555 py-5">
                            Carregando...
                        </p>
                    </div>
                </div>
            @endif
            @if($view_entrada !== false && $view_entrada !== true)
                <div class="modal-content">
                    <div class="modal-header">
                        <div>
                            <h3 class="modal-title" id="exampleModalLabel">Dados e Ações</h3>
                        </div>
                        <div>                    
                            <div class="d-flex">
                                <a href="?editar_entrada_id={{$view_entrada->id}}&mes={{$_GET['mes'] ?? $mes }}&ano={{$_GET['ano'] ?? $ano }}" class="aclNivel2 btn btn-warning text-dark">
                                    <i class="fa fa-edit"></i>
                                </a>
                                <a href="#ModalDeletarEntrada" data-bs-toggle="modal" data-bs-dismiss="modal" wire:click="setIdParaDeletarEntrada({{$view_entrada->id}})" class="aclNivel3 mx-1 btn btn-danger" onclick="document.getElementById('deletar_entrada_ref').innerHTML = 'R$ {{ $view_entrada->valor }} @if($view_entrada->registro_id == null && $view_entrada->n_parcelas > 1) ({{$view_entrada->n_parcelas - 1}} outras parcelas futuras também serão deletadas) @endif';">
                                    <i class="fa fa-trash"></i>
                                </a>
                               <button type="button" class="btn btn-secondary weight-600 fs-14" data-bs-dismiss="modal" aria-label="Close">
                                   X
                               </button>
                            </div>
                        </div>
                    </div>
                    <div class="modal-body">
                        @if(isset($view_entrada) && $view_entrada !== false)
                        <ul class="list-group list-group-flush"> 
                            <li class="list-group-item">
                                <span class="weight-600 color-444">Tipo:</span>
                                <span class="weight-600">
                                    Entrada
                                </span>
                            </li>
                            <li class="list-group-item boxProdutoHide">
                                <span class="weight-600 color-444 nameProduto">Cliente:</span>
                                <a href="{{ route('admin.aluno.info', ['id' => $view_entrada->cliente_id ?? 0]) }}" class="d-block weight-600 color-blue-info-link">
                                    @foreach(explode(' / ', $view_entrada->cliente_nome) as $clienteInfo)
                                        <span class="d-block">
                                            {{ $clienteInfo }}
                                        </span>
                                    @endforeach
                                </a>
                            </li> 
                            <li class="list-group-item boxProdutoHide">
                                <span class="weight-600 color-444 nameProduto">Produto:</span>
                                <a @if($view_entrada->trilha_id) href="{{ route('admin.trilha.info', ['id' => $view_entrada->trilha_id ?? 0]) }}" @else href="{{ route('admin.turma.info', ['id' => $view_entrada->turma_id ?? 0]) }}" @endif class="weight-600 color-blue-info-link">
                                    {{ $view_entrada->produto_nome }}
                                </a>
                            </li> 
                            @if($view_entrada->unidade_id)
                            <li class="list-group-item boxProdutoHide">
                                <span class="weight-600 color-444 nameProduto">Unidade:</span>
                                <a href="{{ route('admin.unidade.info', ['id' => $view_entrada->unidade_id ?? 0]) }}" class="weight-600 color-blue-info-link">
                                    {{ $view_entrada->unidade_nome }}
                                </a>
                            </li> 
                            @endif
                            <li class="list-group-item">
                                @if($view_entrada->n_parcelas != $view_entrada->n_parcela || $view_entrada->registro_id != null)
                                    <div title="R$ {{ $view_entrada->valor_total }} divido em {{ $view_entrada->n_parcelas }} vezes.">
                                        <span class="weight-600 color-444">Valor:</span>
                                        <span class="weight-600">
                                            {{ $view_entrada->n_parcela }}/{{$view_entrada->n_parcelas}} de R$ {{ $view_entrada->valor }}
                                        </span>
                                    </div>
                                @else
                                    <div title="R$ {{ $view_entrada->valor_total }} a vista">
                                        <span class="weight-600 color-444">Valor:</span>
                                        <span class="weight-600">
                                            R$ {{ $view_entrada->valor }}
                                        </span>
                                    </div>
                                @endif
                            </li>
                            <li class="list-group-item">
                                <span class="weight-600 color-444">Forma de Pagamento:</span>
                                <span class="weight-600">
                                    {{ $view_entrada->forma_pagamento ?? 'Não definido' }}
                                </span>
                            </li>
                            <li class="list-group-item">
                                <span class="weight-600 color-444">Link de Pagamento:</span>
                                @if($view_entrada->link != null)
                                    <a href="{{ $view_entrada->link }}" target="_blank" class="weight-600 color-blue-info-link">
                                        Abrir para ver
                                    </a>
                                @else
                                    <span class="weight-600">
                                        Não definido
                                    </span>
                                @endif
                            </li>
                            <li class="list-group-item">
                                <span class="weight-600 color-444">Data de Vencimento:</span>
                                <span class="weight-600">
                                    {{ \App\Models\Util::replaceDatePt($view_entrada->data_vencimento) }}
                                </span>
                            </li>
                            <li class="list-group-item">
                                <span class="weight-600 color-444">Data de Recebimento:</span>
                                <span class="weight-600">
                                    @if($view_entrada->data_recebimento)
                                        {{ \App\Models\Util::replaceDatePt($view_entrada->data_recebimento) }}
                                    @else
                                        Ainda não recebeu
                                    @endif
                                </span>
                            </li>
                            <li class="list-group-item">
                                <span class="weight-600 color-444">Data de Cadastro:</span>
                                <span class="weight-600">
                                    {{ \App\Models\Util::replaceDateTimePt($view_entrada->created_at) }} ({{ \Carbon\Carbon::createFromTimeStamp(strtotime($view_entrada->created_at))->diffForHumans() }}) 
                                </span>
                            </li>
                            @if($view_entrada->data_estorno)
                            <li class="list-group-item">
                                <span class="weight-600 color-444">Data de Estorno:</span>
                                <span class="weight-600">
                                    {{ \App\Models\Util::replaceDatePt($view_entrada->data_estorno) }} ({{ \Carbon\Carbon::createFromTimeStamp(strtotime($view_entrada->data_estorno))->diffForHumans() }}) 
                                </span>
                            </li>
                            @endif
                            <li class="list-group-item">
                                <span class="weight-600 color-444">Vendedor:</span>
                                <a href="{{ route('admin.colaborador.info', ['id' => $view_entrada->vendedor_id ?? 0]) }}" class="weight-600 color-blue-info-link">
                                    {{ $view_entrada->vendedor_nome ?? 'Não definido' }}
                                </a>
                            </li>   
                            <li class="list-group-item">
                                <span class="weight-600 color-444">Cadastrante:</span>
                                <a href="{{ route('admin.colaborador.info', ['id' => $view_entrada->cadastrante_id ?? 0]) }}" class="weight-600 color-blue-info-link">
                                    {{ $view_entrada->cadastrante_nome ?? 'Não definido' }}
                                </a>
                            </li>
                            @if($view_entrada->obs)
                            <li class="list-group-item">
                                <span class="weight-600 color-444">Observações internas:</span>
                                <span class="weight-600">
                                    {!! nl2br($view_entrada->obs) !!}
                                </span>
                            </li>
                            @endif
                            <li class="list-group-item">
                                <span class="weight-600 color-444">Status:</span>
                                @switch($view_entrada->status)
                                    @case(0)
                                        @if(date('Y-m-d') > $view_entrada->data_vencimento)
                                            <span class="text-danger weight-700">
                                                Inadimplente
                                            </span>
                                        @endif
                                        @if(date('Y-m-d') <= $view_entrada->data_vencimento)
                                            <span class="text-info weight-700">
                                                A receber
                                            </span>
                                        @endif
                                    @break
                                    @case(1)
                                        <span class="text-success weight-700">
                                            Recebido
                                        </span>
                                    @break
                                    @case(2)
                                        <span class="text-inadimplente weight-700">
                                            Estornado
                                        </span>
                                    @break
                                @endswitch
                            </li>
                            <li class="list-group-item">
                                <div class="d-lg-flex w-100 mt-1">
                                    @if($view_entrada->status == 0)
                                        <a href="#ModalMarcarVendaComoRecebida" data-bs-toggle="modal" data-bs-dismiss="modal" wire:click="setIdParaMarcarVendaComoRecebida({{$view_entrada->id}})" class="d-block mb-2 mb-lg-0 btn btn-success weight-600 py-2-5 w-100" onclick="document.getElementById('marcarVendaComoRecebidaRef').innerHTML = 'R$ {{ $view_entrada->valor }}';">
                                            <i class="far fa-check-circle"></i>
                                            Marcar venda como Recebida
                                        </a>
                                    @endif
                                    @if($view_entrada->status == 1)
                                    <a href="#ModalMarcarVendaComoEstornada" data-bs-toggle="modal" data-bs-dismiss="modal" wire:click="setIdParaMarcarVendaComoEstornada({{$view_entrada->id}})" class="d-block btn bg-inadimplente text-light weight-600 py-2-5 w-100 ms-lg-2" onclick="document.getElementById('marcarVendaComoEstornadaRef').innerHTML = 'R$ {{ $view_entrada->valor }}';">
                                        <i class="far fa-check-circle"></i>
                                        Marcar venda como Estornada
                                    </a>
                                    @endif
                                </div>
                            </li>
                        </ul>
                        @endif
                    </div>
                </div>
            @endif
            @if($view_entrada === false)
                <div class="modal-content">
                    <div class="modal-header">
                        <div>
                            <h3 class="modal-title" id="exampleModalLabel">Dados e Ações</h3>
                        </div>
                        <div>
                           <button type="button" class="btn btn-secondary weight-600 fs-14" data-bs-dismiss="modal" aria-label="Close">
                               X
                           </button>
                        </div>
                    </div>
                    <div class="modal-body">
                        <p class="text-center m-0 weight-600 fs-16 color-555 py-5">
                            Entrada não encontrada.
                        </p>
                    </div>
                </div>
            @endif
        </div>
    </div>

    <div wire:ignore.self class="modal fade" id="ModalAcoesSaida" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            @if($view_saida === true)
                <div class="modal-content">
                    <div class="modal-header">
                        <div>
                            <h3 class="modal-title" id="exampleModalLabel">Dados e Ações</h3>
                        </div>
                        <div>
                           <button type="button" class="btn btn-secondary weight-600 fs-14" data-bs-dismiss="modal" aria-label="Close">
                               X
                           </button>
                        </div>
                    </div>
                    <div class="modal-body">
                        <p class="text-center m-0 weight-600 fs-16 color-555 py-5">
                            Carregando...
                        </p>
                    </div>
                </div>
            @endif
            @if($view_saida !== false && $view_saida !== true)
                <div class="modal-content">
                    <div class="modal-header">
                        <div>
                            <h3 class="modal-title" id="exampleModalLabel">Dados e Ações</h3>
                        </div>
                        <div>                    
                            <div class="d-flex">
                                <a href="?editar_saida_id={{$view_saida->id}}&mes={{$_GET['mes'] ?? $mes }}&ano={{$_GET['ano'] ?? $ano }}" class="aclNivel2 btn btn-warning text-dark">
                                    <i class="fa fa-edit"></i>
                                </a>
                                <a href="#ModalDeletarSaida" data-bs-toggle="modal" data-bs-dismiss="modal" wire:click="setIdParaDeletarSaida({{$view_saida->id}})" class="aclNivel3 mx-1 btn btn-danger" onclick="document.getElementById('deletar_saida_ref').innerHTML = ' R$ {{ $view_saida->valor }} @if($view_saida->registro_id == null && $view_saida->n_parcelas > 1) ({{$view_saida->n_parcelas - 1}} outras parcelas futuras também serão deletadas) @endif';">
                                    <i class="fa fa-trash"></i>
                                </a>
                               <button type="button" class="btn btn-secondary weight-600 fs-14" data-bs-dismiss="modal" aria-label="Close">
                                   X
                               </button>
                            </div>
                        </div>
                    </div>
                    <div class="modal-body">
                        @if(isset($view_saida) && $view_saida !== false)
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item">
                                <span class="weight-600 color-444">Tipo:</span>
                                <span class="weight-600">
                                    Saída
                                </span>
                            </li>
                            <li class="list-group-item">
                                <span class="weight-600 color-444">Referência:</span>
                                <span class="weight-600">
                                    {{ $view_saida->ref_saida ?? 'Não definido' }}
                                </span>
                            </li>
                            @if($view_saida->unidade_id)
                            <li class="list-group-item boxProdutoHide">
                                <span class="weight-600 color-444 nameProduto">Unidade:</span>
                                <a href="{{ route('admin.unidade.info', ['id' => $view_saida->unidade_id ?? 0]) }}" class="weight-600 color-blue-info-link">
                                    {{ $view_saida->unidade_nome }}
                                </a>
                            </li> 
                            @endif
                            <li class="list-group-item">
                                @if($view_saida->n_parcelas != $view_saida->n_parcela || $view_saida->registro_id != null)
                                    <div title="R$ {{ $view_saida->valor_total }} divido em {{ $view_saida->n_parcelas }} vezes.">
                                        <span class="weight-600 color-444">Valor:</span>
                                        <span class="weight-600">
                                            {{ $view_saida->n_parcela }}/{{$view_saida->n_parcelas}} de R$ {{ $view_saida->valor }}
                                        </span>
                                    </div>
                                @else
                                    <div title="R$ {{ $view_saida->valor_total }} a vista">
                                        <span class="weight-600 color-444">Valor:</span>
                                        <span class="weight-600">
                                            R$ {{ $view_saida->valor }}
                                        </span>
                                    </div>
                                @endif
                            </li>
                            <li class="list-group-item">
                                <span class="weight-600 color-444">Forma de Pagamento:</span>
                                <span class="weight-600">
                                    {{ $view_saida->forma_pagamento ?? 'Não definido' }}
                                </span>
                            </li>
                            <li class="list-group-item">
                                <span class="weight-600 color-444">Link de Pagamento:</span>
                                @if($view_saida->link != null)
                                    <a href="{{ $view_saida->link }}" target="_blank" class="weight-600 color-blue-info-link">
                                        Abrir para ver
                                    </a>
                                @else
                                    <span class="weight-600">
                                        Não definido
                                    </span>
                                @endif
                            </li>
                            <li class="list-group-item">
                                <span class="weight-600 color-444">Data de Vencimento:</span>
                                <span class="weight-600">
                                    {{ \App\Models\Util::replaceDatePt($view_saida->data_vencimento) }}
                                </span>
                            </li>
                            <li class="list-group-item">
                                <span class="weight-600 color-444">Data de Pagamento:</span>
                                <span class="weight-600">
                                    @if($view_saida->data_recebimento)
                                        {{ \App\Models\Util::replaceDatePt($view_saida->data_recebimento) }}
                                    @else
                                        Ainda não pagou
                                    @endif
                                </span>
                            </li>
                            <li class="list-group-item">
                                <span class="weight-600 color-444">Data de Cadastro:</span>
                                <span class="weight-600">
                                    {{ \App\Models\Util::replaceDateTimePt($view_saida->created_at) }} ({{ \Carbon\Carbon::createFromTimeStamp(strtotime($view_saida->created_at))->diffForHumans() }}) 
                                </span>
                            </li>   
                            <li class="list-group-item">
                                <span class="weight-600 color-444">Cadastrante:</span>
                                <a href="{{ route('admin.colaborador.info', ['id' => $view_saida->cadastrante_id ?? 0]) }}" class="weight-600 color-blue-info-link">
                                    {{ $view_saida->cadastrante_nome ?? 'Não definido' }}
                                </a>
                            </li>
                            @if($view_saida->obs)
                            <li class="list-group-item">
                                <span class="weight-600 color-444">Observações internas:</span>
                                <span class="weight-600">
                                    {!! nl2br($view_saida->obs) !!}
                                </span>
                            </li>
                            @endif
                            <li class="list-group-item">
                                <span class="weight-600 color-444">Status:</span>
                                @switch($view_saida->status)
                                    @case(0)
                                        @if(date('Y-m-d') > $view_saida->data_vencimento)
                                            <span class="text-danger weight-700">
                                                Em atraso
                                            </span>
                                        @endif
                                        @if(date('Y-m-d') <= $view_saida->data_vencimento)
                                            <span class="text-secondary weight-700">
                                                A pagar
                                            </span>
                                        @endif
                                    @break
                                    @case(1)
                                        <span class="text-success weight-700">
                                            Pago
                                        </span>
                                    @break
                                @endswitch
                            </li>
                            @if($view_saida->status != 1)
                            <li class="list-group-item">
                                <div class="d-lg-flex w-100 mt-1">
                                    <a href="#ModalMarcarDespesaComoPaga" wire:click="setIdParaMarcarDespesaComoPaga({{$view_saida->id}})" data-bs-toggle="modal" data-bs-dismiss="modal" class="aclNivel2 d-block mb-2 mb-lg-0 btn btn-success weight-600 py-2-5 w-100" onclick="document.getElementById('marcarDespesaComoPagaRef').innerHTML = 'R$ {{ $view_saida->valor }}';">
                                        <i class="far fa-check-circle"></i>
                                        Marcar despesa como Paga
                                    </a>
                                </div>
                            </li>
                            @endif
                        </ul>
                        @endif
                    </div>
                </div>
            @endif
            @if($view_saida === false)
                <div class="modal-content">
                    <div class="modal-header">
                        <div>
                            <h3 class="modal-title" id="exampleModalLabel">Dados e Ações</h3>
                        </div>
                        <div>
                           <button type="button" class="btn btn-secondary weight-600 fs-14" data-bs-dismiss="modal" aria-label="Close">
                               X
                           </button>
                        </div>
                    </div>
                    <div class="modal-body">
                        <p class="text-center m-0 weight-600 fs-16 color-555 py-5">
                            Saída não encontrada.
                        </p>
                    </div>
                </div>
            @endif
        </div>
    </div>

    <form wire:submit.prevent="marcarVendaComoRecebida" method="POST">
        <div wire:ignore class="modal fade" id="ModalMarcarVendaComoRecebida" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3 class="modal-title" id="exampleModalLabel">Marcar Venda como Recebida</h3>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body text-center py-5">

                        <p id="marcarVendaComoRecebidaRef" class="m-0 fs-15 weight-600">
                            
                        </p>

                        <hr/>
                        <p class="m-0 fs-15">
                            Você tem certeza que deseja marcar esta venda como recebida?
                        </p>
                    </div>

                    <div class="modal-footer">
                        <div class="d-flex align-items-end w-100">
                            
                            <div class="w-100 me-3">
                                <label>Data de Recebimento</label>
                                <input type="date" wire:model="editar_entrada_data_recebimento" class="w-100 form-control p-form" required>
                            </div>
                            <div>
                                <button type="submit" class="btn btn-primary weight-600 py-2">Confirmar</button>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <form wire:submit.prevent="marcarDespesaComoPaga" method="POST">
        <div wire:ignore class="modal fade" id="ModalMarcarDespesaComoPaga" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3 class="modal-title" id="exampleModalLabel">Marcar Despesa como Paga</h3>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body text-center py-5">

                        <p id="marcarDespesaComoPagaRef" class="m-0 fs-15 weight-600">
                            
                        </p>

                        <hr/>
                        <p class="m-0 fs-15">
                            Você tem certeza que deseja marcar esta despesa como paga?
                        </p>

                    </div>
                    <div class="modal-footer">
                        <div class="d-flex align-items-end w-100">
                            
                            <div class="w-100 me-3">
                                <label>Data de Pagamento</label>
                                <input type="date" wire:model="editar_saida_data_recebimento" class="w-100 form-control p-form" required>
                            </div>
                            <div>
                                <button type="submit" class="btn btn-primary weight-600 py-2">Confirmar</button>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <form wire:submit.prevent="marcarVendaComoEstornada" method="POST">
        <div wire:ignore class="modal fade" id="ModalMarcarVendaComoEstornada" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3 class="modal-title" id="exampleModalLabel">Marcar Venda como Estornada</h3>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body text-center py-5">

                        <p id="marcarVendaComoEstornadaRef" class="m-0 fs-15 weight-600">
                            
                        </p>

                        <hr/>
                        <p class="m-0 fs-15">
                            Você tem certeza que deseja marcar esta venda como estornada?
                        </p>

                    </div>
                    <div class="modal-footer">
                        <div class="d-flex align-items-end w-100">
                            
                            <div class="w-100 me-3">
                                <label>Data de Estorno</label>
                                <input type="date" wire:model="editar_entrada_data_estorno" class="w-100 form-control p-form" required>
                            </div>
                            <div>
                                <button type="submit" class="btn btn-primary weight-600 py-2">Confirmar</button>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <form wire:submit.prevent="deletarEntrada" method="POST">
        <div wire:ignore class="modal fade" id="ModalDeletarEntrada" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3 class="modal-title" id="exampleModalLabel">Deletar Venda</h3>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body text-center py-5">

                        <p id="deletar_entrada_ref" class="m-0 fs-15 weight-600">
                            
                        </p>

                        <hr/>

                        <p class="m-0 fs-15">
                            Você tem certeza que deseja deletar esta venda?
                            <br>
                            Todas as informações também serão deletadas definitivamente.
                        </p>
                    </div>
                    <div class="modal-footer">
                        <div class="d-flex w-100">
                            <input type="password" wire:model="password_to_delete" class="w-100 form-control p-form me-3" placeholder="Digite sua senha" required>
                            <button type="submit" class="btn btn-danger weight-600">Confirmar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <form wire:submit.prevent="deletarSaida" method="POST">
        <div wire:ignore class="modal fade" id="ModalDeletarSaida" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3 class="modal-title" id="exampleModalLabel">Deletar Despesa</h3>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body text-center py-5">

                        <p id="deletar_saida_ref" class="m-0 fs-15 weight-600">
                            
                        </p>

                        <hr/>

                        <p class="m-0 fs-15">
                            Você tem certeza que deseja deletar esta despesa?
                            <br>
                            Todas as informações também serão deletadas definitivamente.
                        </p>
                    </div>
                    <div class="modal-footer">
                        <div class="d-flex w-100">
                            <input type="password" wire:model="password_to_delete" class="w-100 form-control p-form me-3" placeholder="Digite sua senha" required>
                            <button type="submit" class="btn btn-danger weight-600">Confirmar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>

    @include('livewire.util.toast')

</div>