<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTrilhasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trilhas', function (Blueprint $table) {
            $table->id();
            $table->integer('plataforma_id');
            $table->integer('cadastrante_id')->nullable();
            $table->integer('formulario_id')->nullable();
            $table->integer('certificado_id')->nullable();
            $table->integer('atestado_id')->nullable();
            $table->integer('unidade_id')->nullable();
            $table->integer('coordenador_id')->nullable();
            $table->string('nome')->nullable();
            $table->string('referencia')->nullable();
            $table->string('slug')->nullable();
            $table->string('tipo_inscricao')->nullable();
            $table->text('foto_capa')->nullable();
            $table->text('foto_miniatura')->nullable();
            $table->text('matriz_curricular')->nullable();
            $table->string('publicar', 1)->default('N');
            $table->text('video')->nullable();
            $table->string('exibir_turmas_como_cc', 1)->default('N');
            $table->string('carga_horaria')->nullable();
            $table->string('duracao')->nullable();
            $table->string('nivel')->nullable();
            $table->string('tipo_formacao')->nullable();
            $table->string('modalidade')->nullable();
            $table->string('area_negocio')->nullable();
            $table->string('slogan')->nullable();
            $table->text('descricao')->nullable();
            $table->string('tipo_valor', 1)->nullable();
            $table->string('tipo_mensalidade', 1)->nullable();
            $table->string('valor_parcelado')->nullable();
            $table->string('valor_a_vista')->nullable();
            $table->string('valor_mensalidade')->nullable();
            $table->string('valor_promocional')->nullable();
            $table->string('desconto_agilpass')->nullable();
            $table->text('seo_title')->nullable();
            $table->text('seo_keywords')->nullable();
            $table->text('seo_description')->nullable();
            $table->integer('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trilhas');
    }
}
