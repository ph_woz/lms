@extends('layouts.admin.master')
@section('content')
    
	@livewire('admin.responsavel.add', ['aluno_id' => $aluno_id])

@endsection

@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.js"></script>
<script>
	
	$(".cpf").mask('000.000.000-00');
	$(".cep").mask('00000-000');

</script>
@endpush
