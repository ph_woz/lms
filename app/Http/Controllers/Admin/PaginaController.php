<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Models\PaginaConteudo;
use App\Models\Parceiro;
use App\Models\LinkAcademico;
use App\Models\BlogPost;
use App\Models\BlogPostCategoria;
use App\Models\SecaoDescritiva;

class PaginaController extends Controller
{
    public function lista()
    {
        return view('admin.pagina.lista');
    }

    public function home()
    {
        $conteudo = PaginaConteudo::plataforma()->where('url','/')->first();

        $banners = \App\Models\Banner::plataforma()->orderBy('ordem')->whereNotNull('link')->get();
        $depoimentos = \App\Models\Depoimento::plataforma()->orderBy('ordem')->get();

        if(request('sent'))
        {
            $conteudo = PaginaConteudo::updateOrCreate(
                ['plataforma_id' => session('plataforma_id'), 'url' => '/'], 
                request()->all()
            );

            return back()->with('success', 'Página atualizada com êxito.');
        }

        if(request()->hasFile('upload_banner'))
        {
            $banner = \App\Models\Banner::create(request()->all() + ['plataforma_id' => session('plataforma_id')]);
            
            if(request()->hasFile('upload_banner'))
            {
                $this->syncFoto($banner, 'link', 'upload_banner');
            }

            return redirect('/admin/pagina/editar/home?editar_banner='.$banner->id)->with('success', 'Banner adicionado com êxito.');
        }

        if(request('editarBanner'))
        {
            \App\Models\Banner::plataforma()->where('id', request('banner_id'))->update(request()->only('titulo','subtitulo','link','botao_texto','botao_link','overlay','overlay_color','overlay_opacity','ordem','status'));

            return redirect()->route('admin.pagina.editar.home')->with('success', 'Banner atualizado com êxito.');
        }

        if(request('deletarBanner'))
        {
            $banner = \App\Models\Banner::plataforma()->find(request('delete_id'));

            $path = str_replace(\App\Models\Util::getLinkStorage(), '', $banner->link);
            Storage::delete($path);

            $banner->delete();

            return redirect()->route('admin.pagina.editar.home')->with('success', 'Banner deletado com êxito.');
        }

        if(request('addDepoimento'))
        {
            $depoimento = \App\Models\Depoimento::create(request()->all());

            if(request()->hasFile('foto_resp'))
            {
                $this->syncFotoDepoimento($depoimento, 'foto', 'foto_resp');
            }

            return redirect()->route('admin.pagina.editar.home')->with('success', 'Depoimento adiconado com êxito.');
        }

        if(request('editarDepoimento'))
        {
            $depoimento = \App\Models\Depoimento::plataforma()->where('id', request('depoimento_id'))->first();
            $depoimento->update(request()->only('nome','ordem','status','descricao'));

            if(request()->hasFile('foto_resp'))
            {
                $this->syncFotoDepoimento($depoimento, 'foto', 'foto_resp');
            }

            return redirect()->route('admin.pagina.editar.home')->with('success', 'Depoimento atualizado com êxito.');
        }

        if(request('deletarDepoimento'))
        {
            $depoimento = \App\Models\Depoimento::plataforma()->find(request('depoimento_delete_id'));

            $path = str_replace(\App\Models\Util::getLinkStorage(), '', $depoimento->foto);
            Storage::delete($path);

            $depoimento->delete();

            return redirect()->route('admin.pagina.editar.home')->with('success', 'Depoimento deletado com êxito.');
        }

        return view('admin.pagina.editar.home', compact('conteudo','banners','depoimentos') );
    }

    public function blog()
    {
        return view('admin.pagina.editar.blog.lista');
    }

    public function blogPostAdd()
    {
        $publicar_e_destaque = explode('-', request('publicar_e_destaque'));
        $publicar = $publicar_e_destaque[0];
        $destaque = $publicar_e_destaque[1];

        $destaque_ordem = null;
        
        if($destaque == 'S')
        {
            $destaque_ordem = request('destaque_ordem') ?? 1;
        }

        $post = BlogPost::create(request()->all() + [
            'slug'           => \Str::slug(request('titulo')),
            'publicar'       => $publicar,
            'destaque_ordem' => $destaque_ordem,
        ]);

        $this->syncFotoPost($post, 'foto_capa', 'foto');
        $this->syncCategoriasPost($post->id);

        return back()->with('success', 'Post criado com êxito.');
    }

    public function blogPostEditar()
    {
        $publicar_e_destaque = explode('-', request('publicar_e_destaque'));
        $publicar = $publicar_e_destaque[0];
        $destaque = $publicar_e_destaque[1];

        $destaque_ordem = null;
        
        if($destaque == 'S')
        {
            $destaque_ordem = request('destaque_ordem') ?? 1;
        }

        $post = BlogPost::where('id', request('editar_post_id'))->first();

        $post->update([
            'slug'            => \Str::slug(request('titulo')),
            'publicar'        => $publicar,
            'destaque_ordem'  => $destaque_ordem,
            'autor_id'        => request('autor_id'),
            'titulo'          => request('titulo'),
            'descricao'       => request('descricao'),
            'seo_title'       => request('seo_title'),
            'seo_keywords'    => request('seo_keywords'),
            'seo_description' => request('seo_description'),

        ]);

        $this->syncFotoPost($post, 'foto_capa', 'editar_foto');
        $this->syncCategoriasPost($post->id);

        return back()->with('success', 'Post atualizado com êxito.');
    }

    public function syncCategoriasPost($post_id)
    {
        BlogPostCategoria::plataforma()->where('post_id', $post_id)->delete();

        if(request('categorias') == null)
        {
            return;
        }

        foreach(request('categorias') as $categoria_id)
        {
            BlogPostCategoria::create([
                'plataforma_id' => session('plataforma_id'),
                'post_id'       => $post_id, 
                'categoria_id'  => $categoria_id,
            ]);
        }
    }

    public function cursos()
    {
        $conteudo = PaginaConteudo::plataforma()->where('url','/cursos')->first();

        if(request('sent'))
        {
            $conteudo = PaginaConteudo::updateOrCreate(
                ['plataforma_id' => session('plataforma_id'), 'url' => '/cursos'], 
                request()->all()
            );

            return back()->with('success', 'Página atualizada com êxito.');
        }

        return view('admin.pagina.editar.cursos', compact('conteudo') );
    }

    public function sobreNos()
    {
        $conteudo = PaginaConteudo::plataforma()->where('url','/sobre-nos')->first();

    	if(request('sent'))
    	{
    		$conteudo = PaginaConteudo::updateOrCreate(
    			['plataforma_id' => session('plataforma_id'), 'url' => '/sobre-nos'], 
                request()->all()
    		);

    		return back()->with('success', 'Página atualizada com êxito.');
    	}

        return view('admin.pagina.editar.sobre-nos', compact('conteudo') );
    }

    public function comoFunciona()
    {
        $conteudo = PaginaConteudo::plataforma()->where('url','/como-funciona')->first();

        if(request('sent'))
        {
            $conteudo = PaginaConteudo::updateOrCreate(
                ['plataforma_id' => session('plataforma_id'), 'url' => '/como-funciona'], 
                request()->all()
            );

            return back()->with('success', 'Página atualizada com êxito.');
        }

        return view('admin.pagina.editar.como-funciona', compact('conteudo') );
    }

    public function contato()
    {
        $conteudo = PaginaConteudo::plataforma()->where('url','/contato')->first();

        if(request('sent'))
        {
            $conteudo = PaginaConteudo::updateOrCreate(
                ['plataforma_id' => session('plataforma_id'), 'url' => '/contato'], 
                request()->all()
            );

            return back()->with('success', 'Página atualizada com êxito.');
        }

        return view('admin.pagina.editar.contato', compact('conteudo') );
    }

    public function termosDeUso()
    {
        $conteudo = PaginaConteudo::plataforma()->where('url','/termos-de-uso')->first();

        if(request('sent'))
        {
            $conteudo = PaginaConteudo::updateOrCreate(
                ['plataforma_id' => session('plataforma_id'), 'url' => '/termos-de-uso'], 
                request()->all()
            );

            return back()->with('success', 'Página atualizada com êxito.');
        }

        return view('admin.pagina.editar.termos-de-uso', compact('conteudo') );
    }

    public function biblioteca()
    {
        $conteudo = PaginaConteudo::plataforma()->where('url','/biblioteca')->first();

        if(request('sent'))
        {
            $conteudo = PaginaConteudo::updateOrCreate(
                ['plataforma_id' => session('plataforma_id'), 'url' => '/biblioteca'], 
                request()->all()
            );

            return back()->with('success', 'Página atualizada com êxito.');
        }

        return view('admin.pagina.editar.biblioteca', compact('conteudo') );
    }

    public function politicaDePrivacidade()
    {
        $conteudo = PaginaConteudo::plataforma()->where('url','/politica-de-privacidade')->first();

        if(request('sent'))
        {
            $conteudo = PaginaConteudo::updateOrCreate(
                ['plataforma_id' => session('plataforma_id'), 'url' => '/politica-de-privacidade'], 
                request()->all()
            );

            return back()->with('success', 'Página atualizada com êxito.');
        }

        return view('admin.pagina.editar.politica-de-privacidade', compact('conteudo') );
    }

    public function editais()
    {
        $conteudo = PaginaConteudo::plataforma()->where('url','/editais')->first();

        if(request('sent'))
        {
            $conteudo = PaginaConteudo::updateOrCreate(
                ['plataforma_id' => session('plataforma_id'), 'url' => '/editais'], 
                request()->all()
            );

            return back()->with('success', 'Página atualizada com êxito.');
        }

        return view('admin.pagina.editar.editais', compact('conteudo') );
    }

    public function dou()
    {
        $conteudo = PaginaConteudo::plataforma()->where('url','/dou')->first();

        if(request('sent'))
        {
            $conteudo = PaginaConteudo::updateOrCreate(
                ['plataforma_id' => session('plataforma_id'), 'url' => '/dou'], 
                request()->all()
            );

            return back()->with('success', 'Página atualizada com êxito.');
        }

        return view('admin.pagina.editar.dou', compact('conteudo') );
    }

    public function ouvidoria()
    {
        $conteudo = PaginaConteudo::plataforma()->where('url','/ouvidoria')->first();

        if(request('sent'))
        {
            $conteudo = PaginaConteudo::updateOrCreate(
                ['plataforma_id' => session('plataforma_id'), 'url' => '/ouvidoria'], 
                request()->all()
            );

            return back()->with('success', 'Página atualizada com êxito.');
        }

        return view('admin.pagina.editar.ouvidoria', compact('conteudo') );
    }

    public function cpa()
    {
        $conteudo = PaginaConteudo::plataforma()->where('url','/cpa')->first();

        if(request('sent'))
        {
            $conteudo = PaginaConteudo::updateOrCreate(
                ['plataforma_id' => session('plataforma_id'), 'url' => '/cpa'], 
                request()->all()
            );

            return back()->with('success', 'Página atualizada com êxito.');
        }

        return view('admin.pagina.editar.cpa', compact('conteudo') );
    }

    public function fies()
    {
        $conteudo = PaginaConteudo::plataforma()->where('url','/fies')->first();

        $secoes = SecaoDescritiva::plataforma()->fies()->orderBy('ordem')->get();

        $editarSecao = SecaoDescritiva::plataforma()->where('id', $_GET['editar_secao_id'] ?? 0)->first();

        if(request('add'))
        {
            $secao = SecaoDescritiva::create([
                'tipo'        => 'FIES',
                'registro_id' => 0,
                'titulo'      => request('titulo'),
                'expandido'   => request('expandido'),
                'descricao'   => request('descricao'),
                'ordem'       => request('ordem'),
            ]);

            return back()->with('success', 'Seção cadastrada com êxito.');
        }

        if(request('editar'))
        {
            $editarSecao->update([
                'titulo'      => request('titulo'),
                'expandido'   => request('expandido'),
                'descricao'   => request('descricao'),
                'ordem'       => request('ordem'),
            ]);

            return back()->with('success', 'Seção atualizada com êxito.');
        }

        if(request('deletar'))
        {
            $secao = SecaoDescritiva::plataforma()->fies()
                ->where('registro_id', 0)
                ->where('id', request('deletar_id'))
                ->delete();

            return back()->with('success', 'Seção deletado com êxito.');
        }

        if(request('sent'))
        {
            $conteudo = PaginaConteudo::updateOrCreate(
                ['plataforma_id' => session('plataforma_id'), 'url' => '/fies'], 
                request()->all()
            );

            return back()->with('success', 'Página atualizada com êxito.');
        }

        return view('admin.pagina.editar.fies', compact('conteudo','secoes','editarSecao') );
    }

    public function prouni()
    {
        $conteudo = PaginaConteudo::plataforma()->where('url','/prouni')->first();

        $secoes = SecaoDescritiva::plataforma()->prouni()->orderBy('ordem')->get();

        $editarSecao = SecaoDescritiva::plataforma()->where('id', $_GET['editar_secao_id'] ?? 0)->first();

        if(request('add'))
        {
            $secao = SecaoDescritiva::create([
                'tipo'        => 'ProUni',
                'registro_id' => 0,
                'titulo'      => request('titulo'),
                'expandido'   => request('expandido'),
                'descricao'   => request('descricao'),
                'ordem'       => request('ordem'),
            ]);

            return back()->with('success', 'Seção cadastrada com êxito.');
        }

        if(request('editar'))
        {
            $editarSecao->update([
                'titulo'      => request('titulo'),
                'expandido'   => request('expandido'),
                'descricao'   => request('descricao'),
                'ordem'       => request('ordem'),
            ]);

            return back()->with('success', 'Seção atualizada com êxito.');
        }

        if(request('deletar'))
        {
            $secao = SecaoDescritiva::plataforma()->prouni()
                ->where('registro_id', 0)
                ->where('id', request('deletar_id'))
                ->delete();

            return back()->with('success', 'Seção deletado com êxito.');
        }

        if(request('sent'))
        {
            $conteudo = PaginaConteudo::updateOrCreate(
                ['plataforma_id' => session('plataforma_id'), 'url' => '/prouni'], 
                request()->all()
            );

            return back()->with('success', 'Página atualizada com êxito.');
        }

        return view('admin.pagina.editar.prouni', compact('conteudo','secoes','editarSecao') );
    }

    public function historicoEscolar()
    {
        $conteudo = PaginaConteudo::plataforma()->where('url','/aluno/info/0/historico-escolar')->first();

        if(request('sent'))
        {
            $conteudo = PaginaConteudo::updateOrCreate(
                ['plataforma_id' => session('plataforma_id'), 'url' => '/aluno/info/0/historico-escolar'], 
                request()->all()
            );

            return back()->with('success', 'Página atualizada com êxito.');
        }

        return view('admin.pagina.editar.historico-escolar', compact('conteudo') );
    }    

    public function enem()
    {
        $conteudo = PaginaConteudo::plataforma()->where('url','/enem')->first();

        if(request('sent'))
        {
            $conteudo = PaginaConteudo::updateOrCreate(
                ['plataforma_id' => session('plataforma_id'), 'url' => '/enem'], 
                request()->all()
            );

            return back()->with('success', 'Página atualizada com êxito.');
        }

        return view('admin.pagina.editar.enem', compact('conteudo') );
    }

    public function transferencia()
    {
        $conteudo = PaginaConteudo::plataforma()->where('url','/transferencia')->first();

        if(request('sent'))
        {
            $conteudo = PaginaConteudo::updateOrCreate(
                ['plataforma_id' => session('plataforma_id'), 'url' => '/transferencia'], 
                request()->all()
            );

            return back()->with('success', 'Página atualizada com êxito.');
        }

        return view('admin.pagina.editar.transferencia', compact('conteudo') );
    }

    public function vestibular()
    {
        $conteudo = PaginaConteudo::plataforma()->where('url','/vestibular')->first();

        if(request('sent'))
        {
            $conteudo = PaginaConteudo::updateOrCreate(
                ['plataforma_id' => session('plataforma_id'), 'url' => '/vestibular'], 
                request()->all()
            );

            return back()->with('success', 'Página atualizada com êxito.');
        }

        return view('admin.pagina.editar.vestibular', compact('conteudo') );
    }

    public function polos()
    {
        $conteudo = PaginaConteudo::plataforma()->where('url','/polos')->first();

        if(request('sent'))
        {
            $conteudo = PaginaConteudo::updateOrCreate(
                ['plataforma_id' => session('plataforma_id'), 'url' => '/polos'], 
                request()->all()
            );

            return back()->with('success', 'Página atualizada com êxito.');
        }

        return view('admin.pagina.editar.polos', compact('conteudo') );
    }

    public function parceiros()
    {
        $parceiros = Parceiro::plataforma()->orderBy('ordem')->get();

        $conteudo = PaginaConteudo::plataforma()->where('url','/parceiros')->first();

        if(request('sent'))
        {
            $conteudo = PaginaConteudo::updateOrCreate(
                ['plataforma_id' => session('plataforma_id'), 'url' => '/parceiros'], 
                request()->all()
            );

            return back()->with('success', 'Página atualizada com êxito.');
        }

        if(request('add'))
        {
            $parceiro = Parceiro::create(request()->all());

            $this->syncFotoParceiro($parceiro, 'logotipo', 'logo');

            return back()->with('success', 'Parceiro adicionado com êxito.');
        }

        if(request('editar'))
        {
            $parceiro = Parceiro::where('id', request('editar_id'))->first();

            $parceiro->update(request()->only('nome','site','publicar','ordem'));

            $this->syncFotoParceiro($parceiro, 'logotipo', 'logo');

            return back()->with('success', 'Parceiro atualizado com êxito.');
        }

        if(request('delete'))
        {
            $parceiro = Parceiro::plataforma()->where('id', request('delete_id'))->first();

            $path = str_replace(\App\Models\Util::getLinkStorage(), '', $parceiro->logotipo);
            Storage::delete($path);

            $parceiro->delete();

            return back()->with('success', 'Parceiro deletado com êxito.');
        }

        return view('admin.pagina.editar.parceiros', compact('conteudo','parceiros') );
    }

    public function alunoAcademico()
    {
        $links = LinkAcademico::plataforma()->orderBy('ordem')->get();

        if(request('add'))
        {
            LinkAcademico::create(request()->all());

            return back()->with('success', 'Link adicionado com êxito.');
        }

        if(request('editar'))
        {
            LinkAcademico::plataforma()->where('id', request('editar_id'))->update(request()->only('titulo','link','ordem'));

            return back()->with('success', 'Link atualizado com êxito.');
        }

        if(request('delete'))
        {
            LinkAcademico::plataforma()->where('id', request('delete_id'))->delete();

            return back()->with('success', 'Link deletado com êxito.');
        }

        return view('admin.pagina.editar.aluno-academico', compact('links') );
    }

    public function syncFoto($banner, $tipo, $inputFileName)
    {
        $file = request()->file($inputFileName);

        if($file)
        {
            $path = str_replace(\App\Models\Util::getLinkStorage(), '', $banner->$tipo);
            Storage::delete($path);

            $diretorio = 'plataformas/'.session('plataforma_id').'/banners';
            $filename = $banner->id.'.'.$file->getClientOriginalExtension();

            $path = Storage::putFileAs($diretorio, $file, $filename, 'public');
            $linkStorage = \App\Models\Util::getLinkStorage();
            $caminho = $linkStorage . $path;

            $banner->update([$tipo => $caminho]);
        }
    }

    public function syncFotoDepoimento($depoimento, $tipo, $inputFileName)
    {
        $file = request()->file($inputFileName);

        if($file)
        {
            $path = str_replace(\App\Models\Util::getLinkStorage(), '', $depoimento->$tipo);
            Storage::delete($path);

            $diretorio = 'plataformas/'.session('plataforma_id').'/depoimentos';
            $filename = $depoimento->id.'.'.$file->getClientOriginalExtension();

            $path = Storage::putFileAs($diretorio, $file, $filename, 'public');
            $linkStorage = \App\Models\Util::getLinkStorage();
            $caminho = $linkStorage . $path;

            $depoimento->update([$tipo => $caminho]);
        }
    }

    public function syncFotoParceiro($parceiro, $tipo, $inputFileName)
    {
        $file = request()->file($inputFileName);

        if($file)
        {
            $path = str_replace(\App\Models\Util::getLinkStorage(), '', $parceiro->$tipo);
            Storage::delete($path);

            $diretorio = 'plataformas/'.session('plataforma_id').'/parceiros';
            $filename = $parceiro->id.'.'.$file->getClientOriginalExtension();

            $path = Storage::putFileAs($diretorio, $file, $filename, 'public');
            $linkStorage = \App\Models\Util::getLinkStorage();
            $caminho = $linkStorage . $path;

            $parceiro->update([$tipo => $caminho]);
        }
    }

    public function syncFotoPost($post, $tipo, $inputFileName)
    {
        $file = request()->file($inputFileName);

        if($file)
        {
            $path = str_replace(\App\Models\Util::getLinkStorage(), '', $post->$tipo);
            Storage::delete($path);

            $diretorio = 'plataformas/'.session('plataforma_id').'/blog/posts';
            $filename = $post->id.'.'.$file->getClientOriginalExtension();

            $path = Storage::putFileAs($diretorio, $file, $filename, 'public');
            $linkStorage = \App\Models\Util::getLinkStorage();
            $caminho = $linkStorage . $path;

            $post->update([$tipo => $caminho]);
        }
    }

}