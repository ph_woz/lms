<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFormacoesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('formacoes', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('plataforma_id');
            $table->unsignedBigInteger('user_id');
            $table->string('tipo_formacao');
            $table->string('curso');
            $table->string('instituicao');
            $table->string('data_inicio', 7)->nullable();
            $table->string('data_fim', 7)->nullable();
            $table->string('status_curso')->nullable();
            $table->text('descricao')->nullable();
            $table->integer('estrelas')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('formacoes');
    }
}
