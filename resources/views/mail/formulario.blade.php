<p style="font-size:17px; margin-bottom: 0.5rem;">Nome:  {{ $nome }}</p>
<p style="font-size:17px; margin-bottom: 0.5rem;">Email: {{ $email }}</p>

<div style="margin-top: 20px;">
	<a href="{{\Request::root()}}/admin/contato/info/{{$id}}" style="display: inline-block; color: #FFF; text-decoration: none; border-radius:2px; background:#1865d9; padding-left:20px; padding-right:20px; padding-top:10px; padding-bottom:10px;">
		Mais informações
	</a>
</div>

<p style="font-size: 17px;">{{ $assunto }}</p>