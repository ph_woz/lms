@extends('layouts.aluno')
@section('seo_title', 'Avaliação ' . $avaliacao->referencia)

@section('css')
<style>
	.bg-alternativa { background: #4d5256; }
	.bg-selected { background: #16181b; }
</style>
@endsection

@section('content')

<section class="container-fluid px-xl-5 z-3 mt-5 position-relative">
    <div class="mb-4 px-4 py-4 rounded-10 box-shadow bg-14">

        <p class="mb-0 title fs-16">
            Sua nota foi: <strong>{{ $avaliacaoAluno->nota }} de {{ $avaliacao->getValorNotaMaxima($avaliacao->id) }} pontos.</strong>
        </p>

        @if($avaliacaoAluno->n_tentativa)
            <p class="mb-0 title fs-15">
                Esta foi sua <strong>{{ $avaliacaoAluno->n_tentativa }}° tentativa</strong>.
            </p>
        @endif

        @if($avaliacaoAluno->data_finalizada)
            <p class="mb-0 title fs-16">
                Data de realização: <strong>{{ \App\Models\Util::replaceDateTimePt($avaliacaoAluno->data_finalizada) }}</strong>
            </p>
        @endif
        
    </div>
</section>

<form action="{{ route('aluno.finalizar', ['id' => $avaliacao->id]) }}" method="POST" class="container-fluid px-xl-5 z-3 mt-5 position-relative">
@csrf

    @foreach($questoes as $questao)

    	<div class="mb-4 px-4 py-4 rounded-10 box-shadow bg-14">

    		<div class="d-flex align-items-center">

    		    <div>
    		        <div class="bg-db mr-4 square-40 rounded-circle flex-center text-dark">
    		            <span class="fs-18 weight-600">{{ $loop->iteration }}</span>
    		        </div>
    		    </div>

                <div class="ps-4">
                    <p class="color-enunciado mb-0 fs-19">
                    	{!! nl2br($questao->enunciado) !!}
                    </p>

                    @if($questao->arquivo)
                        @if($questao->isImage($questao->arquivo))
                            <img src="{{ $questao->arquivo }}" class="img-fluid my-3" width="200">
                            <div></div>
                            <a href="{{ $questao->arquivo }}" target="_blank" class="btn btn-primary">
                                Ampliar imagem
                                &nbsp;<i class="fa fa-search-plus"></i>
                            </a>
                        @else
                            <div class="mt-3 mb-1">
                                <a href="{{ $questao->arquivo }}" target="_blank" class="btn-cadastrar">
                                    <i class="fas fa-external-link-alt"></i>
                                    Abrir Arquivo em Anexo
                                </a>
                            </div>
                        @endif
                    @endif
                </div>

    		</div>

            <hr/>

            @if($questao->modelo == 'D')

                @php
                    $respostaDissertativa = \App\Models\AvaliacaoAlunoResposta::plataforma()
                        ->where('aluno_id', \Auth::id())
                        ->where('avaliacao_id', $avaliacao->id)
                        ->where('questao_id', $questao->id)
                        ->first();
                @endphp

                <textarea name="respostasDiscursivas[{{$questao->id}}]" class="w-100 rounded p-form bg-alternativa border-0 text-white py-3 color-placeholder-light h-min-100" placeholder="Sua resposta" required disabled readonly>{{ $respostaDissertativa->resposta_dissertativa ?? 'Não respondeu' }}</textarea>

                <p class="mb-0 mt-3 title color-enunciado fs-15">
                    <strong class="d-block">
                        Correção:
                        @if($respostaDissertativa->acertou == 'S')
                            <span class="ms-1 text-success">
                                Acertou!
                            </span>
                        @endif
                        @if($respostaDissertativa->acertou == 'N')
                            <span class="ms-1 text-danger">
                                Errou.
                            </span>
                        @endif
                        @if($respostaDissertativa->acertou == 'Em correção')
                            <span class="ms-1 text-secondary">
                                Aguardando Correção.
                            </span>
                        @endif
                    </strong>
                    <span>{!! nl2br($respostaDissertativa->resposta_dissertativa_correcao) !!}</span>
                </p>

            @endif
            
            @if($questao->modelo == 'O')

                @php
                    $alternativas = \App\Models\QuestaoAlternativa::plataforma()->where('questao_id', $questao->id)->inRandomOrder()->get();

                    $verificaRespondeu = \App\Models\AvaliacaoAlunoResposta::plataforma()
                        ->where('aluno_id', \Auth::id())
                        ->where('avaliacao_id', $avaliacao->id)
                        ->where('questao_id', $questao->id)
                        ->exists();
                @endphp

                @if($verificaRespondeu == false)
                    <p class="weight-600 text-danger">Questão não respondida.</p>
                @endif

                @foreach($alternativas as $alternativa)
                
                    @php
                        $minhaRespostaAlternativaId = \App\Models\AvaliacaoAlunoResposta::plataforma()
                            ->where('aluno_id', \Auth::id())
                            ->where('avaliacao_id', $avaliacao->id)
                            ->where('questao_id', $alternativa->questao_id)
                            ->pluck('alternativa_id')[0] ?? null;
                    @endphp

                    <label for="opt{{$loop->index}}p{{$alternativa->id}}" class="w-100 cursor-pointer">

                        <input type="radio" name="respostasObjetivas[{{$questao->id}}]" id="opt{{$loop->index}}p{{$alternativa->id}}" class="optAlternativa questao{{$questao->id}} d-none" data-questao="{{ $questao->id }}" value="{{ $alternativa->id }}" required> 
                        
                        <div class="d-flex">
                            <span class="mb-1 border rounded-start bg-db rounded-right-0 w-40px flex-center weight-600">
                                {{ $alternativa->alphabetic($loop->index) }}
                            </span>
                            <span class="
                                @if($alternativa->correta == 'S') bg-success @endif
                                @if($alternativa->correta != 'S' && $minhaRespostaAlternativaId == $alternativa->id) bg-danger @endif 
                                @if($alternativa->correta != 'S' && $minhaRespostaAlternativaId != $alternativa->id) bg-alternativa @endif 
                                w-100 weight-500 text-light p-3 mb-1 rounded-left-0 rounded-end fs-17">
                                    {!! $alternativa->alternativa !!} 
                            </span>
                        </div>

                    </label>
            
                @endforeach

            @endif

            @if($questao->gabarito)
                <p class="mb-0 mt-3 title color-enunciado fs-15">
                    <strong class="d-block">Gabarito:</strong>
                    <span>{!! nl2br($questao->gabarito) !!}</span>
                </p>
            @endif

    	</div>

    @endforeach

</form>

@endsection