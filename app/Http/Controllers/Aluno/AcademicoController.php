<?php

namespace App\Http\Controllers\Aluno;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use App\Models\LinkAcademico;
use App\Models\User;
use App\Models\Documento;
use App\Models\Formulario;
use App\Models\FormularioPergunta;

class AcademicoController extends Controller
{
    public function academico()
    {
        $getNomePage = 'Serviços Acadêmicos';

        $links = LinkAcademico::plataforma()->orderBy('ordem')->get();

        return view('aluno.academico', compact('getNomePage','links') );
    }

    public function documentos()
    {      
        $formulario = Formulario::plataforma()->where('slug','formulario-de-documentacao')->first();
            
        if(is_null($formulario))
        {
            return back()->with('danger', 'Formulário de Documentos não encontrado.');
        }

        $formulario_id = $formulario->id ?? null;

        $getNomePage = $formulario->referencia ?? null;


        if($formulario_id)
        {
            $perguntas = FormularioPergunta::plataforma()->where('formulario_id', $formulario_id)->where('tipo_input','file')->orderBy('ordem')->get();
        
        } else {

            $perguntas = array();
        }
        
        
        $countJaRespondidas = Documento::plataforma()->where('user_id', \Auth::id())->whereIn('nome', $perguntas->pluck('pergunta')->toArray())->count();

        if($countJaRespondidas == count($perguntas))
        {
            User::plataforma()->where('id', \Auth::id())->update(['data_docs_recebidas' => date('Y-m-d H:i')]);
        
        } else {

            User::plataforma()->where('id', \Auth::id())->update(['data_docs_recebidas' => null]);
        }


        if(request()->has('sent'))
        { 
            if(request('files'))
            {
                foreach(request('files') as $nome => $file)
                {
        
                    $oldDocumento = Documento::plataforma()->where('id', \Auth::id())->where('nome', $nome)->first();
                    if($oldDocumento)
                    {
                        $path = str_replace(\App\Models\Util::getLinkStorage(), '', $oldDocumento->link);
                        Storage::delete($path);
                        $oldDocumento->delete();
                    }

                    $documento = Documento::create([
                        'plataforma_id' => \Auth::user()->plataforma_id,
                        'user_id'       => \Auth::user()->id,
                        'nome'          => $nome,
                        'link'          => null,
                    ]);

                    $diretorio = 'plataformas/'.\Auth::user()->plataforma_id.'/users/documentos';
                    $filename = $documento->id.'.'.$file->getClientOriginalExtension();

                    $path = Storage::putFileAs($diretorio, $file, $filename, 'public');
                    $linkStorage = \App\Models\Util::getLinkStorage();
                    $caminho = $linkStorage . $path;

                    $documento->update(['link' => $caminho]);
                }
            }

            $countJaRespondidas = Documento::plataforma()->where('user_id', \Auth::id())->whereIn('nome', $perguntas->pluck('pergunta')->toArray())->count();

            if($countJaRespondidas == count($perguntas))
            {
                User::plataforma()->where('id', \Auth::id())->update(['data_docs_recebidas' => date('Y-m-d H:i')]);
            
            } else {

                User::plataforma()->where('id', \Auth::id())->update(['data_docs_recebidas' => null]);
            }


            return back()->with('success', 'Documentos atualizado com êxito.');
        }

        return view('aluno.documentos', compact('formulario','perguntas','countJaRespondidas','getNomePage') );   
    }

}