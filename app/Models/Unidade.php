<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Unidade extends Model
{
    use HasFactory;

    public $timestamps = true;

    protected $table = 'unidades';

    public $fillable = [
        'plataforma_id',
    	'cadastrante_id',
        'nome',
        'telefone',
        'celular',
        'email',
        'endereco',
        'publicar',
        'status',
    ];

    public static  function boot()
    {
        parent::boot(); 
        static::creating(function ($model)
        {
            $model->plataforma_id  = session('plataforma_id') ?? \Auth()->user()->plataforma_id ?? Plataforma::dominio()->pluck('id')[0] ?? null;
            $model->cadastrante_id = \Auth()->user()->id ?? null;
        });
    }

    public function scopePlataforma($query)
    {
        return $query->where('plataforma_id', session('plataforma_id') ?? Plataforma::dominio()->pluck('id')[0] ?? null);
    }

    public function scopeAtivo($query)
    {
        return $query->where('status', 0);
    }

    public function scopePublico($query)
    {
        return $query->where('publicar', 'S');
    }
    
    public function scopeUnidade($query)
    {
        if(\Auth::user()->restringir_unidade == 'N' || \Auth::user()->unidade_id == null)
            return;
        
        return $query->where('id', \Auth::user()->unidade_id);
    }

    public static function checkActive()
    {
        $check = PlataformaModulo::plataforma()->ativo()->where('url','unidade')->exists();
        
        return $check;
    }

}
