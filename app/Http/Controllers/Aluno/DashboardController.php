<?php

namespace App\Http\Controllers\Aluno;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ComunicadoAluno;
use App\Models\PaginaConteudo;
use App\Models\User;
use App\Models\TurmaAluno;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Carbon;

class DashboardController extends Controller
{
    public function dashboard()
    {
        $getNomePage = 'Dashboard';

        $plataforma_id = session('plataforma_id') ?? Auth::user()->plataforma_id;
        $termosDeUsoParaAceite = Cache::remember('termosDeUsoParaAceite-'.$plataforma_id, Carbon::now()->addMonth(), function() {
            return PaginaConteudo::select('conteudo')->plataforma()->where('url','/termos-de-uso')->pluck('conteudo')[0] ?? null;
        });


        $cursos = $this->carregarCursosAlunos();

        $turmasEmAndamento = $cursos['turmasEmAndamento'];
        $turmasDisponiveis = $cursos['turmasDisponiveis'];
        $turmasConcluidas = $cursos['turmasConcluidas'];

        $checkComunicadosNaoLidos = ComunicadoAluno::select('id')->plataforma()->where('aluno_id', Auth::id())->whereNull('data_visualizacao')->exists();


        $trilhas = DB::table('trilhas_alunos')
            ->join('trilhas','trilhas_alunos.trilha_id','trilhas.id') 
            ->where('trilhas_alunos.aluno_id', Auth::id())
            ->where('trilhas.plataforma_id', $plataforma_id)
            ->where('trilhas.status', 0)
            ->where('trilhas_alunos.plataforma_id', $plataforma_id)
            ->where('trilhas_alunos.status', 0)
            ->select(
                'trilhas.id',
                'trilhas.nome',
                'trilhas.foto_miniatura',
                'trilhas.slug',
                'trilhas_alunos.data_conclusao'
            )
            ->get();


        return view('aluno.dashboard', 
            compact(
                'getNomePage',
                'turmasEmAndamento',
                'turmasDisponiveis',
                'turmasConcluidas',
                'trilhas',
                'checkComunicadosNaoLidos',
                'termosDeUsoParaAceite'
            ) 
        );
    }

    public function aceitarTermosDeUso()
    {
        User::plataforma()->where('id', Auth::id())->update(['aceita_termos' => 'S']);

        return redirect()->route('aluno.dashboard')->with('success', 'Termos de uso aceito com êxito.');
    }


    private function carregarCursosAlunos(): array
    {
        $user_id = Auth::id();
        $plataforma_id = session('plataforma_id') ?? Auth::user()->plataforma_id;

        $cursos = DB::select( DB::raw("
            SELECT
                cursos.id,
                cursos.nome,
                cursos.slug,
                cursos.foto_miniatura,
                turmas_alunos.data_primeiro_acesso,
                turmas_alunos.data_ultimo_acesso,
                turmas_alunos.data_conclusao_curso
            FROM
                cursos
            INNER JOIN turmas_alunos ON
                turmas_alunos.curso_id = cursos.id
            INNER JOIN turmas ON
                turmas.id = turmas_alunos.turma_id
            WHERE
                    cursos.status = 0
                AND cursos.plataforma_id = ".$plataforma_id."
                AND turmas_alunos.status = 0
                AND turmas_alunos.aluno_id = ".$user_id."
                AND turmas.status = 0;
        "));

        $turmasEmAndamento = [];
        $turmasDisponiveis = [];
        $turmasConcluidas = [];

        foreach ($cursos as $curso)
        {
            if ($curso->data_conclusao_curso === null && $curso->data_ultimo_acesso !== null) {
                $turmasEmAndamento[] = $curso;
            } else if ($curso->data_primeiro_acesso === null) {
                $turmasDisponiveis[] = $curso;
            } else if ($curso->data_ultimo_acesso !== null) {
                $turmasConcluidas[] = $curso;
            }
        }

        return compact('turmasEmAndamento', 'turmasDisponiveis', 'turmasConcluidas');
    }
}