<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Hash;
use App\Models\Plataforma;
use App\Models\User;
use App\Models\FluxoCaixa;
use App\Models\UserCategoria;
use App\Models\Categoria;
use App\Models\UserEndereco;
use App\Models\Trilha;
use App\Models\TrilhaAluno;
use App\Models\TrilhaTurma;
use App\Models\TurmaAluno;


class APIAlunoController extends Controller
{
    public function auth($api_key)
    {
        $plataforma_idIFOK = Plataforma::select('id','api_key')->where('api_key', $api_key)->pluck('id')[0] ?? null;
 
        if($plataforma_idIFOK == null)
        {
            return 0;

        } else {

            return $plataforma_idIFOK;
        }
    }

    public function lista(Request $request)
    {
        $plataforma_id = $this->auth($request->api_key);

        if($plataforma_id === 0)
        {
            return response()->json(['mensagem' => 'API KEY não autorizado.'], 403);
        }


        $alunos = User::where('plataforma_id', $plataforma_id)->aluno()->ativo()->paginate(25);

        return response()->json(['data' => $alunos]);
    }

    public function info($email)
    {
        $plataforma_id = $this->auth($request->api_key);

        if($plataforma_id === 0)
        {
            return response()->json(['mensagem' => 'API KEY não autorizado.'], 403);
        }


        $aluno = User::where('plataforma_id', $plataforma_id)->aluno()->where('email', $email)->first();

        if(is_null($aluno))
        {
            return response()->json(['mensagem' => 'Aluno não encontrado.'], 404);
        }

        return response()->json(['data' => $aluno]);
    }

    public function infoFinanceiro(Request $request)
    {
        $plataforma_id = $this->auth($request->api_key);

        if($plataforma_id === 0)
        {
            return response()->json(['mensagem' => 'API KEY não autorizado.'], 403);
        }

        $user_id = User::select('id')->where('plataforma_id', $plataforma_id)->aluno()->where('email', $request->email)->value('id');
        $listar = request('listar') ?? 'abertas';

        $entradas = FluxoCaixa::select('produto_nome','n_parcelas','n_parcela','registro_id','status','data_vencimento','valor_parcela','link','data_recebimento','data_estorno')
            ->where('plataforma_id', $plataforma_id)
            ->entrada()
            ->where('cliente_id', $user_id);

        if($listar == 'abertas')
        {
            $entradas = $entradas->where('status', 0);
        }

        $entradas = $entradas
            ->orderBy('data_vencimento')
            ->get();

        return response()->json(['data' => $entradas]);
    }

    public function add(Request $request)
    {
        $camposBody = $request->post();

        $plataforma_id = $this->auth($request->api_key);

        if($plataforma_id === 0)
        {
            return response()->json(['mensagem' => 'API KEY não autorizado.'], 403);
        }


        $checkEmailExists = User::select('id')->where('plataforma_id', $plataforma_id)->where('email', $request->email ?? $camposBody['email'])->exists();

        if($checkEmailExists == true)
        {
            return response()->json(['mensagem' => 'Já existe uma pessoa cadastrada com este Email.'], 403);
        }

        if($request->cpf || isset($camposBody['cpf']))
        {
            $checkCPFExists = User::where('plataforma_id', $plataforma_id)->where('cpf', $request->cpf ?? $camposBody['cpf'])->exists();

            if($checkCPFExists == true)
            {
                return response()->json(['mensagem' => 'Já existe uma pessoa cadastrada com este CPF.'], 403);
            }
        }

        if($request->rg || isset($camposBody['rg']))
        {
            $checkRGExists = User::where('plataforma_id', $plataforma_id)->where('rg', $request->rg ?? $camposBody['rg'])->exists();

            if($checkRGExists == true)
            {
                return response()->json(['mensagem' => 'Já existe uma pessoa cadastrada com este RG.'], 403);
            }
        }

        $password = $request->password ?? $camposBody['password'];
        
        if($password == null)
        {
            if($request->cpf || isset($camposBody['cpf']))
            {
                $password = str_replace('.', '', $request->cpf ?? $camposBody['cpf']);
                $password = str_replace('-', '', $password);
            
            } else {

                $password = date('YmdHi');
            }
        }

        $user = User::create([
            'plataforma_id'      => $plataforma_id,
            'nome'               => $camposBody['nome'] ?? $request->nome,
            'email'              => $camposBody['email'] ?? $request->email,
            'password'           => Hash::make($password) ?? null,
            'status'             => $camposBody['status'] ?? $request->status ?? null,
            'unidade_id'         => $camposBody['unidade_id'] ?? $request->unidade_id,
            'cpf'                => $camposBody['cpf'] ?? $request->cpf,
            'rg'                 => $camposBody['rg'] ?? $request->rg,
            'naturalidade'       => $camposBody['naturalidade'] ?? $request->naturalidade,
            'data_nascimento'    => $camposBody['data_nascimento'] ?? $request->data_nascimento,
            'celular'            => $camposBody['celular'] ?? $request->celular,
            'telefone'           => $camposBody['telefone'] ?? $request->telefone,
            'genero'             => $camposBody['genero'] ?? $request->genero,
            'profissao'          => $camposBody['profissao'] ?? $request->profissao,
            'obs'                => $camposBody['obs'] ?? $request->obs,
            'tipo'               => 'aluno',
        ]);

        UserEndereco::create([
            'plataforma_id'    =>  $plataforma_id,
            'plataforma_id'    =>  $user->plataforma_id,
            'user_id'          =>  $user->id,
            'cep'              =>  $camposBody['endereco_cep'] ?? $request->endereco_cep,
            'rua'              =>  $camposBody['endereco_rua'] ?? $request->endereco_rua,
            'numero'           =>  $camposBody['endereco_numero'] ?? $request->endereco_numero,
            'bairro'           =>  $camposBody['endereco_bairro'] ?? $request->endereco_bairro,
            'cidade'           =>  $camposBody['endereco_cidade'] ?? $request->endereco_cidade,
            'estado'           =>  $camposBody['endereco_estado'] ?? $request->endereco_estado,
            'complemento'      =>  $camposBody['endereco_complemento'] ?? $request->endereco_complemento,
            'ponto_referencia' =>  $camposBody['endereco_ponto_referencia'] ?? $request->endereco_ponto_referencia,
        ]);

        if($request->categorias || isset($camposBody['categorias']))
        {
            $categorias = $request->categorias ?? $camposBody['categorias'] ?? array();

            foreach($camposBody['categorias'] as $categoriaNome)
            {
                $categoria = Categoria::where('plataforma_id', $plataforma_id)->tipo('aluno')->where('nome', $categoriaNome)->first();

                if(is_null($categoria))
                {
                    $categoria = Categoria::create([
                        'plataforma_id' => $plataforma_id,
                        'tipo' => 'aluno',
                        'nome' => $categoriaNome,
                    ]);
                }

                UserCategoria::create([
                    'plataforma_id' => $plataforma_id,
                    'user_id' => $user->id, 
                    'categoria_id' => $categoria->id
                ]);
            }
        }


        $add_aluno_para_trilha_id = $_GET['add_aluno_para_trilha_id'] ?? $camposBody['add_aluno_para_trilha_id'] ?? $request->add_aluno_para_trilha_id ?? null;

        if($add_aluno_para_trilha_id != null)
        {
            $checkExistsTrilha = Trilha::where('plataforma_id', $plataforma_id)->where('id', $add_aluno_para_trilha_id)->exists();

            if($checkExistsTrilha == true)
            {
                TrilhaAluno::create([
                    'plataforma_id' => $plataforma_id,
                    'trilha_id' => $add_aluno_para_trilha_id,
                    'aluno_id'  => $user->id,
                ]);

                $turmas  = TrilhaTurma::where('trilhas_turmas.plataforma_id', $plataforma_id)
                    ->where('trilhas_turmas.trilha_id', $add_aluno_para_trilha_id)
                    ->join('turmas','trilhas_turmas.turma_id','turmas.id')
                    ->select('turmas.id','turmas.curso_id')
                    ->get();
                

                foreach($turmas as $turma)
                {
                    TurmaAluno::create([
                        'plataforma_id' => $plataforma_id,
                        'turma_id'      => $turma->id,
                        'curso_id'      => $turma->curso_id,
                        'aluno_id'      => $user->id,
                    ]);
                }

                return response()->json(['mensagem' => 'Aluno cadastrado e matriculado na Trilha com êxito.'], 200);
            
            } else {

                return response()->json(['mensagem' => 'Aluno cadastrado com êxito e Trilha setada inexistente. Ref de saída para debug, var add_aluno_para_trilha_id:' . $add_aluno_para_trilha_id], 200);
            }
        }

        return response()->json(['mensagem' => 'Aluno cadastrado com êxito.'], 200);
    }

    public function editar($email, Request $request)
    {
        $plataforma_id = $this->auth($request->api_key);

        if($plataforma_id === 0)
        {
            return response()->json(['mensagem' => 'API KEY não autorizado.'], 403);
        }



        $user = User::where('plataforma_id', $plataforma_id)->aluno()->where('email', $email)->first();

        if(is_null($user))
        {
            return response()->json(['mensagem' => 'Aluno não encontrado.'], 404);
        }


        $checkEmailExists = User::where('plataforma_id', $plataforma_id)->where('id','<>', $user->id)->where('email', $request->email)->exists();

        if($checkEmailExists == true)
        {
            return response()->json(['mensagem' => 'Já existe uma pessoa cadastrada com este Email.'], 403);
        }

        if($request->cpf)
        {
            $checkCPFExists = User::where('plataforma_id', $plataforma_id)->where('id','<>', $user->id)->where('cpf', $request->cpf)->exists();

            if($checkCPFExists == true)
            {
                return response()->json(['mensagem' => 'Já existe uma pessoa cadastrada com este CPF.'], 403);
            }
        }

        if($request->rg)
        {
            $checkRGExists = User::where('plataforma_id', $plataforma_id)->where('id','<>', $user->id)->where('rg', $request->rg)->exists();

            if($checkRGExists == true)
            {
                return response()->json(['mensagem' => 'Já existe uma pessoa cadastrada com este RG.'], 403);
            }
        }

        $user->update($request->all());

        return response()->json(['mensagem' => 'Cadastro do Aluno atualizado com êxito.'], 200);
    }

    public function delete($email)
    {
        $plataforma_id = $this->auth($request->api_key);

        if($plataforma_id === 0)
        {
            return response()->json(['mensagem' => 'API KEY não autorizado.'], 403);
        }


        $aluno = User::where('plataforma_id', $plataforma_id)->aluno()->where('email', $email)->first();

        if(is_null($aluno))
        {
            return response()->json(['mensagem' => 'Aluno não encontrado.'], 404);
        }

        UserEndereco::where('plataforma_id', $plataforma_id)->where('user_id', $aluno->id)->delete();
        UserCategoria::where('plataforma_id', $plataforma_id)->where('user_id', $aluno->id)->delete();
        TrilhaAluno::where('plataforma_id', $plataforma_id)->where('aluno_id', $aluno->id)->delete();
        TurmaAluno::where('plataforma_id', $plataforma_id)->where('aluno_id', $aluno->id)->delete();
        \App\Models\AvaliacaoAluno::where('plataforma_id', $plataforma_id)->where('aluno_id', $aluno->id)->delete();
        \App\Models\AvaliacaoAlunoResposta::where('plataforma_id', $plataforma_id)->where('aluno_id', $aluno->id)->delete();
        \App\Models\AlunoAulaConcluida::where('plataforma_id', $plataforma_id)->where('aluno_id', $aluno->id)->delete();
        \App\Models\ComunicadoAluno::where('plataforma_id', $plataforma_id)->where('aluno_id', $aluno->id)->delete();

        \App\Models\Log::create([
            'plataforma_id' => $plataforma_id,
            'user_id' => 0,
            'tipo' => 'aluno',
            'ref' => 'Aluno ' . $aluno->nome . ' ('.$aluno->email .' - ' . $aluno->celular.')' . ' foi deletado por API.'
        ]);

       $aluno->delete();

       return response()->json(['mensagem' => 'Aluno deletado com êxito.'], 200);
    }
}