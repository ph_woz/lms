<!DOCTYPE html>
<html lang="pt-br">
<head>

  <!-- Google tag (gtag.js) -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=G-MREB450HCT"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'G-MREB450HCT');
  </script>
        
   <style>
#mainBlogArtigo { padding-top: 100px!important;}
</style>

   <meta charset="utf-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1">

   <title>Nossos Serviços - Resgate Vertical</title>
   <meta name="description" content="Nossos Serviços">
   <meta name="keywords" content="Nossos Serviços">
   <meta name="robots" content="index, follow">

   <meta property="og:locale" content="pt_BR">
   <meta property="og:type" content="website">
   <meta property="og:image" content="https://lmcv2.s3-sa-east-1.amazonaws.com/plataformas/19/blog/artigos/28.jpg">
   <meta property="og:url" content="https://resgatevertical.com.br/blog/post/28/nossos-servicos">
   <meta property="og:description" content="Nossos Serviços">
   <meta property="og:title" content="Nossos Serviços - Resgate Vertical">
   
   <link href="https://resgatevertical.com.br/blog/post/28/nossos-servicos" rel="canonical">
       
   <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/uikit@3.5.4/dist/css/uikit.min.css" />

   <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
   <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">

   <link rel="stylesheet" href="https://resgatevertical.com.br/css/app.css">
   <link rel="stylesheet" href="https://resgatevertical.com.br/css/util.css">
   <link rel="stylesheet" href="https://resgatevertical.com.br/css/blog.css">
   <link rel="stylesheet" href="https://resgatevertical.com.br/aluno/css/header.css">
    
   <link rel="shortcut icon" href="">
    
   <style>
    
    #menuPlataforma nav { background: #dc3545; }
    .menu-link, .menu-link-color { color: #F9F9F9;; }
    .menu-link:hover { text-decoration: none; background: #bd3131; }
    .navbar-brand span, .activeNavLink { text-decoration: none; background: #bd3131; }

    .color-base { color: #555; }
    .color-base-secundaria { color: #555; }
    
    .bg-base, 
    .bg-selected, 
    .btnDown a div:hover,
    ::-webkit-scrollbar-thumb { background: rgba(181, 36, 36, .95); }

    .bg-base-secundaria { background: rgba(178, 49, 49, .9); }

    .btn-cadastrar, .btn-link, .btn-submit, .btnTop, .btn-append { background: rgba(181, 36, 36, .95); color: #FFF; }
    .btn-hover:hover, .header-icons a div:hover, .btn-cadastrar:hover, .btn-link:hover, .btn-submit:hover, .btnTop:hover, .btn-append:hover { background: #dc3545; }
    
    .section-jumbontron { padding-top: 125px!important; }

#menu {
    width: 100%;
    box-sizing: border-box;
    padding: 9px 10px!important;
    position: fixed;
    z-index: 200;
    box-shadow: 0px -1px 8px 0px rgba(0, 0, 0, 0.60);
    background: transparent;
    transition: background-color 0.3s ease-in-out;
}
@media (min-width: 769px) 
{
    #menu { height: 75px; }

    .menu-link { 
      margin: 0 10px;
      padding-top: 27.2px!important;
      padding-bottom: 27.2px!important;
      padding-left: 10px!important; 
      padding-right: 10px!important; 
    }
}
.menu-link {
    font-family: 'Open Sans',sans-serif;
    font-size: 13.5px;
    text-transform: uppercase;
    text-decoration: none;
    font-weight: 700;
    color: #F5F5F5!important;
    cursor: pointer;
    transition: all .2s;
    text-shadow: 1px 1px rgba(0, 0, 0, .1);
}
.line-vertical { display:none }

.btn-entrar { 
  background: #ffc107!important; 
  color: #021a36!important; 
  font-family: 'Poppins', sans-serif; 
  font-weight: 600!important; 
  border-radius: 30px!important;
  padding: 12px 24px!important;
}

.btn-sell {
 background-color:#c92027!important;
 display:inline-block!important;
 cursor:pointer!important;
 color:#ffffff!important;
 font-family:'Raleway', sans-serif!important;
 font-size:18px!important;
 padding:14px 35px!important;
 text-decoration:none!important;
 text-shadow:0px -1px 0px #7a2a1d!important;
 text-transform: uppercase!important;
 font-weight: 800!important;
 border-radius: 3px!important;
 border-bottom: 3.5px solid #5c0f19!important;
 text-shadow: 1.5px 1.5px rgba(0, 0, 0, .20)!important;
 letter-spacing: .2px!important;
}
.btn-sell:hover { background-color:#b51d23!important; }
#mainBlog { padding-top: 100px!important; }
    
    #footer { position: absolute!important; }
    
</style>
</head>
<body class="bg-body">

   <header id="menuPlataforma">
    <nav class="navbar-header navbar navbar-expand-lg fixed-top shadow" id="menu">
        <div class="container-fluid px-xl-7">

          <a class="navbar-brand" href="/#inicio">
                          <img src="https://lmcv2.s3-sa-east-1.amazonaws.com/plataformas/19/logotipoMenu.jpg" class="img-fluid" width="175">
                      </a>

          <button class="bg-transparent d-lg-none outline-0 border-0 menu-link-color" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="fa fa-bars fs-20"></span>
          </button>

          <div class="collapse navbar-collapse py-4 py-lg-0" id="navbarSupportedContent">

              <ul class="navbar-nav ml-auto ulNavLinks">
                                      <li class="nav-item">
                        <a class="menu-link " 
                           href="/#inicio" >
                            Inicio
                        </a>
                    </li>
                                          <div class="line-vertical"></div>
                                                          <li class="nav-item">
                        <a class="menu-link " 
                           href="/#cursos" >
                            Nossos cursos
                        </a>
                    </li>
                                          <div class="line-vertical"></div>
                                                          <li class="nav-item">
                        <a class="menu-link " 
                           href="/quem-somos" >
                            Quem Somos
                        </a>
                    </li>
                                          <div class="line-vertical"></div>
                                                          <li class="nav-item">
                        <a class="menu-link activeNavLink " 
                           href="/blog/post/28/nossos-servicos" >
                            Nossos serviços
                        </a>
                    </li>

                  <li class="nav-item">
                     <a class="nav-link activeNavLink" href="/login">
                        <i class="fa fa-user-graduate mr-1"></i>
                        Área do Aluno
                     </a>
                  </li>

             </ul>

          </div>

        </div>
    </nav>
</header>


   <main class="py-3" id="mainBlog">
        
      <section class="container-fluid px-xl-8 py-5">
         <div class="row">
            
            <div class="col-lg-8">

               <div class="uk-position-relative uk-visible-toggle uk-light" tabindex="-1" uk-slideshow="animation: fade; max-height: 385; max-height: 385;">

                  <ul class="uk-slideshow-items">
                      <li>
                          <img src="https://lmcv2.s3-sa-east-1.amazonaws.com/plataformas/19/blog/artigos/28.jpg" alt="Foto Nossos Serviços - Resgate Vertical" uk-cover>
                          <div class="uk-overlay uk-overlay-primary uk-position-bottom uk-text-center uk-transition-slide-bottom">
                              <h3 class="uk-margin-remove fs-xs-17">Nossos Serviços</h3>
                          </div>
                          <div class="px-2 d-lg-flex justify-content-between align-items-center uk-position-bottom text-left mb-0">
                              <h6 class="color-c9 fs-9">
                                                              </h6>
                              <h6 class="color-c9 fs-9">
                                <i class="fa fa-clock"></i>
                                há 1 ano
                              </h6>
                          </div>
                      </li>
                  </ul>

                  <a class="p-lg-5 uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous uk-slideshow-item="previous"></a>
                  <a class="p-lg-5 uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-next uk-slideshow-item="next"></a>

               </div>

               <div class="py-5">
                  <div class="container">
                     <div class="row" style="box-sizing: border-box; margin-right: -15px; margin-left: -15px; color: #454545; font-family: 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', 'DejaVu Sans', Verdana, sans-serif; font-size: 16px; background-color: #ffffff;"><br />
<div class="col-md-12" style="box-sizing: border-box; position: relative; min-height: 1px; padding-right: 15px; padding-left: 15px; float: left; width: 1170px;"><br />
<h3 style="box-sizing: border-box; font-weight: 400; line-height: 1.1; color: darkred; margin-top: 20px; margin-bottom: 10px; font-size: 20px;">ENGENHARIA DE SEGURAN&Ccedil;A DO TRABALHO</h3><br />
<ul style="box-sizing: border-box; margin: 0px; list-style: none; padding: 0px;"><br />
<li style="box-sizing: border-box;">&bull; Capacita&ccedil;&atilde;o dos Membros da Comiss&atilde;o Interna de Preven&ccedil;&atilde;o de Acidentes - CIPA - NR-5</li><br />
<li style="box-sizing: border-box;">&bull; Capacita&ccedil;&atilde;o em Seguran&ccedil;a em servi&ccedil;os com eletricidade - NR-10</li><br />
<li style="box-sizing: border-box;">&bull; Capacita&ccedil;&atilde;o em NR 20 - Integra&ccedil;&atilde;o</li><br />
<li style="box-sizing: border-box;">&bull; Capacita&ccedil;&atilde;o Inicial nos Trabalhos em espa&ccedil;os confinados - Autorizados e Vigias - NR 33</li><br />
<li style="box-sizing: border-box;">&bull; Capacita&ccedil;&atilde;o no Trabalho em Altura - NR 35</li><br />
<li style="box-sizing: border-box;">&bull; Curso de Elabora&ccedil;&atilde;o de PPRA - Programa de Preven&ccedil;&atilde;o de Riscos Ambientais</li><br />
<li style="box-sizing: border-box;">&bull; Seguran&ccedil;a e Sa&uacute;de no Trabalho Aquavi&aacute;rio - NR - 30</li><br />
<li style="box-sizing: border-box;">&bull; Capacita&ccedil;&atilde;o de membro designado da CIPA, abaixo de 20 profissionais - NR-5</li><br />
<li style="box-sizing: border-box;">&bull; Capacita&ccedil;&atilde;o no Trabalho Portu&aacute;rio - NR - 29</li><br />
<li style="box-sizing: border-box;">&bull; Capacita&ccedil;&atilde;o em NR 20 - Intermedi&aacute;rio</li><br />
<li style="box-sizing: border-box;">&bull; Capacita&ccedil;&atilde;o em NR 20 - Avan&ccedil;ado I</li><br />
<li style="box-sizing: border-box;">&bull; Capacita&ccedil;&atilde;o em NR 20 - Avan&ccedil;ado II</li><br />
<li style="box-sizing: border-box;">&bull; Capacita&ccedil;&atilde;o em NR 20 - Espec&iacute;fico</li><br />
<li style="box-sizing: border-box;">&bull; Lockout Tagout - Bloqueio e Sinaliza&ccedil;&atilde;o de Energias Perigosas</li><br />
<li style="box-sizing: border-box;">&bull; Capacita&ccedil;&atilde;o de Dire&ccedil;&atilde;o Defensiva e Primeiros Socorros</li><br />
<li style="box-sizing: border-box;">&bull; Curso de Identifica&ccedil;&atilde;o de Perigos e Avalia&ccedil;&atilde;o de Riscos em Seguran&ccedil;a no Trabalho</li><br />
<li style="box-sizing: border-box;">&bull; Curso de An&aacute;lise Avan&ccedil;ada de Riscos</li><br />
<li style="box-sizing: border-box;">&bull; Curso de An&aacute;lise de Acidentes e Incidentes em SSO</li><br />
<li style="box-sizing: border-box;">&bull; Curso de NR 15 B&aacute;sico - Atividades e Opera&ccedil;&otilde;es Insalubres</li><br />
<li style="box-sizing: border-box;">&bull; E-Social, Leiautes e Tabelas.</li><br />
<li style="box-sizing: border-box;">&nbsp;</li><br />
<li style="box-sizing: border-box;"><span style="color: darkred; font-size: 20px;">SA&Uacute;DE OCUPACIONAL</span></li><br />
</ul><br />
</div><br />
</div><br />
<div class="row" style="box-sizing: border-box; margin-right: -15px; margin-left: -15px; color: #454545; font-family: 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', 'DejaVu Sans', Verdana, sans-serif; font-size: 16px; background-color: #ffffff;"><br />
<div class="col-md-12" style="box-sizing: border-box; position: relative; min-height: 1px; padding-right: 15px; padding-left: 15px; float: left; width: 1170px;"><br />
<ul style="box-sizing: border-box; margin: 0px; list-style: none; padding: 0px;"><br />
<li style="box-sizing: border-box;">&bull; Curso de Suporte B&aacute;sico de Vida (BLS - Basic Life Support)</li><br />
<li style="box-sizing: border-box;">&bull; Primeira resposta em emerg&ecirc;ncias m&eacute;dicas</li><br />
<li style="box-sizing: border-box;">&bull; Resgate e Salvamento - APH T&aacute;tico</li><br />
<li style="box-sizing: border-box;">&bull; Curso de Elabora&ccedil;&atilde;o de PCMSO - Programa de Controle M&eacute;dico de Sa&uacute;de Ocupacional - NR 07</li><br />
<li style="box-sizing: border-box;">&bull; Seguran&ccedil;a e Sa&uacute;de no Trabalho em Estabelecimento de Sa&uacute;de - NR - 32</li><br />
<li style="box-sizing: border-box;">&bull; Curso do DEA - Desfibrilador Externo Autom&aacute;tico</li><br />
<li style="box-sizing: border-box;">&bull; Curso de First Responder</li><br />
</ul><br />
</div><br />
</div><br />
<div class="row" style="box-sizing: border-box; margin-right: -15px; margin-left: -15px; color: #454545; font-family: 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', 'DejaVu Sans', Verdana, sans-serif; font-size: 16px; background-color: #ffffff;"><br />
<div class="col-md-12" style="box-sizing: border-box; position: relative; min-height: 1px; padding-right: 15px; padding-left: 15px; float: left; width: 1170px;"><br />
<h3 style="box-sizing: border-box; font-weight: 400; line-height: 1.1; color: darkred; margin-top: 20px; margin-bottom: 10px; font-size: 20px;">ENGENHARIA DE MEIO AMBIENTE</h3><br />
<ul style="box-sizing: border-box; margin: 0px; list-style: none; padding: 0px;"><br />
<li style="box-sizing: border-box;">&bull; Curso de Identifica&ccedil;&atilde;o e Avalia&ccedil;&atilde;o de Aspectos e Impactos Ambientais</li><br />
<li style="box-sizing: border-box;">&bull; Curso de An&aacute;lise de Acidentes e Incidentes em MA</li><br />
</ul><br />
</div><br />
</div><br />
<div class="row" style="box-sizing: border-box; margin-right: -15px; margin-left: -15px; color: #454545; font-family: 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', 'DejaVu Sans', Verdana, sans-serif; font-size: 16px; background-color: #ffffff;"><br />
<div class="col-md-12" style="box-sizing: border-box; position: relative; min-height: 1px; padding-right: 15px; padding-left: 15px; float: left; width: 1170px;"><br />
<h3 style="box-sizing: border-box; font-weight: 400; line-height: 1.1; color: darkred; margin-top: 20px; margin-bottom: 10px; font-size: 20px;">ENGENHARIA DE COMBATE A INC&Ecirc;NDIO E P&Acirc;NICO</h3><br />
<ul style="box-sizing: border-box; margin: 0px; list-style: none; padding: 0px;"><br />
<li style="box-sizing: border-box;">&bull; Bombeiro Profissional Civil - Forma&ccedil;&atilde;o Inicial/Reciclagem</li><br />
<li style="box-sizing: border-box;">&bull; Brigada de Inc&ecirc;ndio - Forma&ccedil;&atilde;o/Reciclagem</li><br />
<li style="box-sizing: border-box;">&bull; Preven&ccedil;&atilde;o e combate a inc&ecirc;ndio florestal</li><br />
<li style="box-sizing: border-box;">&bull; Laudo SPDA;</li><br />
<li style="box-sizing: border-box;">&bull; Laudo de funcionabilidade de sistema de combate a incendio</li><br />
<li style="box-sizing: border-box;">&bull; Recarga e manuten&ccedil;&atilde;o de extintores</li><br />
<li style="box-sizing: border-box;">&bull; Teste hidrostatico de extintores e mangueiras</li><br />
<li style="box-sizing: border-box;">&bull; Laudo de funcionabilidade de sistema de combate a incendio.</li><br />
<li style="box-sizing: border-box;">&bull; Projeto e Execu&ccedil;&atilde;o de sistema de combate a inc&ecirc;ndio&nbsp;</li><br />
</ul><br />
</div><br />
</div><br />
<div class="row" style="box-sizing: border-box; margin-right: -15px; margin-left: -15px; color: #454545; font-family: 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', 'DejaVu Sans', Verdana, sans-serif; font-size: 16px; background-color: #ffffff;"><br />
<div class="col-md-12" style="box-sizing: border-box; position: relative; min-height: 1px; padding-right: 15px; padding-left: 15px; float: left; width: 1170px;"><br />
<h3 style="box-sizing: border-box; font-weight: 400; line-height: 1.1; color: darkred; margin-top: 20px; margin-bottom: 10px; font-size: 20px;">COMPLEMENTARES</h3><br />
<ul style="box-sizing: border-box; margin: 0px; list-style: none; padding: 0px;"><br />
<li style="box-sizing: border-box;">&bull; Rapel B&aacute;sico</li><br />
<li style="box-sizing: border-box;">&bull; Resgate em Altura</li><br />
<li style="box-sizing: border-box;">&bull; Resgate em Locais Remotos</li><br />
<li style="box-sizing: border-box;">&bull; Instrutor para Trabalhos em Altura - NR 35</li><br />
<li style="box-sizing: border-box;">&bull; Instrutor para Espa&ccedil;os Confinados - NR 33</li><br />
<li style="box-sizing: border-box;">&bull; Curso de Guarda Vida de Piscina</li><br />
<li style="box-sizing: border-box;">&bull; Resgate em locais confinados</li><br />
<li style="box-sizing: border-box;">&bull; Primeira resposta a emerg&ecirc;ncia com produtos perigosos</li><br />
<li style="box-sizing: border-box;">&nbsp;</li><br />
</ul><br />
</div><br />
</div>
                  </div>
               </div>

            </div>
         
            <div class="col-lg-4">
               
              <div class="d-flex align-items-center mb-3">
                <h1 class="title-blog my-0 color-red-dark text-shadow-1 fs-15 w-lg-75 w-xs-100" id="title-ultimas-noticias">ÚLTIMAS NOTÍCIAS</h1>
                <hr class="uk-divider border w-100 my-0 py-0">
              </div>

                                 <a href="/blog/post/32/cursos-de-seguranca-do-trabalho-norma-regulamentadora" class="d-flex align-items-center mb-2 hover-d-none color-51 color-hover-red-dark">
                     <div>
                        <div class="uk-inline-clip uk-transition-toggle" style="width: 125px;height: 80px;" tabindex="0">
                           <img src="https://lmcv2.s3-sa-east-1.amazonaws.com/plataformas/19/blog/artigos/32.png" style="width: 125px;height: 80px;" class="uk-transition-scale-up uk-transition-opaque rounded-2 object-fit-cover" alt=" | Resgate Vertical">
                        </div>
                     </div>
                     <p class="font-roboto ml-2 mb-0 fs-15">
                        Cursos de Segurança do Trabalho ( NORMA REGULAMENTADORA )
                     </p>
                  </a>
                                 <a href="/blog/post/31/terceirizacao-de-mao-de-obra" class="d-flex align-items-center mb-2 hover-d-none color-51 color-hover-red-dark">
                     <div>
                        <div class="uk-inline-clip uk-transition-toggle" style="width: 125px;height: 80px;" tabindex="0">
                           <img src="https://lmcv2.s3-sa-east-1.amazonaws.com/plataformas/19/blog/artigos/31.jpg" style="width: 125px;height: 80px;" class="uk-transition-scale-up uk-transition-opaque rounded-2 object-fit-cover" alt=" | Resgate Vertical">
                        </div>
                     </div>
                     <p class="font-roboto ml-2 mb-0 fs-15">
                        Terceirização de Mão de Obra
                     </p>
                  </a>
                                 <a href="/blog/post/30/spda-elaboracao-de-projeto-e-laudo-de-sistema-de-protecao-contra-descargas-atmosfericas" class="d-flex align-items-center mb-2 hover-d-none color-51 color-hover-red-dark">
                     <div>
                        <div class="uk-inline-clip uk-transition-toggle" style="width: 125px;height: 80px;" tabindex="0">
                           <img src="https://lmcv2.s3-sa-east-1.amazonaws.com/plataformas/19/blog/artigos/30.jpg" style="width: 125px;height: 80px;" class="uk-transition-scale-up uk-transition-opaque rounded-2 object-fit-cover" alt=" | Resgate Vertical">
                        </div>
                     </div>
                     <p class="font-roboto ml-2 mb-0 fs-15">
                        SPDA – Elaboração de  Projeto e Laudo De Sistema de Proteção Contra Descargas Atmosféricas
                     </p>
                  </a>
                                 <a href="/blog/post/29/recarga-e-manutencao-de-extintores" class="d-flex align-items-center mb-2 hover-d-none color-51 color-hover-red-dark">
                     <div>
                        <div class="uk-inline-clip uk-transition-toggle" style="width: 125px;height: 80px;" tabindex="0">
                           <img src="https://lmcv2.s3-sa-east-1.amazonaws.com/plataformas/19/blog/artigos/29.png" style="width: 125px;height: 80px;" class="uk-transition-scale-up uk-transition-opaque rounded-2 object-fit-cover" alt=" | Resgate Vertical">
                        </div>
                     </div>
                     <p class="font-roboto ml-2 mb-0 fs-15">
                        Recarga e manutenção de extintores.
                     </p>
                  </a>
                                 <a href="/blog/post/28/nossos-servicos" class="d-flex align-items-center mb-2 hover-d-none color-51 color-hover-red-dark">
                     <div>
                        <div class="uk-inline-clip uk-transition-toggle" style="width: 125px;height: 80px;" tabindex="0">
                           <img src="https://lmcv2.s3-sa-east-1.amazonaws.com/plataformas/19/blog/artigos/28.jpg" style="width: 125px;height: 80px;" class="uk-transition-scale-up uk-transition-opaque rounded-2 object-fit-cover" alt=" | Resgate Vertical">
                        </div>
                     </div>
                     <p class="font-roboto ml-2 mb-0 fs-15">
                        Nossos Serviços
                     </p>
                  </a>
               
            </div>

         </div>
      </section>

   </main>

   <footer class="bg-2 py-2" id="footer">
		<div class="container">
			<p class="mb-0 text-white font-nunito fs-8 ls-05">&copy; Resgate Vertical - Todos os direitos reservados</p>
		</div>	
	</footer>

	

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>



<script>

	$("#alertDanger").toast('show');
	$("#alertSuccess").toast('show');

	setTimeout(function() {

		$(".boxToast").addClass('d-none');

	}, 4500);
	
</script>

    
   <script src="https://cdn.jsdelivr.net/npm/uikit@3.5.4/dist/js/uikit.min.js"></script>

       
</body>
</html>