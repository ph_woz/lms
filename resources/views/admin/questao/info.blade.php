@extends('layouts.admin.master')

@section('content')
    
    @livewire('admin.questao.info', ['questao_id' => $questao_id])

@endsection