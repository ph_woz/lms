<div>

    <div class="col-12">
        @if (session()->has('success-livewire'))
            <div class="alert alert-success alert-dismissible" role="alert">
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                <div class="alert-message">
                    Avaliação criada:
                    <span>
	                    <a href="{{ route('admin.avaliacao.info', ['id' => $this->avaliacao->id ?? null]) }}" class="weight-600">
	                    	{{ $avaliacao->referencia }} 
	                	</a>
	                </span>
                </div>
            </div>
        @endif
    </div>

    <form wire:submit.prevent="add" method="POST" class="mb-4">

		<section class="mb-4-5">
			
			<a href="#boxInfoPrincipal" data-bs-toggle="collapse" aria-expanded="false" class="hover-d-none">
				<div class="section-title">
					Informações Gerais
				</div>
			</a>
			<hr/>
			
			<div class="collapse show" id="boxInfoPrincipal">

		    	<div class="d-lg-flex mb-3">

			    	<div class="w-100">
			    		<label>Referência</label>
			    		<input type="text" wire:model="referencia" class="form-control p-form" placeholder="Nome" required>
			    	</div>
					<div class="w-100 mx-lg-3 my-3 my-lg-0" wire:ignore>
						<label>Curso</label>
						<select wire:model="curso_id" class="selectpicker" title="Selecione" data-width="100%" data-live-search="true" required>
							@foreach($cursos as $curso)
								<option value="{{ $curso->id }}">
									{{ $curso->referencia }}
								</option>
							@endforeach
						</select>
			    		@error('curso_id')
			    			<span class="text-danger weight-600">
			    				{{ $message }}
			    			</span>
			    		@enderror
					</div>
					<div class="w-100">
						<label>Turma</label>
						<select wire:model="turma_id" class="select-form" required>
							<option value="">Selecione</option>
							@foreach($turmas as $turma)
								<option value="{{ $turma->id }}">
									{{ $turma->nome }}
								</option>
							@endforeach
						</select>
			    		@error('turma_id')
			    			<span class="text-danger weight-600">
			    				{{ $message }}
			    			</span>
			    		@enderror
					</div>

			    </div>

			    <div class="d-lg-flex mb-3">

			    	<div class="w-lg-45">
			    		<label>Status</label>
			    		<select wire:model="status" class="select-form">
			    			<option value="0">Ativo</option>
			    			<option value="1">Desativado</option>
			    		</select>
			    	</div>
			    	<div class="w-lg-45 ms-lg-3 mt-3 mt-lg-0">
			    		<label>Tentativas extras</label>
			    		<input type="text" wire:model="n_tentativas" class="form-control p-form" placeholder="N° de Tentativas extras" maxlength="3" value="0">
			    	</div>
			    	<div class="w-100 mx-lg-3 my-3 my-lg-0">
			    		<label>Condição para fazer a avaliação</label>
			    		<select wire:model="condicao_fazer_avaliacao" class="select-form">
			    			<option value="">Sem condição</option>
			    			<option value="concluir-conteudo-do-curso">É necessário ter concluído todo o conteúdo do curso selecionado para que esta avaliação seja liberada para ser feita</option>
			    		</select>
			    	</div>
			    	<div class="w-100">
			    		<label>Liberar gabarito após finalizar a avaliação</label>
			    		<select wire:model="liberar_gabarito" class="select-form">
			    			<option value="S">Sim</option>
			    			<option value="N">Não</option>
			    		</select>
			    	</div>

			    </div>

		    	<div class="w-100">
		    		<label>Instruções</label>
		    		<textarea wire:model="instrucoes" class="textarea-form" placeholder="Instruções"></textarea>
		    	</div>

			</div>

		</section>

		<section class="mb-4-5">
			
			<a href="#boxConfigNotas" data-bs-toggle="collapse" aria-expanded="false" class="hover-d-none">
				<div class="section-title">
					Configurações de Notas
				</div>
			</a>
			<hr/>
			
			<div class="collapse show" id="boxConfigNotas">

				@foreach($questoesQuantidadesMinimas as $value => $key)
			    	<div class="d-lg-flex align-items-top mb-2 border p-4" wire:ignore>
				    	<div class="w-100 me-lg-3 mb-3 mb-lg-0">
				    		<label>Quantidade de Questões</label>
				    		<input type="number" wire:model="questoesQuantidadesMinimas.{{$value}}" class="form-control p-form" placeholder="N°" required>
				    	</div>
				    	<div class="w-100">
				    		<label>Valor dessas Notas</label>
				    		<select wire:model="questoesNotas.{{$value}}" class="select-form" required>
								@foreach($listaNotas as $nota)
									<option value="{{ $nota }}">{{ $nota }}</option>
								@endforeach
				    		</select>
				    	</div>
				    	<div>
				    		<label> </label>
				    		<button class="btn btn-danger ms-1 py-2" wire:click.prevent="removeCampoNota({{$value}})"><i class="fa fa-trash"></i></button>
				    	</div>
				    </div>
				@endforeach

            	<button class="btn btn-primary fs-13 mt-1" wire:click.prevent="addCampoNota({{$i}})">+ Adicionar outro</button>

			</div>

		</section>

		<section class="mb-4-5" wire:ignore>
			
			<a href="#boxCategorias" data-bs-toggle="collapse" aria-expanded="false" class="hover-d-none">
				<div class="section-title">
					Categorias 
				</div>
			</a>
			<hr/>

			<div class="collapse" id="boxCategorias">
				<select wire:model="categoriasSelected" class="selectpicker" multiple data-width="100%" data-live-search="true" title="Selecione">
					@foreach($categorias as $categoria)
						<option value="{{ $categoria->id }}">{{ $categoria->nome }}</option>
					@endforeach
				</select>
			</div>

		</section>
		
		<button type="submit" class="btn-add mt-3">Cadastrar</button>	

    </form>

    @include('livewire.util.toast')

</div>