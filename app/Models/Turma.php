<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Turma extends Model
{
    use HasFactory;

    protected $table = 'turmas';

	public $timestamps = true;

    public $fillable = [
    	
        'plataforma_id',
        'curso_id',
        'formulario_id',
        'cadastrante_id',
        'unidade_id',
        
        'nome',
        'status',

        'tipo_inscricao',
        'publicar',
        'modalidade',
        'data_inicio',
        'data_termino',
        'frequencia',
        'horario',
        'duracao',

        'tipo_valor',
        'tipo_mensalidade',

        'valor_parcelado',
        'valor_a_vista',
        'valor_promocional',
        'valor_mensalidade',
        'desconto_agilpass',

        'nota_minima_aprovacao',
    ];

    public static  function boot()
    {
        parent::boot(); 
        static::creating(function ($model) {
            $model->plataforma_id  = session('plataforma_id') ?? \Auth()->user()->plataforma_id ?? Plataforma::dominio()->pluck('id')[0] ?? null;
            $model->cadastrante_id = \Auth()->user()->id ?? null;
        });
    }
    
    public function scopePlataforma($query)
    {
        return $query->where('plataforma_id', session('plataforma_id') ?? Plataforma::dominio()->pluck('id')[0] ?? null);
    }

    public function scopeAtivo($query)
    {
        return $query->where('status', 0);
    }

    public function scopePublico($query)
    {
        return $query->where('publicar', 'S');
    }

    public static function checkActive()
    {
        $check = PlataformaModulo::plataforma()->ativo()->where('url','turma')->exists();
        
        return $check;
    }

}
