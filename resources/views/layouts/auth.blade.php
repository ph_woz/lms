<!doctype html>
<html lang="pt-br">
<head>

  <!-- Google tag (gtag.js) -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=G-MREB450HCT"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'G-MREB450HCT');
  </script>
  
    {!! $plataforma->scripts_start_head !!}

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="robots" content="noindex, nofollow">
    <title>Acesse sua conta - {{ $plataforma->nome }}</title>

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/util.css') }}" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">

    <style>
        body { 
            background-image: url("{{ $plataforma->fundo_login ?? asset('images/bg-login.png') }}");
            background-repeat:no-repeat;
            background-size: cover;
        }
        .boxLogin { box-shadow: 0px 10px 20px -10px #022b73; border-radius: 5px; }
        footer a, footer p { opacity: 0.7; color: #F9F9F9; }
        footer a:hover { color: #F9F9F9; }
    </style>

    {!! $plataforma->scripts_final_head !!}

</head>
<body class="bg-body-dark-grade">

    {!! $plataforma->scripts_start_body !!}
    
    <main>
        @yield('content')
    </main>

    <footer class="py-2 text-center">

        <p class="fs-12 mb-0">
            Todos os seus dados estão seguros e jamais serão compartilhados.
        </p>

        <div class="mb-3">
            <a href="{{ route('termos.de.uso') }}" target="_blank" class="fs-12 text-decoration-underline">Termos de Uso</a>
            <span class="color-777 fs-12">e</span>
            <a href="{{ route('politica.de.privacidade') }}" target="_blank" class="fs-12 text-decoration-underline">Política de Privacidade</a>
        </div>

        <a href="https://wozcode.com" target="_blank" class="fs-12 text-decoration-none">
            Tecnologia desenvolvida por WOZCODE &copy;
        </a>

    </footer>

    @if(session()->has('success'))
        <div id="box-toast-success" class="boxToast">
            <div id="alertSuccess" class="toast-success">
                <p class="mt-2 text-white">
                    <span class="weight-800 font-inter">Sucesso!&nbsp;</span>{{ session('success') }}
                </p>
            </div>
        </div>
        <script>
            setTimeout(function() {

                document.getElementById('box-toast-success').style.display = 'none';

            }, 2500);
        </script>
    @endif

    @if(session()->has('danger'))
        <div id="box-toast-danger" class="boxToast">
            <div id="alertDanger" class="toast-danger">
                <p class="mt-2 text-white">
                    <span class="weight-800 font-inter">Ops!&nbsp;</span>{{ session('danger') }}
                </p>
            </div>
        </div>
        <script>
            setTimeout(function() {

                document.getElementById('box-toast-danger').style.display  = 'none';

            }, 2500);
        </script>
    @endif

    {!! $plataforma->scripts_final_body !!}

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.js"></script>
    <script>
        $(".cpf").mask('000.000.000-00');
    </script>

</body>
</html>
