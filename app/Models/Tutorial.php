<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tutorial extends Model
{
    use HasFactory;

    protected $table = 'tutoriais';

    public $timestamps = false;

    public $fillable = [
    	'titulo',
        'capa',
    	'link',
    	'status',
    ];

    public function scopeAtivo($query)
    {
        return $query->where('status', 0);
    }
}
