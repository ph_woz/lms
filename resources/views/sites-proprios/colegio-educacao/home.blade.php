<!doctype html>
<html lang="pt-br">
<head>

   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1">

   <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/uikit@3.5.9/dist/css/uikit.min.css" />
   <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
   <link rel="stylesheet" href="{{ asset('css/util.css') }}">
   <link rel="stylesheet" href="{{ asset('sites-proprios/colegio-educacao/css/home.css') }}?v=2">

   <title>{{ $plataforma->nome }} - Ensino de qualidade e acessível a todos</title>
   <meta name="robots" content="index, follow">
   <meta name="description" content="{{ $plataforma->nome }} oferece um ensino de qualidade e acessível a todos. Atualmente contamos com três modalidades: EJA - Ensino Fundamental e Médio - cada série em 6 meses. Curso Normal (formação de Professores) - 9 meses letivos. Cursos Profissionalizantes e de Aperfeiçoamento."/>
   <meta name="keywords" content="colegio educação, colegio educacao, colegio e curso educacao, colegio e curso educação"/>
   <link rel="canonical" href="{{ Request::url() }}" />
   <link rel="shortcut icon" href="{{ $plataforma->favicon }}">
   <meta property="og:locale" content="pt_BR" />
   <meta property="og:type" content="website" />
   <meta property="og:title" content="{{ $plataforma->nome }} - Ensino de qualidade e acessível a todos" />
   <meta property="og:description" content="{{ $plataforma->nome }} oferece um ensino de qualidade e acessível a todos. Atualmente contamos com três modalidades: EJA - Ensino Fundamental e Médio - cada série em 6 meses. Curso Normal (formação de Professores) - 9 meses letivos. Cursos Profissionalizantes e de Aperfeiçoamento." />
   <meta property="og:url" content="{{ Request::url() }}" />
   <meta property="og:site_name" content="{{ $plataforma->nome }}" />
   <meta property="og:image" content="{{ $plataforma->logotipo }}" />
   <meta name='author' content='WOZCODE | Pedro Henrique' />

</head>
<body class="bg-f4" id="inicio">

   <header>
      
      <div class="sub-header container-fluid px-xl-7 py-2-3">
         <div class="d-flex justify-content-between align-items-center">

            <div>

               <span class="d-xs-none font-poppins text-light text-shadow-2 fs-12">
                  <i class="fa fa-envelope"></i>
                  {{ $plataforma->email }}
               </span>

               <a href="https://api.whatsapp.com/send?phone=55{{preg_replace('/\D/', '', $plataforma->whatsapp)}}&text=Olá, vim através do Site {{ $plataforma->nome }}" target="_blank" class="mx-3 text-shadow-2 text-d-none font-poppins text-light hover-d-none fs-12">
                  <i class="fab fa-whatsapp"></i>
                  {{ $plataforma->whatsapp }}
               </a>

               <a href="tel:+55{{preg_replace('/\D/', '', $plataforma->telefone)}}" class="d-xs-none text-shadow-2 text-d-none font-poppins text-light hover-d-none fs-12">
                  <i class="fa fa-phone"></i>
                  {{ $plataforma->telefone }}
               </a>

            </div>

            <div class="d-flex justify-content-between align-items-end">

               <div>
                  <a href="{{ $plataforma->facebook }}" target="_blank" class="fs-17 text-d-none text-light text-shadow-2">
                       <i class="fab fa-facebook-f flex-center"></i>
                  </a>
                  <a href="{{ $plataforma->instagram }}" target="_blank" class="ms-2 fs-17 text-d-none text-light text-shadow-2">
                     <i class="fab fa-instagram flex-center"></i>
                  </a>
               </div>

            </div>

         </div>
      </div>

      <nav class="navbar navbar-expand-lg bg-white box-shadow py-0 z-3" id="menu">
         <div class="container-fluid px-xl-7">
             <a class="navbar-brand" href="#inicio" uk-scroll>
               <img src="{{ $plataforma->logotipo }}" class="d-xs-none" width="90" alt="Logotipo SD Saúde">
               <img src="{{ $plataforma->logotipo }}" class="d-md-none" id="logoMenuMobile" width="75" alt="Logotipo SD Saúde">
             </a>
             <button class="navbar-toggler outline-0 border-0 text-dark" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
               <span class="fa fa-bars"></span>
             </button>
             <div class="collapse navbar-collapse" id="navbarSupportedContent">
                  <ul id="ulLinks" class="navbar-nav ms-auto mb-2 mb-lg-0 align-items-lg-center p-3 p-lg-auto">
                     <li class="nav-item">
                        <a class="nav-link" href="#cursos" uk-scroll="offset: 140;">
                          Nossos Cursos
                        </a>
                     </li>
                     <li class="nav-item">
                        <a class="nav-link" href="#quem-somos" uk-scroll="offset: 140;">
                           Quem somos
                        </a>
                     </li>
                     <li class="nav-item">
                        <a class="nav-link" href="#cursos" uk-scroll="offset: 140;">
                           Cursos
                        </a>
                     </li>
                     <li class="nav-item">
                        <a class="nav-link" href="#contato" uk-scroll="offset: 100;">
                           Fale Conosco
                        </a>
                     </li>
                     <li class="nav-item ms-lg-5 mt-lg-1 mt-4">
                       <a class="nav-link btnInscrever animation-grow d-inline-block text-shadow-1" href="#pre-matricula" uk-scroll="offset: 140;" style="background: #63bd37;">
                          Matricule-se
                       </a>
                     </li>
                     <li class="nav-item ms-lg-4 mt-lg-1 mt-4">
                       <a class="nav-link btnInscrever animation-grow d-inline-block" href="/login">
                          <i class="fa fa-user-graduate"></i>
                          Área do Aluno
                       </a>
                     </li>
                 </ul>
             </div>
         </div>
      </nav>

   </header>

   <main>

      <section id="slides">
        <div class="uk-position-relative uk-visible-toggle" tabindex="-1" uk-slideshow="animation: push; autoplay: true; autoplay-interval: 5000; pause-on-hover: false; min-height: 550; max-height: 550">

            <ul class="uk-slideshow-items">
              @foreach($banners as $banner)
                <li>
                  <div class="uk-background-cover" uk-parallax="bgy: -100" style="background-image: url('{{ $banner->link }}');" uk-cover>
                     @if($banner->overlay == 'S')
                        <div class="background-overlay" style="background: {{ $banner->overlay_color ?? 'unset' }};opacity: {{ $banner->overlay_opacity ?? '0' }};"></div>
                     @endif
                     <div uk-scrollspy="cls: uk-animation-fade; delay: 400;" class="container-fluid px-xl-7 mt--10px uk-position-center-left uk-position-small uk-light">
                      
                        <h2 class="text-center uk-margin-remove font-montserrat weight-800 display-3 ls--03" uk-slideshow-parallax="x: 300,0,-100">
                           {!! $banner->titulo !!}
                        </h2>

                        @if($banner->subtitulo)
                           <p uk-slideshow-parallax="x: 300,0,-75" class="text-center uk-margin-remove text-light fs-20">
                              {!! $banner->subtitulo !!}
                           </p>
                        @endif

                        @if($banner->botao_texto)
                           <div class="mt-4-5 text-center">
                              <a href="{{ $banner->botao_link }}" class="btnInscricao text-shadow-1 d-xs-block text-center text-shadow-1" uk-slideshow-parallax="x: 300,0,-50">
                                 {!! $banner->botao_texto !!}
                              </a>
                           </div>
                        @endif

                     </div>
                  </div>
                </li>
              @endforeach
            </ul>

            <div class="uk-light">
                <a class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous uk-slideshow-item="previous"></a>
                <a class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-next uk-slideshow-item="next"></a>
            </div>

        </div>
      </section>

      <section id="pre-matricula">
         <div style="background-image: url('{{ asset('images/bg-line.gif') }}');">
            <div class="container-fluid py-5 px-xl-7">

               <div class="bg-white p-lg-5 p-4 box-shadow rounded-20">

                  <h1 class="fs-40 text-center font-montserrat weight-800 text-dark">FAÇA SUA <span class="color-faixa-verde">PRÉ-MATRÍCULA</span></h1>

                  <p class="font-poppins text-center">
                     Preenchendo o formulário abaixo
                  </p>

                  <div class="flex-center mt-4 mb-4-5"><div class="w-80px h-2px bg-line rounded"></div></div>

                  <form method="POST">
                  @csrf

                    @if(session('success_prematricula'))
                       <div class='alert alert-success'><b>Sucesso!</b> {{ session('success_prematricula') }}</div>
                    @endif

                    <input type="hidden" name="assunto" value="Pré-Matrícula">
                     
                     <div class="d-lg-flex">
                        <input type="text" name="nome" class="form" placeholder="Seu nome" required>
                        <input type="text" name="email" class="mx-lg-3 my-3 my-lg-0 form" placeholder="Seu email" required>
                        <input type="text" name="telefone" class="form" placeholder="Telefone ou Celular" required>
                     </div>
                     <div class="w-100 mt-3">
                        <select name="curso_id" class="form weight-700 color-777" required>
                           <option value="">Selecione o Curso de Interesse</option>
                            @foreach($cursos as $curso)
                              <option value="{{ $curso->id }}" class="text-dark">{{ $curso->nome }}</option>
                            @endforeach
                        </select>
                     </div>
                     <div class="mt-3 mb-2">
                       <label class="cursor-pointer font-poppins fs-14 color-base-blue">
                          <input type="checkbox" class="" name="consentimento" value="1" required> Concordo que os dados pessoais fornecidos acima serão utilizados para envio de conteúdo informativo, analítico e publicitário sobre produtos, serviços e assuntos gerais, nos termos da Lei Geral de Proteção de Dados.
                       </label>
                     </div>
                     
                     <input type="text" name="check" class="invisible h-0px d-block" placeholder="Não preencha este campo.">

                     <button type="submit" name="sentContato" value="S" class="btnInscrever weight-700 box-shadow mt--15px">ENVIAR</button>

                  </form>

               </div>

            </div>
         </div>
      </section>

      <section id="quem-somos" class="py-5">
         <div class="bg-faixa p-lg-5 p-4 h-lg-500">
            <div class="container-fluid px-xl-4">

               <h1 class="font-montserrat text-light text-center weight-700 fs-md-55">
                  Há mais de uma década <br> educando para o sucesso
               </h1>

               <div class="mt-4 mb-5 text-center px-xl-7">
                  <p class="px-xl-7 font-poppins fs-16 color-sub">
                    Somos uma Instituição de Ensino, há 17 anos, nosso objetivo é formar para tornar o nosso país uma potência. 
Trabalhamos com tecnologia de ponta e a nossa proposta é fazer com que o aluno  saia de nossas salas com um conhecimento teórico e prático bem sedimentado.
                  </p>
               </div>

              <div class="uk-child-width-1-3@m" uk-grid uk-scrollspy="cls: uk-animation-slide-bottom; target: .uk-card-quem-somos; delay: 300; repeat: true;">
                 <div>
                    <div class="uk-card-quem-somos text-center">
                     <div class="bg-white p-4 box-shadow rounded-20 h-lg-250">
                        <i class="fab fa-connectdevelop d-block mb-3 color-faixa-verde fs-50 mt-4"></i>
                        <p class="mb-0 font-montserrat weight-800 ls-02 color-navlink fs-20 mt-4">
                           Conectividade
                        </p>
                        <p class="mb-0 font-poppins weight-600 text-muted fs-14 mt-2">
                           Ensino dinâmico e interativo.
                        </p>
                     </div>
                    </div>
                 </div>
                 <div>
                    <div class="uk-card-quem-somos text-center">
                     <div class="bg-white p-4 box-shadow rounded-20 h-lg-250">
                        <i class="fas fa-chalkboard-teacher d-block mb-3 color-faixa-verde fs-50 mt-4"></i>
                        <p class="mb-0 font-montserrat weight-800 ls-02 color-navlink fs-20 mt-4">
                           Preparação
                        </p>
                        <p class="mb-0 font-poppins weight-600 text-muted fs-14 mt-2">
                           Ensino dinâmico e interativo.
                        </p>
                     </div>
                    </div>
                 </div>
                 <div>
                    <div class="uk-card-quem-somos text-center">
                     <div class="bg-white p-4 box-shadow rounded-20 h-lg-250">
                        <i class="fas fa-chart-line d-block mb-3 color-faixa-verde fs-50 mt-4"></i>
                        <p class="mb-0 font-montserrat weight-800 ls-02 color-navlink fs-20 mt-4">
                           Excelência
                        </p>
                        <p class="mb-0 font-poppins weight-600 text-muted fs-14 mt-2">
                           Ensino dinâmico e interativo.
                        </p>
                     </div>
                    </div>
                 </div>
              </div>

            </div>
         </div>
      </section>

      <section class="py-5">
         <div style="background-image: url('https://eaducativa.com/wp-content/uploads/2019/05/bg-gray.gif');">
            <div class="container-fluid px-xl-6 pt-lg-5" id="cursos">

               <h1 class="fs-37 text-center font-montserrat text-dark weight-800 mt-lg-5 pt-lg-3">NOSSOS <span class="color-faixa-verde">CURSOS</span></h1>

               <div class="mb-4-5 container px-xl-7">
                  <p class="font-poppins text-center px-xl-7">
                    O {{ $plataforma->nome }} conta com diversas modalidades de cursos e serviços, adequando-se às necessidades específicas de cada cliente.
                  </p>
               </div>

               <div class="flex-center mt-4-5 mb-4-5"><div class="w-80px h-2px bg-line rounded"></div></div>

               <div class="uk-slider-container-offset" uk-slider="autoplay: true; autoplay-interval: 800; pause-on-hover: true">

                   <div class="uk-position-relative uk-visible-toggle uk-light" tabindex="-1">

                       <ul class="uk-slider-items uk-child-width-1-5@s">
                           @foreach($cursos as $curso)
                           <li class="px-1">
                              <a href="/curso/{{$curso->slug}}" class="w-100">
                                 <div class="w-100 uk-inline-clip uk-transition-toggle rounded box-shadow mb-4 mb-lg-0" tabindex="0">
                                    <img src="{{ $curso->foto_capa ?? asset('images/no-image.jpeg') }}" class="uk-transition-scale-up uk-transition-opaque object-fit-cover w-100 h-lg-350 h-xs-350">
                                    <div class="uk-position-bottom fig-caption flex-center px-3">
                                       <h5 class="text-light fs-17 font-montserrat weight-700 ls-05 text-uppercase text-center">
                                          {{ $curso->nome }}
                                       </h5>
                                    </div>
                                 </div>
                              </a>
                           </li>
                           @endforeach
                       </ul>

                       <a class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous uk-slider-item="previous"></a>
                       <a class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-next uk-slider-item="next"></a>

                   </div>

                   <ul class="uk-slider-nav uk-dotnav uk-flex-center uk-margin"></ul>

               </div>

            </div>
         </div>
      </section>

      <section id="quem-somos" class="py-5">
         <div class="bg-faixa p-lg-5 p-4 h-lg-325">
            <div class="container-fluid px-xl-4">
               <div class="row">

                  <div class="col-lg-6 mb-4 mb-lg-0">

                     <div class="pe-lg-5">

                        <h1 class="fs-35 font-montserrat color-faixa weight-800 text-uppercase">O que falam de nós!</h1>
                        <div class="my-4 w-80px h-2px bg-line rounded"></div>

                        <p class="font-poppins color-faixa fs-14">
                           Ao longo dos Anos, com base do que é visivel sobre nossos ensinos, envaidece.nos deixando assim a nosso  cargo a obrigação de proporcionar ao alunado  ensinamentos com tecnologias avançadas,pautadas nos melhores referênciais pedagógicas permitindo.nos oferecer um Ensino com investimento baixo com a qualidade que permite colocar no mercado os melhores profissionais. Primamos pela Excelência.
                        </p>

                     </div>

                  </div>

                  <div class="col-lg-6">

                     <div class="uk-slider-container-offset" uk-slider>

                         <div class="uk-position-relative uk-visible-toggle uk-light" tabindex="-1">

                             <ul class="uk-slider-items uk-child-width-1-1@s uk-grid">
                              @foreach($depoimentos as $depoimento)
                                 <li>
                                     <div class="uk-card box-shadow bg-white rounded-10 h-lg-325 cursor-drag">
                                         <div class="uk-card-body text-dark">
                                             <i class="fas fa-quote-left fs-50 color-faixa-verde opacity-04"></i>
                                             <div class="mt-4 d-lg-flex align-items-center">
                                                <div style="width: 100px;">
                                                   <img src="{{ $depoimento->foto }}" class="object-fit-cover rounded-circle square-100">
                                                   <p class="mb-0 mt-3 color-faixa-verde text-lg-center ms--lg-5 weight-600">
                                                      {{ $depoimento->nome }}
                                                   </p>
                                                </div>
                                                <p class="mt-lg-3 mt-0 ms-lg-4 font-poppins fs-14 color-555">
                                                  {{ $depoimento->descricao }}
                                                </p>
                                             </div>
                                         </div>
                                     </div>
                                 </li>
                                 @endforeach
                             </ul>

                             <a class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous uk-slider-item="previous"></a>
                             <a class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-next uk-slider-item="next"></a>

                         </div>

                         <ul class="uk-slider-nav uk-dotnav uk-flex-center uk-margin"></ul>

                     </div>

                  </div>

               </div>
            </div>
         </div>
      </section>

   </main>

   <footer id="contato" style="background-image: url('{{ asset('sites-proprios/colegio-educacao/images/footer.jpg') }}'); background-repeat: no-repeat; background-size: cover; background-position: center;">
      <div class="container-fluid px-xl-6 pt-5" style="margin-top: -50px;">

         <div class="row py-lg-4 px-3 px-md-0">

            <div class="col-lg-6">      

               <div id="polo-matriz">

                  <h1 class="fs-30 font-montserrat text-light weight-800">CONTATO</h1>
                  <div class="w-80px h-2px bg-line rounded mt-4 mb-4-5"></div>

                  <div class="mb-4-5">
                     <p class="mb-0 font-opensans weight-600 text-white d-flex align-items-center">
                        <i class="fa fa-clock fs-35 color-green"></i>
                        <span class="ms-3">HORÁRIO DE ATENDIMENTO</span>
                     </p>
                     <p class="mt-1 mt-lg-0 mb-0 font-opensans weight-600 fs-15 color-faixa ms-5">
                        {{ $plataforma->horario }}
                     </p>
                  </div>

                  <div class="mb-4-5">
                     <p class="mb-0 font-opensans weight-600 text-white d-flex align-items-center">
                        <i class="fa fa-envelope fs-35 color-green"></i>
                        <span class="ms-3">EMAIL </span>
                     </p>
                     <p class="mt-1 mt-lg-0 mb-0 font-opensans weight-600 fs-15 color-faixa ms-5">
                        &nbsp;{{$plataforma->email}}
                     </p>
                  </div>

                  <div class="mb-4-5">
                     <p class="mb-0 font-opensans weight-600 text-white d-flex align-items-center">
                        <i class="fab fa-whatsapp fs-40 color-green"></i>
                        <span class="ms-3">WHATSAPP</span>
                     </p>
                     <div class="mt-1 mt-lg-0 mb-0 font-opensans weight-600 fs-15 color-faixa ms-5">
                        <p class="mb-0">
                           <a href="https://api.whatsapp.com/send?phone=55{{preg_replace('/\D/', '', $plataforma->whatsapp)}}&text=Olá, vim através do Site {{ $plataforma->nome }}" target="_blank" class="text-d-none color-faixa hover-white">{{ $plataforma->whatsapp }}</a>
                        </p>
                     </div>
                  </div>

                  <div class="mb-4-5">
                     <p class="mb-0 font-opensans weight-600 text-white d-flex align-items-center">
                        <i class="fa fa-phone fs-40 color-green"></i>
                        <span class="ms-2">TELEFONE</span>
                     </p>
                     <div class="mt-1 mt-lg-0 mb-0 font-opensans weight-600 fs-15 color-faixa ms-5">
                        <p class="mb-0">
                           <a href="tel:+55{{preg_replace('/\D/', '', $plataforma->telefone)}}" target="_blank" class="text-d-none color-faixa hover-white">{{ $plataforma->telefone }}</a>
                        </p>
                     </div>
                  </div>

                  <div class="mb-4-5">
                     <p class="mb-0 font-opensans weight-600 text-white d-flex align-items-center">
                        <i class="fa fa-map-marker-alt fs-35 color-green"></i>
                        <span class="ms-3">ENDEREÇO</span>
                     </p>
                     <address class="mt-1 mt-lg-0 mb-0 font-opensans weight-600 fs-15 color-faixa ms-4">
                        <span class="ms-3 d-block"> 
                           {{ $plataforma->endereco }}
                        </span>
                     </address>
                  </div>

               </div>

               <!--
               <div id="outros-polos">

                  <h2 class="fs-21 font-montserrat color-faixa weight-800">OUTROS POLOS</h2>
                  <div class="w-80px h-2px bg-line rounded mt-3 mb-4"></div>

                  <a href="#ModalPoloTijuca" data-bs-toggle="modal" class="btn btn-success bg-wpp text-shadow-1 weight-600 py-2-5 font-poppins hover-d-none">
                     POLO TIJUCA
                  </a>

               </div>
               -->

            </div>

            <hr class="d-lg-none border border-light" />

            <div class="col-lg-6">

               <h1 class="pt-4 fs-35 font-montserrat text-light weight-800">FALE CONOSCO</h1>
               <h2 class="fs-19 font-montserrat color-faixa weight-800">NOS ENVIE UMA MENSAGEM</h2>
               <div class="mt-4-5 mb-4-5"><div class="w-80px h-2px bg-line rounded"></div></div>

               <form method="POST">
               @csrf

                  @if(session('success'))
                     <div class='alert alert-success'><b>Sucesso!</b> {{ session('success') }}</div>
                  @endif
                
                  <input type="hidden" name="assunto" value="Fale Conosco">
                  <input type="text" name="nome" class="form-contact mb-2" placeholder="Nome" required>
                  <input type="tel" name="celular" class="form-contact mb-2" placeholder="Telefone" required>
                  <input type="email" name="email" class="form-contact mb-2" placeholder="Email" required>
                  <textarea name="mensagem" placeholder="Mensagem" class="form-contact h-min-100 mb-2 p-3 h-min-100"></textarea>
               
                  <div class="mt-3 mb-4">
                     <label class="cursor-pointer font-poppins fs-14 text-white">
                        <input type="checkbox" class="" name="consentimento" value="1" required> 
                        Concordo que os dados pessoais fornecidos acima serão utilizados para envio de conteúdo informativo, analítico e publicitário sobre produtos, serviços e assuntos gerais, nos termos da Lei Geral de Proteção de Dados.
                     </label>
                  </div>

                  <input type="text" name="check" class="invisible h-0px d-block" placeholder="Não preencha este campo.">
                  <input type="submit" value="Enviar" name="sentContato" class="btnEntrarEmContato text-shadow-1">

               </form>
            </div>
            
         </div>

         <div class="row mt-5">

            <div class="col-lg-4 mb-5 mb-lg-0">
               <h5 class="text-light fs-17">Localização</h5>
               <hr class="border" />
               <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14698.82269875565!2d-45.4682567!3d-22.9242244!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x70581a8d7cfa2dad!2sCEDETEP%20Escola%20T%C3%A9cnica%20-%20Pindamonhangaba!5e0!3m2!1spt-BR!2sbr!4v1661004483315!5m2!1spt-BR!2sbr" width="100%" height="303" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
            </div>

            <div class="col-lg-4 mb-5 mb-lg-0">
               <h5 class="text-light fs-17">Fã Page Facebook</h5>
               <hr class="border" />

               <script async defer crossorigin="anonymous" src="https://connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v7.0"></script>
               <div class="fb-page w-100" data-href="https://www.facebook.com/cedetepoficial/" data-tabs="timeline" data-height="303" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/cedetepoficial/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/cedetepoficial/">{{ $plataforma->nome }}</a></blockquote></div>
            </div>

            <div class="col-lg-4 mb-5 mb-lg-0">
               <h5 class="text-light fs-17">Instagram</h5>
               <hr class="border" />
               <blockquote class="instagram-media h-lg-300" data-instgrm-permalink="https://www.instagram.com/reel/CgkAQqIlxYr/?utm_source=ig_embed&amp;utm_campaign=loading" data-instgrm-version="14" style=" background:#FFF; border:0; border-radius:3px; box-shadow:0 0 1px 0 rgba(0,0,0,0.5),0 1px 10px 0 rgba(0,0,0,0.15); margin: 1px; max-width:540px; min-width:326px; padding:0; width:99.375%; width:-webkit-calc(100% - 2px); width:calc(100% - 2px);"><div style="padding:16px;"> <a href="https://www.instagram.com/reel/CgkAQqIlxYr/?utm_source=ig_embed&amp;utm_campaign=loading" style=" background:#FFFFFF; line-height:0; padding:0 0; text-align:center; text-decoration:none; width:100%;" target="_blank"> <div style=" display: flex; flex-direction: row; align-items: center;"> <div style="background-color: #F4F4F4; border-radius: 50%; flex-grow: 0; height: 40px; margin-right: 14px; width: 40px;"></div> <div style="display: flex; flex-direction: column; flex-grow: 1; justify-content: center;"> <div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; margin-bottom: 6px; width: 100px;"></div> <div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; width: 60px;"></div></div></div><div style="padding: 19% 0;"></div> <div style="display:block; height:50px; margin:0 auto 12px; width:50px;"><svg width="50px" height="50px" viewBox="0 0 60 60" version="1.1" xmlns="https://www.w3.org/2000/svg" xmlns:xlink="https://www.w3.org/1999/xlink"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g transform="translate(-511.000000, -20.000000)" fill="#000000"><g><path d="M556.869,30.41 C554.814,30.41 553.148,32.076 553.148,34.131 C553.148,36.186 554.814,37.852 556.869,37.852 C558.924,37.852 560.59,36.186 560.59,34.131 C560.59,32.076 558.924,30.41 556.869,30.41 M541,60.657 C535.114,60.657 530.342,55.887 530.342,50 C530.342,44.114 535.114,39.342 541,39.342 C546.887,39.342 551.658,44.114 551.658,50 C551.658,55.887 546.887,60.657 541,60.657 M541,33.886 C532.1,33.886 524.886,41.1 524.886,50 C524.886,58.899 532.1,66.113 541,66.113 C549.9,66.113 557.115,58.899 557.115,50 C557.115,41.1 549.9,33.886 541,33.886 M565.378,62.101 C565.244,65.022 564.756,66.606 564.346,67.663 C563.803,69.06 563.154,70.057 562.106,71.106 C561.058,72.155 560.06,72.803 558.662,73.347 C557.607,73.757 556.021,74.244 553.102,74.378 C549.944,74.521 548.997,74.552 541,74.552 C533.003,74.552 532.056,74.521 528.898,74.378 C525.979,74.244 524.393,73.757 523.338,73.347 C521.94,72.803 520.942,72.155 519.894,71.106 C518.846,70.057 518.197,69.06 517.654,67.663 C517.244,66.606 516.755,65.022 516.623,62.101 C516.479,58.943 516.448,57.996 516.448,50 C516.448,42.003 516.479,41.056 516.623,37.899 C516.755,34.978 517.244,33.391 517.654,32.338 C518.197,30.938 518.846,29.942 519.894,28.894 C520.942,27.846 521.94,27.196 523.338,26.654 C524.393,26.244 525.979,25.756 528.898,25.623 C532.057,25.479 533.004,25.448 541,25.448 C548.997,25.448 549.943,25.479 553.102,25.623 C556.021,25.756 557.607,26.244 558.662,26.654 C560.06,27.196 561.058,27.846 562.106,28.894 C563.154,29.942 563.803,30.938 564.346,32.338 C564.756,33.391 565.244,34.978 565.378,37.899 C565.522,41.056 565.552,42.003 565.552,50 C565.552,57.996 565.522,58.943 565.378,62.101 M570.82,37.631 C570.674,34.438 570.167,32.258 569.425,30.349 C568.659,28.377 567.633,26.702 565.965,25.035 C564.297,23.368 562.623,22.342 560.652,21.575 C558.743,20.834 556.562,20.326 553.369,20.18 C550.169,20.033 549.148,20 541,20 C532.853,20 531.831,20.033 528.631,20.18 C525.438,20.326 523.257,20.834 521.349,21.575 C519.376,22.342 517.703,23.368 516.035,25.035 C514.368,26.702 513.342,28.377 512.574,30.349 C511.834,32.258 511.326,34.438 511.181,37.631 C511.035,40.831 511,41.851 511,50 C511,58.147 511.035,59.17 511.181,62.369 C511.326,65.562 511.834,67.743 512.574,69.651 C513.342,71.625 514.368,73.296 516.035,74.965 C517.703,76.634 519.376,77.658 521.349,78.425 C523.257,79.167 525.438,79.673 528.631,79.82 C531.831,79.965 532.853,80.001 541,80.001 C549.148,80.001 550.169,79.965 553.369,79.82 C556.562,79.673 558.743,79.167 560.652,78.425 C562.623,77.658 564.297,76.634 565.965,74.965 C567.633,73.296 568.659,71.625 569.425,69.651 C570.167,67.743 570.674,65.562 570.82,62.369 C570.966,59.17 571,58.147 571,50 C571,41.851 570.966,40.831 570.82,37.631"></path></g></g></g></svg></div><div style="padding-top: 8px;"> <div style=" color:#3897f0; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:550; line-height:18px;">View this post on Instagram</div></div><div style="padding: 12.5% 0;"></div> <div style="display: flex; flex-direction: row; margin-bottom: 14px; align-items: center;"><div> <div style="background-color: #F4F4F4; border-radius: 50%; height: 12.5px; width: 12.5px; transform: translateX(0px) translateY(7px);"></div> <div style="background-color: #F4F4F4; height: 12.5px; transform: rotate(-45deg) translateX(3px) translateY(1px); width: 12.5px; flex-grow: 0; margin-right: 14px; margin-left: 2px;"></div> <div style="background-color: #F4F4F4; border-radius: 50%; height: 12.5px; width: 12.5px; transform: translateX(9px) translateY(-18px);"></div></div><div style="margin-left: 8px;"> <div style=" background-color: #F4F4F4; border-radius: 50%; flex-grow: 0; height: 20px; width: 20px;"></div> <div style=" width: 0; height: 0; border-top: 2px solid transparent; border-left: 6px solid #f4f4f4; border-bottom: 2px solid transparent; transform: translateX(16px) translateY(-4px) rotate(30deg)"></div></div><div style="margin-left: auto;"> <div style=" width: 0px; border-top: 8px solid #F4F4F4; border-right: 8px solid transparent; transform: translateY(16px);"></div> <div style=" background-color: #F4F4F4; flex-grow: 0; height: 12px; width: 16px; transform: translateY(-4px);"></div> <div style=" width: 0; height: 0; border-top: 8px solid #F4F4F4; border-left: 8px solid transparent; transform: translateY(-4px) translateX(8px);"></div></div></div> <div style="display: flex; flex-direction: column; flex-grow: 1; justify-content: center; margin-bottom: 24px;"> <div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; margin-bottom: 6px; width: 224px;"></div> <div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; width: 144px;"></div></div></a><p style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; line-height:17px; margin-bottom:0; margin-top:8px; overflow:hidden; padding:8px 0 7px; text-align:center; text-overflow:ellipsis; white-space:nowrap;"><a href="https://www.instagram.com/reel/CgkAQqIlxYr/?utm_source=ig_embed&amp;utm_campaign=loading" style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:normal; line-height:17px; text-decoration:none;" target="_blank">A post shared by CEDETEP - Pindamonhangaba (@cedetepoficial)</a></p></div></blockquote> <script async src="//www.instagram.com/embed.js"></script>
            </div>
                
         </div>

         <hr class="mt-4 border border-light" />

         <div class="pb-3 d-lg-flex justify-content-between align-items-center">

            <p class="text-xs-center mb-1 mb-lg-0 p-0 color-faixa font-poppins fs-11 ls-05">
               {{ $plataforma->nome }} © 2021 - Todos os direitos reservados.
            </p>

            <p class="text-xs-center m-0 p-0 color-faixa font-poppins fs-11 ls-05">
               Desenvolvido por <a href="https://wozcode.com" target="_blank" class="text-danger text-d-none fs-11 weight-600 text-shadow-1">WOZCODE</a>
            </p>

         </div>

      </div>
   </footer>

   <a href="https://api.whatsapp.com/send?phone=55{{preg_replace('/\D/', '', $plataforma->whatsapp)}}&text=Olá, vim através do Site {{ $plataforma->nome }}" target="_blank" class="text-white fs-30">
       <div class="fixed-bottom right-0 me-3 mb-3 z-3">
           <div class="square-60 rounded-circle bg-wpp flex-center box-shadow-wpp">
               <i class="fab fa-whatsapp"></i>
           </div>
       </div>
   </a>

   <div class="modal fade" id="ModalPoloTijuca" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-dialog-scrollable modal-lg modal-dialog-centered border-0">
        <div class="modal-content border-0">
            <div class="modal-header bg-faixa">
              <h3 class="modal-title fs-20 text-white" id="exampleModalLabel">Polo Tijuca</h3>
              <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body px-lg-4">

                      <div class="mb-4">
                          <div class="mb-0 font-poppins text-white d-flex align-items-center">
                              <div>
                               <div class="square-50 bg-f1 bg-white-hover box-info rounded-circle flex-center">
                                 <i class="fa fa-envelope fs-30 color-green"></i>
                               </div>
                              </div>
                            <span class="ms-4 fs-xs-15 overflow-hidden text-dark">faleconosco@geracaodevencedores.com.br</span>
                          </div>
                      </div>

                      <div class="mb-4 width-large">
                          <a href="https://api.whatsapp.com/send?phone=5521985105348&text=Olá, vim através do Site Geração de Vencedores" target="_blank" class="text-d-none hover-d-none font-poppins text-white d-flex align-items-center">
                            <div class="square-50 bg-f1 bg-white-hover box-info rounded-circle flex-center">
                              <i class="fab fa-whatsapp fs-30 color-green"></i>
                            </div>
                            <span class="text-dark text-d-none ms-lg-3 ms-2 ls-05">&nbsp;(21) 98510-5348</span>
                          </a>
                      </div>

                      <div class="mb-4">
                          <div class="text-d-none hover-d-none font-poppins d-flex align-items-center">
                            <div class="square-50 bg-f1 bg-white-hover box-info rounded-circle flex-center">
                              <i class="fa fa-phone fs-25 color-green"></i>
                            </div>
                            <span class="ms-3 ls-05">
                              <a href="tel:+552131773515" class="text-dark text-d-none hover-d-none">
                                (21) 3177-3515 
                              </a>
                              /
                              <a href="tel:+552125703515" class="text-dark text-d-none hover-d-none">
                                (21) 2570-3515
                              </a>
                            </span>
                          </div>
                      </div>

                      <div class="mb-4 width-xl-large">
                        <a class="text-d-none hover-d-none" href="https://www.google.com/maps/place/Curso+Preparat%C3%B3rio+Gera%C3%A7%C3%A3o+de+Vencedores+-+Pr%C3%A9+Militar+e+Vestibular+RJ/@-22.9235116,-43.2303729,15z/data=!4m5!3m4!1s0x0:0x659e312e4318103d!8m2!3d-22.9235116!4d-43.2303729" target="_blank">
                          <div class="d-flex align-items-center">
                              <div>
                               <div class="square-50 bg-f1 bg-white-hover box-info rounded-circle flex-center">
                                 <i class="fas fa-map-marker-alt fs-30 color-green"></i>
                               </div>
                              </div>
                            <address class="py-0 ms-3 my-0 text-dark font-poppins">
                              Rua Conde de Bonfim, 277 sobreloja,Tijuca
                            </address>
                          </div>
                        </a>
                      </div>

            </div>
        </div>
      </div>
   </div>

   <script src="https://kit.fontawesome.com/2f91eaaef4.js" crossorigin="anonymous"></script>  
   <script src="https://cdn.jsdelivr.net/npm/uikit@3.5.9/dist/js/uikit.min.js"></script>
   <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-U1DAWAznBHeqEIlVSCgzq+c9gqGAJn5c/t99JyeKa9xxaYpSvHU5awsuZVVFIhvj" crossorigin="anonymous"></script>
   <script>

      var isMobile = {
          Android: function() {
              return navigator.userAgent.match(/Android/i);
          },
          BlackBerry: function() {
              return navigator.userAgent.match(/BlackBerry/i);
          },
          iOS: function() {
              return navigator.userAgent.match(/iPhone|iPad|iPod/i);
          },
          Opera: function() {
              return navigator.userAgent.match(/Opera Mini/i);
          },
          Windows: function() {
              return navigator.userAgent.match(/IEMobile/i) || navigator.userAgent.match(/WPDesktop/i);
          },
          any: function() {
              return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
          }
      };

      if(isMobile.any() != null)
      {
        const navLinks = document.querySelectorAll('.nav-item');
        const menuToggle = document.getElementById('navbarSupportedContent');
        navLinks.forEach((l) => {
            l.addEventListener('click', () => { new bootstrap.Collapse(menuToggle) })
        })
      }

      window.onscroll = function() {myFunction()};

      var header = document.getElementById("menu");
      var sticky = header.offsetTop;

      function myFunction() {
        if (window.pageYOffset > sticky) {
          header.classList.add("fixed-top");
        } else {
          header.classList.remove("fixed-top");
        }
      }
   </script>

  <div class="box-cookies d-none fixed-bottom bg-white py-3 px-4 box-shadow">
    <div class="d-flex align-items-center justify-content-between px-xl-3">
       <p class="msg-cookies font-poppins mb-0 p-0 color-faixa-verde weight-600">
          Este site usa cookies para garantir que você obtenha a melhor experiência. 
          <a href="#ModalPoliticaPrivacidade" data-bs-toggle="modal" class="color-faixa-verde text-decoration-underline">
            Política de Privacidade
          </a>
       </p>
       <button class="btn-cookies btnInscrever weight-700 box-shadow">Ok!</button>
    </div>
  </div>
  <div class="modal fade" id="ModalPoliticaPrivacidade" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-dialog-scrollable modal-lg modal-dialog-centered border-0">
        <div class="modal-content border-0">
            <div class="modal-header bg-faixa">
              <h3 class="modal-title fs-20 text-white" id="exampleModalLabel">Política de Privacidade</h3>
              <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body px-lg-4">

              <p>
                What is Lorem Ipsum?
                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
              </p>
              <p>
                What is Lorem Ipsum?
                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
              </p>

            </div>
        </div>
      </div>
  </div>
  
  <script>
  (() => {
    if (!localStorage.getItem('accept')) {
      document.querySelector(".box-cookies").classList.remove('d-none');
    }
    
    const acceptCookies = () => {
      document.querySelector(".box-cookies").classList.add('d-none');
      localStorage.setItem('accept', 'S');
    };
    
    const btnCookies = document.querySelector(".btn-cookies");

    btnCookies.addEventListener('click', acceptCookies);
  })();
  </script>

</body>
</html>