@extends('layouts.admin.master')
@section('css')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.14.0-beta2/dist/css/bootstrap-select.min.css">
@endsection
@section('content')

    <div class="mt-5 p-3 rounded bg-white border">

        <p class="mb-0 weight-600">
            Filtros condicionais aplicados
        </p>

        <hr class="mt-2">

        @foreach($filtrosAplicados as $filtro)
            <span class="bg-secondary text-light px-2-5 py-2 rounded-2 me-1 d-inline-block mb-2">
                <span class="weight-600">{{ $filtro['column'] }}</span>: 
                {{ $filtro['value'] }}
            </span>
        @endforeach

        @if(count($filtrosAplicados) == 0)
            <span><i>Nenhum filtro realizado.</i></span>
        @endif

    </div>

    <div class="mt-5 d-flex justify-content-between">

        <div class="align-self-end mb-2">
            <span class="resultados-encontrados">
                {{ count($query) }} resultados de {{ $countQuery }}.
            </span>
        </div>

        <div class="mb-3">
            <form method="POST" action="{{ route('admin.relatorio.export') }}">
            @csrf

                <input type="hidden" name="tipo-perfil-aba" value="{{ $_GET['tipo-perfil-aba'] ?? null }}">
                <input type="hidden" name="campos" value="{{ implode(', ', $camposSelected) }}">
                <input type="hidden" name="queryString" value="{{ $queryString }}">

                <button type="submit" class="btn-add aclNivel2 default-show">Exportar</button>
            </form>
        </div>

    </div>

    <div class="row">
        <div class="col-12">

            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    @php

										function getApelido($select, $array)
										{
										   foreach ($array as $key => $val) {
										       if ($val['select'] === $select)
										       {
										       		$apelido = explode('.', $val['select'])[1];
										       		return $apelido;
										       }
										   }
										   return null;
										}

										function searchForSelect($select, $array)
										{
										   foreach ($array as $key => $val) {
										       if ($val['select'] === $select) {
										           return $val['nome'];
										       }
										   }
										   return null;
										}

                                    @endphp

		                            @foreach($camposSelected as $campo)
		                                <th class="weight-600 font-poppins text-dark fs-13">
		                                    {{ searchForSelect($campo, $allCampos) }}
		                                </th>
		                            @endforeach

                                    <th scope="col">
                                        <span class="float-end">
                                            Ações
                                        </span>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($query as $q)
                                    <tr>
									    @foreach($camposSelected as $campo)

									    	@php
                                                if(\Str::contains($campo, '.'))
                                                {
									    		   $apelido = getApelido($campo, $allCampos);

									    	    } else {

                                                    $apelido = $campo;
                                                }
                                            @endphp
									    	
									    	<td class="{{ $campo }}">
									    		{!! $q->$apelido ?? '<i>Não definido</i>' !!}
									    	</td>

									    @endforeach

                                        <td>
                                            <span class="float-end">

                                                <a href="{{ $route }}/{{$q->id}}" class="btn bg-base text-white">
                                                    <i class="fa fa-eye"></i>
                                                </a>

                                            </span>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            {{ $query->withQueryString()->links() }}

        </div>
    </div>

@endsection
