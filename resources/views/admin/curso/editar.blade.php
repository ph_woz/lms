@extends('layouts.admin.master')

@section('css')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.14.0-beta2/dist/css/bootstrap-select.min.css">
@endsection

@section('content')
    
<div>

    <div class="mb-3 d-flex justify-content-end">
	    <a href="{{ route('admin.curso.info', ['id' => $curso->id ?? null]) }}" class="btn bg-base text-white">
	        <i class="fa fa-eye"></i>
	    </a>
	</div>

    <form method="POST" enctype="multipart/form-data" class="mb-4">
    @csrf

		<section class="mb-4-5">
			
			<a href="#boxInfoPrincipal" data-bs-toggle="collapse" aria-expanded="false" class="hover-d-none">
				<div class="section-title">
					Informações Principais
				</div>
			</a>
			<hr/>
			
			<div class="collapse show" id="boxInfoPrincipal">

		    	<div class="d-lg-flex mb-3">

			    	<div class="w-100">
			    		<label>Nome Comercial</label>
			    		<input type="text" name="nome" class="form-control p-form" placeholder="Nome que será exibido para o público e alunos" required value="{{ $curso->nome }}">
			    	</div>
			    	<div class="w-100 mx-lg-3 my-3 my-lg-0">
			    		<label>Nome de Referência</label>
			    		<input type="text" name="referencia" class="form-control p-form" placeholder="Nome que será exibido somente para Colaboradores" required value="{{ $curso->referencia }}">
			    	</div>
			    	<div class="w-lg-35">
			    		<label>Status</label>
			    		<select name="status" class="select-form">
			    			<option value="0">Ativo</option>
			    			<option value="1" @if($curso->status == 1) selected @endif>Desativado</option>
			    		</select>
			    	</div>

			    </div>

			    <div class="d-lg-flex mb-3">

					<div class="w-100">
						<label>Certificado de Conclusão</label>
						<select name="certificado_id" class="selectpicker" title="Selecione" data-width="100%" data-live-search="true">
							<option value="">Selecione</option>
							@foreach($certificados as $certificado)
								<option value="{{ $certificado->id }}" @if($certificado->id == $curso->certificado_id) selected @endif>
									{{ $certificado->referencia }}
								</option>
							@endforeach
						</select>
					</div>

					<div class="w-100 mx-lg-3 my-3 my-lg-0">
						<label>Foto de Miniatura</label>
						@if($curso->foto_miniatura)
							<a href="{{ $curso->foto_miniatura }}" class="font-nunito weight-600 fs-11 color-blue" target="_blank">
								visualizar anexo
							</a>
						@endif
						<input type="file" name="foto_min" class="file-form">
					</div>

					<div class="w-lg-35">
						<label>Publicar</label>
						<select name="publicar" onchange="showCamposApresentacao(this.value)" class="select-form">
							<option value="N">Não</option>
							<option value="S" @if($curso->publicar == 'S') selected @endif>Sim</option>
						</select>
					</div>

			    </div>

			    <div id="boxCursoApresentacao" @if($curso->publicar == 'N') style="display: none;" @endif>

			    	<div class="d-lg-flex mb-3">

						<div class="w-100 me-lg-3 mt-3 mt-lg-0">
							<label>Slogan</label>
							<input type="text" name="slogan" class="form-control p-form" placeholder="Curta descrição" maxlength="275" value="{{ $curso->slogan }}">
						</div>

						<div class="w-lg-80 me-lg-3 mt-3 mt-lg-0">
							<label>Coordenador</label>
							<select name="coordenador_id" class="selectpicker" title="Selecione" data-width="100%" data-live-search="true">
								<option value="">Selecione</option>
								@foreach($colaboradores as $colaborador)
									<option value="{{ $colaborador->id }}" @if($colaborador->id == $curso->coordenador_id) selected @endif>
										{{ $colaborador->nome }}
									</option>
								@endforeach
							</select>
						</div>

						<div class="w-100 me-lg-3 mt-3 mt-lg-0">
							<label>Vídeo de Apresentação</label>
							<input type="text" name="video" class="form-control p-form" placeholder="URL de vídeo" value="{{ $curso->video }}">
						</div>

						<div class="w-lg-75">
							<label>Exibir Conteúdo do Curso</label>
							<select name="exibir_cc" class="select-form">
								<option value="N">Não</option>
								<option value="S" @if(old('exibir_cc') == 'S') selected @endif>Sim</option>
							</select>
						</div>

					</div>

				    <div class="d-lg-flex mb-3">

						<div class="w-100 me-lg-3 mt-3 mt-lg-0">
							<label>Carga horária</label>
							<input type="text" name="carga_horaria" class="form-control p-form" placeholder="N° de horas" value="{{ $curso->carga_horaria }}" maxlength="275">
						</div>
						<div class="w-100 me-lg-3 mt-3 mt-lg-0">
							<label>Duração</label>
							<input type="text" name="duracao" class="form-control p-form" placeholder="Ex: 5 anos" value="{{ $curso->duracao }}" maxlength="275">
						</div>
						<div class="w-100">
							<label>Tipo de Formação</label>
							<input type="text" name="tipo_formacao" class="form-control p-form" value="{{ $curso->tipo_formacao }}">
						</div>
						<div class="w-100 mx-lg-3 my-3 my-lg-0">
							<label>Nível</label>
							<select name="nivel" id="nivel" class="select-form">
								<option value="">Selecione</option>
								<option value="Graduação">Graduação</option>
								<option value="Pós-Graduação">Pós-Graduação</option>
								<option value="Segunda Graduação">Segunda Graduação</option>
								<option value="Segunda Licenciatura">Segunda Licenciatura</option>
								<option value="Cursos de Extensão">Cursos de Extensão</option>
								<option value="Formação Pedagógica R2">Formação Pedagógica R2</option>
								<option value="Aperfeiçoamento">Aperfeiçoamento</option>
								<option value="Curso Livre">Curso Livre</option>
								<option value="Curso Profissionalizante">Curso Profissionalizante</option>
								<option value="Curso Técnico">Curso Técnico</option>
								<option value="Reforço Escolar">Reforço Escolar</option>
								<option value="Preparatório">Preparatório</option>
								<option value="Norma Regulamentadora">Norma Regulamentadora</option>
							</select>
						</div>
						<div class="w-100 me-lg-3 mb-3 mb-lg-0">
							<label>Foto de Capa</label>
							@if($curso->foto_capa)
								<a href="{{ $curso->foto_capa }}" class="weight-600 fs-11" target="_blank">
									visualizar anexo
								</a>
							@endif
							<input type="file" name="foto" class="file-form">
						</div>
						<div class="w-100">
							<label>Matriz Curricular</label>
							@if($curso->matriz_curricular)
								<a href="{{ $curso->matriz_curricular }}" class="weight-600 fs-11" target="_blank">
									visualizar...
								</a>
							@endif
							<input type="file" name="matriz" class="file-form">
						</div>

					</div>

				</div>

			</div>

		</section>

		<section class="mb-4-5">
			
			<a href="#boxCategorias" data-bs-toggle="collapse" aria-expanded="false" class="hover-d-none">
				<div class="section-title">
					Categorias 
				</div>
			</a>
			<hr/>

			<div class="collapse" id="boxCategorias">
				<select name="categorias[]" class="selectpicker" multiple data-width="100%" data-live-search="true" title="Selecione">
					@foreach($categorias as $categoria)
						<option value="{{ $categoria->id }}" @if(in_array($categoria->id, $categoriasSelected)) selected @endif>{{ $categoria->nome }}</option>
					@endforeach
				</select>
			</div>

		</section>

		<section class="mb-4-5" id="boxCursoSEO" @if($curso->publicar == 'N') style="display: none;" @endif>

			<a href="#boxSeo" data-bs-toggle="collapse" aria-expanded="false" class="hover-d-none">
				<div class="section-title">
					Informações de SEO para resultados de buscas em Navegadores
				</div>
			</a>
			<hr/>
			
			<div class="collapse" id="boxSeo">

				<div class="d-lg-flex mb-3">

					<div class="w-100 me-lg-3 mb-3 mb-lg-0">
						<label>Título</label>
						<input type="text" name="seo_title" class="form-control p-form" placeholder="Title" value="{{ $curso->seo_title }}">
					</div>

					<div class="w-100">
						<label>Palavras-chaves</label>
						<input type="text" name="seo_keywords" class="form-control p-form" placeholder="Keywords" value="{{ $curso->seo_keywords }}">
					</div>

				</div>

				<div class="w-100 mb-3">
					<label>Descrição</label>
					<input type="text" name="seo_description" class="form-control p-form" placeholder="Description" value="{{ $curso->seo_description }}">
				</div>
				
			</div>

		</section>

		<button type="submit" name="sent" value="ok" class="btn-add mt-3">Salvar Alterações</button>	

    </form>

</div>

@endsection

@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.14.0-beta2/dist/js/bootstrap-select.min.js"></script>
<script>

	document.getElementById('nivel').value = '{{ $curso->nivel }}';

	function showCamposApresentacao(value)
	{
		if(value == 'S')
		{
			document.getElementById('boxCursoApresentacao').style.display = 'block';
			document.getElementById('boxCursoSEO').style.display = 'block';
		
		} else {

			document.getElementById('boxCursoApresentacao').style.display = 'none';
			document.getElementById('boxCursoSEO').style.display = 'none';
		}
	}

</script>
@endsection
