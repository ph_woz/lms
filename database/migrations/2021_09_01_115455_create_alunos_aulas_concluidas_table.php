<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAlunosAulasConcluidasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alunos_aulas_concluidas', function (Blueprint $table) {
            $table->id();
            $table->integer('plataforma_id');
            $table->integer('aluno_id');
            $table->integer('curso_id');
            $table->integer('modulo_id')->nullable();
            $table->integer('aula_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alunos_aulas_concluidas');
    }
}
