<?php

namespace App\Http\Livewire\Admin\Questao;

use Livewire\WithPagination;
use Livewire\Component;
use App\Models\Questao;
use App\Models\Avaliacao;
use App\Models\Categoria;
use App\Models\QuestaoCategoria;
use App\Models\AvaliacaoConfigNota;
use App\Models\AvaliacaoQuestao;

class Lista extends Component
{
    use WithPagination;

    protected $paginationTheme = 'bootstrap';

    public $questoesIds = [];
    public $add_questoes_para_avaliacao_id;
    public $search;
    public $modelo;
    public $status = 0;
    public $ordem;
    public $categoriasSelected;

    // Add Questões para Avaliação
    public $valor_nota;
    public $add_questao_id;

    protected $queryString = [
        'add_questoes_para_avaliacao_id',
        'search',
        'modelo',
        'status',
        'ordem',
        'categoriasSelected'
    ];

    public function mount()
    {
        $this->add_questoes_para_avaliacao_id = $_GET['add_questoes_para_avaliacao_id'] ?? null;
    }

    public function render()
    {
        $questoes = Questao::select('id','assunto')->plataforma()->where('status', $this->status);

        if($this->categoriasSelected != null)
        {
            $questoesIds = QuestaoCategoria::plataforma()->whereIn('categoria_id', $this->categoriasSelected ?? array())->get()->pluck('questao_id')->toArray();
            $questoes = $questoes->whereIn('id', $questoesIds);
        }

        if($this->search)
        {
            $questoes->where(function ($query)
            {
                $query
                    ->orWhere('assunto', 'like', '%'.$this->search.'%')
                    ->orWhere('enunciado', 'like', '%'.$this->search.'%');
            });
        }

        if($this->modelo)
        {
            $questoes = $questoes->where('modelo', $this->modelo);
        }


        if($this->add_questoes_para_avaliacao_id)
        {
            $questoesIdsJaAdicionadas = AvaliacaoQuestao::plataforma()->where('avaliacao_id', $this->add_questoes_para_avaliacao_id)->select('questao_id')->get()->pluck('questao_id')->toArray();
            $questoes = $questoes->whereNotIn('id', $questoesIdsJaAdicionadas);

            $avaliacao = Avaliacao::plataforma()->where('id', $this->add_questoes_para_avaliacao_id)->first();
            $notasPossiveis = AvaliacaoConfigNota::plataforma()->where('avaliacao_id', $this->add_questoes_para_avaliacao_id)->select('valor_nota')->get()->pluck('valor_nota')->toArray();
        }

        $countQuestoes = $questoes->count();


        if($this->ordem == 'desc' || $this->ordem == 'asc')
        {
            $questoes = $questoes->orderBy('id', $this->ordem);
        
        } else {
        
            $questoes = $questoes->orderBy('assunto');
        }
        

        $questoes = $questoes->paginate(10);

        if($this->add_questoes_para_avaliacao_id)
        {
            $this->questoesIds = $questoes->pluck('id');
        }

        $categorias = Categoria::select('id','nome')->plataforma()->ativo()->tipo('questao')->orderBy('nome')->get();


        return view('livewire.admin.questao.lista', [
            'questoes'       => $questoes,
            'countQuestoes'  => $countQuestoes,
            'categorias'     => $categorias,
            'avaliacao'      => $avaliacao ?? null,
            'notasPossiveis' => $notasPossiveis ?? array(),
        ]);
    }

    public function setEventAddQuestaoId($questao_id)
    {
        $this->add_questao_id = $questao_id;
    }

    public function addQuestao()
    {
        AvaliacaoQuestao::create([
            'avaliacao_id' => $this->add_questoes_para_avaliacao_id,
            'questao_id' => $this->add_questao_id,
            'valor_nota' => $this->valor_nota,
        ]);

        $this->add_questao_id = null;
        $this->valor_nota     = null;

        session()->flash('success-livewire', 'Questão adicionada na Avaliação com êxito.');
        
        $this->dispatchBrowserEvent('toast');
        $this->dispatchBrowserEvent('dissmiss-modal');

    }

    public function addTodasQuestoes()
    {
        foreach($this->questoesIds as $questao_id)
            AvaliacaoQuestao::create([
                'avaliacao_id' => $this->add_questoes_para_avaliacao_id,
                'questao_id'   => $questao_id,
                'valor_nota'   => $this->valor_nota,
            ]);

        session()->flash('success-livewire', 'Todas as Questões foram adicionadas na Avaliação com êxito.');
        
        $this->dispatchBrowserEvent('toast');
        $this->dispatchBrowserEvent('dissmiss-modal');

    }

}
