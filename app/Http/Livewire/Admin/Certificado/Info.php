<?php

namespace App\Http\Livewire\Admin\Certificado;

use Livewire\Component;
use App\Models\Certificado;
use App\Models\User;
use App\Models\Curso;
use App\Models\Turma;
use App\Models\Trilha;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Http;

class Info extends Component
{
    public $certificado;
    public $password_to_delete;
    public $cadastrante;

    # Configurações para Gerar Certificado
    public $gerar_certificado_aluno_id;
    public $gerar_certificado_turma_id;
    public $gerar_certificado_trilha_id;
    public $gerar_certificado_aluno_email;
    public $gerar_certificado_texto1;
    public $gerar_certificado_texto2;
    public $turmas;
    public $trilhas;

    public function mount($certificado_id)
    {
        $this->certificado = Certificado::plataforma()->find($certificado_id);

        $this->gerar_certificado_texto1 = $this->certificado->texto1;
        $this->gerar_certificado_texto2 = $this->certificado->texto2;
        $this->gerar_certificado_aluno_id = $_GET['gerar_certificado_aluno_id'] ?? null;

        $this->turmas = Turma::plataforma()->ativo()->orderBy('nome')->get();
        $this->trilhas = Trilha::plataforma()->ativo()->orderBy('nome')->get();

        $this->cadastrante = User::plataforma()->where('id', $this->certificado->cadastrante_id)->first();

        if(isset($_GET['turma_id']) && $_GET['turma_id'] != null)
        {
            $this->gerar_certificado_turma_id = $_GET['turma_id'];

            $this->changeEventTurma();
        }

        if(isset($_GET['trilha_id']) && $_GET['trilha_id'] != null)
        {
            $this->gerar_certificado_trilha_id = $_GET['trilha_id'];

            $this->changeEventTrilha();
        }
    }

    public function render()
    {
        if(strlen($this->gerar_certificado_aluno_email) >= 5)
        {
            $alunosGerarCertificado = User::plataforma()->ativo()->aluno()->where('email', 'like', '%'.$this->gerar_certificado_aluno_email.'%')->select('id','nome','email')->limit(5)->get();
        }

        if(isset($this->gerar_certificado_aluno_id))
        {
            $alunoEscolhidoGerarCertificado = User::plataforma()->where('id', $this->gerar_certificado_aluno_id)->first();
        }

        return view('livewire.admin.certificado.info', 
            [
                'alunosGerarCertificado'         => $alunosGerarCertificado ?? array(), 
                'alunoEscolhidoGerarCertificado' => $alunoEscolhidoGerarCertificado ?? null
            ]
        );
    }

    public function changeEventTurma()
    {
        if($this->gerar_certificado_turma_id)
        {
            $this->gerar_certificado_trilha_id = null;
            $this->gerar_certificado_texto1 = $this->certificado->replaceVariaveis($this->certificado->texto1, $this->gerar_certificado_aluno_id, $this->gerar_certificado_turma_id, null);
            $this->gerar_certificado_texto2 = $this->certificado->replaceVariaveis($this->certificado->texto2, $this->gerar_certificado_aluno_id, $this->gerar_certificado_turma_id, null);
        }
    }

    public function changeEventTrilha()
    {
        if($this->gerar_certificado_trilha_id)
        {
            $this->gerar_certificado_turma_id = null;
            $this->gerar_certificado_texto1 = $this->certificado->replaceVariaveis($this->certificado->texto1, $this->gerar_certificado_aluno_id, null, $this->gerar_certificado_trilha_id);
            $this->gerar_certificado_texto2 = $this->certificado->replaceVariaveis($this->certificado->texto2, $this->gerar_certificado_aluno_id, null, $this->gerar_certificado_trilha_id);
        }
    }

    public function redirectGerarCerfificado()
    {
        if($this->gerar_certificado_turma_id == null && $this->gerar_certificado_trilha_id == null)
        {
            session()->flash('danger-livewire', 'Selecione uma Turma ou Trilha.');

            $this->dispatchBrowserEvent('toast');

            return;
        }

        return redirect()->route('admin.certificado.info.gerar', 
            [
                'id'        => $this->certificado->id,
                'user_id'   => $this->gerar_certificado_aluno_id,
                'turma_id'  => $this->gerar_certificado_turma_id,
                'trilha_id' => $this->gerar_certificado_trilha_id,
                'texto1'    => $this->gerar_certificado_texto1,
                'texto2'    => $this->gerar_certificado_texto2,
            ]
        );
    }

    public function delete()
    {
        if(\Auth::attempt(['id' => \Auth::id(), 'password' => $this->password_to_delete, 'status' => 0]))
        {
            $certificado = Certificado::plataforma()->where('id', $this->certificado->id)->first();

            $path = str_replace(\App\Models\Util::getLinkStorage(), '', $certificado->modelo);
            Storage::delete($path);

            $certificado->delete();

            session()->flash('success', 'Certificado deletado com êxito.');
            
            return redirect(route('admin.certificado.lista'));

        } else {

            session()->flash('danger-livewire', 'Senha incorreta.');

            $this->dispatchBrowserEvent('toast');
        }
    }
}
