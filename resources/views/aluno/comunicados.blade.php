@extends('layouts.aluno')
@section('content')
@section('seo_title', 'Suporte')

<div class="container-fluid px-xl-5 mt-5">

	@foreach($comunicados as $comunicado)
	<div class="mb-4 p-4 rounded-10 box-shadow bg-14">
		<div class="d-flex align-items-center">
			<i class="fa fa-user-circle color-light-soft fs-50"></i>
			<div class="ps-3">
				<span class="d-block fs-17 color-light-soft">
					{{ $comunicado->cadastrante ?? 'Usuário Deletado' }}
				</span>
				<span class="d-block fs-12 color-b7">
					<i class="far fa-clock fs-11"></i>
					Publicado {{ \Carbon\Carbon::createFromTimeStamp(strtotime($comunicado->created_at))->diffForHumans() }}
				</span>
			</div>
		</div>
		<h5 class="ps-1 title fs-17 mt-4">
			{{ $comunicado->assunto }}
		</h5>

		<p class="ps-1 color-light-soft">
			{!! nl2br($comunicado->descricao) !!}
		</p>

	</div>
	@endforeach

</div>

<script>
	localStorage.removeItem('hasComunicado');
</script>

@endsection