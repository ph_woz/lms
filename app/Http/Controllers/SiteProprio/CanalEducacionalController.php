<?php

namespace App\Http\Controllers\SiteProprio;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Banner;
use App\Models\Depoimento;
use App\Models\PaginaConteudo;
use App\Models\PlataformaIntegracao;
use HubSpot\Factory;
use HubSpot\Client\Crm\Contacts\ApiException;
use HubSpot\Client\Crm\Contacts\Model\SimplePublicObjectInput;

class CanalEducacionalController extends Controller
{
    public function index()
    {
        $banner = Banner::plataforma()->ativo()->select('link')->orderBy('id','desc')->pluck('link')[0] ?? null;

        $depoimentos = Depoimento::select('foto')->plataforma()->ativo()->orderBy('ordem')->get()->pluck('foto')->toArray();

        $seo = PaginaConteudo::plataforma()->where('url','/')->select('plataforma_id','seo_title','seo_keywords','seo_description')->first();

        if(request('sentContato'))
        {
            if(request('check'))
            {
                return back()->with('danger', 'SPAM Detected!');
            }

            if(strpos(request('nome'), 'Cryto') !== false)
            {
                return back()->with('danger', 'SPAM Detected!');
            }

            \App\Models\Contato::create(request()->all() + [
                'plataforma_id' => $seo->plataforma_id,
                'tipo' => 'L',
            ]);

            $chaveApiHubspot = PlataformaIntegracao::plataforma()->ativo()->where('ref','chave-api-hubspot')->pluck('value')[0] ?? null;
            
            if(isset($chaveApiHubspot))
            {
                $this->sendContatoHubspot($chaveApiHubspot);
            }

            if(request('assunto') == 'Fale Conosco')
            {
                return redirect('/#contato')->with('success', 'Inscrição enviada com êxito.');
            
            } else {

                return redirect('/#pre-matricula')->with('success_prematricula', 'Inscrição enviada com êxito.');
            }
        }

    	return view('sites-proprios.canal-educacional.home', compact('banner','depoimentos','seo') );
    }

    public function getLastName($firstname, $nome)
    {
        $lastname  = explode($firstname, $nome);
        $lastname  = $lastname[1];
        
        if($lastname == '')
        {
            $fullname  = explode(' ', $firstname);
            $firstname = $fullname[0];

            if (array_key_exists(1, $fullname))
                $lastname  = $fullname[1];
            else
                $lastname = ' ';
        }

        return $lastname;
    }

    public function sendContatoHubspot($chaveApiHubspot)
    {
        $nome      = request('nome');
        $firstname = $this->getPrimeiroSegundoNome($nome);
        $lastname  = $this->getLastName($firstname, $nome);

        $client = Factory::createWithAccessToken($chaveApiHubspot);

        $properties = [
            'email'     => request('email'),
            'firstname' => $firstname,
            'lastname'  => $lastname,
            'phone'     => request('celular'),
            'website'   => \Request::getHost()
        ];

        $simplePublicObjectInputForCreate = new SimplePublicObjectInput([
            'properties'   => $properties,
            'associations' => null,
        ]);

        try {

            $apiResponse = $client->crm()->contacts()->basicApi()->create($simplePublicObjectInputForCreate);

        } catch (ApiException $e) {

            echo "Exception when calling basic_api->create: ", $e->getMessage();
        }
    }

    public function getPrimeiroSegundoNome($nome)
    {
        $names = explode(' ', $nome);
        $nome = (isset($names[1])) ? $names[0]. ' ' .$names[1] : $names[0];

        if (array_key_exists(2, $names))
            $names2 = $names[2];
        else
            $names2 = null;

        if( $this->endsWith($nome, 'de') || $this->endsWith($nome, 'dos') || $this->endsWith($nome, 'da') )
            $nome = $nome . ' ' . $names2 ?? null;

        return $nome;
    }

    public function endsWith( $haystack, $needle ) {
        return $needle === "" || (substr($haystack, -strlen($needle)) === $needle);
    }

}