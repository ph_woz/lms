<div>

	<section>

		<div class="d-lg-flex justify-content-end mb-4">

			<a href="#" data-bs-toggle="modal" data-bs-target="#ModalCampoAdd" class="btn btn-primary fs-13 py-2" target="_blank">
				+ Adicionar Campo
			</a>

			<a href="#" data-bs-toggle="modal" data-bs-target="#ModalGerarContrato" class="ms-2 btn btn-primary fs-13 py-2" target="_blank">
				+ Fazer Simulação de Teste
			</a>

		</div>

		<div class="section-title rounded-bottom-0 d-flex justify-content-between align-items-center">
			{{ $contrato->referencia }}
			<div>

				<a href="#" data-bs-toggle="modal" data-bs-target="#ModalMetaDados" class="btn btn-info me-2 fs-14 mb-1 py-1 px-2-3">
					<i class="fa fa-align-left"></i>
				</a>
				<a href="/admin/contrato/editar/{{$contrato->id}}" class="aclNivel2 btn btn-warning text-dark fs-14 mb-1 py-1 px-2">
					<i class="fa fa-edit"></i>
				</a>
				<a href="#" data-bs-toggle="modal" data-bs-target="#ModalDeletarContrato" class="aclNivel3 btn btn-danger mb-1 fs-15 py-1 px-2 ms-2">
					<i class="fa fa-trash"></i>
				</a>
			</div>
		</div>

		<div class="bg-white border p-4 d-lg-flex justify-content-between">

			<div class="mb-2 mb-lg-0">
				<p class="mb-0 weight-600 color-444">PDF</p>
				<a href="{{ $contrato->pdf }}" target="_blank" class="mb-0 weight-600">
					Visualizar
				</a>
			</div>

			@if($unidade)
			<div class="mb-2 mb-lg-0">
				<p class="mb-0 weight-600 color-444">Unidade</p>
				<a href="{{ route('admin.unidade.info', ['id' => $unidade->id]) }}" class="color-blue-info-link">
					{{ $unidade->nome }}
				</a>
			</div>
			@endif

		</div>

		<div class="bg-white border p-4">

			@if(count($campos) > 0)
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">Campos</th>
                            <th scope="col">Posição (X)</th>
                            <th scope="col">Posição (Y)</th>
                            <th scope="col">Tamanho da Fonte</th>
                            <th scope="col">Cor da Fonte</th>
                            <th scope="col">
                                <span class="float-end">
                                    Ações
                                </span>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($campos as $campo)
                            <tr>
                                <td>{{ $campo->campo }}</td>
                                <td>{!! $campo->px ?? '<i>Não definido</i>' !!}</td>
                                <td>{!! $campo->py ?? '<i>Não definido</i>' !!}</td>
                                <td>{!! $campo->fontSize ?? '<i>Não definido</i>' !!}</td>
                                <td>{!! $campo->color ?? '<i>Não definido</i>' !!}</td>
                                <td>
                                    <span class="float-end">

                                        <a href="#ModalCampoEditar" data-bs-toggle="modal" wire:click="setCamposParaEditar({{$campo->id}})" class="btn btn-warning text-dark">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                        <a href="#ModalDeletarCampo" wire:click="setDeleteCampoId({{$campo->id}})" onclick="document.getElementById('remover_campo').innerHTML='{{ $campo->campo }}';" data-bs-toggle="modal" class="btn btn-danger">
                                            <i class="fa fa-trash"></i>
                                        </a>

                                    </span>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
			@else
				<p class="mb-0 weight-600">Ainda não há Campos cadastrados.</p>
			@endif

		</div>

	</section>

	<div class="modal fade" id="ModalMetaDados" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	  	<div class="modal-dialog modal-dialog-centered" role="document">
	    	<div class="modal-content">
	      		<div class="modal-header">
	        		<h3 class="modal-title">Metadados</h3>
	    			<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
	      		</div>
	      		<div class="modal-body">

					<ul class="list-group list-group-flush">
	  					<li class="list-group-item">
							<span class="mb-0 weight-600 color-444">Cadastrante:</span>
							@if($cadastrante)
								<a href="/admin/colaborador/info/{{$cadastrante->id}}" class="color-blue-info-link">
									{{ $cadastrante->nome }}
								</a>
							@else
								Não definido
							@endif
	  					</li>   
	  					<li class="list-group-item">
							<span class="mb-0 weight-600 color-444">Status:</span>
							@if($contrato->status == 0)
								<span class="text-success weight-700">Ativo</span>
							@else
								<span class="text-danger weight-700">Desativado</span>
							@endif
	  					</li>
	  					<li class="list-group-item">
							<span class="mb-0 weight-600 color-444">Data de Cadastro:</span>
							@if($contrato->created_at)
								<span class="mb-0">{{ $contrato->created_at->format('d/m/Y H:i') ?? null }} ({{$contrato->created_at->diffForHumans()}}) </span>
	  						@else
	  							Não definido
	  						@endif
	  					</li>   
	  				</ul>

	      		</div>
	    	</div>
	  	</div>
	</div>

	<div wire:ignore.self class="modal fade" id="ModalGerarContrato" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  		<div class="modal-dialog modal-lg modal-dialog-centered">
    		<div class="modal-content">
      			<div class="modal-header">
        			<h3 class="modal-title" id="exampleModalLabel">Simular Contrato</h3>
        			<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      			</div>
      			<div class="modal-body p-lg-4">

      				<div>
      					<label>Email do Aluno</label>
      					<input type="text" wire:model="gerar_contrato_aluno_email" class="form-control p-form" placeholder="Digite o Email do Aluno" required>
      				</div>

      				@if(count($alunosGerarContrato) > 0)
                    <div class="table-responsive mt-3">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col">Nome</th>
                                    <th scope="col">Email</th>
                                    <th scope="col">
                                        <span class="float-end me-2">
                                            Ações
                                        </span>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($alunosGerarContrato as $aluno)
                                    <tr>
                                        <td>
                                        	<a href="/admin/aluno/info/{{$aluno->id}}" target="_blank" class="color-blue-info-link"> 
                                        		{{ $aluno->nome }}
                                        	</a>
                                        </td>
                                        <td>{{ $aluno->email }}</td>
                                        <td>
                                            <span class="float-end">
												<a href="?gerar_contrato_aluno_id={{ $aluno->id }}" class="btn btn-primary weight-600 py-2">Continuar</a>
                                            </span>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    @endif

      			</div>
    		</div>
  		</div>
	</div>

	@if($alunoEscolhidoGerarContrato)
	<form action="{{ route('admin.contrato.info.gerar-simulacao', ['id' => $contrato->id]) }}" target="_blank">
		<input type="hidden" name="simulacao" value="S">
		<div wire:ignore.self class="modal fade" id="ModalAlunoEscolhidoGerarContrato" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  		<div class="modal-dialog modal-dialog-centered">
	    		<div class="modal-content">
	      			<div class="modal-header">
	        			<h3 class="modal-title" id="exampleModalLabel">
	        				Gerar Simulação de Contrato<br>
	        				<span class="fs-15 d-block mt-3">
	        					<a href="/admin/aluno/info/{{ $alunoEscolhidoGerarContrato->id }}" class="color-blue-info-link weight-600" target="_blank"> 
	        						{{ $alunoEscolhidoGerarContrato->nome }}
	        					</a>
	        				</span>
	        				<span class="fs-15">
	        					{{ $alunoEscolhidoGerarContrato->email }}
	        				</span>
	        			</h3>
	        			<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
	      			</div>
	      			<div class="modal-body">

						@if(count($campos) > 0)

						@php

							// PAI
							$resp_pai = \App\Models\ResponsavelAluno::select('responsaveis.*','responsaveis_alunos.grau_parentesco')
								->join('responsaveis','responsaveis_alunos.responsavel_id','responsaveis.id')
								->where('responsaveis_alunos.plataforma_id', session('plataforma_id'))
								->where('responsaveis_alunos.aluno_id', $alunoEscolhidoGerarContrato->id)
								->where('responsaveis_alunos.grau_parentesco','Pai')
								->first();

							if(isset($resp_pai))
							{
				                $resp_pai_enderecoCompleto = $resp_pai->rua . ', ' . $resp_pai->numero  . ' - ' . $resp_pai->bairro . ', ' . $resp_pai->cidade . ' - ' . $resp_pai->estado . ', ' . $resp_pai->cep . '. ' . $resp_pai->complemento;
				                $resp_pai_enderecoRuaENumero = $resp_pai->rua . ', ' . $resp_pai->numero;
							
				                $nome_resp_pai = $resp_pai->nome;
							}


							// MÃE
							$resp_mae = \App\Models\ResponsavelAluno::select('responsaveis.*','responsaveis_alunos.grau_parentesco')
								->join('responsaveis','responsaveis_alunos.responsavel_id','responsaveis.id')
								->where('responsaveis_alunos.plataforma_id', session('plataforma_id'))
								->where('responsaveis_alunos.aluno_id', $alunoEscolhidoGerarContrato->id)
								->where('responsaveis_alunos.grau_parentesco','Mãe')
								->first();

							if(isset($resp_mae))
							{
				                $resp_mae_enderecoCompleto = $resp_mae->rua . ', ' . $resp_mae->numero  . ' - ' . $resp_mae->bairro . ', ' . $resp_mae->cidade . ' - ' . $resp_mae->estado . ', ' . $resp_mae->cep . '. ' . $resp_mae->complemento;
				                $resp_mae_enderecoRuaENumero = $resp_mae->rua . ', ' . $resp_mae->numero;
							
				                $nome_resp_mae = $resp_mae->nome;
							}


							$unidade_nome = \App\Models\Unidade::select('nome')->where('id', $alunoEscolhidoGerarContrato->unidade_id)->value('nome');

					        $camposVariables = [
								'Nome'               => $alunoEscolhidoGerarContrato->nome,
								'Email'              => $alunoEscolhidoGerarContrato->email,
								'CPF'                => $alunoEscolhidoGerarContrato->cpf,
								'Matrícula'          => $alunoEscolhidoGerarContrato->id,
								'RG'                 => $alunoEscolhidoGerarContrato->rg,
								'Gênero'             => $alunoEscolhidoGerarContrato->genero,
								'Profissão'          => $alunoEscolhidoGerarContrato->profissao,
								'Observações'        => $alunoEscolhidoGerarContrato->obs,
								'Celular'            => $alunoEscolhidoGerarContrato->celular,
								'Telefone'           => $alunoEscolhidoGerarContrato->telefone,
								'Naturalidade'       => $alunoEscolhidoGerarContrato->naturalidade,
								'Data de Nascimento' => \App\Models\Util::replaceDatePt($alunoEscolhidoGerarContrato->data_nascimento),
								'Data de Cadastro'   => $alunoEscolhidoGerarContrato->created_at,
								'Polo/Unidade'       => $unidade_nome ?? null,
								'CEP'                => $endereco['cep'] ?? null,
								'Rua'                => $endereco['rua'] ?? null,
								'Número'             => $endereco['numero'] ?? null,
								'Bairro'             => $endereco['bairro'] ?? null,
								'Cidade'             => $endereco['cidade'] ?? null,
								'Estado'             => $endereco['estado'] ?? null,
								'Complemento'        => $endereco['complemento'] ?? null,
								'Endereço'           => $enderecoCompleto,
								'Rua e Número'       => $enderecoRuaENumero,
								'Dia numeral atual'  => date('d'),
								'Mês numeral atual'  => date('m'),
								'Ano numeral atual'  => date('Y'),
								'Cadastrante'        => \Auth::user()->nome ?? null,
								'Nome do Pai'        => $nome_resp_pai ?? null,
								'Nome da Mãe'        => $nome_resp_mae ?? null,
					        ];

					        if($alunoEscolhidoGerarContrato->data_nascimento != null)
					        {
								$idade = \App\Models\Util::getIdade($alunoEscolhidoGerarContrato->data_nascimento);
					        
					        } else {

					        	 $idade = 0;
					        }

							if($idade < 18)
							{
						        $camposResp = [
									'Naturalidade do Responsável'       => $resp_mae->naturalidade ?? $resp_pai->naturalidade ?? null,
									'Cidade do Responsável'             => $resp_mae->cidade ?? $resp_pai->cidade ?? null,
									'CEP do Responsável'                => $resp_mae->cep ?? $resp_pai->cep ?? null,
									'Bairro do Responsável'             => $resp_mae->bairro ?? $resp_pai->bairro ?? null,
									'Endereço do Responsável'           => $resp_mae_enderecoCompleto ?? $resp_pai_enderecoCompleto ?? null,
									'Rua e Número do Responsável'       => $resp_mae_enderecoRuaENumero ?? $resp_pai_enderecoRuaENumero ?? null,
									'Data de Nascimento do Responsável' => \App\Models\Util::replaceDatePt($resp_mae->data_nascimento ?? null) ?? null,
									'RG do Responsável'                 => $resp_mae->rg ?? $resp_pai->rg ?? null,
									'CPF do Responsável'                => $resp_mae->cpf ?? $resp_pai->cpf ?? null,
						        ];


							} else {

						        $camposResp = [
									'Naturalidade do Responsável'       => $alunoEscolhidoGerarContrato->naturalidade,
									'Cidade do Responsável'             => $endereco['cidade'] ?? null,
									'CEP do Responsável'                => $endereco['cep'] ?? null,
									'Bairro do Responsável'             => $endereco['bairro'] ?? null,
									'Endereço do Responsável'           => $enderecoCompleto ?? null,
									'Rua e Número do Responsável'       => $enderecoRuaENumero,
									'Data de Nascimento do Responsável' => \App\Models\Util::replaceDatePt($alunoEscolhidoGerarContrato->data_nascimento),
									'RG do Responsável'                 => $alunoEscolhidoGerarContrato->rg,
									'CPF do Responsável'                => $alunoEscolhidoGerarContrato->cpf,
						        ];
							}


							$camposVariables = array_merge($camposVariables, $camposResp);

						@endphp

						<div>
							<p class="mb-0">Campos de Auto Preenchimento</p>
							<hr class="mt-1 mb-2">
							@foreach($campos as $campo)
								@php
									$column = $camposVariables[$campo->campo] ?? null;
								@endphp
								<div class="d-flex mb-2">
									<input type="hidden" name="camposIds[]" class="w-50 form-control p-form me-2" placeholder="CAMPO ID" value="{{ $campo->id }}" readonly>
									<input type="text" class="w-50 form-control p-form me-2" placeholder="Campo" value="{{ $campo->campo }}" disabled>
									<input type="text" name="values[]" class="form-control p-form @if(is_null($column)) border-danger @endif" placeholder="Preencha" value="{{ $column }}" required>
								</div>
							@endforeach
						</div>
						@endif

	      			</div>
	      			<div class="modal-footer">
		        		<button type="submit" class="btn btn-primary weight-600 py-2">Gerar agora!</button>
	      			</div>
	    		</div>
	  		</div>
		</div>
	</form>
	@endif

	<form wire:submit.prevent="addCampo" method="POST">
		<div wire:ignore class="modal fade" id="ModalCampoAdd" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  		<div class="modal-dialog modal-dialog-centered">
	    		<div class="modal-content">
	      			<div class="modal-header">
	        			<h3 class="modal-title" id="exampleModalLabel">+ Adicionar Campo</h3>
	        			<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
	      			</div>
	      			<div class="modal-body">

	      				<div class="d-lg-flex mb-3">

		      				<div class="w-100 me-lg-3 mb-3 mb-lg-0">
		      					<label>Campo</label>
								<input type="text" wire:model="campo" list="lista-campos" class="form-control p-form" placeholder="Campo" required>
									
									<datalist id="lista-campos">

								    <optgroup label="Dados Cadastrais de Alunos" id="optGroupCadastroAluno">
								        <option value="Nome">Nome</option>
								        <option value="Email">Email</option>
								        <option value="CPF">CPF</option>
								        <option value="Matrícula">Matrícula</option>
								        <option value="RG">RG</option>
								        <option value="Gênero">Gênero</option>
								        <option value="Profissão">Profissão</option>
								        <option value="Observações">Observações</option>
								        <option value="Celular">Celular</option>
								        <option value="Telefone">Telefone</option>
								        <option value="Data de Nascimento">Data de Nascimento</option>
								        <option value="Data de Cadastro">Data de Cadastro</option>
								    </optgroup>

								    <optgroup label="Dados de Endereço de Alunos" id="optGroupEnderecoAluno">
								        <option value="CEP">CEP</option>
								        <option value="Rua">Rua</option>
								        <option value="Número">Número</option>
								        <option value="Bairro">Bairro</option>
								        <option value="Cidade">Cidade</option>
								        <option value="Estado">Estado</option>
								        <option value="Complemento">Complemento</option>
								        <option value="Ponto de Referência">Ponto de Referência</option>
								        <option value="Endereço">Endereço</option>
								        <option value="Rua e Número">Rua e Número</option>
								    </optgroup>

								    <optgroup label="Dados de Trilhas" id="optGroupTrilha">
								        <option value="Trilha">Trilha</option>
								        <option value="Data de Matrícula de Trilha">Data de Matrícula de Trilha</option>
								        <option value="Data de Conclusão de Trilha">Data de Conclusão de Trilha</option>
								    </optgroup>

								    <optgroup label="Dados de Cursos (Para liberar estes campos, é necessário escolher um Curso no filtro)" id="optGroupCurso">
								        <option value="Curso">Curso</option>
								        <option value="Data de Matrícula de Curso">Data de Matrícula de Curso</option>
								        <option value="Data de Conclusão de Curso">Data de Conclusão de Curso</option>
								    </optgroup>

								    <optgroup label="Dados de Turmas (Para liberar estes campos, é necessário escolher uma Turma no filtro)" id="optGroupTurma">
								        <option value="Turma">Turma</option>
								        <option value="Data de Matrícula de Turma">Data de Matrícula de Turma</option>
								        <option value="Data de Conclusão de Turma">Data de Conclusão de Turma</option>
								    </optgroup>

								    <optgroup label="Dados de Responsável" id="optGroupResponsavel">
								        <option value="Nome do Pai">Nome do Pai</option>
								        <option value="Nome da Mãe">Nome da Mãe</option>
								        <option value="Nome do Responsável">Nome do Responsável</option>
								        <option value="CPF do Responsável">CPF do Responsável</option>
								        <option value="RG do Responsável">RG do Responsável</option>
								        <option value="Celular do Responsável">Celular do Responsável</option>
								        <option value="Email do Responsável">Email do Responsável</option>
								        <option value="Data de Nascimento do Responsável">Data de Nascimento do Responsável</option>
								        <option value="Profissão do Responsável">Profissão do Responsável</option>
								        <option value="Naturalidade do Responsável">Naturalidade do Responsável</option>
								        <option value="CEP do Responsável">CEP do Responsável</option>
								        <option value="Rua do Responsável">Rua do Responsável</option>
								        <option value="Número do Responsável">Número do Responsável</option>
								        <option value="Bairro do Responsável">Bairro do Responsável</option>
								        <option value="Cidade do Responsável">Cidade do Responsável</option>
								        <option value="Estado do Responsável">Estado do Responsável</option>
								        <option value="Complemento do Responsável">Complemento do Responsável</option>
								        <option value="Ponto de Referência do Responsável">Ponto de Referência do Responsável</option>
								        <option value="Endereço do Responsável">Endereço do Responsável</option>
								        <option value="Rua e Número do Responsável">Rua e Número do Responsável</option>
								    </optgroup>

								    <optgroup label="Dados de Controle Financeiro" id="optGroupFinanceiro">
								        <option value="Valor">Valor</option>
								        <option value="Parcelas">Parcelas</option>
								        <option value="Produto">Produto</option>
								        <option value="Cadastrante do Financeiro">Cadastrante do Financeiro</option>
								        <option value="Vendedor">Vendedor</option>
								        <option value="Forma de Pagamento">Forma de Pagamento</option>
								        <option value="Link de Pagamento">Link de Pagamento</option>
								        <option value="Data de Vencimento">Data de Vencimento</option>
								    </optgroup>

								    <optgroup label="Dados de Unidades" id="optGroupAlunoUnidade">
								        <option value="Unidade">Unidade</option>
								    </optgroup>

								    <optgroup label="Datas">
								        <option value="Dia numeral atual">Dia numeral atual</option>
								        <option value="Mês numeral atual">Mês numeral atual</option>
								        <option value="Ano numeral atual">Ano numeral atual</option>
								    </optgroup>

								</datalist>

		      				</div>

		      				<div class="w-100">
		      					<label>N° da Página</label>
		      					<input type="text" wire:model="n_pagina" class="number form-control p-form" placeholder="N° da Página a ser exibido">
		      				</div>

		      			</div>

	      				<div class="d-lg-flex mb-3">

		      				<div class="w-100 me-lg-3 mb-3 mb-lg-0">
		      					<label>Posição X</label>
		      					<input type="text" wire:model="px" class="number form-control p-form" placeholder="Defina a Posição Horizontal">
		      				</div>

		      				<div class="w-100">
		      					<label>Posição Y</label>
		      					<input type="text" wire:model="py" class="number form-control p-form" placeholder="Defina a Posição Vertical">
		      				</div>

		      			</div>

		      			<div class="d-lg-flex mb-3">

		      				<div class="w-100 me-lg-3 mb-3 mb-lg-0">
		      					<label>Tamanho da Fonte</label>
		      					<input type="text" wire:model="fontSize" class="number form-control p-form" placeholder="Tamanho da Fonte">
		      				</div>

		      				<div class="w-100">
		      					<label>Cor da Fonte</label>
		      					<input type="text" wire:model="color" class="form-control p-form" placeholder="Em RGB, ex: (255,255,255)">
		      				</div>

		      			</div>

	      			</div>
	      			<div class="modal-footer">
		        		<button type="submit" class="btn btn-primary weight-600 py-2">Confirmar</button>
	      			</div>
	    		</div>
	  		</div>
		</div>
	</form>

	<form wire:submit.prevent="editarCampo" method="POST">
		<div wire:ignore.self class="modal fade" id="ModalCampoEditar" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  		<div class="modal-dialog modal-dialog-centered">
	    		<div class="modal-content">
	      			<div class="modal-header">
	        			<h3 class="modal-title" id="exampleModalLabel">+ Editar Campo</h3>
	        			<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
	      			</div>
	      			<div class="modal-body">

	      				<div class="d-lg-flex mb-3">

		      				<div class="w-100  me-lg-3 mb-3 mb-lg-0">
		      					<label>Campo</label>
								<input type="text" wire:model="campo" list="lista-campos" class="form-control p-form" placeholder="Campo" required>
									
									<datalist id="lista-campos">

								    <optgroup label="Dados Cadastrais de Alunos" id="optGroupCadastroAluno">
								        <option value="Nome">Nome</option>
								        <option value="Email">Email</option>
								        <option value="CPF">CPF</option>
								        <option value="Matrícula">Matrícula</option>
								        <option value="RG">RG</option>
								        <option value="Gênero">Gênero</option>
								        <option value="Profissão">Profissão</option>
								        <option value="Observações">Observações</option>
								        <option value="Celular">Celular</option>
								        <option value="Telefone">Telefone</option>
								        <option value="Data de Nascimento">Data de Nascimento</option>
								        <option value="Data de Cadastro">Data de Cadastro</option>
								    </optgroup>

								    <optgroup label="Dados de Endereço de Alunos" id="optGroupEnderecoAluno">
								        <option value="CEP">CEP</option>
								        <option value="Rua">Rua</option>
								        <option value="Número">Número</option>
								        <option value="Bairro">Bairro</option>
								        <option value="Cidade">Cidade</option>
								        <option value="Estado">Estado</option>
								        <option value="Complemento">Complemento</option>
								        <option value="Ponto de Referência">Ponto de Referência</option>
								        <option value="Endereço">Endereço</option>
								        <option value="Rua e Número">Rua e Número</option>
								    </optgroup>

								    <optgroup label="Dados de Trilhas" id="optGroupTrilha">
								        <option value="Trilha">Trilha</option>
								        <option value="Data de Matrícula de Trilha">Data de Matrícula de Trilha</option>
								        <option value="Data de Conclusão de Trilha">Data de Conclusão de Trilha</option>
								    </optgroup>

								    <optgroup label="Dados de Cursos (Para liberar estes campos, é necessário escolher um Curso no filtro)" id="optGroupCurso">
								        <option value="Curso">Curso</option>
								        <option value="Data de Matrícula de Curso">Data de Matrícula de Curso</option>
								        <option value="Data de Conclusão de Curso">Data de Conclusão de Curso</option>
								    </optgroup>

								    <optgroup label="Dados de Turmas (Para liberar estes campos, é necessário escolher uma Turma no filtro)" id="optGroupTurma">
								        <option value="Turma">Turma</option>
								        <option value="Data de Matrícula de Turma">Data de Matrícula de Turma</option>
								        <option value="Data de Conclusão de Turma">Data de Conclusão de Turma</option>
								    </optgroup>

								    <optgroup label="Dados de Responsável" id="optGroupResponsavel">
								        <option value="Nome do Responsável">Nome do Responsável</option>
								        <option value="CPF do Responsável">CPF do Responsável</option>
								        <option value="RG do Responsável">RG do Responsável</option>
								        <option value="Celular do Responsável">Celular do Responsável</option>
								        <option value="Email do Responsável">Email do Responsável</option>
								        <option value="Data de Nascimento do Responsável">Data de Nascimento do Responsável</option>
								        <option value="Profissão do Responsável">Profissão do Responsável</option>
								        <option value="CEP do Responsável">CEP do Responsável</option>
								        <option value="Rua do Responsável">Rua do Responsável</option>
								        <option value="Número do Responsável">Número do Responsável</option>
								        <option value="Bairro do Responsável">Bairro do Responsável</option>
								        <option value="Cidade do Responsável">Cidade do Responsável</option>
								        <option value="Estado do Responsável">Estado do Responsável</option>
								        <option value="Complemento do Responsável">Complemento do Responsável</option>
								        <option value="Ponto de Referência do Responsável">Ponto de Referência do Responsável</option>
								    </optgroup>

								    <optgroup label="Dados de Controle Financeiro" id="optGroupFinanceiro">
								        <option value="Valor">Valor</option>
								        <option value="Parcelas">Parcelas</option>
								        <option value="Produto">Produto</option>
								        <option value="Cadastrante do Financeiro">Cadastrante do Financeiro</option>
								        <option value="Vendedor">Vendedor</option>
								        <option value="Forma de Pagamento">Forma de Pagamento</option>
								        <option value="Link de Pagamento">Link de Pagamento</option>
								        <option value="Data de Vencimento">Data de Vencimento</option>
								    </optgroup>

								    <optgroup label="Dados de Unidades" id="optGroupAlunoUnidade">
								        <option value="Unidade">Unidade</option>
								    </optgroup>

								    <optgroup label="Datas">
								        <option value="Dia numeral atual">Dia numeral atual</option>
								        <option value="Mês numeral atual">Mês numeral atual</option>
								        <option value="Ano numeral atual">Ano numeral atual</option>
								    </optgroup>

								</datalist>

		      				</div>

		      				<div class="w-100">
		      					<label>N° da Página</label>
		      					<input type="text" wire:model="n_pagina" class="number form-control p-form" placeholder="N° da Página a ser exibido">
		      				</div>

		      			</div>

	      				<div class="d-lg-flex mb-3">

		      				<div class="w-100 me-lg-3 mb-3 mb-lg-0">
		      					<label>Posição X</label>
		      					<input type="text" wire:model="px" class="number form-control p-form" placeholder="Defina a Posição Horizontal">
		      				</div>

		      				<div class="w-100">
		      					<label>Posição Y</label>
		      					<input type="text" wire:model="py" class="number form-control p-form" placeholder="Defina a Posição Vertical">
		      				</div>

		      			</div>

		      			<div class="d-lg-flex mb-3">

		      				<div class="w-100 me-lg-3 mb-3 mb-lg-0">
		      					<label>Tamanho da Fonte</label>
		      					<input type="text" wire:model="fontSize" class="number form-control p-form" placeholder="Tamanho da Fonte">
		      				</div>

		      				<div class="w-100">
		      					<label>Cor da Fonte</label>
		      					<input type="text" wire:model="color" class="form-control p-form" placeholder="Em RGB, ex: (255,255,255)">
		      				</div>

		      			</div>

	      			</div>
	      			<div class="modal-footer">
		        		<button type="submit" class="btn btn-primary weight-600 py-2">Confirmar</button>
	      			</div>
	    		</div>
	  		</div>
		</div>
	</form>

	<form wire:submit.prevent="delete" method="POST">
		<div wire:ignore class="modal fade" id="ModalDeletarContrato" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  		<div class="modal-dialog modal-dialog-centered">
	    		<div class="modal-content">
	      			<div class="modal-header">
	        			<h3 class="modal-title" id="exampleModalLabel">Deletar Contrato</h3>
	        			<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
	      			</div>
	      			<div class="modal-body text-center py-5">
	        			<p class="m-0 fs-16">
	        				Você tem certeza que deseja deletar este contrato?
	        				<br>
	        				Todas as informações serão perdidas definitivamente.
	        			</p>
	      			</div>
	      			<div class="modal-footer">
	      				<div class="d-flex w-100">
		      				<input type="password" wire:model="password_to_delete" class="w-100 form-control p-form me-3" placeholder="Digite sua senha" required>
		        			<button type="submit" class="btn btn-danger weight-600">Confirmar</button>
	    				</div>
	      			</div>
	    		</div>
	  		</div>
		</div>
	</form>

	<form wire:submit.prevent="deleteCampo" method="POST">
		<div wire:ignore class="modal fade" id="ModalDeletarCampo" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  		<div class="modal-dialog modal-dialog-centered">
	    		<div class="modal-content">
	      			<div class="modal-header">
	        			<h3 class="modal-title" id="exampleModalLabel">Deletar Campo</h3>
	        			<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
	      			</div>
	      			<div class="modal-body text-center py-5">

	        			<p id="remover_campo" class="m-0 fs-15 weight-600">
	        				
	        			</p>

	        			<hr/>

	        			<p class="m-0 fs-16">
	        				Você tem certeza que deseja deletar este Campo?
	        			</p>
	      			</div>
	      			<div class="modal-footer">
		        		<button type="submit" class="btn btn-danger weight-600 py-2">Confirmar</button>
	      			</div>
	    		</div>
	  		</div>
		</div>
	</form>

	@include('livewire.util.toast')

</div>