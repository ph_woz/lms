<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFluxoCaixaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fluxo_caixa', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('plataforma_id');
            $table->unsignedBigInteger('registro_id')->nullable(); 
            $table->unsignedBigInteger('cliente_id')->nullable();
            $table->unsignedBigInteger('cadastrante_id')->nullable();
            $table->unsignedBigInteger('unidade_id')->nullable();
            $table->unsignedBigInteger('vendedor_id')->nullable(); 
            $table->unsignedBigInteger('produto_id')->nullable();
            $table->string('referencia')->nullable();
            $table->string('cadastrante_nome')->nullable();
            $table->string('vendedor_nome')->nullable();
            $table->string('cliente_nome')->nullable();
            $table->string('produto_nome')->nullable();
            $table->string('unidade_nome')->nullable();
            $table->string('tipo_transacao', 1); // entrada x saida
            $table->string('tipo_produto');
            $table->string('valor_parcela')->nullable();
            $table->string('valor_total')->nullable();
            $table->string('forma_pagamento')->nullable();
            $table->text('link')->nullable();
            $table->string('n_parcela', 3)->nullable();
            $table->string('n_parcelas', 3)->nullable();
            $table->integer('status')->default(0);
            $table->date('data_vencimento')->nullable();
            $table->date('data_recebimento')->nullable();
            $table->date('data_estorno')->nullable();
            $table->text('motivo_estorno')->nullable();
            $table->text('obs')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fluxo_caixa');
    }
}
