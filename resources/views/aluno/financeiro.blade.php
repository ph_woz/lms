@extends('layouts.aluno')
@section('css')

@endsection
@section('content')

@section('seo_title', 'Financeiro')
<div class="container-fluid px-xl-5 mt-5">

	<div class="mb-4 p-4 rounded-10 box-shadow bg-14">

		<div class="text-center mb-5 mt-3">
			<a href="?listar=todas" class="me-2 btn-suporte-enviar py-2-3 px-lg-5 @if($listar == 'todas') btn-active @endif">
				Todas
			</a>
			<a href="?listar=abertas" class="ms-2 btn-suporte-enviar py-2-3 px-lg-5 @if($listar == 'abertas') btn-active @endif">
				Abertas
			</a>
		</div>

		@if(count($entradas) == 0)
		<p class="text-center mb-0 py-3 color-light fs-17">Não há nenhum pagamento lançado.</p>
		@else
	    <div class="table-responsive">
	        <table class="table">
	          <thead>
	            <tr>
	              <th scope="col" class="color-light-soft">Produto</th>
	              <th scope="col" class="color-light-soft">Valor</th>
	              <th scope="col" class="color-light-soft">Data de Vencimento</th>
	              <th scope="col" class="color-light-soft">
	                <span class="float-end">
	                    Ações
	                </span>
	              </th>
	            </tr>
	          </thead>
	          <tbody>
	            @foreach($entradas as $entrada)
	            <tr>
	                <td class="color-light-soft">
	                	{{ $entrada->produto_nome }}
	                </td>
	                <td class="color-light-soft">
	                    @if($entrada->n_parcelas != $entrada->n_parcela || $entrada->registro_id != null)
	                        {{ $entrada->n_parcela }}/{{$entrada->n_parcelas}} de
	                    @endif
	                    R$ {{ $entrada->valor_parcela }}
	                </td>
	                <td class="color-light-soft">
	                    {{ date('d/m/Y', strtotime($entrada->data_vencimento)) }}
	                </td>
	                <td>
	                    <span class="float-end">
		                    @switch($entrada->status)
		                        @case(0)
		                            @if(date('Y-m-d') > $entrada->data_vencimento)
		                                <a href="{{ $entrada->link }}" target="_blank" class="d-block text-center py-2 px-3 text-light rounded bg-danger hover-d-none">
		                                    A pagar em atraso
		                                </a>
		                            @endif
		                            @if(date('Y-m-d') <= $entrada->data_vencimento)
		                                <a href="{{ $entrada->link }}" target="_blank" class="d-block text-center py-2 px-3 text-light rounded bg-info text-shadow-2 hover-d-none">
		                                    A pagar
		                                </a>
		                            @endif
		                        @break
		                        @case(1)
		                            <span class="d-block text-center py-2 px-3 text-light rounded bg-success text-shadow-2" title="Pago em {{ \App\Models\Util::replaceDatePt($entrada->data_recebimento) }}">
		                                Já foi Pago
		                            </span>
		                        @break
		                        @case(2)
		                            <span class="d-block text-center py-2 px-3 text-light rounded bg-inadimplente" title="Estornado em {{ \App\Models\Util::replaceDatePt($entrada->data_estorno) }}">
		                                Estornado
		                            </span>
		                        @break
		                    @endswitch
		                </span>
	                </td>
	            </tr>
	            @endforeach
	          </tbody>
	        </table>
	    </div>
		@endif

	</div>

</div>
@endsection