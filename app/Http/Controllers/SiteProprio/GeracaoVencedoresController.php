<?php

namespace App\Http\Controllers\SiteProprio;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Contato;
use App\Models\Plataforma;
use App\Models\PaginaConteudo;
use App\Models\Banner;
use App\Models\Curso;
use App\Models\BlogPost;

class GeracaoVencedoresController extends Controller
{

    public function index()
    {
        $plataforma = Plataforma::plataforma()->first();

        $seo = PaginaConteudo::plataforma()->where('url', '/')->first();

        $cursos = Curso::plataforma()->ativo()->publico()->get();

        $banners = Banner::plataforma()->ativo()->orderBy('ordem')->get();

        if(request('btnSend'))
        {
            if(request('check'))
            {
                return back()->with('danger', 'SPAM Detected!');
            }

            \App\Models\Contato::create(request()->all() + [
                'plataforma_id' => $seo->plataforma_id,
                'tipo' => 'L',
                'assunto' => 'Solicitar Contato',
            ]);

            return redirect('/#contato')->with('success','Inscrição enviada com êxito.');
        }

    	return view('sites-proprios.geracaodevencedores-com-br.home', compact('seo','cursos','banners') );
    }

    public function curso($slug)
    {
        $curso = \App\Models\Curso::plataforma()->publico()->where('slug', $slug)->firstOrFail();

        $turmas = \App\Models\Turma::plataforma()->ativo()->publico()->where('curso_id', $curso->id)->get();

        return view('sites-proprios.geracaodevencedores-com-br.curso', compact('curso','turmas') );
    }
    
    public function blog()
    {
        $plataforma_id = $this->getPlataformaId();
        
        $pagina = PaginaConteudo::plataforma()->where('url','/blog')->first();

        $postDestaque1 = BlogPost::select('blog_posts.id','blog_posts.titulo','blog_posts.slug','blog_posts.foto_capa','blog_posts.created_at','autor.nome as autor')
            ->leftJoin('users as autor','blog_posts.autor_id','autor.id')
            ->where('blog_posts.plataforma_id', $plataforma_id)
            ->where('blog_posts.publicar', 'S')
            ->where('blog_posts.destaque_ordem', 1)
            ->first();

        $postDestaque2 = BlogPost::select('blog_posts.id','blog_posts.titulo','blog_posts.slug','blog_posts.foto_capa','blog_posts.created_at','autor.nome as autor')
            ->leftJoin('users as autor','blog_posts.autor_id','autor.id')
            ->where('blog_posts.plataforma_id', $plataforma_id)
            ->where('blog_posts.publicar', 'S')
            ->where('blog_posts.destaque_ordem', 2)
            ->first();

        $postDestaque3 = BlogPost::select('blog_posts.id','blog_posts.titulo','blog_posts.slug','blog_posts.foto_capa','blog_posts.created_at','autor.nome as autor')
            ->leftJoin('users as autor','blog_posts.autor_id','autor.id')
            ->where('blog_posts.plataforma_id', $plataforma_id)
            ->where('blog_posts.publicar', 'S')
            ->where('blog_posts.destaque_ordem', 3)
            ->first();

        $postDestaque4 = BlogPost::select('blog_posts.id','blog_posts.titulo','blog_posts.slug','blog_posts.foto_capa','blog_posts.created_at','autor.nome as autor')
            ->leftJoin('users as autor','blog_posts.autor_id','autor.id')
            ->where('blog_posts.plataforma_id', $plataforma_id)
            ->where('blog_posts.publicar', 'S')
            ->where('blog_posts.destaque_ordem', 4)
            ->first();

        return view('sites-proprios.geracaodevencedores-com-br.blog', compact('pagina','postDestaque1','postDestaque2','postDestaque3','postDestaque4') );
    }

    public function blogPost($id)
    {
        $post = BlogPost::plataforma()->where('id', $id)->first();

        if(isset($post) && $post->publicar != 'S')
        {
            return view('sites-proprios.geracaodevencedores-com-br.sem-acesso', ['mensagem' => 'Página não publicada.']);
        }

        $ultimos_posts = BlogPost::plataforma()->publico()->orderBy('id','desc')->limit(10)->get();

        return view('sites-proprios.geracaodevencedores-com-br.blog-post', compact('post','ultimos_posts') );
    }

    public function getPlataformaId()
    {
        return session('plataforma_id') ?? \App\Models\Plataforma::dominio()->pluck('id')[0] ?? null;
    }

}