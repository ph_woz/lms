<!doctype html>
<html lang="pt-br">
<head>

  {!! $plataforma->scripts_start_head !!}

  <!-- Google tag (gtag.js) -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=G-MREB450HCT"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'G-MREB450HCT');
  </script>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/uikit@3.5.5/dist/css/uikit.min.css" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/5.0.0-alpha1/css/bootstrap.min.css" integrity="sha384-r4NyP46KrjDleawBgD5tp8Y7UzmLA05oM1iAEQ17CSuDqnUK2+k9luXQOfXJCJ4I" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('sites-proprios/geracaodevencedores-com-br/css/home.css') }}?v=8">
    <link rel="stylesheet" href="{{ asset('css/util.css') }}?v=2">

    <link rel="shortcut icon" href="{{ $plataforma->favicon ?? asset('sites-proprios/geracaodevencedores-com-br/images/favicon.ico') }}">
    <link href="{{ Request::url() }}" rel="canonical">

    <meta name="robots" content="index, follow">
    <title>{{ $seo->title ?? $plataforma->nome }}</title>
    <meta name="description" content="{{ $seo->description ?? null }}">
    <meta name="keywords" content="{{ $seo->keywords ?? null }}">

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <meta name='author' content='WOZCODE | Pedro Henrique' />

    <meta property="og:locale" content="pt_BR">
    <meta property="og:type" content="website">
    <meta property="og:image" content="{{ $plataforma->logotipoMenu ?? asset('sites-proprios/geracaodevencedores-com-br/images/logotipo.png') }}">
    <meta property="og:url" content="{{ Request::url() }}">
    <meta property="og:description" content="{{ $seo->description ?? null }}">
    <meta property="og:title" content="{{ $seo->title ?? $plataforma->nome }}">

    <style>
    
      #menuPlataforma nav { background: #355a7f; }
      .menu-link, .menu-link-color { color: #F5F5F5;; }
      .menu-link:hover { text-decoration: none; background: #1e3b57; color: #FFF!important; }
      .navbar-brand span, .activeNavLink { text-decoration: none; background: #1e3b57; color: #FFF!important; }

      #menu {
          width: 100%;
          box-sizing: border-box;
          padding: 9px 10px!important;
          position: fixed;
          z-index: 200;
          box-shadow: 0px -1px 8px 0px rgba(0, 0, 0, 0.60);
          background: transparent;
          transition: background-color 0.3s ease-in-out;
      }
      @media (min-width: 769px) 
      {
          #menu { height: 75px; }

          .menu-link { 
            margin: 0 10px;
            padding-top: 27.2px!important;
            padding-bottom: 27.2px!important;
            padding-left: 10px!important; 
            padding-right: 10px!important; 
          }
      }
      .menu-link {
          font-family: 'Open Sans',sans-serif;
          font-size: 13.5px;
          text-transform: uppercase;
          text-decoration: none;
          font-weight: 700;
          color: #F5F5F5!important;
          cursor: pointer;
          transition: all .2s;
          text-shadow: 1px 1px rgba(0, 0, 0, .1);
      }

      .btn-entrar { 
        background: #f5f5f5!important; 
        color: #355a7f!important; 
        font-family: 'Poppins', sans-serif; 
        font-weight: 600!important; 
        border-radius: 3px!important;
        padding: 12px 24px!important;
      }
          
      #footer { position: absolute!important; }
    
</style>

  {!! $plataforma->scripts_final_head !!}

</head>
<body id="inicio" style="background-image: url({{ asset('images/bg-login.png') }})">

{!! $plataforma->scripts_start_body !!}

      @include('sites-proprios/geracaodevencedores-com-br/componentes/header')

      <form method="POST" class="container-fluid px-xl-5 py-5 w-lg-60 mt-lg-3 mt-5" id="formContainer">
      @csrf

        <section class="bg-white p-lg-5 p-4 my-5 rounded-2 shadow">

          <div id="formDescricao">
            {!! nl2br($formulario->texto) !!}
          </div>

          <hr/>

              <div class="row mt-4">

                  @foreach($perguntas as $pergunta)
                      
                      @if($pergunta->tipo_input == 'select')
                          <div class="mb-4 col-lg-6">
                              <label>{{ $pergunta->pergunta }}</label>
                              <select name="{{ $pergunta->validation ?? 'respostas['.$pergunta->id.']' }}" class="{{ $pergunta->validation ?? null }} select-form" @if($pergunta->obrigatorio == 'S') required @endif>
                                  <option value="" selected disabled>Selecione</option>
                                  @foreach(unserialize($pergunta->options) as $option)
                                      <option value="{{ $option }}" @if(old($pergunta->validation) == $option) selected @endif>{{ $option }}</option>
                                  @endforeach
                              </select> 
                          </div>
                      @endif
                      
                      @if($pergunta->tipo_input == 'text')
                      <div class="mb-4 col-lg-6">
                          <label>{{ $pergunta->pergunta }}</label>
                          <input type="text" name="{{ $pergunta->validation ?? 'respostas['.$pergunta->id.']' }}" class="{{ $pergunta->validation ?? null }} form-control p-form" @if($pergunta->obrigatorio == 'S') required @endif>
                      </div>
                      @endif
                      
                      @if($pergunta->tipo_input == 'radio')
                      <div class="mb-4 col-12">
                          <label class="d-block mb-1">{{ $pergunta->pergunta }}</label>
                          @foreach(unserialize($pergunta->options) as $option)
                              <label for="opt{{$loop->index}}p{{$pergunta->id}}" class="mb-1 d-flex align-items-center">
                                  <input type="radio" class="square-15" id="opt{{$loop->index}}p{{$pergunta->id}}" name="{{ $pergunta->validation ?? 'respostas['.$pergunta->id.']' }}" value="{{ $option }}" @if($pergunta->obrigatorio == 'S') required @endif>
                                  <span class="ms-2">{!! $option !!}</span>
                              </label>
                          @endforeach
                      </div>
                      @endif

                      @if($pergunta->tipo_input == 'checkbox')
                      <div class="mb-4 col-12">
                          <label class="d-block mb-1">{{ $pergunta->pergunta }}</label>
                          @foreach(unserialize($pergunta->options) as $option)
                              <label for="opt{{$loop->index}}p{{$pergunta->id}}" class="mb-1 d-flex align-items-center">
                                  <input type="checkbox" class="square-15" id="opt{{$loop->index}}p{{$pergunta->id}}" name="{{ $pergunta->validation ?? 'respostas['.$pergunta->id.']' }}" value="{{ $option }}">
                                  <span class="ml-2">{!! $option !!}</span>
                              </label>
                          @endforeach
                      </div>
                      @endif

                  @endforeach

              </div>

              <input type="text" name="check" class="d-block h-0 border-0 outline-0 float-end">

              <button type="submit" name="sent" value="ok" id="formBtnEnviar" class="formBtnEnviar weight-600 fs-17 ls-05 d-inline-block rounded-3 box-shadow">
                Enviar
              </button>

        </section>

      </form>

      <div class="modal fade" id="modalCallback" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalCenterTitle">Inscrição enviada com sucesso!</h5>
            </div>
            <div class="modal-body">
              
                  {!! $formulario->texto_retorno !!}

            </div>
            <div class="modal-footer">
              <a href="{{ $formulario->link_retorno }}" class="btn btn-primary weight-600 shadow px-4 py-2">
                  {{ $formulario->texto_botao_retorno ?? 'Retornar' }}
              </a>
            </div>
          </div>
        </div>
      </div>


   </main>

   @include('sites-proprios/geracaodevencedores-com-br/componentes/footer')

   <script src="https://kit.fontawesome.com/2f91eaaef4.js" crossorigin="anonymous"></script>  
   <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-U1DAWAznBHeqEIlVSCgzq+c9gqGAJn5c/t99JyeKa9xxaYpSvHU5awsuZVVFIhvj" crossorigin="anonymous"></script>


    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.js"></script>

    @if(session('success') && $formulario->texto_retorno != null)
    <script>
        $(document).ready(function() {
            $("#modalCallback").modal('show');
        });
    </script>
    @endif

    <script>

        $(document).ready(function() {

            function limpa_formulário_cep() {

                $(".logradouro").val("");
                $(".bairro").val("");
                $(".cidade").val("");
                $(".estado").val("");
            }
            

            $(".cep").keyup(function() {

                var cep = $(this).val().replace(/\D/g, '');

                if (cep != "" && cep.length >= 8) {

                    var validacep = /^[0-9]{8}$/;

                    if(validacep.test(cep)) {

                        $(".logradouro").val("...");
                        $(".bairro").val("...");
                        $(".cidade").val("...");
                        $(".estado").val("...");

                        $.getJSON("https://viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {

                            if (!("erro" in dados)) {
                                $(".logradouro").val(dados.logradouro);
                                $(".bairro").val(dados.bairro);
                                $(".cidade").val(dados.localidade);
                                $(".estado").val(dados.uf);
                            } 
                            else {
                               
                                limpa_formulário_cep();
                            }
                        });
                    } 
                    else {
                        
                        limpa_formulário_cep();
                        
                    }
                }
                else {
                  
                    limpa_formulário_cep();
                }
            });
        });

    </script>

    <script>
      $(".email").attr('type','email');
      $(".password").attr('type','password');
        $(".cpf").mask('000.000.000-00').attr('minlength','14');
        $(".data_nascimento").mask('00/00/0000');
        $(".cep").mask('00000-000');
        $(".estado, .uf").attr('minlength','2').attr('maxlength','2');
        $(".numero").mask('000000000');
        $(".password").val('');

        $(document).on("focus", ".celular, .telefone", function() { 
          jQuery(this)
              .mask("(99) 9999-99999")
              .change(function (event) {  
                  target = (event.currentTarget) ? event.currentTarget : event.srcElement;  
                  phone = target.value.replace(/\D/g, '');
                  element = $(target);  
                  element.unmask();  
                  if(phone.length > 10) {  
                      element.mask("(99) 99999-9999");  
                  } else {  
                      element.mask("(99) 9999-9999?9");  
                  }  
          });
        });

        $(document).ready(function() {

            function limpa_formulário_cep() {

                $(".logradouro").val("");
                $(".bairro").val("");
                $(".cidade").val("");
                $(".estado").val("");
            }
            

            $(".cep").keyup(function() {

                var cep = $(this).val().replace(/\D/g, '');

                if (cep != "" && cep.length >= 8) {

                    var validacep = /^[0-9]{8}$/;

                    if(validacep.test(cep)) {

                        $(".logradouro").val("...");
                        $(".bairro").val("...");
                        $(".cidade").val("...");
                        $(".estado").val("...");

                        $.getJSON("https://viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {

                            if (!("erro" in dados)) {
                                $(".logradouro").val(dados.logradouro);
                                $(".bairro").val(dados.bairro);
                                $(".cidade").val(dados.localidade);
                                $(".estado").val(dados.uf);
                            } 
                            else {
                               
                                limpa_formulário_cep();
                            }
                        });
                    } 
                    else {
                        
                        limpa_formulário_cep();
                        
                    }
                }
                else {
                  
                    limpa_formulário_cep();
                }
            });
        });

        $('.estado, .uf').keyup(function(){
            $(this).val($(this).val().toUpperCase());
        });

    </script>

    @if(session()->has('success'))
        <div id="box-toast-success" class="boxToast">
            <div id="alertSuccess" class="toast-success">
                <p class="mt-2 text-white font-inter fs-14">
                    <span class="weight-800">Sucesso!&nbsp;</span>{{ session('success') }}
                </p>
            </div>
        </div>
        <script>
            setTimeout(function() {

                document.getElementById('box-toast-success').style.display = 'none';

            }, 2500);
        </script>
    @endif

    @if(session()->has('danger'))
        <div id="box-toast-danger" class="boxToast">
            <div id="alertDanger" class="toast-danger">
                <p class="mt-2 text-white font-inter fs-14">
                    <span class="weight-800">Ops!&nbsp;</span>{{ session('danger') }}
                </p>
            </div>
        </div>
        <script>
            setTimeout(function() {

                document.getElementById('box-toast-danger').style.display  = 'none';

            }, 2500);
        </script>
    @endif

    {!! $plataforma->scripts_final_body !!}
    
</body>
</html>