<div>

	<section class="mb-4-5">

		<div class="section-title rounded-bottom-0 d-flex justify-content-between align-items-center">
			{{ $plano->nome }}
			<div>

				<a href="#" data-bs-toggle="modal" data-bs-target="#ModalMetaDados" class="btn btn-info me-2 fs-14 mb-1 py-1 px-2-3">
					<i class="fa fa-align-left"></i>
				</a>
				<a href="{{ route('admin.plano.editar', ['id' => $plano->id]) }}" class="aclNivel2 btn btn-warning text-dark fs-14 mb-1 py-1 px-2">
					<i class="fa fa-edit"></i>
				</a>
				<a href="#" data-bs-toggle="modal" data-bs-target="#ModalDeletarPlano" class="aclNivel3 btn btn-danger mb-1 fs-15 py-1 px-2 ms-2">
					<i class="fa fa-trash"></i>
				</a>
			</div>
		</div>

		<div class="bg-white border p-4 d-lg-flex align-items-center justify-content-between">

			<div class="mb-2 mb-lg-0">
				<p class="mb-0 weight-600 color-444">Publicado</p>
				<div>
					@if($plano->publicar == 'S')
               <a href="#" target="_blank" class="hover-d-none">
                  <div class="bg-primary text-white rounded h-40px d-flex align-items-center justify-content-center w-150px">
                     <div class="weight-500 fs-13">
                        <span>
                           Sim
                        </span>
                     </div>
                     <div>
                        <div class="mx-2-3" style="width: 2.5px;height: 40px; background: #000; opacity: .2;"></div>
                     </div>
                     <div class="weight-500 fs-13"> 
                        <span class="me-1">Visualizar</span>
                        <i class="fas fa-external-link-alt"></i>
                     </div>
                  </div>
               </a>
               @else
                  <div class="bg-secondary text-white px-3 rounded h-40px d-flex align-items-center justify-content-center">
                     <div class="weight-500 fs-13">
                        <span>
                           Não
                        </span>
                     </div>
                  </div>
               @endif
				</div>
			</div>

			@if($unidade)
			<div class="mb-2 mb-lg-0">
				<p class="mb-0 weight-600 color-444">Unidade</p>
				<a href="{{ route('admin.unidade.info', ['id' => $unidade->id]) }}" class="color-blue-info-link">
					{{ $unidade->nome }}
				</a>
			</div>
			@endif

			<div class="mb-2 mb-lg-0">
				<p class="mb-0 weight-600 color-444">Preço</p>
				<div>
					<p class="mb-0">
						@if($plano->preco)
							R$ {{ $plano->preco }}
						@else
							Sob consulta
						@endif
					</p>
				</div>
			</div>

			<div class="mb-2 mb-lg-0">
				<p class="mb-0 weight-600 color-444">Preço Promocional</p>
				<div>
					<p class="mb-0">
						@if($plano->preco_promocional)
							R$ {{ $plano->preco_promocional }}
						@else
							Não definido
						@endif
					</p>
				</div>
			</div>

		</div>

		<div class="bg-white border p-4 d-lg-flex justify-content-between">

			<div class="mb-2 mb-lg-0">
				<p class="mb-0 weight-600 color-444">Categorias</p>
				@if(count($categorias) > 0)
					<ul class="list-group list-group-flush">
				  		@foreach($categorias as $categoria)
				  			<li class="list-group-item">{{ $categoria }}</li>
				  		@endforeach
				  	</ul>
				@else
					<p class="mb-0 font-nunito">Não definido</p>
				@endif
			</div>

		</div>

		@if(count($beneficios) > 0)
		<div class="bg-white border p-4 d-lg-flex justify-content-between">
			<div class="mb-2 mb-lg-0">
				<p class="mb-0 weight-600 color-444">Benefícios</p>
				<ul class="list-group list-group-flush">
			  		@foreach($beneficios as $beneficio)
			  			<li class="list-group-item">{{ $beneficio }}</li>
			  		@endforeach
			  	</ul>
			</div>
		</div>
		@endif

		<div class="bg-white border p-4 d-lg-flex justify-content-between">

			<div class="mb-2 mb-lg-0">
				<p class="mb-0 weight-600 color-444">Categorias</p>
				@if(count($categorias) > 0)
					<ul class="list-group list-group-flush">
				  		@foreach($categorias as $categoria)
				  			<li class="list-group-item">{{ $categoria }}</li>
				  		@endforeach
				  	</ul>
				@else
					<p class="mb-0 font-nunito">Não definido</p>
				@endif
			</div>

		</div>
		
	</section>

	<div class="modal fade" id="ModalMetaDados" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	  	<div class="modal-dialog modal-dialog-centered" role="document">
	    	<div class="modal-content">
	      		<div class="modal-header">
	        		<h3 class="modal-title">Metadados</h3>
	    			<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
	      		</div>
	      		<div class="modal-body">

					<ul class="list-group list-group-flush">
	  					<li class="list-group-item">
							<span class="mb-0 weight-600 color-444">Cadastrante:</span>
							@if($cadastrante)
								<a href="{{ route('admin.colaborador.info', ['id' => $cadastrante->id]) }}" class="color-blue-info-link">
									{{ $cadastrante->nome }}
								</a>
							@else
								Não definido
							@endif
	  					</li>   
	  					<li class="list-group-item">
							<span class="mb-0 weight-600 color-444">Status:</span>
							@if($plano->status == 0)
								<span class="text-success weight-700">Ativo</span>
							@else
								<span class="text-danger weight-700">Desativado</span>
							@endif
	  					</li>
	  					<li class="list-group-item">
							<span class="mb-0 weight-600 color-444">Data de Cadastro:</span>
							@if($plano->created_at)
								<span class="mb-0">{{ $plano->created_at->format('d/m/Y H:i') ?? null }} ({{$plano->created_at->diffForHumans()}}) </span>
	  						@else
	  							Não definido
	  						@endif
	  					</li>   
	  				</ul>

	      		</div>
	    	</div>
	  	</div>
	</div>

	<form wire:submit.prevent="delete" method="POST">
		<div wire:ignore class="modal fade" id="ModalDeletarPlano" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  		<div class="modal-dialog modal-dialog-centered">
	    		<div class="modal-content">
	      			<div class="modal-header">
	        			<h3 class="modal-title" id="exampleModalLabel">Deletar Plano</h3>
	        			<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
	      			</div>
	      			<div class="modal-body text-center py-5">
	        			<p class="m-0 fs-16">
	        				Você tem certeza que deseja deletar esta plano?
	        				<br>
	        				Todas as informações serão perdidas definitivamente.
	        			</p>
	      			</div>
	      			<div class="modal-footer">
	      				<div class="d-flex w-100">
		      				<input type="password" wire:model="password_to_delete" class="w-100 form-control p-form me-3" placeholder="Digite sua senha" required>
		        			<button type="submit" class="btn btn-danger weight-600">Confirmar</button>
	    				</div>
	      			</div>
	    		</div>
	  		</div>
		</div>
	</form>

	@include('livewire.util.toast')

</div>

