<!DOCTYPE html>
<html lang="pt-br">
<head>

  <!-- Google tag (gtag.js) -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=G-MREB450HCT"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'G-MREB450HCT');
  </script>

  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

   <title>{{ $seo->seo_title ?? $plataforma->nome }}</title>
   <meta name="robots" content="index, follow">
   <meta name="description" content="{{ $seo->seo_description ?? null }}"/>
   <meta name="keywords" content="{{ $seo->seo_keywords ?? null }}"/>
   <link rel="canonical" href="{{ Request::url() }}" />
   <link rel="shortcut icon" href="{{ $plataforma->favicon }}">
   <meta property="og:locale" content="pt_BR" />
   <meta property="og:type" content="website" />
   <meta property="og:title" content="{{ $seo->seo_title ?? $plataforma->nome }}" />
   <meta property="og:description" content="{{ $seo->seo_description ?? null }}" />
   <meta property="og:url" content="{{ Request::url() }}" />
   <meta property="og:site_name" content="{{ $plataforma->nome }}" />
   <meta property="og:image" content="{{ $plataforma->logotipo }}" />

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="{{ asset('sites-proprios/cedetep/enfermagem/vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet">
  <link href="{{ asset('sites-proprios/cedetep/enfermagem/vendor/animate.css/animate.min.css') }}" rel="stylesheet">
  <link href="{{ asset('sites-proprios/cedetep/enfermagem/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
  <link href="{{ asset('sites-proprios/cedetep/enfermagem/vendor/bootstrap-icons/bootstrap-icons.css') }}" rel="stylesheet">
  <link href="{{ asset('sites-proprios/cedetep/enfermagem/vendor/boxicons/css/boxicons.min.css') }}" rel="stylesheet">
  <link href="{{ asset('sites-proprios/cedetep/enfermagem/vendor/glightbox/css/glightbox.min.css') }}" rel="stylesheet">
  <link href="{{ asset('sites-proprios/cedetep/enfermagem/vendor/remixicon/remixicon.css') }}" rel="stylesheet">
  <link href="{{ asset('sites-proprios/cedetep/enfermagem/vendor/swiper/swiper-bundle.min.css') }}" rel="stylesheet">
  <link href="https://cedetep.com/sites-proprios/cedetep/css/home.css" rel="stylesheet">  
  <meta name='author' content='WOZCODE | Paulo Sérgio' />
  
  <!-- Template Main CSS File -->
  <link href="{{ asset('sites-proprios/cedetep/enfermagem/css/style.css') }}" rel="stylesheet">

</head>

<body>

  <!-- ======= Top Bar ======= -->
  <div id="topbar" class="d-flex align-items-center fixed-top">
    <div class="container d-flex justify-content-between">
      <div class="contact-info d-flex align-items-center">    
        <i class="bi bi-envelope"></i> <a href="mailto:contact@example.com">contato.cedetep@gmail.com</a>
        <i class="bi bi-phone"></i> (12) 99260-9279
      </div>
      <div class="d-none d-lg-flex social-links align-items-center">
        <a href="https://www.facebook.com/cedetepoficial" target="_blank" class="facebook"><i class="bi bi-facebook"></i></a>
        <a href="https://www.instagram.com/cedetepoficial/" target="_blank" class="instagram"><i class="bi bi-instagram"></i></a>
      </div>
    </div>
  </div>

  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top">
    <div class="container d-flex align-items-center">

      <h1 class="logo me-auto"><a href="index.html">Técnico em enfermagem</a></h1>
    

      <nav id="navbar" class="navbar order-last order-lg-0">
        <ul>
          <li><a class="nav-link scrollto active" href="#hero">Início</a></li>
          <li><a class="nav-link scrollto" href="#about">Sobre</a></li>
          <li><a class="nav-link scrollto" href="#services">Serviços</a></li>
          <li><a class="nav-link scrollto" href="#departments">Departamentos</a></li>
          <!-- <li><a class="nav-link scrollto" href="#doctors">Doctors</a></li> -->
          <!-- <li class="dropdown"><a href="#"><span>Drop Down</span> <i class="bi bi-chevron-down"></i></a>
            <ul>
              <li><a href="#">Drop Down 1</a></li>
              <li class="dropdown"><a href="#"><span>Deep Drop Down</span> <i class="bi bi-chevron-right"></i></a>
                <ul>
                  <li><a href="#">Deep Drop Down 1</a></li>
                  <li><a href="#">Deep Drop Down 2</a></li>
                  <li><a href="#">Deep Drop Down 3</a></li>
                  <li><a href="#">Deep Drop Down 4</a></li>
                  <li><a href="#">Deep Drop Down 5</a></li>
                </ul>
              </li>
              <li><a href="#">Drop Down 2</a></li>
              <li><a href="#">Drop Down 3</a></li>
              <li><a href="#">Drop Down 4</a></li>
            </ul>
          </li> -->
          <li><a class="nav-link scrollto" href="#contact">Contatos</a></li>
        </ul>
        <i class="bi bi-list mobile-nav-toggle"></i>
      </nav><!-- .navbar -->
    </div>
  </header><!-- End Header -->

  <!-- ======= Hero Section ======= -->
  <section id="hero" class="d-flex align-items-center">
    <div class="container">
      <h1>Bem vindo a CEDETEP</h1>
      <h2>ESCOLA TÉCNICA DE ENFERMAGEM EM RIBEIRÃO PRETO</h2>
      <!-- <a href="#about" class="btn-get-started scrollto">Get Started</a> -->
    </div>
  </section><!-- End Hero -->

  <main id="main">

    <!-- ======= Why Us Section ======= -->
    <section id="why-us" class="why-us">
      <div class="container">

        <div class="row">
          <div class="col-lg-4 d-flex align-items-stretch">
            <div class="content">
              <h3> Por que escolher a CEDETEP?</h3>
              <p>                
                O Técnico em Enfermagem atua na prevenção e na reabilitação dos pacientes, colaborando com a orientação, a preparação para exames e prestando assistência. Além disso, realiza cuidados como: curativos, administração de medicamentos e vacinas, banho de leito, entre outros. Veja abaixo como o Técnico em Enfermagem pode atuar.</p>
              <div class="text-center">
                <!-- <a href="#" class="more-btn">Learn More <i class="bx bx-chevron-right"></i></a> -->
              </div>
            </div>
          </div>
          <div class="col-lg-8 d-flex align-items-stretch">
            <div class="icon-boxes d-flex flex-column justify-content-center">
              <div class="row">
                <div class="col-xl-4 d-flex align-items-stretch">
                  <div class="icon-box mt-4 mt-xl-0">
                    <i class="bx bx-receipt"></i>
                    <h4>Enfermeiro da saúde pública</h4>
                    <p>Orienta a população sobre a prevenção de doenças e promove a saúde da coletividade. Atende pacientes em hospitais, centros de saúde, creches e escolas. Forma, capacita e supervisiona os agentes de saúde.</p>
                  </div>
                </div>
                <div class="col-xl-4 d-flex align-items-stretch">
                  <div class="icon-box mt-4 mt-xl-0">
                    <i class="bx bx-cube-alt"></i>
                    <h4>Enfermeiro pediátrico</h4>
                    <p>Acompanha e avalia o crescimento e o desenvolvimento da criança. Incentiva o aleitamento materno e orienta os pais sobre as técnicas e os cuidados com os recém-nascidos.</p>
                  </div>
                </div>
                <div class="col-xl-4 d-flex align-items-stretch">
                  <div class="icon-box mt-4 mt-xl-0">
                    <i class="bx bx-images"></i>
                    <h4>Enfermeiro de resgate</h4>
                    <p>Participa de equipes de salvamento de vítimas de acidentes ou de calamidades públicas.</p>
                  </div>
                </div>
              </div>
            </div><!-- End .content-->
          </div>
        </div>

      </div>
    </section><!-- End Why Us Section -->
    </br> </br>
    <!-- ======= About Section ======= -->
    <section id="about" class="about">
      <div class="container-fluid">

        <div class="row">
          <div class="col-xl-5 col-lg-6 video-box d-flex justify-content-center align-items-stretch position-relative">
            <a href="https://www.youtube.com/watch?v=jDDaplaOz7Q" class="glightbox play-btn mb-4"></a>
          </div>

          <div class="col-xl-7 col-lg-6 icon-boxes d-flex flex-column align-items-stretch justify-content-center py-5 px-lg-5">
            <h3>Motivos para fazer curso técnico em enfermagem.</h3>
            <!-- <p>Esse voluptas cumque vel exercitationem. Reiciendis est hic accusamus. Non ipsam et sed minima temporibus laudantium. Soluta voluptate sed facere corporis dolores excepturi. Libero laboriosam sint et id nulla tenetur. Suscipit aut voluptate.</p> -->

            <div class="icon-box">
              <div class="icon"><i class="bx bx-fingerprint"></i></div>
              <h4 class="title"><a href="">Curso rápido em 20 meses</a></h4>
              <p class="description">Se você tem urgência para entrar no mercado de trabalho e mudar de vida, o Curso Técnico é uma ótima escolha.</p>
            </div>

            <div class="icon-box">
              <div class="icon"><i class="bx bx-gift"></i></div>
              <h4 class="title"><a href="">Salário mais atrativo</a></h4>
              <p class="description">
                Sendo qualificado como técnico, você terá oportunidades de emprego com melhores condições de salário e benefícios. Você ainda poderá fazer um concurso público e ter ganhos ainda maiores.</p>
            </div>

            <div class="icon-box">
              <div class="icon"><i class="bx bx-atom"></i></div>
              <h4 class="title"><a href="">Baixo Custo</a></h4>
              <p class="description">
                Os valores da Cedetep são competitivos e estão de acordo com o mercado. Aqui você ainda ganha desconto na matrícula.</p>
            </div>

          </div>
        </div>

      </div>
    </section><!-- End About Section -->

    <!-- ======= Counts Section ======= -->
    <!-- <section id="counts" class="counts">
      <div class="container">

        <div class="row">

          <div class="col-lg-3 col-md-6">
            <div class="count-box">
              <i class="fas fa-user-md"></i>
              <span data-purecounter-start="0" data-purecounter-end="85" data-purecounter-duration="1" class="purecounter"></span>
              <p>Doctors</p>
            </div>
          </div>

          <div class="col-lg-3 col-md-6 mt-5 mt-md-0">
            <div class="count-box">
              <i class="far fa-hospital"></i>
              <span data-purecounter-start="0" data-purecounter-end="18" data-purecounter-duration="1" class="purecounter"></span>
              <p>Departments</p>
            </div>
          </div>

          <div class="col-lg-3 col-md-6 mt-5 mt-lg-0">
            <div class="count-box">
              <i class="fas fa-flask"></i>
              <span data-purecounter-start="0" data-purecounter-end="12" data-purecounter-duration="1" class="purecounter"></span>
              <p>Research Labs</p>
            </div>
          </div>

          <div class="col-lg-3 col-md-6 mt-5 mt-lg-0">
            <div class="count-box">
              <i class="fas fa-award"></i>
              <span data-purecounter-start="0" data-purecounter-end="150" data-purecounter-duration="1" class="purecounter"></span>
              <p>Awards</p>
            </div>
          </div>

        </div>

      </div>
    </section>End Counts Section -->
  </br>
  </br>
    <!-- ======= Services Section ======= -->
    <section id="services" class="services">
      <div class="container">

        <div class="section-title">
          <h1><strong>Atividades do Técnico em Enfermagem</strong></h1>
          <!-- <p>Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit in iste officiis commodi quidem hic quas.</p> -->
        </div>

        <div class="row">
          <div class="col-lg-4 col-md-6 d-flex align-items-stretch">
            <div class="icon-box">
              <div class="icon"><i class="fas fa-heartbeat"></i></div>
              <h4><a href=""> Realiza curativos, administra medicamentos e vacinas, executa nebulizações, banho de leito e realiza mensuração antropométrica e verificação de sinais vitais.</a></h4>
              <!-- <p> Realiza curativos, administra medicamentos e vacinas, executa nebulizações, banho de leito e realiza mensuração antropométrica e verificação de sinais vitais.</p> -->
            </div>
          </div>

          <div class="col-lg-4 col-md-6 d-flex align-items-stretch mt-4 mt-md-0">
            <div class="icon-box">
              <div class="icon"><i class="fas fa-pills"></i></div>
              <h4><a href="">Auxilia na promoção, prevenção, recuperação e reabilitação no processo saúde-doença.</a></h4>
              <!-- <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore</p> -->
            </div>
          </div>

          <div class="col-lg-4 col-md-6 d-flex align-items-stretch mt-4 mt-lg-0">
            <div class="icon-box">
              <div class="icon"><i class="fas fa-hospital-user"></i></div>
              <h4><a href=""> Prepara o paciente para os procedimentos de saúde.</a></h4>
              <!-- <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia</p> -->
            </div>
          </div>

          <div class="col-lg-4 col-md-6 d-flex align-items-stretch mt-4">
            <div class="icon-box">
              <div class="icon"><i class="fas fa-dna"></i></div>
              <h4><a href=""> Presta assistência de enfermagem a pacientes clínicos, cirúrgicos e gravemente enfermos.</a></h4>
              <!-- <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis</p> -->
            </div>
          </div>

          <div class="col-lg-4 col-md-6 d-flex align-items-stretch mt-4">
            <div class="icon-box">
              <div class="icon"><i class="fas fa-wheelchair"></i></div>
              <h4><a href="">Aplica as normas de biossegurança.</a></h4>
              <!-- <p>Quis consequatur saepe eligendi voluptatem consequatur dolor consequuntur</p> -->
            </div>
          </div>

          <div class="col-lg-4 col-md-6 d-flex align-items-stretch mt-4">
            <div class="icon-box">
              <div class="icon"><i class="fas fa-notes-medical"></i></div>
              <h4><a href="">Divera don</a></h4>
              <p>Modi nostrum vel laborum. Porro fugit error sit minus sapiente sit aspernatur</p>
            </div>
          </div>

        </div>

      </div>
    </section><!-- End Services Section -->   
    </br></br>     

    <!-- ======= Departments Section ======= -->
    <section id="departments" class="departments">
      <div class="container">

        <div class="section-title">
          <h2>Departments</h2>
          <p>Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit in iste officiis commodi quidem hic quas.</p>
        </div>

        <div class="row gy-4">
          <div class="col-lg-3">
            <ul class="nav nav-tabs flex-column">
              <li class="nav-item">
                <a class="nav-link active show" data-bs-toggle="tab" href="#tab-1">Cardiologia</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" data-bs-toggle="tab" href="#tab-2">Neurologia</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" data-bs-toggle="tab" href="#tab-3">Hepatologia</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" data-bs-toggle="tab" href="#tab-4">Pediatria</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" data-bs-toggle="tab" href="#tab-5">Cuidados com os olhos</a>
              </li>
            </ul>
          </div>
          <div class="col-lg-9">
            <div class="tab-content">
              <div class="tab-pane active show" id="tab-1">
                <div class="row gy-4">
                  <div class="col-lg-8 details order-2 order-lg-1">
                    <h3>Cardiologia</h3>
                    <p class="fst-italic">Qui laudantium consequatur laborum sit qui ad sapiente dila parde sonata raqer a videna mareta paulona marka</p>
                    <p>Et nobis maiores eius. Voluptatibus ut enim blanditiis atque harum sint. Laborum eos ipsum ipsa odit magni. Incidunt hic ut molestiae aut qui. Est repellat minima eveniet eius et quis magni nihil. Consequatur dolorem quaerat quos qui similique accusamus nostrum rem vero</p>
                  </div>
                  <div class="col-lg-4 text-center order-1 order-lg-2">
                    <img src="assets/img/departments-1.jpg" alt="" class="img-fluid">
                  </div>
                </div>
              </div>
              <div class="tab-pane" id="tab-2">
                <div class="row gy-4">
                  <div class="col-lg-8 details order-2 order-lg-1">
                    <h3>Neurologia</h3>
                    <p class="fst-italic">Qui laudantium consequatur laborum sit qui ad sapiente dila parde sonata raqer a videna mareta paulona marka</p>
                    <p>Ea ipsum voluptatem consequatur quis est. Illum error ullam omnis quia et reiciendis sunt sunt est. Non aliquid repellendus itaque accusamus eius et velit ipsa voluptates. Optio nesciunt eaque beatae accusamus lerode pakto madirna desera vafle de nideran pal</p>
                  </div>
                  <div class="col-lg-4 text-center order-1 order-lg-2">
                    <img src="assets/img/departments-2.jpg" alt="" class="img-fluid">
                  </div>
                </div>
              </div>
              <div class="tab-pane" id="tab-3">
                <div class="row gy-4">
                  <div class="col-lg-8 details order-2 order-lg-1">
                    <h3>Hepatologia</h3>
                    <p class="fst-italic">Eos voluptatibus quo. Odio similique illum id quidem non enim fuga. Qui natus non sunt dicta dolor et. In asperiores velit quaerat perferendis aut</p>
                    <p>Iure officiis odit rerum. Harum sequi eum illum corrupti culpa veritatis quisquam. Neque necessitatibus illo rerum eum ut. Commodi ipsam minima molestiae sed laboriosam a iste odio. Earum odit nesciunt fugiat sit ullam. Soluta et harum voluptatem optio quae</p>
                  </div>
                  <div class="col-lg-4 text-center order-1 order-lg-2">
                    <img src="assets/img/departments-3.jpg" alt="" class="img-fluid">
                  </div>
                </div>
              </div>
              <div class="tab-pane" id="tab-4">
                <div class="row gy-4">
                  <div class="col-lg-8 details order-2 order-lg-1">
                    <h3>Pediatria</h3>
                    <p class="fst-italic">Totam aperiam accusamus. Repellat consequuntur iure voluptas iure porro quis delectus</p>
                    <p>Eaque consequuntur consequuntur libero expedita in voluptas. Nostrum ipsam necessitatibus aliquam fugiat debitis quis velit. Eum ex maxime error in consequatur corporis atque. Eligendi asperiores sed qui veritatis aperiam quia a laborum inventore</p>
                  </div>
                  <div class="col-lg-4 text-center order-1 order-lg-2">
                    <img src="assets/img/departments-4.jpg" alt="" class="img-fluid">
                  </div>
                </div>
              </div>
              <div class="tab-pane" id="tab-5">
                <div class="row gy-4">
                  <div class="col-lg-8 details order-2 order-lg-1">
                    <h3>Cuidados com os olhos</h3>
                    <p class="fst-italic">Omnis blanditiis saepe eos autem qui sunt debitis porro quia.</p>
                    <p>Exercitationem nostrum omnis. Ut reiciendis repudiandae minus. Omnis recusandae ut non quam ut quod eius qui. Ipsum quia odit vero atque qui quibusdam amet. Occaecati sed est sint aut vitae molestiae voluptate vel</p>
                  </div>
                  <div class="col-lg-4 text-center order-1 order-lg-2">
                    <img src="assets/img/departments-5.jpg" alt="" class="img-fluid">
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <!-- ======= Testimonials Section ======= -->
    <section id="testimonials" class="testimonials">
      <div class="container">

        <div class="testimonials-slider swiper" data-aos="fade-up" data-aos-delay="100">
          <div class="swiper-wrapper">

            @foreach($depoimentos as $depoimento)
            <div class="swiper-slide">
              <div class="testimonial-wrap">
                <div class="testimonial-item">
                  <img src="{{ $depoimento->foto }}" class="testimonial-img" alt="">
                  <h3>{{ $depoimento->nome }}</h3>
                  <h4>Enfermagem</h4>
                  <p>
                    <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                    {{ $depoimento->descricao }}
                    <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                  </p>
                </div>
              </div>
            </div><!-- End testimonial item -->
            @endforeach

          </div>
          <div class="swiper-pagination"></div>
        </div>

      </div>
    </section><!-- End Testimonials Section -->
    

    <!-- ======= Gallery Section ======= -->
    <section id="gallery" class="gallery">
      <div class="container">

        <div class="section-title">
          <h2>Galeria de fotos do curso</h2>
          <p>
            Ao longo dos anos, com base do que é visivel sobre nossos ensinos, envaidece.nos deixando assim a nosso  cargo a obrigação de proporcionar ao alunado  ensinamentos com tecnologias avançadas, pautadas nos melhores referênciais pedagógicas.
         </p>
        </div>
      </div>

      <div class="container-fluid">
        <div class="row g-0">

          <div class="col-lg-3 col-md-4">
            <div class="gallery-item">
              <a href="{{ asset('sites-proprios/cedetep/enfermagem/img/gallery/gallery-1.jpg') }}" class="galelry-lightbox">
                <img src="{{ asset('sites-proprios/cedetep/enfermagem/img/gallery/gallery-1.jpg') }}" alt="" class="img-fluid">
              </a>
            </div>
          </div>

          <div class="col-lg-3 col-md-4">
            <div class="gallery-item">
              <a href="{{ asset('sites-proprios/cedetep/enfermagem/img/gallery/gallery-2.jpg') }}" class="galelry-lightbox">
                <img src="{{ asset('sites-proprios/cedetep/enfermagem/img/gallery/gallery-2.jpg') }}" alt="" class="img-fluid">
              </a>
            </div>
          </div>

          <div class="col-lg-3 col-md-4">
            <div class="gallery-item">
              <a href="{{ asset('sites-proprios/cedetep/enfermagem/img/gallery/gallery-3.jpg') }}" class="galelry-lightbox">
                <img src="{{ asset('sites-proprios/cedetep/enfermagem/img/gallery/gallery-3.jpg') }}" alt="" class="img-fluid">
              </a>
            </div>
          </div>

          <div class="col-lg-3 col-md-4">
            <div class="gallery-item">
              <a href="{{ asset('sites-proprios/cedetep/enfermagem/img/gallery/gallery-4.jpg') }}" class="galelry-lightbox">
                <img src="{{ asset('sites-proprios/cedetep/enfermagem/img/gallery/gallery-4.jpg') }}" alt="" class="img-fluid">
              </a>
            </div>
          </div>

          <div class="col-lg-3 col-md-4">
            <div class="gallery-item">
              <a href="{{ asset('sites-proprios/cedetep/enfermagem/img/gallery/gallery-5.jpg') }}" class="galelry-lightbox">
                <img src="{{ asset('sites-proprios/cedetep/enfermagem/img/gallery/gallery-5.jpg') }}" alt="" class="img-fluid">
              </a>
            </div>
          </div>

          <div class="col-lg-3 col-md-4">
            <div class="gallery-item">
              <a href="{{ asset('sites-proprios/cedetep/enfermagem/img/gallery/gallery-6.jpg') }}" class="galelry-lightbox">
                <img src="{{ asset('sites-proprios/cedetep/enfermagem/img/gallery/gallery-6.jpg') }}" alt="" class="img-fluid">
              </a>
            </div>
          </div>

          <div class="col-lg-3 col-md-4">
            <div class="gallery-item">
              <a href="{{ asset('sites-proprios/cedetep/enfermagem/img/gallery/gallery-7.jpg') }}" class="galelry-lightbox">
                <img src="{{ asset('sites-proprios/cedetep/enfermagem/img/gallery/gallery-7.jpg') }}" alt="" class="img-fluid">
              </a>
            </div>
          </div>

          <div class="col-lg-3 col-md-4">
            <div class="gallery-item">
              <a href="{{ asset('sites-proprios/cedetep/enfermagem/img/gallery/gallery-8.jpg') }}" class="galelry-lightbox">
                <img src="{{ asset('sites-proprios/cedetep/enfermagem/img/gallery/gallery-8.jpg') }}" alt="" class="img-fluid">
              </a>
            </div>
          </div>

        </div>

      </div>
    </section><!-- End Gallery Section -->
  </br></br>

    <!-- ======= Contact Section ======= -->
      <section>
      <div>
        <div class="section-title">
          <h2>Contato</h2>
          </div>         
        <iframe style="border:0; width: 100%; height: 350px;" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d41576.05371270165!2d-45.43507772112088!3d-22.919319944901236!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x70581a8d7cfa2dad!2sCEDETEP%20Escola%20T%C3%A9cnica%20-%20Pindamonhangaba!5e0!3m2!1spt-BR!2sbr!4v1664580570383!5m2!1spt-BR!2sbr" frameborder="0" allowfullscreen></iframe>
      </br>      
    </br>       

<section class="container">
  
     <div class="row">

        <div class="col-lg-6">
              <img src="https://cedetep.com/sites-proprios/cedetep/images/foto-matricula.jpeg" class="img-fluid h-lg-500 box-shadow"> 
        </div>

        <div class="col-lg-6">

           <div class="border-top-gradient p-1"></div>
           <div class="bg-white box-shadow rounded-2 px-4 py-4 h-lg-540">

              <h1 class="font-roboto-condensed color-verde fs-30">
                 CHEGOU A HORA DE <strong>MUDAR O SEU FUTURO</strong>
              </h1>

              <p class="mb-0 pb-0 color-suave font-poppins weight-600 fs-15">
                 Faça agora o seu cadastro e garanta uma oferta super especial para se matricular em qualquer uma de nossas unidades espalhadas pelo Brasil.
              </p>

              <form method="POST" id="formSolicitarContato" class="pt-4-5">
              @csrf
                
                @if(session('success'))
                   <div class='alert alert-success'><b>Sucesso!</b> {{ session('success') }}</div>
                @endif

                 <div class="d-lg-flex mb-3">
                    <input type="text" name="nome" class="form-control lead-form me-lg-3 mb-3 mb-lg-0" placeholder="NOME" required>
                    <input type="email" name="email" class="form-control lead-form" placeholder="EMAIL" required>
                 </div>
                 <div class="d-lg-flex mb-3">                    
                    <input type="phone" name="celular" class="form-control lead-form" placeholder="(DDD) CELULAR" required>
                 </div>

                 <div class="mt-4">
                    <label class="cursor-pointer font-poppins fs-14">
                       <input type="checkbox" class="" name="consentimento" value="1" required> Concordo que os dados pessoais fornecidos acima serão utilizados para envio de conteúdo informativo, analítico e publicitário sobre produtos, serviços e assuntos gerais, nos termos da Lei Geral de Proteção de Dados.
                    </label>
                 </div>

                 <input type="text" name="check" class="invisible h-0px" placeholder="Não preencha este campo.">

                 <input type="submit" name="sentContato" value="Solicitar Contato!" class="btnEntrarEmContato box-shadow">

              </form>
           </div>           
        </div>     
  </div>
</section>

</br>

  <footer id="contact" style="background-image: url('https://cedetep.com/sites-proprios/cedetep/images/footer.jpg');">
    <div class="container-footer px-xl-9 pt-5 ps-lg-5">

       <div class="row align-items-end py-lg-4 px-3 px-md-0">

          <div class="col-lg-9">

             <h1 class="fs-35 font-montserrat text-light weight-800 text-uppercase">FALE CONOSCO</h1>
             <div class="my-4 w-80px h-2px bg-line rounded"></div>

             <img src="https://wozcodelms2.s3.amazonaws.com/plataformas/1/logotipo.jpeg" class="img-fluid" width="100">

             <div class="mt-2">                 

                <div class="mb-3">
                   <span class="text-light font-poppins fs-14">
                      <i class="color-d4 fa fa-envelope"></i> 
                      Contato.cedetep@gmail.com
                   </span>
                </div>
                <div class="mb-3">
                   <a href="https://api.whatsapp.com/send?phone=5512992609279&text=Olá, vim através do Site CEDETEP ESCOLA TECNICA" class="text-light font-poppins fs-14">
                      <i class="color-d4 fab fa-whatsapp"></i> 
                      (12) 99260-9279
                   </a>
                </div>
                <div class="mb-3">
                   <span class="text-light font-poppins fs-14">
                      <i class="color-d4 fa fa-clock"></i> 
                      Horário de Funcionamento: Segunda à Sexta, 07:00h às 22:30h
                   </span>
                </div> 
                <div class="mb-3">
                  <address>
                    <a href="https://www.google.com/maps/place/CEDETEP+Escola+T%C3%A9cnica+-+Pindamonhangaba/@-22.924224,-45.468257,14z/data=!4m5!3m4!1s0x0:0x70581a8d7cfa2dad!8m2!3d-22.9242244!4d-45.4682567?hl=pt-BR" target="_blank" class="text-light font-poppins fs-14">
                         <i class="color-d4 fas fa-map-marker-alt"></i>
                         Rua Bicudo Leme, 611 Jardim irmãos Braga - Pindamonhangaba
                    </a>
                   </address>
                </div>

                <div class="mt-4 d-flex">

                   <a href="https://www.facebook.com/cedetepoficial" target="_blank" class="me-2 hover-d-none">
                      <div class="square-40 rounded-circle social-icon social-icon-facebook flex-center">
                         <i class="text-white fab fa-facebook fs-20"></i>
                      </div>
                   </a>
                   <a href="https://www.instagram.com/cedetepoficial/" target="_blank" class="hover-d-none">
                      <div class="square-40 rounded-circle social-icon social-icon-instagram flex-center">
                         <i class="text-white fab fa-instagram fs-20"></i>
                      </div>
                   </a>

                </div>

             </div>

          </div>

          <div class="col-lg-3 mt-5 mt-lg-0">

             <div class="mb-3">
                <a href="https://cedetep.com/f/formulario-de-matricula/34/t/595" target="_blank" id="botao" class="bg-gradient fs-14 d-block text-center text-shadow-2 px-4 rounded box-shadow text-light weight-600 w-100 py-2-5 font-poppins hover-d-none">
                   MATRICULE-SE JÁ!
                </a>
             </div>
             <div class="mb-3">
                <a href="/login" id="botao" class="bg-gradient fs-14 d-block text-center text-shadow-2 px-4 rounded box-shadow text-light weight-600 w-100 py-2-5 font-poppins hover-d-none">
                   ÁREA DO ALUNO
                </a>
             </div>
             <div class="mb-3">
                <a href="https://cedetep.com/f/torne-se-parceiro/35/t/596" target="_blank" id="botao" class="bg-gradient fs-14 d-block text-center text-shadow-2 px-4 rounded box-shadow text-light weight-600 w-100 py-2-5 font-poppins hover-d-none">
                   TORNE-SE PARCEIRO
                </a>
             </div>
             <div class="mb-3">
                <a href="https://api.whatsapp.com/send?phone=5512992609279&text=Olá, vim através do Site CEDETEP ESCOLA TECNICA" id="botao" class="bg-gradient fs-14 d-block text-center text-shadow-2 px-4 rounded box-shadow text-light weight-600 w-100 py-2-5 font-poppins hover-d-none">
                   ENTRAR EM CONTATO
                </a>
             </div>

          </div>

       </div>

       <hr class="mt-4 border border-light" />

       <div class="pb-3 d-lg-flex justify-content-between align-items-center">

          <p class="text-xs-center mb-1 mb-lg-0 p-0 text-light font-poppins ls-05" style="font-size: 9px;">
             CEDETEP ESCOLA TECNICA © 2022 • Todos os direitos reservados • CNPJ: 44.515.584/0001-03
          </p>

          <p class="text-xs-center m-0 p-0 text-light font-poppins ls-05" style="font-size: 10px;">
             Desenvolvido por <a href="https://wozcode.com" target="_blank" class="text-danger text-d-none fs-11 weight-600 text-shadow-1">WOZCODE</a>
          </p>

       </div>
    </div>
 </footer>

 
  <div id="preloader"></div>
  <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

  <!-- Vendor JS Files -->
  <script src="{{ asset('sites-proprios/cedetep/enfermagem/vendor/purecounter/purecounter_vanilla.js') }}"></script>
  <script src="{{ asset('sites-proprios/cedetep/enfermagem/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
  <script src="{{ asset('sites-proprios/cedetep/enfermagem/vendor/glightbox/js/glightbox.min.js') }}"></script>
  <script src="{{ asset('sites-proprios/cedetep/enfermagem/vendor/swiper/swiper-bundle.min.js') }}"></script>
  <script src="{{ asset('sites-proprios/cedetep/enfermagem/vendor/php-email-form/validate.js') }}"></script>

  <!-- Template Main JS File -->
  <script src="{{ asset('sites-proprios/cedetep/enfermagem/js/main.js') }}"></script>

</body>

</html>