
<!doctype html>
<html lang="pt-br">
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/uikit@3.5.5/dist/css/uikit.min.css" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/5.0.0-alpha1/css/bootstrap.min.css" integrity="sha384-r4NyP46KrjDleawBgD5tp8Y7UzmLA05oM1iAEQ17CSuDqnUK2+k9luXQOfXJCJ4I" crossorigin="anonymous">
    <link rel="stylesheet" href="https://supletivodombosco.com.br/sites-proprios-clientes/supletivodombosco/css/home.css?v=2">
    <link rel="stylesheet" href="https://supletivodombosco.com.br/css/util.css">

    <link rel="shortcut icon" href="https://supletivodombosco.com.br/sites-proprios-clientes/supletivodombosco/images/favicon.png">
    <link href="https://supletivodombosco.com.br/" rel="canonical">

    <meta name="robots" content="index, follow">
    <meta name="description" content="Vamos te ajudar a terminar os seus estudos por meio do Supletivo EAD - Concluir Ensino Médio à Distância com Facilidade e Agilidade.">
    <meta name="keywords" content="supletivo a distancia, ead, supletivo ensino medio, ensino medio, supletivo ead, terminar estudos, ensino a distancia, cursos, funesp, ensino medio ead">
    <title>Supletivo Dom Bosco - Termine seus Estudos</title>
    <meta name="facebook-domain-verification" content="nymf2se8s6fja38tnt39fatlauqb0c" />
    <meta name='author' content='WOZCODE | Pedro Henrique' />
    
    <meta property="og:locale" content="pt_BR">
    <meta property="og:type" content="website">
    <meta property="og:image" content="images/logo.png">
    <meta property="og:url" content="https://supletivodombosco.com.br/">
    <meta property="og:description" content="Vamos te ajudar a terminar os seus estudos por meio do Supletivo EAD - Concluir Ensino Médio à Distância com Facilidade e Agilidade.">
    <meta property="og:title" content="Supletivo Dom Bosco - Termine seus Estudos">

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-162358024-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-162358024-1');
    </script>

    <!-- Facebook Pixel Code -->
    <script>
      !function(f,b,e,v,n,t,s)
      {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
      n.callMethod.apply(n,arguments):n.queue.push(arguments)};
      if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
      n.queue=[];t=b.createElement(e);t.async=!0;
      t.src=v;s=b.getElementsByTagName(e)[0];
      s.parentNode.insertBefore(t,s)}(window, document,'script',
      'https://connect.facebook.net/en_US/fbevents.js');
      fbq('init', '594592098080334');
      fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
      src="https://www.facebook.com/tr?id=594592098080334&ev=PageView&noscript=1"
    /></noscript>

</head>
<body id="inicio">

   <header>

      <div uk-parallax="bgy: -250" class="home-header uk-preserve-color">
         <div class="d-none d-md-block">
            <div uk-sticky="animation: uk-animation-slide-top; sel-target: .uk-navbar-container; cls-active: uk-navbar-sticky; cls-inactive: uk-navbar-transparent uk-light; top: 550">
               <nav class="uk-navbar-container bg-white">
                  <div class="uk-container">
                     <div uk-navbar>
                        <ul class="uk-navbar-nav">
                           <li class="uk-active">
                              <a href="#inicio" uk-scroll>
                                 <img src="https://supletivodombosco.com.br/sites-proprios-clientes/supletivodombosco/images/logotipo.png" width="130" alt="Logotipo Supletivo Dom Bosco">
                              </a>
                           </li>
                        </ul>
                        <ul class="uk-navbar-nav ml-auto">
                           <li><a href="#inicio" uk-scroll="offset:40;" class="scroll-link">Início</a></li>
                           <li><a href="#supletivo" uk-scroll="offset:60;" class="scroll-link">Supletivo</a></li>
                           <li><a href="#quem-somos" uk-scroll="offset: 80;" class="scroll-link">Quem somos</a></li>
                           <li><a href="#contato" uk-scroll="offset: 50;" class="scroll-link">Fale Conosco</a></li>
                           <li><a href="https://colegioejabrasil.com.br/trilha/73/formulario/encceja-enem?toolbar=0?&unidade_id=131" target="_blank" class="scroll-link">Matricule-se</a></li>
                           <li><a href="https://aluno.colegioejabrasil.com.br/login" target="_blank" class="scroll-link"><i class="fa fa-user-graduate mr-1"></i> Área do Aluno</a></li>
                        </ul>
                     </div>
                  </div>
               </nav>
            </div>
         </div>
      </div>

     <nav class="navbar navbar-expand-lg navbar-light bg-white fixed-top d-md-none d-block shadow">
       <div class="container-fluid">
         <a class="navbar-brand" href="#inicio" uk-scroll>
           <img src="https://supletivodombosco.com.br/sites-proprios-clientes/supletivodombosco/images/logotipo.png" width="100" alt="Logotipo Supletivo Dom Bosco">
         </a>
         <button class="navbar-toggler border-0 outline-0" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
           <span class="fa fa-bars"></span>
         </button>
         <div class="collapse navbar-collapse pt-4" id="navbarSupportedContent">
           <ul class="navbar-nav mr-auto mb-2 mb-lg-0">
             <li class="nav-item">
               <a class="nav-link" href="#inicio" uk-scroll="offset: 55;" data-toggle="collapse" data-target="#navbarSupportedContent">Início</a>
             </li>
             <li class="nav-item">
               <a class="nav-link" href="#supletivo" uk-scroll="offset: 55;" data-toggle="collapse" data-target="#navbarSupportedContent">Supletivo</a>
             </li>
             <li class="nav-item">
               <a class="nav-link" href="#quem-somos" uk-scroll="offset: 55;" data-toggle="collapse" data-target="#navbarSupportedContent">Quem somos</a>
             </li>
             <li class="nav-item">
               <a class="nav-link" href="#contato" uk-scroll="offset: 55;" data-toggle="collapse" data-target="#navbarSupportedContent">Fale Conosco</a>
             </li>
             <li class="nav-item">
               <a class="nav-link" href="https://colegioejabrasil.com.br/trilha/73/formulario/encceja-enem?toolbar=0?&unidade_id=131">Matricule-se</a>
             </li>
             <li class="nav-item">
               <a class="nav-link" href="https://aluno.colegioejabrasil.com.br/login" target="_blank"><i class="fa fa-user-graduate mr-1"></i> Área do Aluno</a>
             </li>
           </ul>
         </div>
       </div>
     </nav>
   
   </header>

   <main class="overflow-hidden">

      <section class="position-absolute w-100" style="top: 10%;">
        <div class="uk-section uk-light">
           <div class="uk-container pt-lg-3">
              <div class="row align-items-center">

                <div class="col-lg-6">

                  <h1 class="fs-md-60 fs-xs-30 text-white text-shadow-3 font-nunito weight-800 pt-4">
                    Conclua seus Estudos
                  </h1>
                  <p class="mt-4 mb-0 font-nunito weight-800 fs-23 text-shadow-3 text-white ls-02">
                    Em poucos passos comece hoje mesmo a Estudar. Com Agilidade, Facilidade e sem Burocracia.
                  </p>
                  
                  <div class="mt-2 mt-lg-3">
                    <a href="https://colegioejabrasil.com.br/trilha/73/formulario/encceja-enem?toolbar=0?&unidade_id=131" target="_blank" class="myButton shadow rounded hover-d-none bg-base bg-base-hover">
                      QUERO ME MATRICULAR
                    </a>
                  </div>

                </div>

                <div class="col-lg-6 mt-5 mt-lg-0">

                  
                  <div class="card border-0 shadow">
                    <div class="card-header bg-base">
                        <p class="mb-0 font-nunito weight-800 text-center fs-md-30 fs-xs-20 color-light-085 text-shadow-1">
                          Dúvidas? Solicite em contato!
                        </p>
                    </div>
                    <div class="card-body bg-roxo">
                      <form method="POST" id="formInscreverSe">
                      <input type="hidden" name="_token" value="sk94Gc3ZPRSrcIIsj2XraGaVNfybUqeehhg5gkjt">                      
                        <div class="input-group mb-2">
                          <div class="input-group-prepend">
                            <div class="input-group-text rounded-right-0 bg-white" style="padding-right: 13.5px; padding-top: 17px;padding-bottom: 17px;">
                              <i class="fa fa-user"></i>
                            </div>
                          </div>
                          <input type="text" name="nome" class="form-control py-2-5 outline-0 rounded-2" placeholder="Seu Nome" required>
                        </div>

                        <div class="input-group mb-2">
                          <div class="input-group-prepend">
                            <div class="input-group-text rounded-right-0 bg-white" style="padding-top: 17px;padding-bottom: 17px;">
                              <i class="fa fa-phone"></i>
                            </div>
                          </div>
                          <input type="tel" name="celular" class="telefone rounded-left-0 form-control py-2-5 outline-0 rounded-2" placeholder="Telefone" required>
                        </div>

                        <div class="input-group mb-0">
                          <div class="input-group-prepend">
                            <div class="input-group-text rounded-right-0 bg-white" style="padding-top: 17px;padding-bottom: 17px;">
                              <i class="fa fa-envelope"></i>
                            </div>
                          </div>
                          <input type="email" name="email" class="rounded-left-0 form-control py-2-5 outline-0 rounded-2" placeholder="Email" required>
                        </div>

                        <input type="text" name="spamVerify" class="invisible h-0px" placeholder="Não preencha este campo.">

                        <input type="submit" name="inscrever" value="Solicitar Contato" class="w-100 border-0 rounded-3 bg-base bg-base-hover py-2 px-3 fs-18 font-raleway weight-600 text-white shadow">
                        
                      </form>
                    </div>
                  </div>
                  
                </div>  

              </div>
           </div> 
        </div>
      </section>

      <section class="pt-5-5 pb-5-5" id="como-funciona">
         <div class="container-fluid px-md-5 mt-xs-200">

            <h1 class="fs-40 text-center font-raleway text-dark ls-1 weight-800">
              COMO FUNCIONA
            </h1>

            <div class="flex-center mt-4-5 mb-4-5"><div class="w-80px h-2px bg-base rounded"></div></div>

            <p class="text-center text-dark text-shadow-1 font-opensans fs-19 mt-4 pb-5 col-md-8 offset-md-2">
              A Dom Bosco tem o ideal de promover a possibilidade de pessoas concluirem o Ensino Médio que acabou ficando pelo caminho. Termine agora seus estudos.
            </p>

            <div class="uk-child-width-1-4@m" uk-grid uk-scrollspy="cls: uk-animation-slide-bottom; target: .uk-card; delay: 300;">
               <div>
                  <div class="uk-card flex-center flex-column">
                    <div class="rounded-circle border-profile border-hover-base square-100 flex-center mb-3">
                      <i class="fa fa-desktop fs-35"></i>
                      <i class="fa fa-mobile-alt fs-35 ml-1"></i>
                    </div>
                    <h3 class="font-montserrat weight-500 color-base fs-18">Estudo 100% Online</h3>
                  </div>
               </div>
               <div>
                  <div class="uk-card flex-center flex-column">
                    <div class="rounded-circle border-profile border-hover-base square-100 flex-center mb-3">
                      <i class="fas fa-file-signature fs-50 ml-3"></i>
                    </div>
                    <h3 class="font-montserrat weight-500 color-base fs-18">Reconhecida Pelo MEC</h3>
                  </div>
               </div>
               <div>
                  <div class="uk-card flex-center flex-column">
                    <div class="rounded-circle border-profile border-hover-base square-100 flex-center mb-3">
                      <i class="far fa-clock fs-50"></i>
                    </div>
                    <h3 class="font-montserrat weight-500 color-base fs-18">Faça seu Próprio Horário</h3>
                  </div>
               </div>
               <div>
                  <div class="uk-card flex-center flex-column">
                    <div class="rounded-circle border-profile border-hover-base square-100 flex-center mb-3">
                      <i class="far fa-flag fs-50"></i>
                    </div>
                    <h3 class="font-montserrat weight-500 color-base fs-18">Validade Nacional</h3>
                  </div>
               </div>
            </div>

         </div>   
      </section>

      <section class="pt-5-5 pb-5-5 border-top bg-f5" id="supletivo">
         <div class="container-fluid px-xl-7 bg-supletivo">

            <h1 class="fs-40 text-center font-raleway text-dark ls-2 weight-800">
              O SUPLETIVO
            </h1>

            <div class="flex-center mt-4-5 mb-4-5"><div class="w-80px h-2px bg-base rounded"></div></div>

            <div class="row align-items-center">

              <div class="col-lg-5">

                <p class="font-opensans fs-17">
                  O principal objetivo do curso é avaliar as habilidades e competências básicas de jovens e adultos que não tiveram oportunidade de acesso à escolaridade regular na idade apropriada.
                </p>

                <p class="font-opensans fs-17">
                  Dessa forma, o participante se submete a uma prova e, alcançando a média mínima exigida, obtém a certificação de conclusão daquela etapa educacional.
                </p>

                <p class="font-opensans fs-17">
                  O exame também se propõe a oferecer às secretarias de Educação uma avaliação que lhes permita aferir os conhecimentos e habilidades dos participantes no nível de conclusão do Ensino Fundamental e do Ensino Médio.
                </p>

                <a href="#motivos" uk-scroll="offset: 40;" class="myButton shadow rounded hover-d-none bg-base bg-base-hover">
                  SABER MAIS
                </a>

              </div>
              
              <div class="col-lg-7" uk-scrollspy="cls: uk-animation-slide-right; target: #img; delay: 300;">
                <img src="https://supletivodombosco.com.br/sites-proprios-clientes/supletivodombosco/images/plataforma-elearning.png" alt="Supletivo FUNESP" id="img">
              </div>

            </div>

         </div>   
      </section>

      <section class="bg-white pt-5-5 pb-5 px-lg-5 px-1" id="motivos">
         <div class="container-fluid px-md-5 pb-lg-5">

            <h1 class="fs-40 text-center font-raleway text-dark weight-800 text-uppercase">
              Motivos para Terminar os Estudos
            </h1>

            <div class="flex-center mt-4-5 mb-4-5"><div class="w-80px h-2px bg-base rounded"></div></div>

            <div class="row pt-4">
              
               <div class="col-lg-6 mb-4 mb-lg-0">
                  
                  <p class="mb-4 text-center font-raleway color-base weight-800 text-uppercase fs-22">
                    COM ENSINO MÉDIO COMPLETO
                  </p>

                  <div class="d-flex flex-column">
                     
                     <div class="d-flex pr-lg-5 mb-4 mb-lg-0">
                        <div class="flex-column d-flex align-items-center">
                           <div class="bg-base square-55 rounded-circle shadow flex-center">
                            <i class="far fa-thumbs-up fs-22 text-white"></i>
                           </div>
                           <div class="d-none d-md-block">
                              <hr class="uk-divider-vertical border border-secondary h-lg-50 m-0 p-0">
                           </div>
                        </div>
                        <div class="ml-3 mt-lg-2-5">
                           <h1 class="display-7 weight-600 text-dark">
                              Oportunidades no Mercado de Trabalho
                           </h1>
                        </div>
                     </div>
                     <div class="d-flex pr-lg-5 mb-4 mb-lg-0">
                        <div class="flex-column d-flex align-items-center">
                           <div class="bg-base square-55 rounded-circle shadow flex-center">
                            <i class="far fa-thumbs-up fs-22 text-white"></i>
                           </div>
                           <div class="d-none d-md-block">
                              <hr class="uk-divider-vertical border border-secondary h-lg-50 m-0 p-0">
                           </div>
                        </div>
                        <div class="ml-3 mt-2-5">
                           <h1 class="display-7 weight-600 text-dark">
                              Iniciar uma Faculdade
                           </h1>
                        </div>
                     </div>
                     <div class="d-flex pr-lg-5 mb-4 mb-lg-0">
                        <div class="flex-column d-flex align-items-center">
                           <div class="bg-base square-55 rounded-circle shadow flex-center">
                            <i class="far fa-thumbs-up fs-22 text-white"></i>
                           </div>
                           <div class="d-none d-md-block">
                              <hr class="uk-divider-vertical border border-secondary h-lg-50 m-0 p-0">
                           </div>
                        </div>
                        <div class="ml-3 mt-lg-2-5">
                           <h1 class="display-7 weight-600 text-dark">
                              Iniciar um Curso Técnico
                           </h1>
                        </div>
                     </div>
                     <div class="d-flex pr-lg-5 mb-4 mb-lg-0">
                        <div class="flex-column d-flex align-items-center">
                           <div class="bg-base square-55 rounded-circle shadow flex-center">
                            <i class="far fa-thumbs-up fs-22 text-white"></i>
                           </div>
                           <div class="d-none d-md-block">
                              <hr class="uk-divider-vertical border border-secondary h-lg-50 m-0 p-0">
                           </div>
                        </div>
                        <div class="ml-3 mt-lg-2-5">
                           <h1 class="display-7 weight-600 text-dark">
                              Participar de concursos públicos
                           </h1>
                        </div>
                     </div>
                     <div class="d-flex pr-lg-5 mb-4 mb-lg-0">
                        <div class="flex-column d-flex align-items-center">
                           <div class="bg-base square-55 rounded-circle shadow flex-center">
                            <i class="far fa-thumbs-up fs-22 text-white"></i>
                           </div>
                           <div class="d-none d-md-block">
                              <hr class="uk-divider-vertical border border-secondary h-lg-50 m-0 p-0">
                           </div>
                        </div>
                        <div class="ml-3 mt-2-5">
                           <h1 class="display-7 weight-600 text-dark">
                              Realizar o ENEM
                           </h1>
                        </div>
                     </div>
                     <div class="d-flex pr-lg-5 mb-4 mb-lg-0">
                        <div class="flex-column d-flex align-items-center">
                           <div class="bg-base square-55 rounded-circle shadow flex-center">
                            <i class="far fa-thumbs-up fs-22 text-white"></i>
                           </div>
                           <div class="d-none d-md-block">
                              <hr class="uk-divider-vertical border border-secondary h-lg-50 m-0 p-0">
                           </div>
                        </div>
                        <div class="ml-3 mt-1">
                           <h1 class="display-7 weight-600 text-dark">
                              Crescer dentro de Empresas e melhora na Qualidade de Vida
                           </h1>
                        </div>
                     </div>
                     <div class="d-flex pr-lg-5 mb-4 mb-lg-0">
                        <div class="flex-column d-flex align-items-center">
                           <div class="bg-base square-55 rounded-circle shadow flex-center">
                            <i class="far fa-thumbs-up fs-22 text-white"></i>
                           </div>
                           <div class="d-none d-md-block">
                              <hr class="uk-divider-vertical border border-secondary h-lg-50 m-0 p-0">
                           </div>
                        </div>
                        <div class="ml-3 mt-lg-2-5">
                           <h1 class="display-7 weight-600 text-dark">
                              Oportunidades de Estudar no Exterior
                           </h1>
                        </div>
                     </div>
                     <div class="d-flex pr-lg-5 mb-4 mb-lg-0">
                        <div class="flex-column d-flex align-items-center">
                           <div class="bg-base square-55 rounded-circle shadow flex-center">
                            <i class="far fa-thumbs-up fs-22 text-white"></i>
                           </div>
                           <div class="d-none d-md-block">
                              <hr class="uk-divider-vertical border border-secondary h-lg-50 m-0 p-0">
                           </div>
                        </div>
                        <div class="ml-3 mt-lg-2-5">
                           <h1 class="display-7 weight-600 text-dark">
                              Conhecimento PARA TODA A VIDA!
                           </h1>
                        </div>
                     </div>
                     <div class="d-flex pr-lg-5 mb-4 mb-lg-0">
                        <div class="flex-column d-flex align-items-center">
                           <div class="bg-base square-55 rounded-circle shadow flex-center">
                            <i class="far fa-thumbs-up fs-22 text-white"></i>
                           </div>
                        </div>
                        <div class="ml-3 mt-2-5">
                           <h1 class="display-7 weight-600 text-dark">
                              E muito mais
                           </h1>
                        </div>
                     </div>

                  </div>

               </div>

               <div class="col-lg-6">

                  <p class="mb-4 ml-lg-5 text-xs-center font-raleway text-danger weight-800 text-uppercase fs-22">
                    SEM O ENSINO MÉDIO COMPLETO
                  </p>

                  <div class="d-flex flex-column">
                     
                     <div class="d-flex pr-lg-5 mb-4 mb-lg-0">
                        <div class="flex-column d-flex align-items-center">
                           <div class="bg-danger square-55 rounded-circle shadow flex-center">
                            <i class="far fa-thumbs-down fs-22 text-white"></i>
                           </div>
                           <div class="d-none d-md-block">
                              <hr class="uk-divider-vertical border border-secondary h-lg-50 m-0 p-0">
                           </div>
                        </div>
                        <div class="ml-3 mt-lg-2-5">
                           <h1 class="display-7 weight-600 text-dark">
                              Perde oportunidades de trabalho
                           </h1>
                        </div>
                     </div>
                     <div class="d-flex pr-lg-5 mb-4 mb-lg-0">
                        <div class="flex-column d-flex align-items-center">
                           <div class="bg-danger square-55 rounded-circle shadow flex-center">
                            <i class="far fa-thumbs-down fs-22 text-white"></i>
                           </div>
                           <div class="d-none d-md-block">
                              <hr class="uk-divider-vertical border border-secondary h-lg-50 m-0 p-0">
                           </div>
                        </div>
                        <div class="ml-3 mt-lg-2-5">
                           <h1 class="display-7 weight-600 text-dark">
                              Não poderá cursar Faculdade
                           </h1>
                        </div>
                     </div>
                     <div class="d-flex pr-lg-5 mb-4 mb-lg-0">
                        <div class="flex-column d-flex align-items-center">
                           <div class="bg-danger square-55 rounded-circle shadow flex-center">
                            <i class="far fa-thumbs-down fs-22 text-white"></i>
                           </div>
                           <div class="d-none d-md-block">
                              <hr class="uk-divider-vertical border border-secondary h-lg-50 m-0 p-0">
                           </div>
                        </div>
                        <div class="ml-3 mt-lg-2-5">
                           <h1 class="display-7 weight-600 text-dark">
                              Não poderá fazer Cursos Técnicos
                           </h1>
                        </div>
                     </div>
                     <div class="d-flex pr-lg-5 mb-4 mb-lg-0">
                        <div class="flex-column d-flex align-items-center">
                           <div class="bg-danger square-55 rounded-circle shadow flex-center">
                            <i class="far fa-thumbs-down fs-22 text-white"></i>
                           </div>
                           <div class="d-none d-md-block">
                              <hr class="uk-divider-vertical border border-secondary h-lg-50 m-0 p-0">
                           </div>
                        </div>
                        <div class="ml-3 mt-1">
                           <h1 class="display-7 weight-600 text-dark">
                              Não poderá participar de Concursos Públicos
                           </h1>
                        </div>
                     </div>
                     <div class="d-flex pr-lg-5 mb-4 mb-lg-0">
                        <div class="flex-column d-flex align-items-center">
                           <div class="bg-danger square-55 rounded-circle shadow flex-center">
                            <i class="far fa-thumbs-down fs-22 text-white"></i>
                           </div>
                           <div class="d-none d-md-block">
                              <hr class="uk-divider-vertical border border-secondary h-lg-50 m-0 p-0">
                           </div>
                        </div>
                        <div class="ml-3 mt-lg-2-5">
                           <h1 class="display-7 weight-600 text-dark">
                              Não pode realizar o ENEM
                           </h1>
                        </div>
                     </div>
                     <div class="d-flex pr-lg-5 mb-4 mb-lg-0">
                        <div class="flex-column d-flex align-items-center">
                           <div class="bg-danger square-55 rounded-circle shadow flex-center">
                            <i class="far fa-thumbs-down fs-22 text-white"></i>
                           </div>
                           <div class="d-none d-md-block">
                              <hr class="uk-divider-vertical border border-secondary h-lg-50 m-0 p-0">
                           </div>
                        </div>
                        <div class="ml-3 mt-1">
                           <h1 class="display-7 weight-600 text-dark">
                              Dificuldade no crescimento Pessoal e Profissional
                           </h1>
                        </div>
                     </div>
                     <div class="d-flex pr-lg-5 mb-4 mb-lg-0">
                        <div class="flex-column d-flex align-items-center">
                           <div class="bg-danger square-55 rounded-circle shadow flex-center">
                            <i class="far fa-thumbs-down fs-22 text-white"></i>
                           </div>
                           <div class="d-none d-md-block">
                              <hr class="uk-divider-vertical border border-secondary h-lg-50 m-0 p-0">
                           </div>
                        </div>
                        <div class="ml-3 mt-1">
                           <h1 class="display-7 weight-600 text-dark">
                              Perde a chance de crescer dentro de empresas
                           </h1>
                        </div>
                     </div>
                     <div class="d-flex pr-lg-5 mb-4 mb-lg-0">
                        <div class="flex-column d-flex align-items-center">
                           <div class="bg-danger square-55 rounded-circle shadow flex-center">
                            <i class="far fa-thumbs-down fs-22 text-white"></i>
                           </div>
                           <div class="d-none d-md-block">
                              <hr class="uk-divider-vertical border border-secondary h-lg-50 m-0 p-0">
                           </div>
                        </div>
                        <div class="ml-3 mt-lg-2-5">
                           <h1 class="display-7 weight-600 text-dark">
                              Não obtém conhecimento
                           </h1>
                        </div>
                     </div>
                     <div class="d-flex pr-lg-5 mb-4 mb-lg-0">
                        <div class="flex-column d-flex align-items-center">
                           <div class="bg-danger square-55 rounded-circle shadow flex-center">
                            <i class="far fa-thumbs-down fs-22 text-white"></i>
                           </div>
                        </div>
                        <div class="ml-3 mt-lg-2-5">
                           <h1 class="display-7 weight-600 text-dark">
                              Sem os estudos, não conseguirá avançar
                           </h1>
                        </div>
                     </div>

                  </div>

               </div>

            </div>

         </div>
      </section>

      <section id="faq" class="py-5 bg-f9 border-top">
         <div class="container-fluid px-xl-7 pb-3">

            <h1 class="fs-40 text-center font-raleway text-dark ls-1 weight-800">DÚVIDAS FREQUENTES</h1>
            <div class="flex-center mt-4 mb-4"><div class="w-80px h-2px bg-base rounded"></div></div>

            <div class="accordion px-md-5" id="accordionExample">

              <div class="card shadow border-bottom">
                <div class="card-header bg-white border-0">
                    <a href="#" class="w-100 text-d-none fs-20 color-blue py-1" type="button" data-toggle="collapse" data-target="#collapseFaq1">
                      Qual o prazo para entrega de documentos?
                    </a>
                </div>
                <div id="collapseFaq1" class="collapse">
                  <div class="card-body pt-0">
                    <p class="mb-0 font-opensans weight-600 color-51">
                      O prazo máximo de 180 dias para entrega do certificado devidamente registrado na secretaria de educação. Obs.: os documentos serão emitidos no momento em que o aluno estiver em dia com suas obrigações com a escola, a saber: pagamentos, documentação, e aprovação em todas as disciplinas.
                    </p>
                  </div>
                </div>
              </div>
              <div class="card shadow border-bottom">
                <div class="card-header bg-white border-0">
                    <a href="#" class="w-100 text-d-none fs-20 color-blue py-1" type="button" data-toggle="collapse" data-target="#collapseFaq2">
                      Quais são os documentos exigidos?
                    </a>
                </div>
                <div id="collapseFaq2" class="collapse">
                  <div class="card-body pt-0">
                    <p class="mb-0 font-opensans weight-600 color-51">
                      Ficha de matrícula preenchida de próprio punho, 02 fotos 3×4, 02 cópias do comprovante de endereço, 02 cópias do RG, CPF e certidão de nascimento/casamento, e 01 cópia autenticada do histórico escolar, 02 cópias do Título de Eleitor, e para homens 02 cópias da Reservista e ficha de matrícula preenchida de próprio punho.
                    </p>
                  </div>
                </div>
              </div>
              <div class="card shadow border-bottom">
                <div class="card-header bg-white border-0">
                    <a href="" class="w-100 text-d-none fs-20 color-blue py-1" type="button" data-toggle="collapse" data-target="#collapseFaq3">
                      Quando o aluno pode começar a fazer as provas?
                    </a>
                </div>
                <div id="collapseFaq3" class="collapse">
                  <div class="card-body pt-0">
                    <p class="mb-0 font-opensans weight-600 color-51">
                      Assim que o aluno se sentir preparado, quanto mais rápido fazer as provas, mais rápido sai a documentação.
                    </p>
                  </div>
                </div>
              </div>
              <div class="card shadow border-bottom">
                <div class="card-header bg-white border-0">
                    <a href="" class="w-100 text-d-none fs-20 color-blue py-1" type="button" data-toggle="collapse" data-target="#collapseFaq4">
                      O que você ganha?
                    </a>
                </div>
                <div id="collapseFaq4" class="collapse">
                  <div class="card-body pt-0">
                    <p class="mb-0 font-opensans weight-600 color-51">
                      Com o Curso EJA Ensino Médio você cursa todo o Ensino Médio e obtém um Certificado de Conclusão podendo dar continuidade aos seus estudos em nível técnico ou superior. Veja os Credenciamentos no Programa.
                    </p>
                  </div>
                </div>
              </div>
              <div class="card shadow border-bottom">
                <div class="card-header bg-white border-0">
                    <a href="" class="w-100 text-d-none fs-20 color-blue py-1" type="button" data-toggle="collapse" data-target="#collapseFaq5">
                      Legislação
                    </a>
                </div>
                <div id="collapseFaq5" class="collapse">
                  <div class="card-body pt-0">
                    <p class="mb-0 font-opensans weight-600 color-51">
                      A DOM BOSCO oferece curso preparatório para exames supletivo amparado pela Deliberação do CONSELHO ESTADUAL DE EDUCAÇÃO 114-2012 DOE 07/08/2012. O método de supletivo à distância, assegurado pela Lei Federal 9394/96, garante à população o direito à educação. Desta forma os certificados emitidos por colégios autorizados pelo governo são publicados em Diário Oficial, validando-os para qualquer finalidade, como ingressar em uma faculdade, prestar concursos públicos, realizar cursos técnicos, participar de processos seletivos em empresas que solicitem tal escolaridade, entre outros.
                    </p>
                  </div>
                </div>
              </div>

            </div>

         </div>
      </section>

      <section class="py-5 bg-termine">
         <div class="container">
            <div class="row align-items-center">
            
               <div class="col-lg-8 mb-3 mb-lg-0">
                  <div>
                     <p class="mb-0 fs-md-55 fs-xs-35 weight-900 font-raleway text-shadow-1 text-light">
                       Termine seus Estudos
                     </p>
                     <p class="mb-0 weight-600 fs-22 text-light text-shadow-1 mr-lg-5">
                        Aproveite as vantagens que o EAD proporciona em sua vida e rotina. Matrícula 100% Online.
                     </p>
                  </div>
               </div>
               <div class="col-lg-4">
                  <div uk-scrollspy="cls: uk-animation-slide-top; target: #btnMatricula; delay: 300;">
                     <a href="https://colegioejabrasil.com.br/trilha/73/formulario/encceja-enem?toolbar=0?&unidade_id=131" target="_blank" class="myButton w-100 shadow rounded hover-d-none bg-base bg-base-hover text-center py-4" id="btnMatricula">
                        QUERO FAZER MINHA MATRÍCULA!
                     </a>
                  </div>
               </div>

            </div>
         </div>
      </section>

      <section id="quem-somos" class="py-5 bg-blue">
         <div class="container-fluid px-md-5 pb-5 pt-2">

            <h1 class="fs-40 text-center font-raleway text-light ls-2 weight-800">QUEM SOMOS</h1>
            <div class="flex-center mt-4 mb-4"><div class="w-80px h-2px bg-base rounded"></div></div>

            <p class="text-center text-shadow-1 text-light font-opensans fs-20 mt-4 mb-5 px-xl-7">
              Conheça mais um pouco sobre a história da Bom Dosco
            </p>
            
            <div class="row align-items-center">
              
              <div class="col-lg-6" uk-scrollspy="cls: uk-animation-fade; target: #img2; delay: 300;">
                <img id="img2" class="img-fluid" width="500" src="https://supletivodombosco.com.br/sites-proprios-clientes/supletivodombosco/images/plataforma-elearning2.png" alt="Quem Somos Supletivo Dom Bosco">
              </div>
              <div class="col-lg-5 mt-4 mt-lg-0">
                <p class="font-opensans fs-17 text-light">
                  A Dom Bosco está no mercado de ensino a distância e já preparou centenas de alunos, prestando-lhes um atendimento personalizado, visando sempre a conclusão dos seus objetivos, de acordo com a comodidade e a disponibilidade de cada um. Temos o compromisso de atender o objetivo que o aluno pretende alcançar, com responsabilidade e segurança.
                </p>
                <p class="font-opensans fs-17 text-light">
                  Nossa instituição atua em todo o território nacional, tornando possível o término dos estudos em mais de 1612 cidades, nos 26 Estados Brasileiros e no Distrito Federal. Atuamos de acordo com as principais políticas de certificação do Ensino Fundamental e Ensino Médio vigentes no Brasil para oferecer o que há de mais acessível para nossos alunos.
                </p>
              </div>

            </div>

         </div>
      </section>

      <section class="py-5 bg-fc" id="contato">
         <div class="container-fluid px-xl-7 pt-lg-5">

            <h1 class="fs-40 text-center font-raleway text-dark weight-800">FALE CONOSCO</h1>
            <div class="flex-center mt-4-5 mb-4-5"><div class="w-80px h-2px bg-base rounded"></div></div>

            <div class="row py-lg-4 px-3 px-md-0">

               <div class="col-lg-6">

                  <div class="mb-4">
                    <p class="mb-0 font-opensans weight-600 color-blue-weak d-flex align-items-center">
                      <i class="far fa-clock fs-35 color-blue-weak"></i>
                      <span class="ml-3">HORÁRIO DE ATENDIMENTO</span>
                    </p>
                    <p class="mt-1 mt-lg-0 mb-0 font-opensans weight-600 fs-15 color-ccc ml-lg-5">
                      &nbsp;Segunda à Sexta, 09h às 18h
                    </p>
                  </div>

                  <div class="mb-4">
                    <p class="mb-0 font-opensans weight-600 color-blue-weak d-flex align-items-center">
                      <i class="fa fa-envelope fs-35 color-blue-weak"></i>
                      <span class="ml-3">EMAIL </span>
                    </p>
                    <p class="mt-1 mt-lg-0 mb-0 font-opensans weight-600 fs-15 color-ccc ml-lg-5">
                      &nbsp;ensinodomboscoead@gmail.com
                    </p>
                  </div>

                  <div class="mb-4">
                    <p class="mb-0 font-opensans weight-600 color-blue-weak d-flex align-items-center">
                      <i class="fab fa-whatsapp fs-40 color-blue-weak"></i>
                      <span class="ml-3">CONTATO</span>
                    </p>
                    <div class="mt-1 mt-lg-0 mb-0 font-opensans weight-600 fs-15 color-ccc ml-lg-5">
                      <p class="mb-0">
                        Secretaria:
                        <a href="https://api.whatsapp.com/send?phone=5517988427187&text=Ol%C3%A1,%20vim%20atrav%C3%A9s%20do%20site%20da%20Supletivo Dom Bosco." target="_blank" class="text-d-none color-blue-weak">(17) 988427187</a>
                      </p>
                      <p class="mb-0">
                        Financeiro:
                        <a href="https://api.whatsapp.com/send?phone=5517988445276&text=Ol%C3%A1,%20vim%20atrav%C3%A9s%20do%20site%20da%20Supletivo Dom Bosco." target="_blank" class="text-d-none color-blue-weak">(17) 98844-5276</a>
                      </p>
                      <p class="mb-0">
                        Comercial: 
                        <a href="https://api.whatsapp.com/send?phone=5517988439246&text=Ol%C3%A1,%20vim%20atrav%C3%A9s%20do%20site%20da%20Supletivo Dom Bosco." target="_blank" class="text-d-none color-blue-weak">(17) 98843-9246</a>
                      </p>
                    </div>
                  </div>

                  <div class="mb-4">
                    <p class="mb-0 font-opensans weight-600 color-blue-weak d-flex align-items-center">
                      <i class="fa fa-phone fs-40 color-blue-weak"></i>
                      <span class="ml-3">TELEFONE</span>
                    </p>
                    <div class="mt-1 mt-lg-0 mb-0 font-opensans weight-600 fs-15 color-ccc ml-lg-5">
                      <p class="mb-0">
                         &nbsp;&nbsp;Administrativo:
                        <a href="tel:+551733046681" target="_blank" class="text-d-none color-blue-weak">(17) 3304-6681</a>
                      </p>
                    </div>
                  </div>

                  <div class="mb-3">
                    <p class="mb-0 font-opensans weight-600 color-blue-weak d-flex align-items-center">
                      <i class="fa fa-map-marker-alt fs-40 color-blue-weak"></i>
                      <span class="ml-3">ENDEREÇO</span>
                    </p>
                    <address class="mt-1 mt-lg-0 ml-lg-5 mt-0 font-opensans weight-600 fs-15 color-ccc">
                      R. Voluntários de São Paulo, 3066 - Centro.<br>
                      São José do Rio Preto - SP<br>
                      CEP: 15015-200, SL: 407
                    </address>
                  </div>

               </div>

               <div class="col-lg-6 mt-2">
                <form method="POST">
                <input type="hidden" name="_token" value="sk94Gc3ZPRSrcIIsj2XraGaVNfybUqeehhg5gkjt">
                  
                  <input type="text" name="nome" class="form-control py-2-5 outline-0 rounded-2" placeholder="Seu Nome" required>
                  <input type="tel" name="celular" class="my-2 telefone form-control py-2-5 outline-0 rounded-2" placeholder="Telefone" required>
                  <input type="email" name="email" class="rounded-left-0 form-control py-2-5 outline-0 rounded-2" placeholder="Email" required>
                  <textarea name="mensagem" placeholder="Mensagem" class="mt-2 form-control h-min-100 w-100 outline-0 rounded-2" required></textarea>
                  <input type="text" name="spamVerify" class="invisible h-0px" placeholder="Não preencha este campo.">
                  <input type="submit" value="Enviar" name="btnSend" class="w-100 border-0 rounded-3 bg-base bg-base-hover py-2 px-3 fs-18 font-raleway weight-600 text-white shadow">

                </form>
               </div>
               
            </div>
         
         </div>
      </section>

      <a href="https://api.whatsapp.com/send?phone=5517988439246&text=Ol%C3%A1,%20vim%20atrav%C3%A9s%20do%20site%20da%20Supletivo Dom Bosco." target="_blank" class="text-white fs-30">
          <div class="p-fixed bottom-0 left-0 ml-3 mb-3 z-3">
              <div class="square-60 rounded-circle bg-success flex-center shadow">
                  <i class="fab fa-whatsapp"></i>
              </div>
          </div>
      </a>

   </main>
  
   <footer>
       <div class="bg-blue py-5">
          <div class="container">
              <div class="row">

                <div class="col-lg-4 mb-4 mb-lg-0">
                  <h5 class="mb-lg-4 text-light fs-17">Fã Page Facebook</h5>
                  <hr class="border-light border" />

                  <script async defer crossorigin="anonymous" src="https://connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v7.0"></script>
                  <div class="fb-page w-100" data-href="https://www.facebook.com/supletivodombosco/" data-tabs="timeline" data-height="303" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/supletivodombosco/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/supletivodombosco/">Supletivo Dom Bosco</a></blockquote></div>

                </div>

                <div class="col-lg-4 mb-4 mb-lg-0">
                  <h5 class="mb-lg-4 text-light fs-17">Instagram</h5>
                  <hr class="border-light border" />
                  <blockquote class="instagram-media h-lg-300" data-instgrm-captioned data-instgrm-permalink="https://www.instagram.com/p/CGbPvHRD8M1/?utm_source=ig_embed&amp;utm_campaign=loading" data-instgrm-version="13" style=" background:#FFF; border:0; border-radius:3px; box-shadow:0 0 1px 0 rgba(0,0,0,0.5),0 1px 10px 0 rgba(0,0,0,0.15); margin: 1px; max-width:540px; min-width:326px; padding:0; width:99.375%; width:-webkit-calc(100% - 2px); width:calc(100% - 2px);"><div style="padding:16px;"> <a href="https://www.instagram.com/p/CGbPvHRD8M1/?utm_source=ig_embed&amp;utm_campaign=loading" style=" background:#FFFFFF; line-height:0; padding:0 0; text-align:center; text-decoration:none; width:100%;" target="_blank"> <div style=" display: flex; flex-direction: row; align-items: center;"> <div style="background-color: #F4F4F4; border-radius: 50%; flex-grow: 0; height: 40px; margin-right: 14px; width: 40px;"></div> <div style="display: flex; flex-direction: column; flex-grow: 1; justify-content: center;"> <div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; margin-bottom: 6px; width: 100px;"></div> <div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; width: 60px;"></div></div></div><div style="padding: 19% 0;"></div> <div style="display:block; height:50px; margin:0 auto 12px; width:50px;"><svg width="50px" height="50px" viewBox="0 0 60 60" version="1.1" xmlns="https://www.w3.org/2000/svg" xmlns:xlink="https://www.w3.org/1999/xlink"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g transform="translate(-511.000000, -20.000000)" fill="#000000"><g><path d="M556.869,30.41 C554.814,30.41 553.148,32.076 553.148,34.131 C553.148,36.186 554.814,37.852 556.869,37.852 C558.924,37.852 560.59,36.186 560.59,34.131 C560.59,32.076 558.924,30.41 556.869,30.41 M541,60.657 C535.114,60.657 530.342,55.887 530.342,50 C530.342,44.114 535.114,39.342 541,39.342 C546.887,39.342 551.658,44.114 551.658,50 C551.658,55.887 546.887,60.657 541,60.657 M541,33.886 C532.1,33.886 524.886,41.1 524.886,50 C524.886,58.899 532.1,66.113 541,66.113 C549.9,66.113 557.115,58.899 557.115,50 C557.115,41.1 549.9,33.886 541,33.886 M565.378,62.101 C565.244,65.022 564.756,66.606 564.346,67.663 C563.803,69.06 563.154,70.057 562.106,71.106 C561.058,72.155 560.06,72.803 558.662,73.347 C557.607,73.757 556.021,74.244 553.102,74.378 C549.944,74.521 548.997,74.552 541,74.552 C533.003,74.552 532.056,74.521 528.898,74.378 C525.979,74.244 524.393,73.757 523.338,73.347 C521.94,72.803 520.942,72.155 519.894,71.106 C518.846,70.057 518.197,69.06 517.654,67.663 C517.244,66.606 516.755,65.022 516.623,62.101 C516.479,58.943 516.448,57.996 516.448,50 C516.448,42.003 516.479,41.056 516.623,37.899 C516.755,34.978 517.244,33.391 517.654,32.338 C518.197,30.938 518.846,29.942 519.894,28.894 C520.942,27.846 521.94,27.196 523.338,26.654 C524.393,26.244 525.979,25.756 528.898,25.623 C532.057,25.479 533.004,25.448 541,25.448 C548.997,25.448 549.943,25.479 553.102,25.623 C556.021,25.756 557.607,26.244 558.662,26.654 C560.06,27.196 561.058,27.846 562.106,28.894 C563.154,29.942 563.803,30.938 564.346,32.338 C564.756,33.391 565.244,34.978 565.378,37.899 C565.522,41.056 565.552,42.003 565.552,50 C565.552,57.996 565.522,58.943 565.378,62.101 M570.82,37.631 C570.674,34.438 570.167,32.258 569.425,30.349 C568.659,28.377 567.633,26.702 565.965,25.035 C564.297,23.368 562.623,22.342 560.652,21.575 C558.743,20.834 556.562,20.326 553.369,20.18 C550.169,20.033 549.148,20 541,20 C532.853,20 531.831,20.033 528.631,20.18 C525.438,20.326 523.257,20.834 521.349,21.575 C519.376,22.342 517.703,23.368 516.035,25.035 C514.368,26.702 513.342,28.377 512.574,30.349 C511.834,32.258 511.326,34.438 511.181,37.631 C511.035,40.831 511,41.851 511,50 C511,58.147 511.035,59.17 511.181,62.369 C511.326,65.562 511.834,67.743 512.574,69.651 C513.342,71.625 514.368,73.296 516.035,74.965 C517.703,76.634 519.376,77.658 521.349,78.425 C523.257,79.167 525.438,79.673 528.631,79.82 C531.831,79.965 532.853,80.001 541,80.001 C549.148,80.001 550.169,79.965 553.369,79.82 C556.562,79.673 558.743,79.167 560.652,78.425 C562.623,77.658 564.297,76.634 565.965,74.965 C567.633,73.296 568.659,71.625 569.425,69.651 C570.167,67.743 570.674,65.562 570.82,62.369 C570.966,59.17 571,58.147 571,50 C571,41.851 570.966,40.831 570.82,37.631"></path></g></g></g></svg></div><div style="padding-top: 8px;"> <div style=" color:#3897f0; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:550; line-height:18px;"> View this post on Instagram</div></div><div style="padding: 12.5% 0;"></div> <div style="display: flex; flex-direction: row; margin-bottom: 14px; align-items: center;"><div> <div style="background-color: #F4F4F4; border-radius: 50%; height: 12.5px; width: 12.5px; transform: translateX(0px) translateY(7px);"></div> <div style="background-color: #F4F4F4; height: 12.5px; transform: rotate(-45deg) translateX(3px) translateY(1px); width: 12.5px; flex-grow: 0; margin-right: 14px; margin-left: 2px;"></div> <div style="background-color: #F4F4F4; border-radius: 50%; height: 12.5px; width: 12.5px; transform: translateX(9px) translateY(-18px);"></div></div><div style="margin-left: 8px;"> <div style=" background-color: #F4F4F4; border-radius: 50%; flex-grow: 0; height: 20px; width: 20px;"></div> <div style=" width: 0; height: 0; border-top: 2px solid transparent; border-left: 6px solid #f4f4f4; border-bottom: 2px solid transparent; transform: translateX(16px) translateY(-4px) rotate(30deg)"></div></div><div style="margin-left: auto;"> <div style=" width: 0px; border-top: 8px solid #F4F4F4; border-right: 8px solid transparent; transform: translateY(16px);"></div> <div style=" background-color: #F4F4F4; flex-grow: 0; height: 12px; width: 16px; transform: translateY(-4px);"></div> <div style=" width: 0; height: 0; border-top: 8px solid #F4F4F4; border-left: 8px solid transparent; transform: translateY(-4px) translateX(8px);"></div></div></div> <div style="display: flex; flex-direction: column; flex-grow: 1; justify-content: center; margin-bottom: 24px;"> <div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; margin-bottom: 6px; width: 224px;"></div> <div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; width: 144px;"></div></div></a><p style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; line-height:17px; margin-bottom:0; margin-top:8px; overflow:hidden; padding:8px 0 7px; text-align:center; text-overflow:ellipsis; white-space:nowrap;"><a href="https://www.instagram.com/p/CGbPvHRD8M1/?utm_source=ig_embed&amp;utm_campaign=loading" style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:normal; line-height:17px; text-decoration:none;" target="_blank">A post shared by domboscoead (@ensinodomboscoead)</a></p></div></blockquote> <script async src="//www.instagram.com/embed.js"></script>
                </div>

                <div class="col-lg-4 mb-4 mb-lg-0">
                  <h5 class="mb-lg-4 text-light fs-17">Endereço</h5>
                  <hr class="border-light border" />
                  <iframe class="w-100 h-lg-300" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14917.933204447498!2d-49.3807758!3d-20.8121815!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xf998f12a1d91310f!2sSupletivo%20Dom%20Bosco%20EaD!5e0!3m2!1spt-BR!2sbr!4v1605151289955!5m2!1spt-BR!2sbr" aria-label="Supletivo Dom Bosco" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                </div>
                
              </div>
          </div>
       </div>
       <div class="bg-f9 py-2">
          <div class="container">
            <p class="font-roboto fs-13 weight-600 text-center mb-0 color-copyright">
                2020 &copy; <a href="https://wozcode.com" class="color-copyright hover-d-none text-d-none color-hover-copyright" target="_blank">
                Supletivo Dom Bosco • Todos os direitos reservados • CNPJ: 22.367.647/0001-99
              </a>
            </p>
          </div>
       </div>
   </footer>

   <script src="https://kit.fontawesome.com/2f91eaaef4.js" crossorigin="anonymous"></script> 
   <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.slim.js" integrity="sha512-1lagjLfnC1I0iqH9plHYIUq3vDMfjhZsLy9elfK89RBcpcRcx4l+kRJBSnHh2Mh6kLxRHoObD1M5UTUbgFy6nA==" crossorigin="anonymous"></script>
   <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
   <script src="https://cdn.jsdelivr.net/npm/uikit@3.5.5/dist/js/uikit.min.js"></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.js"></script>
   <script>

    $(document).on("focus", ".telefone", function() { 
      jQuery(this)
          .mask("(99) 9999-99999")
          .change(function (event) {  
              target = (event.currentTarget) ? event.currentTarget : event.srcElement;  
              phone = target.value.replace(/\D/g, '');
              element = $(target);  
              element.unmask();  
              if(phone.length > 10) {  
                  element.mask("(99) 99999-9999");  
              } else {  
                  element.mask("(99) 9999-9999?9");  
              }  
      });
    });

   </script>
   

</body>
</html>