<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAclPlataformasModulosUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('acl_plataformas_modulos_users', function (Blueprint $table) {
            $table->id();
            $table->integer('plataforma_id');
            $table->integer('user_id');
            $table->integer('modulo_id');
            $table->integer('nivel')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('acl_plataformas_modulos_users');
    }
}
