@extends('layouts.admin.master')

@section('content')
    
    @livewire('admin.turma.info', ['turma_id' => $turma_id])

@endsection