<?php

namespace App\Http\Livewire\Admin\Turma;

use Livewire\Component;
use App\Models\Turma;
use App\Models\Curso;
use App\Models\Formulario;
use App\Models\Unidade;
use Illuminate\Support\Facades\Http;

class Add extends Component
{
    public $turma;
    public $curso_id;
    public $formulario_id;
    public $unidade_id;
    public $nome;
    public $status = 0;
    public $tipo_inscricao = 'requer-aprovacao';
    public $publicar = 'N';
    public $modalidade;
    public $data_inicio;
    public $data_termino;
    public $frequencia = [];
    public $horario;
    public $duracao;
    public $valor_parcelado;
    public $valor_a_vista;
    public $nota_minima_aprovacao;

    public $controlBoxApresentacao = 'none';
    public $controlBoxPresencial   = 'none';

    public function add()
    {
        $frequencia = null;

        if($this->frequencia)
            $frequencia = implode(', ', $this->frequencia);

        $this->turma = Turma::create([
            'curso_id'              => $this->curso_id,
            'formulario_id'         => $this->formulario_id,
            'unidade_id'            => $this->unidade_id,
            'nome'                  => $this->nome,
            'status'                => $this->status,
            'publicar'              => $this->publicar,
            'tipo_inscricao'        => $this->tipo_inscricao,
            'modalidade'            => $this->modalidade,
            'data_inicio'           => $this->data_inicio,
            'data_termino'          => $this->data_termino,
            'frequencia'            => $frequencia,
            'horario'               => $this->horario,
            'duracao'               => $this->duracao,
            'valor_parcelado'       => $this->valor_parcelado,
            'valor_a_vista'         => $this->valor_a_vista,
            'nota_minima_aprovacao' => $this->nota_minima_aprovacao,
        ]);

        session()->flash('success-livewire', 'Turma criada com êxito.');
        
        $this->resetFields();

        $this->dispatchBrowserEvent('toast');
    }

    public function resetFields()
    {
        $this->curso_id = null;
        $this->formulario_id = null;
        $this->unidade_id = null;
        $this->nome = null;
        $this->status = 0;
        $this->tipo_inscricao = 'requer-aprovacao';
        $this->publicar = 'N';
        $this->modalidade = null;
        $this->data_inicio = null;
        $this->data_termino = null;
        $this->frequencia = [];
        $this->horario = null;
        $this->duracao = null;
        $this->valor_parcelado = null;
        $this->valor_a_vista = null;
        $this->nota_minima_aprovacao = null;
        $this->controlBoxApresentacao = 'none';
        $this->controlBoxPresencial   = 'none';
    }

    public function render()
    {
        $cursos       = Curso::plataforma()->ativo()->orderBy('referencia')->get();
        $formularios  = Formulario::plataforma()->ativo()->orderBy('referencia')->get();
        $unidades     = Unidade::plataforma()->ativo()->orderBy('nome')->get();

        return view('livewire.admin.turma.add', 
            [
                'cursos'       => $cursos,
                'formularios'  => $formularios,
                'unidades'     => $unidades,
            ] 
        );
    }

    public function changeEventPublicar($value)
    {
        if($value == 'S')
            $this->controlBoxApresentacao = 'block';
        else
            $this->controlBoxApresentacao = 'none';
    }

    public function changeEventModalidade($value)
    {
        if($value == 'EaD' || $value == '')
            $this->controlBoxPresencial = 'none';
        else
            $this->controlBoxPresencial = 'block';
    }

}
