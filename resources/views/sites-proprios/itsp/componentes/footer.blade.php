<footer id="contato" style="background-image: url('{{ asset('sites-proprios/cedetep/images/footer.jpg') }}'); background-repeat: no-repeat; background-size: cover; background-position: center;">
   <div class="container-fluid px-xl-8 pt-5 ps-lg-5" style="margin-top: -50px;">

      <div class="row align-items-end py-lg-4 px-3 px-md-0">

         <div class="col-lg-9">

            <h1 class="fs-35 font-montserrat text-light weight-800 text-uppercase">FALE CONOSCO</h1>
            <div class="my-4 w-80px h-2px bg-line rounded"></div>

            <img src="{{ $plataforma->logotipo }}" class="img-fluid" width="100">

            <div class="mt-2">                 

               <div class="mb-3">
                  <span class="text-light font-poppins fs-14">
                     <i class="color-d4 fa fa-envelope"></i> 
                     {{ $plataforma->email ?? 'meuemail@exemlo.com' }}
                  </span>
               </div>
               <div class="mb-3">
                  <a href="https://api.whatsapp.com/send?phone=55{{preg_replace('/\D/', '', $plataforma->whatsapp)}}&text=Olá, vim através do Site {{ $plataforma->nome }}" class="text-light font-poppins fs-14">
                     <i class="color-d4 fab fa-whatsapp"></i> 
                     {{ $plataforma->whatsapp ?? '(00) 00000-0000' }}
                  </a>
               </div>
               <div class="mb-3">
                  <span class="text-light font-poppins fs-14">
                     <i class="color-d4 fa fa-clock"></i> 
                     Horário de Funcionamento: {{ $plataforma->horario ?? 'Segunda a sexta-feira das 09:00 às 18:00' }}
                  </span>
               </div> 
               <div class="mb-3">
                 <address>
                   <a href="" target="_blank" class="btnEnderecoSiteCedetep text-light font-poppins fs-14">
                        <i class="color-d4 fas fa-map-marker-alt"></i>
                        {{ $plataforma->endereco ?? 'Pintangueiras - RJ. Rua Nossa Senhora das Graças, 16. 21932-230.' }}
                   </a>
                  </address>
               </div>

               <div class="mt-4 d-flex">

                  <a href="{{ $plataforma->facebook }}" target="_blank" class="me-2 hover-d-none">
                     <div class="square-40 rounded-circle social-icon social-icon-facebook flex-center">
                        <i class="text-white fab fa-facebook fs-20"></i>
                     </div>
                  </a>
                  <a href="{{ $plataforma->instagram }}" target="_blank" class="hover-d-none">
                     <div class="square-40 rounded-circle social-icon social-icon-instagram flex-center">
                        <i class="text-white fab fa-instagram fs-20"></i>
                     </div>
                  </a>

               </div>

            </div>

         </div>

         <div class="col-lg-3 mt-5 mt-lg-0">

            <div class="mb-3">
               <a @if(isset($curso)) href="/f/formulario-de-matricula/47/t/{{$curso->id}}" @else href="https://itspensino.com.br/f/formulario-de-matricula/47/t/650" @endif target="_blank" class="btn btn-primary weight-600 w-100 py-2-5 font-poppins hover-d-none">
                  MATRICULE-SE JÁ!
               </a>
            </div>
            <div class="mb-3">
               <a href="/login" class="btn btn-warning weight-600 w-100 py-2-5 font-poppins hover-d-none">
                  ÁREA DO ALUNO
               </a>
            </div>
            <div class="mb-3">
               <a href="https://itspensino.com.br/f/torne-se-parceiro/48/t/651" target="_blank" class="btn btn-light weight-600 w-100 py-2-5 font-poppins hover-d-none">
                  TORNE-SE PARCEIRO
               </a>
            </div>
            <div class="mb-3">
               <a href="https://api.whatsapp.com/send?phone=55{{preg_replace('/\D/', '', $plataforma->whatsapp)}}&text=Olá, vim através do Site {{ $plataforma->nome }}" class="btn btn-success bg-wpp text-shadow-1 weight-600 w-100 py-2-5 font-poppins hover-d-none">
                  ENTRAR EM CONTATO
               </a>
            </div>

            <div class="mb-3">
               <a href="/editais" class="btn btn-light weight-600 w-100 py-2-5 font-poppins hover-d-none">
                  EDITAIS
               </a>
            </div>

            <div class="mb-3">
               <a href="/sobre-nos" class="btn btn-light weight-600 w-100 py-2-5 font-poppins hover-d-none">
                  INSTITUCIONAL
               </a>
            </div>
            
         </div>

      </div>

      <hr class="mt-4 border border-light" />

      <div class="pb-3 d-lg-flex justify-content-between align-items-center">

         <p class="text-xs-center mb-1 mb-lg-0 p-0 text-light font-poppins fs-11 ls-05">
            {{ $plataforma->nome }} © 2022 • Todos os direitos reservados • CNPJ: {{ $plataforma->cnpj }}
         </p>

         <p class="text-xs-center m-0 p-0 text-light font-poppins fs-11 ls-05">
            Desenvolvido por <a href="https://wozcode.com" target="_blank" class="text-light text-d-none fs-11 weight-600 text-shadow-1">WOZCODE</a>
         </p>

      </div>

   </div>
</footer>