<?php

namespace App\Http\Livewire\Admin\Turma;

use Livewire\WithPagination;
use Livewire\Component;
use App\Models\Turma;
use App\Models\Curso;

class Lista extends Component
{
    use WithPagination;

    protected $paginationTheme = 'bootstrap';

    public $curso_id;
    public $unidade_id;
    public $search;
    public $status = 0;
    public $ordem;

    protected $queryString = ['curso_id','unidade_id','search','status','ordem'];

    public function mount()
    {
        if(\Auth::user()->restringir_unidade == 'S' && \Auth::user()->unidade_id != null)
        {
            $this->unidade_id = \Auth::user()->unidade_id;
        }
    }

    public function render()
    {
        $turmas = Turma::select('id','nome')->plataforma()->where('status', $this->status);

        if($this->unidade_id)
        {
            $turmas = $turmas->where('unidade_id', $this->unidade_id);
        }
        
        if($this->curso_id)
        {
            $turmas = $turmas->where('curso_id', $this->curso_id);
        }

        if($this->search)
        {
            $turmas = $turmas->where('nome', 'like', '%'.$this->search.'%');
        }

        $countTurmas = $turmas->count();


        if($this->ordem == 'desc' || $this->ordem == 'asc')
        {
            $turmas = $turmas->orderBy('id', $this->ordem);
        
        } else {

            $turmas = $turmas->orderBy('nome');
        }


        $turmas = $turmas->paginate(10);

        $cursos = Curso::select('id','referencia')->plataforma()->ativo()->orderBy('referencia')->get();


        return view('livewire.admin.turma.lista', [
            'turmas'      => $turmas,
            'countTurmas' => $countTurmas,
            'cursos'      => $cursos,
        ]);
    }
}
