<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlanosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('planos', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('plataforma_id');
            $table->unsignedBigInteger('cadastrante_id')->nullable();
            $table->unsignedBigInteger('unidade_id')->nullable();
            $table->string('nome');
            $table->string('valor')->nullable();
            $table->string('valor_promocional')->nullable();
            $table->string('publicar', 1)->default('S');
            $table->string('renovacao_automatica', 1)->default('N');
            $table->integer('ordem')->nullable();
            $table->integer('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('planos');
    }
}
