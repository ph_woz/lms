@extends('layouts.admin.master')
@section('content')

<nav aria-label="breadcrumb" class="mb-4-5 p-3 bg-db rounded ashadow border">
	<ol class="breadcrumb p-0 m-0">
    	<li class="breadcrumb-item"><a href="{{ route('admin.trilha.lista') }}" class="color-blue-info-link">Trilhas</a></li>
    	<li class="breadcrumb-item"><a href="{{ route('admin.trilha.info', ['id' => $trilha_id]) }}" class="color-blue-info-link">{{ $trilha }}</a></li>
    	<li class="breadcrumb-item text-dark" aria-current="page">Páginas Descritivas</li>
  	</ol>
</nav>


<section class="mb-4-5">

	<div class="section-title rounded-bottom-0 d-lg-flex justify-content-between align-items-center">
		<div>
			Seções ({{ count($secoes) }})
		</div>
		<div>
	    	<a href="#ModalSecaoAdd" data-bs-toggle="modal" class="btn btn-outline-primary border-light text-light">
	    		+ Adicionar novo
	    	</a>
	    </div>
	</div>

</section>

<form method="POST">
@csrf

@foreach ($secoes as $secao)
<section class="mb-4">

	<div class="border bg-white p-3 rounded">
		<div class="bg-white d-lg-flex justify-content-between align-items-center mb-3">
			<div class="d-flex w-100 me-3">
				<input type="text" name="secao[{{$secao->id}}][titulo]" class="form-control p-form w-100 me-3" placeholder="Título" value="{{ $secao->titulo }}">
				<select name="secao[{{$secao->id}}][expandido]" class="select-form w-lg-25 me-3">
					<option value="S">Expandido</option>
					<option value="N" @if($secao->expandido == 'N') selected @endif>Não expandido</option>
				</select>
				<input type="text" name="secao[{{$secao->id}}][ordem]" class="form-control p-form w-lg-15" placeholder="N° de Ordem" value="{{ $secao->ordem }}">
			</div>
			<div>
		    	<a href="#ModalDeletar" onclick="document.getElementById('deletar_id').value = '{{$secao->id}}'; document.getElementById('secao_titulo').innerHTML = '{{ $secao->titulo }}';" data-bs-toggle="modal" class="aclNivel3 btn btn-danger py-2">
		    		<i class="fa fa-trash"></i>
		    	</a>
		    </div>
		</div>

	    <textarea name="secao[{{$secao->id}}][descricao]" class="textarea-form h-min-150" placeholder="Descrição">{!! $secao->descricao !!}</textarea>

	    <input type="hidden" name="secao[{{$secao->id}}][id]" value="{{ $secao->id }}">

	</div>
</section>
@endforeach

<button type="submit" name="editar" value="ok" class="btn-add mb-4">Salvar Alterações</button>	

</form>


<form method="POST" enctype="multipart/form-data">
@csrf
    <div class="modal fade" id="ModalSecaoAdd" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      	<div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        	<div class="modal-content">
	          	<div class="modal-header">
	            	<h3 class="modal-title color-444 font-nunito weight-600">Adicionar Seção</h3>
	            	<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
	          	</div>
	          	<div class="modal-body">
					
					<div class="d-lg-flex mb-3">

		       			<div class="w-100 me-lg-3 mb-3 mb-lg-0">
		       				<label>Título</label>
		       				<input type="text" name="titulo" class="form-control p-form" placeholder="Título" required>
		       			</div>
		       			<div class="w-lg-40 me-lg-3 mb-3 mb-lg-0">
		       				<label>Forma de Exibição</label>
		       				<select name="expandido" class="select-form">
		       					<option value="S">Expandido</option>
		       					<option value="N">Não expandido</option>
		       				</select>
		       			</div>
		       			<div class="w-lg-35">
		       				<label>Ordem</label>
		       				<input type="number" name="ordem" class="form-control p-form" placeholder="N° de Ordem">
		       			</div>

				    </div>

				    <div class="w-100">
				    	<label>Descrição</label>
				    	<textarea name="descricao" class="textarea-form" placeholder="Descreva o depoimento"></textarea>
				    </div>

	          	</div>
          		<div class="modal-footer">
            		<button type="submit" name="add" value="S" class="btn btn-primary py-2 weight-600">
                		Confirmar
            		</button>
          		</div>
	        </div>
      	</div>
    </div>
</form>


<form method="POST">
@csrf
		
	<input type="hidden" name="deletar_id" id="deletar_id">

	<div class="modal fade" id="ModalDeletar" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  		<div class="modal-dialog modal-dialog-centered">
    		<div class="modal-content">
      			<div class="modal-header">
        			<h3 class="modal-title" id="exampleModalLabel">Deletar Seção</h3>
        			<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      			</div>
      			<div class="modal-body text-center py-5">
      				<p class="m-0 weight-600 fs-17" id="secao_titulo"></p>
      				<hr>
        			<p class="m-0 fs-16">
        				Você tem certeza que deseja deletar esta seção?
        				<br>
        				Todas as informações serão perdidas definitivamente.
        			</p>
      			</div>
      			<div class="modal-footer">
	        		<button type="submit" name="deletar" value="S" class="btn btn-danger weight-600">Confirmar</button>
      			</div>
    		</div>
  		</div>
	</div>
</form>


@endsection