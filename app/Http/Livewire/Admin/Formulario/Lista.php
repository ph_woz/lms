<?php

namespace App\Http\Livewire\Admin\Formulario;

use Livewire\WithPagination;
use Livewire\Component;
use App\Models\Formulario;
use App\Models\Categoria;
use App\Models\Unidade;
use App\Models\FormularioCategoria;

class Lista extends Component
{
    use WithPagination;

    protected $paginationTheme = 'bootstrap';

    public $search;
    public $unidade_id;
    public $status = 0;
    public $ordem;
    public $categoriasSelected;

    protected $queryString = ['search','unidade_id','status','ordem','categoriasSelected'];

    public function render()
    {
        $formularios = Formulario::select('id','referencia')->plataforma()->where('status', $this->status);

        if($this->categoriasSelected != null)
        {
            $formulariosIds = FormularioCategoria::plataforma()->whereIn('categoria_id', $this->categoriasSelected ?? array())->get()->pluck('formulario_id')->toArray();
            $formularios = $formularios->whereIn('id', $formulariosIds);
        }

        if($this->search)
        {
            $formularios->where(function ($query)
            {
                $query
                    ->orWhere('referencia', 'like', '%'.$this->search.'%');
            });
        }


        if(\Auth::user()->restringir_unidade == 'S' && \Auth::user()->unidade_id != null)
        {
            $formularios = $formularios->where('unidade_id', \Auth::user()->unidade_id);
        }

        if($this->unidade_id)
        {
            $formularios = $formularios->where('unidade_id', $this->unidade_id);
        }

        $countFormularios = $formularios->count();


        if($this->ordem == 'desc' || $this->ordem == 'asc')
        {
            $formularios = $formularios->orderBy('id', $this->ordem);
        
        } else {

            $formularios = $formularios->orderBy('referencia');
        }
        

        $formularios = $formularios->paginate(10);


        $categorias = Categoria::plataforma()->ativo()->tipo('formulario')->orderBy('nome')->get();
        $unidades   = Unidade::plataforma()->ativo()->orderBy('nome')->get();

        return view('livewire.admin.formulario.lista', [
            'formularios'      => $formularios,
            'countFormularios' => $countFormularios,
            'categorias'       => $categorias,
            'unidades'         => $unidades,
        ]);
    }
}
