<div>

    <nav aria-label="breadcrumb" class="mb-4-5 p-3 bg-db rounded ashadow border">
        <ol class="breadcrumb p-0 m-0">
            <li class="breadcrumb-item"><a href="{{ route('admin.avaliacao.lista') }}" class="color-blue-info-link">Avaliações</a></li>
            <li class="breadcrumb-item"><a href="{{ route('admin.avaliacao.info', ['id' => $avaliacao->id]) }}" class="color-blue-info-link">{{ $avaliacao->referencia }}</a></li>
        </ol>
    </nav>

    <h1 class="text-center mt-5">Página em Manutenção</h1>

{{--     <form>
        <input wire:model="search" type="search" class="form-control p-form" placeholder="Buscar">
    </form>

	<section class="mt-4">

		<div class="section-title">
			<div class="d-flex justify-content-between align-items-center">
				Alunos que finalizaram ({{$countAlunosQueFinalizaram}})
				<div class="d-flex">
					<button type="button" class="hover-d-none outline-0 btn btn-light px-2 py-1 border-0 text-dark" href="#alunosQueFinalizaram" data-bs-toggle="collapse" aria-expanded="false">
						<i class="fas fa-minus"></i>
					</button>
				</div>
			</div>
		</div>


		<div class="collapse show" id="alunosQueFinalizaram">

			<div class="border">

				<div class="table-responsive bg-white p-3">
					<table class="table">
					  	<thead>
					    	<tr>
						      	<th scope="col">Nome</th>
							 	<th scope="col">Email</th>
						      	<th scope="col">
									<div class="float-end mr-1">	
						      			Ações
									</div>
						      	</th>
					    	</tr>
					  	</thead>
					  	<tbody>

						  	@foreach($alunosQueFinalizaram as $user)
						    <tr>
						      	<td>
									<a class="text-dark" href="{{ route('admin.aluno.info', ['id' => $user->id]) }}">
						      			{{ $user->nome }}
						      		</a>
						      	</td>
						      	<td>{{ $user->email }}</td>
						      	<td>
							      	<div class="float-end">
										<a href="{{ route('admin.aluno.info', ['id' => $user->id]) }}" target="_blank" class="aclNivel1 btn btn-secondary font-nunito weight-600">
											<i class="fa fa-eye"></i>
										</a>
										<a href="#" wire:click="setAlterarNotaManualmente({{$user->id}}, 'Não fez')" data-bs-toggle="modal" data-bs-target="#ModalAlterarNota" target="_blank" class="aclNivel3 btn btn-warning font-nunito weight-600 text-dark">
											<i class="fa fa-edit"></i>
										</a>
								    </div>
						      	</td>
						    </tr>
						    @endforeach
					    
					  	</tbody>
					</table>
				</div>

				<div class="bg-white pb-2 px-3">
					{{ $alunosQueFinalizaram->links() }}
				</div>

			</div>

		</div>

	</section>

	<form wire:submit.prevent="alterarNotaManualmente" method="POST">
		<input type="hidden" wire:model="resetar_aluno_id">
		<div wire:ignore class="modal fade" id="ModalAlterarNota" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  		<div class="modal-dialog modal-dialog-centered">
	    		<div class="modal-content">
	      			<div class="modal-header">
	        			<h3 class="modal-title" id="exampleModalLabel">Alterar Nota da Avaliação</h3>
	        			<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
	      			</div>
	      			<div class="modal-body py-5 px-4">

	      				<div class="w-100 d-flex justify-content-center">
			        		<div class="w-100">
			        			<p class="mb-0"> Nome</p>
			        			<input type="text" wire:model="alterar_nota_aluno_nome" class="w-100 border-0 bg-white text-dark outline-0" readonly>
			        		</div>
			        		<div class="mx-3"></div>
			        		<div class="w-25">
			        			<p class="mb-0"> Nota atual</p>
			        			<input type="text" wire:model="alterar_nota_aluno_nota" class="w-100 border-0 bg-white text-dark outline-0" readonly>
			        		</div>
		        		</div>

	        			<hr/>

	        			<p class="m-0 fs-15 text-center">
	        				Você tem certeza que deseja resetar a avaliação deste(a) aluno(a)?<br>
	        			</p>

	      			</div>
	      			<div class="modal-footer">
	      				<div class="d-flex w-100">
	      					<select wire:model="set_nova_nota" class="select-form me-3" required>
	      						<option value="">Selecione a nova Nota</option>
								@foreach($listaNotas as $nota)
									<option value="{{ $nota }}">{{ $nota }}</option>
								@endforeach
				    		</select>
		      				<input type="password" wire:model="password_to_delete" class="w-100 form-control p-form me-3" placeholder="Digite sua senha" required>
		        			<button type="submit" class="btn btn-danger weight-600">Confirmar</button>
	    				</div>
	      			</div>
	    		</div>
	  		</div>
		</div>
	</form>

	<form wire:submit.prevent="resetarAvaliacao" method="POST">
		<input type="hidden" wire:model="resetar_aluno_id">
		<div wire:ignore class="modal fade" id="ModalResetar" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  		<div class="modal-dialog modal-dialog-centered">
	    		<div class="modal-content">
	      			<div class="modal-header">
	        			<h3 class="modal-title" id="exampleModalLabel">Resetar Avaliação</h3>
	        			<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
	      			</div>
	      			<div class="modal-body text-center py-5 px-4">

	        			<p id="resetar_aluno_nome" class="m-0 fs-15 weight-600">
	        				
	        			</p>

	        			<hr/>

	        			<p class="m-0 fs-15">
	        				Você tem certeza que deseja resetar a avaliação deste(a) aluno(a)?<br>
	        			</p>

	      			</div>
	      			<div class="modal-footer">
	      				<div class="d-flex w-100">
		      				<input type="password" wire:model="password_to_delete" class="w-100 form-control p-form me-3" placeholder="Digite sua senha" required>
		        			<button type="submit" class="btn btn-danger weight-600">Confirmar</button>
	    				</div>
	      			</div>
	    		</div>
	  		</div>
		</div>
	</form> --}}

	@include('livewire.util.toast')
	
</div>