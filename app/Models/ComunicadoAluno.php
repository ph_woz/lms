<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ComunicadoAluno extends Model
{
    use HasFactory;
    protected $table = 'comunicados_alunos';

    public $timestamps = false;

    public $fillable = [

    	'plataforma_id',
    	'comunicado_id',
    	'aluno_id',
    	'data_visualizacao',
    ];

    public static  function boot()
    {
        parent::boot(); 
        static::creating(function ($model)
        {
            $model->plataforma_id = session('plataforma_id') ?? \Auth()->user()->plataforma_id ?? Plataforma::dominio()->pluck('id')[0] ?? null;
        });
    }

    public function scopePlataforma($query)
    {
        return $query->where('plataforma_id', session('plataforma_id') ?? Plataforma::dominio()->pluck('id')[0] ?? null);
    }
    
    public function scopeAtivo($query)
    {
        return $query->where('status', 0);
    }

}
