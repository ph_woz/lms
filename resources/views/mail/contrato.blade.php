<p style="font-size:17px; margin-bottom: 0.5rem;">
	Olá, {{ $nome }}. Clique no botão abaixo para assinar o Contrato digitalmente.
</p>

<div>
	<a href="{{ $linkBotaoAssinatura }}" style="display: inline-block; color: #FFF; text-decoration: none; border-radius:2px; background:#1865d9; padding-left:15px; padding-right:15px; padding-top:7px; padding-bottom:7px;">
		Assinar contrato agora!
	</a>
</div>