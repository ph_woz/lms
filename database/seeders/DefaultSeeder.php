<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\Models\WozcodeModulo;
use App\Models\WozcodeCategoria;
use App\Models\WozcodeSubcategoria;
use App\Models\WozcodeComodidade;
use App\Models\PlataformaComodidade;
use App\Models\HorarioFuncionamento;
use App\Models\Plataforma;
use App\Models\Categoria;
use App\Models\PlataformaModulo;
use App\Models\PlataformaEndereco;
use App\Models\User;
use App\Models\AclPlataformaModuloUser;
use App\Models\Curso;
use App\Models\Turma;
use App\Models\Plano;
use App\Models\Servico;
use App\Models\Produto;
use App\Models\Trilha;
use App\Models\TrilhaTurma;
use App\Models\TrilhaCategoria;
use App\Models\UserEndereco;
use App\Models\Certificado;
use App\Models\Banner;

class DefaultSeeder extends Seeder
{
    public function run()
    {
    	$categoria1 = WozcodeCategoria::create(['nome' => 'Educação']);
    	$categoria2 = WozcodeCategoria::create(['nome' => 'Fitness']);
    	$categoria3 = WozcodeCategoria::create(['nome' => 'Beleza & Estética']);

    	WozcodeSubcategoria::create([
    		'categoria_id' => $categoria1->id,
    		'nome' => 'Escola de Dança',
    	]);
    	WozcodeSubcategoria::create([
    		'categoria_id' => $categoria1->id,
    		'nome' => 'Escola de Música',
    	]);
    	WozcodeSubcategoria::create([
    		'categoria_id' => $categoria1->id,
    		'nome' => 'Creche',
    	]);

    	WozcodeSubcategoria::create([
    		'categoria_id' => $categoria2->id,
    		'nome' => 'Crossfit',
    	]);
    	WozcodeSubcategoria::create([
    		'categoria_id' => $categoria2->id,
    		'nome' => 'Pilates',
    	]);
    	WozcodeSubcategoria::create([
    		'categoria_id' => $categoria2->id,
    		'nome' => 'Aeróbica',
    	]);
    	WozcodeSubcategoria::create([
    		'categoria_id' => $categoria2->id,
    		'nome' => 'Artes Marciais',
    	]);

    	WozcodeSubcategoria::create([
    		'categoria_id' => $categoria3->id,
    		'nome' => 'Barbearia',
    	]);

    	WozcodeSubcategoria::create([
    		'categoria_id' => $categoria3->id,
    		'nome' => 'Manicure',
    	]);

		WozcodeModulo::create([
			'texto' => 'Dashboard',
			'url'   => 'dashboard',
		]);
		WozcodeModulo::create([
			'texto' => 'Alunos',
			'url'   => 'aluno',
		]);
		WozcodeModulo::create([
			'texto' => 'Controle Financeiro',
			'url'   => 'controle-financeiro',
		]);
		WozcodeModulo::create([
			'texto' => 'Colaboradores',
			'url'   => 'colaborador'
		]);
		WozcodeModulo::create([
			'texto' => 'Turmas',
			'url'   => 'turma'
		]);
		WozcodeModulo::create([
			'texto' => 'Trilhas de Aprendizagem',
			'url'   => 'trilha'
		]);
		WozcodeModulo::create([
			'texto' => 'Cursos',
			'url'   => 'curso'
		]);
		WozcodeModulo::create([
			'texto' => 'Avaliações',
			'url'   => 'avaliacao'
		]);
		WozcodeModulo::create([
			'texto' => 'Questões',
			'url'   => 'questao'
		]);
		WozcodeModulo::create([
			'texto' => 'Comunicados',
			'url'   => 'comunicado'
		]);
		WozcodeModulo::create([
			'texto' => 'Contratos',
			'url'   => 'contrato'
		]);
		WozcodeModulo::create([
			'texto' => 'Gestão de Documentos',
			'url'   => 'documento'
		]);
		WozcodeModulo::create([
			'texto' => 'Certificados',
			'url'   => 'certificado'
		]);
		WozcodeModulo::create([
			'texto' => 'Páginas',
			'url'   => 'pagina'
		]);
		WozcodeModulo::create([
			'texto' => 'Formulários',
			'url'   => 'formulario'
		]);
		WozcodeModulo::create([
			'texto' => 'Contatos',
			'url'   => 'contato'
		]);
		WozcodeModulo::create([
			'texto' => 'Unidades',
			'url'   => 'unidade'
		]);
		WozcodeModulo::create([
			'texto' => 'Planos',
			'url'   => 'plano'
		]);
		WozcodeModulo::create([
			'texto' => 'Serviços',
			'url'   => 'servico'
		]);
		WozcodeModulo::create([
			'texto' => 'Produtos',
			'url'   => 'produto'
		]);
		WozcodeModulo::create([
			'texto' => 'Relatórios Gerais',
			'url'   => 'relatorio'
		]);
		WozcodeModulo::create([
			'texto' => 'Plataforma',
			'url'   => 'plataforma'
		]);

    	$categoriaEducacao = WozcodeCategoria::create(['nome' => 'Educação', 'slug' => 'educacao']);
    	$categoriaFitness = WozcodeCategoria::create(['nome' => 'Fitness', 'slug' => 'fitness']);
    	$categoriaBelEstet = WozcodeCategoria::create(['nome' => 'Beleza & Estética', 'slug' => 'beleza-e-estetica']);

    	WozcodeSubcategoria::create([
    		'categoria_id' => $categoriaEducacao->id,
    		'nome' => 'Escola de Dança',
    		'slug' => 'escola-de-danca',
    	]);
    	WozcodeSubcategoria::create([
    		'categoria_id' => $categoriaEducacao->id,
    		'nome' => 'Escola de Música',
    		'slug' => 'escola-de-musica',
    	]);
    	WozcodeSubcategoria::create([
    		'categoria_id' => $categoriaEducacao->id,
    		'nome' => 'Creche',
    		'slug' => 'creche',
    	]);

    	WozcodeSubcategoria::create([
    		'categoria_id' => $categoriaFitness->id,
    		'nome' => 'Crossfit',
    		'slug' => 'crossfit',
    	]);
    	WozcodeSubcategoria::create([
    		'categoria_id' => $categoriaFitness->id,
    		'nome' => 'Pilates',
    		'slug' => 'pilates',
    	]);
    	WozcodeSubcategoria::create([
    		'categoria_id' => $categoriaFitness->id,
    		'nome' => 'Aeróbica',
    		'slug' => 'aerobica',
    	]);
    	WozcodeSubcategoria::create([
    		'categoria_id' => $categoriaFitness->id,
    		'nome' => 'Artes Marciais',
    		'slug' => 'artes-marciais',
    	]);

    	WozcodeSubcategoria::create([
    		'categoria_id' => $categoriaBelEstet->id,
    		'nome' => 'Barbearia',
    		'slug' => 'barbearia',
    	]);

    	WozcodeSubcategoria::create([
    		'categoria_id' => $categoriaBelEstet->id,
    		'nome' => 'Manicure',
    		'slug' => 'manicure',
    	]);


		$allComodidades = ['Acesso para deficientes físicos', 'Acupuntura', 'Ambiente Climatizado', 'Aquecimento', 'Bebedouro', 'Buitenbad', 'Cadeado', 'Campo de Futebol', 'Clínica de Estética', 'Day SPA', 'Equipamentos especializados', 'Espaço Kids', 'Espaço para crianças', 'Espaço Vida Saudável', 'Estacionamento', 'Estacionamento para Bicicletas', 'Fisioterapia', 'Armários', 'Hidratação', 'Itens de higiene pessoal', 'Lanchonete', 'Loja', 'Loja de Artigos Esportivos', 'Manobrista', 'Massagem', 'Massoterapeuta', 'Nutricionista', 'Personal Trainer', 'Piscina', 'Quadra Poli-esportiva', 'Reabilitação', 'Restaurante', 'Sala de Bike', 'Sala de Cardio', 'Sala de Ginástica', 'Salão de Beleza', 'Sauna', 'Secador de cabelo', 'Shampoo', 'Toalha', 'Chuveiro', 'Toalha de banho', 'Treinador', 'Vestiários', 'WC Infantil', 'WiFi', 'Workshops'];
		
		foreach($allComodidades as $comodidade)
		{
			WozcodeComodidade::create([
				'texto' => $comodidade,
				'icon'   => '<i class="fa fa-users"></i>'
			]);
		}

	    $plataforma = Plataforma::create([
	    	'nome'    => 'WOZCODE',
	    	'dominio' => 'localhost',
	    	'email_contratacao_turma'  => '<p style="color: #222222; font-family: Arial, Helvetica, sans-serif; font-size: small; background-color: #ffffff;">Prezado(a) [NOME],</p> <p style="color: #222222; font-family: Arial, Helvetica, sans-serif; font-size: small; background-color: #ffffff;"><span class="il">Seja</span>&nbsp;<span class="il">bem</span>&nbsp;<span class="il">vindo</span> a turma [TURMA] do curso [CURSO] da plataforma [PLATAFORMA_NOME].</p> <p style="color: #222222; font-family: Arial, Helvetica, sans-serif; font-size: small; background-color: #ffffff;">Preparamos conte&uacute;dos de qualidade para que voc&ecirc; vivencie uma incr&iacute;vel experi&ecirc;ncia de aprendizagem!</p> <p style="color: #222222; font-family: Arial, Helvetica, sans-serif; font-size: small; background-color: #ffffff;">Segue abaixo o link de acesso para voc&ecirc; iniciar a sua jornada de aprendizagem.</p> <p style="color: #222222; font-family: Arial, Helvetica, sans-serif; font-size: small; background-color: #ffffff;"><strong>Seu link: [PLATAFORMA_LINK]</strong></p> <p style="color: #222222; font-family: Arial, Helvetica, sans-serif; font-size: small; background-color: #ffffff;"><strong>Seu Login: [EMAIL]</strong></p> <p style="color: #222222; font-family: Arial, Helvetica, sans-serif; font-size: small; background-color: #ffffff;">&nbsp;</p> <p style="color: #222222; font-family: Arial, Helvetica, sans-serif; font-size: small; background-color: #ffffff;"><span style="color: #222222; font-family: Arial, Helvetica, sans-serif; font-size: small;"><strong>Caso n&atilde;o tenha sua Senha, clique no bot&atilde;o a seguir para redefinir e entrar na plataforma.</strong></span></p> <p style="color: #222222; font-family: Arial, Helvetica, sans-serif; font-size: small; background-color: #ffffff;"><span style="color: #222222; font-family: Arial, Helvetica, sans-serif; font-size: small;"><strong>[BOTAO_REDEFINIR_SENHA]</strong></span></p> <div id="njcdgcofcbnlbpkpdhmlmiblaglnkpnj"></div> <div id="njcdgcofcbnlbpkpdhmlmiblaglnkpnj"></div> <div id="njcdgcofcbnlbpkpdhmlmiblaglnkpnj"></div> <div id="njcdgcofcbnlbpkpdhmlmiblaglnkpnj"></div>',
	    	'email_contratacao_trilha' => '<p style="color: #222222; font-family: Arial, Helvetica, sans-serif; font-size: small; background-color: #ffffff;">Prezado(a) [NOME],</p> <p style="color: #222222; font-family: Arial, Helvetica, sans-serif; font-size: small; background-color: #ffffff;"><span class="il">Seja</span>&nbsp;<span class="il">bem</span>&nbsp;<span class="il">vindo</span> a trilha [TRILHA] da plataforma [PLATAFORMA_NOME].</p> <p style="color: #222222; font-family: Arial, Helvetica, sans-serif; font-size: small; background-color: #ffffff;">Preparamos conte&uacute;dos de qualidade para que voc&ecirc; vivencie uma incr&iacute;vel experi&ecirc;ncia de aprendizagem!</p> <p style="color: #222222; font-family: Arial, Helvetica, sans-serif; font-size: small; background-color: #ffffff;">Segue abaixo o link de acesso para voc&ecirc; iniciar a sua jornada de aprendizagem.</p> <p style="color: #222222; font-family: Arial, Helvetica, sans-serif; font-size: small; background-color: #ffffff;"><strong>Seu link: [PLATAFORMA_LINK]</strong></p> <p style="color: #222222; font-family: Arial, Helvetica, sans-serif; font-size: small; background-color: #ffffff;"><strong>Seu Login: [EMAIL]</strong></p> <p style="color: #222222; font-family: Arial, Helvetica, sans-serif; font-size: small; background-color: #ffffff;">&nbsp;</p> <p style="color: #222222; font-family: Arial, Helvetica, sans-serif; font-size: small; background-color: #ffffff;"><span style="color: #222222; font-family: Arial, Helvetica, sans-serif; font-size: small;"><strong>Caso n&atilde;o tenha sua Senha, clique no bot&atilde;o a seguir para redefinir e entrar na plataforma.</strong></span></p> <p style="color: #222222; font-family: Arial, Helvetica, sans-serif; font-size: small; background-color: #ffffff;"><span style="color: #222222; font-family: Arial, Helvetica, sans-serif; font-size: small;"><strong>[BOTAO_REDEFINIR_SENHA]</strong></span></p>',
	    	'tipo_negocio' => 'Cursos Profissionalizantes',
	    	'logotipo'     => 'https://wozcodelms2.s3.amazonaws.com/plataformas/1/logotipo.webp',
	    	'categoria_id' => $categoriaEducacao->id,
	    	'whatsapp'     => '(21) 96839-0250',
	    	'facebook'     => 'https://facebook.com/wozcode',
	    	'instagram'    => 'https://instagram.com/wozcode',
	    ]);

		$plataforma_id = $plataforma->id;

	    PlataformaEndereco::create([
	    	'plataforma_id' => $plataforma_id,
	        'cep' => '25060-330',
	        'rua' => 'Rua Taquari',
	        'estado' => 'RJ',
	        'bairro' => 'Gramacho',
	        'cidade' => 'Duque de Caxias',
	        'numero' => '25',
	        'complemento' => 'Casa 3',
	    ]);

	    Banner::create(['plataforma_id' => $plataforma_id, 'link' => 'https://wozcodelms2.s3.amazonaws.com/plataformas/1/banners/2.jpg']);
	    Banner::create(['plataforma_id' => $plataforma_id, 'link' => 'https://wozcodelms2.s3.amazonaws.com/plataformas/1/banners/3.jpg']);
	    Banner::create(['plataforma_id' => $plataforma_id, 'link' => 'https://wozcodelms2.s3.amazonaws.com/plataformas/1/banners/4.jpeg']);


	    $user = User::create([
			'plataforma_id' =>  $plataforma->id,
			'nome'          => 'Pedro Henrique Caetano da Silva',
			'email'         => 'pedrohenrique1207ph@gmail.com',
			'password'      =>  Hash::make('26533955'),
			'tipo'          => 'colaborador',
	    ]);

	    $plataforma->update(['responsavel_id' => $user->id]);

	    $aluno = User::create([
			'plataforma_id' =>  $plataforma->id,
			'nome'          => 'Beatriz Silva',
			'email'         => 'bea.silva@gmail.com',
			'password'      =>  Hash::make('26533955'),
			'tipo'          => 'aluno',
	    ]);

	    UserEndereco::create([
	    	'plataforma_id' =>  $plataforma->id,
	    	'user_id' => $aluno->id,
	    	'estado'  => 'RJ',
	    	'cidade'  => 'Duque de Caxias',
	    	'bairro'  => 'Gramacho',
	    	'rua'     => 'Taquarí',
	    	'numero'  => '25',
	    ]);

	    // Criar os módulos da plataforma e o ACL do admin
	    $modulos = WozcodeModulo::all();
	    foreach($modulos as $modulo)
	    {
	      	$newModulo = PlataformaModulo::create([
				'plataforma_id' => $plataforma->id,
				'texto'         => $modulo->texto,
				'url'           => $modulo->url,
	      	]);         
	      	AclPlataformaModuloUser::create([
				'plataforma_id'  => $plataforma->id,
				'user_id' => $user->id,
				'modulo_id'      => $newModulo->id,
				'nivel'          => 3,
	      	]);
	    } 


	    $curso = Curso::create([
	    	'nome'         => 'Análise e Desenvolvimento de Sistemas',
	    	'referencia'   => 'Análise e Desenvolvimento de Sistemas',
	    	'slug'         => 'analise-e-desenvolvimento-de-sistemas',
	    	'carga_horaria' => '2.500',
	    ]);

	    $curso2 = Curso::create([
	    	'nome'         => 'Sistemas de Informação',
	    	'referencia'   => 'Sistemas de Informação',
	    	'slug'         => 'sistemas-de-informacao',
	    	'carga_horaria' => '2.500',
	    ]);

	    $curso3 = Curso::create([
	    	'nome'         => 'Ciências da Computação',
	    	'referencia'   => 'Ciências da Computação',
	    	'slug'         => 'ciencias-da-computacao',
	    	'carga_horaria' => '4.500',
	    ]);

	    $turma = Turma::create([
	    	'nome'     => 'ADS - 01.07',
	    	'curso_id' =>  $curso->id,
	    	'duracao'  => '2.5 anos',
	    	'publicar' => 'S',
	    ]);
	    $turma2 = Turma::create([
	    	'nome'     => 'ADS - 01.01',
	    	'curso_id' =>  $curso->id,
	    	'publicar' => 'S',
	    ]);

	    $turma3 = Turma::create([
	    	'nome'     => 'SI - 01.07',
	    	'curso_id' =>  $curso2->id,
	    	'publicar' => 'S',
	    ]);

	    $turma4 = Turma::create([
	    	'nome'     => 'CC - 01.07',
	    	'curso_id' =>  $curso3->id,
	    	'duracao'  => '4 anos',
	    	'publicar' => 'S',
	    ]);


	    $comodidades = WozcodeComodidade::all();
	    foreach($comodidades as $comodidade)
	    {
	      	PlataformaComodidade::create([
				'plataforma_id' => $plataforma_id,
				'comodidade_id' => $comodidade->id,
	      	]);         
	    }


	    $plano1 = Plano::create([
	    	'plataforma_id' => $plataforma_id,
	    	'nome'          => 'JIU JITSU',
	    	'publicar' => 'S',
	    ]);

	    $plano2 = Plano::create([
	    	'plataforma_id' => $plataforma_id,
	    	'nome'          => 'JUDÔ',
	    	'publicar' => 'S',
	    ]);

	    $plano3 = Plano::create([
	    	'plataforma_id' => $plataforma_id,
	    	'nome'          => 'BOXE',
	    	'publicar' => 'S',
	    ]);

	    $plano4 = Plano::create([
	    	'plataforma_id' => $plataforma_id,
	    	'nome'          => 'MUAY THAI',
	    	'publicar' => 'S',
	    ]);

	    $plano5 = Plano::create([
	    	'plataforma_id' => $plataforma_id,
	    	'nome'          => 'MMA',
	    	'publicar' => 'S',
	    ]);

	    $servico1 = Servico::create([
			'plataforma_id' => $plataforma_id,
	    	'nome'          => 'Combo (Barba+Corte)',
	    	'descricao'     => 'Corte cabelo masculino e barba completa',
	    	'publicar'      => 'S',
	    	'valor'         => '30,00',
	    	'duracao'       => '1h 10min',
	    ]);

	    $produto = Produto::create([
	    	'plataforma_id' => $plataforma_id,
	    	'nome'          => 'Kimono Jiu-jitsu Atama Classic',
	    	'publicar' => 'S',
	    ]);

	    $categoria1 = Categoria::create([
			'plataforma_id' => $plataforma_id,
	    	'nome'          => 'Combos',
	    	'tipo'          => 'Serviços',
	    	'publicar' => 'S',
	    ]);

	    $categoria2 = Categoria::create([
			'plataforma_id' => $plataforma_id,
	    	'nome'          => 'Kimonos',
	    	'tipo'          => 'Produtos',
	    	'publicar' => 'S',
	    ]);

	    $catTrilha = Categoria::create([
	    	'nome' => 'Graduação',
	    	'publicar' => 'S',
	    	'tipo' => 'Trilha',
	    ]);

	    $catTrilha2 = Categoria::create([
	    	'nome' => 'Pós-Graduação',
	    	'publicar' => 'S',
	    	'tipo' => 'Trilha',
	    ]);


	    $trilha = Trilha::create([
	    	'nome'           => 'Gestão de Recursos Humanos',
	    	'referencia'     => 'Gestão de Recursos Humanos',
	    	'slug'           => 'gestao-de-Recursos-humanos',
	    	'publicar'       => 'S',
	    	'modalidade'     => 'EaD',
	    	'tipo_inscricao' => 'auto-inscricao',
	    	'duracao'        => '2.5 anos',
	    	'nivel'          => 'Técnologo',
	        'tipo_valor' => 'F',
	        'desconto_agilpass' => '20',
	        'valor_parcelado' => '12x de R$ 600,00',
	        'valor_a_vista' => '5.900,00',
	        'valor_promocional' => '4.200,00',
	    ]);

	    $trilha2 = Trilha::create([
	    	'nome'           => 'Análise e Desenvolvimento de Sistemas',
	    	'referencia'     => 'Análise e Desenvolvimento de Sistemas',
	    	'slug'           => 'analise-e-desenvolvimento-de-sistemas',
	    	'publicar'       => 'S',
	    	'modalidade'     => 'EaD',
	    	'tipo_inscricao' => 'auto-inscricao',
	    	'duracao'        => '2.5 anos',
	    	'nivel'          => 'Técnologo',
	        'tipo_valor' => 'M',
	        'tipo_mensalidade' => 'F',
	        'desconto_agilpass' => '50',
	        'valor_mensalidade' => '129,99',
	        'valor_promocional' => '99,99',
	    ]);

	    $trilha3 = Trilha::create([
	    	'nome'           => 'Arquitetura e Urbanismo',
	    	'referencia'     => 'Arquitetura e Urbanismo',
	    	'slug'           => 'arquitetura-e-urbanismo',
	    	'publicar'       => 'S',
	    	'modalidade'     => 'Presencial',
	    	'tipo_inscricao' => 'auto-inscricao',
	    	'duracao'        => '5 anos',
	    	'nivel'          => 'Bacharelado',
	        'tipo_valor' => 'M',
	        'tipo_mensalidade' => 'R',
	        'desconto_agilpass' => '50',
	        'valor_mensalidade' => '297,99',
	        'valor_promocional' => '149,99',
	    ]);

	    TrilhaCategoria::create([
	    	'plataforma_id' => $plataforma_id,
	    	'categoria_id'  => $catTrilha->id,
	    	'trilha_id'     => $trilha->id,
	    ]);
	    TrilhaCategoria::create([
	    	'plataforma_id' => $plataforma_id,
	    	'categoria_id'  => $catTrilha->id,
	    	'trilha_id'     => $trilha2->id,
	    ]);
	    TrilhaCategoria::create([
	    	'plataforma_id' => $plataforma_id,
	    	'categoria_id'  => $catTrilha2->id,
	    	'trilha_id'     => $trilha3->id,
	    ]);

	    TrilhaTurma::create([
	    	'trilha_id' => $trilha->id,
	    	'turma_id'  => $turma->id,
	    ]);
	    TrilhaTurma::create([
	    	'trilha_id' => $trilha->id,
	    	'turma_id'  => $turma3->id,
	    ]);
	    TrilhaTurma::create([
	    	'trilha_id' => $trilha->id,
	    	'turma_id'  => $turma4->id,
	    ]);

	    Certificado::create([
	    	'referencia' => 'Certificado de Conclusão',
	    	'modelo' => 'https://admlmc.wozcode.com/padroes-lmcv2/certificado.png',
	    	'texto1' => 'pela participação no curso: [CURSO] \n realizado em [DATA_CONCLUSAO] com carga horária total de [CARGA_HORARIA] horas.',
	    	'texto2' => '[ESTADO_ALUNO], [DATA_ATUAL].',
	    ]);

	    HorarioFuncionamento::create([
	    	'plataforma_id' => $plataforma_id,
	    	'dia'           => 'Segunda-feira',
	    	'fechado' 		=> 'S',
	    ]);
	    HorarioFuncionamento::create([
	    	'plataforma_id' => $plataforma_id,
	    	'dia'           => 'Terça-feira',
	    	'fechado' 		=> 'S',
	    ]);
	    HorarioFuncionamento::create([
	    	'plataforma_id' => $plataforma_id,
	    	'dia'           => 'Quarta-feira',
	    	'fechado' 		=> 'S',
	    ]);
	    HorarioFuncionamento::create([
	    	'plataforma_id' => $plataforma_id,
	    	'dia'           => 'Quinta-feira',
	    	'fechado' 		=> 'S',
	    ]);
	    HorarioFuncionamento::create([
	    	'plataforma_id' => $plataforma_id,
	    	'dia'           => 'Sexta-feira',
	    	'fechado' 		=> 'S',
	    ]);
	    HorarioFuncionamento::create([
	    	'plataforma_id' => $plataforma_id,
	    	'dia'           => 'Sábado',
	    	'fechado' 		=> 'S',
	    ]);
	    HorarioFuncionamento::create([
	    	'plataforma_id' => $plataforma_id,
	    	'dia'           => 'Domingo',
	    	'fechado' 		=> 'S',
	    ]);

    }
}
