<?php

namespace App\Http\Livewire\Admin\Comunicado;

use Livewire\Component;
use DB;
use App\Models\Comunicado;
use App\Models\ComunicadoAluno;
use App\Models\User;
use App\Models\Log;
use Illuminate\Support\Facades\Http;

class Info extends Component
{
    public $comunicado;
    public $cadastrante;
    public $password_to_delete;

    public function mount($comunicado_id)
    {
        $this->comunicado = Comunicado::plataforma()->find($comunicado_id);

        $this->cadastrante = User::plataforma()->where('id', $this->comunicado->cadastrante_id)->first();
    }

    public function render()
    {
        $alunos = DB::table('comunicados_alunos')
            ->join('users','comunicados_alunos.aluno_id','users.id')
            ->where('comunicados_alunos.comunicado_id', $this->comunicado->id)
            ->where('users.plataforma_id', session('plataforma_id'))
            ->where('comunicados_alunos.plataforma_id', session('plataforma_id'))
            ->select(
                'users.id',
                'users.nome',
                'users.email',
                'users.data_ultimo_acesso',
                'comunicados_alunos.data_visualizacao',
            )
            ->orderBy('users.data_ultimo_acesso')
            ->get();

        return view('livewire.admin.comunicado.info', ['alunos' => $alunos]);
    }

    public function delete()
    {
        if(\Auth::attempt(['id' => \Auth::id(), 'password' => $this->password_to_delete, 'status' => 0]))
        {
            $comunicado = Comunicado::plataforma()->where('id', $this->comunicado->id)->first();

            $deleteAndReturnCountDeleteds = ComunicadoAluno::plataforma()->where('comunicado_id', $comunicado->id)->delete();

            Log::create([
                'tipo' => 'comunicado',
                'ref'  => 'Comunicado deletado: ' . $comunicado->assunto . ' | ' . $comunicado->descricao . '. Quantidade de alunos que foram desvinculados: ' . $deleteAndReturnCountDeleteds
            ]);

            $comunicado->delete();

            session()->flash('success', 'Comunicado deletado com êxito.');
            
            return redirect(route('admin.comunicado.lista'));

        } else {

            session()->flash('danger-livewire', 'Senha incorreta.');

            $this->dispatchBrowserEvent('toast');
        }
    }
}
