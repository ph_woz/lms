@extends('layouts.admin.master')
@section('content')

<nav aria-label="breadcrumb" class="mb-4-5 p-3 bg-db rounded ashadow border">
	<ol class="breadcrumb p-0 m-0">
    	<li class="breadcrumb-item"><a href="{{ route('admin.turma.lista') }}" class="color-blue-info-link">Turmas</a></li>
    	<li class="breadcrumb-item"><a href="{{ route('admin.turma.info', ['id' => $turma->id]) }}" class="color-blue-info-link">{{ $turma->nome }}</a></li>
    	<li class="breadcrumb-item"><a href="/admin/turma/editar/{{ $turma->id }}/lista-de-presenca" class="color-blue-info-link">Lista de Presença</a></li>
    	<li class="breadcrumb-item text-dark" aria-current="page">Lançar</li>
  	</ol>
</nav>

<form method="POST">
@csrf
	
	<div class="mb-5">
		<label>Data da Aula</label>
		<input type="date" class="form-control p-form" name="data_aula" value="{{ date('Y-m-d') }}" required>
	</div>

	@foreach($users as $user)

		<div class="d-lg-flex align-items-center">

            <div class="me-lg-3">
            	@if($user->foto_perfil)
            	    <img src="{{ $user->foto_perfil }}" class="border border-light square-55 border-profile rounded-circle object-fit-cover">
            	@else
            		<i class="fa fa-user-circle fs-55"></i>
            	@endif
            </div>

            <div>

            	<div class="mb-0 w-100">
		        	<p class="font-nunito fs-17 color-555 weight-600 mb-0">{{ $user->nome }}</p>
		        </div>

		        <div class="w-100 d-flex">	            
		            <label for="f-aluno{{ $user->id }}" class="me-4 d-flex align-items-center">
		            	<input type="radio" name="status[{{ $user->id }}]" value="0" id="f-aluno{{ $user->id }}" class="square-15" required @if(in_array($user->id, $faltaram)) checked @endif>
						<span class="ms-1">Faltou</span>
					</label>

		            <label for="c-aluno{{ $user->id }}" class="d-flex align-items-center">
		            	<input type="radio" name="status[{{ $user->id }}]" value="1" id="c-aluno{{ $user->id }}" class="square-15" required @if(in_array($user->id, $compareceram)) checked @endif>
						<span class="ms-1">Compareceu</span>
					</label>
		        </div>

		    </div>

	    </div>

		<hr>

	@endforeach

	<button type="submit" class="btn-add mt-3" name="sent" value="ok">
		Salvar
	</button>

</form>

@endsection