@extends('layouts.admin.master')
@section('content')
    
<div>

    <section class="pt-5">
        
        <div class="border col-md-8 offset-md-2 px-4 py-5 text-center bg-white rounded-2">

            <h1 class="mb-3 display-5">Ops!</h1>
            <p class="mb-0 fs-20 ls-05 color-444">
                {{ $erro ?? null }}
            </p>
        
        </div>

    </section>

</div>    

@endsection