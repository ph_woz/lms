<?php

namespace App\Http\Livewire\Admin\Curso\Modulo\Aula;

use Livewire\WithPagination;
use Livewire\Component;
use App\Models\Curso;
use App\Models\CursoModulo;
use App\Models\CursoAula;
use App\Models\Categoria;
use App\Models\ModuloCategoria;

class Lista extends Component
{
    use WithPagination;

    protected $paginationTheme = 'bootstrap';

    public $curso_id;
    public $modulo_id;
    public $search;
    public $status = 0;
    public $ordem;

    protected $queryString = ['search','status','ordem'];

    public function mount($curso_id, $modulo_id)
    {
        $this->curso_id  = $curso_id;
        $this->modulo_id = $modulo_id;
    }

    public function render()
    {
        $aulas = CursoAula::plataforma()->where('modulo_id', $this->modulo_id);

        if($this->search)
            $aulas = $aulas->where('nome', 'like', '%'.$this->search.'%');

        $aulas = $aulas->where('status', $this->status);

        $countAulas = $aulas->count();

        $aulas = $aulas->orderBy('ordem')->paginate(10);


        $curso  = Curso::plataforma()->where('id', $this->curso_id)->first();
        $modulo = CursoModulo::plataforma()->where('id', $this->modulo_id)->first();

        return view('livewire.admin.curso.modulo.aula.lista', [
            'curso'        => $curso,
            'modulo'       => $modulo,
            'aulas'        => $aulas,
            'countAulas'   => $countAulas,
        ]);
    }

}
