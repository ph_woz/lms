@extends('layouts.site')

@section('seo_title', $pagina->seo_title ?? $nomePlataforma)
@section('seo_description', $pagina->seo_description ?? null)
@section('seo_keywords', $pagina->seo_keywords ?? null)

@section('content')
<section class="py-5 bg-dark" id="contato">
 <div class="container-fluid px-xl-7 py-5">

    <h1 class="pt-5 fs-40 text-center font-poppins text-light weight-800">FALE CONOSCO</h1>
    <div class="d-flex justify-content-center mt-4-5 mb-4-5"><div style="width: 80px; height: 2px;" class="bg-primary rounded"></div></div>

    <div class="row align-items-center py-5 px-3 px-md-0">

       <div class="col-lg-6">

       		  @if($plataforma->horario)
              <div class="mb-4">
                  <p class="mb-0 font-poppins text-white d-flex align-items-center">
                    <i class="fa fa-clock fs-30 text-primary"></i>
                    <span class="ms-4">{!! nl2br($plataforma->horario ?? null) !!}</span>
                  </p>
              </div>
              @endif

              <div class="mb-4">
                  <p class="mb-0 font-poppins text-white d-flex align-items-center">
                    <i class="fa fa-envelope fs-30 text-primary"></i>
                    <span class="ms-4">{{ $plataforma->email ?? 'meuemail@exemplo.com' }}</span>
                  </p>
              </div>

              @if($plataforma->whatsapp)
              <div class="mb-4 width-large">
                  <a href="https://api.whatsapp.com/send?phone=55{{preg_replace('/\D/', '', $plataforma->whatsapp)}}&text=Olá, vim através do Site {{ $plataforma->nome }}." target="_blank" class="text-decoration-none hover-d-none font-poppins text-white d-flex align-items-center">
                    <i class="fab fa-whatsapp fs-30 text-primary"></i>
                    <span class="text-light text-decoration-none ms-3 ls-05">&nbsp;{{ $plataforma->whatsapp }}</span>
                  </a>
              </div>
              @endif

              @if($plataforma->telefone)
              <div class="mb-4 width-large">
                  <a href="tel:+55{{preg_replace('/\D/', '', $plataforma->telefone)}}" class="text-decoration-none hover-d-none font-poppins text-white d-flex align-items-center">
                    <i class="mr-2 fa fa-phone fs-25 text-primary"></i>
                    <span class="text-light text-decoration-none ms-3 ls-05">{{ $plataforma->telefone }}</span>
                  </a>
              </div>
              @endif

              @if($plataforma->endereco)
              <div class="mb-lg-5 mb-4">
                  <div class="d-flex align-items-center">
                    <i class="mr-2 fas fa-map-marker-alt fs-30 text-primary"></i>
                    <address class="py-0 ms-3 my-0 text-light font-poppins">
                      {!! nl2br($plataforma->endereco ?? null) !!}
                    </address>
                  </div>
              </div>
              @endif

       </div>

       <div class="col-lg-6 mt-lg-0 mt-5">
        <form method="POST" action="/">
          @csrf
           
          <input type="text" name="nome" class="form-control placeholder-edit py-2-5 outline-0 rounded-3" placeholder="Nome" required>
          <input type="email" name="email" class="my-2 rounded-left-0 form-control placeholder-edit py-2-5 outline-0 rounded-3" placeholder="Email" required>
          <input type="tel" name="celular" class="telefone form-control placeholder-edit py-2-5 outline-0 rounded-3" placeholder="Celular" required>
          <input type="text" name="assunto" class="mt-2 form-control placeholder-edit py-2-5 outline-0 rounded-3" placeholder="Assunto" required>
          <textarea name="mensagem" placeholder="Mensagem" class="mt-2 textarea-form placeholder-edit rounded-3"></textarea>
        
          <div class="my-3">
           <label class="cursor-pointer font-poppins fs-14 text-light">
              <input type="checkbox" class="" name="consentimento" value="1" required> Concordo que os dados pessoais fornecidos acima serão utilizados para envio de conteúdo informativo, analítico e publicitário sobre produtos, serviços e assuntos gerais, nos termos da Lei Geral de Proteção de Dados.
           </label>
          </div>

          <input type="text" name="check" class="invisible h-0 d-block" placeholder="Não preencha este campo.">
          <button type="submit" value="S" name="sentContato" class="btn btn-primary weight-600 shadow px-4 py-2">
          	Enviar
          </button>

        </form>
       </div>
       
    </div>
 
 </div>
</section>
@endsection