<!doctype html>
<html lang="pt-br">
<head>

   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1">

   <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/uikit@3.5.9/dist/css/uikit.min.css" />
   <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
   <link rel="stylesheet" href="{{ asset('css/util.css') }}?v=3">
   <link rel="stylesheet" href="{{ asset('sites-proprios/faro/css/home.css') }}?v=3">

   <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">

   <title>{{ $seo->seo_title ?? $plataforma->nome }}</title>
   <meta name="robots" content="index, follow">
   <meta name="description" content="{{ $seo->seo_description ?? null }}"/>
   <meta name="keywords" content="{{ $seo->seo_keywords ?? null }}"/>
   <link rel="canonical" href="{{ Request::url() }}" />
   <link rel="shortcut icon" href="{{ $plataforma->favicon }}">
   <meta property="og:locale" content="pt_BR" />
   <meta property="og:type" content="website" />
   <meta property="og:title" content="{{ $seo->seo_title ?? $plataforma->nome }}" />
   <meta property="og:description" content="{{ $seo->seo_description ?? null }}" />
   <meta property="og:url" content="{{ Request::url() }}" />
   <meta property="og:site_name" content="{{ $plataforma->nome }}" />
   <meta property="og:image" content="{{ $plataforma->logotipo }}" />
   <meta name='author' content='WOZCODE | Pedro Henrique' />

</head>
<body class="bg-f4" id="inicio">

   @include('sites-proprios/faro/componentes/header')

   <main class="pb-5">

      <section class="w-100 h-lg-200 background-size-cover background-position-center" style="background-image: url('{{ $plataforma->jumbotron ?? asset('sites-proprios/faro/images/jumbotron.jpeg') }}');">
              <div class="position-relative h-100">
              <div class="background-overlay opacity-08 h-100" style="background: rgba(0,0,0,1);"></div>
               <div class="container h-100 py-5 py-lg-0">
                 <div class="row container align-items-center h-100 position-relative">
                    <div>
                        <h1 class="text-light font-poppins weight-800 fs-md-40 fs-xs-35 text-shadow-3" id="tituloTransferencia">
                           CONSULTA PÚBLICA DE DIPLOMAS
                        </h1>
                    </div>
                 </div>
              </div>
            </div>
      </section>


      <section>
         <div class="container py-5">

            <form class="mb-4">
               <div class="d-lg-flex">
                  <div class="w-lg-75 me-lg-3 mb-3 mb-lg-0">
                     <select name="tipo" class="filtro-select" required>
                         <option value="">Buscar com</option>
                         <option value="CPF" @if($tipo == 'CPF') selected @endif>CPF</option>
                         <option value="R" @if($tipo == 'R') selected @endif>Número do registro</option>
                     </select>
                  </div>
                  <div class="w-100 me-lg-3 mb-3 mb-lg-0">
                     <input type="text" name="value" class="filtro-search-curso rounded" placeholder="Digite" required value="{{ $value }}">
                  </div>
                  <div class="w-100">
                     <button type="submit" name="sent" value="ok" class="d-block w-lg-75 btn-azul rounded px-4 py-2-5 weight-700 font-poppins ls-05 hover-d-none border-0">
                        CONSULTAR
                     </button>
                  </div>
               </div>
            </form>

            @if(isset($_GET['sent']))

              <div class="mt-3">
                 @if(isset($aluno))
                    <div class='alert font-poppins alert-secondary'><b>Aluno:</b> {{ $aluno->nome }}</div>
                 @else
                    @if($tipo == 'CPF')
                       <div class='alert font-poppins alert-danger'><b>Ops!</b> Aluno não encontrado.</div>
                    @else
                       <div class='alert font-poppins alert-danger'><b>Ops!</b> Registro de conclusão não encontrado.</div>
                    @endif
                 @endif
              </div>

               <div class="table-responsive mt-3">

                  @if(count($trilhas) > 0)
                     <table class="table">
                        <thead>
                           <tr>
                              <th scope="col" class="border-th font-poppins weight-600 color-titulo fs-15">Curso</th>
                              <th scope="col" class="border-th font-poppins weight-600 color-titulo fs-15">Data de Conclusão</th>
                              <th scope="col" class="border-th font-poppins weight-600 color-titulo fs-15">Data de Colação de Grau</th>
                              <th scope="col" class="border-th font-poppins weight-600 color-titulo fs-15">Número de Registro</th>
                           </tr>
                        </thead>
                        <tbody>
                           @foreach($trilhas as $trilha)
                              <tr class=" @if($trilha->data_conclusao) table-success @else table-warning @endif ">
                                 <td class="font-poppins fs-14">{{ $trilha->nome }}</td>
                                 <td class="font-poppins fs-14">{!! \App\Models\Util::replaceDatePt($trilha->data_conclusao) ?? '<i>Ainda não concluiu</i>' !!}</td>
                                 <td class="font-poppins fs-14">{!! \App\Models\Util::replaceDatePt($trilha->data_colacao_grau) ?? '<i>Não definido</i>' !!}</td>
                                 <td class="font-poppins fs-14">{!! $trilha->codigo_certificado ?? '<i>Não definido</i>' !!}</td>
                              </tr>
                           @endforeach
                        </tbody>
                     </table>
                  @endif

                  @if($tipo == 'CPF' && count($trilhas) == 0 && isset($aluno))
                     <div class='alert font-poppins alert-warning'><b>Ops!</b> Não identificamos nenhum curso matriculado para este aluno.</div>
                  @endif

               </div>

            @endif
            
         </div>
      </section>

   </main>

   @include('sites-proprios/faro/componentes/footer')

   <a href="https://api.whatsapp.com/send?phone=55{{preg_replace('/\D/', '', $plataforma->whatsapp)}}&text=Olá, vim através do Site {{ $plataforma->nome }}" target="_blank" class="text-white fs-30">
       <div class="fixed-bottom right-0 me-3 mb-3 z-3">
           <div class="square-60 rounded-circle bg-wpp flex-center box-shadow-wpp">
               <i class="fab fa-whatsapp"></i>
           </div>
       </div>
   </a>

   <script src="https://cdn.jsdelivr.net/npm/uikit@3.5.9/dist/js/uikit.min.js"></script>
   <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-U1DAWAznBHeqEIlVSCgzq+c9gqGAJn5c/t99JyeKa9xxaYpSvHU5awsuZVVFIhvj" crossorigin="anonymous"></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>
   <script>

      window.onscroll = function() {myFunction()};

      var header = document.getElementById("menu");
      var sticky = header.offsetTop;

      function myFunction() {
        if (window.pageYOffset > sticky) {
          header.classList.add("fixed-top");
        } else {
          header.classList.remove("fixed-top");
        }
      }
   </script>

</body>
</html>