<div>

	<section>

		<div class="mb-3 d-flex justify-content-end align-items-center">

			@if($curso->publicar == 'S')
				<a href="{{ route('admin.curso.editar-secoes', ['id' => $curso->id]) }}" class="btn btn-primary fs-13 py-1-5">
					Seções descritivas
				</a>
				<a href="{{ route('admin.curso.editar-corpodocente', ['id' => $curso->id]) }}" class="btn btn-primary fs-13 py-1-5 my-2 my-lg-0 mx-lg-3">
					Corpo Docente
				</a>
			@endif
			<a href="{{ route('admin.curso.modulo.lista', ['id' => $curso->id]) }}" class="aclNivel3 btn btn-primary fs-13 py-1-5">
				Gerenciar Conteúdo do Curso
			</a>

		</div>

		<div class="section-title rounded-bottom-0 d-flex justify-content-between align-items-center">
			{{ $curso->referencia }}
			<div>
				<a href="#" data-bs-toggle="modal" data-bs-target="#ModalMetaDados" class="btn btn-info me-2 fs-14 mb-1 py-1 px-2-3">
					<i class="fa fa-align-left"></i>
				</a>
				<a href="/admin/curso/editar/{{$curso->id}}" class="aclNivel2 btn btn-warning text-dark fs-14 mb-1 py-1 px-2">
					<i class="fa fa-edit"></i>
				</a>
				<a href="#" data-bs-toggle="modal" data-bs-target="#ModalDeletarCurso" class="aclNivel3 btn btn-danger mb-1 fs-15 py-1 px-2 ms-2">
					<i class="fa fa-trash"></i>
				</a>
			</div>
		</div>

		<div class="bg-white border p-4 d-lg-flex justify-content-between align-items-center">

			<div class="mb-2 mb-lg-0">
				<p class="mb-0 weight-600 color-444">Publicado</p>
				<div>
					@if($curso->publicar == 'S')
                     <a href="/curso/{{ $curso->slug }}" target="_blank" class="hover-d-none">
                        <div class="bg-primary text-white rounded h-40px d-flex align-items-center justify-content-center w-150px">
                           <div class="weight-500 fs-13">
                              <span>
                                 Sim
                              </span>
                           </div>
                           <div>
                              <div class="mx-2-3" style="width: 2.5px;height: 40px; background: #000; opacity: .2;"></div>
                           </div>
                           <div class="weight-500 fs-13"> 
                              <span class="me-1">Visualizar</span>
                              <i class="fas fa-external-link-alt"></i>
                           </div>
                        </div>
                     </a>
                     @else
                        <div class="bg-secondary text-white px-3 rounded h-40px d-flex align-items-center justify-content-center">
                           <div class="weight-500 fs-13">
                              <span>
                                 Não
                              </span>
                           </div>
                        </div>
                     @endif
				</div>
			</div>

			<div class="mb-2 mb-lg-0">
				<p class="mb-0 weight-600 color-444">Nome Comercial</p>
				<p class="mb-0">
					{{ $curso->nome }}
				</p>
			</div>

			<div class="mb-2 mb-lg-0">
				<p class="mb-0 weight-600 color-444">Certificado de Conclusão</p>
				@if($certificado)
					<a href="{{ route('admin.certificado.info', ['id' => $certificado->id]) }}" class="color-blue-info-link">
						{{ $certificado->referencia }}
					</a>
				@else
					<p class="mb-0">
						Não definido
					</p>
				@endif
			</div>

			<div class="mb-2 mb-lg-0">
				<p class="mb-0 weight-600 color-444">Quantidade de Alunos</p>
				<p class="mb-0">
					@if($countAlunos > 0)
						{{ $countAlunos }}
					@else
						Não há nenhum aluno neste curso
					@endif
				</p>
			</div>

		</div>

		<div class="bg-white border p-4 d-lg-flex justify-content-between">

			<div class="mb-2 mb-lg-0">
				<p class="mb-0 weight-600 color-444">Categorias</p>
				@if(count($categorias) > 0)
					<ul class="list-group list-group-flush">
				  		@foreach($categorias as $categoria)
				  			<li class="list-group-item">{{ $categoria }}</li>
				  		@endforeach
				  	</ul>
				@else
					<p class="mb-0 font-nunito">Não definido</p>
				@endif
			</div>

		</div>

		<div class="bg-white border p-4 d-lg-flex justify-content-between">

			<div class="mb-2 mb-lg-0">
				<p class="mb-0 weight-600 color-444">Turmas</p>
				@if(count($turmas) > 0)
					<ul class="list-group list-group-flush">
				  		@foreach($turmas as $turma)
				  			<a href="{{ route('admin.turma.info', ['id' => $turma->id]) }}" class="list-group-item">{{ $turma->nome }}</a>
				  		@endforeach
				  	</ul>
				@else
					<p class="mb-0 font-nunito">Não definido</p>
				@endif
			</div>

		</div>

	</section>

	<div class="modal fade" id="ModalMetaDados" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	  	<div class="modal-dialog modal-dialog-centered" role="document">
	    	<div class="modal-content">
	      		<div class="modal-header">
	        		<h3 class="modal-title">Metadados</h3>
	    			<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
	      		</div>
	      		<div class="modal-body">

					<ul class="list-group list-group-flush">
	  					<li class="list-group-item">
							<span class="mb-0 weight-600 color-444">Cadastrante:</span>
							@if($cadastrante)
								<a href="/admin/colaborador/info/{{$cadastrante->id}}" class="color-blue-info-link">
									{{ $cadastrante->nome }}
								</a>
							@else
								Não definido
							@endif
	  					</li>   
	  					<li class="list-group-item">
							<span class="mb-0 weight-600 color-444">Status:</span>
							@if($curso->status == 0)
								<span class="text-success weight-700">Ativo</span>
							@else
								<span class="text-danger weight-700">Desativado</span>
							@endif
	  					</li>
	  					<li class="list-group-item">
							<span class="mb-0 weight-600 color-444">Data de Cadastro:</span>
							@if($curso->created_at)
								<span class="mb-0">{{ $curso->created_at->format('d/m/Y H:i') ?? null }} ({{$curso->created_at->diffForHumans()}}) </span>
	  						@else
	  							Não definido
	  						@endif
	  					</li>   
	  				</ul>

	      		</div>
	    	</div>
	  	</div>
	</div>

	<form wire:submit.prevent="delete" method="POST">
		<div wire:ignore class="modal fade" id="ModalDeletarCurso" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  		<div class="modal-dialog modal-dialog-centered">
	    		<div class="modal-content">
	      			<div class="modal-header">
	        			<h3 class="modal-title" id="exampleModalLabel">Deletar Curso</h3>
	        			<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
	      			</div>
	      			<div class="modal-body text-center py-5">
	        			<p class="m-0 fs-16">
	        				Você tem certeza que deseja deletar este curso?
	        				<br>
	        				Todas as informações serão perdidas definitivamente.
	        			</p>
	      			</div>
	      			<div class="modal-footer">
	      				<div class="d-flex w-100">
		      				<input type="password" wire:model="password_to_delete" class="w-100 form-control p-form me-3" placeholder="Digite sua senha" required>
		        			<button type="submit" class="btn btn-danger weight-600">Confirmar</button>
	    				</div>
	      			</div>
	    		</div>
	  		</div>
		</div>
	</form>

	@include('livewire.util.toast')

</div>

