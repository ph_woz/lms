<?php

namespace App\Http\Controllers\SiteProprio;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Banner;
use App\Models\Trilha;
use App\Models\SecaoDescritiva;
use App\Models\Categoria;
use App\Models\Depoimento;
use App\Models\PaginaConteudo;

class CedetepController extends Controller
{
    public function index()
    {
        $banners = Banner::plataforma()->ativo()->orderBy('ordem')->get();
        $allCursos = Trilha::plataforma()->ativo()->publico()->orderBy('nome')->get();
        $categorias = Categoria::plataforma()->ativo()->publico()->orderBy('nome')->get();
        $depoimentos = Depoimento::plataforma()->ativo()->orderBy('id','desc')->get();

        $seo = PaginaConteudo::plataforma()->where('url','/')->select('plataforma_id','seo_title','seo_keywords','seo_description')->first();

        if(request('sentContato'))
        {
            if(request('check'))
            {
                return back()->with('danger', 'SPAM Detected!');
            }

            \App\Models\Contato::create(request()->all() + [
                'plataforma_id' => $seo->plataforma_id,
                'tipo' => 'L',
                'assunto' => 'Solicitar Contato',
            ]);
            
            return redirect('/#pre-matricula')->with('success', 'Inscrição enviada com êxito.');
        }

        return view('sites-proprios.cedetep.home', compact('banners','allCursos','depoimentos','categorias','seo') );
    }

    public function sobreNos()
    {
        $seo = \App\Models\PaginaConteudo::plataforma()->where('url','/sobre-nos')->first();

        return view('sites-proprios.cedetep.sobre-nos', compact('seo') );
    }

    public function curso($curoSlug)
    {
        $curso = Trilha::plataforma()->ativo()->publico()->where('slug', $curoSlug)->first();

        $corpoDocente = \App\Models\CorpoDocente::plataforma()->curso()->where('registro_id', $curso->id)->get();

        $secoesDescritivas = SecaoDescritiva::plataforma()->curso()->where('registro_id', $curso->id)->orderBy('ordem')->get();

        if($curso->exibir_turmas_como_cc)
        {
            $turmasIds = \App\Models\TrilhaTurma::select('turma_id')->plataforma()->where('trilha_id', $curso->id)->get()->pluck('turma_id')->toArray();

            $turmas = \App\Models\Turma::select('nome')->plataforma()->ativo()->whereIn('id', $turmasIds)->orderBy('nome')->get();
        
        } else {

            $turmas = [];
        }

        return view('sites-proprios.cedetep.curso', compact('curso','turmas','corpoDocente','secoesDescritivas') );
    }

    public function categoria()
    {
        $plataforma_id = $this->getPlataformaId();

        $categoria = \App\Models\Categoria::select('nome','seo_title','seo_keywords','seo_description')->plataforma()->ativo()->publico()->tipo('trilha')->where('slug', \Request::segment(1))->first();

        $cursos = Trilha::select('nome','foto_miniatura','slug')
            ->where('plataforma_id', $plataforma_id)
            ->where('status', 0)
            ->where('publicar', 'S')
            ->where('nivel', $categoria->nome ?? 'N/D')
            ->orderBy('nome')
            ->get();

        return view('sites-proprios.cedetep.categoria', compact('categoria','cursos') );
    }

    public function getPlataformaId()
    {
        return session('plataforma_id') ?? \App\Models\Plataforma::dominio()->pluck('id')[0] ?? null;
    }
}