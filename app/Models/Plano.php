<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Plano extends Model
{
    protected $table = 'planos';

    public $timestamps = true;

    public $fillable = [
        'plataforma_id',
        'cadastrante_id',
        'nome',
        'valor',
        'valor_promocional',
        'renovacao_automatica',
        'publicar',
        'ordem',
        'status',
    ];

    public static  function boot()
    {
        parent::boot(); 
        static::creating(function ($model)
        {
            $model->plataforma_id  = session('plataforma_id') ?? \Auth::user()->plataforma_id ?? 1;
            $model->cadastrante_id = \Auth::user()->id ?? null;
        });
    }
    
    public function scopeAtivo($query)
    {
        return $query->where('status', 0);
    }

    public function scopePlataforma($query)
    {
        return $query->where('plataforma_id', session('plataforma_id') ?? \Auth::user()->plataforma_id);
    }

    public static function checkActive()
    {
        $check = PlataformaModulo::plataforma()->ativo()->where('url','plano')->exists();
        
        return $check;
    }
    
}
