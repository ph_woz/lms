<?php

namespace App\Http\Controllers\SiteProprio;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Banner;
use App\Models\Depoimento;
use App\Models\PaginaConteudo;
use App\Models\PlataformaIntegracao;

class CefaepController extends Controller
{
    public function index()
    {
        $banner = Banner::plataforma()->ativo()->select('link')->orderBy('id','desc')->pluck('link')[0] ?? null;

        $seo = PaginaConteudo::plataforma()->where('url','/')->select('plataforma_id','seo_title','seo_keywords','seo_description')->first();

        $fotosAlunosFormados = Depoimento::select('foto')->plataforma()->ativo()->orderBy('id','desc')->get()->pluck('foto')->toArray();

        if(request('sentContato'))
        {
            if(request('check'))
                return back()->with('danger', 'SPAM Detected!');

            \App\Models\Contato::create(request()->all() + [
                'plataforma_id' => $seo->plataforma_id,
                'tipo' => 'L',
            ]);

            if(request('assunto') == 'Fale Conosco')
                return redirect('/#contato')->with('success', 'Inscrição enviada com êxito.');
            else
                return redirect('/#pre-matricula')->with('success_prematricula', 'Inscrição enviada com êxito.');
        }

    	return view('sites-proprios.cefaep.home', compact('banner','seo','fotosAlunosFormados') );
    }

}