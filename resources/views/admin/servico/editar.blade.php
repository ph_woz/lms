@extends('layouts.admin.master')

@section('css')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.14.0-beta2/dist/css/bootstrap-select.min.css">
@endsection

@section('content')
    
<div>

    <div class="mb-3 d-flex justify-content-end">
	    <a href="{{ route('admin.servico.info', ['id' => $servico->id ?? null]) }}" class="btn bg-base text-white">
	        <i class="fa fa-eye"></i>
	    </a>
	  </div>

    <form method="POST" class="mb-4">
    @csrf

		<section class="mb-4-5">
			
			<a href="#boxInfoPrincipal" data-bs-toggle="collapse" aria-expanded="false" class="hover-d-none">
				<div class="section-title">
					Informações Principais
				</div>
			</a>
			<hr/>
			
			<div class="collapse show" id="boxInfoPrincipal">

		    	<div class="d-lg-flex mb-3">

			    	<div class="w-lg-75 me-lg-3 mb-3 mb-lg-0">
			    		<label>Nome</label>
			    		<input type="text" name="nome" class="form-control p-form" placeholder="Referência" required value="{{ old('nome') ?? $servico->nome }}">
			    	</div>

			    	<div class="w-lg-25 me-lg-3 mb-3 mb-lg-0">
			    		<label>Duração</label>
			    		<input type="text" name="duracao" class="mask_number form-control p-form" placeholder="Em minutos" value="{{ old('duracao') ?? $servico->duracao }}">
			    	</div>

			    	<div class="w-lg-60 me-lg-3 mb-3 mb-lg-0">
			    		<label>Preço</label>
			    		<div class="d-flex w-100">
				    		<input type="text" name="valor" class="mask_valor form-control p-form rounded-right-0" placeholder="R$" value="{{ old('valor') ?? $servico->valor }}">
							<select name="tipo_valor" class="w-lg-80 select-form rounded-left-0">
								<option value="F">Fixo</option>
								<option value="A" @if(old('tipo_valor') == 'A') selected @endif>A partir</option>
							</select>
						</div>
			    	</div>

			    	<div class="w-lg-25 me-lg-3 mb-3 mb-lg-0">
			    		<label>Publicar</label>
			    		<select name="publicar" class="select-form">
			    			<option value="S">Sim</option>
			    			<option value="N" @if(old('publicar') == 'N' || $servico->publicar == 'N') selected @endif>Não</option>
			    		</select>
			    	</div>

			    	<div class="w-lg-25">
			    		<label>Status</label>
			    		<select name="status" class="select-form">
			    			<option value="0">Ativo</option>
			    			<option value="1" @if(old('staus') == '1' || $servico->status == 1) selected @endif>Desativado</option>
			    		</select>

			    	</div>

			    </div>
				
				@if(\App\Models\Unidade::checkActive() === true)
				<div class="mb-3 w-100 boxUnidadeRemove">
					<label>Unidade</label>
					<select name="unidade_id" id="unidade_id" class="selectpicker" data-width="100%">
						<option value="">Selecione</option>
						@foreach($unidades as $unidade)
							<option value="{{ $unidade->id }}" @if(old('unidade_id') == $unidade->id || $servico->unidade_id == $unidade->id) selected @endif>
								{{ $unidade->nome }}
							</option>
						@endforeach
					</select>
				</div>
				@endif

		    	<div class="w-100 me-lg-3 mb-3 mb-lg-0">
		    		<label>Descrição</label>
		    		<textarea name="descricao" class="textarea-form" placeholder="Curta descrição">{{ old('descricao') ?? $servico->descricao }}</textarea>
		    	</div>

			</div>

		</section>

		<section class="mb-4-5">
			
			<a href="#boxCategorias" data-bs-toggle="collapse" aria-expanded="false" class="hover-d-none">
				<div class="section-title">
					Categorias 
				</div>
			</a>
			<hr/>

			<div class="collapse" id="boxCategorias">
				<select name="categorias[]" class="selectpicker" multiple data-width="100%" data-live-search="true" title="Selecione">
					@foreach($categorias as $categoria)
						<option value="{{ $categoria->id }}" @if(in_array($categoria->id, $categoriasSelected)) selected @endif>{{ $categoria->nome }}</option>
					@endforeach
				</select>
			</div>

		</section>

		<button type="submit" name="sent" value="ok" class="btn-add mt-3">Salvar Alterações</button>	

    </form>

</div>


@endsection

@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.14.0-beta2/dist/js/bootstrap-select.min.js"></script>
<script>

	@if(\Auth::user()->restringir_unidade == 'S' && \Auth::user()->unidade_id != null)
		jQuery("#unidade_id option:contains('Selecione')").remove();
	@endif
	
	$('.mask_number').mask('00000');

    $(".mask_valor").mask('#.##0,00', {
      reverse: true
    });

</script>
@endpush