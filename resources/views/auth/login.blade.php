@extends('layouts.auth')

@section('content')
<div class="h-100vh">
    <div class="container h-100">
        <div class="row align-items-center h-100">
            <div class="col-md-5 mx-auto">
                <div class="boxLogin bg-white px-4">

                    <div class="card-body">
                        <form method="POST" action="{{ route('login') }}">
                            @csrf

                            <div class="text-center mb-4-5 mt-2" id="boxLogotipoLogin">
                                <a href="/">
                                    <img src="{{ $plataforma->logotipo }}" id="imgLogotipoLogin" class="img-fluid" width="80">
                                </a>
                            </div>

                            <h1 class="text-center color-555 color-555 fs-19">
                                Faça seu Login
                            </h1>
                            <hr/>

                            @if($plataforma->forma_login == 'email')
                            <div class="input-group mb-2">

                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <i class="fa fa-envelope color-555"></i>
                                    </div>
                                </div>

                                <input type="email" class="form-control bg-f9 weight-600 color-555 ls-5 p-4 @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="Digite seu email" autofocus>
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            @else
                            <div class="input-group mb-2">

                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <i class="fas fa-address-card color-555"></i>
                                    </div>
                                </div>

                                <input type="text" class="cpf form-control bg-f9 weight-600 color-555 ls-5 p-4" name="cpf" value="{{ old('cpf') }}" required autocomplete="cpf" placeholder="Digite seu CPF" autofocus>
                            </div>
                            @endif

                            <div class="input-group">
                                    
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <i class="fa fa-key color-555"></i>
                                    </div>
                                </div>

                                <input id="password" type="password" class="form-control bg-f9 weight-600 color-555 ls-5 p-4 @error('password') is-invalid @enderror" name="password" required placeholder="Digite sua senha" autocomplete="current-password">
                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="mb-4 mt-2 text-right">
                                <a href="{{ route('recuperar-senha') }}" class="weight-600 color-555 fs-14 text-decoration-none">
                                    Esqueceu sua senha?
                                </a>
                            </div>

                            <div class="text-center mt-4-5">
                                <button type="submit" class="btn btn-primary weight-600 w-100 py-2">
                                    Entrar
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection