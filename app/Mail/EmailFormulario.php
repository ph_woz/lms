<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Http\Request;
use App\Models\Plataforma;
use App\Models\Contato;

class EmailFormulario extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $contato;

    public function __construct(Contato $contato)
    {
        $this->contato = $contato;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $plataforma = Plataforma::plataforma()->first();

        $assunto = ' Novo contato recebido! | ' . $plataforma->nome;

        return $this->view('mail.formulario')
                ->subject($assunto)
                ->from($plataforma->email, $plataforma->nome)
                ->replyTo($this->contato->email, $this->contato->nome)
                ->with([
                    'id'             => $this->contato->id,
                    'nome'           => $this->contato->nome,
                    'email'          => $this->contato->email,
                    'assunto'        => $this->contato->assunto,
                ]);
    }
}
