<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAvaliacoesAlunosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('avaliacoes_alunos', function (Blueprint $table) {
            $table->id();
            $table->integer('plataforma_id');
            $table->integer('curso_id');
            $table->integer('turma_id');
            $table->integer('avaliacao_id');
            $table->integer('aluno_id');
            $table->string('nota')->nullable();
            $table->string('status', 1)->nullable();
            $table->integer('n_tentativa')->nullable()->default(0);
            $table->string('questoesIds')->nullable();
            $table->dateTime('data_finalizada')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('avaliacoes_alunos');
    }
}
