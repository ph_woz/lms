<?php

namespace App\Http\Livewire\Admin\Contrato;

use Livewire\WithPagination;
use Livewire\Component;
use App\Models\Contrato;

class Lista extends Component
{
    use WithPagination;

    protected $paginationTheme = 'bootstrap';

    public $search;
    public $status = 0;
    public $ordem;

    protected $queryString = ['search','status','ordem'];

    public function render()
    {
        $contratos = Contrato::select('id','referencia')->plataforma()->where('status', $this->status);

        if($this->search)
        {
            $contratos = $contratos->where('referencia', 'like', '%'.$this->search.'%');
        }

        $countContratos = $contratos->count();


        if($this->ordem == 'desc' || $this->ordem == 'asc')
        {
            $contratos = $contratos->orderBy('id', $this->ordem);
        
        } else {

            $contratos = $contratos->orderBy('referencia');
        }


        $contratos = $contratos->paginate(10);


        return view('livewire.admin.contrato.lista', [
            'contratos'      => $contratos,
            'countContratos' => $countContratos,
        ]);
    }
}
