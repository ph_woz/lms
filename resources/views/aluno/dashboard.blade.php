@extends('layouts.aluno')
@section('content')
@section('seo_title', 'Dashboard')

@if($checkComunicadosNaoLidos === true)
<script>
	localStorage.setItem('hasComunicado', true);
</script>
@endif

<section>
	<img src="{{ $plataforma->jumbotron ?? asset('images/jumbotron.png') }}" class="img-fluid w-100 object-fit-cover" style="max-height: 300px;">
</section>

<section class="container-fluid px-xl-4 z-3 position-relative">

	<div id="boxCursosEmAndamento" class="mb-4 px-4 pt-4 pb-3 rounded-10 box-shadow mt--15px bg-14">

		<h5 class="title mb-4">Em andamento</h5>

		<div class="row">

			@foreach($turmasEmAndamento as $curso)
			<div class="col-lg-3 mb-4">
				<a href="{{ route('aluno.aulas', ['slug' => $curso->slug, 'curso_id' => $curso->id]) }}" class="color-b7 hover-white-to-dark fs-16 hover-d-none">
					<img src="{{ $curso->foto_miniatura ?? asset('images/no-image.jpeg') }}" class="figure-img img-fluid rounded w-100 h-150 object-fit-cover">
					<div class="px-1">
						<span>
							{{ $curso->nome }}
						</span>
					</div>
				</a>
			</div>
			@endforeach

		</div>

	</div>

	<div id="boxCursosDisponiveis" class="mb-4 px-4 pt-4 pb-3 rounded-10 box-shadow bg-14">

		<h5 class="title mb-4">Disponíveis</h5>

		<div class="row">

			@foreach($turmasDisponiveis as $curso)
			<div class="col-lg-3 mb-4">
				<a href="{{ route('aluno.aulas', ['slug' => $curso->slug, 'curso_id' => $curso->id]) }}" class="color-b7 hover-white-to-dark fs-16 hover-d-none">
					<img src="{{ $curso->foto_miniatura ?? asset('images/no-image.jpeg') }}" class="figure-img img-fluid rounded w-100 h-150 object-fit-cover">
					<div class="px-1">
						<span>
							{{ $curso->nome }}
						</span>
					</div>
				</a>
			</div>
			@endforeach

		</div>

	</div>

	<div id="boxCursosConcluido" class="mb-4 px-4 pt-4 pb-3 rounded-10 box-shadow bg-14">

		<h5 class="title mb-4">Concluído</h5>

		<div class="row">

			@foreach($turmasConcluidas as $curso)
			<div class="col-lg-3 mb-4">
				<a href="{{ route('aluno.aulas', ['slug' => $curso->slug, 'curso_id' => $curso->id]) }}" class="color-b7 hover-white-to-dark fs-16 hover-d-none">
					<img src="{{ $curso->foto_miniatura ?? asset('images/no-image.jpeg') }}" class="figure-img img-fluid rounded w-100 h-150 object-fit-cover">
					<div class="px-1">
						<span>
							{{ $curso->nome }}
						</span>
					</div>
				</a>
			</div>
			@endforeach

		</div>

	</div>

	<div id="boxTrilhas" class="mb-4 px-4 pt-4 pb-3 rounded-10 box-shadow bg-14">

		<h5 class="title mb-4" id="title-h5-trilhas">Trilhas</h5>

		<div class="row">

			@foreach($trilhas as $trilha)
			<div class="col-lg-3 mb-4">
				<a href="{{ route('aluno.trilha', ['slug' => $trilha->slug, 'trilha_id' => $trilha->id]) }}" class="color-b7 hover-white-to-dark fs-16 hover-d-none">
					<img src="{{ $trilha->foto_miniatura ?? asset('images/no-image.jpeg') }}" class="figure-img img-fluid rounded w-100 h-150 object-fit-cover">
					<div class="px-1">
						<span>
							{{ $trilha->nome }}
						</span>
					</div>
				</a>
			</div>
			@endforeach

		</div>
	</div>

</section>

<form action="{{ route('aluno.dashboard.aceitar_termos') }}" method="POST">
@csrf
	<div class="modal fade" id="ModalTermosDeUso" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  		<div class="modal-dialog modal-dialog-scrollable modal-lg">
    		<div class="modal-content">
      			<div class="modal-header">
        			<h3 class="modal-title" id="exampleModalLabel">Termos de Uso</h3>
      			</div>
      			<div class="modal-body">

					<div class="px-1 pt-3">
						{!! nl2br($termosDeUsoParaAceite ?? null) !!}
					</div>

      			</div>
      			<div class="modal-footer">

			      	<div class="d-lg-flex align-items-center">

			      		<div class="mb-4 mb-lg-0">
					      	<label for="checkTermosDeUso" class="color-444 flex-center me-5">
					      		<input type="checkbox" required id="checkTermosDeUso" class="square-15">
					      		<span class="ms-2">Li e aceito os Termos de Uso</span>
					      	</label>
					    </div>

				        <button type="submit" name="aceitar_termos" value="S" class="btn btn-primary weight-600 py-2">
				        	<i class="fas fa-check-circle"></i>
				        	&nbsp;Confirmar
				        </button>

				    </div>

      			</div>
    		</div>
  		</div>
	</div>
</form>

@endsection


@push('scripts')

	@if($termosDeUsoParaAceite && \Auth::user()->aceita_termos != 'S')

		<script>
			var myModal = new bootstrap.Modal(document.getElementById("ModalTermosDeUso"), {});
			document.onreadystatechange = function () {
			  myModal.show();
			};
		</script>

		@section('css')
			<style>
				::-webkit-scrollbar-track { background-color: #ccc!important; }
				::-webkit-scrollbar { width: 8px; background: #ccc!important; }
				::-webkit-scrollbar-thumb { background: #444!important; }
			</style>
		@endsection

	@endif

@endpush