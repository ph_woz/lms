<?php

namespace App\Http\Livewire\Admin\Curso\Modulo;

use Livewire\WithPagination;
use Livewire\Component;
use App\Models\Curso;
use App\Models\CursoModulo;
use App\Models\Categoria;
use App\Models\ModuloCategoria;

class Lista extends Component
{
    use WithPagination;

    protected $paginationTheme = 'bootstrap';

    public $curso_id;
    public $search;
    public $status = 0;
    public $ordem;

    // add
    public $add_nome;
    public $add_status = 0;
    public $add_ordem;

    protected $queryString = ['search','status','ordem'];

    public function mount($curso_id)
    {
        $this->curso_id = $curso_id;
    }

    public function render()
    {
        $modulos = CursoModulo::plataforma()->where('curso_id', $this->curso_id);

        if($this->search)
            $modulos = $modulos->where('nome', 'like', '%'.$this->search.'%');

        if($this->status !== null)
            $modulos = $modulos->where('status', $this->status);


        $countModulos = $modulos->count();
        

        $modulos = $modulos->orderBy('ordem')->paginate(10);


        $curso = Curso::plataforma()->where('id', $this->curso_id)->first();

        return view('livewire.admin.curso.modulo.lista', [
            'curso'        => $curso,
            'modulos'      => $modulos,
            'countModulos' => $countModulos,
        ]);
    }

    public function cadastrarModulo()
    {
        CursoModulo::create([
            'curso_id' => $this->curso_id,
            'nome'     => $this->add_nome,
            'ordem'    => $this->add_ordem,
            'status'   => $this->add_status,
        ]);

        $this->add_nome   = null;
        $this->add_status = 0;
        $this->add_ordem  = null;

        session()->flash('success-livewire', 'Módulo cadastrado com êxito.');

        $this->dispatchBrowserEvent('dissmiss-modal');
        $this->dispatchBrowserEvent('toast');

    }

}
