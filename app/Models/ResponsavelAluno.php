<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ResponsavelAluno extends Model
{
    use HasFactory;

    protected $table = 'responsaveis_alunos';

    public $timestamps = false;

    public $fillable = [
        'plataforma_id',
        'responsavel_id',
        'aluno_id',
        'grau_parentesco',
    ];

    public static  function boot()
    {
        parent::boot(); 
        static::creating(function ($model) {
            $model->plataforma_id = session('plataforma_id') ?? \Auth()->user()->plataforma_id ?? Plataforma::dominio()->pluck('id')[0] ?? null;
        });
    }

    public function scopePlataforma($query)
    {
        return $query->where('plataforma_id', session('plataforma_id') ?? Plataforma::dominio()->pluck('id')[0] ?? null);
    }
}
