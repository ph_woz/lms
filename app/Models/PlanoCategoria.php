<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PlanoCategoria extends Model
{
    protected $table = 'planos_categorias';

    public $timestamps = false;

    public $fillable = [
    	'plataforma_id',
    	'categoria_id',
    	'plano_id', 
    ];

    public static  function boot()
    {
        parent::boot(); 
        static::creating(function ($model) {
            $model->plataforma_id = session('plataforma_id') ?? \Auth()->user()->plataforma_id;
        });
    }

    public function scopePlataforma($query)
    {
        return $query->where('plataforma_id', session('plataforma_id') ?? \Auth::user()->plataforma_id);
    }
}
