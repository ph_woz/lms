<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Storage;

class ColaboradorController extends Controller
{
    public function lista()
    {
        return view('admin.colaborador.lista');
    }
    
    public function info($user_id)
    {
        return view('admin.colaborador.info', compact('user_id') );
    }

    public function add()
    {
    	return view('admin.colaborador.add');
    }

    public function editar($user_id)
    {
        return view('admin.colaborador.editar', compact('user_id') );
    }

    public function setFotoPerfil($user_id)
    {
        $file = request()->file('foto');

        if($file)
        {
            $diretorio = 'plataformas/'.session('plataforma_id').'/users/'.$user_id;
            $filename = 'foto_perfil.'.$file->getClientOriginalExtension();

            $path = Storage::putFileAs($diretorio, $file, $filename, 'public');
            $linkStorage = \App\Models\Util::getLinkStorage();
            $caminho = $linkStorage . $path;

            User::plataforma()->where('id', $user_id)->update(['foto_perfil' => $caminho]);
        }

        return back()->with('success', 'Foto de Perfil atualizada com êxito.');
    }

}