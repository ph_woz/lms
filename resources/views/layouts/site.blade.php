<!DOCTYPE html>
<html lang="pt-br">
<head>
    
  <!-- Google tag (gtag.js) -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=G-MREB450HCT"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'G-MREB450HCT');
  </script>
  
    {!! $plataforma->scripts_start_head !!}

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('seo_title')</title>
    <meta name="description" content="@yield('seo_description')">
    <meta name="keywords" content="@yield('seo_keywords')">
    <meta name="robots" content="index, follow">
    <meta name='author' content='WOZCODE | Pedro Henrique' />
    <link href="{{ Request::url() }}" rel="canonical">
    
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">

    <link rel="stylesheet" href="{{ asset('css/util.css') }}">
    <link rel="stylesheet" href="{{ asset('css/pages.css') }}">
    <link rel="shortcut icon" href="{{ $plataforma->favicon }}">

    <meta property="og:locale" content="pt_BR">
    <meta property="og:type" content="website">
    <meta property="og:image" content="{{ $plataforma->logotipo }}">
    <meta property="og:url" content="{{ Request::url() }}">
    <meta property="og:description" content="@yield('seo_description')">
    <meta property="og:title" content="@yield('seo_title')">

    @yield('css')

    {!! $plataforma->scripts_final_head !!}

</head>
<body>

   {!! $plataforma->scripts_start_body !!}

   <header id="headerMenu" class="layoutSiteHeader">
        <nav class="navbar navbar-expand-lg navbar-light bg-white box-shadow py-lg-0 fixed-top" id="navMenu">
            <div class="container-fluid px-xl-7">
                @if($plataforma->logotipo)
                    <a href="/">
                      <img src="{{ $plataforma->logotipo }}" alt="Logotipo {{ $plataforma->nome }}" width="75" id="logotipoMenu" class="img-fluid">
                    </a>
                 @else
                    <a class="navbar-brand hover-d-none" href="/">
                       <span>
                          {{ $plataforma->nome }}
                       </span>
                    </a>
                 @endif
                <button class="navbar-toggler border-0 outline-0" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ms-auto mb-2 mb-lg-0 align-items-lg-center">
                        <li class="nav-item">
                            <a class="menu-link" href="/cursos">Cursos</a>
                        </li>
                        <li class="nav-item">
                            <a class="menu-link" href="/sobre-nos">Sobre nós</a>
                        </li>
                        <li class="nav-item">
                            <a class="menu-link" href="/contato">Contato</a>
                        </li>
                        <li class="nav-item">
                            <a class="menu-link" id="menu-link-matriculese" href="#" data-bs-toggle="modal" data-bs-target="#ModalMatriculeSe">Matricule-se!</a>
                        </li>
                        <li class="nav-item">
                            <a id="btnMenuAreaAluno" class="menu-link btn btn-primary fs-14 text-light hover-d-none px-3 py-2 font-poppins" href="/login">
                                <i class="fas fa-user-graduate"></i>&nbsp;
                                Área do Aluno
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
   </header>

   <main>

        @yield('content')

   </main>

    <form method="POST" action="/">
    @csrf
     <div id="ModalMatriculeSe" class="modal fade" tabindex="-1">
       <div class="modal-dialog modal-lg modal-dialog-centered">
         <div class="modal-content bg-f9 border-0">
           <div class="modal-header bg-blue-dark border-0">
             <h4 class="modal-title text-light weight-600 font-poppins">Faça sua Inscrição!</h4>
             <button type="button" data-bs-dismiss="modal" class="text-white bg-transparent border-0 fs-20">
                 <i class="fas fa-times"></i>
             </button>
           </div>
           <div class="modal-body bg-f9 mt-3">
              
              <input type="hidden" name="assunto" value="Matricule-se!">

              <div class="d-lg-flex">
                 <input type="text" name="nome" class="form-control p-form" placeholder="NOME" required>
                 <input type="email" name="email" class="mx-lg-3 my-3 my-lg-0 form-control p-form" placeholder="EMAIL" required>
                 <input type="text" name="celular" class="w-lg-55 form-control p-form" placeholder="(DDD) TELEFONE" required>
              </div>

             <div class="mt-3">
               <label class="cursor-pointer font-poppins fs-14 color-base-blue">
                  <input type="checkbox" class="" name="consentimento" value="1" required> Concordo que os dados pessoais fornecidos acima serão utilizados para envio de conteúdo informativo, analítico e publicitário sobre produtos, serviços e assuntos gerais, nos termos da Lei Geral de Proteção de Dados.
               </label>
            </div>

           </div>
           <div class="modal-footer">
             <button type="submit" name="sentContato" value="S" class="btn btn-primary weight-600 shadow px-4 py-2">Enviar</button>
           </div>
         </div>
       </div>
     </div>
    </form>

    <footer id="layoutSiteFooter">
        <div class="bg-white py-5 px-xl-7 px-4 border-top">
           <div class="container-fluid mb-4">
              <div class="row">

                 <div class="col-lg-4 mb-5 mb-lg-0">
                 
                    <p class="mb-0 pb-0 font-poppins">
                       Contato
                    </p>
                    <hr class="mt-2 border border-secondary" />

                    @if($plataforma->horario)
                    <p>
                        <i class="far fa-clock"></i>
                        <span class="font-poppins fs-14">
                            {{ $plataforma->horario }}
                        </span>
                    </p>
                    @endif

                    @if($plataforma->endereco)
                    <p>
                        <i class="fas fa-map-marker-alt"></i>
                        <span class="font-poppins fs-14">
                            {{ $plataforma->endereco }}
                        </span>
                    </p>
                    @endif

                    <p>
                        <i class="fa fa-envelope"></i>
                        <span class="font-poppins fs-14">
                            {{ $plataforma->email ?? 'meuemail@exemplo.com' }}
                        </span>
                    </p>

                    @if($plataforma->whatsapp)
                    <div class="mb-3">
                        <a class="font-poppins text-dark hover-d-none" href="https://api.whatsapp.com/send?phone=55{{preg_replace('/\D/', '', $plataforma->whatsapp)}}&text=Olá, vim através do Site {{ $plataforma->nome }}.">
                            <i class="fab fa-whatsapp"></i>
                            <span class="font-poppins fs-14">
                                {{ $plataforma->whatsapp }}
                            </span>
                        </a>
                    </div>
                    @endif

                    @if($plataforma->telefone)
                    <div>
                        <a class="font-poppins text-dark hover-d-none" href="tel:+55{{preg_replace('/\D/', '', $plataforma->telefone)}}">
                            <i class="fa fa-phone"></i>
                            <span class="font-poppins fs-14">
                                {{ $plataforma->telefone }}
                            </span>
                        </a>
                    </div>
                    @endif

                 </div>

                 <div class="col-lg-4 mb-5 mb-lg-0">
                 
                    <p class="mb-0 pb-0 font-poppins">
                       Navegação
                    </p>
                    <hr class="mt-2 border border-secondary" />

                    <div>
                        <a href="/cursos" class="text-dark font-poppins fs-14 text-decoration-none">
                            <i class="fas fa-angle-double-right color-777"></i>
                            Cursos
                        </a>
                    </div>
                    <div>
                        <a href="/sobre-nos" class="text-dark font-poppins fs-14 text-decoration-none">
                            <i class="fas fa-angle-double-right color-777"></i>
                            Sobre nós
                        </a>
                    </div>
                    <div>
                        <a href="/contato" class="text-dark font-poppins fs-14 text-decoration-none">
                            <i class="fas fa-angle-double-right color-777"></i>
                            Contato
                        </a>
                    </div>
                    <div>
                        <a href="#ModalMatriculeSe" data-bs-toggle="modal" class="text-dark font-poppins fs-14 text-decoration-none">
                            <i class="fas fa-angle-double-right color-777"></i>
                            Matricule-se
                        </a>
                    </div>
                    <div>
                        <a href="/login" class="text-dark font-poppins fs-14 text-decoration-none">
                            <i class="fas fa-angle-double-right color-777"></i>
                            Área do Aluno
                        </a>
                    </div>

                 </div>

                 <div class="col-lg-4">
                 
                    <p class="mb-0 pb-0 font-poppins">
                       Siga-nos
                    </p>
                    <hr class="mt-2 border border-secondary" />

                    <div class="d-flex">
                        <a href="{{ $plataforma->facebook }}" target="_blank" class="me-2 fs-20 hover-d-none text-decoration-none text-white">
                            <div class="square-40 bg-base bg-base-hover d-flex justify-content-center align-items-center rounded-2">
                                <i class="fab fa-facebook-f"></i>
                            </div>
                        </a>
                        <a href="{{ $plataforma->instagram }}" target="_blank" class="me-2 fs-20 hover-d-none text-decoration-none text-white">
                            <div class="square-40 bg-base bg-base-hover d-flex justify-content-center align-items-center rounded-2">
                                <i class="fab fa-instagram"></i>
                            </div>
                        </a>
                        @if($plataforma->youtube)
                        <a href="{{ $plataforma->youtube }}" target="_blank" class="me-2 fs-20 hover-d-none text-decoration-none text-white">
                            <div class="square-40 bg-base bg-base-hover d-flex justify-content-center align-items-center rounded-2">
                                <i class="fab fa-youtube"></i>
                            </div>
                        </a>
                        @endif
                    </div>

                 </div>

              </div>
           </div>
        </div>
        <div class="bg-dark py-2-5 px-xl-7 px-4">
            <div class="row text-xs-center">
                <div class="col-lg-6">
                    <p id="txtCopyright" class="mb-lg-0 mb-1 text-light text-lg-left font-poppins weight-600 fs-11 text-shadow-1">&copy; {{ $plataforma->nome }} - Todos os direitos reservados</p>
                </div>
                <div class="col-lg-6">
                    <p class="m-0 text-light text-lg-end font-poppins weight-600 fs-11 text-shadow-1">
                        Desenvolvido por <a href="https://wozcode.com" target="_blank" class="text-decoration-none text-danger">WOZCODE</a>
                    </p>
                </div>
            </div>
        </div>
    </footer>

   <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-U1DAWAznBHeqEIlVSCgzq+c9gqGAJn5c/t99JyeKa9xxaYpSvHU5awsuZVVFIhvj" crossorigin="anonymous"></script>

   {!! $plataforma->scripts_final_body !!}

   @if(session()->has('success'))
        <div id="box-toast-success" class="boxToast">
            <div id="alertSuccess" class="toast-success">
                <p class="mt-2 text-white font-inter fs-14">
                    <span class="weight-800">Sucesso!&nbsp;</span>{{ session('success') }}
                </p>
            </div>
        </div>
        <script>
            setTimeout(function() {

                document.getElementById('box-toast-success').style.display = 'none';

            }, 2500);
        </script>
   @endif

   @if(session()->has('danger'))
        <div id="box-toast-danger" class="boxToast">
            <div id="alertDanger" class="toast-danger">
                <p class="mt-2 text-white font-inter fs-14">
                    <span class="weight-800">Ops!&nbsp;</span>{{ session('danger') }}
                </p>
            </div>
        </div>
        <script>
            setTimeout(function() {

                document.getElementById('box-toast-danger').style.display  = 'none';

            }, 2500);
        </script>
   @endif

   @stack('scripts')

</body>
</html>