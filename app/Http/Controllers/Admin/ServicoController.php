<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Servico;
use App\Models\Unidade;
use App\Models\Categoria;


class ServicoController extends Controller
{
    public function lista()
    {
        return view('admin.servico.lista');
    }

    public function info($servico_id)
    {
        return view('admin.servico.info', compact('servico_id') );
    }

    public function add()
    {
        if(request()->has('sent'))
        {
            $servico = Servico::create(request()->all());

            return redirect()->route('admin.servico.info', ['id' => $servico->id])->with('success', 'Serviço cadastrado com êxito.');
        }

        $unidades   = Unidade::select('id','nome')->plataforma()->ativo()->orderBy('nome')->get();
        $categorias = Categoria::select('id','nome')->plataforma()->ativo()->tipo('Serviços')->orderBy('nome')->get();

        return view('admin.servico.add', compact('unidades','categorias') );
    }

    public function editar($id)
    {
        $servico = Servico::plataforma()->find($id);

        if(is_null($servico))
        {
            return view('admin.page-whoops', ['erro' => 'Serviço não encontrado.']);
        }

        if(request()->has('sent'))
        {
            $servico->update(request()->all());

            return back()->with('success', 'Serviço atualizado com êxito.');
        }

        $unidades   = Unidade::select('id','nome')->plataforma()->ativo()->orderBy('nome')->get();
        $categorias = Categoria::select('id','nome')->plataforma()->ativo()->tipo('Serviços')->orderBy('nome')->get();

        return view('admin.servico.editar', compact('servico','unidades','categorias') );
    }
}