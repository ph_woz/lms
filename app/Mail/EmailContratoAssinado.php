<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Http\Request;
use App\Models\Plataforma;
use App\Models\User;
use App\Models\UserContrato;

class EmailContratoAssinado extends Mailable
{
    use Queueable, SerializesModels;

    public $plataforma;
    public $user;
    public $userContrato;

    public function __construct(User $user, UserContrato $userContrato, Plataforma $plataforma)
    {
        $this->plataforma = $plataforma;
        $this->user = $user;
        $this->userContrato = $userContrato;
    }

    public function build()
    {
        $assunto = $this->userContrato->referencia . ' | Contrato Assinado por ' . $this->user->nome . ' | ' . $this->plataforma->nome;

        return $this->view('mail.contrato-assinado')
            ->subject($assunto)
            ->from($this->plataforma->email, $this->plataforma->nome)
            ->replyTo($this->plataforma->email, $this->plataforma->nome)
            ->attach($this->userContrato->pdf_assinado);
    }

}
