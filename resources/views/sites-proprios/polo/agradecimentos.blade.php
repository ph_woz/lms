<!doctype html>
<html lang="pt-br">
<head>

  <!-- Google tag (gtag.js) -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=G-MREB450HCT"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'G-MREB450HCT');
  </script>
  
   {!! $plataforma->scripts_start_head !!}

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('css/util.css') }}">
    <link rel="stylesheet" href="{{ asset('sites-proprios/polo/css/home.css') }}">

    <title>{{ $plataforma->nome }} - Agradecimentos</title>
    <link rel="shortcut icon" href="images/favicon.ico">
    <meta name="robots" content="noindex, nofollow">

    {!! $plataforma->scripts_final_head !!}

</head>
<body>
  
  {!! $plataforma->scripts_start_body !!}

  <main>

      <div class="h-100vh text-center">
        <div class="container h-100">
          <div class="row align-items-center h-100">
            <div class="col-md-6 mx-auto">
              <h1 class="display-5 text-success text-center h-100 align-items-center mt--20px">
                Obrigado!
              </h1>
              <p class="mt-3 mb-4-5 text-center fs-20 weight-600 font-opensans color-555">
                Iremos retornar o mais rápido possível.<br>
                Caso queira contato imediato, chame um consultor agora.
              </p>

             <div class="mb-3 d-xs-flex justify-content-center">
               <a href="https://api.whatsapp.com/send?phone=55{{preg_replace('/\D/', '', $plataforma->whatsapp)}}&text=Olá, vim através do Site {{ $plataforma->nome }}" target="_blank" class="hover-d-none text-d-none text-light w-xs-100">
                  <div class="bg-wpp box-shadow-wpp rounded px-4 py-3-5 d-lg-flex align-items-center">
                     <i class="d-xs-block text-shadow-2 bi bi-whatsapp fs-30 me-lg-3"></i>
                     <span class="me-lg-2 weight-600 text-shadow-2 font-inter">Fale com um Consultor pelo WhatsApp</span>
                     <span class="ms-auto weight-600 font-opensans text-shadow-2 font-inter">{{ $plataforma->whatsapp ?? '(00) 00000-0000' }}</span>
                  </div>
               </a>
            </div>

            </div>
          </div>
        </div>
      </div>

  </main>

  {!! $plataforma->scripts_final_body !!}

</body>
</html>