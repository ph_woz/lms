<!doctype html>
<html lang="pt-br">
<head>
 
  <!-- Google tag (gtag.js) -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=G-MREB450HCT"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'G-MREB450HCT');
  </script>
  
	{!! $plataforma->scripts_start_head !!}

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/uikit@3.5.9/dist/css/uikit.min.css" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/5.0.0-alpha2/css/bootstrap.min.css" integrity="sha384-DhY6onE6f3zzKbjUPRc2hOzGAdEf4/Dz+WJwBvEYL/lkkIsI3ihufq9hk9K4lVoK" crossorigin="anonymous">
    <link rel="stylesheet" href="https://lmc.wozcode.com/css/util.css">
    <link rel="stylesheet" href="{{ asset('sites-proprios/colegio-comercial/css/home.css') }}?v=2">

    <title>{{ $seo->title ?? $plataforma->nome }}</title>
    <meta name="robots" content="index, follow">
    <meta name="description" content="{{ $seo->description ?? null }}"/>
    <meta name="keywords" content="{{ $seo->keywords ?? null }}"/>
    <link rel="canonical" href="{{ Request::url() }}" />
    <link rel="shortcut icon" href="{{ $plataforma->favicon }}">
    <meta property="og:locale" content="pt_BR" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="{{ $seo->title ?? $plataforma->nome }}" />
    <meta property="og:description" content="{{ $seo->description ?? null }}" />
    <meta property="og:url" content="{{ Request::url() }}" />
    <meta property="og:site_name" content="{{ $plataforma->nome }}" />
    <meta property="og:image" content="{{ $plataforma->logotipo }}" />
    <meta name='author' content='WOZCODE | Pedro Henrique' />

	<!-- Facebook Pixel Code -->
	<script>
	!function(f,b,e,v,n,t,s)
	{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	n.callMethod.apply(n,arguments):n.queue.push(arguments)};
	if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
	n.queue=[];t=b.createElement(e);t.async=!0;
	t.src=v;s=b.getElementsByTagName(e)[0];
	s.parentNode.insertBefore(t,s)}(window, document,'script',
	'https://connect.facebook.net/en_US/fbevents.js');
	fbq('init', '716154402137612');
	fbq('track', 'PageView');
	</script>
	<noscript><img height="1" width="1" style="display:none"
	src="https://www.facebook.com/tr?id=716154402137612&ev=PageView&noscript=1"
	/></noscript>
	<!-- End Facebook Pixel Code -->

	{!! $plataforma->scripts_final_head !!}

</head>
<body class="bg-f9">

	{!! $plataforma->scripts_start_body !!}

	<header>
		<nav class="navbar navbar-expand-lg navbar-light bg-white shadow fixed-top">
		 	<div class="container-fluid px-xl-7">
			    <a class="navbar-brand" href="#inicio" uk-scroll>
			    	<img src="{{ $plataforma->logotipo }}" alt="Logotipo Colégio Comercial" width="150">
			    </a>
			    <button class="navbar-toggler outline-0 border-0" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			     	<span class="fa fa-bars"></span>
			    </button>
			    <div class="collapse navbar-collapse" id="navbarSupportedContent">
			      	<ul class="navbar-nav ml-auto mb-2 mb-lg-0">
			        	<li class="nav-item">
			         		<a class="nav-link" href="#inicio" uk-scroll>Home</a>
			        	</li>
			        	<li class="nav-item">
			         		<a class="nav-link" href="#quem-somos" uk-scroll="offset: 80;" data-toggle="collapse" data-target="#navbarSupportedContent">
			         			Quem somos
			         		</a>
			        	</li>
			        	<li class="nav-item">
			         		<a class="nav-link" href="#como-funciona" uk-scroll="offset: 20;" data-toggle="collapse" data-target="#navbarSupportedContent">
			         			Como funciona
			         		</a>
			        	</li>
			        	<li class="nav-item">
			         		<a class="nav-link" href="#contato" uk-scroll="offset:80;" data-toggle="collapse" data-target="#navbarSupportedContent">
			         			Contato
			         		</a>
			        	</li>
			        	<li class="nav-item">
			         		<a class="nav-link" href="https://colegiocomercial.com.br/f/formulario-de-matricula/12/c/135" target="_blank">
			         			Matricule-se
			         		</a>
			        	</li>
			        	<li class="nav-item ml-lg-4 mt-3 mt-lg-0">
			         		<a class="nav-link btnAreaAluno animation-grow d-inline-block" href="/login">
			         			<i class="fas fa-graduation-cap mr-1"></i>
			         			Área do Aluno
			         		</a>
			        	</li>
			        </ul>
			    </div>
		 	</div>
		</nav>
	</header>

    <main>
    	

      	<section class="pb-5 cursor-drag">

			<div class="uk-position-relative uk-visible-toggle uk-light" tabindex="-1" uk-slideshow="animation: push; autoplay: true; autoplay-interval: 5000; pause-on-hover: false; min-height: 550; max-height: 550">

			    <ul class="uk-slideshow-items h-100vh object-fit-cover">
			        <li class="h-100vh object-fit-cover">
			            <div class="uk-background-cover" uk-parallax="bgy: -100" style="background-image: url('{{ asset('sites-proprios/colegio-comercial/images/slides/1.jpg')}}');" uk-cover>
							<div class="background-overlay"></div>
				            <div class="uk-position-center uk-position-small uk-text-left uk-light px-xl-7">
				                <h2 uk-slideshow-parallax="x: 300,0,-100" class="font-poppins fs-md-50 fs-xs-35 weight-700 text-shadow-2">
				                	Colégio Comercial
				                </h2>
				                <p uk-slideshow-parallax="x: 300,0,-100" class="mt-3 mb-4-5 font-poppins line-height-1-4 fs-17 col-md-8 text-light">
				                	Através do Colégio Comercial de Votuporanga, nossos alunos podem estudar em cursos superiores ou concluir os estudos do ensino fundamental e ensino médio com o EJA. Você poderá concluir os seus estudos e acelerar sua carreira profissional com flexibilidade e seriedade. São mais de 50 anos de história e paixão pela educação.
				                </p>
				                <div uk-slideshow-parallax="x: 300,0,-100">
					         		<a href="#quem-somos" uk-scroll="offset: 80;" class="btnSaibaMais">
					         			Saiba Mais
					         		</a>
					         	</div>
				            </div>
				        </div>
			        </li>
			    </ul>

			    <ul class="uk-slideshow-nav uk-dotnav uk-flex-center mt--md-130 position-relative"></ul>

			    <div class="uk-light">
			        <a class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous uk-slideshow-item="previous"></a>
			        <a class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-next uk-slideshow-item="next"></a>
			    </div>

			</div>

     	</section>


     	<section class="py-5 border-bottom" id="quem-somos">
     		<div class="container-fluid px-xl-7">

     			<h1 class="titulo" id="tituloQuemSomos">Quem somos</h1>
     			<div class="flex-center mt-4 mb-4"><div class="w-80px h-2px bg-line rounded"></div></div>

     			<div class="row align-items-center">

     				<div class="col-lg-8">

     					<div class="pr-lg-5">
			     			<p class="font-poppins weight-400 fs-17-5 line-height-1-5 text-dark">	
			     				O Colégio Comercial é uma instituição que há mais de 50 anos, trabalha através de Educação superior e Supletivo EJA, com educação de qualidade e ajudando inúmeras pessoas a buscar uma carreira, qualificação ou completarem os estudos de forma rápida e conquistarem seus objetivos pessoais e profissionais.
			     			</p>

			     			<p class="font-poppins weight-400 fs-17-5 line-height-1-5 text-dark">
			     				Oferecemos muito mais que apenas um certificado, oferecemos a oportunidade exata para que você volte a sentir orgulho de si mesmo e autoconfiança ao ter a certeza de que está pronto para enfrentar novos desafios profissionais e que não será mais eliminado de um processo seletivo, antes mesmo de ter suas capacidades avaliadas, simplesmente por causa do seu nível de escolaridade.
			     			</p>

			     			<p class="font-poppins weight-400 fs-17-5 line-height-1-5 text-dark">
			     				Se você está à procura de um lugar que promova o crescimento pessoal e profissional de seus alunos através dos estudos, o Colégio Comercial é a escolha certa.
			     			</p>
			     		</div>

		     		</div>

		     		<div class="col-lg-4">

		     			<div class="mx-lg-3">

			     			<div class="bg-base-blue box-info rounded-30 text-center py-2-5 text-light">
			     				<p class="mb-0 fs-40 weight-700" id="countEffectYears">50</p>
			     				<p class="font-poppins fs-18">Anos de Experiência</p>
			     			</div>
			     			<div class="my-lg-3 bg-base-blue box-info rounded-30 text-center py-2-5 text-light">
			     				<p class="mb-0 fs-40 weight-700" id="countEffectStudents">+0</p>
			     				<p class="font-poppins fs-18">Alunos Formados</p>
			     			</div>
			     			<div class="bg-base-blue box-info rounded-30 text-center py-2-5 text-light">
			     				<p class="mb-0 fs-40 weight-700" id="countEffectPercent">100%</p>
			     				<p class="font-poppins fs-18">Reconhecido pelo CEE</p>
			     			</div>

			     		</div>

		     		</div>

		     	</div>

     		</div>
     	</section>

     	<section class="py-5" id="alunos-formados">
     		<div class="container-fluid px-xl-7">

     			<h1 class="titulo">Turmas de Alunos</h1>
     			<div class="flex-center mt-4 mb-4"><div class="w-80px h-2px bg-line rounded"></div></div>

     			<div uk-scrollspy="cls: uk-animation-slide-bottom; target: .alunos-formados; delay: 200;">
     				<div class="alunos-formados">
		     			<div class="row align-items-center px-2-5">
		     				<img src="{{ asset('sites-proprios/colegio-comercial/images/turmas-de-alunos/1.jpg') }}" class="col-6 px-0" alt="Alunos Formados - Ensino Certo">
		     				<img src="{{ asset('sites-proprios/colegio-comercial/images/turmas-de-alunos/2.jpg') }}" class="col-6 px-0" alt="Alunos Formados - Ensino Certo">
		     			</div>
		     		</div>
     			</div>

     		</div>
     	</section>

     	<section class="py-5" id="cursos">
     		<div class="container-fluid px-xl-5">

     			<h1 class="titulo">Cursos</h1>
     			<div class="flex-center mt-4 mb-4"><div class="w-80px h-2px bg-line rounded"></div></div>

     			<p class="mb-0 text-center font-poppins text-dark fs-17 col-md-10 offset-md-1">
     				Muito mais do que lhe promover um diploma, a intenção principal do Colégio Comercial é fornecer através da educação, conhecimento e possibilidades de melhores conquistas profissionais e pessoais. Conheça nossos cursos:
     			</p>

            	<div class="pt-4-5 uk-child-width-1-4@m" uk-grid uk-scrollspy="cls: uk-animation-fade; target: .box-info; delay: 175;">

					@foreach($cursos as $curso)
					<div>
						<div class="card box-info">
						 	<a href="/curso/{{$curso->slug}}" target="_blank" class="text-d-none hover-d-none">
						 		<img src="{{ $curso->foto_capa }}" class="p-2 rounded-12 card-img-top h-lg-200 object-fit-cover" alt="Curso de {{ $curso->nome }}">
							 	<div class="card-body text-center h-min-lg-300">
							    	<h3 class="font-poppins weight-600 fs-1-5rem color-base-blue">
							    		{{ $curso->nome }}
							    	</h3>
							    	<div>
							    		<i class="fa fa-star color-star"></i>
							    		<i class="fa fa-star color-star"></i>
							    		<i class="fa fa-star color-star"></i>
							    		<i class="fa fa-star color-star"></i>
							    		<i class="fa fa-star color-star"></i>
							    	</div>
							    	<p class="mt-2-5 mb-0 card-text font-poppins color-grafite weight-500">
							    		{{ $curso->seo_description }}
							    	</p>
							  	</div>
						    	<div class="text-center pb-4-5">
						    		<span class="mb-0 btnSaibaMais text-light fs-14">Saiba Mais</span>
						    	</div>
							</a>
						</div>
					</div>
					@endforeach

                </div>

     		</div>
     	</section>

     	<section class="pt-5" id="como-funciona">
     		<div class="bg-como-funciona py-5" style="background-image: url('{{ asset('sites-proprios/colegio-comercial/images/slides/3.jpg') }}');">
     			<div class="bg-como-funciona-overlay"></div>
	     		<div class="container-fluid px-xl-7 position-relative">

	     			<h1 class="titulo color-base-green">Como funciona o Colégio Comercial?</h1>
	     			<div class="flex-center mt-4 mb-4"><div class="w-80px h-2px bg-light rounded"></div></div>

	     			<p class="mb-0 text-center font-poppins text-light fs-17 col-md-10 offset-md-1">
	     				Conseguir seu certificado ou diploma através do Colégio Comercial nunca foi tão fácil! Entre em contato com um de nossos representantes para tirar suas dúvidas e iniciar o processo de matrícula. Estude nos momentos em que desejar e faça a prova em um local escolhido por nós, e se for aprovado, seu certificado ou diploma estará em suas mãos.
	     			</p>

	     			<div class="pt-5 uk-child-width-1-3@m" uk-grid uk-scrollspy="cls: uk-animation-slide-bottom; target: .boxComoFunciona; delay: 100;">

	     				<div>
		     				<div class="boxComoFunciona text-center">
		     					<img width="100" class="mb-3" src="{{ asset('sites-proprios/colegio-comercial/images/como-funciona/1.png') }}" alt="Educacional Ensino Certo - Entre em Contato" class="img-fluid">
		     					<h4 class="color-base-green font-poppins">1. Entre em contato</h4>
		     					<p class="text-light font-poppins">
		     						Entre em contato com um de nossos representantes para tirar dúvidas e iniciar os processos.
		     					</p>
		     				</div>
	     				</div>
	     				<div>
		     				<div class="boxComoFunciona text-center">
		     					<img width="100" class="mb-3" src="{{ asset('sites-proprios/colegio-comercial/images/como-funciona/2.png') }}" alt="Educacional Ensino Certo - Faça sua Mátricula" class="img-fluid">
		     					<h4 class="color-base-green font-poppins">2. Faça sua Mátricula</h4>
		     					<p class="text-light font-poppins">
									Faça sua Matrícula e receba o material didático necessário, você poderá estudar onde e quando quiser.
		     					</p>
		     				</div>
	     				</div>
	     				<div>
		     				<div class="boxComoFunciona text-center">
		     					<img width="100" class="mb-3" src="{{ asset('sites-proprios/colegio-comercial/images/como-funciona/3.png') }}" alt="Educacional Ensino Certo - Faça a prova!" class="img-fluid">
		     					<h4 class="color-base-green font-poppins">3. Faça a prova!</h4>
		     					<p class="text-light font-poppins">
		     						Faça a prova e receba seu certificado ou diploma! Vamos escolher o melhor dia e local para você realizar a prova!
		     					</p>
		     				</div>
	     				</div>

	     			</div>

	     			<div class="text-center pt-4-5">
		     			<a href="#contato" uk-scroll="offset:80;" class="btnEntrarEmContato animation-grow color-base-blue-hover hover-d-none">
		     				Entrar em Contato!
		     			</a>
	     			</div>

	     		</div>
	     	</div>
     	</section>

     	<section class="py-5 bg-f5" id="parcerias">
     		<div class="container-fluid px-xl-7">
     			<div class="row align-items-center text-center px-xl-7">
     				<div class="col-lg-6 mb-4 mb-lg-0">
     					<div class="px-lg-4">
     						<a href="https://ensinocerto.com.br" target="_blank">
     							<img src="{{ asset('sites-proprios/colegio-comercial/images/logo-ensinocerto.png') }}" class="img-fluid" width="200">
     						</a>
     						<p class="my-2 font-poppins fs-14">Parceria Comercial</p>
     						<p class="mb-2 font-poppins fs-14">"A Ensino Certo é responsável por toda parte comercial do colégio, auxiliando o aluno na decisão de qual curso é ideal."</p>
     						<p class="mb-2 font-poppins fs-14">
     							www.ensinocerto.com.br
     						</p>
     					</div>
     				</div>
     				<div class="col-lg-6">
     					<div class="px-lg-4">
     						<img src="{{ asset('sites-proprios/colegio-comercial/images/logo-unopar.png') }}" class="img-fluid" width="200">
     						<p class="my-2 font-poppins fs-14">Polo Unopar</p>
     						<p class="mb-2 font-poppins fs-14">"Sendo polo da Unopar o Colégio Comercial oferece um portfólio de cursos e soluções que o aluno precisa."</p>
     						<p class="mb-2 font-poppins fs-14">
     							www.unopar.com.br
     						</p>
     					</div>
     				</div>
     			</div>
     		</div>
     	</section>

     	<section class="py-5 bg-white" id="faq">
     		<div class="container-fluid px-xl-7">

     			<h1 class="titulo">Perguntas frequentes Sobre o Colégio Comercial</h1>
     			<div class="flex-center mt-4 mb-4"><div class="w-80px h-2px bg-line rounded"></div></div>

     			<p class="mb-0 text-center font-poppins text-dark fs-17 col-md-10 offset-md-1">
     				Aqui estão todas as informações necessárias para você tirar suas dúvidas sobre nosso processo de ensino.
     			</p>

     			<div class="pt-4-5 px-xl-8 ml-lg-2">

     				<div>
		     			<a href="#collapseFaq1" data-toggle="collapse" href="#collapseFaq1" class="clickFaq text-d-none hover-d-none color-base-blue font-poppins weight-600">
		     				<i class="fas fa-caret-right"></i>
		     				O que é Educação à Distância?
		     			</a>
						<p class="collapse mb-0 mt-1 font-poppins text-dark" id="collapseFaq1">
							Educação à Distância – EAD – É assim que é conhecida em todo o mundo a modalidade de ensino-aprendizagem em que o aluno não precisa estar necessariamente presente na sala de aula. Ele pode estudar em sua própria casa, no local de trabalho ou qualquer outro lugar, usando da melhor forma o tempo de que disponha para destinar ao aprendizado.
						</p>	
		     			<hr/>
		     		</div>
     				<div>
		     			<a href="#collapseFaq2" data-toggle="collapse" href="#collapseFaq2" class="clickFaq text-d-none hover-d-none color-base-blue font-poppins weight-600">
		     				<i class="fas fa-caret-right"></i>
		     				Como funciona o supletivo a distância?
		     			</a>
						<p class="collapse mb-0 mt-1 font-poppins text-dark" id="collapseFaq2">
							O Supletivo EJA a distância permite que o aluno estude onde ele estiver, e no tempo que ele puder. Esse método possibilita a autoaprendizagem, onde há uma separação entre o professor e o aluno podendo estar interligados por diferentes formas. Assim o aluno estuda no seu ritmo. O Colégio Comercial possui ensino supletivo fundamental e médio a distância, autorizado pelo C.E.E. (Conselho Estadual de Educação), parecer 023/16.
						</p>	
		     			<hr/>
		     		</div>
     				<div>
		     			<a href="#collapseFaq4" data-toggle="collapse" href="#collapseFaq4" class="clickFaq text-d-none hover-d-none color-base-blue font-poppins weight-600">
		     				<i class="fas fa-caret-right"></i>
		     				Qual a idade mínima para realizar o supletivo?
		     			</a>
						<p class="collapse mb-0 mt-1 font-poppins text-dark" id="collapseFaq4">
							É necessário que você tenha 18 anos e 6 meses para cursar o Ensino Médio (antigo 2º grau) ou 16 anos para cursar o Ensino fundamental (antigo 1º grau). Você deverá comprovar a escolaridade anterior para continuar da série que você parou.
						</p>	
		     			<hr/>
		     		</div>
     				<div>
		     			<a href="#collapseFaq5" data-toggle="collapse" href="#collapseFaq5" class="clickFaq text-d-none hover-d-none color-base-blue font-poppins weight-600">
		     				<i class="fas fa-caret-right"></i>
		     				O curso à distância é reconhecido oficialmente?
		     			</a>
						<p class="collapse mb-0 mt-1 font-poppins text-dark" id="collapseFaq5">
							Os cursos à distância são regulamentados pelos Conselhos Estaduais de Educação de cada estado e amparados pela Lei Federal 9394/96.
						</p>	
		     			<hr/>
		     		</div>
     				<div>
		     			<a href="#collapseFaq6" data-toggle="collapse" href="#collapseFaq6" class="clickFaq text-d-none hover-d-none color-base-blue font-poppins weight-600">
		     				<i class="fas fa-caret-right"></i>
		     				Como são feitas as avaliações?
		     			</a>
						<p class="collapse mb-0 mt-1 font-poppins text-dark" id="collapseFaq6">
							As provas, tanto de ensino fundamental quanto ensino médio, são feitas no Colégio Comercial ou em um dos polos credenciados junto ao Conselho Estadual de Educação.
						</p>	
		     			<hr/>
		     		</div>
     				<div>
		     			<a href="#collapseFaq8" data-toggle="collapse" href="#collapseFaq8" class="clickFaq text-d-none hover-d-none color-base-blue font-poppins weight-600">
		     				<i class="fas fa-caret-right"></i>
		     				Eu tenho restrições em meu nome (SCPC / SERASA), posso fazer mesmo assim?
		     			</a>
						<p class="collapse mb-0 mt-1 font-poppins text-dark" id="collapseFaq8">
							O Colégio Comercial sempre faz uma análise básica para verificar se o aluno é apto a realizar qualquer curso em nossa instituição. Estando apto uma checagem interna de dados é feita e assim é definido se o aluno pode ou não estudar conosco.
						</p>	
		     			<hr/>
		     		</div>
     				<div>
		     			<a href="#collapseFaq9" data-toggle="collapse" href="#collapseFaq9" class="clickFaq text-d-none hover-d-none color-base-blue font-poppins weight-600">
		     				<i class="fas fa-caret-right"></i>
		     				Como receberei o material didático? Terei que pagar o material?​
		     			</a>
						<p class="collapse mb-0 mt-1 font-poppins text-dark" id="collapseFaq9">
							O Colégio Comercial oferece todo material didático online em nossa Área do Aluno. O aluno que desejar o material impresso, poderá adquirir separadamente.
							<br><i>*Consulte condições comerciais.</i>
						</p>	
		     			<hr/>
		     		</div>

	     		</div>

     		</div>
     	</section>

      	<section class="pt-lg-4 pt-5 pb-2-5 bg-faixa">
        	<div class="container-fluid px-md-5">

            	<div class="uk-child-width-1-4@m" uk-grid uk-scrollspy="cls: uk-animation-slide-bottom; target: .uk-card; delay: 100;">
               		
               		<div>
	                  	<div class="uk-card flex-center flex-column color-faixa">
	                  		<div>
	                      		<i class="fa fa-desktop fs-25"></i>
	                      		<i class="fa fa-mobile-alt fs-25 ml-1"></i>
	                    	</div>
	                    	<h3 class="font-poppins weight-500 fs-15 color-faixa mt-2">Estudo 100% Online</h3>
	                  	</div>
              	 	</div>

               		<div>
                  		<div class="uk-card flex-center flex-column color-faixa">
                    		<i class="far fa-clock fs-30"></i>
                    		<h3 class="font-poppins weight-500 fs-15 color-faixa mt-2">Faça seu Próprio Horário</h3>
                  		</div>
               		</div>

               		<div>
                  		<div class="uk-card flex-center flex-column color-faixa">
                    		<i class="fas fa-file-signature fs-25"></i>
                    		<h3 class="font-poppins weight-500 fs-15 color-faixa mt-2">Reconhecido Pelo CEE</h3>
                  		</div>
               		</div>

               		<div>
                  		<div class="uk-card mb-4 mb-md-0 flex-center flex-column color-faixa">
                    		<i class="far fa-flag fs-30"></i>
                    		<h3 class="font-poppins weight-500 fs-15 color-faixa mt-2">Validade Nacional</h3>
                  		</div>
               		</div>

            	</div>

        	</div>   
      	</section>

     	<section class="pt-5 bg-base-blue" id="contato">
     		<div class="container-fluid px-xl-7">

     			<h1 class="titulo text-light">Contato</h1>
     			<div class="flex-center mt-4 mb-4"><div class="w-80px h-2px bg-line rounded"></div></div>

     			<p class="mb-0 text-center font-poppins text-light fs-17 col-md-10 offset-md-1">
     				Entre em contato conosco para iniciar sua matrícula, tirar alguma dúvida ou enviar uma sugestão.
					<br>
					Preencha o formulário abaixo ou entre em contato via Whatsapp.
     			</p>

     			<div class="row align-items-center pt-5 pb-5 pb-lg-0">

     				<div class="col-lg-7">
                      <form method="POST" id="formSolicitarContato" class="mr-lg-5 bg-base-blue p-4 rounded-2">
	                   @csrf

	                  	@if(session('success'))
	                    	<div class='alert alert-success'><b>Sucesso!</b> Email enviado com êxito.</div>
	                  	@endif

	                  	<input type="hidden" name="assunto" value="Fale Conosco">
	                  	
                        <div class="input-group mb-2">
                          <div class="input-group-prepend">
                            <div class="input-group-text bg-base-green rounded-right-0 color-base-blue border-0 py-4" style="padding-right: 13.5px;">
                              <i class="fa fa-user"></i>
                            </div>
                          </div>
                          <input type="text" name="nome" class="placeholder-edit form-control py-2-5 outline-0 rounded-2" placeholder="Seu Nome" required>
                        </div>

                        <div class="input-group mb-2">
                          <div class="input-group-prepend">
                            <div class="input-group-text bg-base-green rounded-right-0 color-base-blue border-0 py-4">
                              <i class="fa fa-phone"></i>
                            </div>
                          </div>
                          <input type="tel" name="celular" class="celular placeholder-edit rounded-left-0 form-control py-2-5 outline-0 rounded-2" placeholder="Celular" required>
                        </div>

                        <div class="input-group mb-0">
                          <div class="input-group-prepend">
                            <div class="input-group-text bg-base-green rounded-right-0 color-base-blue border-0 py-4">
                              <i class="fa fa-envelope"></i>
                            </div>
                          </div>
                          <input type="email" name="email" class="placeholder-edit rounded-left-0 form-control py-2-5 outline-0 rounded-2" placeholder="Email" required>
                        </div>

                        <input type="text" name="check" class="invisible h-0px" placeholder="Não preencha este campo.">

                        <input type="submit" name="sentContato" value="Solicitar Contato!" class="btnEntrarEmContato mt--5 rounded-3 border-0 w-100 weight-600 outline-0">
                                                
                      </form>
     				</div>

     				<div class="col-lg-5 d-xs-none">
     					<div>
     						<img class="img-fluid" src="{{ asset('sites-proprios/colegio-comercial/images/contato.png') }}" alt="Contato - Educacional Ensino Certo">
     					</div>
     				</div>

     			</div>

     		</div>
     	</section>

     	<section id="contato-rodape">
     		<div class="bg-contato py-5" style="background-image: url('{{ asset('sites-proprios/colegio-comercial/images/slides/1.jpg') }}');">
     			<div class="bg-contato-overlay"></div>
	     		<div class="container-fluid px-xl-7 position-relative">

	     			<div class="row">

	     				<div class="col-lg-4 mb-4 mb-lg-0">
		     			
	     					<p class="text-light font-poppins weight-600 fs-17">Fale Conosco</p>
	     					<hr class="border border-light" />

			                <div class="mb-4">
			                    <p class="mb-0 font-poppins text-white d-flex align-items-center">
			                    	<i class="fa fa-envelope fs-30 color-base-green"></i>
			                    	<span class="ml-3">{{ $plataforma->email }}</span>
			                    </p>
			                </div>

			                <div class="mb-4">
			                    <a href="tel:+55{{preg_replace('/\D/', '', $plataforma->whatsapp)}}" class="text-d-none hover-d-none font-poppins text-white d-flex align-items-center">
			                    	<i class="ml-1 fas fa-mobile-alt fs-30 color-base-green"></i>
			                    	<span class="text-light text-d-none ml-3 ls-05">&nbsp;{{ $plataforma->whatsapp }}</span>
			                    </a>
			                </div>

			                <div class="mb-3">
			                    <a href="tel:+55{{preg_replace('/\D/', '', $plataforma->telefone)}}" class="text-d-none hover-d-none font-poppins text-white d-flex align-items-center">
			                    	<i class="fa fa-phone fs-25 color-base-green"></i>
			                    	<span class="text-light text-d-none ml-3 ls-05">{{ $plataforma->telefone }}</span>
			                    </a>
			                </div>

			                <div class="mb-4">
		                    	<div class="d-flex align-items-center">
			                    	<i class="fas fa-map-marker-alt fs-30 color-base-green"></i>
			     					<address class="ml-3 mb-0 text-light font-poppins">
			     						{{ $plataforma->endereco }}
			     					</address>
		     					</div>
			                </div>

	     				</div>

	     				<div class="col-lg-4 mb-4 mb-lg-0">

	     					<p class="text-light font-poppins weight-600 fs-17">
	     						Navegação
	     					</p>
	     					<hr class="border border-light mb-4-5" />

	     					<div class="mb-5">
		     					<a href="/login" class="btnNavegacao btnNavegaAreaAluno" target="_blank">
		     						<i class="fas fa-graduation-cap"></i>
		     						Área do Aluno
		     					</a>
		     				</div>
		     				<div class="mb-5">
		     					<a href="https://form.jotform.com/ensinocerto/parceria?marca=Colegio%20Comercial" target="_blank" class="btnNavegacao btnNavegaParceiro">
		     						<i class="fas fa-building"></i>
		     						Torna-se Parceiro
		     					</a>
		     				</div>

	     				</div>

	     				<div class="col-lg-4 mb-4 mb-lg-0">

	     					<p class=" text-light font-poppins weight-600 fs-17">Redes Sociais</p>
	     					<hr class="border border-light" />

						    <div class="d-flex">
						      <div class="media-icons">

						        <a href="https://www.facebook.com/colegiocomercialead" target="_blank">
						          <div>
						            <i class="fab fa-facebook-f"></i>
						          </div>
						        </a>
						        <a href="https://twitter.com/colegiocomercial" target="_blank">
						          <div>
						            <i class="fab fa-twitter"></i>
						          </div>
						        </a>
						        <a href="https://www.instagram.com/ensino_certo/" target="_blank">
						          <div>
						            <i class="fab fa-instagram"></i>
						          </div>
						        </a>
						        <a href="https://api.whatsapp.com/send?phone=55{{preg_replace('/\D/', '', $plataforma->whatsapp)}}&text=Olá, vim através do Site {{ $plataforma->nome }}." target="_blank">
						          <div>
						            <i class="fab fa-whatsapp"></i>
						          </div>
								</a>
						        <a href="https://www.youtube.com/channel/UCBGPx2VqWSKspTAnT4aZ6-g" target="_blank">
						          <div>
						            <i class="fab fa-youtube"></i>
						          </div>
						        </a>
						      </div>
						    </div>

	     				</div>

	     			</div>

	     		</div>
	     	</div>
     	</section>

    </main>

    <a href="https://api.whatsapp.com/send?phone=55{{preg_replace('/\D/', '', $plataforma->whatsapp)}}&text=Olá, vim através do Site {{ $plataforma->nome }}." target="_blank" class="text-white fs-30">
        <div class="p-fixed bottom-0 right-0 mr-4 mb-4">
            <div class="square-60 rounded-circle bg-success flex-center shadow">
                <i class="fab fa-whatsapp"></i>
            </div>
        </div>
    </a>
    
   	<footer>
       	<div class="bg-base-blue py-2">
        	<div class="container text-center py-2">

		     	<img width="80" src="{{ asset('sites-proprios/colegio-comercial/images/logo-white.png') }}" alt="Logotipo Colégio Comercial - Rodapé">

            	<p class="mt-2-5 font-roboto fs-13 weight-600 text-center mb-0 color-c9 color-hover-c9">
                	2020 &copy; 
                	<a href="https://wozcode.com" class="color-c9 color-hover-c9 hover-d-none text-d-none color-hover-copyright" target="_blank">
                		Colégio Comercial de Votuporanga • Todos os direitos reservados • CNPJ: 72.958.085/0001-26
              			<br>
              			Educacional Ensino Certo - Parceiro Comercial
              		</a>
            	</p>

          	</div>
       	</div>
   	</footer>

	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.slim.js" integrity="sha512-1lagjLfnC1I0iqH9plHYIUq3vDMfjhZsLy9elfK89RBcpcRcx4l+kRJBSnHh2Mh6kLxRHoObD1M5UTUbgFy6nA==" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
    <script src="https://kit.fontawesome.com/2f91eaaef4.js" crossorigin="anonymous"></script> 
    <script src="https://cdn.jsdelivr.net/npm/uikit@3.5.9/dist/js/uikit.min.js"></script>

	<!-- Start of HubSpot Embed Code -->
	<script type="text/javascript" id="hs-script-loader" async defer src="//js.hs-scripts.com/4719984.js"></script>
	<!-- End of HubSpot Embed Code -->
    
	<script>

		var isMobile = {
		    Android: function() {
		        return navigator.userAgent.match(/Android/i);
		    },
		    BlackBerry: function() {
		        return navigator.userAgent.match(/BlackBerry/i);
		    },
		    iOS: function() {
		        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
		    },
		    Opera: function() {
		        return navigator.userAgent.match(/Opera Mini/i);
		    },
		    Windows: function() {
		        return navigator.userAgent.match(/IEMobile/i) || navigator.userAgent.match(/WPDesktop/i);
		    },
		    any: function() {
		        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
		    }
		};

		if(isMobile.any() == null)
		{
			$(".nav-link").removeAttr('data-target').removeAttr('data-toggle');
		}

		var control = true;

		window.addEventListener('scroll', function() {
			var element = document.querySelector('#tituloQuemSomos');
			var position = element.getBoundingClientRect();

			// checking whether fully visible
			if(position.top >= 0 && position.bottom <= window.innerHeight && control == true)
			{
				animateValue(document.getElementById('countEffectYears'), 0, 50, 2000);
				animateValue(document.getElementById('countEffectStudents'), 0, 9000, 2000);
				animateValue(document.getElementById('countEffectPercent'), 0, 100, 2000);
				control = false;
			}

			// checking for partial visibility
			if(position.top < window.innerHeight && position.bottom >= 0) {
				console.log('Element is partially visible in screen');
			}
		});

		function animateValue(obj, start = 0, end = null, duration = 3000) {
		    if (obj) {

		        // save starting text for later (and as a fallback text if JS not running and/or google)
		        var textStarting = obj.innerHTML;

		        // remove non-numeric from starting text if not specified
		        end = end || parseInt(textStarting.replace(/\D/g, ""));

		        var range = end - start;

		        // no timer shorter than 50ms (not really visible any way)
		        var minTimer = 50;

		        // calc step time to show all interediate values
		        var stepTime = Math.abs(Math.floor(duration / range));

		        // never go below minTimer
		        stepTime = Math.max(stepTime, minTimer);

		        // get current time and calculate desired end time
		        var startTime = new Date().getTime();
		        var endTime = startTime + duration;
		        var timer;

		        function run() {
		            var now = new Date().getTime();
		            var remaining = Math.max((endTime - now) / duration, 0);
		            var value = Math.round(end - (remaining * range));
		            // replace numeric digits only in the original string
		            obj.innerHTML = textStarting.replace(/([0-9]+)/g, value);
		            if (value == end) {
		                clearInterval(timer);
		            }
		        }

		        timer = setInterval(run, stepTime);
		        run();
		    }
		}

		$(".clickFaq").click(function()
		{
		  	caret = $(this).children();

		  	if(caret.hasClass('fa-caret-right'))
		  	{
			 	caret.removeClass('fa-caret-right').addClass('fa-caret-down');

			} else {

				caret.addClass('fa-caret-right').removeClass('fa-caret-down');
			}
		});

	</script>	

  	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.js"></script>
   	<script>
	    $(document).on("focus", ".celular", function() { 
	      jQuery(this)
	          .mask("(99) 9999-99999")
	          .change(function (event) {  
	              target = (event.currentTarget) ? event.currentTarget : event.srcElement;  
	              phone = target.value.replace(/\D/g, '');
	              element = $(target);  
	              element.unmask();  
	              if(phone.length > 10) {  
	                  element.mask("(99) 99999-9999");  
	              } else {  
	                  element.mask("(99) 9999-9999?9");  
	              }  
	      });
	    });
   	</script>

   	{!! $plataforma->scripts_final_body !!}
   	
</body>
</html>