<!doctype html>
<html lang="pt-br">
<head>

  <!-- Google tag (gtag.js) -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=G-MREB450HCT"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'G-MREB450HCT');
  </script>

  {!! $plataforma->scripts_start_head !!}

   <meta charset="utf-8">
   <meta name="viewport" content="width=device-width, initial-scale=1">

   <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/uikit@3.5.9/dist/css/uikit.min.css" />
   <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
   <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
   <link rel="stylesheet" href="{{ asset('css/util.css') }}?v=14">
   <link rel="stylesheet" href="{{ asset('sites-proprios/brasedu/css/home.css') }}?v=23">

   <title>{{ $seo->seo_title ?? $plataforma->nome }}</title>
   <meta name="robots" content="index, follow">
   <meta name="description" content="{{ $seo->seo_description ?? null }}"/>
   <meta name="keywords" content="{{ $seo->seo_keywords ?? null }}"/>
   <link rel="canonical" href="{{ Request::url() }}" />
   <link rel="shortcut icon" href="{{ $plataforma->favicon }}">
   <meta property="og:locale" content="pt_BR" />
   <meta property="og:type" content="website" />
   <meta property="og:title" content="{{ $seo->seo_title ?? $plataforma->nome }}" />
   <meta property="og:description" content="{{ $seo->seo_description ?? null }}" />
   <meta property="og:url" content="{{ Request::url() }}" />
   <meta property="og:site_name" content="{{ $plataforma->nome }}" />
   <meta property="og:image" content="{{ $plataforma->logotipo }}" />
   <meta name='author' content='WOZCODE | Pedro Henrique' />

   {!! $plataforma->scripts_final_head !!}

</head>
<body class="bg-f4" id="inicio">

   {!! $plataforma->scripts_start_body !!}

   @include('sites-proprios/brasedu/componentes/header')

   <main class="pt-9rem">

      <section id="banners">
         <div class="uk-position-relative uk-visible-toggle" tabindex="-1" uk-slideshow="autoplay: true; autoplay-interval: 5000; pause-on-hover: false; max-height: 450">

            <ul class="uk-slideshow-items">
               @foreach($banners as $banner)
                  <li>
                  <div style="background-image: url('{{ $banner->link }}');" class="bg-img-banner">
                     @if($banner->overlay == 'S')
                        <div class="background-overlay" style="background: {{ $banner->overlay_color ?? 'unset' }};opacity: {{ $banner->overlay_opacity ?? '0' }};"></div>
                     @endif
                     <div uk-scrollspy="cls: uk-animation-fade; delay: 400;" class="container-fluid px-xl-7 mt--10px uk-position-center-left uk-position-small uk-light">
                        
                        <h2 class="text-center uk-margin-remove font-montserrat weight-800 display-3 ls--03" uk-slideshow-parallax="x: 300,0,-100">
                           {!! $banner->titulo !!}
                        </h2>

                        @if($banner->subtitulo)
                           <p uk-slideshow-parallax="x: 300,0,-75" class="text-center uk-margin-remove text-light fs-20">
                              {!! $banner->subtitulo !!}
                           </p>
                        @endif

                        @if($banner->botao_texto)
                           <div class="mt-4-5 text-center">
                              <a href="{{ $banner->botao_link }}" class="btnInscricao text-shadow-1 d-xs-block text-center text-shadow-1" uk-slideshow-parallax="x: 300,0,-50">
                                 {!! $banner->botao_texto !!}
                              </a>
                           </div>
                        @endif

                     </div>
                  </div>
                  </li>
               @endforeach
            </ul>

            <div class="uk-light">
                  <a class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous uk-slideshow-item="previous"></a>
                  <a class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-next uk-slideshow-item="next"></a>
            </div>

         </div>
      </section>

      <section id="cursos" class="pt-lg-5 pb-5">
         <div class="container-fluid px-xl-8 pt-lg-5">

            <div class="d-lg-flex align-items-end w-100">

               <h1 class="w-100 text-dark font-poppins fs-25 weight-700">
                  Conheça <span class="color-titulo">nossos cursos em Destaque</span>
               </h1>

               <form class="d-flex align-items-center w-100" action="buscar" target="_blank">
                  <input type="text" name="curso-de-interesse" class="search-curso" placeholder="Digite o nome do Curso de Interesse">
                  <button type="submit" class="btn-search" style="margin-left: -4px;">
                     <i class="fa fa-search"></i>
                  </button>
               </form>

            </div>

            <hr/>


            <div class="row" id="rowCursos">

            </div>

            <div id="boxCarregandoCursos">
               <div class="d-flex align-items-center">
                  <div class="spinner-grow text-primary" role="status">
                     <span class="visually-hidden">Loading...</span>
                  </div>
                  <p class="ms-2 mb-0 weight-600">Carregando...</p>
               </div>
            </div>

         </div>
      </section>

      <section id="diferenciais" class="pt-lg-5 pb-lg-5 pt-3 pb-5">
         <div class="bg-dif py-5">
            <div class="container-fluid px-xl-7">

            <div class="row align-items-center">
               
               <div class="col-lg-5">

                  <h1 class="font-montserrat weight-900 fs-md-60 text-light text-shadow-1 text-xs-center">
                     O CAMINHO RUMO À FORMAÇÃO
                  </h1>

                  <p class="mb-lg-5 mb-4 mt-3 text-light font-montserrat weight-500 fs-19 text-xs-center">
                     Venha viver uma experiência de ensino único. Descubra as vantagens de estudar na BRASEDU
                  </p>
                  
                  <div class="d-xs-none mb-lg-0 text-xs-center">
                     <a href="https://api.whatsapp.com/send?phone=55{{preg_replace('/\D/', '', $plataforma->whatsapp)}}&text=Olá, vim através do Site {{ $plataforma->nome }}." target="_blank" class="btnCall bg-wpp box-shadow-wpp">
                        QUERO SABER MAIS PELO WHATSAPP!
                        <i class="fab fa-whatsapp ms-2"></i>
                     </a>
                  </div>
               
               </div>

               <div class="col-lg-7 mt-4 mt-lg-0">
                  <div class="row">
                     <div class="col-lg-6 border-o-caminho border-right border-bottom">
                        <div class="p-lg-3">
                           <p class="d-flex align-items-center weight-700 text-shadow-1 fs-17 font-poppins text-light mb-1">
                              <i class="fas fa-play-circle fs-40 text-warning"></i>
                              <span class="ms-3">
                                 Vídeo Aulas Dinâmicas
                              </span>
                           </p>

                           <p class="text-light font-poppins">
                              Uma vídeo aula atraente é o melhor método para aprender novos conhecimentos. Nossos conteúdos são excelência em qualidade. Você poderá estudar num notebook, num tablet ou no seu smarphone.
                           </p>
                           <hr class="my-4 border border-light border-color-xs" />
                        </div>
                     </div>
                     <div class="col-lg-6 border-o-caminho border-bottom">
                        <div class="p-lg-3">
                           <p class="d-flex align-items-center weight-700 text-shadow-1 fs-17 font-poppins text-light mb-1">
                              <i class="fas fa-desktop fs-40 text-warning"></i>
                              <span class="ms-3">
                                 Plataforma
                              </span>
                           </p>

                           <p class="text-light font-poppins">
                              Todos os conteúdos foram enriquecidos com apostilas, jogos, exercícios dinâmicos para você utilizar. Você terá uma verdadeira biblioteca virtual com vários e-books para baixar e estudar.
                           </p>
                           <hr class="my-4 border border-light border-color-xs" />
                        </div>
                     </div>
                     <div class="col-lg-6 border-o-caminho border-right">
                        <div class="p-lg-3">
                           <p class="d-flex align-items-center weight-700 text-shadow-1 fs-17 font-poppins text-light mb-1">
                              <i class="fas fa-chalkboard-teacher fs-40 text-warning"></i>
                              <span class="ms-3">
                                 Nossos Professores
                              </span>
                           </p>

                           <p class="text-light font-poppins">
                              Todos os nossos professores oferecem aulões semanais de revisão de cada disciplina. Aproveite para tirar as suas dúvidas participando dos nossos encontros presencias. Junte-se a nossa galera!
                           </p>
                           <hr class="my-4 border border-light border-color-xs" />
                        </div>
                     </div>         
                     <div class="col-lg-6">
                        <div class="p-lg-3">
                           <p class="d-flex align-items-center weight-700 text-shadow-1 fs-17 font-poppins text-light mb-1">
                              <i class="fa fa-clock fs-40 text-warning"></i>
                              <span class="ms-3">
                                 Faça seu próprio horário
                              </span>
                           </p>

                           <p class="text-light font-poppins">
                              Aqui conosco você pode estudar no horário que planejar melhor sua rotina. Tenha um estudo flexível e inicie as aulas no seu tempo, no seu horário que melhor desejar.
                           </p>
                        </div>
                     </div>
                  </div>
                  <div class="pt-4 text-center">
                     <a href="https://braseducursos.com.br/f/ficha-de-matricula/37/t/627" target="_blank" class="btnCall bg-warning box-shadow-warning text-dark hover-dark">
                        QUERO FAZER MINHA PRÉ-MATRÍCULA!
                     </a>
                  </div>
               </div>
            
            </div>

            </div>
         </div>
      </section>

      <section class="bg-f5 py-lg-5">
         <div class="container-fluid px-xl-7 bg-lines">

            <h1 class="text-center font-montserrat weight-800 fs-35 text-shadow-1">
               Temos o melhor conteúdo
            </h1>

            <p class="mt-3 mb-4-5 text-center font-raleway weight-600 color-666 col-md-8 offset-md-2 fs-18">
               Variedades de conteúdos e ferramentas para você começar a estudar do seu jeito!
            </p>

            <div class="row mb-5">

               <div class="col-lg-4 px-2 rounded-2 mb-3">
                  <div class="bg-white px-4-5 d-flex align-items-center box-info">
                     <p class="mb-0 d-flex align-items-center">
                        <i class="fas fa-play-circle fs-40 color-verde"></i>
                        <span class="ms-3 font-poppins weight-700 fs-18 color-444">
                           Mais de 600 vídeo-aulas dinâmicas
                        </span>
                     </p>
                  </div>
               </div>
               <div class="col-lg-4 px-2 rounded-2 mb-3">
                  <div class="bg-white px-4-5 d-flex align-items-center box-info">
                     <p class="mb-0 d-flex align-items-center">
                        <i class="fas fa-book-reader fs-40 color-verde"></i>
                        <span class="ms-3 font-poppins weight-700 fs-18 color-444">
                           Diversas disciplinas modulares
                        </span>
                     </p>
                  </div>
               </div>
               <div class="col-lg-4 px-2 rounded-2 mb-3">
                  <div class="bg-white px-4-5 d-flex align-items-center box-info">
                     <p class="mb-0 d-flex align-items-center">
                        <i class="fas fa-book fs-40 color-verde"></i>
                        <span class="ms-3 font-poppins weight-700 fs-18 color-444">
                           Apostilas e E-books para baixar em PDF
                        </span>
                     </p>
                  </div>
               </div>
               <div class="col-lg-4 px-2 rounded-2 mb-3">
                  <div class="bg-white px-4-5 d-flex align-items-center box-info">
                     <p class="mb-0 d-flex align-items-center">
                        <i class="fas fa-comment-dots fs-40 color-verde"></i>
                        <span class="ms-3 font-poppins weight-700 fs-18 color-444">
                           Plantão de Dúvidas e Tutoria Online
                        </span>
                     </p>
                  </div>
               </div>
               <div class="col-lg-4 px-2 rounded-2 mb-3">
                  <div class="bg-white px-4-5 d-flex align-items-center box-info">
                     <p class="mb-0 d-flex align-items-center">
                        <i class="fas fa-chalkboard-teacher fs-40 color-verde"></i>
                        <span class="ms-3 font-poppins weight-700 fs-18 color-444">
                           Professores experientes de cursinhos presenciais
                        </span>
                     </p>
                  </div>
               </div>
               <div class="col-lg-4 px-2 rounded-2 mb-3">
                  <div class="bg-white px-4-5 d-flex align-items-center box-info">
                     <p class="mb-0 d-flex align-items-center">
                        <i class="fa fa-edit fs-40 color-verde"></i>
                        <span class="ms-3 font-poppins weight-700 fs-18 color-444">
                           Avaliações, Provas e Simulados Online
                        </span>
                     </p>
                  </div>
               </div>

            </div>

         </div>
      </section>

      <section id="blog_destaque" class="pb-lg-5">
      <div class="container pb-lg-5">

         <h1 class="fs-md-40 font-montserrat weight-800 text-dark">NOTÍCIAS/<span class="color-faixa-verde">BLOG</span></h1>

         <div class="d-lg-none mb-5 mb-lg-0">

            <div class="uk-position-relative uk-visible-toggle" tabindex="-1" uk-slideshow="animation: push">

               <ul class="uk-slideshow-items">
                     <li>
                     <a href="/blog/post/{{$postDestaque1->slug}}/{{$postDestaque1->id}}">
                        <div class="w-100 rounded-2">
                           <div class="bg-size-cover h-md-500 h-xs-250 rounded-2" style="background-image: linear-gradient(to bottom, rgba(0, 0, 0, .1), rgba(0, 0, 0, 1)), url('{{ $postDestaque1->foto_capa }}');"></div>
                           <div class="uk-position-bottom px-4 pb-4">
                              <h5 class="text-light text-shadow-2 weight-700 fs-md-30 fs-xs-18 font-montserrat mb-0 p-0">
                                 {{ $postDestaque1->titulo }}
                              </h5>
                              <hr class="border" />
                              <div class="d-flex justify-content-between align-items-center">
                                 <p class="mb-0 fs-12">
                                 @if($postDestaque1->autor)
                                    <span class="color-c9">por</span>
                                    <span class="color-c9 weight-600">{{ \App\Models\Util::getPrimeiroSegundoNome($postDestaque1->autor) }}</span>
                                 @endif
                                 </p>
                                 <p class="mb-0 font-nunito fs-12">
                                 <i class="fa fa-clock color-c9"></i>
                                 <span class="color-c9">{{ \Carbon\Carbon::createFromTimeStamp(strtotime($postDestaque1->created_at))->diffForHumans() }}</span>
                                 </p>
                              </div>
                           </div>
                        </div>
                     </a>
                     </li>
                     <li>
                     <a href="/blog/post/{{$postDestaque2->slug}}/{{$postDestaque2->id}}">
                        <div class="w-100 rounded-2">
                           <div class="bg-size-cover h-md-500 h-xs-250 rounded-2" style="background-image: linear-gradient(to bottom, rgba(0, 0, 0, .1), rgba(0, 0, 0, 1)), url('{{ $postDestaque2->foto_capa }}');"></div>
                           <div class="uk-position-bottom px-4 pb-4">
                              <h5 class="text-light text-shadow-2 weight-700 fs-md-25 fs-xs-18 font-montserrat mb-0 p-0">
                                 {{ $postDestaque2->titulo }}
                              </h5>
                              <hr class="border" />
                              <div class="d-flex justify-content-between align-items-center">
                                 <p class="mb-0 fs-12">
                                 @if($postDestaque2->autor)
                                    <span class="color-c9">por</span>
                                    <span class="color-c9 weight-600">{{ \App\Models\Util::getPrimeiroSegundoNome($postDestaque2->autor) }}</span>
                                 @endif
                                 </p>
                                 <p class="mb-0 font-nunito fs-12">
                                 <i class="fa fa-clock color-c9"></i>
                                 <span class="color-c9">{{ \Carbon\Carbon::createFromTimeStamp(strtotime($postDestaque2->created_at))->diffForHumans() }}</span>
                                 </p>
                              </div>
                           </div>
                        </div>
                     </a>
                     </li>
                     <li>
                     <a href="/blog/post/{{$postDestaque3->slug}}/{{$postDestaque3->id}}">
                        <div class="w-100 rounded-2">
                           <div class="bg-size-cover h-md-500 h-xs-250" style="background-image: linear-gradient(to bottom, rgba(0, 0, 0, .1), rgba(0, 0, 0, 1)), url('{{ $postDestaque3->foto_capa }}');"></div>
                           <div class="uk-position-bottom px-4 pb-4">
                              <h5 class="text-light text-shadow-2 weight-700 fs-18 font-montserrat mb-0 p-0">
                                 {{ $postDestaque3->titulo }}
                              </h5>
                              <hr class="border" />
                              <div class="d-flex justify-content-between align-items-center">
                                 <p class="mb-0 fs-12">
                                 @if($postDestaque3->autor)
                                 <span class="color-c9">por</span>
                                 <span class="color-c9 weight-600">{{ \App\Models\Util::getPrimeiroSegundoNome($postDestaque3->autor) }}</span>
                                 @endif
                                 </p>
                              <p class="mb-0 font-nunito fs-12">
                                 <i class="fa fa-clock color-c9"></i>
                                 <span class="color-c9">{{ \Carbon\Carbon::createFromTimeStamp(strtotime($postDestaque3->created_at))->diffForHumans() }}</span>
                              </p>
                              </div>
                           </div>
                        </div>
                     </a>
                     </li>
                     <li>
                        <a href="/blog/post/{{$postDestaque4->slug}}/{{$postDestaque4->id}}">
                           <div class="w-100 rounded-2">
                              <div class="bg-size-cover h-md-500 h-xs-250" style="background-image: linear-gradient(to bottom, rgba(0, 0, 0, .1), rgba(0, 0, 0, 1)), url('{{ $postDestaque4->foto_capa }}');"></div>
                              <div class="uk-position-bottom px-4 pb-4">
                                 <h5 class="text-light text-shadow-2 weight-700 fs-18 font-montserrat mb-0 p-0">
                                    {{ $postDestaque4->titulo }}
                                 </h5>
                                 <hr class="border" />
                                 <div class="d-flex justify-content-between align-items-center">
                                 <p class="mb-0 fs-12">
                                    @if($postDestaque4->autor)
                                    <span class="color-c9">por</span>
                                    <span class="color-c9 weight-600">{{ \App\Models\Util::getPrimeiroSegundoNome($postDestaque4->autor) }}</span>
                                    @endif
                                 </p>
                                 <p class="mb-0 font-nunito fs-12">
                                    <i class="fa fa-clock color-c9"></i>
                                    <span class="color-c9">{{ \Carbon\Carbon::createFromTimeStamp(strtotime($postDestaque4->created_at))->diffForHumans() }}</span>
                                 </p>
                                 </div>
                              </div>
                           </div>
                        </a>
                     </li>
               </ul>

               <div class="uk-light">
                     <a class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous uk-slideshow-item="previous"></a>
                     <a class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-next uk-slideshow-item="next"></a>
               </div>

            </div>

         </div>

         <div class="d-md-none d-xs-none d-lg-block">

            <div class="row">

               <div class="col-lg-6 px-lg-2">
                  
                  <a href="/blog/post/{{$postDestaque1->slug}}/{{$postDestaque1->id}}">
                     <div class="w-100 uk-inline-clip uk-transition-toggle box-convenios rounded-2">
                        <div class="uk-transition-scale-up uk-transition-opaque bg-size-cover h-lg-500 h-xs-250 rounded-2" style="background-image: linear-gradient(to bottom, rgba(0, 0, 0, .1), rgba(0, 0, 0, 1)), url('{{ $postDestaque1->foto_capa }}');"></div>
                        <div class="uk-position-bottom px-4 pb-4">
                           <h5 class="text-light text-shadow-2 weight-700 fs-md-30 fs-xs-18 font-montserrat mb-0 p-0">
                              {{ $postDestaque1->titulo }}
                           </h5>
                           <hr class="border" />
                           <div class="d-flex justify-content-between align-items-center">
                              <p class="mb-0 fs-12">
                              @if($postDestaque1->autor)
                              <span class="color-c9">por</span>
                              <span class="color-c9 weight-600">{{ \App\Models\Util::getPrimeiroSegundoNome($postDestaque1->autor) }}</span>
                              @endif
                              </p>
                           <p class="mb-0 font-nunito fs-12">
                              <i class="fa fa-clock color-c9"></i>
                              <span class="color-c9">{{ \Carbon\Carbon::createFromTimeStamp(strtotime($postDestaque1->created_at))->diffForHumans() }}</span>
                           </p>
                           </div>
                        </div>
                     </div>
                  </a>

               </div>

               <div class="col-lg-3 px-lg-2 py-3 py-lg-0">

                  <a href="/blog/post/{{$postDestaque2->slug}}/{{$postDestaque2->id}}">
                     <div class="w-100 uk-inline-clip uk-transition-toggle box-convenios rounded-2">
                        <div class="uk-transition-scale-up uk-transition-opaque bg-size-cover h-lg-500 h-xs-250 rounded-2" style="background-image: linear-gradient(to bottom, rgba(0, 0, 0, .1), rgba(0, 0, 0, 1)), url('{{ $postDestaque2->foto_capa }}');"></div>
                        <div class="uk-position-bottom px-4 pb-4">
                           <h5 class="text-light text-shadow-2 weight-700 fs-md-25 fs-xs-18 font-montserrat mb-0 p-0">
                              {{ $postDestaque2->titulo }}
                           </h5>
                           <hr class="border" />
                           <div class="d-flex justify-content-between align-items-center">
                              <p class="mb-0 fs-12">
                              @if($postDestaque2->autor)
                              <span class="color-c9">por</span>
                              <span class="color-c9 weight-600">{{ \App\Models\Util::getPrimeiroSegundoNome($postDestaque2->autor) }}</span>
                              @endif
                              </p>
                           <p class="mb-0 font-nunito fs-12">
                              <i class="fa fa-clock color-c9"></i>
                              <span class="color-c9">{{ \Carbon\Carbon::createFromTimeStamp(strtotime($postDestaque2->created_at))->diffForHumans() }}</span>
                           </p>
                           </div>
                        </div>
                     </div>
                  </a>

               </div>

               <div class="col-lg-3 px-lg-2">

                  <div>
                     <a href="/blog/post/{{$postDestaque3->slug}}/{{$postDestaque3->id}}">
                        <div class="w-100 uk-inline-clip uk-transition-toggle box-convenios rounded-2">
                           <div class="uk-transition-scale-up uk-transition-opaque bg-size-cover h-lg-240 h-xs-250" style="background-image: linear-gradient(to bottom, rgba(0, 0, 0, .1), rgba(0, 0, 0, 1)), url('{{ $postDestaque3->foto_capa }}');"></div>
                           <div class="uk-position-bottom px-4 pb-4">
                              <h5 class="text-light text-shadow-2 weight-700 fs-18 font-montserrat mb-0 p-0">
                                 {{ $postDestaque3->titulo }}
                              </h5>
                              <hr class="border" />
                              <div class="d-flex justify-content-between align-items-center">
                                 <p class="mb-0 fs-12">
                                 @if($postDestaque3->autor)
                                 <span class="color-c9">por</span>
                                 <span class="color-c9 weight-600">{{ \App\Models\Util::getPrimeiroSegundoNome($postDestaque3->autor) }}</span>
                                 @endif
                                 </p>
                              <p class="mb-0 font-nunito fs-12">
                                 <i class="fa fa-clock color-c9"></i>
                                 <span class="color-c9">{{ \Carbon\Carbon::createFromTimeStamp(strtotime($postDestaque3->created_at))->diffForHumans() }}</span>
                              </p>
                              </div>
                           </div>
                        </div>
                     </a>
                  </div>
                  <div class="py-2"></div>
                  <div>
                     <a href="/blog/post/{{$postDestaque4->slug}}/{{$postDestaque4->id}}">
                        <div class="w-100 uk-inline-clip uk-transition-toggle box-convenios rounded-2">
                           <div class="uk-transition-scale-up uk-transition-opaque bg-size-cover h-lg-240 h-xs-250" style="background-image: linear-gradient(to bottom, rgba(0, 0, 0, .1), rgba(0, 0, 0, 1)), url('{{ $postDestaque4->foto_capa }}');"></div>
                           <div class="uk-position-bottom px-4 pb-4">
                              <h5 class="text-light text-shadow-2 weight-700 fs-18 font-montserrat mb-0 p-0">
                                 {{ $postDestaque4->titulo }}
                              </h5>
                              <hr class="border" />
                              <div class="d-flex justify-content-between align-items-center">
                                 <p class="mb-0 fs-12">
                                 @if($postDestaque4->autor)
                                 <span class="color-c9">por</span>
                                 <span class="color-c9 weight-600">{{ \App\Models\Util::getPrimeiroSegundoNome($postDestaque4->autor) }}</span>
                                 @endif
                                 </p>
                              <p class="mb-0 font-nunito fs-12">
                                 <i class="fa fa-clock color-c9"></i>
                                 <span class="color-c9">{{ \Carbon\Carbon::createFromTimeStamp(strtotime($postDestaque4->created_at))->diffForHumans() }}</span>
                              </p>
                              </div>
                           </div>
                        </div>
                     </a>
                  </div>

               </div>

            </div>

         </div>

      </div>
      </section>

      <section id="newsletter" class="py-5 bg-newsletter">
         <div class="container-fluid px-xl-7" id="contato">

            @if(session('success'))
               <div class='alert alert-success'><b>Sucesso!</b> {{ session('success') }}</div>
            @endif

            <div class="row justify-content-between align-items-center">

               <div class="col-lg-4">
                  <p class="mb-0 weight-700 text-shadow-2 fs-25 pe-lg-5 font-roboto">
                     <span class="text-white">Que tal receber nosso contato para tirar todas as suas dúvidas?</span><span class="color-amarelo"> Cadastre-se!</span>
                  </p>
               </div>

               <form method="POST" class="col-lg-8 mt-3 mt-lg-0">
               @csrf

                  <input type="hidden" name="assunto" value="Solicitar Contato">

                  <div class="d-lg-flex w-100">

                     <input type="text" name="nome" class="w-100 form-control lead-form rounded-left rounded-right-lg-0" placeholder="NOME" required>
                     
                     <div class="d-xs-none line-form"></div>
                     
                     <input type="tel" name="celular" class="my-2 my-lg-0 w-100 form-control lead-form rounded-lg-0" placeholder="(DDD) CELULAR" required>
                     
                     <div class="d-xs-none line-form"></div>

                     <input type="email" name="email" class="w-100 form-control lead-form rounded-lg-0" placeholder="EMAIL" required>

                     <input type="text" name="check" class="invisible w-0px h-0px" placeholder="Não preencha este campo.">

                     <button type="submit" class="btn-news" name="sentContato" value="S">
                        Enviar
                     </button>

                  </div>

               </form>
            </div>

         </div>
      </section>

      <a href="https://api.whatsapp.com/send?phone=55{{preg_replace('/\D/', '', $plataforma->whatsapp)}}&text=Olá, vim através do Site {{ $plataforma->nome }}" target="_blank" class="text-white fs-30">
            <div class="position-fixed bottom-0 right-0 me-3 me-lg-4 mb-3 mb-lg-4 z-4 wpp-pulse-button z-8">
            <i class="fab fa-whatsapp"></i>
            </div>
      </a>

   </main>

   @include('sites-proprios/brasedu/componentes/footer')

   <div class="box-cookies d-none fixed-bottom bg-white py-3 px-4 box-shadow z-9">
      <div class="d-flex align-items-center justify-content-between px-xl-3">
         <p class="msg-cookies font-poppins mb-0 p-0 color-faixa-verde weight-600">
            Este site usa cookies para garantir que você obtenha a melhor experiência. 
            <a href="/politica-de-privacidade" target="_blank" class="color-blue-info-link text-decoration-underline">
            Política de Privacidade
            </a>
         </p>
         <button class="btn-cookies btnLGPD box-shadow weight-700 box-shadow">Ok!</button>
      </div>
   </div>
  
  <script>
  (() => {
    if (!localStorage.getItem('accept')) {
      document.querySelector(".box-cookies").classList.remove('d-none');
    }
    
    const acceptCookies = () => {
      document.querySelector(".box-cookies").classList.add('d-none');
      localStorage.setItem('accept', 'S');
    };
    
    const btnCookies = document.querySelector(".btn-cookies");

    btnCookies.addEventListener('click', acceptCookies);
  })();
  </script>

   <script>

      window.onload = function()
      {
         getCursosByCategoria(59);
      };

      function rowCursos(data)
      {
          let row = `
               <div class="col-lg-4 mb-4">
                  <div class="card border-0 box-shadow rounded">

                     <div class="card-header px-3 px-1 d-flex justify-content-center align-items-center position-relative text-center" style="min-height: 75px;">
                        <h5 class="mb-0 pb-0 text-white weight-700 text-shadow-2 ls-05 font-montserrat">${data.nome}</h5>
                     </div>
                     
                     <img src="${data.foto_capa ?? '{{ asset('images/no-image.jpeg') }}'}" class="img-fluid h-200 object-fit-cover">

                      <div class="position-relative p-4">

                         <div>
                            <span class="d-inline-block mb-2 me-1 text-white font-poppins weight-500 bg-newsletter px-3 py-1-5 box-shadow text-shadow-1 rounded fs-14 ls-05">${data.nivel ?? 'Graduação'}</span>
                            <span class="d-inline-block mb-2 me-1 text-white font-poppins weight-500 bg-newsletter px-3 py-1-5 box-shadow text-shadow-1 rounded fs-14 ls-05">${data.carga_horaria ?? '0 horas'}</span>
                            <span class="d-inline-block mb-2 me-1 text-white font-poppins weight-500 bg-newsletter px-3 py-1-5 box-shadow text-shadow-1 rounded fs-14 ls-05">${data.modalidade ?? 'EAD'}</span>
                         </div>

                         <hr class="border border-secondary" />

                         <div>
                            <div class="text-dark weight-600">
                               <p class="m-0">
                                  R$ ${data.valor_promocional ?? '0.000,00'} a vista ou
                               </p>
                               <p class="m-0 fs-23 font-montserrat weight-800 ls-05">
                                  ${data.valor_parcelado ?? '00x de R$ 000,00'}
                               </p>
                            </div>
                         </div>

                         <hr class="border border-secondary">

                         <div class="text-center w-100 mt-3">
                             <a href="/curso/${data.slug}" class="btnSaibaMais">
                                SAIBA MAIS
                             </a>
                         </div>

                      </div>

                     </div>

                  </div>
               </div>
          `;

          $("#rowCursos").append(row);
      }


      function getCursosByCategoria(categoria_id)
      {
         $("#rowCursos").empty();
         $("#boxCarregandoCursos").show();

         fetch('{{ route('brasedu.cursos.by.categoria.id') }}?categoria_id='+categoria_id)
            .then(response => response.text())
            .then(res => { 
               try {                    
                  
                  data = JSON.parse(res);

                  data.forEach((item) => {
                    rowCursos(item);
                  });

                  $("#boxCarregandoCursos").hide();
            
               } catch (error) {
                  
                  console.log(error);
               }   
          
         });
      }

    </script>

</body>
</html>