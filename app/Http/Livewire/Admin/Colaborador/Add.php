<?php

namespace App\Http\Livewire\Admin\Colaborador;

use Livewire\Component;
use App\Models\User;
use App\Models\UserEndereco;
use App\Models\UserCategoria;
use App\Models\Categoria;
use App\Models\PlataformaModulo;
use App\Models\AclPlataformaModuloUser;
use App\Models\Unidade;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Http;

class Add extends Component
{
    public $user;
    public $nome;
    public $email;
    public $password;
    public $status = 0;
    public $unidade_id;
    public $restringir_unidade = 'N';
    public $cpf;
    public $rg;
    public $matricula;
    public $data_nascimento;
    public $celular;
    public $telefone;
    public $genero;
    public $profissao;
    public $naturalidade;
    public $obs;
    public $descricao;

    public $endereco_cep;
    public $endereco_rua;
    public $endereco_numero;
    public $endereco_bairro;
    public $endereco_cidade;
    public $endereco_estado;
    public $endereco_complemento;
    public $endereco_ponto_referencia;

    public $categoriasSelected = [];

    public $modulos = [];
    public $acl = [];

    protected $rules = [
        'nome'     => 'required',
        'password' => 'required',
    ];

    protected $messages = [
        'email.required' => 'O Email não pode estar vazio.',
        'email.email'    => 'O formato do email não está correto.',
    ];

    public function add()
    {
        $this->validate();

        $checkEmailExists = User::plataforma()->where('email', $this->email)->exists();

        if($checkEmailExists)
        {
            session()->flash('danger-livewire', 'Já existe um colaborador cadastrado com este email');

        } else {

            $this->user = User::create([
                'nome'               => $this->nome,
                'email'              => $this->email,
                'password'           => Hash::make($this->password),
                'status'             => $this->status,
                'unidade_id'         => $this->unidade_id,
                'restringir_unidade' => $this->restringir_unidade,
                'cpf'                => $this->cpf,
                'rg'                 => $this->rg,
                'matricula'          => $this->matricula,
                'data_nascimento'    => $this->data_nascimento,
                'celular'            => $this->celular,
                'telefone'           => $this->telefone,
                'profissao'          => $this->profissao,
                'naturalidade'       => $this->naturalidade,
                'genero'             => $this->genero,
                'obs'                => $this->obs,
                'descricao'          => $this->descricao,
                'tipo'               => 'colaborador',
            ]);

            if($this->endereco_cep != null || $this->endereco_rua != null || $this->endereco_numero != null || $this->endereco_bairro != null || $this->endereco_cidade != null || $this->endereco_estado != null || $this->endereco_complemento != null || $this->endereco_ponto_referencia != null)
                UserEndereco::create([
                    'plataforma_id'    =>  $this->user->plataforma_id,
                    'user_id'          =>  $this->user->id,
                    'cep'              =>  $this->endereco_cep,
                    'rua'              =>  $this->endereco_rua,
                    'numero'           =>  $this->endereco_numero,
                    'bairro'           =>  $this->endereco_bairro,
                    'cidade'           =>  $this->endereco_cidade,
                    'estado'           =>  $this->endereco_estado,
                    'complemento'      =>  $this->endereco_complemento,
                    'ponto_referencia' =>  $this->endereco_ponto_referencia,
                ]);


            $this->syncACL();
            $this->syncCategorias($this->user->id);

            session()->flash('success-livewire', 'Colaborador criado com êxito.');
        }
    }

    public function syncACL()
    {
        AclPlataformaModuloUser::plataforma()->where('user_id', $this->user->id)->delete();

        foreach($this->acl as $modulo_id => $nivel)
        {
            AclPlataformaModuloUser::create([
                'user_id'   => $this->user->id,
                'modulo_id' => $modulo_id,
                'nivel'     => $nivel,
            ]);
        }
    }
    
    public function syncCategorias($user_id)
    {
        UserCategoria::plataforma()->where('user_id', $user_id)->delete();

        if($this->categoriasSelected == null)
            return;

        foreach($this->categoriasSelected as $categoria_id)
        {
            $countDuplicate = UserCategoria::plataforma()->where('user_id', $user_id)->where('categoria_id', $categoria_id)->count();
            
            if($countDuplicate <= 0)
                UserCategoria::create([
                    'plataforma_id' => session('plataforma_id'),
                    'user_id'       => $user_id, 
                    'categoria_id'  => $categoria_id,
                ]);
        }
    }
    
    public function setEndereco()
    {
        // Retorna endereço do ViaCEP.
        $response = Http::get("http://viacep.com.br/ws/{$this->endereco_cep}/json/");
        $viacep = $response->json();
        $this->endereco_rua    = $viacep['logradouro'] ?? null;
        $this->endereco_bairro = $viacep['bairro'] ?? null;
        $this->endereco_cidade = $viacep['localidade'] ?? null;
        $this->endereco_estado = $viacep['uf'] ?? null;
    }

    public function mount()
    {
        $this->unidade_id = \Auth::user()->unidade_id;
        
        if(\Auth::user()->restringir_unidade == 'S')
        {
            $this->restringir_unidade = 'S';
        }

        $this->modulos = PlataformaModulo::meusModulosSidebar();

        foreach($this->modulos as $modulo)
        {
            $this->acl[$modulo->id] = '2';
        }
    }

    public function render()
    {
        $categorias = Categoria::plataforma()->ativo()->tipo('colaborador')->orderBy('nome')->get();
        $unidades   = Unidade::plataforma()->ativo()->unidade()->orderBy('nome')->get();

        return view('livewire.admin.colaborador.add', ['categorias' => $categorias, 'unidades' => $unidades]);
    }

}
