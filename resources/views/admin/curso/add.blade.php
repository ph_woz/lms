@extends('layouts.admin.master')

@section('css')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.14.0-beta2/dist/css/bootstrap-select.min.css">
@endsection

@section('content')
    
<div>

    <form method="POST" enctype="multipart/form-data" class="mb-4">
    @csrf

		<section class="mb-4-5">
			
			<a href="#boxInfoPrincipal" data-bs-toggle="collapse" aria-expanded="false" class="hover-d-none">
				<div class="section-title">
					Informações Principais
				</div>
			</a>
			<hr/>
			
			<div class="collapse show" id="boxInfoPrincipal">

		    	<div class="d-lg-flex mb-3">

			    	<div class="w-100">
			    		<label>Nome Comercial</label>
			    		<input type="text" name="nome" class="form-control p-form" placeholder="Nome que será exibido para o público e alunos" required>
			    	</div>
			    	<div class="w-100 mx-lg-3 my-3 my-lg-0">
			    		<label>Nome de Referência</label>
			    		<input type="text" name="referencia" class="form-control p-form" placeholder="Nome que será exibido somente para Colaboradores" required>
			    	</div>
			    	<div class="w-lg-35">
			    		<label>Status</label>
			    		<select name="status" class="select-form">
			    			<option value="0">Ativo</option>
			    			<option value="1">Desativado</option>
			    		</select>
			    	</div>

			    </div>

			    <div class="d-lg-flex mb-3">
					<div class="w-100">
						<label>Certificado de Conclusão</label>
						<select name="certificado_id" class="selectpicker" title="Selecione" data-width="100%" data-live-search="true">
							<option value="">Selecione</option>
							@foreach($certificados as $certificado)
								<option value="{{ $certificado->id }}">
									{{ $certificado->referencia }}
								</option>
							@endforeach
						</select>
					</div>

					<div class="w-100 mx-lg-3 my-3 my-lg-0">
						<label>Foto de Miniatura</label>
						<input type="file" name="foto_min" class="file-form">
					</div>

					<div class="w-lg-35">
						<label>Publicar</label>
						<select name="publicar" onchange="showCamposApresentacao(this.value)" class="select-form">
							<option value="N">Não</option>
							<option value="S">Sim</option>
						</select>
					</div>

			    </div>

			    <div id="boxCursoApresentacao" style="display:none;">

			    	<div class="d-lg-flex mb-3">

						<div class="w-100 me-lg-3 mt-3 mt-lg-0">
							<label>Slogan</label>
							<input type="text" name="slogan" class="form-control p-form" placeholder="Curta descrição" maxlength="275">
						</div>

						<div class="w-lg-80 me-lg-3 mt-3 mt-lg-0">
							<label>Coordenador</label>
							<select name="coordenador_id" class="selectpicker" title="Selecione" data-width="100%" data-live-search="true">
								<option value="">Selecione</option>
								@foreach($colaboradores as $colaborador)
									<option value="{{ $colaborador->id }}">
										{{ $colaborador->nome }}
									</option>
								@endforeach
							</select>
						</div>

						<div class="w-100 me-lg-3 mt-3 mt-lg-0">
							<label>Vídeo de Apresentação</label>
							<input type="text" name="video" class="form-control p-form" placeholder="URL de vídeo">
						</div>

						<div class="w-lg-75">
							<label>Exibir Conteúdo do Curso</label>
							<select name="exibir_cc" class="select-form">
								<option value="N">Não</option>
								<option value="S">Sim</option>
							</select>
						</div>

					</div>

				    <div class="d-lg-flex mb-3">

						<div class="w-100 me-lg-3 mt-3 mt-lg-0">
							<label>Carga horária</label>
							<input type="text" name="carga_horaria" class="form-control p-form" placeholder="N° de horas">
						</div>
						<div class="w-100 me-lg-3 mt-3 mt-lg-0">
							<label>Duração</label>
							<input type="text" name="duracao" class="form-control p-form" placeholder="Ex: 5 anos" maxlength="275">
						</div>
						<div class="w-100">
							<label>Tipo de Formação</label>
							<input type="text" name="tipo_formacao" class="form-control p-form">
						</div>
						<div class="w-100 mx-lg-3 my-3 my-lg-0">
							<label>Nível</label>
							<select name="nivel" class="select-form">
								<option value="">Selecione</option>
								<option value="Graduação">Graduação</option>
								<option value="Pós-Graduação">Pós-Graduação</option>
								<option value="Segunda Graduação">Segunda Graduação</option>
								<option value="Segunda Licenciatura">Segunda Licenciatura</option>
								<option value="Cursos de Extensão">Cursos de Extensão</option>
								<option value="Formação Pedagógica R2">Formação Pedagógica R2</option>
								<option value="Aperfeiçoamento">Aperfeiçoamento</option>
								<option value="Curso Livre">Curso Livre</option>
								<option value="Curso Profissionalizante">Curso Profissionalizante</option>
								<option value="Curso Técnico">Curso Técnico</option>
								<option value="Reforço Escolar">Reforço Escolar</option>
								<option value="Preparatório">Preparatório</option>
								<option value="Norma Regulamentadora">Norma Regulamentadora</option>
							</select>
						</div>

						<div class="w-100 me-lg-3 mb-3 mb-lg-0">
							<label>Foto de Capa</label>
							<input type="file" name="foto" class="file-form">
						</div>

						<div class="w-100">
							<label>Matriz Curricular</label>
							<input type="file" name="matriz" class="file-form">
						</div>

					</div>

					<div class="mb-3">
						<label>Descrição</label>
						<textarea name="descricao" class="descricao textarea-form" placeholder="Descrição"></textarea>
					</div>

				</div>

			</div>

		</section>

		<section class="mb-4-5">
			
			<a href="#boxCategorias" data-bs-toggle="collapse" aria-expanded="false" class="hover-d-none">
				<div class="section-title">
					Categorias 
				</div>
			</a>
			<hr/>

			<div class="collapse" id="boxCategorias">
				<select name="categorias[]" class="selectpicker" multiple data-width="100%" data-live-search="true" title="Selecione">
					@foreach($categorias as $categoria)
						<option value="{{ $categoria->id }}">{{ $categoria->nome }}</option>
					@endforeach
				</select>
			</div>

		</section>

		<section class="mb-4-5" id="boxCursoSEO" style="display:none;">

			<a href="#boxSeo" data-bs-toggle="collapse" aria-expanded="false" class="hover-d-none">
				<div class="section-title">
					Informações de SEO para resultados de buscas em Navegadores
				</div>
			</a>
			<hr/>
			
			<div class="collapse" id="boxSeo">

				<div class="d-lg-flex mb-3">

					<div class="w-100 me-lg-3 mb-3 mb-lg-0">
						<label>Título</label>
						<input type="text" name="seo_title" class="form-control p-form" placeholder="Title">
					</div>

					<div class="w-100">
						<label>Palavras-chaves</label>
						<input type="text" name="seo_keywords" class="form-control p-form" placeholder="Keywords">
					</div>

				</div>

				<div class="w-100 mb-3">
					<label>Descrição</label>
					<input type="text" name="seo_description" class="form-control p-form" placeholder="Description">
				</div>
				
			</div>

		</section>

		<button type="submit" name="sent" value="ok" class="btn-add mt-3">Cadastrar</button>	

    </form>

</div>


@endsection

@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.14.0-beta2/dist/js/bootstrap-select.min.js"></script>
<script src="https://cdn.tiny.cloud/1/f50dygt4s4q2k3urd4bh4c2hkjb74m6exr798tjwbayaa0om/tinymce/5/tinymce.min.js"></script>
<script>

	function showCamposApresentacao(value)
	{
		if(value == 'S')
		{
			document.getElementById('boxCursoApresentacao').style.display = 'block';
			document.getElementById('boxCursoSEO').style.display = 'block';
		
		} else {

			document.getElementById('boxCursoApresentacao').style.display = 'none';
			document.getElementById('boxCursoSEO').style.display = 'none';
		}
	}

</script>
<script>
	tinymce.init({
		selector: '.descricao',
		language: 'pt_BR',
		height: 300,
		plugins: 'image lists link code',
		toolbar: "image | undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link | code"
	});
</script>
@endsection
