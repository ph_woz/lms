<?php

namespace App\Http\Controllers\SiteProprio;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Banner;
use App\Models\Trilha;
use App\Models\TrilhaCategoria;
use App\Models\PaginaConteudo;
use App\Models\BlogPost;

class BrasEduController extends Controller
{
    public function index()
    {
        $banners = Banner::plataforma()->ativo()->orderBy('ordem')->get();

        $seo = PaginaConteudo::plataforma()->where('url','/')->select('plataforma_id','seo_title','seo_keywords','seo_description')->first();


        $postDestaque1 = BlogPost::select('blog_posts.id','blog_posts.titulo','blog_posts.slug','blog_posts.foto_capa','blog_posts.created_at','autor.nome as autor')
            ->leftJoin('users as autor','blog_posts.autor_id','autor.id')
            ->where('blog_posts.plataforma_id', $seo->plataforma_id)
            ->where('blog_posts.publicar', 'S')
            ->where('blog_posts.destaque_ordem', 1)
            ->first();

        if(is_null($postDestaque1))
        {
            $postDestaque1 = (object)['id' => 0, 'titulo' => 'Não definido', 'slug' => 'n-d' ,'foto_capa' => 'https://itspensino.com.br/images/no-image.jpeg', 'created_at' => date('Y-m-d H:i'), 'autor' => 'Não definido'];
        }


        $postDestaque2 = BlogPost::select('blog_posts.id','blog_posts.titulo','blog_posts.slug','blog_posts.foto_capa','blog_posts.created_at','autor.nome as autor')
            ->leftJoin('users as autor','blog_posts.autor_id','autor.id')
            ->where('blog_posts.plataforma_id', $seo->plataforma_id)
            ->where('blog_posts.publicar', 'S')
            ->where('blog_posts.destaque_ordem', 2)
            ->first();

        if(is_null($postDestaque2))
        {
            $postDestaque2 = (object)['id' => 0, 'titulo' => 'Não definido', 'slug' => 'n-d' ,'foto_capa' => 'https://itspensino.com.br/images/no-image.jpeg', 'created_at' => date('Y-m-d H:i'), 'autor' => 'Não definido'];
        }

        $postDestaque3 = BlogPost::select('blog_posts.id','blog_posts.titulo','blog_posts.slug','blog_posts.foto_capa','blog_posts.created_at','autor.nome as autor')
            ->leftJoin('users as autor','blog_posts.autor_id','autor.id')
            ->where('blog_posts.plataforma_id', $seo->plataforma_id)
            ->where('blog_posts.publicar', 'S')
            ->where('blog_posts.destaque_ordem', 3)
            ->first();

        if(is_null($postDestaque3))
        {
            $postDestaque3 = (object)['id' => 0, 'titulo' => 'Não definido', 'slug' => 'n-d' ,'foto_capa' => 'https://itspensino.com.br/images/no-image.jpeg', 'created_at' => date('Y-m-d H:i'), 'autor' => 'Não definido'];
        }

        $postDestaque4 = BlogPost::select('blog_posts.id','blog_posts.titulo','blog_posts.slug','blog_posts.foto_capa','blog_posts.created_at','autor.nome as autor')
            ->leftJoin('users as autor','blog_posts.autor_id','autor.id')
            ->where('blog_posts.plataforma_id', $seo->plataforma_id)
            ->where('blog_posts.publicar', 'S')
            ->where('blog_posts.destaque_ordem', 4)
            ->first();


        if(is_null($postDestaque4))
        {
            $postDestaque4 = (object)['id' => 0, 'titulo' => 'Não definido', 'slug' => 'n-d' ,'foto_capa' => 'https://itspensino.com.br/images/no-image.jpeg', 'created_at' => date('Y-m-d H:i'), 'autor' => 'Não definido'];
        }

        if(request('sentContato'))
        {
            if(request('check'))
            {
                return back()->with('danger', 'SPAM Detected!');
            }

            \App\Models\Contato::create(request()->all() + [
                'plataforma_id' => $seo->plataforma_id,
                'tipo' => 'L',
            ]);
            
            return redirect('/#newsletter')->with('success', 'Inscrição enviada com êxito.');
        }

    	return view('sites-proprios.brasedu.home', compact('banners','seo','postDestaque1','postDestaque2','postDestaque3','postDestaque4') );
    }

    public function sobreNos()
    {
        $pagina = PaginaConteudo::plataforma()->ativo()->where('url','/sobre-nos')->first();


        return view('sites-proprios.brasedu.sobre-nos', compact('pagina') );
    }

    public function curso($slug)
    {
        $trilha = Trilha::plataforma()->publico()->where('slug', $slug)->first();

        if($trilha->exibir_turmas_como_cc == 'S')
        {
            $turmasIds = \App\Models\TrilhaTurma::select('turma_id')->plataforma()->where('trilha_id', $trilha->id)->get()->pluck('turma_id')->toArray();

            $turmas = \App\Models\Turma::select('nome')->plataforma()->ativo()->whereIn('id', $turmasIds)->orderBy('id')->get();
        
        } else {

            $turmas = [];
        }

        return view('sites-proprios.brasedu.trilha', compact('trilha','turmas') );
    }

    public function getCursosByCategoriaId()
    {
        $categoria_id = request('categoria_id');

        if($categoria_id != 0)
        {
            $cursosIds = TrilhaCategoria::select('trilha_id')->plataforma()->where('categoria_id', $categoria_id)->get()->pluck('trilha_id')->toArray();
            $cursos = Trilha::select('id','nome','slug','foto_capa','valor_promocional','valor_parcelado','nivel','carga_horaria','modalidade')->plataforma()->publico()->whereIn('id', $cursosIds)->get();
        
        } else {

            $cursos = Trilha::select('id','nome','slug','foto_capa','valor_promocional','valor_parcelado','nivel','carga_horaria','modalidade')->plataforma()->publico()->get();

        }

        return response()->json($cursos);
    }


    public function blog()
    {
        $plataforma_id = $this->getPlataformaId();
        
        $pagina = PaginaConteudo::plataforma()->where('url','/blog')->first();

        $postDestaque1 = BlogPost::select('blog_posts.id','blog_posts.titulo','blog_posts.slug','blog_posts.foto_capa','blog_posts.created_at','autor.nome as autor')
            ->leftJoin('users as autor','blog_posts.autor_id','autor.id')
            ->where('blog_posts.plataforma_id', $plataforma_id)
            ->where('blog_posts.publicar', 'S')
            ->where('blog_posts.destaque_ordem', 1)
            ->first();

        $postDestaque2 = BlogPost::select('blog_posts.id','blog_posts.titulo','blog_posts.slug','blog_posts.foto_capa','blog_posts.created_at','autor.nome as autor')
            ->leftJoin('users as autor','blog_posts.autor_id','autor.id')
            ->where('blog_posts.plataforma_id', $plataforma_id)
            ->where('blog_posts.publicar', 'S')
            ->where('blog_posts.destaque_ordem', 2)
            ->first();

        $postDestaque3 = BlogPost::select('blog_posts.id','blog_posts.titulo','blog_posts.slug','blog_posts.foto_capa','blog_posts.created_at','autor.nome as autor')
            ->leftJoin('users as autor','blog_posts.autor_id','autor.id')
            ->where('blog_posts.plataforma_id', $plataforma_id)
            ->where('blog_posts.publicar', 'S')
            ->where('blog_posts.destaque_ordem', 3)
            ->first();

        $postDestaque4 = BlogPost::select('blog_posts.id','blog_posts.titulo','blog_posts.slug','blog_posts.foto_capa','blog_posts.created_at','autor.nome as autor')
            ->leftJoin('users as autor','blog_posts.autor_id','autor.id')
            ->where('blog_posts.plataforma_id', $plataforma_id)
            ->where('blog_posts.publicar', 'S')
            ->where('blog_posts.destaque_ordem', 4)
            ->first();

        return view('sites-proprios.brasedu.blog', compact('pagina','postDestaque1','postDestaque2','postDestaque3','postDestaque4') );
    }

    public function blogPost($id)
    {
        $post = BlogPost::plataforma()->where('id', $id)->first();

        if(isset($post) && $post->publicar != 'S')
        {
            return view('sites-proprios.brasedu.sem-acesso', ['mensagem' => 'Página não publicada.']);
        }

        $ultimos_posts = BlogPost::plataforma()->publico()->orderBy('id','desc')->limit(10)->get();

        return view('sites-proprios.brasedu.blog-post', compact('post','ultimos_posts') );
    }


    public function categoria()
    {
        $plataforma_id = $this->getPlataformaId();

        $categoria = \App\Models\Categoria::select('nome','seo_title','seo_keywords','seo_description')->plataforma()->ativo()->publico()->tipo('trilha')->where('slug', \Request::segment(1))->first();

        $cursos = Trilha::select('id','nome','slug','foto_capa','valor_promocional','valor_parcelado','nivel','carga_horaria','modalidade')
            ->where('plataforma_id', $plataforma_id)
            ->where('status', 0)
            ->where('publicar', 'S')
            ->where('nivel', $categoria->nome ?? 'N/D')
            ->orderBy('nome')
            ->get();

        return view('sites-proprios.brasedu.categoria', compact('categoria','cursos') );
    }

    public function getPlataformaId()
    {
        return session('plataforma_id') ?? \App\Models\Plataforma::dominio()->pluck('id')[0] ?? null;
    }


}