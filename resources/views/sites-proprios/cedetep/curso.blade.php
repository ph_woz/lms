<!doctype html>
<html lang="pt-br">
<head>

  <!-- Google tag (gtag.js) -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=G-MREB450HCT"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'G-MREB450HCT');
  </script>
  
   <meta charset="utf-8">
   <meta name="viewport" content="width=device-width, initial-scale=1">
   
   <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/uikit@3.5.9/dist/css/uikit.min.css" />
   <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
   <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
   <link rel="stylesheet" href="{{ asset('css/util.css') }}?v=3">
   <link rel="stylesheet" href="{{ asset('sites-proprios/cedetep/css/home.css') }}">
   <link rel="stylesheet" href="{{ asset('css/pages.css') }}">

   <title>{{ $curso->seo_title ?? $curso->nome . ' - ' . $nomePlataforma }}</title>
   <meta name="robots" content="index, follow">
   <meta name="description" content="{{ $curso->seo_description ?? null }}"/>
   <meta name="keywords" content="{{ $curso->seo_keywords ?? null }}"/>
   <link rel="canonical" href="{{ Request::url() }}" />
   <link rel="shortcut icon" href="{{ $plataforma->favicon }}">
   <meta property="og:locale" content="pt_BR" />
   <meta property="og:type" content="website" />
   <meta property="og:title" content="{{ $curso->seo_title ?? $curso->nome . ' - ' . $nomePlataforma }}" />
   <meta property="og:description" content="{{ $curso->seo_description ?? null }}" />
   <meta property="og:url" content="{{ Request::url() }}" />
   <meta property="og:site_name" content="{{ $plataforma->nome }}" />
   <meta property="og:image" content="{{ $plataforma->logotipo }}" />
   <meta name='author' content='WOZCODE | Pedro Henrique' />

</head>
<body class="bg-f4" id="inicio">

   <header>
      
      <div class="sub-header container-fluid px-xl-7 py-2-3">
         <div class="row align-items-center">
            <div class="col-6 col-lg-4">

               <a href="https://api.whatsapp.com/send?phone=55{{preg_replace('/\D/', '', $plataforma->whatsapp)}}&text=Olá, vim através do Site {{ $plataforma->nome }}." target="_blank" class="btn-submenu">
                  <i class="fab fa-whatsapp fs-17"></i>
                  <span class="d-xs-none"> 
                    Matricule-se pelo WhatsApp
                  </span>
                  <span class="d-lg-none ml-1">
                    {{ $plataforma->whatsapp }}
                  </span>
               </a>

               <a href="tel:+55{{preg_replace('/\D/', '', $plataforma->telefone)}}" class="d-xs-none btn-submenu">
                  <i class="fa fa-phone fs-17"></i>
                  {{ $plataforma->telefone }}
               </a>

            </div>

            <div class="col-6 col-lg-8" align="right">

               <a href="https://canaleducacionalonline.com.br/" target="_blank" class="btn-submenu d-xs-none">
                  Cursos Profissionalizantes
               </a>
               
               <a href="/sobre-nos" class="btn-submenu d-xs-none">
                  Institucional
               </a>

               <a href="https://cedetep.com/f/trabalhe-conosco/36/t/597" target="_blank" class="btn-submenu d-xs-none">
                  Trabalhe Conosco
               </a>

               <a href="/login" class="btn-submenu btnPortalAluno">
                  Portal do Aluno
               </a>

            </div>

         </div>
      </div>

      <nav class="navbar navbar-expand-lg bg-white box-shadow" id="menu">
         <div class="container-fluid px-xl-7">
             <a class="navbar-brand" href="/#inicio">
               <img src="{{ $plataforma->logotipo }}" width="75" alt="Logotipo {{ $plataforma->nome }}">
             </a>
             <button class="navbar-toggler outline-0 border-0 text-dark" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
               <span class="fa fa-bars"></span>
             </button>
             <div class="collapse navbar-collapse" id="navbarSupportedContent">
                  <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
                     <li class="nav-item">
                        <a class="nav-link" href="/#inicio">HOME</a>
                     </li>
                     <li class="nav-item">
                        <a class="nav-link" href="/#cursos">CURSOS</a>
                     </li>
                     <li class="nav-item">
                        <a class="nav-link" href="https://aeducacao.com.br/saladeaula/" target="_blank">PÓS</a>
                      </li>
                     <li class="nav-item">
                        <a class="nav-link" href="/graduacao">GRADUAÇÃO</a>
                     </li>
                     <li class="nav-item">
                        <a class="nav-link" href="/#contato">
                           CONTATO
                        </a>
                     </li>
                     <li class="nav-item ms-lg-4 mt-3 mt-lg-0">
                       <a class="nav-link btnAreaAluno animation-grow d-inline-block" href="https://cedetep.com/f/formulario-de-matricula/34/t/595" target="_blank">
                          <i class="fas fa-graduation-cap mr-1"></i>
                          MATRICULE-SE
                       </a>
                     </li>
                 </ul>
             </div>
         </div>
      </nav>

   </header>

   <main class="pb-5">

      <section>
       <div class="w-100 h-200 background-size-cover background-position-center" style="background-image: url('{{ $curso->foto_capa ?? $plataforma->jumbotron }}">
          <div class="position-relative h-100 px-xl-7 px-3">
          <div class="background-overlay opacity-08 h-100" style="background: rgba(0,0,0,1);"></div>

             <div class="row align-items-center h-100 position-relative">
                <div class="mt-2">
                   <h1 class="text-light font-poppins weight-800 fs-xs-35 fs-md-50 text-shadow-3" id="cursoTiulo">
                      {!! $curso->nome !!}
                   </h1>
                </div>
             </div>
          </div>
       </div>
      </section>

      <section class="pb-lg-3">
         <div class="row container-fluid px-3 gx-0 gx-lg-4 px-xl-6 mt--15px position-relative pb-5">

            <div class="col-lg-4 mb-4 mb-lg-0">
               
               <div class="bg-fundo-faca-sua-inscricao px-4-5 pt-4-5 pb-4-5 box-shadow rounded-top-20 box-shadow">
                  
                  <div class="text-center">              
                     <a href="/f/formulario-de-matricula/45/t/{{$curso->id}}" target="_blank" class="btnCursoInscreverSe w-100 animation-grow box-shadow-warning hover-d-none">
                        FAÇA SUA<br>PRÉ-INSCRIÇÃO
                     </a>
                  </div>

               </div>

               <div class="bg-white px-4-5 pt-4-5 pb-4-5 rounded-top-0 rounded-bottom-20 box-shadow">

                  <div class="bg-investimento px-4-5 pt-4 pb-5 rounded box-shadow">

                     <h1 class="p-0 m-0 text-light font-inter text-center fs-25 text-shadow-1 weight-700">Investimento</h1>

                     <hr class="border">

                     <p class="mb-3 weight-700 font-montserrat text-dark fs-17 text-shadow-1" style="text-decoration: line-through;">
                        De R$ {{ $turma->valor_a_vista ?? '0.000,00' }}
                     </p>
                     <span class="text-light weight-600 font-montserrat d-block" style="margin-bottom: -10px;">Por</span>
                     <div class="d-flex">
                      <p class="m-0 pe-2 fs-40 font-montserrat weight-700 text-light ls-05 text-shadow-1">
                          R$
                      </p>
                       <p class="mb-0 fs-40 font-montserrat weight-800 text-light ls-05 text-shadow-1">
                          {{ $turma->valor_promocional ?? 'N/D' }}
                       </p>
                     </div>

                  </div>

                  <div class="text-center mt--20px">
                    <span class="color-promo bg-promo px-3-5 py-2 rounded box-shadow weight-600 font-poppins ls-05 fs-18">
                      PROMOCIONAL
                    </span>
                  </div>

                  <div class="pt-5">
                    
                    <h1 class="weight-600 font-roboto weight-800 fs-22 text-center color-titulo">Formas de Pagamento</h1>

                    <div class="w-100 box-shadow mb-4-5 d-flex align-items-center">
                      <button type="button" class="btn-forma-pg-cc bg-forma-pg border-forma-pg w-100 weight-700 font-poppins text-shadow-1 fs-18 ls-05 px-3 rounded-0 height-categorias text-light" style="height: 60px;">
                         Cartão de Crédito
                      </button>
                      <button type="button" class="btn-forma-pg-bo border-forma-pg color-forma-pg w-100 weight-700 font-poppins bg-light fs-18 ls-05 px-3 rounded-0 height-categorias text-light" style="height: 60px;">
                         Boleto
                      </button>
                    </div>

                    <div>

                      @php
                        $formasPagamento = \App\Models\FormaPagamento::plataforma()->where('turma_id', $turma->id ?? 0)->get();
                      @endphp

                      <div id="boxFormaPagamentoCC"> 
                        @foreach($formasPagamento->where('tipo','CC') as $formaPagamento)
                          <div class="bg-f4 mb-1 px-3-5 py-2 rounded-2">
                            <p class="m-0 font-montserrat ls-05 weight-800 fs-20 text-center">
                              {{ $formaPagamento->parcela }}x de R$ {{ $formaPagamento->valor }}
                            </p>
                          </div>
                        @endforeach
                      </div>

                      <div id="boxFormaPagamentoBoleto" style="display: none;"> 
                        @foreach($formasPagamento->where('tipo','BO') as $formaPagamento)
                          <div class="bg-f4 mb-1 px-3-5 py-2 rounded-2">
                            <p class="m-0 font-montserrat ls-05 weight-800 fs-20 text-center">
                              {{ $formaPagamento->parcela }}x de R$ {{ $formaPagamento->valor }}
                            </p>
                          </div>
                        @endforeach
                      </div>

                    </div>

                  </div>

               </div>

               @if($curso->video)
                <div class="mt-4 bg-white px-4-5 pt-4-5 pb-4-5 rounded-top-20 rounded-bottom-20 box-shadow">

                  <h1 class="mb-3 weight-600 font-roboto weight-800 fs-20 color-titulo">Apresentação em Vídeo</h1>

                  <iframe width="100%" height="200" class="rounded" src="{{ \App\Models\CursoAula::getVideoYoutubeEmbed($curso->video) }}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

               </div>
               @endif

                <div class="mt-4 bg-white px-4-5 pt-4-5 pb-4-5 rounded-top-20 rounded-bottom-20 box-shadow">

                  <h1 class="weight-600 font-roboto weight-800 fs-20 color-titulo mb-4">Corpo Docente</h1>

                  <div>
                    
                    @foreach($corpoDocente as $user)
                     <div class="d-flex align-items-center">

                        <img src="{{ $user->foto_perfil ?? asset('images/no-image.jpeg') }}" alt="{{ $user->nome }} | Corpo Docente" class="me-2 img-fluid square-60 rounded-circle border object-fit-cover">
                        
                        <div>
                           <h1 class="font-poppins m-0 p-0 weight-600 fs-15 color-grafite">
                              {{ $user->nome }}
                           </h1>
                           <p class="mb-0 font-inter weight-500 fs-14 text-secondary">
                              {{ $user->titulacao ?? 'Titulação N/D' }}
                           </p>
                        </div>

                     </div>

                     <div class="ps-1 mt-1">
                        <p class="font-poppins m-0 weight-600 color-grafite fs-14">
                           Formação
                        </p>
                        <p class="mb-0 font-inter weight-500 fs-14 text-secondary">
                          {!! nl2br($user->formacao) ?? 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.' !!}
                        </p>
                     </div>

                     <hr class="mt-3">

                    @endforeach

                  </div>

                </div>

            </div>

            <div class="col-lg-8">

               <section class="mb-4 bg-white px-lg-5 px-4 pt-4-5 pb-4-5 box-shadow border-top rounded-20">

                  <p class="mb-0 pb-0 font-poppins weight-600 fs-14 color-666">
                     Informações do Curso
                  </p>

                  <hr class="border border-secondary">

                   <div class="d-lg-flex justify-content-between">
                      <div class="mb-3 mb-lg-0">
                          <p class="fs-15 mb-0 weight-600 font-poppins color-555">NÍVEL:</p>
                          <p class="fs-14 mb-0 weight-600 font-poppins color-444">{{ $curso->tipo_formacao ?? 'Não definido' }} ({{ $curso->nivel ?? 'Não definido' }})</p>
                      </div>

                      <div class="mb-3 mb-lg-0">
                          <p class="fs-15 mb-0 weight-600 font-poppins color-555">DURAÇÃO:</p>
                          <p class="fs-14 mb-0 weight-600 font-poppins color-444">{{ $curso->duracao ?? 'Não definido' }}</p>
                      </div>

                      <div class="mb-3 mb-lg-0">
                          <p class="fs-15 mb-0 weight-600 font-poppins color-555">CARGA HORÁRIA:</p>
                          <p class="fs-14 mb-0 weight-600 font-poppins color-444">{{ $curso->carga_horaria ?? '0.000' }} horas</p>
                      </div>

                      <div class="mb-3 mb-lg-0">
                          <p class="fs-15 mb-0 weight-600 font-poppins color-555">MATRIZ CURRICULAR:</p>
                           <a href="{{ $curso->matriz_curricular }}" target="_blank" class="fs-14 mb-0 weight-600 font-poppins color-blue-info-link">
                              @if($curso->matriz_curricular)
                                 Visualizar
                              @else
                                 Não definido
                              @endif
                           </a>
                      </div>

                    </div>

               </section>

               @foreach($secoesDescritivas as $secao)
               <section class="mb-3">
                 <a href="#" class="hover-d-none color-444" data-bs-toggle="collapse" data-bs-target="#secaoDesc{{$secao->id}}" aria-expanded="false" aria-controls="collapseExample">
                   <div class="bg-white px-lg-5 p-3 box-shadow border-top rounded-20">

                      <p class="mb-0 pb-0 font-poppins weight-600 fs-14 color-666">
                         {{ $secao->titulo }}
                      </p>

                     <div class="collapse @if($secao->expandido == 'S') show @endif " id="secaoDesc{{$secao->id}}">
                         <hr class="border border-secondary">
                         <p class="m-0 fs-15 color-444 weight-500 font-poppins">
                           {!! nl2br($secao->descricao) !!}
                         </p>
                     </div>

                   </div>
                 </a>
               </section>
               @endforeach

               @if(count($turmas) > 0)
               <section class="mb-3">
                 <a href="#" class="hover-d-none color-444" data-bs-toggle="collapse" data-bs-target="#secaoDisciplinas" aria-expanded="false" aria-controls="collapseExample">
                   <div class="bg-white px-lg-5 p-3 box-shadow border-top rounded-20">

                      <p class="mb-0 pb-0 font-poppins weight-600 fs-14 color-666">
                         Disciplinas
                      </p>

                      <div class="collapse show" id="secaoDisciplinas">

                        <hr class="border border-secondary">

                        @foreach($turmas as $turma)
                           
                           <p class="fs-14 mb-0 weight-600 font-poppins color-444">
                              {{ $turma->nome }}
                           </p>

                           <hr class="my-2 border-secondary">

                        @endforeach

                      </div>

                   </div>
                 </a>
               </section>
               @endif

            </div>

         </div>
      </section>

   </main>

   <footer id="contato" style="background-image: url('{{ asset('sites-proprios/cedetep/images/footer.jpg') }}'); background-repeat: no-repeat; background-size: cover; background-position: center;">
      <div class="container-fluid px-xl-8 pt-5 ps-lg-5" style="margin-top: -50px;">

         <div class="row align-items-end py-lg-4 px-3 px-md-0">

            <div class="col-lg-9">

               <h1 class="fs-35 font-montserrat text-light weight-800 text-uppercase">FALE CONOSCO</h1>
               <div class="my-4 w-80px h-2px bg-line rounded"></div>

               <img src="{{ $plataforma->logotipo }}" class="img-fluid" width="100">

               <div class="mt-2">                 

                  <div class="mb-3">
                     <span class="text-light font-poppins fs-14">
                        <i class="color-d4 fa fa-envelope"></i> 
                        {{ $plataforma->email ?? 'meuemail@exemlo.com' }}
                     </span>
                  </div>
                  <div class="mb-3">
                     <a href="https://api.whatsapp.com/send?phone=55{{preg_replace('/\D/', '', $plataforma->whatsapp)}}&text=Olá, vim através do Site {{ $plataforma->nome }}" class="text-light font-poppins fs-14">
                        <i class="color-d4 fab fa-whatsapp"></i> 
                        {{ $plataforma->whatsapp ?? '(00) 00000-0000' }}
                     </a>
                  </div>
                  <div class="mb-3">
                     <span class="text-light font-poppins fs-14">
                        <i class="color-d4 fa fa-clock"></i> 
                        Horário de Funcionamento: {{ $plataforma->horario ?? 'Segunda à sexta-feira das 09:00 às 18:00' }}
                     </span>
                  </div> 
                  <div class="mb-3">
                    <address>
                      <a href="https://www.google.com/maps/place/CEDETEP+Escola+T%C3%A9cnica+-+Pindamonhangaba/@-22.924224,-45.468257,14z/data=!4m5!3m4!1s0x0:0x70581a8d7cfa2dad!8m2!3d-22.9242244!4d-45.4682567?hl=pt-BR" target="_blank" class="btnEnderecoSiteCedetep text-light font-poppins fs-14">
                           <i class="color-d4 fas fa-map-marker-alt"></i>
                           {{ $plataforma->endereco ?? 'Pintangueiras - RJ. Rua Nossa Senhora das Graças, 16. 21932-230.' }}
                      </a>
                     </address>
                  </div>

                  <div class="mt-4 d-flex">

                     <a href="{{ $plataforma->facebook }}" target="_blank" class="me-2 hover-d-none">
                        <div class="square-40 rounded-circle social-icon social-icon-facebook flex-center">
                           <i class="text-white fab fa-facebook fs-20"></i>
                        </div>
                     </a>
                     <a href="{{ $plataforma->instagram }}" target="_blank" class="hover-d-none">
                        <div class="square-40 rounded-circle social-icon social-icon-instagram flex-center">
                           <i class="text-white fab fa-instagram fs-20"></i>
                        </div>
                     </a>

                  </div>

               </div>

            </div>

            <div class="col-lg-3 mt-5 mt-lg-0">

               <div class="mb-3">
                  <a href="https://cedetep.com/f/formulario-de-matricula/34/t/595" target="_blank" class="btn btn-primary weight-600 w-100 py-2-5 font-poppins hover-d-none">
                     MATRICULE-SE JÁ!
                  </a>
               </div>
               <div class="mb-3">
                  <a href="/login" class="btn btn-warning weight-600 w-100 py-2-5 font-poppins hover-d-none">
                     ÁREA DO ALUNO
                  </a>
               </div>
               <div class="mb-3">
                  <a href="https://cedetep.com/f/torne-se-parceiro/35/t/596" target="_blank" class="btn btn-light weight-600 w-100 py-2-5 font-poppins hover-d-none">
                     TORNE-SE PARCEIRO
                  </a>
               </div>
               <div class="mb-3">
                  <a href="https://api.whatsapp.com/send?phone=55{{preg_replace('/\D/', '', $plataforma->whatsapp)}}&text=Olá, vim através do Site {{ $plataforma->nome }}" class="btn btn-success bg-wpp text-shadow-1 weight-600 w-100 py-2-5 font-poppins hover-d-none">
                     ENTRAR EM CONTATO
                  </a>
               </div>

            </div>
 
         </div>

         <hr class="mt-4 border border-light" />

         <div class="pb-3 d-lg-flex justify-content-between align-items-center">

            <p class="text-xs-center mb-1 mb-lg-0 p-0 text-light font-poppins fs-11 ls-05">
               {{ $plataforma->nome }} © 2022 • Todos os direitos reservados • CNPJ: {{ $plataforma->cnpj }}
            </p>

            <p class="text-xs-center m-0 p-0 text-light font-poppins fs-11 ls-05">
               Desenvolvido por <a href="https://wozcode.com" target="_blank" class="text-light text-d-none fs-11 weight-600 text-shadow-1">WOZCODE</a>
            </p>
 
         </div>

      </div>
   </footer>

   <a href="https://api.whatsapp.com/send?phone=55{{preg_replace('/\D/', '', $plataforma->whatsapp)}}&text=Olá, vim através do Site {{ $plataforma->nome }}" target="_blank" class="text-white fs-30">
       <div class="fixed-bottom right-0 me-3 mb-3 z-4">
           <div class="square-60 rounded-circle bg-wpp flex-center box-shadow-wpp">
               <i class="fab fa-whatsapp"></i>
           </div>
       </div>
   </a>


   <script src="https://cdn.jsdelivr.net/npm/uikit@3.5.9/dist/js/uikit.min.js"></script>
   <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa" crossorigin="anonymous"></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.1/jquery.min.js" integrity="sha512-aVKKRRi/Q/YV+4mjoKBsE4x3H+BkegoM/em46NNlCqNTmUYADjBbeNefNxYV7giUp0VxICtqdrbqU7iVaeZNXA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

</body>
</html>