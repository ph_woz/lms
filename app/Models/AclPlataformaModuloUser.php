<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AclPlataformaModuloUser extends Model
{
    use HasFactory;

    protected $table = 'acl_plataformas_modulos_users';

    public $timestamps = false;

    protected $fillable = [
        'plataforma_id',
    	'user_id',
        'modulo_id',
        'nivel'
    ];

    public static  function boot()
    {
        parent::boot(); 
        static::creating(function ($model)
        {
            $model->plataforma_id = session('plataforma_id') ?? Auth()->user()->plataforma_id ?? Plataforma::dominio()->pluck('id')[0] ?? null;
        });
    }
    
    public function scopePlataforma($query)
    {
        return $query->where('plataforma_id', session('plataforma_id') ?? Plataforma::dominio()->pluck('id')[0] ?? null);
    }

    public function scopeUrl($query, $url)
    {
        return $query->where('user_id', \Auth::user()->id)->where('url', $url);
    }

    public function scopeUser($query, $id)
    {
        return $query->where('user_id', $id);
    }

    public static function getMeusModulosIds()
    {
        $getMeusModulosIds = AclPlataformaModuloUser::plataforma()
            ->user(Auth::id())
            ->where('nivel','>', 0)
            ->select('modulo_id')
            ->get()
            ->pluck('modulo_id')
            ->toArray();

        return $getMeusModulosIds;
    }

    public static function getModuloId($modulo_id)
    {
        $getModuloId = AclPlataformaModuloUser::plataforma()->user(\Auth::id())->where('modulo_id', $modulo_id)->first();

        return $getModuloId;
    }

    public static function verificaMeuAcessoUrlNivel($url, $nivel)
    {
        $modulo_id = PlataformaModulo::plataforma()->where('url', $url)->pluck('id')[0] ?? null;

        $acl = null;

        if($modulo_id)
            $getMeuAtualNivel = AclPlataformaModuloUser::plataforma()
                ->where('modulo_id', $modulo_id)
                ->where('user_id', \Auth::id())
                ->pluck('nivel')[0] ?? null;

        if($getMeuAtualNivel >= $nivel)
            return true;
        else
            return false;
    }

    public static function getAclNivel($url)
    {
        $modulo = WozcodeModulo::where('url', $url)->first();

        if(is_null($modulo))
        {
            return 0;
        }
        
        if($modulo->status == 1)
        {
            $nivel = 0;

        } else {

            $modulo = PlataformaModulo::plataforma()->where('url', $url)->first();
            
            if(!isset($modulo))
            {
                return 0;
            }

            if($modulo->status == 1)
            {
                $nivel = 0;

            } else {

                $acl = AclPlataformaModuloUser::getModuloId($modulo->id);

                if(isset($acl))
                {
                    $nivel = $acl->nivel;

                } else {

                    return 0;
                }
            }
        }

        return $nivel;
    }
}
