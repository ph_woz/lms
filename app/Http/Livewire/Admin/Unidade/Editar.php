<?php

namespace App\Http\Livewire\Admin\Unidade;

use Livewire\Component;
use App\Models\Unidade;
use App\Models\FluxoCaixa;
use Illuminate\Support\Facades\Http;

class Editar extends Component
{
    public $unidade;
    public $nome;
    public $telefone;
    public $celular;
    public $email;
    public $endereco;
    public $status = 0;
    public $publicar = 'N';

    public function mount($unidade_id)
    {
        $this->unidade = Unidade::plataforma()->where('id', $unidade_id)->first();
    
        $this->nome     = $this->unidade->nome;
        $this->telefone = $this->unidade->telefone;
        $this->celular  = $this->unidade->celular;
        $this->email    = $this->unidade->email;
        $this->endereco = $this->unidade->endereco;
        $this->status   = $this->unidade->status;
        $this->publicar = $this->unidade->publicar;
    }

    public function editar()
    {   
        if($this->nome != $this->unidade->nome)
        {
            if(FluxoCaixa::checkActive())
            {
                FluxoCaixa::plataforma()->where('unidade_id', $this->unidade->id)->update(['unidade_nome' => $this->nome]);
            }
        }
        
        $this->unidade->update([
            'nome'     => $this->nome,
            'telefone' => $this->telefone,
            'celular'  => $this->celular,
            'email'    => $this->email,
            'endereco' => $this->endereco,
            'status'   => $this->status,
            'publicar' => $this->publicar,
        ]);

        session()->flash('success-livewire', 'Unidade atualizada com êxito.');

        $this->dispatchBrowserEvent('toast');
    }

    public function render()
    {
        return view('livewire.admin.unidade.editar');
    }

}
