<?php

namespace App\Http\Livewire\Admin\Questao;

use Livewire\Component;
use App\Models\Questao;
use App\Models\Categoria;
use App\Models\QuestaoCategoria;
use App\Models\User;
use Illuminate\Support\Facades\Http;

class Info extends Component
{
    public $questao;
    public $cadastrante;
    public $categorias;
    public $password_to_delete;

    public function mount($questao_id)
    {
        $this->questao = Questao::plataforma()->find($questao_id);
    
        $this->cadastrante = User::plataforma()->where('id', $this->questao->cadastrante_id)->first();

        $categoriasIds = QuestaoCategoria::plataforma()->where('questao_id', $this->questao->id)->get()->pluck('categoria_id')->toArray();
        $this->categorias = Categoria::plataforma()->ativo()->whereIn('id', $categoriasIds)->orderBy('nome')->get();
    }

    public function render()
    {
        return view('livewire.admin.questao.info');
    }

    public function delete()
    {
        if(\Auth::attempt(['id' => \Auth::id(), 'password' => $this->password_to_delete, 'status' => 0]))
        {
            QuestaoCategoria::plataforma()->where('questao_id', $this->questao->id)->delete();
            Questao::plataforma()->where('id', $this->questao->id)->delete();

            session()->flash('success', 'Questão deletada com êxito.');
            
            return redirect(route('admin.questao.lista'));

        } else {

            session()->flash('danger-livewire', 'Senha incorreta.');

            $this->dispatchBrowserEvent('toast');
        }
    }
}
