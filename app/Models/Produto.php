<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Produto extends Model
{
    protected $table = 'produtos';

	public $timestamps = true;

    public $fillable = [
        'plataforma_id',
        'cadastrante_id',
        'unidade_id',
        'produtos_iguais_cores_diferentes',
        'produtos_iguais_tamanhos_diferentes',
        'nome',
        'marca',
        'modelo',
        'tamanho',
        'cor',
        'foto',
        'video',
        'descricao',
        'status',
        'visibilidade',
        'frete_gratis',
        'disponivel',
        'publicar',
        'exibir_preco',
        'valor_parcelado',
        'valor',
        'valor_promocional',
        'estoque',
        'peso',
        'comprimento',
        'largura',
        'altura',
    ];

    public static  function boot()
    {
        parent::boot(); 
        static::creating(function ($model)
        {
            $model->plataforma_id  = session('plataforma_id') ?? \Auth::user()->plataforma_id ?? 1;
            $model->cadastrante_id = \Auth::user()->id ?? null;
        });
    }

    public function scopePlataforma($query)
    {
        return $query->where('plataforma_id', session('plataforma_id') ?? \Auth::user()->plataforma_id);
    }

    public function scopeAtivo($query)
    {
        return $query->where('status', 0);
    }

    public function scopePublico($query)
    {
        return $query->where('publicar', 'S');
    }

    public static function checkActive()
    {
        $check = PlataformaModulo::plataforma()->ativo()->where('url','produto')->exists();
        
        return $check;
    }
    
}
