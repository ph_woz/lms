@extends('layouts.admin.master')
@section('content')

<nav aria-label="breadcrumb" class="mb-4-5 p-3 bg-db rounded ashadow border">
	<ol class="breadcrumb p-0 m-0">
    	<li class="breadcrumb-item"><a href="{{ route('admin.pagina.lista') }}" class="color-blue-info-link">Páginas</a></li>
    	<li class="breadcrumb-item text-dark" aria-current="page">Contato</li>
  	</ol>
</nav>

<form method="POST">
@csrf

	<section class="mb-4-5" id="boxCursoSEO">

		<a href="#boxSeo" data-bs-toggle="collapse" aria-expanded="false" class="hover-d-none">
			<div class="section-title">
				Informações de SEO para resultados de buscas em Navegadores
			</div>
		</a>
		
		<div class="collapse show bg-white border p-4" id="boxSeo">
		
			<div class="d-lg-flex mb-3">

				<div class="w-100 me-lg-3 mb-3 mb-lg-0">
					<label>Título</label>
					<input type="text" name="seo_title" class="form-control p-form" placeholder="Title" value="{{ $conteudo->seo_title ?? null }}">
				</div>

				<div class="w-100">
					<label>Palavras-chaves</label>
					<input type="text" name="seo_keywords" class="form-control p-form" placeholder="Keywords" value="{{ $conteudo->seo_keywords ?? null }}">
				</div>

			</div>

			<div class="w-100 mb-3">
				<label>Descrição</label>
				<input type="text" name="seo_description" class="form-control p-form" placeholder="Description" value="{{ $conteudo->seo_description ?? null }}">
			</div>
			
		</div>

	</section>

	<button type="submit" name="sent" value="true" class="btn-add">
		Salvar
	</button>

</form>

@endsection